<?php

$contents = $Page->getDirectoryContents();
$cwd = urlencode($Page->getParentDirectory());

$i = 0;
// controls whether to set extract_subfolder
// checks for subdirectory under root
// and ensures no files exist within root
//
$parent_entries = 0;
$prev_file = null;
$prev_depth = null;
$parent_dir = null;
$archive_head = '';
$arbaselv = null;
// pretty-printed archive tree
$contents_parsed = array();

if (is_array($contents)) {
	while (false !== ($file = current($contents))) {

		$depth = 0;
		$pos = 0;
		$fn = $file['file_name'];
		$dirs = array();
		$dirs[] = strtok($fn, "/");

		while (false !== ($tok = strtok("/"))) {
			$dirs[] = $tok;
			$depth++;
		}
		$dirslice = $depth;

		if ($depth == 0) {
			$parent_entries++;
		}


		if ($depth > $prev_depth && (is_null($prev_depth) || $prev_file['file_type'] != 'dir')) {
			/*$depth > 0 && $file['file_type'] == 'file' ||
			$depth > 1 && $file['file_type'] == 'dir') {*/
			if (is_null($prev_depth)) {
				$prev_depth = 0;
			}
			$dirs = array_slice($dirs, 0, $prev_depth - $depth);
			$new_dir = array(
				'file_name'   => join("/", $dirs),
				'file_type'   => 'dir',
				'size'        => 4096,
				'crc'         => 0,
				'packed_size' => 4096,
				'can_read'    => true,
				'can_execute' => true,
				'can_write'   => true
			);
			$depth = $prev_depth;
			$file = $new_dir;
			$fn = $file['file_name'];
		}

		if ($file['file_type'] == 'dir' && $depth == 0 && $parent_entries < 2) {
			$archive_head = $file['file_name'];
		}


		$file_trun = join("/", array_slice($dirs, $depth));

		$contents_parsed[] = array_merge(
			array(
				'depth'     => ($depth - $arbaselv),
				'file_trim' => $file_trun,
				'file_full' => $fn
			),
			$file
		);
		$prev_depth = $depth;

		if (!isset($new_dir) || isset($prev_file)) {
			next($contents);
		}

		$prev_file = $file;
	}
}

$extract_dir = $Page->extract_name_from_archive($Page->getCurrentPath());
$noconflict_ex = $Page->file_noconflict($Page->extract_name_from_archive($Page->getCurrentPath()));
?>
<tr>
	<td class="" colspan="6"><h4>Commands</h4></td>
</tr>
<tr>
	<td colspan="6" class="cell1" align="left">
		<label>Destination</label>
		<div class="input-group mb-1">
			<input type="text" id="browse_target" class="form-control" name="Extract_Path"
			       value="{{ dirname($Page->getCurrentPath()) }}" size="30"/>
			<span class="btn-group">
					<button id="browse_path" type="button" class="btn btn-secondary" name="browse_path"><i
								class="fa fa-folder-open"></i>Browse</button>
			</span>
		</div>

		<fieldset class="form-group">
			<button type="submit" value="Extract Archive" class="btn btn-primary " name="Extract"/>
			<i class="fa fa-expand"></i> Extract Archive
			</button>
			<input type="hidden" name="archive_path" value="{{ $Page->getCurrentPath() }}">
		</fieldset>

		<fieldset class="form-group">
			<label for="extract_subdir" class="block">
				<input class="checkbox-inline" type="checkbox" name="extract_subdir"
					   <?=$parent_entries > 1 ? 'checked="checked"' : ''?> id="extract_subdir"/>
				Extract files into sub-folder named <?php print($extract_dir); ?>/
			</label>
			<label for="extract_noconflict" class="block">
				<input type="checkbox" name="extract_noconflict" checked="checked" class="checkbox-inline"
				       id="extract_noconflict"/>
				Use numeric suffix if destination folder exists
			</label>
		</fieldset>
		<input type="hidden" name="archive_head" value="<?=$archive_head?>"/>
	</td>
</tr>
<tr id="comp-hdr">
	<td class="head1" width="25" align="center">
		<!--<input type="checkbox" id="select_all" />-->
	</td>
	<td class="head1" style="text-align:left;">Name</td>
	<td class="head1" width="100" align="right">Size</td>
	<td class="head1" width="60" align="center">Checksum</td>
	<td class="head1" width="70" align="center">% Packed</td>
</tr>

<tr>
	<td class="cell1" align="center">
		&nbsp;
	</td>
	<td class="cell1 name" align="left">
		<a class="node node-parent-dir"
		   href="filemanager.php?cwd=<?=\HTML_Kit::url_encode($Page->getParentDirectory()); ?>">
			Parent Directory
		</a>
	</td>
	<td class="cell1" align="right">4.00 KB</td>
	<td class="cell1" align="right"><code>&nbsp;<code></td>
	<td class="cell1" align="right"></td>
	<?php if ($Page->getState() != 'list_archive'): ?>
	<td class="cell1" align="center"><img src="/images/spacer.gif" alt="" height="1" width="50"/></td><?php endif; ?>
</tr>
<?php
if ($contents instanceof Exception || !$contents)
	return;
?>

<?php
foreach ($contents_parsed as $content):
$depth = $content['depth'];
$file_full = $content['file_full'];
$file_trim = $content['file_trim'];

?>
<tr>
	<td class="cell1" style="border:none;"
	    align="center"><!--<?php if ($content['can_read']): ?><input type="checkbox" name="file[]" value="<?php print($cwd . ':' . $file_full); ?>"><?php endif; ?>--></td>
	<td class="cell1" align="left">
		<?php
		printf('%s<img src="%s" alt="" border="0" align="middle" />',
			str_repeat('&nbsp;', 4 * $depth),
			$Page->getIconType($content['file_type'], 0)
		); ?>
		<?php
		if ($content['file_type'] != 'dir'):
			print $file_trim;
		else:
		if (0 & $content['can_read'] && $content['can_execute']): ?>
		<a href="filemanager.php?f_co=<?=$cwd . ':' . $file_full?>">
			<?php
			endif;
			print $file_trim;
			if (0 & $content['can_read'] && $content['can_execute']): ?></a><?php endif;
		endif;
		?>
	</td>
	<td class="cell1" align="right"><?php print(\Formatter::reduceBytes($content['size'])); ?></td>
	<td class="cell1" align="center"><code><?php print($content['crc']); ?></code></td>
	<td class="cell1" align="right"><?php printf("%.2f%%",
			($content['size'] - $content['packed_size']) / ($content['size'] + 1) * 100); ?></td>
	<?php if (0):
	?>
	<td class="cell1" align="center">
	<!--<?php if(($content['file_type'] != 'dir') && $content['can_write'] && $content['can_read'] ): ?><a href="filemanager.php?f=<?php print($cwd . ':' . $file_full); ?>"><img src="icon_edit_gif" alt="Edit" border="0" /><?php endif; ?>
	<?php if (($content['file_type'] != 'dir') && $content['can_write']): ?>
			<a href="?d=<?php print($cwd . ':' . $file_full);?>" onClick="return confirm('Are you sure you want to delete <?php print($file_full); ?>?');"><img border="0" src="icon_remove_gif" alt="Delete" /></a>
        <?php endif; ?> -->
		<br/><img src="/images/spacer.gif" alt="" height="1" width="50"/></td>
	<?php endif; ?>
</tr>
<?php
$i++;
$prev_file = $content['file_name'];
$prev_depth = $depth;
endforeach;
?>

<!--<tr>
    <td class="cell1" align="center"><img src="icon63_addtoclipboard_gif" OnMouseOver="tip(event,'clipboard');" OnMouseOut="untip();" name="Clipboard" /></td>
    <td colspan="5" class="cell1" align="left"><input type="submit" name="Add_Clipboard" value="Add File(s) to Clipboard" /></td>
</tr>
<tr>
    <td class="cell1"  align="center" ><img src="icon62_deletefile_gif" OnMouseOver="tip(event,'delete_files');" OnMouseOut="untip();"  name="Delete Files" /></td>
    <td colspan="5" class="cell1" align="left"><input type="submit"name="Delete" value="Delete File(s)" onClick="return confirm('Are you sure you want to delete the checked files?');" /> force&nbsp;<input type="checkbox" name="Force" /></td>
</tr>
<tr>
    <td class="cell1" align="center"><img src="icon61_createdirectory_gif" onMouseOver="tip(event,'create_directory');" onMouseOut="untip();" alt="Click here for a tip on directory creation" border="0" /></td>
    <td colspan="5" class="cell1" align="left"><input type="text" name="Directory_Name" width="10" />&nbsp;<input type="submit" value="Create Directory" name="Create" /></td>
</tr>
-->
