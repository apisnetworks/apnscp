<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2020
 */


	use Illuminate\Support\Facades\Route;

	Route::get('download/{service}/{name}', static function (Page $Page, $service, $name) {
		$fileName = data_get($Page->var($service . '-file-ext'), "$name.name");
		$fileName = BladeLite::parseString($fileName);
		if (!$fileName) {
			abort(404);
		}
		$view = view('partials.' . $service . '.config.' . $name, [
			'auth' => $Page->createAuthModel()
		]);
		return response()->streamDownload(static function () use ($view, $fileName) {
			echo $view->render();
		}, $fileName, ['Content-type' => 'application/octet-stream']);
	});
	Route::get('{view?}', 'Page@index')->where('view', '(.*)');
