@component('partials.generic-service', [
	'service' => "SSH",
	'auth' => $auth,
	'vars' => $vars,
	'port' => $auth->sshPort()
	])
	@slot("extra")
	<tr>
		<th>
			{{ _("ssh example") }}
		</th>
		<td>
			<kbd class="d-block">
				ssh -l {{ $auth->username() }}{{ '@'. $auth->domain() }} @if($auth->sshPort() !== 22) -p {{ $auth->sshPort() }} @endif {{ $auth->hostname('ssh') }}</kbd>
			or
			<kbd class="d-block">ssh @if($auth->sshPort() !== 22) -p {{ $auth->sshPort() }} @endif {{ $auth->username() }}#{{ $auth->domain() }}{{ '@' . $auth->hostname('ssh') }}</kbd>
		</td>
	</tr>
	@endslot
@endcomponent