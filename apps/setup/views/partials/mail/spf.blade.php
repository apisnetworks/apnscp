<tr>
	<th>
			<span class="ui-action-copy" data-clipboard-text="{{ MAIL_DEFAULT_SPF  }}"
			      title="Copy to Clipboard" id="clipboardCopySpf"
			      data-toggle="tooltip">
				SPF
			</span>
	</th>
	<td>
		<code>{{ MAIL_DEFAULT_SPF }}</code>
	</td>
</tr>