<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\mailertable;

	use Module\Support\Webapps;
	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->get_subdomains();
			$this->add_javascript('/apps/mailertable/mailertable.js');
			$this->add_css('/apps/mailertable/mailertable.css');
			$this->init_hostname_map();
		}

		public function get_subdomains()
		{
			$subs = array_map(
				function ($sub) {
					return !($pos = strpos($sub, ".")) ? $sub : substr($sub, 0, $pos);
				},
				array_keys($this->web_list_subdomains())
			);
			$subs[] = 'www';
			\Util_Conf::sort_domains($subs);

			return $subs;
		}

		private function init_hostname_map()
		{
			$hostnames = $this->get_hostnames();

			$js = 'var host_map = {subdomain: {';
			$maps = array();
			foreach ($hostnames['subdomain'] as $subdomain => $domains) {
				$maps[] = "'" . $subdomain . "'" . ': [' .
					trim("'" . implode("','", $domains), ",") . "']";
			}
			$js .= implode(',', $maps) . '}, domain: { ';
			$maps = array();
			foreach ($hostnames['domain'] as $domain => $subdomains) {
				$maps[] = "'" . $domain . "'" . ': [' .
					trim("'" . implode("','", $subdomains), ",") . "']";
			}
			$js .= implode(',', $maps) . '}};';
			$this->add_javascript($js, 'internal', false);
		}

		public function get_aliases()
		{
			$aliases = \Util_Conf::all_domains();
			\Util_Conf::sort_domains($aliases);

			return $aliases;
		}

		public function get_hostnames()
		{
			return Webapps::getAllPartitionedHostnames($this->getAuthContext(), false, true);
		}

		public function on_postback($params)
		{
			if (isset($params['d'])) {
				$status = $this->remove_table(base64_decode($params['d']));
				$this->bind($status);
			} else if (isset($params['add_table'])) {

				$status = $this->add_table($params['domain'], $params['subdomain']);
				$this->bind($status);
			}
		}

		public function remove_table($mEntry, $keepdns = null)
		{
			return $this->email_remove_virtual_transport($mEntry, $keepdns);
		}

		public function add_table($mDomain, $mSubdomain = '')
		{
			return $this->email_add_virtual_transport($mDomain, $mSubdomain);

		}

		public function get_mailer_table_entries()
		{
			$domains = $this->email_list_virtual_transports();
			\Util_Conf::sort_domains($domains);

			return $domains;
		}
	}

?>
