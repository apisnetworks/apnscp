<form method="post" action="changemysql.php" id="addForm">
	<h3 class="mb-0">Create Database</h3>
	<div class="mb-1">
		<div class="form-group row ">
			<div class="col-12 col-md-6 mt-1">
				<label for="database_new" class="">Database Name</label>
				<div class="input-group">
			    <span class="input-group-addon flex-row">
                    <i class="fa fa-tag mr-1 align-self-center"></i>
				    {{  $Page->getDBPrefix() }}
                </span>
					<input class="form-control mr-3" type="text" name="database_name" value="" id="database_new"/>
				</div>
			</div>
			<div class="col-12 col-md-6 mt-3 d-lg-flex align-self-end">
				<div class="btn-group">
					<button type="submit" id="create_db" class="btn main add" value="Create" name="CreateDB">
						<i class="fa fa-mysql"></i>
						Create Database
					</button>
					<button class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
					        data-toggle="collapse" aria-controls="backupOptions" data-target="#backupOptions"
					        aria-expanded="false">
						<span class="sr-only">Toggle Advanced</span>
					</button>
				</div>
			</div>
			<div class="col-12">
				<p class="note mt-3">All databases are prefixed with {{ $Page->getDBPrefix() }}</p>
			</div>
		</div>
	</div>

	<div id="backupOptions" class="collapse">
		<div class="row">
			<fieldset class="form-group col-12">
				<label for="backup_db">
					<input type="checkbox" class="checkbox checkbox-inline" name="backup_db" value="1" checked="1"
					       id="backup_db"/>
					Backup database (see <a class="ui-action ui-action-switch-app ui-action-label"
					                        href="{{ \HTML_Kit::page_url() }}">MySQL Backups</a>)
				</label>
			</fieldset>

			<fieldset class="form-group col-12 col-md-4">
				<label>Number of backups to hold</label>
				<input type="text" name="b_hold" size="2" value="{{ DATABASE_BACKUP_PRESERVE }}" class="form-control"/>
			</fieldset>

			<fieldset class="form-group col-12 col-md-4">
				<label>Compression Method</label>

				<select class="form-control custom-select" name="b_extension">
					@foreach (Sql_Module::BACKUP_EXTENSIONS as $ext)
						<option value="{{ $ext }}" @if ($ext === DATABASE_BACKUP_EXTENSION) selected="selected" @endif>
							{{ $ext }}
						</option>
					@endforeach
				</select>
			</fieldset>

			<fieldset class="form-group  col-12 col-md-4">
				<label>Backup Frequency</label>
				<div class="input-group">
					<input type="text" name="b_frequency" size="2" value="{{ DATABASE_BACKUP_SPAN }}" class="form-control"/>
					<span class="input-group-addon">days</span>
				</div>
			</fieldset>

			<fieldset class="col-12 form-group form-inline mt-1 show" id="uploadContainer">
				<div class="row">
					<div class="col-12" id="progressBox"></div>
				</div>
				<div class="row">
					<h6 class="col-12">Use SQL Export</h6>
					<div class="pa-1 col-12" style="position: relative;">
						<label class="custom-file">
							<input type="file" id="upload-file" class="custom-file-input">
							<span class="custom-file-control">
                                    <i class="fa fa-upload"></i>
                                </span>
						</label>&nbsp;

						<input type="hidden" id="UPLOAD_PROGRESS_KEY" name="{{ ini_get('session.upload_progress.name') }}"
						       value="{{ $Page->upload_id }}"/>
					</div>


					<div class="col-12">
						<div id="container-upload-list"></div>
					</div>

					<div class="col-12">
						<p class="note">
							Max upload size is {{ Formatter::changeBytes(ini_get('upload_max_filesize'), 'MB') }} &mdash;
							use <a class="ui-action ui-action-label ui-action-switch-app"
							       href="{{ Template_Engine::app('webdisk') }}">WebDisk</a> or
							<a class="ui-action ui-action-label ui-action-kb"
							   href="{{ MISC_KB_BASE }}/ftp/accessing-ftp-server/">FTP</a> for larger files.
						</p>
						<p class="note">
							Compressed database backups are supported.
						</p>
					</div>
					<div id="progressbar"></div>

				</div>
			</fieldset>
		</div>
	</div>
</form>


<form method="post" action="{{ Template_Engine::app('changemysql') }}" data-toggle="validator">
	<h3>Create User</h3>
	<div class="mb-1 clearfix row">
		@php
			$userLen = \cmd('mysql_schema_column_maxlen', 'user') - strlen($Page->getDBPrefix());
		@endphp
		<fieldset class="form-group col-12 col-md-6">
			<label>User</label>
			<div class="input-group">
            <span class="input-group-addon flex-row">
                <i class="fa fa-user mr-1 align-self-center"></i>
	            {{ $Page->getDBPrefix() }}
            </span>
				<input type="text" name="new_user_name"
				       class="form-control" @if ($userLen < 1) DISABLED @endif maxlength="{{ $userLen }}"/>
			</div>
		</fieldset>

		<fieldset class="form-group has-feedback col-12 col-md-6">
			<div class="row">
				<label class="col-md-12">Password</label>
				<div class="col-md-12 input-group">
					<div class="input-group-addon"><i class="fa fa-lock"></i></div>
					<input id="password" type="password" data-minlength="6" class="form-control"
					       name="password" value="" id="password" required/>
				</div>
				<div class="help-block col-12">
					Seven character minimum
				</div>
			</div>
		</fieldset>

		<fieldset class="form-group col-12 col-md-6 password-confirm-container">
			<div class="row">
				<label class="col-md-12">Verify Password</label>
				<div class="col-md-12 input-group">
					<div class="input-group-addon"><i class="fa fa-lock"></i></div>
					<input type="password" data-match-error="Password does not match"
					       class="form-control form-control-error"
					       data-match="#password" required name="password_confirm" value="" id="password_confirm"
					       required/>
				</div>
				<div class="col-12 help-block with-errors text-danger"></div>
			</div>
		</fieldset>

		<div class=" col-md-6 col-12 d-flex align-items-end form-group">
			<div class="btn-group">
				<button type="submit" id="create-user" name="Create_User"
				        class="btn btn-primary add ui-action ui-action-add ui-action-label " value="Add User">
					Add User
				</button>
				<button class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
				        data-toggle="collapse" aria-controls="userAdvanced" data-target="#userAdvanced"
				        aria-expanded="false">
					<span class="sr-only">Toggle Advanced</span>
				</button>
			</div>
		</div>

		<div id="userAdvanced" class="mt-1 collapse col-12">
			<div class="row">
				<fieldset class="form-group  col-12 col-md-4">
					<label>Host</label>
					<input type="text" class="form-control" name="new_user_host" value="localhost" maxlength="60"/>
				</fieldset>

				<fieldset class="form-group  col-12 col-md-4">
					<label>Max Connections</label>
					<input type="number" min=0 max={{ \Mysql_Module::MAX_CONCURRENCY_LIMIT }} name="new_max_user_connections" value="{{ \Mysql_Module::DEFAULT_CONCURRENCY_LIMIT }}"
					       class="form-control"/>
				</fieldset>

				<fieldset class="form-group col-12 col-md-4">
					<label>Max Updates</label>
					<input type="number" maxlength="5" size="2" name="new_max_updates" value="0" step=50 class="form-control"/>
				</fieldset>

				<fieldset class="form-group col-12 col-md-4">
					<label>Max Questions</label>
					<input type="number" maxlength=5 size=2 name="new_max_questions" class="form-control" step=50 value="0"/>
				</fieldset>

				<div class="form-group col-12 col-md-4 d-flex align-items-end">

					<label class="custom-control mb-2 custom-checkbox mb-0 align-items-center">
						<input type="checkbox" class="form-check-input custom-control-input"
						       name="new_use_ssl" />
						Require SSL
						<span class="custom-control-indicator"></span>
					</label>
				</div>

				<fieldset class="form-group col-12 col-md-4">
					<label>SSL Cipher</label>
					<select class="form-control custom-select" name="new_ssl_type">
						<option value="">ANY</option>
						<option value="X509">Valid X509</option>
					</select>
				</fieldset>

				<h4 class="col-12">SSL Requirements</h4>
				<fieldset class="form-group col-12 col-md-4">
					<label>Cipher Type</label>
					<textarea rows="5" class="form-control" name="new_cipher"></textarea>
				</fieldset>

				<fieldset class="form-group col-12 col-md-4">
					<label>X509 Issuer</label>
					<textarea rows="5" class="form-control" name="new_issuer"></textarea>
				</fieldset>

				<fieldset class="form-group col-12 col-md-4">
					<label>X509 Subject</label>
					<textarea rows="5" class="form-control" name="new_subject"></textarea>
				</fieldset>
			</div>
		</div>
</form>