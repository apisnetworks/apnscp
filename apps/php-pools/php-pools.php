<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

	namespace apps\php_pools;


	use Opcenter\Http\Php\Fpm\Configuration;
	use Opcenter\Http\Php\Fpm\PoolStatus;

	// @TODO namespace loading of - => _ apps broken
	class Page extends \Page_Container
	{
		use \FilesystemPathTrait;

		// @var PoolStatus $pool
		protected $poolStatus;

		protected function _layout()
		{
			parent::_layout();
			$this->add_css('php-pools.css');
			$this->add_javascript('php-pools.js');
		}

		public function configuration(string $pool): string
		{

			$ip = $this->site_ip_address();
			// @TODO pull configuration, determine deets in multipool
			$cfg = $this->getPoolConfiguration($pool);
			if (!$cfg) {
				return "POOL UNKNOWN";
			}

			$docroot = $cfg[$pool]['chdir'] ?? \Web_Module::MAIN_DOC_ROOT;
			$hostname = $this->web_get_hostname_from_docroot($docroot);

			return (string)Build::instantiateContexted($this->getAuthContext(),
				[$ip, $hostname])->readFromPath($docroot);
		}

		/**
		 * Get active pool
		 *
		 * @return PoolStatus
		 */
		public function activePool(): PoolStatus
		{
			if (isset($this->poolStatus)) {
				return $this->poolStatus;
			}

			if (isset($_GET['pool'])) {
				$poolId = $_GET['pool'];
			} else {
				$poolId = array_get($this->php_pools(), 0, []);
			}

			$this->poolStatus = new PoolStatus($this->php_pool_info($poolId));

			return $this->poolStatus;
		}

		public function getPoolLogName(string $pool): ?string
		{
			$cfg = $this->getPoolConfiguration($pool);
			return $cfg['global']['error_log'] ?? null;
		}

		public function getPoolConfiguration(string $pool): ?array
		{
			static $cfg = [];
			if (isset($cfg[$pool])) {
				return $cfg[$pool];
			}

			$file = $this->file_get_file_contents(Configuration::bindTo()->setName($pool)->getFpmConfigurationPath(true));
			return $cfg[$pool] = parse_ini_string($file, true, INI_SCANNER_RAW) ?: null;

		}

		/**
		 * Get pool name formatted for site
		 *
		 * @return string
		 */
		public function getName(): string
		{
			return substr($this->activePool()->getName(), strlen($this->getAuthContext()->site . '-'));
		}

		public function getCacheConfig() {
			return $this->activePool()->getCacheMetrics($this->getAuthContext());
		}

		public function on_postback($params)
		{
			return parent::dispatchPostback($params, $params['php-pool']);
		}

		public function managePool(string $pool, array $params): bool
		{
			if (isset($params['restart'])) {
				return $this->getApnscpFunctionInterceptor()->call('php_pool_restart', [$pool]);
			}

			if (isset($params['start']) || isset($params['stop'])) {
				$state = isset($params['start']) ? 'start' : 'stop';

				return $this->getApnscpFunctionInterceptor()->call('php_pool_set_state', [$pool, $state]);
			}
			return false;
		}

		public function handleRestart(string $pool, array $params = []): bool
		{
			return $this->managePool($pool, $params);
		}

		public function handleChangeVersion(string $pool, array $params = []): bool
		{
			return $this->php_pool_set_version($params['pool-version'], $pool);
		}

		public function handleStop(string $pool, array $params = []): bool
		{
			return $this->managePool($pool, $params);
		}

		public function handleChangeOwner(string $pool, array $params = []): bool
		{
			return $this->php_pool_change_owner($params['pool-owner']);
		}

		public function handleDownload(string $pool, array $params): bool
		{
			$log = $params['download'];
			if ( !($file = $this->file_expose($log)) ) {
				return false;
			}

			\HTML_Kit::makeDownloadable($this->domain_fs_path($file), basename($log), true);
			return true;
		}

		public function handlePhpinfo(string $pool, array $params = []) {
			$config = $this->configuration($pool);
			if ($this->isAjax()) {
				return $config;
			}

			echo $config;
			exit();
		}
	}