<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\subdomains;

	use Error_Reporter;
	use Lararia\Jobs\Job;
	use Lararia\Jobs\SimpleCommandJob;
	use Opcenter\Mail\Services\Webmail;
	use Page_Container;

	class Page extends Page_Container
	{
		private $state = 'list';

		public function __construct()
		{
			parent::__construct();
			if (isset($_GET['mode']) && $_GET['mode'] == 'add') {
				$this->state = 'add';
				$this->setTitle("Add Subdomain");
			} else {
				$this->setTitle("List Subdomains");
				$this->state = 'list';
				$this->add_javascript(
					"var users=['" . implode("','", array_keys($this->user_get_users())) . "'];",
					'internal', false, true
				);
			}

			$this->add_css('subdomains.css');


			if ($this->state == 'list') {
				$this->add_javascript('subdomains.js');
				$this->init_js('sorter', 'browser');
			} else {
				$this->init_js('browser', 'multiselect');
				$this->add_javascript('subdomain-add.js');
			}
			$this->add_javascript('apnscp.hinted();', 'internal', true, false);

		}

		public function on_postback($params)
		{
			if (array_get($params, 'mode') === 'add') {
				$this->state = 'add';
			}

			if (isset($params['src-domain'], $params['target-domain'])) {
				return $this->web_import_subdomains_from_domain($params['target-domain'], $params['src-domain']);
			}
			if (!isset($params['mode'])) {
				return;
			}

			if (isset($params['ren_orig']) && isset($params['edit'])) {
				foreach(array_keys($params['ren_orig']) as $i) {
					$old = $params['ren_orig'][$i];
					$new = $params['ren_new'][$i];
					$sd_fs = $new['subdomain_fs'];
					$sd_orig = $old['subdomain'];
					$sd_name = $new['subdomain'];
					$this->web_rename_subdomain(
						$sd_orig,
						$sd_name,
						$sd_fs ?: '/var/www/' . $sd_name
					);
					if ($old['user'] !== $new['user'])
					{
						// takeovers can take some time, background it
						$this->file_chown($sd_fs ?: '/var/www/' . $sd_name, $new['user']);
						(Job::create(new SimpleCommandJob(
							$this->getAuthContext(),
							'file_takeover_user',
							$old['user'],
							$new['user'],
							$sd_fs ?: '/var/www/' . $sd_name
						)))->
							setTags([$this->getAuthContext()->site, 'file_takeover_user'])->
							dispatch();
					}
				}
			} else if (isset($params['file-manager'])) {
				header('Location: ' . \HTML_Kit::new_page_url_params(\Template_Engine::app('filemanager'),
						array('f' => current($params['file-manager']))));
				exit();
			} else if (isset($params['Delete_Subdomains'])) {
				if (!isset($params['subdomains'])) {
					return error("no subdomains selected");
				}
				foreach ($params['subdomains'] as $subdomain) {
					$this->web_remove_subdomain($subdomain);
				}
			} else if (isset($params['Add_Subdomain'])) {
				$this->pb_add_subdomain($params);
			} else if (isset($params['delete'])) {
				// delete a subdomain
				$this->web_remove_subdomain($params['delete']);
			} else {
				// initial page
				$this->hide_pb();
			}

		}

		private function pb_add_subdomain($params)
		{
			$default = isset($params['subdomain_ft']) && $params['subdomain_ft'];
			if (!isset($params['domains']) || !$params['domains']) {
				return error("no domains selected");
			}

			$alldomains = $this->list_domains();
			$global = count($alldomains) === count($params['domains']) && count($alldomains) > 1;

			$subdomain = '';
			if (isset($params['subdomain'])) {
				$subdomain = $params['subdomain'];
			} else {
				$default = true;
			}

			// log file pesonality
			if (isset($params['create_log_profile'])) {
				$log_cb = function ($domain, $subdomain, $log) {
					$self = \apnscpFunctionInterceptor::init();
					if (!Error_Reporter::is_error()) {
						$self->logs_add_logfile($domain, $subdomain, $log);
					}
				};
				if (!$default && $global) {
					$log_domain = '*';
					$log_subdomain = $subdomain;
					$log_name = $log_subdomain . '_log';
					$this->add_callback($log_cb, $log_domain, $log_subdomain, $log_name);
				} else {
					foreach ($params['domains'] as $domain) {
						$log_domain = $domain;
						if ($default) {
							// fallthrough for subdomains
							$subdomain = $domain;
							$log_name = '_' . $domain . '_log';
							$log_subdomain = '*';
						} else {
							// local subdomain
							$log_subdomain = $subdomain;
							$log_name = $subdomain . '.' . $domain . '_log';
						}
						//@TODO properly detect when callback returns warning and not error, i.e. FAILED status
						$this->add_callback($log_cb,
							$log_domain,
							$log_subdomain,
							$log_name);
					}
				}
			}

			if ($params['subdomain_path']) {
				$path = $params['subdomain_path'];
			} else if ($default) {
				$path = '/var/www/_.' . $params['domains'][0];
			} else {
				$path = '/var/www/' . $subdomain;
			}
			if ($path[0] != '/') {
				return error("subdomain folder must be a fully-qualified location beginning with " .
					"/ such as /var/www/myfolder");
			}

			// subdomain bound to all domains
			$user = $params['user'] ?? $this->getAuthContext()->username;
			$stat = $this->file_stat($path);
			if (!$stat) {
				$this->file_create_directory($path, 0755, true);
			}

			$chown = $this->file_chown($path, $user, true);

			if ($stat && $stat['owner'] !== $user && $chown) {
				warn("Docroot `%(path)s' owner changed from `%(oldowner)s' to `%(newowner)s'", [
					'path'     => $path,
					'oldowner' => $stat['owner'],
					'newowner' => $user
				]);
			}

			if ($global) {
				return $this->web_add_subdomain($subdomain, $path);
			}

			$success = 0;
			foreach ($params['domains'] as $domain) {
				if ($default) {
					$hostname = $domain;
				} else {
					$hostname = $subdomain . '.' . $domain;
				}
				if (!$this->web_add_subdomain($hostname, $path)) {
					continue;
				}
				$success++;
			}

			return $success > 0;
		}

		public function list_domains()
		{
			$domains = $this->aliases_list_aliases();
			sort($domains, SORT_LOCALE_STRING);
			array_unshift($domains, $this->common_get_service_value('siteinfo', 'domain'));

			return $domains;

		}

		public function get_state()
		{
			return $this->state;
		}

		public function list_subdomains()
		{
			$subdomains = $this->web_list_subdomains();
			\Util_Conf::sort_domains($subdomains, 'key');
			foreach ($this->email_webmail_apps() as $loc) {
				if (isset($subdomains[$loc]) && /* unresolved location */
					!$subdomains[$loc]) {
					unset ($subdomains[$loc]);
				}
			}

			return array_filter($subdomains, static function ($path) {
				return !str_starts_with($path, Webmail::REDIRECTION_PATH . '/');
			});
		}

		public function list_users()
		{
			return $this->user_get_users();
		}

		public function subdomain_info(string $subdomain)
		{
			return $this->web_subdomain_info($subdomain);
		}

		public function getSubdomainUsers(): array
		{
			$users = $this->user_get_users();
			array_forget($users, $this->getAuthContext()->username);
			return [-1 => $this->getAuthContext()->username] + array_keys($users);
		}
	}