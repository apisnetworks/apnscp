var rename_ctr = 0;
$(document).ready(function () {
	$('#Delete_Subdomains').click(function () {
		return confirm("Are you sure you wish to delete the selected domains?");
	});

	$('.ui-action-manage-files').click(function (e) {
		var $target = $(e.target);
		var win = window.open(apnscp.appUrl('filemanager', '?mini&cwd=' + $target.val(), 'file-manager-window', 'menubar=no,toolbar=no'));
		win.focus();
		return false;
	});
	$(".ui-action-edit").click(function (e) {
		make_editable.apply(this, [e.target.value]);
		return false;
	});

	$('#subdomains').tablesorter();

	$('tr.entry').highlight();

	$('.subdomain-filter').click(function (e) {
		if (e.target.id == "do_filter") {
			var field = $('#filter_spec').val(),
				pattern = $('#filter').val(),
				re = new RegExp(pattern, "i"),
				selector = 'span.' + field;
			$('tr.entry').filter(function () {
				return re.test($('td', this).children(selector).text()) == false;
			}).fadeOut('fast', function () {
				$(this).removeClass('ui-highlight').find(':checkbox').prop('checked', false);
			});
			var $el = $('<li class="ui-action-delete filter-' + field + '" />').text(pattern).click(function () {
				return remove_filter.apply(this, [pattern, $(this).data("selector")]);
			});

			$el.data("selector", selector);
			$(this).find('.ui-active-filters').append($el);
			if ($('#reset_filter:hidden').length > 0) {
				$('#reset_filter').fadeIn();
			}
			return false;
		}
		if (e.target.id == "reset_filter") {
			$(this).find('.ui-active-filters').empty();
			$('#reset_filter').hide();
			return $('tr.entry:hidden').fadeIn();
		}
	}).bind('keydown', function (e) {
		if (e.keyCode === $.ui.keyCode.ESCAPE) {
			$('#reset_filter').click();
			$('#filter').val('');
		} else if (e.keyCode === $.ui.keyCode.ENTER) {
			$(this).triggerHandler('click');
		}
	});
	;

	$('#cloneSubdomains').click(function () {
		return clone_subdomains();
	});
});

function remove_filter(pattern, selector) {
	var re = new RegExp(pattern, "i");
	$('tr.entry:hidden').filter(function () {
		return re.test($('td', this).children(selector).text()) == false;
	}).fadeIn('fast');

	$(this).remove();
	if ($('.ui-active-filters li').length == 0) {
		$('#reset_filter').fadeOut();
	}
}

function clone_subdomains() {

	var $btn, dialog = apnscp.modal($('#cloneSubdomainModal'), {
		buttons:
			[
				$btn = $('<button>').attr({
					'type': 'submit',
					name: 'clone-mailboxes',
					'class': 'btn btn-primary',
					value: 1,
					'id': 'confirmRecordSubmit'
				}).prop({
					'disabled': true
				}).text("Clone Subdomains").click(function () {
					$('.modal-content form').submit();
				})
			]
	});

	dialog.on('show.bs.modal', function () {
		$('select', this).on('change', function () {
			if (this.name === "target-domain") {
				$('#cloneSubdomainModal .dest').text(this.value);
			} else {
				$('#cloneSubdomainModal .source').text(this.value);
			}
			$('#cloneSubdomainModal #confirmRecordImport').prop(
				{
					'checked': false,
					'disabled': $('#cloneSubdomainModal .dest').text() == $('#cloneSubdomainModal .source').text()
				});
			if ($btn) {
				$btn.prop('disabled', true);
			}

		}).each(function () {
			$(this).triggerHandler('change');
		});

		var $indicator = $('<i class="ml-2"></i>'),
			$btn = dialog.find('.btn-primary').find('.ui-ajax-indicator').remove().end().append($indicator);
		dialog.find('.ui-ajax-error-msg').remove();

		$(':checkbox', dialog).on('change', function () {
			var $btn = $('.modal-footer :submit');
			$btn.prop('disabled', !$(this).prop('checked'));
		});
		$('form', dialog).on('submit', function () {
			$btn.data('old-title', $(this).text()).text('Processing').append($indicator.addClass('ui-ajax-loading'));
		});
	}).modal('show');
}

function make_editable(hostname) {
	++rename_ctr;
	var $row = $(this).parent().closest('.entry');
	$('span', $row).each(function () {
		if ($(this).data('immutable')) {
			return;
		}

		var $html, val = $(this).text().trim();

		switch ($(this).data('source')) {
			case 'user':
				var owner = $(this).text().trim();
				$html = $('<select name="ren_new[' + rename_ctr + '][user]"  class="removable form-control custom-select"/>');
				for (var i in users) {
					var selected = '', user = users[i];
					if (user == owner)
						selected = " selected='SELECTED'";
					$html.append($('<option value="' + user + '"' + selected + '>' + user + '</option>'));
				}
				break;
			case 'file':
				$html = $('<input type="text">').attr({
					'class': 'form-control removable',
					'value': val,
					'name': 'ren_new[' + rename_ctr + '][' + $(this).get(0).className + ']'

				}).add($html);
				var path = $(this).text().trim(), $selectField = $html;
				$html = $('<div class="input-group">').append([$html, $('<button id="browse" type="button" class="removable btn btn-secondary" name="browse_path">\n' +
					'<i class="fa fa-folder-open"></i>\n' +
					'Browse\n' +
					'</button></div>').click(function () {

					apnscp.explorer({
						selected: path,
						filter: 'filter=file,0;chown,1;show,/var/www;show,/home',
						selectField: $selectField,
						onSelect: function (localFile) {
							$('#selected_dir').text(localFile.trim());
						}
					}, path);
				})]);
				break;
			default:
				$html = $('<input type="text">').attr({
					'class': 'form-control removable',
					'value': val,
					'name' : 'ren_new[' + rename_ctr + '][' + $(this).get(0).className + ']'

				});
				break;
		}

		$prev = $('<input type="hidden" />').attr({
			name: 'ren_orig[' + rename_ctr + '][' + $(this).get(0).className +']',
			value: val
		});
		var $el = $(this).hide().attr('data-original', val).find(':hidden').remove().end().after([$prev, $html]);
	});
	var $reset = $('<button />').attr({
		'class': 'btn btn-secondary warn ml-1',
		'name': 'reset',
		'type': 'button'

	}).text("Reset").click(function () {
		var $row = $(this).closest('.entry');
		$('span', $row).each(function () {
			$(this).show().text($(this).data('original')).siblings(':hidden').remove();
		});
		$row.find('.removable').remove();
		$(this).closest('.save-group').siblings('.btn-group').show().end().remove();
		return true;
	});

	var $saveGroup = $(
		'<div class="btn-group save-group">' +
		'<button type="submit" class="btn btn-primary" name="edit">Save</button>' +
		'</div>'
	).append($reset);
	$('.actions .btn-group', $row).hide().parent().append($saveGroup);
	$(":text:visible:enabled:first", $row).focus();
	return false;
}