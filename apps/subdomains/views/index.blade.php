<div class="row hidden-print mb-1">
	<div class="col-12 text-right">
		Change Mode:
		@if ($Page->get_state() == 'add')
			<a class="ui-action ui-action-switch-app ui-action-label" href="{{ \HTML_Kit::new_page_url_params(null, ['mode' => 'list']) }}"
				>List Subdomains
			</a>
		@else
			<a class="ui-action ui-action-switch-app ui-action-label" href="{{ \HTML_Kit::new_page_url_params(null, ['mode' => 'add']) }}"
				>Add Subdomain
			</a>
		@endif
	</div>
</div>

@includeWhen($Page->get_state() === 'list', 'partials.list-toolbar')

<!-- main object data goes here... -->

<form method="post" id="{{ $Page->get_state() === 'list' ? 'listForm' : 'addForm' }}" action="{{ \HTML_Kit::new_page_url_params(null, ['mode' => 'list']) }}">
	@includeWhen($Page->get_state() === 'list', 'list-subdomains')
	@includeWhen($Page->get_state() !== 'list', 'add-subdomain')
</form>

@includeWhen($Page->get_state() === 'list', 'partials.clone-modal')