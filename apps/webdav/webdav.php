<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\webdav;

	use Page_Container;

	class Page extends Page_Container
	{

		public function __construct()
		{
			parent::__construct();
			$this->add_javascript('webdav.js');
			$this->add_css('webdav.css');
			$this->init_js('browser');

		}

		public function on_postback($params)
		{
			if (isset($params['synchronize'])) {
				$this->aliases_synchronize_changes();
			} else if (isset($params['add'])) {
				$this->web_bind_dav($params['new_directory'], $params['provider']);
			} else if (isset($params['d'])) {
				$dir = key($params['d']);
				$this->web_unbind_dav($dir);
			}

		}

		public function svn_enabled()
		{
			return $this->verco_svn_enabled();
		}

		public function get_dav_locations()
		{
			return $this->web_list_dav_locations();
		}


	}

?>
