$(document).ready(function () {
	$('#browser').click(function () {
		apnscp.explorer(
			{selectField: '#webdav_dir'},
			$('#webdav_dir').val()
		);
		return false;
	});
});

function rm(url) {
	location.assign(apnscp.appUrl(null, '?d=' + url));
}