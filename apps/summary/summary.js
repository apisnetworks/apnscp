var AccountSummary = {
	truncateFields: ['div.mail:contains(Domains Accepting) > div',
		'div.general_information:contains(Domains Hosted) .lvl-3:eq(1)',
		'div.sql:contains(\MySQL) div:contains(Database List)',
		'div.sql:contains(\PostgreSQL) div:contains(Database List)']
	,

	truncate: function (settings) {
		var fields = this.truncateFields;
		var fl = fields.length;
		for (var i = 0; i < fl; i++) {
			var field = fields[i];
			var $ul = $(field);
			var $frag = this.createFrag($ul);
			//$ul.hide().before($('<p>').append(AccountSummary.createFrag($ul)));
			$ul.hide();
			$('<div class="frag">').append($frag).insertBefore($ul).append($ul);
		}
	},

	createFrag: function ($rel) {
		var el = $('<a class="hide_frag b dropdown-toggle">Show Entries</a>').click(function () {
			var text;
			if (el.text().match(/show entries/i)) {
				text = "Hide Entries";
				$(this).addClass('expanded');
			} else {
				text = "Show Entries";
				$(this).removeClass('expanded');
			}
			el.text(text);
			$rel.toggle('medium');
			return false;
		});
		return el;
	}

}

$(document).ready(function () {
	$('#amnesty').click(function () {
		var AMNESTY_BOOST = 100,// from Site_Module::AMNESTY_MULTIPLIER
			width = 550,
			$btns = $('<div class="my-1"/>'),
			$msg = $btns.append($("<p>Storage amnesty will increase your storage by " + AMNESTY_BOOST + "% for 12 hours. Your site will " +
				"continue to operate while you resolve storage. This may be done once a month at most, so use it wisely!</p>")),
			$indicator = $('<span/>');

		$btns.append(
			$('<button type="button" class="btn btn-primary">Request Amnesty</button>').append($indicator).click(function () {
				$('#amnesty-status-msg').remove();
				apnscp.cmd('diskquota_amnesty', [], {indicator: $indicator}).done(function (e) {

					var $frag = $('<span id="amnesty-status-msg" class="ui-ajax-success-msg ui-ajax-error-msg-block"/>').text("Storage amnesty granted. Refresh the storage gauge (top-right) to see changes. ").append($('<a href="#" class="mt-3 btn btn-primary">Dismiss</a>').click(function (e) {
						$('#modal').modal('hide');
						return false;
					}));
					$indicator.parent().parent().after($frag);
				}).fail(function (e, msg) {
					var json = $.parseJSON(e.responseText);
					if (json.errors) {
						var $frag = $('<div id="amnesty-status-msg" class="ui-ajax-error-msg ui-ajax-error-msg-block"/>').text(json.errors[0]);
						$indicator.parent().after($frag);
					}
					return true;
				});
			})
		);
		$('#modal .modal-body').empty().append($btns).attr('id', 'storage-crisis').closest('#modal').modal();
	});
});