<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\quotatracker;

	use Image_Color;
	use Page_Container;

	class Page extends Page_Container
	{
		const MAX_RECORDS = 100;
		const FORECAST_LIMIT = 9125/*365*25*/
		;
		private $quota_records;
		private $models;

		// maximum records per user
		private $user_quota;
		private $_storageData;
		private mixed $_legend;

		public function __construct()
		{
			parent::__construct();
			$this->models = array();
			$this->user_quota = array();

			$scripts = array('flot', 'flot.errorbars', 'flot.navigate', 'flot.dashed');
			$browser = (array)\HTML_Kit::get_browser();

			call_user_func_array(array($this, 'init_js'), $scripts);
			$this->add_javascript('quota.js');
			$this->add_css('quota.css');

			$users = $this->user_get_users();
			$quotas = $this->user_get_quota(array_keys($users));

			$css = '<style type="text/css">';
			foreach ($users as $user => $info) {
				$this->user_quota[$user] = $quotas[$user];
				$quota = $quotas[$user];
				$css .= '#quota-' . $user . ' .ui-gauge-used { width:' .
					round($quota['qused'] / $quota['qhard'] * 100) . '%; }' .
					"\n";
			}
			$css .= '</style>';

			$this->add_head($css);
		}

		private static function regress(array &$Y, array &$X, array &$W)
		{
			$V = array();   // least squares and var/cover matrix
			$C = array();   // coefficients
			$SEC = array(); // std error of coefficients

			// Y[j]   = j-th observed data point
			// X[i,j] = j-th value of the i-th independent variable
			// W[j]   = j-th weight value

			$M = count($Y);            // number of data points
			$N = floor(count($X) / $M);  // number of linear terms
			print $N . "\n";
			$NDF = $M - $N;            // degrees of freedom
			$DY = $Ycalc = array_fill(0, $M, 0);
			if ($NDF < 1) {
				return false;
			}
			$SEC = $C = $V = array_fill(0, $N, 0);
			$B = array_fill(0, $N, 0); // Vector for LSQ

			// Clear the matrices to start out
			for ($i = 0; $i < $N; $i++) {
				$V[$i] = array();
				for ($j = 0; $j < $N; $j++) {
					$V[$i][$j] = 0;
				}
			}
			// Form least squares matrix
			for ($i = 0; $i < $N; $i++) {
				for ($j = 0; $j < $N; $j++) {
					$V[$i][$j] = 0;
					for ($k = 0; $k < $M; $k++) {
						$V[$i][$j] = $V[$i][$j] +
							$W[$k] * $X[$i][$k] * $X[$j][$k];
						print $k . " " . $V[$i][$j] . "\n";
					}
				}
				$B[$i] = 0;
				for ($k = 0; $k < $M; $k++) {
					$B[$i] = $B[$i] + $W[$k] * $X[$i][$k] * $Y[$k];
				}
			}

			// V now contains the raw least squares matrix
			if (!self::symmetricMatrixInvert($V)) {
				return false;
			}

			// V now contains the inverted LSM
			// Matrix multiply to get coefficients C=VB
			for ($i = 0; $i < $N; $i++) {
				$C[$i] = 0;
				for ($j = 0; $j < $N; $j++) {
					$C[$i] = $C[$i] + $V[$i][$j] * $B[$j];
				}
			}
			// Calculate statistics
			$TSS = $RSS = $YBAR = $WSUM = 0;
			for ($k = 0; $k < $M; $k++) {
				$YBAR += $W[$k] * $Y[$k];
				$WSUM += $W[$k];
			}

			$YBAR /= $WSUM;

			for ($k = 0; $k < $M; $k++) {
				$Ycalc[$k] = 0;
				for ($i = 0; $i < $N; $i++) {
					$Ycalc[$k] = $Ycalc[$k] + $C[$i] * $X[$i][$k];
				}
				$DY[$k] = $Ycalc[$k] - $Y[$k];
				$TSS += $W[$k] * ($Y[$k] - $YBAR) * ($Y[$k] - $YBAR);
				$RSS += $W[$k] * $DY[$k] * $DY[$k];
			}
			$SSQ = $RSS / $NDF;
			$RYSQ = 1 - $RSS / $TSS;
			$FReg = 999999999;
			if ($RYSQ < 0.999999999) {
				$FReg = $RYSQ / (1 - $RYSQ) * $NDF / ($N - 1);
			}
			$SDV = sqrt($SSQ);

			// Calculate the var-covar matrix and std error of coefficients
			for ($i = 0; $i < $N; $i++) {
				for ($j = 0; $j < $N; $j++) {
					$V[$i][$j] = $V[$i][$j] * $SSQ;
				}
				$SEC[$i] = sqrt($V[$i][$i]);

			}
			self::$regvar = array(
				'V'     => $V,
				'C'     => $C,
				'SEC'   => $SEC,
				'RYSQ'  => $RYSQ,
				'SDV'   => $SDV,
				'FReg'  => $FReg,
				'Ycalc' => $Ycalc,
				'DY'    => $DY
			);

			return true;
		}

		private static function symmetricMatrixInvert(array &$v)
		{
			$N = floor(sqrt(count($v)));
			$Q = $t = array_fill(0, $N, null);
			// invert a symmetric matrix in V
			$R = array_fill(0, $N, 1);
			$K = 0;
			for ($M = 0; $M < $N; $M++) {
				$big = 0;
				for ($L = 0; $L < $N; $L++) {

					$AB = abs($v[$L][$L]);
					if ($AB > $big && $R[$L] != 0) {
						$big = $AB;
						$K = $L;
					}
				}
				if ($big == 0) {
					return false;
				}

				$R[$K] = 0;
				$Q[$K] = 1 / $v[$K][$K];
				$t[$K] = 1;
				$v[$K][$K] = 0;
				if ($K != 0) {
					for ($L = 0; $L < $K; $L++) {
						$t[$L] = $v[$L][$K];
						if ($R[$L] == 0) {
							$Q[$L] = $v[$L][$K] * $Q[$K];
						} else {
							$Q[$L] = -$v[$L][$K] * $Q[$K];
						}
						$v[$L][$K] = 0;

					}
				}

				if (($K + 1) < $N) {
					for ($L = $K + 1; $L < $N; $L++) {
						if ($R[$L] != 0) {
							$t[$L] = $v[$K][$L];
						} else {
							$t[$L] = -$v[$K][$L];
						}
						$Q[$L] = -$v[$K][$L] * $Q[$K];
						$v[$K][$L] = 0;
					}
				}
				for ($L = 0; $L < $N; $L++) {
					for ($K = $L; $K < $N; $K++) {
						$v[$L][$K] = $v[$L][$K] + $t[$L] * $Q[$K];
					}
				}
			}
			$M = $N;
			$L = $N - 1;
			for ($K = 0; $K < $N; $K++) {
				$M = $M - 1;
				$L = $L - 1;
				for ($j = 0; $j < $L; $j++) {
					$v[$M][$j] = $v[$j][$M];
				}
			}

			return true;
		}

		public function getModel($user)
		{
			return $this->models[$user];
		}

		public function getCurrentUsage($mUser)
		{
			return $this->user_quota[$mUser];
		}

		// http://www.codeproject.com/KB/recipes/LinReg.aspx

		public function getAccountQuota()
		{
			return $this->getApnscpFunctionInterceptor()->common_get_service_value('diskquota', 'quota') * 1024;
		}

		// Compute various statistics from a series of weighted linear regressions

		public function getColor($item, $extinfo = null)
		{
			if ($extinfo) {
				$item = $item . ' ' . $extinfo;
			}
			if (isset($this->_legend[$item])) {
				return $this->_legend[$item];
			}
			warn('color not found for `%s\' in graph `%s\'',
				$item, $extinfo);

			return '#8a8a8a';
		}

		public function getStorageData()
		{
			if (isset($this->_storageData)) {
				return $this->_storageData;
			}

			$cache = \Cache_Account::spawn();
			$key = "qh.all";

			if (false !== ($data = $cache->get($key))) {
				$data =\Util_PHP::unserialize(gzinflate($data));
				$this->_storageData = $data;

				return $data;
			}

			$data = $this->getUsers();
			$cache->set($key, gzdeflate(serialize($data)));
			$this->_storageData = $data;

			return $this->_storageData;
		}

		public function getUsers()
		{
			if (!isset($this->quota_records)) {
				$users = $this->user_get_users();
				foreach ($users as $user => $misc) {
					$quota = $this->getApnscpFunctionInterceptor()->user_get_quota_history($user, -1);
					$quota = $this->collapseRecords($quota);
					$color = $this->_setColor($user);
					$this->quota_records[$user] = array(
						'start' => 0,
						'eq'    => -1,
						'color' => $color,
						'data'  => $quota
					);

					if (!$quota) {
						continue;
					}

					$this->quota_records[$user]['start'] = $quota[0]['ts'];
					$this->quota_records[$user]['eq'] = $this->calculateModel($quota, 'ts', 'quota');
				}
			}

			return $this->quota_records;
		}

		private function collapseRecords($quota)
		{
			$items = count($quota);
			$nbuckets = ceil($items / self::MAX_RECORDS);
			$newquota = array();

			for ($i = 0; $i < $items; $i += $nbuckets) {
				$slice = array_slice($quota, $i, $nbuckets);
				$max = $nbuckets - 1;
				if (!isset($slice[$max])) {
					$max = count($slice) - 1;
				}
				$sum = array_sum(
					array_column($slice, 'quota')
				);

				$collapsed = array(
					'quota' => $sum / ($max + 1),
					'ts'    => $slice[$max]['ts']
				);
				$newquota[] = $collapsed;
			}

			return $newquota;


		}

		/**
		 * Set hex color for item on graph
		 *
		 * @param string $item
		 * @param string $graph
		 * @param string $color hex color
		 */
		private function _setColor($item, $extinfo = null, $color = null)
		{
			if (is_null($color)) {
				list ($h, $s, $l) = Image_Color::randhsl();
				$color = Image_Color::hsl2hex($h, $s, $l);
			} else {
				if ($color[0] != '#' || strspn($color, 'abcdef0123456789', 1)) {
					return error("invalid hex value `%s'", $color);
				}
			}
			if ($extinfo) {
				$item = $item . ' ' . $extinfo;
			}
			$this->_legend[$item] = $color;

			return $color;
		}

		public function calculateModel(array $mData, $X = 0, $Y = 1, $crit = -1)
		{
			if (count($mData) < 2) {
				return array('a' => 0, 'b' => 0);
			};

			/**
			 * Equation:
			 * y = b0 + b1*x + E
			 * Where
			 * b1 = sigma(xy) - 1/n*sigma(x)*sigma(y)
			 *      ---------------------------------
			 *        sigma(x^2) - 1/n*sigma(x)^2
			 *
			 * y = y-bar - b1*x-bar
			 * X = 0
			 */
			$y_bar = 0;
			$x_bar = 0;
			$sxy = 0;
			$sx2 = 0;
			$sx = 0;
			$sy = 0;
			$b1 = $b0 = 0;
			$n = sizeof($mData);
			$x0 = $mData[0][$X];
			$y0 = $mData[0][$Y];
			$xn = $mData[$n - 1][$X] - $x0;
			$sw = 0;
			$d = 1;
			$xprev = 0;
			$yprev = 0;
			$yt = 0;
			/*printf ("%4s %7s %10s %5s %5s %5s %10s %10s<br />",
				"i",'w','Xi','Yi','Xi-w', 'Yi-w','Xi-Xi\'', 'Yi-Yi\''*/
			//);
			$weights = array();
			$YV = array();
			$XV = array();
			for ($i = 1; $i < $n; $i++) {
				$xi = $mData[$i][$X] - $x0;
				$yi = $mData[$i][$Y] - $y0;
				$w = pow((1 - $d) + $xi / $xn * $d, 2);
				$weights[] = $w;
				$YV[] = $yi;
				$XV[] = array($xi);
				$sw += $w;
				$sxy += $w * $xi * $yi;
				$sx += $w * $xi;
				$sy += $w * $yi;
				$sx2 += $w * $xi * $xi;

				/* printf ("%4d %2.5f %10d %5d %5d %5d %10d %10d<br />",
					 $i,
					 $w,
					 $xi,
					 $yi,
					 $xi*$w,
					 $yi*$w,
					 ($xi-$xprev),
					 $yi-$yprev);
					$yt += ($yi-$yprev);*/
				$xprev = $xi;
				$yprev = $yi;
			}
			$sxy /= $sw;
			$sx /= $sw;
			$sy /= $sw;
			$sx2 /= $sw;
			//print $sx2." ".$sy." ".$sx." ".$sxy."<br />";
			$n--;
			$x = $y = 0;
			if (($sx * $sx - $n * $sx2) != 0) {
				$x = ($sx * $sy - $n * $sxy) / ($sx * $sx - $n * $sx2);
				$y = ($sx * $sxy - $sy * $sx2) / ($sx * $sx - $n * $sx2);
			}

			return array(
				'a' => $y,
				'b' => $x
			);

		}

	}

?>