<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace apps\Horizon;

	use Frontend\Multipath;
	use Laravel\Horizon\Horizon;

	class Page extends \Page_Container
	{
		protected $container;

		public function __construct($doPostback = true)
		{
			\Page_Renderer::hide_all();
			\Page_Renderer::set(\Page_Renderer::HIDE_BUILTIN_RENDERER);
			Horizon::auth(function () {
				return \Auth::get_driver()->session_valid() && (\Session::get('level') === PRIVILEGE_ADMIN);
			});
			$app = \Lararia\Bootstrapper::app();
			$kernel = $app->make(\Illuminate\Contracts\Http\Kernel::class);
			$request = \Illuminate\Http\Request::capture();
			$request->server->set('REQUEST_URI', Multipath::strip($request->server->get('REQUEST_URI', '')));
			$response = $kernel->handle($request);
			if (!$response) {
				fatal("failed to load Horizon");
			}
			$response->send();

			$kernel->terminate($request, $response);
			exit();
		}
	}

