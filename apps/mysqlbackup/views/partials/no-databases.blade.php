<div class="alert alert-info">
	No databases found - create one through
	<a class="ui-action ui-action-switch-app ui-action-label"
	   href="{{ \Template_Engine::app("change{$Page->getMode()}") }}"
		>{{ \Template_Engine::init()->getApplicationFromId("change{$Page->getMode()}")->getName() }}
	</a>
</div>