<div id="add-backup" class="row mt-1">

	<fieldset class="form-group col-12 col-md-4 col-lg-2">
		<label for="database_name">Database</label>
		<select class="form-control custom-select" name="database_name" id="database_name">
			@foreach ($Page->get_dbs() as $db)
				<option value="{{ $db }}">
					{{ $db }}
				</option>
			@endforeach
		</select>
	</fieldset>

	<fieldset class="form-group col-12 col-md-4 col-lg-2">
		<label>Number of backups to hold</label>
		<input type="text" name="b_hold" size="2" value="{{ DATABASE_BACKUP_PRESERVE }}" class="form-control"/>
	</fieldset>

	<fieldset class="form-group col-12 col-md-4 col-lg-2">
		<label>Compression Method</label>

		<select class="form-control custom-select" name="b_extension">
			@foreach (Sql_Module::BACKUP_EXTENSIONS as $ext)
				<option value="{{ $ext }}" @if ($ext === DATABASE_BACKUP_EXTENSION) selected="selected" @endif>
					{{ $ext }}
				</option>
			@endforeach
		</select>
	</fieldset>

	<fieldset class="form-group  col-12 col-md-4 col-lg-2">
		<label>Backup Frequency</label>
		<div class="input-group">
			<input type="text" name="b_frequency" size="2" value="{{ DATABASE_BACKUP_SPAN }}"
			       class="form-control"/>
			<span class="input-group-addon">days</span>
		</div>
	</fieldset>


	<fieldset class="form-group col-12 col-md-4 col-lg-2">
		<label class="hidden-sm-down col-12">&nbsp;</label>
		<label for="email" class="custom-checkbox custom-control my-1">
			<input class="custom-control-input " data-toggle="tooltip"
			       title="Sent to email address on file (Acccount &gt; Settings)" type="checkbox" name="email"
			       id="email" value="1"/>
			<span class="custom-control-indicator mr-2"></span>
			Email Confirmation

		</label>
	</fieldset>

	<fieldset class="form-group col-12 col-md-4 col-lg-2">
		<label class="hidden-sm-down col-12">&nbsp;</label>
		<input type="submit" name="Add" value="Add Backup" class="btn primary"/>
	</fieldset>
</div>