<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\mysqlbackup;

	use Page_Container;

	class Page extends Page_Container
	{
		private $mode;

		public function __construct()
		{
			$this->add_css('/apps/mysqlbackup/mysqlbackup.css');
			if (strstr(\HTML_Kit::page_url(), "mysql")) {
				$this->mode = 'mysql';
			} else {
				$this->mode = 'pgsql';
			}
			$this->add_javascript('$("[data-toggle=tooltip]").tooltip();', 'internal');
			parent::__construct();
		}

		public function on_postback($params)
		{
			if (isset($params['Add'])) {
				if ($this->mode == 'mysql') {
					$func = 'mysql_add_backup';
				} else {
					$func = 'pgsql_add_backup';
				}
				$ret = $this->$func(
					$params['database_name'],
					$params['b_extension'],
					(int)$params['b_frequency'],
					(int)$params['b_hold'],
					$params['email'] ?? null
				);
				$this->bind($ret);
			} else if (isset($params['Edit'])) {

			} else if (isset($params['d'])) {
				if ($this->mode == 'mysql') {
					$ret = $this->mysql_delete_backup(base64_decode($params['d']));
				} else {
					$this->pgsql_delete_backup(base64_decode($params['d']));
				}
				$this->bind($ret);
			}
		}

		public function list_backups()
		{
			return $this->mode == 'mysql' ? $this->mysql_list_backups() : $this->pgsql_list_backups();
		}

		public function get_dbs()
		{
			return $this->mode == 'mysql' ? $this->mysql_list_databases() : $this->pgsql_list_databases();
		}

		public function getMode()
		{
			return $this->mode;
		}

		public function get_email()
		{
			return $this->common_get_service_value('siteinfo', 'email');
		}

		public function backup_program($ext)
		{
			switch (strtolower($ext)) {
				case 'gz':
					return 'gzip';
				case 'zip':
					return 'zip';
				case 'bz':
					return 'bzip';
				default:
					return '';
			}
		}

	}
