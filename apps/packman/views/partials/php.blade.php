<p>
	PHP packages may be installed from
	<a href="https://packagist.org/" class="ui-action ui-action-label ui-action-visit-site">packagist.org</a>
	using Composer.
</p>
<p class="example">
	Example: to install <a href="https://packagist.org/packages/laravel/installer"
	                       class="ui-action ui-action-label ui-action-visit-site">Laravel</a>'s bootstrap utility,
</p>
<pre class="command-line" data-output="2-100" data-host="{{ SERVER_NAME_SHORT }}"
     data-user="{{ \Session::get('username', 'root') }}">
    <code class="language-bash">composer require laravel/installer</code>
</pre>