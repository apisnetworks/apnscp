<p>
	Ruby packages may be installed from
	<a href="https://rubygems.org/" class="ui-action ui-action-label ui-action-visit-site">rubygems.org</a>
	using gem.
	@if (version_compare(platform_version(), '6', '>='))
		It is strongly recommended to select a <a class="ui-action ui-action-kb ui-action-label"
		                                          href="<?=MISC_KB_BASE?>/ruby/changing-ruby-versions/">Ruby
			version</a> other than system default.
	@endif
</p>
<div class="mb-3">
	<p class="example">
		Example: to change Ruby versions
	</p>
	<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
		@if (version_compare(platform_version(), '7.5', '<'))
			<code class="language-bash">rvm use 2.4</code>
		@else
			<code class="language-bash">rbenv install 2.4.3
				rbenv local 2.4.3
				# Set 2.4.3 as the default system interpreter
				rbenv global 2.4.3
			</code>
		@endif
	</pre>
</div>

<div class="mb-3">
	<p class="example">
		Example: to install <a href="https://rubygems.org/gems/rails"
		                       class="ui-action ui-action-label ui-action-visit-site">rails</a>,
	</p>
	<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
	    <code class="language-bash">gem install rails</code>
	</pre>
</div>