<p>
	Perl packages may be installed using CPAN.
	Package listings are available via
	<a class="ui-action ui-action-label ui-action-visit-site" href="https://www.cpan.org/">CPAN.org</a>
</p>
<p class="example">
	Example: to install <a href="https://metacpan.org/pod/Time::Date"
	                       class="ui-action ui-action-label ui-action-visit-site">Time::Date</a>,
</p>
<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
    <code class="language-bash">perl -MCPAN -e 'install Time::Date'</code>
</pre>