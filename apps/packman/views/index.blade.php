<p class="alert alert-info">
	Package management is now done from the <a href="{{ \Template_Engine::app('terminal') }}">Terminal</a>.
</p>

<h3>Language Instructions</h3>
<div id="accordion" role="tablist" aria-multiselectable="true">
	<?php
	$langs = [
		'Node'   => [
			'icon' => 'node-w'
		],
		'Perl'   => [
			'icon' => 'perl',
			'data' => ''
		],
		'PHP'    => [
			'icon' => 'php-w',
		],
		'Python' => [
			'icon' => 'python'
		],
		'Ruby'   => [
			'icon' => 'ruby',
		],
	];
	if (version_compare(platform_version(), '7.5', '>=')) {
		$langs['Go'] = ['icon' => 'go'];
	}
	?>
	@foreach ($langs as $lang => $data)
		@php $lclang = strtolower($lang); @endphp
		<div class="card">
			<div class="card-header instructions" role="tab" id="{{$lang}}">
				<h5 class="mb-0">
					<a data-toggle="collapse" data-parent="#accordion" href="#{{$lang}}Instructions"
					   aria-expanded="@if ($loop->first) true @else false @endif"
					   aria-controls="{{$lang}}Instructions">
						<h4 class="mb-0">
							<i class="fa fa-<?=$data['icon']?> mr-2"></i><?=$lang?>
						</h4>
					</a>
				</h5>
			</div>

			<div id="<?=$lang?>Instructions" class="collapse @if ($loop->first) show @endif" role="tabpanel"
			     aria-labelledby="<?=$lang?>">
				<div class="card-block">
					@include("partials/$lclang")
				</div>
			</div>
		</div>
	@endforeach
</div>