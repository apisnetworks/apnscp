<h3>Vacation Auto-responder</h3>
<p class="note">
	Auto-responder affects the following addresses attached to <b>{{ UCard::get()->getUser() }}</b>:
	<br />
	{{ implode(", ", \cmd('email_user_mailboxes')) }}
</p>
<form method="POST">
	<input type="hidden" name="{{ $optDriver->getOption('contenttype')->getInputName() }}"
	       value="{{ $optDriver->getOption('contenttype')->getValue() }}"/>
	<div class="row">
		<div class="form-group ">
			<div class="col-12">
				<label class="custom-switch custom-control mb-0 pl-0 mr-0 mt-3 form-control-lg">
					<input type="hidden" name="vacation" value="0" />
					<input type="checkbox" class="master-select custom-control-input filter-control"
					       @if ($Page->vacation_exists()) checked @endif id="vacation-toggle" name="vacation"/>
					<span class="custom-control-indicator"></span>
					Enable vacation mode
				</label>
			</div>
		</div>

		<div class="col-12 form-group mt-1">
			<label for="vacation_msg" id="label-vacation_msg">Vacation Message</label>
			<textarea class="form-control" id="vacation_msg" rows="5"
			          @if (!$optDriver->enabled()) DISABLED @endif
			          name="{{ $optDriver->getOption('message')->getInputName() }}">{{ $optDriver->getOption('message')->getValue() }}</textarea>
			<label class="custom-switch custom-control mb-0 pl-0 mr-0 mt-3">
				<input type="hidden" name="{{ $optDriver->getOption('contenttype')->getInputName() }}" value="text"/>
				<input type="checkbox" id="modeSwitch" class="master-select custom-control-input filter-control"
				      name="{{ $optDriver->getOption('contenttype')->getInputName() }}"
				       @if ($optDriver->getOption('contenttype')->getValue() === 'html') CHECKED @endif
				       value="html"/>
				<span class="custom-control-indicator"></span>
				Respond in HTML
			</label>
		</div>
		@if (!empty($response))
			<hr/>
			<h4 class="col-12">Sample Response</h4>
			<blockquote class="col-12 blockquote mt-1 vacation-sample">
				@if ($optDriver->getOption('contenttype')->getValue() === 'html')
					{!! nl2br($response) !!}
				@else
					{!! nl2br(e($response)) !!}
				@endif
			</blockquote>
		@endif
	</div>
	<div class="btn-group mt-3">
		<button type="submit" value="Save Changes" id="saveForm" name="save" class="btn btn-primary">
			Save Changes
		</button>
		<button id="advancedToggle" type="button"
		        class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
		        data-toggle="collapse"
		        aria-controls="advancedOptions" data-target="#advancedOptions" aria-expanded="true"><span
					class="sr-only">Toggle Advanced</span></button>
	</div>

	<div id="advancedOptions" class="collapse">
		<hr/>
		<h4>Auto-responder Options</h4>
		<input type="hidden" name="charset" value="UTF-8"/>
		<input type="hidden" name="text" value="1"/>
		<div class="row">
			<div class="col-12 col-md-4">
				<label class="form-check">
					<input type="checkbox" name="{{ $optDriver->getOption('includemessage')->getInputName() }}"
					       value="1"
					       class="checkbox-inline" @if ($optDriver->getOption('includemessage')->getValue()) CHECKED
					       @endif id="includemessage"/>
					Include original message
				</label>
			</div>
			<div class="col-md-8 col-12">
				Include the original message before the vacation reply.
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-4">
				<label class="form-label d-block">
					<input type="hidden" name="{{ $optDriver->getOption('nodupe')->getInputName() }}" value="0" />
					<input type="checkbox" name="{{ $optDriver->getOption('nodupe')->getInputName() }}" value="1"
					       @if ($optDriver->getOption('nodupe')->getValue()) CHECKED @endif id="nodupe"/>
					Disallow duplicate vacation replies
				</label>
				<label class="form-label d-block">
					Permit duplicate replies after <em>N</em> days.
					<input type="number" class="form-control"
					       name="{{ $optDriver->getOption('dupedelay')->getInputName() }}" size="2"
					       value="{{ $optDriver->getOption('dupedelay')->getValue() }}"
					       @if ($optDriver->getOption('nodupe')->getValue()) DISABLED @endif id="dupedelay"/>
				</label>
			</div>
			<div class="col-12 col-md-8 hidden-sm-down">
				Allow vacation replies to be sent more than once to a recipient during a vacation period.
				Optionally limit duplicates after <em>N days</em> in vacation period.
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-4">
				<label class="form-label d-block">
					Quote salutation
					<input type="text" class="form-control" name="salutation" id="salutation"
					       value="{{ $optDriver->getOption('salutation')->getValue() }}"/>
				</label>
			</div>
			<div class="col-12 col-md-8">
				<h5>Variable reference</h5>
				<div class="row">
					<div class="col-12 col-md-6">
						<dt>%F</dt>
						<dd>Sender's name</dd>
					</div>
					<div class="col-12 col-md-6">
						<dt>%S</dt>
						<dd>Subject</dd>
					</div>
					<div class="col-12 col-md-6">
						<dt>%f</dt>
						<dd>Sender's address</dd>
					</div>
					<div class="col-12 col-md-6">
						<dt>%d</dt>
						<dd>Original message's date</dd>
					</div>
					<div class="col-12 col-md-6">
						<dt>%n</dt>
						<dd>Line break</dd>
					</div>
					<div class="col-12 col-md-6">
						<dt>%{&LT;SPEC&GT;}d</dt>
						<dd>Where &lt;SPEC&gt; is <a href="https://linux.die.net/man/3/strftime">POSIX.1-compliant</a>
							date format
						</dd>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
