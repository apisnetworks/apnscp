<div id="container-overview" class="">
	<h2 class="title overview">Overview</h2>
	<div class="row overview text-center">
		@php
			$templateClass = Template_Engine::init();
			$firstAccess = !$Page->lastLogin();
			$hidden = false;
			$showAll = !isset($_GET['collapse']) && (isset($_GET['expand']) || $firstAccess);
		@endphp
		@while( FALSE !== ($info = $templateClass->get_info()))
			@php
				$css = '';
				$value = $info->getValue();
				$id = $info->getId();
			@endphp
			@if (!$info->getVisibility())
				@if (!$hidden)
					@if ($showAll)
						<div class="col-12 overview-expand" id="overview-expand">
							<a class="ui-expandable ui-expanded" href="{{ \Template_Engine::app('dashboard') }}?collapse">show less</a>
						</div>
						<div class="col-12">
							<div class="row overview-hidden show">
								@else
									<div class="col-12 overview-expand" id="overview-expand">
										<a class="ui-expandable ui-collapsed" href="{{ \Template_Engine::app('dashboard') }}?expand">show
											more</a>
									</div>
									<div class="col-12 hide">
										<div class="row overview-hidden">
											@endif
											@endif
											@php
												$css = 'hide';
												$hidden = true;
											@endphp
											@endif

											<div class="col-4 mb-3">
												<h5 class="overview-param mb-0" @if ($id) id="{{ $id }}" @endif>
													@if ($help = $templateClass->get_help($info->getName()))
														<a href="/ajax?engine=tip&amp;tip={{ $help['token'] }}"
														   rel="{{ $help['token'] }}" class="ui-tooltip">
															{{ $info->getName() }}
														</a>
													@else
														{{ $info->getName() }}
													@endif
												</h5>
												<span class="overview-value">{!! $value !!}</span>
											</div>
											@endwhile
											@if ($hidden)
										</div>
									</div>
								@endif

								<div class="col-sm-2"></div>
								<div class="col-sm-2"></div>
							</div>
						</div>