<div class="mt-3 col-4 col-md-2" @if ($id) id="{{ $id }}" @endif>
	<h6 class="gauge-name">{{ $title }}</h6>
	<div class="gauge-display">
		<span class="gauge" @if (isset($data)) data-values="{{ implode(' ', $data) }} @endif">
			@if (!isset($data))
				{{ $label }}
			@else
				{{ sprintf($label, ...$data) }}
			@endif
		</span>
	</div>
	@includeIf("partials.gauge-data." . substr($id, 0, strrpos($id, '-')))
</div>