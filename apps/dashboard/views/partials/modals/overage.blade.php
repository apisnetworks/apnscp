<div id="overage" style="display: none;">
	<h2 class="col-12 text-center text-danger">
		Storage Limit Reached
	</h2>
	<p class='alert alert-danger'>
		<span class='badge badge-danger'>EMERGENCY</span>
		You have reached your storage capacity. You will not
		be able to receive mail nor will your web site, or any
		component therein, function until this is resolved. Storage
		<b>must</b> be resolved first.
	</p>
	<p class="ui-multi-role-show role-user">
		<i>Please contact the account administrator to increase your storage
			limit within the control panel.</i>
	</p>
	<div class="ui-multi-role-show role-site">
		<h4>View Usage</h4>
		<p>
			A birds eye view of all files counting towards storage usage
		</p>

		@if (\cmd('crm_configured'))
			<h4>Contact Support</h4>
			<p>
				Open a <a href='{{ Template_Engine::app('troubleticket') }}?new&amp;symbol=storage'>ticket</a> to upgrade your
				account
			</p>
		@endif
		<h4>Storage Amnesty</h4>
		<p>
			Storage amnesty will increase your storage
			by {{ round(Diskquota_Module::AMNESTY_MULTIPLIER * 100 / 2) }}%
			for {{ round(Diskquota_Module::AMNESTY_DURATION / 3600) }} hours.
			Your site will continue to operate while you resolve storage usage
		</p>

		<a href='{{ Template_Engine::app('diskbd') }}' class="btn btn-secondary m-r-3">
			View Usage
		</a>
		@if (\cmd('crm_configured'))
			<a href='{{ Template_Engine::app('troubleticket') }}?new&amp;symbol=storage' class='btn btn-secondary m-r-3'>
				Buy More Storage
			</a>
		@endif
		<a href="#" id="amnesty" class='btn btn-primary'>
			Storage Amnesty <i class="fa" id="amnesty-indicator"></i>
		</a>
	</div>
</div>