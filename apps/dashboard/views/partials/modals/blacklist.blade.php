@component('theme::partials.app.modal')
	@slot('title')
		Oh no! Your IP is blacklisted.
	@endslot
	@slot('id')
		blacklistModal
	@endslot
	<div class="row alert-danger overflow-hidden">
		<p class="text-danger py-3 mb-0 col-12">
			We've detected an excessive number of invalid logins from <b>{{ Auth::client_ip() }}</b>.
			To protect this network, your IP address has been automatically blocked from one or more services:
			<b class="services"></b>
		</p>
	</div>
	<hr/>
	<p>
		Carefully review that all machines are properly configured
		before unblocking this address.
	</p>
	<div class="reason">
		@if (!RAMPART_SHOW_REASON)
			<span class="no-reason">
				Contact support for a record of logs that triggered this automated block.
			</span>
		@else
			<label class="d-block mb-0">Last offending activity</label>
			<span class="log font-italic"> </span>
		@endif
	</div>
	@slot('buttons')
		<button type="submit" name="unban" class="btn btn-primary ajax-wait">
			Unblock {{ \Auth::client_ip() }}
		</button>
		<p class="ajax-response"></p>
	@endslot
@endcomponent