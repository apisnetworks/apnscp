<ul class="label-pair mt-0">
	<li class="label">
		<span class="ui-action ui-action-tooltip ui-action-label" data-toggle="tooltip"
			  title="{{ $Page->var('tooltips.ooms') }}">OOMs</span>
	</li>
	<li class="data">
		@if (\UCard::get()->getCgroups()['memory']['oom'] > 0)<b class="text-danger">@endif
			{{ \UCard::get()->getCgroups()['memory']['oom'] }}
		@if (\UCard::get()->getCgroups()['memory']['oom'] > 0)</b>@endif
	</li>
</ul>