@if (!empty(UCard::get()->getStorage()['dynamic']))
	<ul class="label-pair mt-0">
		<li class="label">
		<span class="ui-action ui-action-tooltip ui-action-label" data-toggle="tooltip"
			  title="{{ $Page->var('tooltips.dynamic_storage') }}">Dynamic</span>
		</li>
		<li class="data">✅</li>
	</ul>
@endif