
<div class="d-flex-row mt-3 col-lg-6 col-xl-4 col-12" id="rampart">
	<div class="align-content-around align-items-center 'd-flex">
		<h6 class="d-flex mb-2 ml-1 mr-3">Firewall</h6>
		<form method="POST" id="rampartIpForm" class="ml-auto d-flex" action="{{ \Template_Engine::app('dashboard') }}/rampart">
			<div class="d-flex ml-auto text-right input-group input-group-sm">
				<input autofocus id="ipAddress" data-mode="unban" placeholder="{{ _('IP Address') }}" class="form-control form-control-sm">
				<div class="btn-group">
					<button class="btn-primary btn btn-sm" id="rampartUnban" data-action="unban">Unban</button>
				</div>
			</div>
		</form>
	</div>

	<table class="table text-left table-hover table-sm mt-1">
		<thead>
			<tr>
				<th class="align-baseline">
					@if (!($full ?? false))
						<label class="custom-switch custom-control mb-0 pl-0 mr-0">
							<input type="checkbox" aria-expanded="false" data-target="#rampart .data-row.multi-collapse" id="allRampartEntries" class="master-select custom-control-input filter-control"/>
							<span class="custom-control-indicator mr-2"></span>
							All
						</label>

					@endif
				</th>
				<th class="align-baseline">Active</th>
				<th class="align-baseline">24 Hrs</th>
				<th class="align-baseline">Change</th>
				<th class="text-right">
					<button type="button" id="refreshRampart" class="pr-0 py-0 btn btn-sm btn-border-thin btn-link btn-secondary ui-action-refresh ui-action-label"
					>Refresh</button>
				</th>
			</tr>
		</thead>
		<tbody>
			@php
				$historic = \cmd('rampart_bans_since', time()-86400*2, time()-86400);
				$yesterday = \cmd('rampart_bans_since', time()-86400, time());
			@endphp
			@foreach(\cmd('rampart_get_jails') as $jail)
				{{-- Sent over the wire without "f2b-" prefix when tallied up --}}
				@php
					if (!isset($entries[$jail])) {
						$count = \count(array_get($entries, Rampart_Module::FAIL2BAN_IPT_PREFIX . $jail, []));
					} else {
						$count = (int)$entries[$jail];
					}
					$delta = ($yesterday[$jail] ?? 0) - ($historic[$jail] ?? 0);
				@endphp

				<tr class="data-row collapse @if (!$full && !$count) multi-collapse @else show @endif" data-jail="{{ $jail }}">
					<th class="align-middle">{{ $jail }}</th>
					<td class="align-middle active">
						{{ $count }}
					</td>
					<td class="align-middle 24-hour">
						{{ array_get($yesterday, $jail, 0) }}
					</td>
					<td class="change align-middle @if ($delta > 0) text-danger @else text-success @endif">
						{{ ($delta > 0 ? '▲' : ($delta !== 0 ? '▼' : '')) }} {{ abs($delta) }}
					</td>
					<td class="pr-0 align-middle">
						<form method="POST" class="flush-form text-right" action="{{ \Template_Engine::app('dashboard') }}/rampart/flush">
							<button type="submit" name="jail" value="{{ $jail }}" class="flush-action btn btn-sm btn-secondary btn-border-thin warn ui-action-delete">Flush</button>
							<input type="hidden" name="jail" value="{{ $jail }}" />
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>