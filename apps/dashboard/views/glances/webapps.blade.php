<div class="">
	<h2 class="title mt-0 webapps d-flex align-items-center">
		Web Apps
		<a href="{{ \Template_Engine::app('webapps') }}" class="ui-action ui-action-label ui-action-switch-app btn btn-outline-primary ml-auto">
			Manage
		</a>
	</h2>
	@php
		// we need this to expose map info
		$kernel = Page_Container::kernelFromApp('webapps');
		$wapage = Page_Container::init($kernel);
		$proxy = \Module\Support\Webapps\App\UIPanel::instantiateContexted(\Auth::profile());
		$i = 0;
	@endphp
	<div class="row ui-webapp-panel small">
		@foreach($wapage->mergeHostsApps() as $docroot => $meta)
			@break ($i++ === 4)
			@php
					$pane = $proxy->get($meta['hostname'], $meta['path'] ?? null)
			@endphp
			<div class="col-12 col-xl-3 col-lg-4 col-md-6 @if ($i > 3) d-none d-xl-block @elseif ($i > 2) d-none d-lg-block @endif">
				<div class="row">
					@include('master::partials.shared.wa-screenshot', [
						'pane'    => $pane,
						'Page'    => $wapage,
						'href'    => \HTML_Kit::new_page_url_params('webapps', [
							'hostname' => $pane->getHostname(),
							'path'     => $pane->getPath()
						]),
						'actions' => [
							'view' => 'master::partials.shared.wa-actions',
							'dropdownLocation' => 'dropdown-menu-right',
							'btnGroupClass' => 'btn-group-sm'
						]
					])

				</div>
			</div>
		@endforeach
	</div>
</div>