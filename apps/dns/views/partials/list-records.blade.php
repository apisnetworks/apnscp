@include('partials.search-records')
<table width="100%" id="dns_records" class="tablesorter table table-sm">
	<thead>
	<tr>
		<th class="head1 header text-center nosort">
			<input type="checkbox" id="select-all"/>
		</th>
		<th class="head1 dns_hostname">
			Subdomain
		</th>
		<th class="head1 dns_rr center">
			RR
		</th>
		<th class="head1 dns_class hidden-lg-down">
			Class
		</th>
		<th class="head1 dns_ttl hidden-xs-down">
			TTL
		</th>
		<th class="head1">
			Parameter
		</th>
		<th class="head1 center actions">
			Actions
		</th>

	</tr>
	</thead>
	<tbody>
	@php
		$zoneData = $Page->getZoneData();
		$trimMask = strlen($Page->getDomain()) + 2;
		$idx = -1;
	@endphp
	@foreach($zoneData as $recordType => $recordClass)
		@if ($recordType == 'SOA' && (!\UCard::is('admin') || !in_array($Page->dns_get_provider(), $Page::FULL_EDITING, true)))
			@continue
		@endif
		@for ($j = 0, $k = array_keys($recordClass), $n = sizeof($k); $j < $n; $j++)
			@php
				$key = $k[$j];
				$recordClass[$key]['name'] = substr_replace($recordClass[$key]['name'], '', -$trimMask);
			@endphp
		@endfor
		@php
			uasort($recordClass, array($Page, 'sort_dns'));
		@endphp
		@foreach($recordClass as $recordData)
			@if ($recordType == 'NS' && $recordData['name'] === '' && (!\Preferences::get($Page::SHOW_APEX_NS_PREFERENCE) || !$Page->dns_get_default('apex-ns')))
				@continue
			@endif
			@php
				$idx++;
				$deltoken = $Page->record_encode($recordData['name'], $recordType, $recordData['parameter']);
			@endphp
			<tr class="entry" rel="{{ $idx }}">
				<td class="">
					<label class="custom-checkbox custom-control my-1 p-0 align-items-center d-flex">
						<input type="checkbox" name="delete[]" class="custom-control-input" value="{{ $deltoken }}"/>
						<span class="custom-control-indicator"></span>
					</label>
				</td>
				<td class="monospace dns_hostname name" id="name{{ $idx }}" align="left">
						<span class="hostname @if (strlen($recordData['name']) > $Page::HOSTNAME_TRUNCATE_LEN) hostname_full @endif">{{ $recordData['name']}}</span>
					@if (strlen($recordData['name']) > $Page::HOSTNAME_TRUNCATE_LEN)
						<span class="hostname_truncated">
		                        {{
		                            substr_replace($recordData['name'],
										' ... ',
										$Page::HOSTNAME_TRUNCATE_LEN / 2 - 2,
										strlen($recordData['name']) - ($Page::HOSTNAME_TRUNCATE_LEN / 2 - 2) * 2)
								}}
		                    </span>
					@endif
				</td>
				<td class="monospace dns_rr rr-{{ strtolower($recordType) }}" id="rr{{ $idx }}"
				    align="center">{{ $recordType }}</td>
				<td class="monospace dns_class hidden-lg-down" id="class{{ $idx }}" align="center"
				    class="dns_class">{{ $recordData['class'] }}</td>
				<td class="monospace dns_ttl hidden-xs-down" id="ttl{{ $idx }}" align="center"
				    class="dns_ttl">{{ $recordData['ttl'] }}</td>
				<td class="monospace dns_parameter" id="parameter{{ $idx }}" align="left">
		                <span
			                class="parameter @if (strlen($recordData['parameter']) > $Page::PARAM_TRUNCATE_LEN) parameter_full @endif">{{ $recordData['parameter'] }}</span>
					@if (strlen($recordData['parameter']) > $Page::PARAM_TRUNCATE_LEN)
						<span class="parameter_truncated">
		                        {{
		                            substr_replace($recordData['parameter'], ' ... ',
										$Page::PARAM_TRUNCATE_LEN / 2 - 2,
										strlen($recordData['parameter']) - ($Page::PARAM_TRUNCATE_LEN / 2 - 2) * 2)
								}}
		                    </span>
					@endif
				</td>
				<td class="actions" align="center">
					<div class="input-group justify-content-center">
						<div class="btn-group">
							<a href="#" class="hidden-sm-down btn btn-secondary ui-action ui-action-edit ui-action-label"
							   rel="{{ $idx }}">
								Edit
							</a>
							<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
							        aria-haspopup="true" aria-expanded="false">
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu  dropdown-menu-right">
								<a href="#"
								   class="hidden-md-up mb-1 dropdown-item btn-block ui-action ui-action-edit ui-action-label"
								   rel="{{ $idx }}">
									Edit
								</a>
								<a href="#" class="dropdown-item ui-action ui-action-delete ui-action-label btn-block warn btn"
								   rel="{{ $deltoken }}">
									Delete
								</a>
							</div>

						</div>
					</div>
				</td>
			</tr>
		@endforeach
	@endforeach
	</tbody>
	<tfoot>
	<tr>
		<td colspan="7" class="center">
			<hr/>
			<button type="submit" name="delete_sel"
			        class="btn btn-secondary ui-action-label ui-action ui-action-delete warn" value="Delete">
				Delete all checked entries
			</button>
		</td>
	</tr>
	</tfoot>
</table>