<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\dns;

	use Opcenter\Mail\Providers\Gmail\Module as GMail;

	class Page extends \Page_Container
	{
		const HOSTNAME_TRUNCATE_LEN = 44;
		const PARAM_TRUNCATE_LEN = 44;
		const SHOW_APEX_NS_PREFERENCE = 'dns.show-apex-ns';
		// DNS providers that allow complete editing of records (remotely + SOA)
		const FULL_EDITING = ['powerdns'];
		public $authorative = false;
		public $domain_ns;
		public $primary_ns;
		protected $hosting_ns;
		private $zoneCache = [];
		private $domain_state;
		private $mode = 'add';


		public function __construct()
		{
			$this->domain_state = $_POST['render-details'] ?? $_GET['domain'] ?? $_POST['domain'] ?? \Session::get('domain');

			if ($this->bypassAuthorization()) {
				$this->getApnscpFunctionInterceptor()->swap('dns',
					Powerbroker::instantiateContexted($this->getAuthContext()));
			}
			parent::__construct();

			if (is_debug()) {
				// no cache in debug
				\Session::set('cache.DNS', []);
			}

			$this->add_css('dns.css');
			if (isset($_GET['mode']) && $_GET['mode'] == 'edit') {
				$this->mode = 'edit';
			}
			$this->add_javascript('dns.js');
			$this->init_js('sorter');
			if (\UCard::get()->is('admin')) {
				$this->getAvailableDomains();
			}

			if ($this->domain_state) {
				$this->bindByDomainState($this->domain_state);
			} else if (\UCard::get()->is('admin')) {
				return;
			}

			// PowerDNS allows aggregation
			if ((!in_array($this->dns_get_provider(), self::FULL_EDITING, true) && !$this->dns_enabled()) || $this->isAjax()) {
				return;
			}

			try {
				$nameservers = $this->getHostingNameservers();
			} catch (\Exception $e) {
				error("DNS misconfigured for domain: %s", $e->getMessage());
				return;
			}

			$this->primary_ns = $nameservers[0] ?? null;
			$this->_populateZoneCache($this->domain_state);
			$this->authorative = null !== $this->zoneCache && $this->dns_verified($this->domain_state);
			$ns = $this->getAuthoritativeNameservers($this->domain_state);
			$this->domain_ns = $ns[0] ?? null;
			$this->add_javascript(
				"var ns = '" . json_encode($ns) . "'; " .
				"var dns_domain = '" . $this->domain_state . "';" .
				"var DNS_RRS = ['".implode("','", $this->getSupportedRRs()) . "'];",
				'internal',
				false,
				true
			);
		}

		private function bypassAuthorization(): bool
		{
			return \UCard::get()->is('admin') && in_array($this->dns_get_provider(), self::FULL_EDITING, true) &&
				$this->domain_state;
		}

		public function bypassed(): bool
		{
			static $bypassed;
			return $bypassed ?? $this->bypassAuthorization();
		}

		protected function bindByDomainState(string $domain): void
		{
			if ($this->bypassAuthorization() || !\UCard::get()->is('admin')) {
				return;
			}
			// @TODO cross-server support
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
			try {
				// @TODO move to ImpersonableTrait so session_valid() marker is automatically set
				// it's fixed in the constructor, which rebinds UI to Site Administrator
				$this->setContext(\Auth::context(null, $domain));
			} catch (\apnscpException $e) {
				\error($e->getMessage());

				return;
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}
			if (!$this->isAjax()) {
				$this->add_javascript('var contextId = "' . $this->getAuthContext()->id . '";', 'internal', false,
					true);

			}
		}

		public function getHostingNameservers(string $domain = null)
		{
			if ($domain) {
				return $this->dns_get_hosting_nameservers($domain);
			}
			if (null === $this->hosting_ns) {
				$this->hosting_ns = $this->dns_get_hosting_nameservers($this->domain_state);
			}

			return $this->hosting_ns;
		}

		private function _populateZoneCache($domain)
		{
			$dns = $this->dns_get_zone_information($this->domain_state);
			if ($dns) {
				ksort($dns);
			}
			$this->zoneCache = $dns;
		}

		public function getAuthoritativeNameservers($domain)
		{
			// nameservers per WHOIS
			$nameservers = $this->_getAuthNSFromCache($domain);
			// internal nameservers provided by hosting
			natsort($nameservers);
			$delegated = $this->dns_domain_uses_nameservers($domain);
			$nag = 'nag-' . $domain;
			if (isset($_SESSION[$nag])) {
				return $nameservers;
			}

			if (\count($nameservers) < 1) {
				warn("Domain is not registered.");
			} else if (!$delegated) {
				// domain isn't delegated to proper nameservers
				warn("Nameserver for domain assigned " .
					join(", ", $nameservers) . ". DNS below does not reflect actual DNS of domain.");
			} else {
				$this->checkGlueRecords($domain);
			}

			$_SESSION[$nag] = 1;

			return $nameservers;
		}

		/**
		 * Get authorative NS from cache, populate if necessary
		 *
		 * @param string $domain
		 * @return array nameservers
		 */
		private function _getAuthNSFromCache($domain)
		{
			$nameservers = $this->getHostingNameservers();
			$cache = \Session::get('cache.DNS', []);

			if (isset($cache[$domain])) {
				$entry = $cache[$domain];
				if ($_SERVER['REQUEST_TIME'] - $entry['ttl'] < 60 ||
					in_array($nameservers[0], $entry['nameservers'])) {
					return $entry['nameservers'];
				}

			}

			$nameservers = [];
			$dns = silence(function () use ($domain) {
				return dns_get_record($domain, DNS_NS);
			});

			if ($dns) {
				for ($i = 0, $n = sizeof($dns); $i < $n; $i++) {
					if (isset($dns[$i]['target'])) {
						$nameservers[] = $dns[$i]['target'];
					}
				}
			}

			$cache[$domain] = array(
				'ttl'         => $_SERVER['REQUEST_TIME'],
				'nameservers' => $nameservers
			);
			\Session::set('cache.DNS', $cache);

			return $nameservers;
		}

		protected function checkGlueRecords(string $domain): bool
		{
			if (!$this->dns_get_default('apex-ns')) {
				// NS apex are managed by provider, ignore glue
				return true;
			}
			// domain is fully delegated, make sure glue is present
			// if not, add the nameserver glue
			$tmp = $this->dns_get_records_by_rr('ns', $domain);
			if ($tmp === null) {
				return false;
			}
			$nsrec = array();
			while (false !== ($rec = current($tmp))) {
				// pull parameter entry
				$nsrec[] = rtrim($rec['parameter'], '.');
				next($tmp);
			}
			$missing = array_diff($this->getHostingNameservers(), $nsrec);
			// something odd, all nameservers missing yet reporting
			// as delegated - whitelabel NS
			if (count($missing) !== count($nsrec)) {
				foreach ($missing as $ns) {
					warn("Domain missing glue nameserver `%s'. " .
						"Automatically added nameserver record!", $ns);
					$this->dns_add_record($domain, '', 'NS', $ns);
				}
			}

			return true;
		}

		public static function sort_dns($a, $b)
		{
			$name1 = $a['name'];
			$name2 = $b['name'];
			if ($name1 == $name2) {
				return 0;
			} else if (strstr($name1, '.') && !strstr($name2, '.')) {
				return 1;
			} else if (!strstr($name1, '.') && strstr($name2, '.')) {
				return -1;
			} else if (($cnt_a = sizeof(explode('.', $name1))) != ($cnt_b = sizeof(explode('.', $name2)))) {
				if ($cnt_a > $cnt_b) {
					return 1;
				} else {
					return -1;
				}
			} else {
				return strnatcmp($name1, $name2);
			}
		}

		public static function record_encode($subdomain, $rr, $param)
		{
			return base64_encode($subdomain . " " . $rr . " " . $param);
		}

		public function _render()
		{
			if (!sizeof($_POST) && ($this->getStatus() === \Error_Reporter::E_OK) && !isset($_GET['toolbox'])) {
				$this->hide_pb();
			}
		}

		public function getMode()
		{
			return $this->mode;
		}

		public function getSupportedRRs(): array
		{
			static $records;
			if (null === $records) {
				$records = $this->dns_permitted_records();
				asort($records);
			}

			return $records;

		}

		public function on_postback($params)
		{
			if (isset($params['render-details'])) {
				$this->domain_state = $params['render-details'];
				$this->bindByDomainState($this->domain_state);
				$instance = $this->loadBlade();
				echo (string)$instance->render('partials.domain-info.rendered', ['domain' => $this->domain_state, 'Page' => $this]);
				exit(0);
			}

			if (isset($params['verify'])) {
				$ret = $this->dns_verify($params['verify']);
				if (!$ret) {
					$stack = \Error_Reporter::get_buffer(\Error_Reporter::E_FATAL|\Error_Reporter::E_ERROR);
					if ($stack) {
						\Error_Reporter::set_buffer($stack);
					}
				}
				return $ret;
			}

			if (!$this->authorative && 'restore' !== array_get($params, 'toolbox')) {
				return false;
			}

			if (isset($params['src-domain']) && isset($params['target-domain'])) {
				return $this->dns_import_from_domain($params['target-domain'], $params['src-domain']) && $this->_populateZoneCache($params['target-domain']);;
			}
			if (isset($params['do_filter'])) {
				$this->zoneCache = $this->_filter_dns($this->zoneCache, $params['filter_spec'], $params['filter']);
			}
			if (isset($params['toolbox'])) {
				return $this->_processToolboxCmd($params['domain'], $params['toolbox']);
			}
			if (isset($params['d'])) {
				$opts = explode(' ', base64_decode($params['d']));
				$this->deleteZone($opts[0], $opts[1], join(' ', array_slice($opts, 2)));
			} else if (isset($params['Add_Zone'])) {
				if (!$params['add_parameter']) {
					return error("missing parameter");
				}
				$this->addZone($params['add_name'], $params['add_rr'], $params['add_ttl'], $params['add_parameter']);
			} else if (isset($params['original_state'])) {
				foreach (array_keys($params['original_state']) as $idx) {
					$args = explode(' ', $params['original_state'][$idx]);
					if (!$params['parameter'][$idx]) {
						return error("missing parameter for record " . $args[0] . " " . $args[1]);
					}
					$this->modifyZone($args[0],
						$args[1],
						join(' ', array_slice($args, 2)),
						array(
							'name'      => $params['name'][$idx],
							'rr'        => $params['rr'][$idx],
							'ttl'       => $params['ttl'][$idx],
							'parameter' => $params['parameter'][$idx]
						));
				}

				return $this->errors_exist();
			} else if (isset($params['delete_sel'])) {
				if (!isset($params['delete']) || !is_array($params['delete'])) {
					return error("no DNS records selected to delete");
				}
				foreach ($params['delete'] as $record) {
					$record = explode(" ", base64_decode($record));
					$name = $record[0];
					$rr = $record[1];
					$params = array_slice($record, 2);
					$this->deleteZone($name, $rr, join(' ', $params));
				}
			}
		}

		private function _filter_dns($records, $spec, $regex)
		{
			if (false === preg_match('/' . str_replace('/', '\/', $regex) . '/', '.')) {
				error("invalid regular expression");

				return $records;
			}

			if ($spec == 'hostname') {
				$spec = 'name';
			} else if ($spec == 'rr') {
				foreach (array_keys($records) as $rr) {
					if (!preg_match('/' . trim($regex, '/') . '/', $rr)) {
						unset($records[$rr]);
					}
				}

				return $records;
			}

			$filtered = array();
			foreach ($records as $rr => $recs) {
				$filtered[$rr] = array();
				foreach ($recs as $key => $rec) {
					if (preg_match('/' . trim($regex, '/') . '/', $rec[$spec])) {
						$filtered[$rr][] = $rec;
					}
				}
			}

			return $filtered;
		}

		private function _processToolboxCmd($domain, $cmd)
		{
			switch ($cmd) {
				// restore DNS to original settings
				case 'restore':
					return $this->dns_reset($domain);
				// set MX records to Gmail/Apis
				case 'mxgmail':
				case 'mxapnscp':
					return $this->_cmdAssignMXRecords($domain, $cmd);
				case 'mxclone':
					return $this->_cmdCloneRecords($domain, 'MX');
				case 'export':
					return $this->_exportZone($domain);
			}
		}

		private function _purgeByRR($domain, $RR)
		{
			if (strtoupper($RR) == "SOA") {
				return;
			}
			foreach ($this->dns_get_records_by_rr($RR, $domain) as $rec) {
				$this->deleteZone(trim($rec['name'], '.') . '.', $RR, $rec['parameter']);
			}
		}

		public function deleteZone($mName, $mRR, $mParam)
		{
			$mName = rtrim($mName, '.');
			if (substr($mName, -strlen($this->domain_state)) == $this->domain_state) {
				$mName = (string)substr($mName, 0, -strlen($this->domain_state) - 1);
			}
			$status = $this->dns_remove_record($this->domain_state, $mName, $mRR, $mParam);
			if ($status) {
				$this->_cacheRemove($mName . '.' . $this->domain_state, $mRR, $mParam);
			}

			return $status;
		}

		private function _cacheRemove($mName, $mRR, $mParameter, $mTTL = null)
		{
			$mName = $this->_canonicalize($mName);
			$mParameter = trim($mParameter);
			$delim = $mParameter[0];
			if (($delim == '"' || $delim == "'") && substr($mParameter, -1, 1) == $delim &&
				strpos($mParameter, $delim) == strrpos($mParameter, $delim, -2)) {
				$mParameter = trim($mParameter, $delim);
			}
			if (!isset($this->zoneCache[$mRR])) {
				report("%s %s",
					var_export($this->zoneCache, true),
					var_export(func_get_args(), true)
				);

				return false;

			}
			for ($i = 0,
				 $k = array_keys($this->zoneCache[$mRR]),
				 $n = sizeof($this->zoneCache[$mRR]); $i < $n; $i++) {
				$j = $k[$i];
				if ($this->zoneCache[$mRR][$j]['name'] == $mName) {
					if ($this->zoneCache[$mRR][$j]['parameter'] == $mParameter) {
						unset($this->zoneCache[$mRR][$j]);

						//echo "Removing $mName - $mRR - $mParameter<br />";
						return true;
					}
				}
			}

			return false;
		}

		private function _canonicalize($record)
		{
			$domain = $this->getDomain();
			$domainLen = strlen($domain);
			$record = trim($record, '.');
			if (!isset($record[0])) {
				$record = $domain;
			} else if (substr($record, -$domainLen) != $domain) {
				$record .= '.' . $domain;
			}

			return ltrim($record . '.', '.');
		}

		public function getDomain()
		{
			return $this->domain_state;
		}

		private function _batchAddRecords($domain, $recs)
		{
			foreach ($recs as $r) {
				$host = ltrim($r['name'] . ".{$domain}", '.');
				$this->addZone($host, $r['rr'], $r['ttl'], $r['parameter']);
			}
		}

		public function addZone($mName, $mRR, $mTTL, $mParameter)
		{
			$mName = rtrim($mName, '.');
			if (substr($mName, -strlen($this->domain_state)) == $this->domain_state) {
				$mName = substr($mName, 0, -strlen($this->domain_state));
			}
			$status = $this->dns_add_record($this->domain_state, $mName, $mRR, $mParameter, $mTTL);
			if ($status) {
				$this->_cacheAdd(ltrim(rtrim($mName, '.') . '.' . $this->domain_state, '.'), $mRR, $mParameter, $mTTL);
			}

			return $status;
		}

		private function _cacheAdd($mName, $mRR, $mParameter, $mTTL)
		{
			$mName = $this->_canonicalize($mName);
			$mParameter = trim($mParameter);
			$delim = $mParameter[0];
			if (($delim == '"' || $delim == "'") && substr($mParameter, -1, 1) == $delim &&
				strpos($mParameter, $delim) == strrpos($mParameter, $delim, -2)) {
				$mParameter = trim($mParameter, $delim);
			}
			//if (substr($mName, 0-strlen($this->domain_state)) == $mName)
			//	$mName = substr($mName,0,-strlen($this->domain_state));
			//print "Adding $mName - $mRR - $mParameter<br />";
			$this->zoneCache[$mRR][] = array(
				'name'      => $mName,
				'parameter' => $mParameter,
				'class'     => 'IN',
				'ttl'       => $mTTL
			);
		}

		private function _cmdAssignMXRecords($domain, $service)
		{
			// email_remove_virtual_transport() implicitly purges
			// the mx record, remove it first to ensure it is removed from
			// the cache
			$this->_purgeByRR($domain, 'MX');
			// GMail
			if ($service == 'mxgmail') {
				$recs = (GMail::instantiateContexted($this->getAuthContext()))->get_records($domain);
				if ($this->email_enabled() && $this->email_transport_exists($domain)) {
					$this->email_remove_virtual_transport($domain);
				}
				foreach (['A', 'AAAA'] as $rr) {
					if ($this->dns_record_exists($domain, 'mail', $rr)) {
						$this->dns_remove_record($domain, 'mail', $rr);
					}
				}
			} // Apis
			else {
				// NB: order matters, A record must be populated first before adding an MX
				// record dependent upon the value of the A record
				$recs = $this->email_provisioning_records($domain);

				if ($this->dns_record_exists($domain, 'mail', 'CNAME')) {
					$this->dns_remove_record($domain, 'mail', 'CNAME');
				}
				if ($this->email_enabled() && !$this->email_transport_exists($domain)) {
					$this->email_add_virtual_transport($domain);
				}
			}
			$this->_batchAddRecords($domain, $recs);

			$this->_populateZoneCache($domain);
		}

		private function _cmdCloneRecords($domain, $rr)
		{
			$theirs = dns_get_record($domain, \Dns_Module::record2const($rr));
			$mine = $this->dns_get_records_by_rr($rr, $domain);
			$ntheirs = sizeof($theirs);
			$nmine = sizeof($mine);
			$src = array($rr => array());
			$srcttl = array($rr => array());
			for ($i = 0; $i < $ntheirs; $i++) {
				$record = $theirs[$i];
				$host = $record['host'];

				if (!isset($src[$rr][$host])) {
					$src[$rr][$host] = array();
					$srcttl[$rr][$host] = array();
				}

				$pri = $record['pri'];
				$target = $record['target'];
				$ttl = $record['ttl'];
				$param = '';
				// http://us.php.net/manual/en/function.dns-get-record.php
				switch ($record['type']) {
					case 'A':
						$param = $record['ip'];
						break;
					case 'MX':
						$param = $record['pri'] . " " . $record['target'];
						break;
					case 'CNAME':
					case 'NS':
					case 'PTR':
						$param = $record['target'];
						break;
					case 'TXT':
						$param = $record['txt'];
						break;
					case 'SRV':
						$param = $record['pri'] . " " . $record['target'] . " " . $record['port'];
						break;
					case 'AAAA':
						$param = $record['ipv6'];
						break;
					case 'HINFO':
						$param = $record['cpu'] . " " . $record['os'];
						break;
					default:
						warn("Skipping unknown rr `" . $record['type'] . "' on `" . $record['host'] . "'");
				}

				$src[$rr][$host][] = $param;
				$srcttl[$rr][$host][] = $ttl;
			}

			for ($i = 0; $i < $nmine; $i++) {
				$param = $mine[$i]['parameter'];
				$host = rtrim($mine[$i]['name'], ".");
				$class = $mine[$i]['class'];
				if (!isset($src[$rr]) || !isset($src[$rr][$host]) ||
					!in_array(rtrim($param, '.'), $src[$rr][$host])) {
					info("Removed record $rr `" . $host . "' - $param");
					$this->deleteZone($host, $rr, $param);
					//$this->dns_remove_record($domain, $host, $rr, $param);
				} else {
					$pos = array_search($param, $src[$rr][$host]);
					unset ($src[$rr][$host][$pos]);
					unset ($srcttl[$rr][$host][$pos]);
				}

			}

			// Add missing records
			foreach ($src as $rr => $colrec) {
				foreach ($colrec as $host => $colhost) {
					for ($i = 0, $k = array_keys($colhost), $n = sizeof($k); $i < $n; $i++) {
						$param = $colhost[$k[$i]];
						// this shouldn't happen
						//if (!isset($srcttl[$rr][$host][$k[$i]]))
						//	$ttl = 86400;
						//else
						$ttl = $srcttl[$rr][$host][$k[$i]];
						info("Added $rr `$host' - $param");
						$this->addZone($host, $rr, $ttl, $param);
						//$this->dns_add_record($domain, $host, $rr, $param, $ttl);
					}
				}
			}
		}

		private function _exportZone($zone)
		{
			$data = $this->dns_export($zone);
			header('Content-disposition: attachment; filename="' . $zone . '-zone.txt"');
			header("Content-Type: text/plain");
			header("Content-Transfer-Encoding: binary");
			header("Pragma: no-cache");
			header("Expires: 0");
			print $data;
			die();
		}

		public function modifyZone($mName, $mRR, $mParameter, array $mNewData)
		{
			$status = $this->dns_modify_record($this->domain_state, $mName, $mRR, $mParameter, $mNewData);
			if ($status) {
				$this->_cacheRemove($mName, $mRR, $mParameter);
				$TTL = $this->dns_get_default('ttl');
				if (isset($mNewData['name'])) {
					$mName = $mNewData['name'];
				}
				if (isset($mNewData['ttl'])) {
					$TTL = $mNewData['ttl'];
				}
				if (isset($mNewData['rr'])) {
					$mRR = $mNewData['rr'];
				}
				if (isset($mNewData['parameter'])) {
					$mParameter = $mNewData['parameter'];
				}
				$this->_cacheAdd($mName, $mRR, $mParameter, $TTL);
			}

			return $status;
		}

		public function getAvailableDomains(): ?array
		{
			static $domains;
			if (isset($domains)) {
				return $domains;
			}
			$domains = $this->dns_zones();

			if (!$domains && \Error_Reporter::is_error()) {
				return null;
			}

			asort($domains);
			if (\UCard::init($this->getAuthContext())->is('admin')) {
				if (!$this->getDomain()) {
					array_unshift($domains, '');
				}

				return $domains;
			}

			$loginDomain = \Util_Conf::login_domain() ?: $this->common_get_service_value('siteinfo', 'domain');
			array_unshift($domains, $loginDomain);
			return $domains = array_unique($domains);
		}

		public function getZoneData()
		{
			return $this->zoneCache;

		}

		public function getSOAInformation()
		{
			return array_pop($this->zoneCache['SOA']);
		}
	}