@extends('theme::layout')
@section('content')
	<div class="row align-content-around">
		<div class="form-inline col-12 col-sm-6 mb-3 ui-widget">
			<label class="b d-block mr-1">Select Website</label>
			<select name="domain" id="domain-picker" class="custom-select" placeholder="{{ _('Select domain') }}" tabindex="1"
			        @if (!$Page->getDomain()) autofocus @endif>
				@foreach ($Page->getAvailableDomains() as $d)
					<option @if ($Page->getDomain() === $d) SELECTED
					        @endif value="{{ $d }}">{{ $d }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-12 col-sm-6 mb-3">
			<form method="post" action="{{ \Template_Engine::app('pagespeed') }}/runTest/{{ $Page->getDomain() }}" id="insightForm">
				<div class="btn-group  d-sm-flex">

					<button type="submit" class="ml-sm-auto d-flex btn btn-primary ajax-wait" id="insight">
						{{ _("Run Insight Test") }}
					</button>
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
					        aria-haspopup="true" aria-expanded="false">
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<div class="dropdown-menu  dropdown-menu-right">
						<a href="{{ $Page->getPreferredDomainUri($Page->getDomain()) }}" class="dropdown-item ui-action ui-action-visit-site ui-action-label btn-block btn">
							Visit Site
						</a>
						<button name="purge" value="{{ $Page->getDomain() }}" id="purge" type="button"
						   class="dropdown-item ui-action-label btn-block btn">
							<i class="fa fa-trash-o"></i>
							Flush Cache
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	@includeWhen($malformedHtaccess, 'master::partials.shared.htaccess-malformed', ['file' => \cmd('web_get_docroot', $domain) . '/.htaccess'])
	@includeWhen(!$malformedHtaccess, 'pagespeed-test', ['filters' => $Page->getActiveFilters()])
	@includeWhen(!$malformedHtaccess, 'pagespeed-options', ['filters' => $Page->getAvailableFilters()])
@endsection