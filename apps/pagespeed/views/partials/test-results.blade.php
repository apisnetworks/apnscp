{{--
	@var $details Page\models\InsightDetails
--}}
<div class="mt-3">
	<form method="POST" action="{{ \Template_Engine::app('pagespeed') }}/reset/{{ $Page->getDomain() }}" class="row d-flex">
		<div class="col-12 col-lg-6 d-flex mb-3 align-content-center align-items-center">
			<div class="align-items-center flex-row mr-3">
				<h6 class="text-center">Speed Index</h6>
				<figure class="display-4 mb-0 d-flex align-self-center {{ $Page->formatSpeedIndex($details->getSpeedIndex()) }}">
					{{ $details->getSpeedIndex() }}
				</figure>
			</div>
			<figure class="mb-0 mr-3">
				<img src="{{ $test->getScreenshot() }}" class="img img-fluid img-thumbnail p-0" width="128"/>
			</figure>
			<div class="flex-column">
				<h5 class="d-flex">{{ $domain }}</h5>
				<h6 class="d-flex">{{ (new \DateTime($test->getStart()))->format(\DATE_RFC2822) }}</h6>
				<button type="submit" name="filter" class="ui-action ui-action-label ui-action-restore  btn btn-secondary mr-3" value="{{ implode(' ', $Page->getActiveFilters()) }}"
					>Restore Filters
				</button>
				<button type="button" name="" data-toggle="collapse" data-target="#{{ $test->getHash() }}" aria-expanded="false" aria-controls="{{ $test->getHash() }}" class="btn btn-secondary ui-action-details ui-action ui-action-label"
					>Show Filters
				</button>
			</div>
		</div>
		<div class="col-12 col-lg-6 d-block d-md-flex">
			<div class="d-flex align-content-around table-responsive">
				<table class="w-100 table">
					<thead>
						<th>Size</th>
						<th>RTT</th>
						<th>TTFB</th>
						<th>Interactive</th>
						<th>First paint</th>
						<th>Request count</th>
					</thead>
					<tbody>
						<td>
							{{ sprintf("%.2f KB", Formatter::changeBytes($details->getSize(), 'KB', 'B')) }}
						</td>
						<td>
							{{ sprintf("%.2f ms", $details->getRtt()) }}
						</td>
						<td>
							{{ sprintf("%d ms", $details->getTtfb()) }}
						</td>
						<td>
							{{ sprintf("%d ms", $details->getInteractive()) }}
						</td>
						<td>
							{{ sprintf("%d ms", $details->getFirstPaint()) }}
						</td>
						<td>
							{{ $details->getNumRequests() }}
							(<em>{{ $details->getNumStylesheets() }} <i class="fa fa-css-alt-2"></i>/{{ $details->getNumScripts() }} <i class="fa fa-js-w"></i></em>)
						</td>
					</tbody>
				</table>
			</div>
		</div>
	</form>
	<div id="{{ $test->getHash() }}" class="collapse">
		<h5>Filters</h5>
		<p class="">
			{!! implode("<br />", $filters) !!}
		</p>
	</div>
</div>