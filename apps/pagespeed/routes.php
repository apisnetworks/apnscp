<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

	use Illuminate\Support\Facades\Route;

Route::post('/apply/{domain}', function (Page $p, $domain) {
	$filters = request()->post('filter');
	$superclass = request()->post('superclass');
	if (!$p->apply($domain, (array)$filters, (string)$superclass)) {
		return false;
	}
	return;
});

Route::post('/purge', function (Page $p) {
	return $p->purge(request()->post('domain'));
});

Route::post('/reset/{domain}', function (Page $p, $domain) {
	$filters = request()->post('filter');
	if (is_string($filters)) {
		$filters = explode(' ', $filters);
	}
	$p->apply($domain, (array)$filters);
	return $p->loadDomain($domain);
});
Route::get('/raw/{domain}', function (Page $p, $domain) {
	return json_encode($p->runTest($domain));
});
Route::post('/runTest/{domain}', function (Page $p, $domain) {
	$result = $p->runTest($domain);
	return $p->renderResults($domain, $result);
});
Route::get('/', 'Page@index');
Route::get('{domain?}', 'Page@loadDomain')->where('view', '(.*)');
