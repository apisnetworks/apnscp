<div class="ui-infobox ui-infobox-info">
	<p>
		Your account password and PostgreSQL password differ. A password must be on file to login automatically.
		Please enter it below or manually access phpPgAdmin at
		<a href="{{ $Page->phpPgAdminURL() }}">{{ PHPPGADMIN_LOCATION }}</a>
	</p>
</div>
<br/><br/>
@if (\UCard::get()->hasPrivilege("user"))
	<div class="ui-infobox ui-infobox-warning">
        <div class="ui-info">
            This is a non-privileged account. You must receive a PostgreSQL user account from the account
            owner to proceed to phpPgAdmin. If you have this information, then enter the username
            in the box below.
        </div>
	</div>
@endif
<form action="{{ \Template_Engine::app('phppgadmin') }}" method="POST">
	@if (\UCard::get()->hasPrivilege("user"))
		<fieldset class="form-group">
			<label for="username">PostgreSQL Username</label>
			<input id="username" class="form-control" type="text" name="username"
			       value="{{ $Page->get_pgsql_username() }}"/>
		</fieldset>

	@endif

	<fieldset class="form-group">
		<label for="password" class="mb-2">PostgreSQL Password</label>
		<input class="form-control mr-2 mb-2" id="password" type="password" name="password" value=""/>
		<button type="submit" class="mb-2 btn btn-primary" name="submit">
			Update
		</button>

	</fieldset>

	@if (UCard::get()->hasPrivilege('site'))
		<fieldset class="form-group">
			<label class="custom-control custom-checkbox mb-0 d-block">
				<input type="hidden" name="reset-password" value="0">
				<input type="checkbox" class="custom-control-input" name="reset-password"
				       value="1">
				<span class="custom-control-indicator"></span>
				Reset password for user <b>{{ $Page->get_pgsql_username() }}</b> to this value.
			</label>
			<i>All applications that rely on this username/password combination must be manually updated after
				resetting.</i>
		</fieldset>
	@endif
</form>