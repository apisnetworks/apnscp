<?php
	error_reporting(E_ALL);
	function fatal()
	{
		header('Internal Server Error', true, 550);
		exit();
	}

	$httpsonly = !empty($_SERVER['HTTPS']);

	$webdir = dirname(strtok($_SERVER['REQUEST_URI'], '?'));
	$redirect = isset($_GET['return']) ? $_GET['return'] : '';
	if (!$webdir || !$redirect) {
		fatal();
	}

	function sqmail()
	{
		// sqmail comes across as <hostname> <path>
		// also it's an ornery bastard
		cookie("SQMSESSID", $_GET['SQMSESSID'], true);
		cookie("key", $_GET['key'], true);
	}

	function horde()
	{
		// horde sends as .<hostname> on /
		cookie("Horde", $_GET['Horde']);
		$k = $v = null;
		foreach (array('auth_key', 'secret_key', 'horde_secret_key') as $k) {
			if (isset($_GET[$k])) {
				$v = $_GET[$k];
				break;
			}
		}
		cookie($k, $v, false, '/');
	}

	function roundcube()
	{
		// rc sends as <hostname> on /
		cookie("roundcube_sessid", null, true, '/');
		cookie("roundcube_sessid", $_GET['sessid'], true, '/');
		cookie("roundcube_sessauth", $_GET['sessauth'], true, '/');
	}

	function phpmyadmin()
	{
		$httpsStyle = !empty($_GET['https']) ? '_https' : '';
		cookie("phpMyAdmin{$httpsStyle}", $_GET['c'], true);
		foreach (['pma_iv-1', 'pma_mcrypt_iv', 'pmaAuth-1', 'pmaAuth-1_https'] as $ivname) {
			if (isset($_GET[$ivname])) {
				cookie($ivname, $_GET[$ivname], true);
				break;
			}
		}
		cookie("pmaUser-1{$httpsStyle}", $_GET['u'], true);
		cookie("pmaPass-1{$httpsStyle}", $_GET['pwd'], true);
		cookie("pma_collation_connection", 'utf8_general_ci', true);
	}

	function phppgadmin()
	{
		cookie("PPA_ID", $_GET['p']);
	}

	function cookie($name, $val, $trimHost = false, $path = null)
	{
		global $webdir, $httpsonly;
		$time = null === $val ? time() - 86400 * 7 : 0;
		$host = !empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
		$host = (@inet_pton($host) || $trimHost ? '' : '.') . $host;
		if ($path === null) {
			$path = isset($_GET['cookiePath']) && $_GET['cookiePath'][0] == '/' ? $_GET['cookiePath'] :
				rtrim($webdir, '/') . '/';
		}
		if ($trimHost) {
			$host = null;
		}
		if (null === $val) {
			setcookie($name, $val, $time, $path, ($trimHost ? $_SERVER['HTTP_HOST'] : $host));
		}
		setcookie($name, $val, $time, $path, $host, $httpsonly, 1);
	}

	$app = strtok(strtolower(trim($webdir, '/')), '/');

	if ($app === 'webmail') {
		sqmail();
	} else if ($app === 'horde') {
		horde();
	} else if ($app === 'roundcube') {
		roundcube();
	} else if ($app === 'myadmin' || $app === 'phpmyadmin') {
		phpmyadmin();
	} else if ($app === 'phppgadmin') {
		phppgadmin();
	} else {
		fatal();
	}
	header('Location: ' . $webdir . '/' . $redirect, true, 302);
	exit();
