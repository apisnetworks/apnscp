$(document).ready(function () {
	$("a.lightbox").lightBox();
	$("#ssl").click(function () {
		var ssl = $(this).prop("checked");
		$(".webmail-link").each(function () {
			var alt = $(this).attr("href"),
				neu = alt.replace(/&ssl=(true|false|\d)?/, "") + "&ssl=" + ssl;
			$(this).attr("href", neu);
		});
		return true;
	});
	$('[data-toggle=tooltip]').tooltip();
	$(document).on('click', '.edit-url', function () {
		var html = $('<div id="ch-webmail-modal">' +
			'<label for="url-input" class="b"><span id="app-name"></span> Location</label>' +
			'<span class="location-group">http:// <input type="text" name="new-url" ' +
			'maxlength="16" size="8" value="" id="url-input" />.' + session.domain + '/</span></div>');

		var appTitle = $(this).siblings('h4').text(),
			appName = this.id.replace(/^change-/, ""),
			re = new RegExp('^http://|\\.' + session.domain.replace(/\./g, '\\.') + '$', 'g'),
			oldLocation = $(this).siblings('.url').text().trim().replace(re, '');
		dialogWindow = apnscp.modal([html], {
			buttons: [
				$('<button type="button" class="btn btn-primary" name="change">').text("Change URL").click(function () {
					var newLocation = $('#url-input').val();
					if (!newLocation) {
						alert("missing location");
						return false;
					}
					processChange.call(this, appName, oldLocation, newLocation);

				})
			]
		})
			.on('show.bs.modal', function () {
				$('#url-input').val(oldLocation).setCursorPosition(0, -1);
				$('#app-name').text(appTitle);
			});
		dialogWindow.modal();
		return false;
	});
});

function processChange(webmailApp, oldLoc, newLoc) {
	var $indicator = apnscp.indicator(),
		dialogBar = dialogWindow.find('.btn.btn-primary');
	dialogBar.find('.ui-ajax-indicator').remove().end().append($indicator);
	dialogWindow.find('.ui-ajax-error-msg').remove();
	var ret = apnscp.cmd('email_set_webmail_location', [webmailApp, newLoc], {
		useCustomHandlers: true,
		indicator: $indicator
	}).fail(function (xhr, textStatus, errorThrown) {
		var status = $.parseJSON(xhr.responseText);
		if (status['return']) {
			// ignore flakey DNS
			return true;
		}
		// uses global error handler
		$indicator.removeClass('ui-ajax-loading').addClass('ui-ajax-error');
		return false;
	}).always(function (xhr, textStatus, errorThrown) {
		var status = xhr.hasOwnProperty('responseText') ? $.parseJSON(xhr.responseText) : xhr;

		if (!status['return']) {
			return apnscp.ajaxError(xhr, textStatus, "unknown error");
		}
		if (status['return']) {
			if (!status['success']) {
				// @TODO show errors/warnings?
				// apnscp.ajaxError(xhr, textStatus, "unknown error");
			}
			$indicator.removeClass('ui-ajax-loading').addClass('ui-ajax-success');
			setTimeout(function () {
				dialogWindow.modal('hide');
				var re = new RegExp('(http://|\\b)' + oldLoc + '\.', 'g'), newURL = '$1' + newLoc + '.';
				$('.ui-app').html($('.ui-app').html().replace(re, newURL));
			}, 500);
			return false;
		}

		// failure
	});
}