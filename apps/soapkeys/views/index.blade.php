<h3>API Endpoint</h3>
<div class="row mb-3">
	<p class="col-12">
		Use the following endpoint to connect to the API:
	</p>
	<div class="col-md-6">
		<label class="form-control-label">Endpoint URI</label>
		<div class="input-group">
			<input type="text" id="endpointURI" class="form-control" aria-describedby="clipboardCopyTip"
			       value="{{ $Page->soapUri() }}/soap"/>
			<span class="ui-action-copy input-group-addon ui-action-d-compact" id="clipboardCopyTip" data-toggle="tooltip"
			      data-clipboard-target="#endpointURI" title="Copy to Clipboard"></span>
		</div>
	</div>
	<div class="col-md-6">
		<label class="form-control-label">WSDL</label>
		<div class="input-group">
			<input type="text" id="clipboardCopyWsdl" class="form-control" aria-describedby="clipboardCopyWsdl"
			       value="{{ $Page->soapUri() }}/{{SOAP_WSDL}}"/>
			<span class="ui-action-copy input-group-addon ui-action-d-compact" id="clipboardCopyWsdlTip" data-toggle="tooltip"
			      data-clipboard-target="#clipboardCopyWsdl" title="Copy to Clipboard"></span>
		</div>
	</div>
</div>
<div class="row mb-3">
	<div class="col-12">
		<h3>Sample code</h3>
		@foreach ($langs as $lang => $data)
			@php $lclang = strtolower($lang); @endphp
			<div class="card">
				<div class="card-header instructions" role="tab" id="{{$lang}}">
					<h5 class="mb-0">
						<a data-toggle="collapse" data-parent="#accordion" href="#{{$lang}}Instructions"
						   aria-expanded="@if ($loop->first) true @else false @endif"
						   aria-controls="{{$lang}}Instructions">
							<h4 class="mb-0">
								<i class="fa fa-{{ $data['icon'] }} mr-2"></i>{{$lang}}
							</h4>
						</a>
					</h5>
				</div>

				<div id="{{ $lang }}Instructions" class="collapse @if ($loop->first)  @endif" role="tabpanel"
				     aria-labelledby="{{ $lang }}">
					<div class="card-block">
						@include("partials.examples.{$lclang}")
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>
<form method="post">
	<h3>Active Keys</h3>
	<table width="100%" class="multirow table" id="keys">
		<thead>
		<tr>
			<th class="">
				API Key
			</th>
			<th class="" width="30%">
				Last Used
			</th>
			<th class="center" width="15%">
				Actions
			</th>
		</tr>
		</thead>
		@foreach ($soapkeys as $soapkey)
			<tr class="entry">
				<td class="cell1 key-data">
		            <span class="key">{{ strtoupper(implode('-',str_split($soapkey['key'],8))) }}</span>
					<span class="comment">{{ $soapkey['comment'] }}</span>
				</td>
				<td class="cell1 key-timestamp">
					{{ $soapkey['last_used'] ? date('M d, Y h:i:s A T',$soapkey['last_used']) : 'Never' }}
				</td>
				<td class="center actions">
					<div class="input-group">
						<div class="btn-group mx-auto">
							<button class="btn btn-secondary ui-action-copy ui-action-label" type="button"
							        data-clipboard-text="{{ $soapkey['key'] }}">
								Copy
							</button>
							<button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
							        aria-haspopup="true"
							        aria-expanded="false"></button>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="ui-action-edit ui-action-label btn btn-block dropdown-item"
								   href="?edit={{ $soapkey['key'] }}">Set Comment</a>
								<div class="dropdown-divider"></div>
								<a class="ui-action btn btn-block warn ui-action-delete ui-action-label dropdown-item"
								   href="?delete={{ $soapkey['key'] }}"
								   OnClick="return confirm('Are you sure you would like to delete this key?');">
									Delete
								</a>
							</div>
						</div>
					</div>

				</td>
			</tr>
		@endforeach
	</table>
	<div class="mt-1 btn-group">
		<input type="text" id="comment" class="form-control" name="comment" maxlength="255" value=""
		       placeholder="Optional Comment" size="64"/>
		<button type="submit" class="btn btn-primary primary" name="Generate_Key" value="Create Key">
			Create Key
		</button>
	</div>
</form>
