<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\usermanage;

	use Page_Container;

	class Page extends Page_Container
	{
		private $users;
		private $users_filtered;
		private $search_options;
		private array $filtered_users;

		public function __construct()
		{
			parent::__construct();
			$users = $this->get_users();
			$css = '<style type="text/css">';
			foreach ($users as $user => $info) {
				$css .= '#quota-' . $user . ' .ui-gauge-used { width:' .
					min(100, round($info['quota-used'] / max($info['quota-total'], 1) * 100)) . '%; }' .
					"\n";
			}
			$css .= '</style>';
			$this->add_head($css);
			$this->add_javascript('usermanage.js');
			$this->init_js('sorter');
		}

		public function get_users()
		{
			if (isset($this->users_filtered)) {
				return $this->users_filtered;
			}

			$uids = array();
			$ssh_enabled = \Util_Conf::get_svc_config('ssh', 'enabled');
			$user_list = $this->user_get_users();
			$quotas = $this->user_get_quota(array_keys($user_list));
			$admin = $this->common_get_service_value('siteinfo', 'admin_user');
			$prefix = $this->getAuthContext()->domain_fs_path();
			foreach ($user_list as $username => $pw) {
				if (!isset($quotas[$username])) {
					report("%s %s",
						var_export($quotas, true), var_export(array_keys($user_list), true));
				}
				$smtp_enabled = $imap_enabled = null;
				if ($this->email_enabled()) {
					$smtp_enabled = $this->email_user_permitted($username, 'smtp');
					$imap_enabled = $this->email_user_permitted($username, 'imap');

				}

				$sub_local = sizeof(glob($prefix . '/var/subdomain/' . $username . '.*')) > 0;
				$sub_global = file_exists($prefix . '/var/subdomain/' . $username);

				$jail_conf = $prefix . \Ftp_Module::VSFTPD_CONF_DIR . '/' . $username;
				$users[$username] = array(
					'quota-total'  => round($quotas[$username]['qhard'] / 1024, 2),
					'quota-used'   => round($quotas[$username]['qused'] / 1024, 2),
					'gecos'        => $pw['gecos'],
					'ftp-enabled'  => $this->ftp_enabled() && $this->ftp_user_permitted($username),
					'ftp-jail'     => $this->ftp_enabled() && $this->ftp_user_jailed($username),
					'ftp-custom'   => file_exists($jail_conf) && $this->ftp_get_option($username, 'local_root'),
					'mail-enabled' => $smtp_enabled && $imap_enabled,
					'mail-in'      => $imap_enabled,
					'mail-out'     => $smtp_enabled,
					'www-enabled'  => $sub_local || $sub_global || $username == $admin && $this->common_get_service_value('apache', 'enabled'),
					'www-loc'      => $sub_local,
					'www-gbl'      => $sub_global,
					'cp-enabled'   => $this->auth_user_permitted($username, 'cp'),
					'dav-enabled'  => $this->auth_user_permitted($username, 'dav'),
					'ssh-enabled'  => $ssh_enabled && $this->ssh_user_permitted($username)
				);
			}
			$this->users = $this->users_filtered = $users;

			return $this->users_filtered;
		}

		public function on_postback($params)
		{
			if (isset($params['searchop'])) {
				$this->search_options = array(
					'clause' => $params['searchtext'],
					'op'     => $params['searchop'],
					'field'  => $params['searchby']
				);
				$this->filtered_users = $this->filter_users($params['searchtext'],
					$params['searchop'],
					$params['searchby']
				);

			}

			if (isset($params['delete']) || isset($params['remove_users'])) {
				// user blindly clicked remove users without selecting anything
				if (!isset($params['delete']) && !isset($params['users'])) {
					return error("no users selected to delete");
				}
				if (isset($params['delete'])) {
					$users = array_keys($params['delete']);
				} else {
					$users = $params['users'];
				}
				foreach ($users as $user) {
					if ($this->user_delete($user)) {
						unset($this->users[$user],
							$this->users_filtered[$user]);
					}
				}
				if (isset($params['searchtext'])) {
					$this->filter_users($params['searchtext'], $params['searchop'], $params['searchby']);
				}
			} else if (isset($params['edit'])) {
				$user = array_keys($params['edit']);
				header('Location: ' . \HTML_Kit::new_page_url_params(\Template_Engine::app('useredit'), ['user' => array_pop($user)]), 302);
			} else if (isset($params['hijack'])) {
				if (!$sid = $this->site_hijack($params['hijack'])) {
					return false;
				}
				\Auth::get_driver()->login_success();
				exit();
			} else if (isset($params['reset'])) {
				$user = current($params['reset']);
				return $this->auth_reset_password($user);
			}

			return true;
		}

		private function filter_users($clause, $comparator, $field)
		{
			$services = array('mail', 'ftp', 'www', 'cp', 'ssh');
			$filtered = array();

			$users = $this->get_users();
			if (!$clause && !in_array($field, $services)) {
				return $users;
			}

			foreach ($users as $user => $info) {
				$cmp = false;
				if ($field == 'username') {
					$cmp = $user;
				} else if ($field == 'gecos') {
					$cmp = $info['gecos'];
				} else if (in_array($field, $services)) {
					$cmp = $field;
				}

				if ($comparator == 'contains') {
					$cmp = stristr($cmp, $clause);
				} else if ($comparator == 'notcontains') {
					$cmp = !stristr($cmp, $clause);
				} else if ($comparator == 'service-enabled') {
					$idx = $field . '-enabled';
					$cmp = (bool)$info[$idx];
				} else if ($comparator == 'service-disabled') {
					$idx = $field . '-enabled';
					$cmp = !(bool)$info[$idx];
				}

				if (!$cmp) {
					continue;
				}

				$filtered[$user] = $info;
			}
			$this->users_filtered = $filtered;

			return $filtered;
		}

		public function get_search_text()
		{
			return $this->search_options ? $this->search_options['clause'] : '';
		}

		public function enable_css_class($user, $svc)
		{
			$users = $this->get_users();

			return !empty($users[$user][$svc . '-enabled']) ? 'ui-action-is-enabled' : 'ui-action-is-disabled';
		}

		public function hide_show_svc($user, $svc)
		{
			$users = $this->get_users();
			if ($users[$user][$svc]) {
				return 'show';
			}

			$hide_cls = 'hide';
			switch ($svc) {
				case 'mail-in':
				case 'mail-out':
					return $hide_cls . ' mail';
				case 'ftp-jail':
				case 'ftp-custom':
					return $hide_cls . ' ftp';
				case 'www-gbl':
				case 'www-loc':
					return $hide_cls . ' www';

			}
			warn("unmatched service " . $svc);

			return 'show';
		}

		public function ssh_enabled()
		{
			return \Util_Conf::get_svc_config('ssh', 'enabled');
		}
	}

?>