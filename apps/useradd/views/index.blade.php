<div class="d-flex align-content-around">
	@if ($Page->get_mode() == 'edit')
		<div class="head5 ml-auto d-none d-md-block">
			<a class="ui-action ui-action-switch-app ui-action-label" href="{{ \Template_Engine::app('usermanage') }}">Return to User
				Overview</a>
		</div>
	@elseif ($Page->get_mode() === 'add')
		<div class="head5 ml-auto d-none d-md-block">
			Change Mode: <a class="ui-action ui-action-switch-app ui-action-label" href="{{ \Template_Engine::app('usermanage') }}">Manage
				Users</a>
		</div>
	@endif
</div>

<form method="post" class="" data-toggle="@if ($Page->get_mode() == 'add') validator @endif" id="userForm">
	@includeWhen($Page->get_mode() != 'defaults', 'partials.edit-fields')

	@include('partials.general')
	@includeWhen(\cmd('email_configured'), 'partials.email')
	@includeWhen(\cmd('ftp_enabled'), 'partials.ftp')
	@includeWhen(DAV_ENABLED, 'partials.dav')
	@includeWhen(\cmd('common_get_service_value', 'apache', 'enabled'), 'partials.web')
	@include('partials.cp')
	@includeWhen(\Util_Conf::get_svc_config('ssh', 'enabled'), 'partials.ssh')


	<div class="row">
		<div class="col-12">
			@if ($Page->get_mode() == 'add')
				<button type="submit" class="btn btn-primary my-3" value="Add User" name="add_user">
					Add User
				</button>
			@elseif ($Page->get_mode() == 'defaults')
				<button type="submit" class="btn btn-primary my-3" value="Set Defaults" name="save">
					Set Defaults
				</button>
			@else
				<button type="submit" class="btn btn-primary my-3" value="Save Changes" name="ch_user">
					Save Changes
				</button>
			@endif
			<button type="reset" class="btn btn-secondary ml-3 my-3 warn" value="Reset" name="reset">
				Reset
			</button>
		</div>
	</div>

	@if ($Page->get_mode() === "add")
		<fieldset class="row form-group">
			<div class="col-12 mt-1">
				<label class="pl-0 form-inline custom-checkbox custom-control mt-1 align-items-center d-flex">
					<input type="checkbox" class="custom-control-input" name="setdefault" value="1" id="setdefault"/>
					<span class="custom-control-indicator"></span>
					Set as default for new users
				</label>
			</div>
		</fieldset>
	@endif
</form>

<form method="post" action="{{ HTML_Kit::page_url_params() }}" class="hide" id="change-form">
	<fieldset class="form-group  py-3">
		<label class="" for="new-data">New Username</label>
		<div class=" input-group">
			<div class="input-group-addon">
				<i class="fa fa-user"></i>
			</div>
			<input id="new-data" class="form-control" type="text" pattern="{{ \HTML_Kit::regexHtml(\Regex::USERNAME) }}" name="new-value"
			       value="{{ $Page->get_option('username') }}" />
		</div>
	</fieldset>
	<button type="submit" name="change" class="btn btn-primary" value="1">
		Change Username
	</button>
</form>