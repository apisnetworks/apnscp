$(document).ready(function () {
	$('[data-toggle=tooltip]').tooltip();
	$('#username').focus();
	$('#random-password').on('change', function () {
		if ($(this).prop('checked')) {
			$('#password').val("XXXXXX").prop('disabled', true);
			$('#password_confirm').val("XXXXXX");
		} else {
			$(['#password', '#password_confirm']).each(function (i, v) {
				$(v).prop('disabled', false).val("").removeAttr("formnovalidate");
			});
		}
		return true;
	});

	$('#disk_slider').slider({
		minValue: 0,
		maxValue: 100,
		step: 5,
		startValue: Math.floor($('#disk_quota').val() / quota['max'] * 100),
		slide: function (e, ui) {
			$('#disk_quota').val(Math.floor(ui.value / 100 * quota['max']));
		},
		change: function (e, ui) {
			if (ui.value == 0 || ui.value == 100) {
				$('#disk_quota').val(0);
				$('#disk_unlimited').prop('checked', true);
			} else {
				$('#disk_unlimited').prop('checked', false);

			}
		}
	});

	$('#email_domains, #subdomain_domains').multiSelect({
		noneSelected: "Select domains",
		oneOrMoreSelected: "% domains selected"
	});

	$('#randomPassword').on('change', function () {
		if ($(this).prop('checked')) {
			$('#password').val("").attr('data-empty', false).prop('disabled', true);
			$('#password, #password_confirm').prop('required', false);
		} else {
			$('#password').attr('data-empty', true).prop('disabled', false);
			$('#password, #password_confirm').prop('required', true);
		}
	});

});

$(window).on('load', function () {
	var updateQuota = function () {
		var val = $(this).val();
		if (val > quota.max) val = 0;

		$('#disk_slider').slider('value', Math.floor(val / quota.max * 100));
		$(this).val(val);
	};

	$('#password').focus(function () {
		$('.password-confirm-container').fadeIn('fast');
		$('.password-random-container').fadeOut('fast');
	}).blur(function () {
		if ($("#password").val()) {
			$('.password-random-container').fadeOut('fast');
		} else {
			$('.password-random-container').fadeIn('fast');
			$('.password-confirm-container').val("").fadeOut('fast');
		}
	});

	/*$('#password').checkPassword({

	 });*/

	$('#disk_quota').change(function () {
		if ($(this).val() > 0)
			$('#disk_unlimited').prop('checked', false);
	});

	$('#disk_quota').focus(function () {
		$('#disk_slider').show();
	});

	$('#disk_quota').change(updateQuota);//.change();

	$('#jail_chpath').click(
		function () {
			var dialog = apnscp.modal($('#file_container'));
			apnscp.explorer({
				filter: 'filter=dir;show,/;show,/var/www;show,/home;show,/usr/local',
				onSelect: function (file, b) {
					$('#selected_dir').text(file);
				}
			});
			$('#select_dir').click(function () {
				$('#custom_jail').val($("#selected_dir").text());
				$('#file_container').hide();
				dialog.modal('hide');
				return true;
			})
			return false;
		}
	);
	$('#email_enable').click(function () {
		$('#email_smtp, #email_imap').prop('checked', $(this).prop('checked'));
		return true;
	});

	$('#ssh_enable').click(function () {
		$('#crontab_enable').prop('checked', $(this).prop('checked'));
		return true;
	});

	$('#change-username').click(function () {
		$('#change-warning').slideToggle(function () {
			$(this).find(':input').click(function () {
				if (!$(this).prop('checked')) {
					return true;
				}
				dialogWindow = apnscp.modal($('#change-form')).on('shown.bs.modal', function () {
					dialogWindow.find(':text').setCursorPosition(0, -1);
					$(this).find('.btn.btn-primary').on('click', function () {
						changeUsername.call(this, edituser.username, dialogWindow.find(':text').val());
						return false;
					});
				});
				dialogWindow.modal();
				return true;
			});
			return false;
		});
	});
});

function changeUsername(olduser, newuser) {

	var $indicator = $('<i></i>'),
		dialogBar = $('#modal');
	dialogBar.find('.btn-primary').find('.ui-ajax-indicator').remove().end().append($indicator);
	dialogWindow.find('.ui-ajax-error-msg').remove();
	$('#modal .btn-primary').prop('disabled', function (i, val) {
		$(this).append($indicator);
		return true;
	});
	apnscp.cmd('user_rename_user', [olduser, newuser], {
		useCustomHandlers: true,
		indicator: $indicator
	}).fail(function (xhr, textStatus, errorThrown) {
		var status = $.parseJSON(xhr.responseText);
		if (status['errors']) {
			var $err = [];
			for (var i in status['errors']) {
				$err.push($('<p class="ui-ajax-error-msg">' + status['errors'][i] + '</p>'));
			}
			dialogWindow.find('.modal-body').append($err);
		}
		$indicator.removeClass('ui-ajax-loading').addClass('ui-ajax-error');
		$('#modal .btn-primary').prop('disabled', false);
		return false;
	}).done(function (status, textStatus, xhr) {
			if (!status['success']) {
				return apnscp.ajaxError(xhr, textStatus, "unknown error");
			}
			var time = 3000;
			$indicator.removeClass('ui-ajax-loading').addClass('ui-ajax-success');
			dialogWindow.find('.modal-body').empty().append($('<p class="ui-ajax-success-msg">Success! Redirecting to new user in ' +
				'<span class="seconds">' + time / 1000 + '</span>...</p>'));
			setInterval(function () {
				time -= 1000;
				dialogWindow.find('.seconds').text(time / 1000);
				if (time <= 0) {
					dialogWindow.modal('hide');
					window.location = apnscp.appUrl('useredit', '?user=' + newuser);
				}
			}, 1000);
			// failure
		}
	);
}