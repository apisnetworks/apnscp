@extends('theme::layout')

@section('content')
@if (!$Page->is_postback)
<div class="row">
	<div class="col-12 d-flex align-items-center mb-3">
		<figure class="mr-1">
			@if (\Opcenter\Mail\Services\Spamassassin::present())
				<img src="/images/apps/saconfig/spamassassinlogo.png" class="img img-fluid d-none d-sm-inline-block" style="max-height:128px;"/>
			@else
				<img src="/images/apps/saconfig/rspamdlogo.png" class="img img-fluid d-none d-sm-inline-block"
				     style="max-height:128px;"/>
			@endif
		</figure>
		<p class="lead mb-0">
			Tweak {{ $Page->getFilterProper() }} filtering with our famous five minute
			utility. Answer a few questions, then let us do the heavy
			lifting to deliver a personalized configuration and
			mail processing recipe.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-12">
		<h4>Filtering beyond tagging</h4>
		<p class="">
			Successful spam filtering is broken into two parts: tagging and filtering.
			Tagging happens when {{ $Page->getFilterProper() }} scans a message to determine
			legitimacy. A message is either tagged as spam or ham (non-spam). Think
			of this as sorting mail in a mail distribution center. After scanning,
			mail goes to a local delivery agent (LDA) that handles
			putting your mail in your inbox, very much like a postal carrier putting a
			letter in the correct mailbox.
		</p>
	</div>
</div>
@endif


@includeWhen(isset($recipe), 'partials.generated')
@includeWhen(UCard::is('site'), 'partials.global')

<form method="post" id="saconfig">
	@includeWhen(\Opcenter\Mail\Services\Spamassassin::present(), 'partials.steps.thresholds-tagging')
	@include('partials.steps.handling')
	@includeWhen(\Opcenter\Mail\Services\Spamassassin::present(), 'partials.steps.miscellaneous')
	@include('partials.steps.setup')

	<button type="submit" name="generate" value="1" class="btn btn-lg btn-primary">
		Submit
	</button>
</form>
@endsection