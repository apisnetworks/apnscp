<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\verco;

	use Page_Container;

	class Page extends Page_Container
	{

		public function on_postback($params)
		{
			if (isset($params['enable_svn'])) {
				$status = $this->getApnscpFunctionInterceptor()->verco_enable_svn();
				$this->bind($status);

				return $status;
			} else if (isset($params['enable_cvs'])) {
				$status = $this->getApnscpFunctionInterceptor()->verco_enable_cvs();
				$this->bind($status);

				return $status;
			} else if (isset($params['create_svn'])) {
				$status = $this->getApnscpFunctionInterceptor()->verco_create_svn_repository($params['svn_name']);
				$this->bind($status);

				return $status;
			} else if (isset($params['create_cvs'])) {
				$status = $this->getApnscpFunctionInterceptor()->verco_create_cvs_repository($params['cvs_name']);
				$this->bind($status);

				return $status;
			} else if (isset($params['t']) && $params['t'] == 'c') {
				$status = $this->getApnscpFunctionInterceptor()->verco_delete_cvs_repository($params['d']);
				$this->bind($status);

				return $status;
			} else if (isset($params['t']) && $params['t'] == 's') {
				$status = $this->getApnscpFunctionInterceptor()->verco_delete_svn_repository($params['d']);
				$this->bind($status);

				return $status;
			} else if (isset($params['enable_trac'])) {
				$this->bind($this->getApnscpFunctionInterceptor()->verco_install_trac());
			}

			if (!$this->errors_exist()) {
			}

		}

		public function get_trac_version()
		{
			return $this->getApnscpFunctionInterceptor()->verco_get_trac_version();
		}

		public function getSWCVSVer()
		{
			return $this->getApnscpFunctionInterceptor()->verco_get_current_system_cvs_version();
		}

		public function getInstalledCVSVer()
		{
			return $this->getApnscpFunctionInterceptor()->verco_get_local_installed_cvs_version();
		}

		public function getSWSVNVer()
		{
			return $this->getApnscpFunctionInterceptor()->verco_get_current_system_svn_version();
		}

		public function getInstalledSVNVer()
		{
			return $this->getApnscpFunctionInterceptor()->verco_get_local_installed_svn_version();
		}

		public function svnEnabled()
		{
			return $this->getApnscpFunctionInterceptor()->verco_svn_enabled();
		}

		public function listKnownSVNRepos()
		{
			return $this->getApnscpFunctionInterceptor()->verco_list_known_svn_repositories();
		}

		public function listKnownCVSRepos()
		{
			return $this->getApnscpFunctionInterceptor()->verco_list_known_cvs_repositories();
		}

		public function cvsEnabled()
		{
			return $this->getApnscpFunctionInterceptor()->verco_cvs_enabled();
		}

		public function trac_enabled()
		{
			return $this->getApnscpFunctionInterceptor()->verco_trac_installed();
		}
	}

?>
