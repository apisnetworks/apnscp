<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\cockpit;

	use Page_Container;

	class Page extends Page_Container
	{
		const IFRAME_URL = '/admin/';
		/**
		 * @var string|null
		 */
		protected $password;

		public function __construct()
		{
			parent::__construct();
			\Page_Renderer::hide_all();

			$this->add_javascript('cockpit.js', 'external', false, true);
			$this->add_css('#ui-app, .container-fluid, #ui-app-container {margin: 0 !important; padding: 0 !important;};', 'internal');
		}
	}