var bounding;

var observer = new MutationObserver(function (mutations) {
	bounding = document.getElementById('terminal');
	if (bounding) {
		observer.disconnect();

		var elements = bounding.contentDocument.getElementsByTagName("base")
		for (var i = elements.length -1 ; i >= 0; --i) {
			const urlString = elements[i].href;
			const url = new URL(session.multiPath + new URL(urlString).pathname, urlString);
			if (url) {
				elements[i].href = url.toString();
			}
		}
	}
});

window.onload=function() {
	observer.observe(document.body, { //document.body is node target to observe
		childList: true, //This is a must have for the observer with subtree
		subtree: true //Set to true if changes must also be observed in descendants.
	});
}