<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace apps\ssl;

// @TODO move to standalone library in lib/Ssl?

use Opcenter\Crypto\Ssl\Certificate;
use Opcenter\Crypto\Ssl\Key;

class CertificateResource
	{
		use \apnscpFunctionInterceptorTrait;
		use \ContextableTrait;

		// @var Certificate
		protected $certificate;
		// @var Certificate
		protected $chain;
		// @var Key
		protected $pkey;

		protected function __construct(string $pkey = null, string $x509 = null)
		{
			if ($pkey) {
				$this->pkey = new Key($pkey);
			}
			if ($x509) {
				$this->certificate = new Certificate($x509);
			}
		}

		public function hasChain(): bool
		{
			return null !== $this->chain;
		}

		public function setChain(string $crt): void
		{
			$this->chain = new Certificate($crt);
		}

		public function getPublicKey(): ?string
		{
			if (!$crttxt = $this->getX509()) {
				return null;
			}
			$x509 = openssl_x509_read($crttxt);

			return openssl_pkey_get_details(openssl_pkey_get_public($x509))['key'];
		}

		public function getX509(): string
		{
			return (string)$this->certificate;
		}

		public function isLetsEncrypt(): bool
		{
			return $this->certificate && $this->letsencrypt_is_ca($this->getX509());
		}

		public function isSelfSigned(): bool
		{
			return $this->ssl_is_self_signed($this->getX509());
		}

		public function getKeyField(string $field)
		{
			return array_get($this->pkey, $field, null);
		}

		public function getX509Field(string $field)
		{
			return $this->certificate?->field($field);
		}

		public function getName(): string
		{
			return 'server.crt';
		}

		public function isExtendedValidation(): bool
		{
			return preg_match('/1\.3\.6\.1\.4\.1\.311\.60\.2\.1\.[1-3]/',
					$this->certificate->field('name', '')) > 0;
		}

		/**
		 * Get hostnames attached
		 *
		 * @return array
		 */
		public function getHostnames(): array
		{
			if (!$this->getX509()) {
				return [];
			}

			return $this->ssl_get_alternative_names($this->getX509());
		}

		public function getIssuerUrl(): ?string
		{
			$patterns = [
				$this->certificate->field('issuer.CN')
			];
			foreach ($patterns as $pattern) {
				if (preg_match('!^https?://!', (string)$pattern)) {
					return $pattern;
				}
			}
			return null;
		}

		public function getIssuerOrganization(): ?string
		{
			if (null === $this->certificate->field('issuer.CN')) {
				return null;
			}

			if (null !== ($ou = $this->certificate->field('issuer.OU'))) {
				return $ou;
			}

			if (null !== ($o = $this->certificate->field('issuer.O'))) {
				return $o;
			}

			return 'UNKNOWN';
		}

		public function getIssuerOrganizationUnit(): ?string
		{
			if (null !== ($ou = $this->certificate->field('issuer.OU'))) {
				return $ou;
			}

			return preg_replace('!https?://!', '', $this->certificate->field('issuer.CN') ?? 'UNKNOWN');
		}
	}