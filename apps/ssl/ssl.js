var GLOBAL_MODE = 'LE', modal;
$(window).on('load', function () {
	$('[data-toggle=popover]').popover({
		trigger: 'hover click'
	}).click(function () {
		return false;
	});
	var maps = buildMap();
	$('#subdomain, #domain').change(function (e) {
		var empty, fill, name = e.target.name;
		if (name == 'subdomain') {
			empty = 'domain';
			fill = 'subdomain';
		} else {
			empty = 'subdomain';
			fill = 'domain';
		}

		var val = $('#' + fill).val(),
			$populate = $('#' + empty), oldVal;
		oldVal = $populate.val();
		$populate.empty();

		Object.keys(maps[name][val]).forEach(function (subdomain) {
			var label = subdomain;
			if (subdomain === '') {
				label = '&lt;none&gt;';
			}
			if (subdomain === '*' && GLOBAL_MODE === 'LE' &&
				(!getNestedObject(session, 'svc.dns.enabled') || getNestedObject(session, 'svc.dns.provider') === 'null'))
			{
				return;
			}
			$populate.append(
				$('<option ' + (subdomain == oldVal ? 'selected="SELECTED"' : '') + 'value="' + subdomain + '">' + label + '</option>')
			);
		});

		updateURLPreview();
		return true;
	}).change();
	$('#sslSubjects').click(function (e) {
		var $target = $(e.target);
		if ($target.hasClass('ui-action-delete')) {
			$target.fadeOut('fast', function () {
				$(this).parent().remove();
			});
		}
	});
	$('#country').change(function () {
		return loadStates($(this).val());
	});
	if ($('#country').val().toLowerCase() !== 'us') {
		$('#country').triggerHandler('change');
	}
	$('#addHostname').click(function () {
		addHostname();
		return false;
	});
	$('#certificateForm').bind('submit', function () {
		if (!$('#create-certificate:visible').length) {
			return;
		}
		return validateForm();
	});
	$('div.button-actions button').click(function () {
		var btnname = $(this).attr('name');
		GLOBAL_MODE = null;
		switch (btnname) {
			case 'download':
				return true;
			case 'selfsign':
			case 'lecert':
			case 'generate-newkey':
			case 'generate':
				GLOBAL_MODE = 'csr';
				var submitMode = 'csr', label = 'Generate', formLabel = 'Certificate Request';
				if (btnname == 'selfsign' || btnname == 'lecert') {
					submitMode = 'self';
					label = 'Generate & Install';
					if (btnname == 'lecert') {
						GLOBAL_MODE = 'LE';
						$('#letsencrypt-legal, #generate-request').show();
						submitMode = 'lecert';
						formLabel = "Let's Encrypt Request";
						// update subdomain options to remove wildcard
						$('#domain').triggerHandler('change');
						$('#addHostname').show();
					} else {
						GLOBAL_MODE = 'self';
						formLabel = 'Self-Signed Certificate Request';
					}
				}

				// hide LE niceties
				var method = 'hide';
				if (btnname !== 'lecert') {
					$('#letsencrypt-legal').hide();
					method = 'show';
					$('#locale').show().find('#country').triggerHandler('change');
					$('.le-hidden').show();
				} else if (btnname === 'lecert') {
					method = 'show';
					$('#locale').hide();
					$('.le-hidden').hide();

				}
				if (method === 'show') {
					$('#sslSubjects').show();
				}
				$('#domain').triggerHandler('change');

				$('#generate-request').attr('value', submitMode);
				$('#generate-request').text(label);
				$('#create-certificate').collapse(method);
				$('#create-certificate legend').text(formLabel);
				return false;
				break;

			case 'delete':
				showBackupDialog(
					[$("<button id='delete-warn' type='button'>").append([$("<i class='ui-action ui-action-delete'></i>"), "Delete"]).click(function () {
						modal.modal('hide');
						$('#delete').unbind().click();
						return true;
					}).prop('disabled', true).addClass('btn btn-secondary warn')
					]
				);
				break;
			case 'import':
				redirectTicket();
				return true;
			case 'replace-prompt':
				showBackupDialog(
					[$('<button id="replace-btn" type="button"><i class="fa fa-files-o"></i> Replace Certificate</button>').addClass('btn warn btn-secondary').click(function () {
						$('#modal').modal('hide');
						redirectTicket();
						//$('#replace-container').slideDown();
						return false;
					}).prop('disabled', true)
					]
				);
				break;
			default:
				return true;
		}
		return false;
	}).end();
	// @TODO no wildcard SSL yet for Let's Encrypt
});

function buildMap()
{
	var domains = {}, subdomains = {};
	Object.keys(domain_map).forEach(function (domain) {
		domains[domain] = {};
		domain_map[domain].sort().forEach(function (subdomain) {
			domains[domain][subdomain] = 1;

			if (subdomain === '*' && GLOBAL_MODE === 'LE' &&
				(!getNestedObject(session, 'svc.dns.enabled') || getNestedObject(session, 'svc.dns.provider') === 'null')) {
				$('.dns-hide').remove();
				return;
			}
			if (!subdomains.hasOwnProperty(subdomain)) {
				subdomains[subdomain] = {};
			}

			subdomains[subdomain][domain] = 1;

		});
	});
	return {'domain': domains, 'subdomain': subdomains};
}


/**
 * Add a subdomain + domain combination to SAN
 */
function addHostname() {
	var subdomain = $('#subdomain').val(),
		domain = $('#domain').val();
	var add = function (subdomain, domain) {
		var san = (subdomain + '.' + domain).replace(/^\.|\.$/, ""),
			$input = $('<input type="hidden" name="hostname[]" value="' + san + '" />'),
			$btn = $('<button type="button">').addClass('ui-action ui-action-label btn warn ui-action-delete btn-secondary').text(san).append($input),
			$child = $('<li/>').addClass('pb-2 list-inline-item').append($btn);
		if ($('#sslSubjects li').filter(function () {
			return $(this).text().trim() === san;
		}).length > 0) {
			return false;
		}
		$('#sample-url').effect("transfer", {to: $("#sslSubjects")}, 500, function () {
			$('#sslSubjects').append($child);
		});
	}
	add(subdomain, domain);
	if (subdomain === "www" || !subdomain) {
		add(subdomain === "www" ? "" : "www", domain);
	} else if (subdomain === '*') {
		add('', domain);
	}
}

function _initializeUploader() {
	var uploadButton = $('<button/>').text("Delete").addClass('warn btn btn-secondary');
	$('#file-upload').fileupload({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		done: function () {
			console.log("Done");
		},
		autoUpload: true,
		acceptFileTypes: /\.(?:zip|crt|key)$/i
	}).on('fileuploadadd', function (e, data) {
		data.context = $('<div/>').appendTo('#files');
		$.each(data.files, function (index, file) {
			var node = $('<p/>').append(uploadButton.clone(true).data(data))
				.append($('<span/>').text(file.name))
				.append('<br>');
			node.appendTo(data.context);
		});
	}).on('fileuploadprocessalways', function (e, data) {
		var index = data.index,
			file = data.files[index],
			node = $(data.context.children()[index]);
		console.log(data);
		if (file.preview) {
			node
				.prepend('<br>')
				.prepend(file.preview);
		}
		if (file.error) {
			node
				.append('<br>')
				.append($('<span class="text-danger"/>').text(file.error));
		}
		if (index + 1 === data.files.length) {
			console.log("beginning");
			data.context.find('#upload-begin')
				.text('Upload')
				.prop('disabled', !!data.files.error);
		}
	}).on('fileuploadprogressall', function (e, data) {
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress .progress-bar').css(
			'width',
			progress + '%'
		);
	}).on('fileuploaddone', function (e, data) {
		console.log(e, data);
		$.each(data.result.files, function (index, file) {
			if (file.url) {
				var link = $('<a>')
					.attr('target', '_blank')
					.prop('href', file.url);
				$(data.context.children()[index])
					.wrap(link);
			} else if (file.error) {
				var error = $('<span class="text-danger"/>').text(file.error);
				$(data.context.children()[index])
					.append('<br>')
					.append(error);
			}
		});
	}).on('fileuploadfail', function (e, data) {
		$.each(data.files, function (index) {
			var error = $('<span class="text-danger"/>').text('File upload failed.');
			$(data.context.children()[index])
				.append('<br>')
				.append(error);
		});
	}).prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');

	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: $('#file-upload').fileupload('option', 'url'),
		dataType: 'json',
		context: $('#file-upload')
	}).always(function () {
		$(this).removeClass('fileupload-processing');
	}).done(function (result) {
		$(this).fileupload('option', 'done')
			.call(this, $.Event('done'), {result: result});
	});
};

function redirectTicket() {
	apnscp.cmd('crm_get_subject_id_by_subject', ['http, ssl'], function (data) {
		var id = data['return'];

		window.location.href = apnscp.appUrl('troubleticket', '?new=1&subjectid=' + id);
	});
	return false;
}

function showBackupDialog(buttons) {
	defaultButtons = [
		$("<button type='button'>").addClass('btn btn-secondary mr-3').append($("<i class='fa fa-download'></i>")).append(" Download Certificate + Key").click(function (e) {
			$('#download').click();
			$(e.currentTarget).siblings('button').removeAttr('disabled').removeClass('ui-state-disabled');
			return false;
		})
	];
	if (!buttons) {
		buttons = [];
	}
	buttons = defaultButtons.concat(buttons);
	buttons.unshift('<div class="my-1"><b class="text-danger">WARNING:</b> the old certificate will ' +
		'be removed from your account. Download a backup before making changes. ' +
		'This certificate will be deleted.</div>');
	//buttons = jQuery.extend({}, defaultButtons, buttons);
	modal = apnscp.modal(buttons);
	modal.modal('show');
	return modal;
}

function loadStates(cc) {
	var states = apnscp.call_app(null, 'load_states', [cc], {dataType: 'json'}).done(function (d) {
		var $elm;
		var attrs = {
			'name': 'st',
			'id': 'state',
			'class': 'form-control'
		};
		if (!d || d instanceof Array) {
			$elm = $('<input type="text" />').attr(
				attrs
			);
		} else {
			var states = $.map(d,
				function (el, idx) {
					return '<option value="' + idx + '">' + el + '</option>';
				}
			).join("");
			$elm = $('<select />').attr(attrs).append($(states));
		}

		$('#state').replaceWith($elm);
		$elm.change(function loadStates() {
			($(this).val());
		});
	});
}

function updateURLPreview() {
	var subdomain = $('#subdomain').val(),
		domain = $('#domain').val(),
		$exsub = $('#ex-subdomain'),
		$exdomain = $('#ex-domain');
	if (subdomain) {
		$exsub.text(subdomain).show();
	} else {
		$exsub.hide();
	}
	$exdomain.text(domain);
}

function validateForm() {
	var error = function (msg, focus, id) {
		if (!id) {
			alert(msg);
		} else {
			$(id).text(msg).show();
		}
		if (focus) {
			$(focus).focus();
		}
		return false;
	};
	if (!$('#sslSubjects li').length) {
		return error("missing hostnames", '#sslSubjects');
	}
	if ($('#country:visible').get(0)) {
		if (!$('#country').val()) {
			return error("missing country", '#country');
		} else if (!$('#state').val()) {
			return error("missing state", '#state');
		} else if (!$('#city').val()) {
			return error("missing city", '#city');
		}
	}
	return true;
}