<fieldset class="form-group">
	<label class="" for="country">Country</label>
	<select class="custom-select form-control" name="c" id="country">
		@php $selected = $certificate->getX509Field('subject.C'); @endphp
		@foreach (\HTML_Kit::countries() as $cc => $cn)
			<option @if ($selected == $cc) SELECTED="SELECTED" @endif value="{{ $cc }}">
				{{ $cn }}
			</option>
		@endforeach
	</select>
</fieldset>

<fieldset class="form-group">
	<label for="state-us">State/Territory</label>
	<select name="st" class="custom-select form-control" id="state">
		@php
			$selected = strtolower((string)$certificate->getX509Field('subject.ST'));
		@endphp
		@foreach (\HTML_Kit::states($certificate->getX509Field('subject.C')) as $sc => $sn)
			<option value="{{ strtoupper($sc) }}" @if ($selected == $sc) SELECTED="SELECTED" @endif>
				{{ $sn }}
			</option>
		@endforeach
	</select>
</fieldset>

<fieldset class="form-group">
	<label for="city">City/Locality</label>
	<input type="text" class="form-control" name="l" placeholder="Atlanta" id="city"
	       value="{{ $certificate->getX509Field('subject.L') }}"/>
</fieldset>

<fieldset class="form-group">
	<label for="organization">Organization Name</label>
	<input type="text" class="form-control" name="o" placeholder="Optional" id="organization"
	       value="{{ $certificate->getX509Field('subject.O') }}"/>
</fieldset>

<fieldset class="form-group">
	<label for="organizationalunit">Organization Unit Name</label>
	<input type="text" class="form-control" name="ou" placeholder="Optional"
	       id="organizationalunit" {{ $certificate->getX509Field('subject.OU') }}/>
</fieldset>