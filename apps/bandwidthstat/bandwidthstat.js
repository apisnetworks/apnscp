var predictionValue = '.95';

$(document).ready(function () {
	var data = apnscp.call_app(null, 'getBandwidthData', [], {dataType: "json"}).then(function (data) {
		drawChart(data);
	})

});

function drawChart(bwdata) {
	var points = [], max = 0, ciRange = stats.stats['tscores'][stats.predictor],
		total, date;
	for (var i in bwdata) {
		total = parseFloat(bwdata[i]['total']);
		max = Math.max(max, total);
		date = parseInt(bwdata[i]['end']) * 1000,
			point = [date, parseFloat(bwdata[i]['total'])];
		points.push(point);
	}

	var data = [
		{
			label: "Historic Bandwidth",
			color: apnscp.themeColors().primary,
			lines: {show: true},
			data: points
		}
	];

	// not enough data for day
	if (stats.mode == 'month') {
		var mid = (ciRange['upper'] - ciRange['lower']) / 2, pointsCI = [
			/* last known data point */
			[points[points.length - 1][0], points[points.length - 1][1], 0],
			/* inteferred data point from CI calculation */
			[points[points.length - 1][0] + 86400 * 1000 * 30, mid + ciRange['lower'], mid],
			/* show a lower bound or no bound if lower bound is negative */
		], lowerCap = (ciRange['lower'] > 0 ? '-' : null);

		data.push(
			{
				label: "Predicted Usage",
				color: "darkorange",
				dashes: {show: true},
				data: pointsCI,
				points: {
					errorbars: "y",
					radius: 1,
					yerr: {
						symmetric: true,
						color: "rgba(255, 165, 0, 0.33);",
						show: true,
						upperCap: "-",
						lowerCap: lowerCap,
						radius: 5
					}
				}
			}
		);

		data.push({
			label: "Bandwidth Limit",
			color: "darkred",
			data: [
				[points[0][0], stats.limit],
				[points[points.length - 1][0] + 86400 * 1000 * 30, stats.limit]
			],
			dashes: {show: true, lineWidth: 1, dashLength: [3, 5]}
		});
	}
	//var width = $('#bandwidth-graph');
	$.plot('#bandwidth-graph', data, {
		grow: {
			steps: 75,
			growings: [
				{
					valueIndex: 1,
					stepMode: 'linear'
				}
			]
		},
		series: {
			grow: {
				active: true,
				duration: 2000
			}
		},
		legend: {
			show: false,
			margin: 0
		},
		xaxis: {
			min: points[0][0],
			max: points[points.length - 1][0] +
			(stats.mode == 'month' ? 86400 * 1000 * 35 : 0),
			mode: "time",
			minTickSize: [1, "day"]
		},
		yaxis: {
			min: 0,
			max: max * 1.03, /** add 3% */
			tickFormatter: function (x) {
				return x + " MB";
			},
			labelWidth: 60
		},
		zoom: {
			interactive: true
		},
		grid: {},
		pan: {
			interactive: true
		}
	});
}