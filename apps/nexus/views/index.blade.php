<form method="post">
	<fieldset class="">
		<a href="{{ \Template_Engine::app('nexus') }}/new" class="btn btn-primary ajax-wait ui-action ui-action-label">
			<i class="fa fa-plus"></i>
			Create Site
		</a>
	</fieldset>
</form>
<hr/>
@includeWhen(!empty($Page->listAccounts()), 'partials.search-accounts')
<form method="post" id="accountManagement">
	@includeWhen(!empty($Page->listAccounts()), 'partials.list-accounts')
	@includeWhen(empty($Page->listAccounts()), 'partials.first-run')
</form>