<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace apps\usermanage\models;

use Template\Searchlet;

class Search extends Searchlet
{
	public function specs(): array
	{
		$vars = [
			'username'   => 'Username',
			'gecos'      => 'Full Name'
		];
		if (\cmd('email_configured')) {
			$vars['mail'] = 'Mail';
		}

		if (cmd('ftp_enabled')) {
			$vars['ftp'] = 'FTP';
		}

		if (DAV_ENABLED) {
			$vars['dav'] = 'DAV';
		}

		$vars['www'] = 'Web';
		$vars['cp']  = 'CP';
		if (\cmd('ssh_enabled')) {
			$vars['ssh'] = 'SSH';
		}

		return $vars;
	}

	public function operations(): array
	{
		return parent::operations() + append_config([
			'service-enabled'  => 'enabled',
			'service-disabled' => 'disabled'
		]);
	}

	public function filterClass(): string
	{
		return 'filter-users';
	}


}

