@include('base')

<p class="mt-3 text-center">
	Refer to the <a href="{{ \Template_Engine::app('sitemap') }}" class="ui-action-app-index ui-action ui-action-label">application index</a>
	for an index of applications.
</p>