<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace apps\webapps\models;

use Module\Support\Webapps\App\Type\Unknown\Handler;

enum STATE {
	case PRESENT;
	case FAILED_INSTALL;
	case FAILED_UPDATE;
	case PENDING_INSTALL;
};

class AppState
{
	public function __construct(private readonly Handler $app)
	{ }

	public function installing(): bool
	{
		return $this->app->isInstalling();
	}

	public function installPending(): bool
	{
		return null !== $this->overrideType();
	}

	public function present(): bool
	{
		return $this->app->applicationPresent();
	}

	public function overrideType(): ?string
	{
		return array_get($_GET, 'app');
	}
}