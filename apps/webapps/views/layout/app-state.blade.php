@if (!empty($meta['failed']))
	@includeWhen(!($present = $app->applicationPresent()), 'partials.failed-install')
	@includeWhen($present, 'partials.failed-update')
@else
	@if ($state->installPending() && $state->present())
		@include('partials.detection-required')
	@elseif ($state->installing())
		<h4 class="d-flex">Installation in progress</h4>
		<div class="alert alert-info">
			We'll send a confirmation to <strong>{{ $app->getOption('email', $app->common_get_email()) }}</strong> once it completes.
		</div>
	@endif
@endif