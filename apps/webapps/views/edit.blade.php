<?php
/** @var \apps\webapps\Page $Page */
/** @var \Module\Support\Webapps\App\Unknown $app */
/** @var \apps\webapps\models\AppState $state */
?>
<div class="row">
	<div class="col-12 text-right">
		<label>Change Mode:</label>
		<a class="ui-action ui-action-switch-app ui-action-label"
		   href="{!! \HTML_Kit::new_page_url_params(null, array('hostname' => null, 'path' => null)) !!}">Back to Site
			Listing</a>
	</div>
</div>

@php
$formAction = \HTML_Kit::page_url_params();
$meta = $app->getApplicationInfo();
@endphp

@include('layout.app-state', ['app' => $app])

<form method="post" action="{{ $formAction }}" class="" data-toggle="validator">
<div class="row">
	<!-- app meta container -->
	<div class="col-12 push-sm-6 col-sm-6 push-md-8 col-md-4 text-center">
		@include('partials.app-meta', [
			'appPath' => ($state->present() && $app->getModuleName()) ? $app->getAppRoot() : $app->getDocumentMetaPath()
		])
		@includeWhen(!$app->getModuleName(), 'partials.options-wrapper')
	</div>

		<div class="actions col-12 pull-sm-6 col-sm-6 pull-md-4 col-md-8">
			@if (!$state->installPending())
				<h4 class="d-flex align-content-around align-items-center header-actions">
					Actions
					<button type="button" class="ml-auto btn-sm btn btn-outline-secondary" data-toggle="collapse"
				        data-target="#helpTopics">
						<i class="fa-question fa"></i>
						Help
					</button>
				</h4>

				<hr />
			@endif

			<div class="row">
				<div class="actions-list col-12 mb-1">
					<div id="helpTopics" class="collapse" data-toggle="collapse">
						@include ('partials/help', ['app' => $app])
					</div>
					@includeWhen(!$state->installPending() || $state->installing(), 'partials.actions', ['app' => $app])
				</div>
			</div>

			@if (!$app->applicationPresent() && !$app->isInstalling() && !$app->hasManifest())
				<h4 class="d-flex">
					Install application @if($state->installPending()) &ndash; {{ $pane->getLocation() }} @endif
				</h4>
				<hr />
				@include('partials.install-app-grid')
			@endif
		@includeIf("@webapp({$app->getModuleName()})::extras")
		@includeWhen($state->present() && !$state->installing(), 'partials.options-wrapper')
	</div>
</div>
</form>