<div class="alert alert-warning">
	<form method="post" action="{!! $formAction !!}">
		<p>
			This application failed to install properly. We've sent an email to
			<strong>{{ $app->getOption('email', $app->common_get_email()) }}</strong>
			with additional information.
		</p>
		<button type="submit" class="btn btn-secondary ui-action ui-action-label" name="clear"
				value="{{$app->getDocumentMetaPath()}}">
			<i class="fa fa-check"></i>
			Dismiss
		</button>
	</form>
</div>