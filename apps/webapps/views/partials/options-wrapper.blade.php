<div class="app-options mb-3">
	<div id="options" class="mt-3 collapse @if (!$state->present()) show @endif row">
		<h4 class="col-12 mb-0">Options</h4>
		<div class="col-12 text-left">
			<hr/>
			@include('partials.options')
		</div>
		<div class="col-12 mt-2  text-left">
			@if ($state->present() || !$state->installPending())
				<button type="submit" name="options" value="1" class="btn btn-primary">
					Save Changes
				</button>
			@endif
		</div>
	</div>
</div>
