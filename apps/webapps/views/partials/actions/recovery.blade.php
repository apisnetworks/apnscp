<button name="recovery" data-note-target="#recoveryNote" type="submit" data-toggle="confirm"
        class="mb-3 btn btn-secondary warn" name="recovery" value="{{$app->getAppRoot()}}" id="recovery"
		data-message="Recovery will disable all plugins and reset theme to default while preserving existing data. Do you want to proceed?">
	<i class="fa fa-life-ring"></i>
	Recovery Mode
</button>