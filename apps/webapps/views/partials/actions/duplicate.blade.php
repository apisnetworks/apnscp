<button data-note-target="#duplicateHelp" type="submit" class="mb-3 btn btn-secondary" id="duplicate"
        name="duplicate" value="{{ $app->getDocumentMetaPath() }}">
	<i class="ui-action ui-action-label ui-action-clone ui-action-d-compact"></i>
	Duplicate Site
</button>

@include('partials.modals.duplicate')