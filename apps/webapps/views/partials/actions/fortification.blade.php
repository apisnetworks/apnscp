<div class="btn-group mb-3" role="group">
	<button data-note-target="#fortifyNote" id="fortify" type="button"
	        class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
	        aria-haspopup="true" aria-expanded="false">
		<i class="fa fa-shield"></i> Change Fortification
	</button>
	<div class="dropdown-menu" aria-labelledby="fortify">
		@if ($app->hasFortification())
			@foreach ($app->fortificationModes() as $mode)
				<button type="submit" name="fortify[{{ $mode }}]"
			        class="btn-block dropdown-item" href="#">
					<i class="fa fa-lock"></i>
					Fortify <b>{{ strtoupper($mode) }}</b> @includeWhen($Page->modeActive($mode, $app), 'partials.fortification-mode-active')
				</button>
			@endforeach
		@endif

		<button type="submit" name="unfortify" class="btn-block dropdown-item"
		        href="#">
			<i class="fa fa-unlock"></i> Release
			Fortification @includeWhen($Page->modeActive(false, $app), 'partials.fortification-mode-active')
		</button>

		@unless ($app->hasFortification())
			<button type="submit" name="fortify[learn]" class="btn-block dropdown-item" data-toggle="wa-duration"
			        href="#">
				<i class="fa fa-compass"></i>
				<span class="title">Learning Mode</span>
				@includeWhen($Page->modeActive('learn', $app), 'partials.fortification-mode-active')
			</button>
		@endunless

		<div class="dropdown-divider"></div>
		<button type="submit" name="fortify[write]" class="btn-block dropdown-item" data-toggle="wa-duration"
		        href="#">
			<i class="fa fa-exclamation"></i>
			<span class="title">Web App Write Mode</span>
			@includeWhen($Page->modeActive('write', $app), 'partials.fortification-mode-active')
		</button>

		<button type="submit" name="fortify[reset]" class="btn-block dropdown-item warn"
		        href="#">
			<i class="ui-action ui-action-danger"></i>
			Reset Permissions
			@includeWhen($Page->modeActive('reset', $app), 'partials.fortification-mode-active')
		</button>

	</div>
</div>
@include('partials.modals.write-mode')