@if ($app->allowGit())
	<div class="form-group">
		<label class="custom-control custom-checkbox form-group mb-1 mr-0 d-block">
			<input type="hidden" name="git" value="0"/>
			<input type="checkbox" name="git"
			       class="custom-control-input form-check-input" value="1"
			       @if (array_get($app->getOptions(), 'git', false)) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			Enable snapshots
			&ndash;
			<span class="ui-action-tooltip" data-toggle="tooltip"
			      title="Use git to create checkpoints.">
            <abbr class="small text-uppercase">Help</abbr>
        </span>
		</label>
	</div>
@endif
<div class="form-group">
	<label class="custom-control custom-checkbox form-group mb-1 mr-0 d-block">
		<input type="hidden" name="empty" value="0"/>
		<input type="checkbox" name="empty"
		       class="custom-control-input form-check-input" value="1"
		       @if (array_get($app->getOptions(), 'empty', false)) checked="CHECKED" @endif />
		<span class="custom-control-indicator"></span>
		Empty {{ $app->getDocumentRoot() }} before install
		&ndash;
		<span class="ui-action-tooltip" data-toggle="tooltip"
		      title="All files within this directory will be removed.">
            <abbr class="small text-uppercase">Help</abbr>
        </span>
	</label>
</div>
@if ($versions = $app->getInstallableVersions())
	<div class="form-group">
		<label>Version</label>
		<select name="version" class="form-control custom-select">
			@php
				$selected = false;
			@endphp
			@foreach (array_reverse($versions) as $version)
				{{-- WordPress releases new versions as MAJOR.MINOR, asPatch would format as 5.4.0 when 5.4 is preferred --}}
				<option @if (!$selected && $selected = \in_array(\Opcenter\Versioning::asPatch($version), [$version, "{$version}.0"], true))
				        selected="SELECTED" @endif value="{{ $version }}">{{ $version }}</option>
			@endforeach
		</select>
	</div>
@endif
<div class="form-group">
	<label>Email</label>
	<input type="text" class="form-control" name="email"
	       value="{{ \cmd('site_get_admin_email') }}">
</div>