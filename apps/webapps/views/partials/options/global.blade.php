@if (!$app->getPath())
	<label class="custom-control custom-checkbox form-group mb-1 d-block">
		<input type="hidden" value="0" name="http10" class="form-check-input"/>
		<input type="checkbox" value="1" name="http10" class="custom-control-input form-check-input"
		       @if ($app->getOption('http10')) checked="CHECKED" @endif />
		<span class="custom-control-indicator"></span>
		<span class="custom-control-description">Allow HTTP/1.0 connections</span>
		&ndash;
		<span class="ui-action-tooltip" data-toggle="tooltip"
		      title="Disable to reduce spam bots. Disabling may prevent connections behind proxies.">
            <abbr class="small text-uppercase">Help</abbr>
        </span>
	</label>
@endif

@if ($app->allowSsl() || !$app->applicationPresent())
	<label class="custom-control custom-checkbox form-group mb-1 d-block">
		<input type="hidden" name="ssl" value="0"/>
		<input type="checkbox" name="ssl" class="custom-control-input form-check-input" value="1"
		       @if (array_get($app->getOptions(), 'ssl', $app->sslPresent() || $state->installPending())) checked="CHECKED" @endif />
		<span class="custom-control-indicator"></span>
		Enable SSL (https)
	</label>
@endif

@if ($app->hasUpdate())
	<label class="custom-control custom-checkbox form-group mb-1 d-block">
		<input type="hidden" name="autoupdate" value="0"/>
		<input type="checkbox" name="autoupdate" class="form-check-input custom-control-input" value="1"
		       @if (array_get($app->getOptions(), 'autoupdate', true)) checked="CHECKED" @endif />
		<span class="custom-control-indicator"></span>
		Enable auto-updates
	</label>

	<label class="form-check-label form-group  mb-1 d-block">
		<span class="mr-1">Update version lock</span>
		<select name="verlock" class="custom-select">
			<option value="none" @if (!$app->getVersionLock()) selected="SELECTED" @endif>None</option>
			<option value="major" @if ($app->getVersionLock() === 'major') selected="SELECTED" @endif>Major (8.x)
			</option>
			<option value="minor" @if ($app->getVersionLock() === 'minor') selected="SELECTED" @endif>Minor (8.0.x)
			</option>

		</select>
	</label>
@endif

<label class="form-check-label form-group mb-1 d-block">
	<span class="mr-1">User ownership</span>
	<select name="user" class="custom-select">
		@foreach (\cmd('user_get_users') as $user => $pwd)
			<option @if ($user === array_get($app->getOptions(), 'user', \Session::get('username'))) selected="SELECTED"
			        @endif
			        value="{{ $user }}">{{ $user }}</option>
		@endforeach
	</select>
</label>