<form method="post" id="massDetectionForm">
	<div class="btn-group mr-2 my-1">
		<button class="btn btn-secondary ajax-wait" type="button" name="scan-all" id="scanAll">
			<i class="fa fa-eye"></i>
			Scan for apps
		</button>
		<button type="button" class="btn btn-secondary dropdown-toggle"
		        data-toggle="dropdown"
		        aria-haspopup="true"
		        aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<div class="dropdown-menu" aria-labelledby="">
			<button class="ui-action-delete ui-action-label dropdown-item ui-action"
			   type="submit" name="forget" value="true">
				Prune invalid apps
			</button>
		</div>
	</div>

	<button class="btn btn-secondary ajax-wait mr-2 my-1" id="updateAll" disabled type="button" name="scan-all"
	        id="scanAll">
		<i class="ui-action ui-action-update ui-action-d-compact"></i>
		Update all apps
	</button>
	<label class="custom-control custom-checkbox my-1"
	       for="showDetected">
		<input type="checkbox" id="showDetected" class="custom-control-input"
		       name="show-detected" value="1"/>
		<span class="custom-control-indicator"></span>
		Display only detected apps
	</label>
</form>