<div class="app-container mb-3 w-100" id="appGrid">
	@include('master::partials.shared.search', [
		'filter' => new \apps\webapps\models\InstallableAppSearch
	])
	<div class="list-group">
	@foreach ($Page->getKnownApps() as $appName)
		@php
			$handlerName = \Module\Support\Webapps::handlerFromApplication($appName);
			$handler = new $handlerName(null);
		@endphp
		@continue(!$handler->hasInstall())
		<div class="list-group-item app @if ($Page->appSelected($appName)) active @endif align-items-center" data-name="{{ $appName }}">
			<span class="card-img-top text-center app-icon mr-3">
				@includeIf("@webapp($appName)::icon")
			</span>
			<h4 class="card-title text-center mb-0 mr-3" id="install{{$appName}}">
				{{ $handler->getName() }}
			</h4>
			@if ($Page->appSelected($appName))
				<div class="align-self-center ml-auto">
					@if(defined("$handlerName::LINK"))
						<a href="{{ $handlerName::LINK }}"
						   class="btn btn-secondary">
							<i class="fa"></i>
							Product Website
						</a>
					@endif
					<button data-note-target="#installNote" type="submit"
					        class="ajax-wait btn btn-primary"
					        name="install[{{ $appName }}]" value="{{ $app->getDocumentMetaPath() }}">
						<i class="fa"></i>
						Install Application
					</button>

				</div>
				<div class="col-12 px-0">
					<hr class="my-2"/>
					<h5 class="mb-3">Install options</h5>
					@include('partials.options.install.global')
					@includeIf("@webapp($appName)::options-install")
					@include('partials.options')
				</div>
			@else
				<a class="btn btn-secondary ajax-wait ml-auto"
				   href="{!! \HTML_Kit::page_url_params(['app' => $appName]); !!}#install{{$appName}}">
					<i class="fa"></i>
					Select Application
				</a>
			@endif

		</div>
	@endforeach
	</div>
</div>