<div class="alert alert-warning">
	<p>
		This application failed its last auto-update. Ensure that your application is installed
		correctly. To clear this message, attempt another update. Upon completion, your application
		will be eligible for automatic updates once again.
	</p>
	<form method="post" action="{!! $formAction !!}">
		<button type="submit" id="updateNote"
				class="ajax-wait btn btn-secondary ui-action ui-action-label" name="update"
				value="{{$app->getDocumentMetaPath()}}">
			<i class="ui-action ui-action-update"></i>
			Update
		</button>
		<button type="submit" class="btn btn-secondary ui-action ui-action-label" name="clear"
				value="{{$app->getDocumentMetaPath()}}">
			<i class="fa fa-check"></i>
			Reset Failed Status
		</button>
	</form>

</div>