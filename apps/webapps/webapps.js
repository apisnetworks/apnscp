$(window).on('load', function () {
	$('.ajax-wait').ajaxWait();
	$('[data-toggle="tooltip"]').tooltip();
	if (__WA_MODE === 'list') {
		bindList();
	} else {
		bindEdit();
	}
});
var $filters;

$(document).ready(function() {
	$('.filter-accounts').bind('click change', function (e) {
		do_filter(e);
		return false;
	}).bind('keydown', function (e) {
		if (e.keyCode === $.ui.keyCode.ESCAPE) {
			$('#reset_filter').click();
			$('#filter').val('');
		} else if (e.keyCode === $.ui.keyCode.ENTER) {
			$(this).triggerHandler('click');
		}
	});

	$filters = $('#searchop').clone();
	update_filters();

	$('#appGrid .filter-apps :text').on('keyup', function (e) {
		var val;
		switch (e.keyCode) {
			case $.ui.keyCode.ENTER:
				return false;
			case $.ui.keyCode.ESCAPE:
				$(this).val("").blur();
				break;
			default:
				val = $(this).val().trim().toLowerCase();
				break;

		}
		$('#appGrid .app').each(function () {
			if (!val || -1 !== $(this).data('name').indexOf(val)) {
				$(this).show();
			} else {
				$(this).hide();
			}
		})
	}).triggerHandler('keyup');
});

function bindList() {
	$('#massDetectionForm').on('click change', function (e) {
		switch (e.target.id) {
			case 'scanAll':
				var a = document.createElement('a'),
					queue = [];
				$('.item.hostname').each(function () {
					var $a = $(this).find('a[data-docroot]'), $row = $(this).closest('.ui-webapp-panel'),
						hostname = $a.data('hostname'), path = $a.data('path');
					$row.find('.status-indicator').remove();
					return $.ajaxQueue(apnscp.call_app_ajax(session.appId, 'scan', [hostname, path])).done(function (ret) {
						var $status;
						if (!ret) {
							$row.fadeOut();
							return;
						}
						$row.find('.app').addClass('fa fa-' + ret['app']);
						$row.find('.version').text(ret['version']);
						if (ret['current']) {
							$status = $('<i class="ui-ajax-success text-success status-indicator" data-toggle="tooltip" title="Update Installed!"></i>').tooltip();
						} else {
							$status = $('<i class="update-available status-indicator ui-action-d-compact ui-action ui-action-update text-warning" data-toggle="tooltip" title="Update Available"></i>').tooltip();
						}
						$('.version', $row).after($status).siblings('.ui-ajax-indicator').remove();
					});
				});
				$.ajaxQueue().done(function () {
					$('#showDetected').prop('checked', true);
					$('#updateAll').prop('disabled', false);
				});
				break;
			case 'updateAll':
				var a = document.createElement('a'),
					queue = [], $collection = $('.item.hostname:has(.update-available)');
				if (!$collection.length) {
					$.ajax();
					return apnscp.addMessage("All Web Apps Updated!", "success");
				}
				$collection.each(function () {
					var $a = $(this).find('a[data-docroot]'), $row = $(this),
						hostname = $a.data('hostname'),
						type = $(this).find('.app').get(0).className.replace(/.*?fa-([^ ]+)|^.+$/, '$1'),
						$indicator = $(this).find('.update-available').addClass('m-0'),
						errorHandler = function (jqXHR, textStatus, errorThrown) {
							// @todo messy
							$indicator.addClass('ui-ajax-error').attr('original-title', 'Update failed');
							apnscp.addMessage(hostname + ' Failed: ' + errorThrown, 'error', 'Update Failed');
						};
					if (!type) {
						return;
					}
					jqxhr = $.ajaxQueue(apnscp.call_app_ajax(session.appId, 'update', [hostname], {
						indicator: $indicator, beforeSend: function () {
							$indicator.removeClass('fa-' + type + ' ui-ajax-success ui-ajax-error').addClass('ui-ajax-indicator ui-ajax-loading');

						}
					})).done(function (ret, textStatus, jqXHR) {
						$indicator.addClass('ui-ajax-success');
						if (ret) {
							$row.find('.version').fadeOut(function () {
								$(this).text(ret['version']).fadeIn();
							});
							$indicator.removeClass('text-warning');
							/*fadeOut(function() {
							 $(this).removeClass('text-warning').fadeIn();
							 };*/
						} else {
							return errorHandler(jqXHR, textStatus, "Failed to update");
						}
					}).fail(errorHandler).catch(function (e) {
						// ???
					}).always(function () {
						$indicator.removeClass('text-warning ui-ajax-loading');
					});
				});
				break;
			case 'showDetected':
				var selector = '.fa';
				if ($('#' + e.target.id).prop('checked')) {
					$('.ui-webapp-panel:visible').hide();
					$('.ui-webapp-panel').filter(':has(.app-meta .app ' + selector + ')').show();
				} else {
					$('.ui-webapp-panel').show();
				}
				break;
		}
		return true;
	});

	$('.ui-action-folder-browse').click(function () {
		var docroot = $(this).data('docroot'),
			hostname = $(this).data('hostname');

		apnscp.explorer({
			selected: docroot,
			filter: 'filter=file,0;chown,1;show,' + docroot,
			onSelect: function (file, b) {
				$('#selected_dir').text(file);
			}
		});
		$('#select_dir').click(function () {
			$('#modal').modal('hide');
			var url = apnscp.getUrl(), scheme = {
				path: $('#selected_dir').text().substring(docroot.length + 1),
				hostname: hostname
			};
			window.location.href = url.replace(/\?.*$/, '') + '?' + $.param(scheme);
			return false;
		});
		return false;
	});
};

function bindEdit() {
	var cleanup = function () {
		$('.delete-db,.dbname').remove();
	};
	$('[data-toggle=confirm]').on('click', function (e) {
		var process = e.target.dataset._confirmed || false;

		if (process) {
			return true;
		}
		var dialog = apnscp.modal($('<span>').text($(this).data('message')), {
			buttons: [
				$('<button class="btn btn-primary">').text("OK").on('click', function() {
					e.target.dataset._confirmed = true;
					$(e.target).trigger('click');
					$(this).empty().append($('<i />').addClass('ui-ajax-indicator ui-ajax-loading'));
					return false;
				})
			]
		});
		dialog.modal('show');
		return false;
	});

	var $items = $('.actions-list > *');
	$items.each(function(i, k) {
		if (k.nodeName.toUpperCase() !== 'H5') {
			return;
		}
		if (++i >= $items.length || $items[i].nodeName.toUpperCase() === 'H5') {
			$(this).hide();
		}

	})

	$('#git-checkpoint').click(function (e) {
		e.preventDefault();
		$('#createModal').modal('show').on('shown.bs.modal', function () {
			$(this).find(':text').focus();
		});
	});

	$('[data-toggle="wa-duration"]').click(function () {
		var title = $(this).find('.title').text(), name = $(this).attr('name');
		$('#writeModal').modal('show').one('shown.bs.modal', function () {

			$('#writeModal').find(':submit').attr('name', name).end().
				find('h5').text(title).end().find('form').validator();
		});
		return false;
	});

	$('#git-manage').click(function () {
		importDialog($(this).val());
		return false;
	});

	$('#migrate').click(function() {
		return migrateDialog($(this).val());
	});

	$('#duplicate').click(function () {
		return duplicateDialog($(this).val());
	});

	if (getNestedObject(__WA_META, 'type') == undefined && !getNestedObject(__WA_META, 'manifest')) {
		// no meta
		cleanup();
		if ($('#installerCheck').length > 0) {
			$('.app-meta .ui-job-indicator').addClass('running');
			pollInstaller();
		}
		return;
	}
	var fn = appType = __WA_META['type'],
		hostname = __WA_META['hostname'],
		path = __WA_META['path'] || "";

	var ajax = apnscp.call_app(null, 'db_config_safe', [appType, hostname, path]).done(function (ret) {
		if (!ret || !ret['db']) {
			cleanup();
			return;
		}

		$('.app-meta .dbuser').text(ret['user']);
		$('.ui-app-container .dbname').each(function () {

			switch ($(this).get(0).nodeName.toUpperCase()) {
				case "INPUT":
				case "BUTTON":
					$(this).val(ret['db']);
					break;
				case "A":
					var url = apnscp.appUrl(ret['type'] === 'pgsql' ? 'phppgadmin' : 'phpmyadmin');
					$(this).attr('href', $(this).attr('href').replace(/%(?:(URL)|(DB))%/g, function (str, p1, p2) {
						if (p1) {
							return url;
						}
						return ret['db'];
					}));
					break;
				default:
					$(this).text(ret['db']);
					break;
			}
			$(this).removeClass('disabled');
		});
	}).fail(function () {
		cleanup();
	});
}


function migrateDialog(path) {
	$('#migrateModal').modal().on('shown.bs.modal', function () {
		var siteUrl = $('#migrateModal').find('.site-url').text();
		apnscp.call_app(null, 'getReconfigurable', [__WA_META['docroot'], 'migrate']).done(function (e) {
			siteUrl = e;
			$('#migrateModal').find('.site-url').text(e);
		}).then(function () {
			apnscp.call_app(null, 'multihomeHostname', [__WA_META['hostname']]).done(function (hosts) {
				var $select = $('#migrateModal select[name=migrate]');
				$select.empty();
				hosts.forEach(function (host) {
					var attrs = {
						value: host + "/" + __WA_META['path']
					}
					if (siteUrl === host || 0 === siteUrl.indexOf(host + '/')) {
						attrs.selected = "SELECTED";
					}
					$select.append($('<option>').attr(attrs).text(host));
				})

				$('#migrateModal .ui-ajax-indicator').remove();
				$('#migrateModal').find('.body').addClass('show');
			});

		});

	});

	return false;
}

function duplicateDialog(path) {
	$('#duplicateModal').modal().on('shown.bs.modal', function () {
		var siteUrl = $('#duplicateModal').find('.site-url').text();
		apnscp.call_app(null, 'getReconfigurable', [__WA_META['docroot'], 'migrate']).done(function (e) {
			siteUrl = e;
			$('#duplicateModal').find('.site-url').text(e);
		}).then(function () {
			apnscp.call_app(null, 'mergeHostsApps').done(function (hosts) {
				console.log(hosts);
				var $select = $('#duplicateModal select[name=duplicate]');
				$select.empty();
				for (var path in hosts) {
					var meta = hosts[path];
					var host = meta.hostname;
					if (-1 == host.indexOf('.')) {
						host += "." + session.domain;
					}
					host += ("/" + meta.path).replace(/\/$/, '');
					var attrs = {
						value: host
					}
					if (siteUrl === host) {
						attrs.selected = "SELECTED";
					}
					$select.append($('<option>').attr(attrs).text(host));
				}

				$('#duplicateModal .ui-ajax-indicator').remove();
				$('#duplicateModal').find('.body').addClass('show');
			});

		});

	});

	return false;
}

function importDialog(path) {
	apnscp.call_app(null, 'listCommits', [path]).done(function (ret) {
		var $entries = [], commits = ret;
		for (var i in commits) {
			var commit = commits[i];
			var snapshot = '<i class="mx-1 ui-action ui-action-snapshot ui-action-label"></i>';
			var $frag = $("<li class='list-inline mb-2'></li>").append([
					$('<label class="custom-control custom-radio font-weight-normal" title="' +
						commit.hash + '">').tooltip().text(commit.subject).
					prepend([
						$('<input />').attr({
							'name': 'git-commit',
							value: commit.hash,
							'class': 'custom-control-input',
							'type': 'radio'
						}).prop('required', true),
						$('<span class="custom-control-indicator"></span>'),
						snapshot
					]),
					$('<span class="d-block snapshot-date ml-4 small text-muted">').text(new Date(commit.ts * 1000))
				]
			);
			$entries.push($frag);
		}
		$('#backup-list').empty().append($entries);
		if ($entries.length > 0) {
			$('#backup-list + .note').hide();
		} else {
			$('#backup-list + .note').show();
		}
		$('#importModal').modal('show').one('shown.bs.modal', function () {
			$('#importModal').find('form').validator();
		});

	});
	var reqColl = [], dfd;
	$('#importModal form').unbind('.modal').on('click.modal', function (e) {

		var $el = $(e.target), $ajax = $('#importModal .ajax-response'),
			$indicator = $ajax.siblings('.btn-primary').find('.fa');
		if ($el.hasClass('restore')) {
			$(reqColl).prop('required', true);
			$ajax.empty();
			// call once to avoid multiple binds
			$(this).closest('form').validator('validate').one('submit', function (e) {
				var styleClasses = 'ui-ajax-indicator ui-ajax-loading';
				if (e.isDefaultPrevented()) {
					return false;
				}
				e.preventDefault();
				dfd = $.Deferred();
				$indicator.addClass(styleClasses);
				apnscp.call_app(null, 'on_postback', $(this).serializeArray()).done(function () {
					// check extension, decompress as necessary
				}).fail(function (xhr) {
					dfd.reject(xhr);
				});
				dfd.done(function () {
					$ajax.removeClass('text-danger').addClass('text-success').text("Success!");
					setTimeout(function () {
						$('#importModal').modal('hide');
					}, 2500);
				}).fail(function (xhr) {
					var resp = JSON.parse(xhr.responseText);
					$ajax.removeClass('text-success').addClass('text-danger').text("failed: " + (resp.errors[0] || "general failure"));
				}).always(function () {
					$indicator.removeClass(styleClasses);
				});
				return false;
			}).submit();
			return false;
		}
	}).on('submit.modal', function (e) {
		if (reqColl.length) {
			return true;
		}
		if (e.isDefaultPrevented()) {
			return false;
		}
		return dfd;
	});

}

/**
 * Check installation task
 */
var timeout;

function pollInstaller() {
	apnscp.call_app('webapps', 'appStatus', [apnscp.getParameterByName('hostname'), apnscp.getParameterByName('path')]).done(function (v) {
		if (v !== "") {
			// installed or failed, refresh
			clearTimeout(timeout);
			setTimeout(function() {
				// let things clean up
				$('.app-meta .ui-job-indicator').removeClass('running');
				window.location.reload();
			}, 1500);
			return;
		}
		timeout = setTimeout(function () {
			return pollInstaller();
		}, 750);
	});
}

function do_filter(e) {
	if (e.type == 'change' && e.target.id == 'filter_spec') {
		return update_filters();
	} else if (e.target.id == "do_filter") {
		var field = $('#filter_spec').val(),
			patternbase = $('#filter').val(),
			op = $('#searchop').val(), re,
			pattern = patternbase.trim(),
			/** base regex comparator */
			comparator;

		switch (op) {
			case 'contains':
				break;
			case 'notcontains':
				pattern = '^(?!.*' + pattern + ').*$';
				break;
			case 'is':
				pattern = pattern.toLowerCase();
				comparator = function (field) {
					return $(this).data(field).toString().trim().toLowerCase() != pattern;
				};
				break;
			case 'isnot':
				pattern = pattern.toLowerCase();
				comparator = function (field) {
					return $(this).data(field).toString().trim().toLowerCase() == pattern;
				};
				break;
		}
		// some switches may define their own comparator
		// if none is set revert to the regex-based comparator
		if (!comparator) {
			comparator = function (field) {
				re = new RegExp(pattern, "i");
				return re.test($(this).data(field).toString().trim()) == false;
			}
		}

		$('.item.hostname').filter(function () {
			if (!comparator.call(this, field)) {
				var $dataField = $('.attributes .attribute.' + field, this);
				$dataField.data('refcnt', (parseInt($dataField.data('refcnt')) || 0) + 1);
				$dataField.removeClass('d-none');
				return false;
			}
			return true;
		}).closest('.ui-webapp-panel').fadeOut('fast', function () {
		});

		var $el = $('<li class="mr-3"/>').addClass("label ui-filter-spec " +
			'filter-comp-' + op + ' filter-' + field).text(patternbase).click(function () {
				return remove_filter.apply(this, [$(this).data('comparator'), $(this).data("type")]);
		}).data({
			type: field,
			comparator: comparator
		});

		$('ul.ui-active-filters').append($el);

		if ($('#reset_filter:hidden').length > 0) {
			$('#reset_filter').removeClass('hide').get(0).style = '';
		}

		return false;
	} else if (e.target.id == "reset_filter") {
		$('.filter-accounts ul.ui-active-filters').empty();
		$('#reset_filter').hide();
		$('.item.hostname .account-attributes .attribute').data('refcnt', 0).addClass('d-none');
		$('.item.hostname:hidden').closest('.ui-webapp-panel').fadeIn('fast');
		return false;
	}

	return true;
}

/**
 * update filter operations
 */
function update_filters() {
	var $el = $('#filter_spec');
	$('#searchop').replaceWith($filters.clone());

	if ($.inArray($el.val(), []) >= 0) {
		$('#searchop option').each(function (e) {
			var val = $(this).val();
			if (val != 'service-enabled' && val != 'service-disabled') {
				$(this).remove();
			}
		});
		$('#filter').fadeOut();
	} else {
		if ($('#filter:hidden')) {
			$('#filter').fadeIn();
		}
	}

	return true;
}

/**
 * Remove filter and relist hidden items
 */
function remove_filter(comparator, field) {
	if ($('ul.ui-active-filters').length === 1) {
		$('#reset_filter').click();
		$(this).remove();
		return true;
	}

	$('.item.hostname').filter(function () {
		var $attrData = $('.attributes .attribute.' + field, this),
			refcnt = parseInt($attrData.data('refcnt') || 0);
		if (!comparator.call(this, field)) {
			if (!refcnt) {
				$attrData.addClass('d-none');
			}
			$attrData.data('refcnt', --refcnt);
			return true;
		}
		return false;
	}).fadeIn('fast');

	$(this).remove();
	return false;
}