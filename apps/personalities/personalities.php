<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	//namespace apps\personalities;

	namespace apps\personalities;

	use Module\Support\Webapps;
	use Page_Container;

	class Page extends Page_Container
	{
		protected $hostname;
		// active personality target
		protected $path;
		private $_mode = 'list';
		private $_target;
		private $fspath;

		public function __construct()
		{
			// use v2 layout with bootstrap
			parent::__construct();
			// @todo maybe add hooks?
			$this->add_css('personalities.css');
			$this->add_javascript('personalities.js');
			$this->init_js('browser');
		}

		public function on_postback($params)
		{
			if (!isset($params['hostname'])) {
				return;
			}
			if (count($params) < 2) {
				$this->hide_pb();
			}
			$this->hostname = $params['hostname'];
			$this->path = array_get($params, 'path', null);
			$this->_mode = 'edit';
			$this->_target = $this->hostname;
			if (isset($params['save'])) {
				// removed all lines, unlink the .htaccess file
				if (!isset($params['lines'])) {
					return $this->file_delete($this->getControlFile());
				}
				return $this->personality_commit([$this->hostname, $this->path],
					$params['hash'],
					trim(implode("\n", array_filter($params['lines'])), "\n") . "\n"
				);
			}
		}

		public function getControlFile(): ?string
		{
			if (null === $this->getPersonalityPath()) {
				return null;
			}
			return implode(DIRECTORY_SEPARATOR,
				array(
					$this->getPersonalityPath(),
					\Personality_Module::CONTROL_FILE
				)
			);
		}

		public function getPersonalityPath()
		{
			if (null === $this->fspath && $this->hostname) {
				$target = $this->hostname;
				$root = rtrim($this->web_get_docroot($target) . '/' . $this->path, '/');
				$this->fspath = $root;
				if (!$root) {
					// @XXX to-fix
					return error("Personality path cannot be found for host `%s'", $target);
				}
			}

			return $this->fspath;
		}

		public function getMode()
		{
			return $this->_mode;
		}

		public function getPersonalityTarget()
		{
			if ($this->_mode == 'edit') {
				return rtrim($this->hostname . '/' . $this->path, '/');
			}
		}

		/**
		 * Get verification hash
		 *
		 * @param null $obj
		 * @return mixed
		 */
		public function getHash($obj = null)
		{
			if (null === $obj) {
				$obj = $this->getControlFile();
			}

			return $this->personality_hash($obj);
		}

		public function getPersonalityApp()
		{
			// unconditionally loads full framework, meh
			$app = Webapps\App\Loader::fromHostname(null, array_get($_GET, 'hostname'), array_get($_GET, 'path', ''), $this->getAuthContext());
			return (string)$app;
		}

		public function _render()
		{
			// check personality for valid path first
			if ($this->_mode === 'edit') {
				$this->getPersonalityPath();
			}
		}

		public function scan($file)
		{
			$parser = $this->personality_scan($file);
			if (!$parser) {
				return $parser;
			}

			return $parser;
		}

		public function parseLine(\Tivie\HtaccessParser\Token\BaseToken $p, $lineno)
		{
			$html = '<input type="hidden" name="lines[]" data-line="' . $lineno . '" value="' . htmlentities($p,
					ENT_QUOTES) . '" />';
			$dataline = 'data-line="' . $lineno . '"';
			if ($p instanceof \Tivie\HtaccessParser\Token\WhiteLine) {
				return "<li $dataline class='hidden'>" . $html . '</li>';

			}
			$resolve = $this->personality_resolve($p->getName());
			$classes = 'p-' . $resolve . ' input-group';
			$buttons = '';
			// no support for nested blocks yet
			if (null !== $lineno) {
				$buttons = '<div class="btn-group"><button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="sr-only">Toggle Dropdown</span>
            </button>' .
					'<div class="dropdown-menu dropdown-menu-left">' .
					'<a title="edit" rel="' . $lineno . '" class="ui-action dropdown-item ui-action-edit ui-action-label" name="" value="">Edit</a>' .
					'<div class="dropdown-divider"></div>' .
					'<a title="delete" rel="' . $lineno . '" class="ui-action dropdown-item ui-action-delete ui-action-label" ' .
					'>Delete</a>' .
					'</div></div>';
			}

			$safename = htmlentities($p->getName());
			$safeargs = htmlentities(implode(" ", $p->getArguments()));

			if ($p instanceof \Tivie\HtaccessParser\Token\Block) {
				return "<li $dataline class='block $classes'> $buttons $html $safename $safeargs </li>";
			} else if ($p instanceof \Tivie\HtaccessParser\Token\Directive) {
				return "<li $dataline class='directive $classes'> $buttons $html " . (!$resolve && is_debug() ? ' <b style="color:red;">!!!</b> ' : '') .
					"$safename " . htmlentities(implode(" ", $p->getArguments())) . '</li>';
			} else if ($p instanceof \Tivie\HtaccessParser\Token\Comment) {
				return "<li $dataline class='comment $classes'>" . $buttons . ' ' . $safeargs . '</li>';
			}

			return "<li>Unknown ? " . get_class($p) . "</li>";
		}

		public function remove($config, $line)
		{
			return $this->personality_remove($config, $line);
		}

		public function add($config, $offset, $directive, $val = null)
		{
			return $this->personality_insert($config, $offset, $directive);
		}

		public function replace($config, $line, $newdirective, $newval)
		{
			return $this->personality_replace($config, $line, $newdirective, $newval);
		}

		public function getPersonalities()
		{
			return $this->personality_get_personalities();
		}

		public function getDirectives($personality)
		{
			return $this->personality_get_directives($personality);
		}

		public function getDescription($personality, $directive)
		{
			$desc = $this->personality_get_description($personality, $directive);

			return $desc ? $desc : '<em>no help available</em>';
		}

		private function init_hostname_map()
		{
			$hostnames = $this->get_hostnames();

			$js = 'var host_map = {subdomain: {';
			$maps = array();
			foreach ($hostnames['subdomain'] as $subdomain => $domains) {
				$maps[] = "'" . $subdomain . "'" . ': [' .
					trim("'" . implode("','", $domains), ",") . "']";
			}
			$js .= implode(',', $maps) . '}, domain: { ';
			$maps = array();
			foreach ($hostnames['domain'] as $domain => $subdomains) {
				$maps[] = "'" . $domain . "'" . ': [' .
					trim("'" . implode("','", $subdomains), ",") . "']";
			}
			$js .= implode(',', $maps) . '}};';
			$this->add_javascript($js, 'internal', false);
		}

		public function get_hostnames()
		{
			Webapps::getAllHostnames($this->getAuthContext());
			\Util_Conf::sort_domains($hosts, 'assoc', true);

			return $hosts;
		}

		public function get_aliases()
		{
			$aliases = \Util_Conf::all_domains();
			\Util_Conf::sort_domains($aliases);

			return $aliases;
		}
	}