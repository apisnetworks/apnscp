<h3>Hostnames</h3>
@php
	$kernel = Page_Container::kernelFromApp('webapps');
	$wapage = Page_Container::init($kernel);
	$proxy = \Module\Support\Webapps\App\UIPanel::instantiateContexted(\Auth::profile());
@endphp
@include('master::partials.shared.search', [
	'filter' => new \apps\personalities\models\Search
])
<hr/>
<div class="row ui-webapp-panel">
@foreach ($wapage->mergeHostsApps() as $docroot => $meta)
	@continue(empty($docroot))

	@php
		$pane = $proxy->get($meta['hostname'], $meta['path'] ?? null);
		$href = \HTML_Kit::new_page_url_params(null, [
			'hostname' => $pane->getHostname(),
			'path'     => $pane->getPath()
		]);
	@endphp
	@if (!SCREENSHOTS_ENABLED)
		@include('theme::partials.shared.wa-compact', [
					'pane'    => $pane,
					'Page'    => $wapage,
					'actions' => [
						'view' => 'partials.actions',
						'dropdownLocation' => 'dropdown-menu-right'
					]
			])
	@else
		<div class="col-12 col-xl-3 col-lg-4 col-md-6 ui-webapp-panel">
			<div class="row">
				@include('theme::partials.shared.wa-screenshot', [
					'pane'    => $pane,
					'Page'    => $wapage,
					'actions' => [
						'view' => 'partials.actions',
						'btnGroupClass' => 'btn-group-sm'
					]
				])

			</div>
		</div>
	@endif
@endforeach
</div>