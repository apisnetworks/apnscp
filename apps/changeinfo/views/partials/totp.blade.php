<h4>TOTP Passcodes</h4>
<p>
	Add support for time-based one-time passcodes (RFC 6238) using any compatible device, such as <a
			href="https://www.yubico.com/">YubiKey</a>,
	Google Authenticator (<a href="{{ $Page->var('links.authenticator.google')  }}">Android</a>, <a
			href="{{ $Page->var('links.authenticator.apple')  }}">Apple</a>) or <a href="https://authy.com/">Authy</a>.
</p>
<p><em>Note: TOTP is required to use certain extended functionality in this platform.</em></p>

<div class="mb-3">
	@if (\cmd('auth_totp_exists'))
		<p class="alert-success note">
			TOTP protecting this account
		</p>
		<button class="warn ui-action-label btn btn-primary ui-action-unlock" name="totp" value="0">
			Disable TOTP
		</button>
	@else
		<button class="ui-action-label btn btn-primary ui-action-lock" id="enable-totp" type="button">Enable TOTP</button>
	@endif
		<div id="totp-modal" class="hide">
			<h3>TOTP Key</h3>
			<p>
				The following is your TOTP key. You may scan the QR code below or enter it manually.
				Enter the code below generated from this key to verify TOTP has been correctly added. If a TOTP passkey is lost,
				it may only be removed administratively.
			</p>
			<div class="">
				<span class="d-block text-monospace text-break" style="word-break: break-word; font-size: 1.5em;" id="totp-key"></span>
				<img src="" id="totp-qr-code" alt="QR Code" class="my-3 d-none img-fluid"/>
			</div>

			@include('@app(login)::partials.forms.totp-challenge')
		</div>
</div>