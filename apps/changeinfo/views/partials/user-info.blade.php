<div id="site" class="tab-pane active" role="tabpanel" aria-labelledby="">
	@if (\UCard::get()->hasPrivilege('site'))
		@includeWhen(AUTH_ALLOW_DOMAIN_CHANGE, 'partials.change-domain')
		@includeWhen(AUTH_ALLOW_USERNAME_CHANGE, 'partials.change-username')
		@includeWhen(AUTH_ALLOW_DATABASE_CHANGE && (\cmd('mysql_enabled') || \cmd('pgsql_enabled')), 'partials.change-database')
	@endif
	@if (!UCard::is('admin'))
		<label class="form-control-static">
			Full Name
		</label>
		<fieldset class="form-group">
			@php $gecos = $Page->get_full_name(); @endphp
			<input type="text" name="full_name" class="form-control" id="full_name" value="{{ $gecos }}"/>
			<input type="hidden" name="gecos-challenge" value="{{ $Page->checksum($gecos) }}"/>
		</fieldset>
	@endif

	<label class="form-control-static">
		Email Address
	</label>
	<fieldset class="form-group">
		@php $email = $Page->getEmail(); @endphp
		<input type="text" name="email" class="form-control" id="email" value="{{ $email }}"/>
		<input type="hidden" name="email-challenge" value="{{ $Page->checksum($email) }}"/>
	</fieldset>

	<label class="form-control-static">
		Change Password
	</label>
	<fieldset class="form-group has-feedback row">
		<div class="col-md-12 input-group ">
			<div class="input-group-addon"><i class="fa fa-lock"></i></div>
			<input type="password" class="form-control" name="password" id="password"
			       value="" data-minlength="6" autocomplete="new-password" readonly
			       onfocus="this.removeAttribute('readonly');"/>
		</div>
		<div class="help-block col-12">
			{{ AUTH_MIN_PW_LENGTH }} character minimum
		</div>
	</fieldset>
	<fieldset class="form-group verify-password row has-feedback">
		<label class="col-12">
			Verify New Password
		</label>
		<div class="col-md-12 input-group">
			<div class="input-group-addon"><i class="fa fa-lock"></i></div>
			<input type="password" name="password_confirm" class="form-control"
			       id="password_confirm form-control-error" data-match="#password" value=""/>
		</div>
		<div class="col-12 help-block with-errors text-danger"></div>
	</fieldset>
</div>