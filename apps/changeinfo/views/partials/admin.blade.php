<div id="system" class="tab-pane" role="tabpanel" aria-labelledby="">
	@if (AUTH_ADMIN_API)
		<fieldset class="form-group d-block">
			<label>
				System Hostname
			</label>
			<div class="input-group ">
				<div class="input-group-addon"><i class="fa fa-cloud"></i></div>
				<input type="text" class="form-control" name="hostname" id="hostname"
				       value="{{ gethostname() }}"/>
			</div>
		</fieldset>
		<fieldset class="form-group d-block mb-0">
			<p class="alert-info note">
				If the hostname has changed, SSL certificate will be reissued for the new hostname upon
				restart.
			</p>
			<label class="">
				<button type="submit" name="restart-panel"
		            class="btn-secondary btn mr-3 mt-2 ui-action ui-action-refresh ui-action-label warn">
					Restart {{ PANEL_BRAND }}
				</button>

			</label>
		</fieldset>
		<fieldset class="form-group d-block mb-0">
			<label class="d-block">
				{{ PANEL_BRAND }} Version
			</label>
			{{ \Util_Conf::cp_version('full') }}
			@php
				$latest = Opcenter::latest();
			@endphp
			@if ($latest)
			&mdash;
				@if (version_compare($latest, \Util_Conf::cp_version('full')))
					<span class="text-success">
						<i class="fa fa-check-circle"></i>
						Latest version
					</span>
				@else
					<span class="text-warning">
						<i class="fa fa-exclamation-circle"></i>
						Update available ({{ $latest }})
					</span>
				@endif
			@endif
			@if (AUTH_ADMIN_API)
				<a class="btn btn-secondary mr-3" href="{{ HTML_Kit::new_page_url_params('scopes/apply/cp.update-policy') }}">
					Manage Update Policy
				</a>
			@endif
		</fieldset>
	@endif
</div>