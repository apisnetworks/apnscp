<h4 class="">
	<i class="ui-action ui-action-label ui-menu-category-web d-inline"></i>
	Web Apps
</h4>
<fieldset class="form-group">
	@if (!\UCard::is('admin'))
		<div class="form-check">
			<label class="custom-control custom-checkbox mb-0 align-items-center">
				<input type="hidden" name="pref{{ HTML_Kit::prefify(apps\webapps\Page::LAYOUT_KEY)  }}" value="compact" />
				<input class="form-check-input custom-control-input"
				       @if (\Preferences::get(\apps\webapps\Page::LAYOUT_KEY, SCREENSHOTS_ENABLED ? 'wide' : 'compact') === 'wide') checked @endif
				       type="checkbox" value="wide" name="pref{{ HTML_Kit::prefify(apps\webapps\Page::LAYOUT_KEY)  }}">
				<span class="custom-control-indicator"></span>
				<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip" title="Display screenshots and additional details with sites"></i>
				{{ _("List sites in wide format (screenshot mode)") }}
			</label>
		</div>
	@endif
	<div class="form-check">
		<label class="mb-0 align-items-center">
			Web App update notifications
			@php
				$notificationSetting = \Module\Support\Webapps\Notifier::instantiateContexted(\Auth::profile())->getNotification();
				$email = \Module\Support\Webapps\Notifier::instantiateContexted(\Auth::profile())->getEmail();
			@endphp
			<select name="pref{{ HTML_Kit::prefify(\Module\Support\Webapps\Notifier::PREFKEY) }}" class="form-control custom-select">
				@foreach (\Module\Support\Webapps\Notifier::NOTIFICATION_MODES as $val => $label)
					<option value="{{ $val }}" @if ($val === $notificationSetting) selected="SELECTED" @endif>{{ _($label) }}</option>
				@endforeach
			</select>
		</label>
		<p class="">
			Notifications control how frequently you will receive an email informing you of update status in Web &gt;
			<a href="{{ Template_Engine::app('webapps') }}" class="ui-action ui-action-switch-app ui-action-label">Web Apps</a>.
			@if (\UCard::is('site'))
				Notifications are always sent to the address on account, <b>{{ $email }}</b>.
			@else
				Notifications are always sent to <em>[crm]</em> => <em>copy_admin</em>, <b>{{ $email ?: "UNDEFINED" }}</b>.
				This value may be changed using the <a href="{{ Template_Engine::app('scopes') }}/apply/cp.config" class="ui-action ui-action-switch-app ui-action-label">cp.config Scope</a>.
			@endif
		</p>
	</div>
</fieldset>