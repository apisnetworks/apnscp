<div id="security" class="tab-pane" role="tabpanel" aria-labelledby="">
	<h4>Email Notifications</h4>
	<p>All notifications will be sent to the address on record (<i>{{ $Page->getEmail() }}</i>).
		Your email address may be changed under <b>User Information</b>.
	</p>
	<fieldset class="form-group">
		<label class="block custom-control custom-checkbox align-middle">
			<input type="checkbox" class="custom-control-input" name="notify[login]" value="1"
			       @if ($Page->prefGet('login')) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			CP login from unrecognized source
		</label>

		<label class="block custom-control custom-checkbox align-middle">
			<input type="checkbox" class="custom-control-input" name="notify[passwordchange]" value="1"
			       @if ($Page->prefGet('passwordchange')) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			Password change
		</label>

		@if (UCard::is('site'))
			<label class="block custom-control custom-checkbox align-middle">
				<input type="checkbox" class="custom-control-input" name="notify[usernamechange]" value="1"
				       @if ($Page->prefGet('usernamechange')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				Username change
			</label>

			<label class="block custom-control custom-checkbox align-middle">
				<input type="checkbox" class="custom-control-input" name="notify[domainchange]" value="1"
				       @if ($Page->prefGet('domainchange')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				Primary domain change
			</label>
		@endif
	</fieldset>

	@if (\UCard::is('site'))
		<h4>Service Access</h4>
		<p>
			Limit access to additional services. By default, the Site Administrator is granted access to all services.
			This is useful with TOTP to effectively lockdown an account.
		</p>
		<input type="hidden" name="service-state" value="{{ json_encode(\cmd('user_enrollment', Session::get('username'))) }}" />
		<label class="block custom-control custom-checkbox align-middle">
			<input type="checkbox" class="custom-control-input" name="service[auth][cp]" value="1" checked="CHECKED" disabled />
			<span class="custom-control-indicator"></span>
			Control panel access
			<input type="hidden" name="service[auth][cp]" value="1" />
		</label>
		@if (\cmd('email_configured'))
			<label class="block custom-control custom-checkbox align-middle">
				<input type="hidden" name="service[email][imap]" value="0"/>
				<input type="checkbox" class="custom-control-input" name="service[email][imap]" value="1"
				       @if (\cmd('email_user_permitted', svc: 'imap')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				IMAP access
			</label>

			<label class="block custom-control custom-checkbox align-middle">
				<input type="hidden" name="service[email][pop3]" value="0"/>
				<input type="checkbox" class="custom-control-input" name="service[email][pop3]" value="1"
				       @if (\cmd('email_user_permitted', svc: 'pop3')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				POP3 access
			</label>

			<label class="block custom-control custom-checkbox align-middle">
				<input type="hidden" name="service[email][smtp]" value="0"/>
				<input type="checkbox" class="custom-control-input" name="service[email][smtp]" value="1"
				       @if (\cmd('email_user_permitted', svc: 'smtp')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				SMTP access
			</label>
		@endif

		@if (\cmd('ftp_enabled'))
			<label class="block custom-control custom-checkbox align-middle">
				<input type="hidden" name="service[ftp]" value="0"/>
				<input type="checkbox" class="custom-control-input" name="service[ftp]" value="1"
				       @if (\cmd('ftp_user_permitted')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				FTP access
			</label>
		@endif

		@if (\cmd('ssh_enabled'))
			<label class="block custom-control custom-checkbox align-middle">
				<input type="hidden" name="service[ssh]" value="0"/>
				<input type="checkbox" class="custom-control-input" name="service[ssh]" value="1"
				       @if (\cmd('ssh_user_permitted')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				Terminal access
			</label>
		@endif

		@if (DAV_ENABLED)
			<label class="block custom-control custom-checkbox align-middle">
				<input type="hidden" name="service[auth][dav]" value="0"/>
				<input type="checkbox" class="custom-control-input" name="service[auth][dav]" value="1"
				       @if (\cmd('auth_user_permitted', svc: 'dav')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				DAV access
			</label>
		@endif
		<p></p>
	@endif

	@include('partials.totp')

	@if (\UCard::is('admin') || \Auth\IpRestrictor::instantiateContexted(\Auth::profile())->getLimit())
		<h4>Login Tracking</h4>
		<p>
			Track the last login in this browser. For each successful login from a new IP address, implicitly
			add this IP address to <b>Login Access</b>. This is intended to help track login access from
			dynamic IPs that constantly change.

			<label class="block custom-control custom-checkbox align-middle">
				<input type="checkbox" class="custom-control-input" name="sticky-pref" value="1"
				       @if (\Preferences::get(\Auth\IpRestrictor::STICKY_PREF)) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				Track logins across IP changes
			</label>
		</p>

		<h4>Login Access</h4>
		<p>
			All IPv4, IPv6, and CIDRs listed below are permitted access. Any address <b>not listed</b>
			will <b>not be permitted access</b> to the panel on next login. Before adding any
			address, your active login, {{ \Auth::client_ip() }} will also be added.
		</p>
		<p>
			Do not restrict login access if you do not have access to a static IP address. This affects
			all services panel services including DAV and API.
		</p>
		<div class="row">
			<div class="col-12">
				<form action="{{ HTML_Kit::page_url() }}" method="POST">
					<label class="form-control-label d-block">
						IPv4, IPv6, or CIDR Range
					</label>
					<div class="form-inline">
						<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-globe"></i>
						</span>
							<label for="ipInput" class="hinted">{{ \Auth::client_ip() }}</label>
							<input class="form-control mr-3" maxlength="39" size="39" id="ipInput" name="ip"
							       placeholder="" value=""/>
						</div>
						<button class=" btn btn-primary ui-action ui-action-add ui-action-label bindable" name="add-ip"
						        type="submit">
							Add Entry
						</button>
					</div>
					<ul class="text-danger list-unstyled mt-1" id="errors">
					</ul>

				</form>

				<form method="POST" id="active">
					<div id="ip-entries">
						@foreach (cmd('auth_get_ip_restrictions') as $entry => $gates)
							<div class="row ip-entry">
								<div class="col-12 mb-2">
									<button type="submit" data-ip="{{$entry}}"
									        name="remove-ip" value="{{ $entry }}"
									        class="ui-action warn ui-action-label ui-action-delete btn btn-secondary bindable">
										Delete
									</button>
									{{ $entry }}
								</div>
							</div>
						@endforeach
						<p class="alert-info note">
							Restrictions are not configured on this account. All IP addresses may login to this account.
						</p>
					</div>
				</form>

				<div class="row ip-entry hide" id="item-template">
					<div class="col-12 mb-2">
						<button type="submit" data-ip=""
						        name="remove-ip"
						        value=""
						        class="ui-action warn ui-action-label ui-action-delete btn btn-secondary bindable">
							Delete
						</button>
						<span class="ip"></span>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>