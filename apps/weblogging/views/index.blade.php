<form action=".." method="post">
	<table width="100%">
		<tr>
			<td align='center' class="head5" colspan="4">
				Add Logfile
			</td>
		</tr>
		<tr>
			<td class="head3">Subdomain</td>
			<td class="head3" width="150">Domain</td>
			<td class="head3">Logfile Name</td>
			<td class="head3 center" width="36">Actions</td>
		</tr>
		<tr>
			<td>
				<select name="subdomain" id="subdomains">
					<?php foreach($Page->list_subdomains() as $subdomain => $doc_root):?>
					<option value="<?=$subdomain == 'www' ? '' : $subdomain?>"><?=$subdomain?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td>
				<select name="domain" id="domain">
					<?php foreach($Page->list_aliases() as $alias):?>
					<option value="<?=$alias?>"><?=$alias?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td>
				<label for="location" class="hinted">Example: mydomain_log</label>
				<input type="text" name="location" id="location" size="40" maxlength="40"/>
			</td>
			<td class="center">
				<input type="submit" class="primary add" name="add" value="Add"/>
			</td>
		</tr>


		<tr>
			<td class="head1" align="center">Subdomain</td>
			<td class="head1" align="center" width="150">Domain</td>
			<td class="head1" align="center" width="300">Logfile Name</td>
			<td class="head1 actions center" width="70">Actions</td>
		</tr>
		<?php
  foreach ($Page->list_logfiles() as $domain => $sub_rec):
		foreach ($sub_rec as $subdomain => $file):

		?>
		<tr>
			<td>
				<?php print($subdomain); ?>
			</td>
			<td>
				<?php print($domain); ?>
			</td>
			<td>
				<?php print($file); ?>
			</td>
			<td class="center">
				<a class="ui-action ui-action-details"
				   href="{{ \Template_Engine::app('filemanager') }}?f=<?php print urlencode('/var/log/httpd/'.$file); ?>">view log</a>
				<a class="ui-action ui-action-download"
				   href="{{ \Template_Engine::app('weblogging') }}?download=<?php print urlencode($file); ?>">download log</a>
				<a class="ui-action ui-action-delete"
				   href="{{ \Template_Engine::app('weblogging') }}?d=<?=base64_encode($subdomain.' '.$domain)?>"
				   onClick="return confirm('Are you sure you want to delete this logfile?');">delete</a>
			</td>
		</tr>
		<?php
    endforeach;
  endforeach;
  ?>
	</table>
</form>