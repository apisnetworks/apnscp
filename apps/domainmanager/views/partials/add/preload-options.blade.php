<fieldset class="form-group">
	<label class="form-label">
		<a href="{{ \Template_Engine::app('webapps') }}" class="ui-action ui-action-label ui-action-switch-app">
			Webapp
		</a> Preload</label>
	<select class="form-control custom-select" name="app-preload">
		<option value=""></option>
		@foreach (\Module\Support\Webapps\App\Loader::getKnownApps() as $symbol)
			@php
				$handlerName = \Module\Support\Webapps::handlerFromApplication($symbol);
				$handler = new $handlerName(null);
			@endphp
			@continue(!$handler->hasInstall())
			<option value="{{ $symbol }}">{{ $handler->getName() }}</option>
		@endforeach
	</select>
</fieldset>