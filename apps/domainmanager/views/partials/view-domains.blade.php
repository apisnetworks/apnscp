<form method="post" action="{{ $DEFAULT_FORM_ACTION }}">
	<div class="row">
		<div class="col-12">
			<formset class="form-group form-inline align-items-end" id="domainFilter">
				<div class="flex-column mr-3">
					<label class="ui-action ui-action-label ui-filter justify-content-start">
						Filter By
					</label>
					<select class="custom-select" name="filter_spec" id="filter_spec">
						<option value="domain">Domain</option>
						<option value="owner">Owner</option>
						<option value="path">Path</option>
					</select>
				</div>
				<div class="flex-column mr-3">
					<label class="justify-content-start">
						Filter pattern
					</label>
					<input type="text" class="form-control" name="filter" value=".*" id="filter"/>
				</div>
				<button type="submit" name="do_filter" id="do_filter" class="btn btn-secondary mr-3" value="Filter">
					<i class="fa fa-search"></i>
					Filter
				</button>
				<button type="reset" name="reset_filter" id="reset_filter"
				        class="warn ui-quiet-warn btn btn-secondary mr-3" value="Reset Filters">
					Reset Filters
				</button>
				<ul class="ui-active-filters align-self-end my-0"></ul>
			</formset>
		</div>
	</div>
</form>

<h3>Addon Domains</h3>
<div class="row hidden-sm-down">
	<div class="col-6 col-sm-4 col-md-3 ">Domain</div>
	<div class="col-6 col-sm-4 col-md-3 ">Owner</div>
	<div class="col-6 col-sm-4 col-md-3 ">Path</div>
	<div class="col-6 col-sm-4 col-md-3 ">Actions</div>
</div>

@foreach ($Page->list_shared_domains() as $domain => $info)
	<form method="post" action="{{ $DEFAULT_FORM_ACTION }}">
		<div class="row entry domain-meta" id="row-{{ str_replace('.', '-', $domain) }}">
			<div class="col-12 col-sm-6 col-xl-3 mb-1 domain">
				<div class="input-group mask-readonly">
						<span class="input-group-addon">
							<i class="fa fa-cloud"></i>
						</span>
					<input type="form-control" name="domain" value="{{  $domain }}" class="form-control" readonly="READONLY"/>
				</div>
			</div>

			<div class="col-12 col-sm-6 col-xl-3 mb-1 owner">
				<div class="input-group mask-readonly" data-source="user">
					<span class="input-group-addon @if (!$info['owner']) text-danger @endif">
						<i class="fa @if ($info['owner']) fa-user @else fa-exclamation-triangle @endif" aria-hidden="true"></i>
					</span>
					@if ($info['owner'])
						<input type="form-control" name="user" value="{{  $info['owner'] }}" class="form-control"
						       readonly="READONLY"/>
					@else
						<input type="form-control" name="user" value="Path Missing"
						       class="form-control text-danger font-italic text-uc"
						       readonly="READONLY"/>
					@endif
				</div>
			</div>

			<div class="col-12 col-sm-6 col-xl-3 mb-1 path">
				<div class="input-group mask-readonly" data-source="file">
						<span class="input-group-addon">
							<i class="fa fa-folder"></i>
						</span>
					<input type="text" class="form-control path" name="path" value="{{ $info['path'] }}"
					       readonly="READONLY"/>
				</div>
			</div>

			<div class="actions col-12 col-sm-6 col-xl-3 mb-1 actions">
				<div class="input-group mask-readonly">
					<div class="btn-group">
						<a title="visit site" class="btn btn-secondary ui-action-label ui-action ui-action-visit-site"
						   href="http://{{ $domain }}/">
							Visit
						</a>
						<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
						        aria-haspopup="true" aria-expanded="false">
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a title="App Manager"
							   href="{{ \HTML_kit::new_page_url_params(Template_Engine::app('webapps'), ['hostname' => $domain]) }}"
							   class="dropdown-item  btn-block ui-action-label ui-action">
								<i class="fa fa-magic"></i>
								App Manager
							</a>
							<a title="Modify .htaccess"
							   href="{{ \HTML_kit::new_page_url_params(Template_Engine::app('personalities'), ['edit' => $domain]) }}"
							   class="dropdown-item  btn-block ui-action-label ui-action">
								<i class="fa fa-apache"></i>
								.htaccess Editor
							</a>

							<button title="Open File Manager" type="submit"
							        class="dropdown-item btn-block ui-action ui-action-manage-files ui-action-label"
							        name="file-manager[{{ $info['path']}}]" value="{{ $info['path']}}">
								Browse Files
							</button>

							<button title="Edit" type="submit"
							        class="dropdown-item btn-block ui-action ui-action-label ui-action-edit"
							        name="rename[{{ $domain }}]"
							        value="{{ $domain }}">
								Edit
							</button>
							<div class="dropdown-divider"></div>
							<button title="delete" type="submit"
							        class="dropdown-item  btn-block warn ui-action-label ui-action ui-action-delete"
							        name="delete[{{ $domain }}]" value="{{ $domain }}">
								Delete
							</button>

						</div>

					</div>
				</div>
			</div>
		</div>
	</form>
	<hr/>
@endforeach