$(window).on('load', function () {
	$('#ui-app input[data-toggle="collapseCheckbox"]').each(function (index, item) {
		var $item = $(item),
			$target = $($item.data('target'));
		$('input:checkbox[name="' + item.name + '"]').on('change', function () {
			var $that = $(this);
			if ($item.is(':checked')) {
				$target.collapse('show').on('shown.bs.collapse', function () {
					$that.prop('checked', true);
				});
			} else {
				$target.collapse('hide');
			}
		});
	});

	$('input[name=edit_mode]').bind('change', function () {
		location.href = apnscp.appUrl('logrotate') + '?mode=' + $(this).val();
	});
	$('#rotationSelection').bind('change', function () {
		var $target = $($(':selected', this).data('target'));
		$target.collapse('show').siblings().collapse('hide');
	}).triggerHandler('change');
});