$(window).on('load', function () {
	activateCopyCommand(jQuery);

	$('#ui-app .input').on('change click', function () {
		update_rsf(document.getElementById('spfForm'));
	}).change();
});

$('document').ready(function () {
	$('#domain').on('change', function () {
		$(this).closest('form').submit();
		return true;
	});
});

function activateCopyCommand($) {
	var COPY_SUCCESS_MSG = 'copied!';
	clipboard = new Clipboard('#ui-app .ui-action-copy');
	$('#ui-app [data-toggle="tooltip"]').tooltip();

	clipboard.on('success', function (e) {
		var originalTitle = $(e.trigger).attr('data-original-title') || 'copy';
		$(e.trigger).attr('data-original-title', COPY_SUCCESS_MSG).tooltip('show').on('hidden.bs.tooltip', function (e) {
			$(e.currentTarget).attr('data-original-title', originalTitle);
		});
		e.clearSelection();
	});

}

function textarea2colon(prefix, textarea) {
	mywords = textarea.value.split(/\s+/);
	for (i in mywords) {
		if (mywords[i].match(/\S/)) {
			newvalue.push(prefix + ":" + mywords[i]);
		}
	}
}

function update_rsf(myform) {
	newvalue = new Array("v=spf1");

	if (myform.a && myform.a[0].checked) {
		newvalue.push("a");
	}
	if (myform.mx && myform.mx[0].checked) {
		newvalue.push("mx");
	}
	if (myform.ptr && myform.ptr[0].checked) {
		newvalue.push("ptr");
	}
	if (myform.a_colon && myform.a_colon.value.length && myform.a_colon.value != "regular hostnames") {
		textarea2colon("a", myform.a_colon);
	}
	if (myform.mx_colon && myform.mx_colon.value.length && myform.mx_colon.value != "MX servers") {
		textarea2colon("mx", myform.mx_colon)
	}
	if (myform.ip4_colon && myform.ip4_colon.value.length && myform.ip4_colon.value != "IP addresses") {
		textarea2colon("ip4", myform.ip4_colon)
	}
	if (myform.include && myform.include.value.length && myform.include.value != "example.com") {
		textarea2colon("include", myform.include);
	}
	if (myform.all) {
		if (myform.all[0] && myform.all[0].checked) {
			newvalue.push("-all");
		}
		if (myform.all[1] && myform.all[1].checked) {
			newvalue.push("~all");
		}
		if (myform.all[2] && myform.all[2].checked) {
			newvalue.push("?all");
		}
	}
	if (myform.record_so_far) {
		myform.record_so_far.value = "\"" + newvalue.join(" ") + "\"";
	}
}