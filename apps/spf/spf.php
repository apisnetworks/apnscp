<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\spf;

	use Page_Container;

	class Page extends Page_Container
	{
		public $authorative = true;
		protected $zoneCache;
		protected $spf_domain;

		public function __construct()
		{
			parent::__construct();
			$this->add_css('spf.css');
			$this->add_javascript('spf.js');
			$this->spf_domain = isset($_GET['domain']) ? $_GET['domain'] : $_SESSION['domain'];
			$this->zoneCache = $this->dns_get_zone_information($this->spf_domain);
			if (null === $this->zoneCache) {
				$this->authorative = false;
			}
		}


		public function on_postback($params)
		{
			$this->spf_domain = $_GET['domain'] ?? $_SESSION['domain'];
			if (isset($params['Save'])) {
				if ($this->spfExists($this->spf_domain)) {
					$this->deleteSpf($this->spf_domain);
				}
				$status = $this->addZone($this->spf_domain . ".", 'TXT', 86400, $params['record_so_far']);
				if (($status instanceof Exception) && (false === strpos($status->getMessage(), "OK"))) {
					$this->bind($status);

				}

				return $status;
			}
		}

		public function spfExists($domain = null)
		{
			if (null === $domain) {
				$domain = $this->spf_domain;
			}

			return (bool)$this->getSpf($domain);
		}

		/**
		 * Get active SPF record
		 *
		 * @param string|null $domain
		 * @return null|string
		 */
		public function getSpf($domain = null): ?string
		{
			if (null === $domain) {
				$domain = $this->spf_domain;
			}
			$recs = $this->authorative ? $this->dns_get_records_by_rr('txt', $domain) :
				$this->dns_get_records_external('', 'txt', $domain);
			if (empty($recs)) {
				return null;
			}
			foreach ($recs as $rec) {
				if (str_starts_with((string)$rec['parameter'], 'v=spf')) {
					return $rec['parameter'];
				}
			}

			return null;
		}

		public function deleteSpf($domain)
		{
			$record = $this->getSpf($domain);
			if (!$record) {
				return warn("no SPF record set for `$domain'");
			}

			return $this->dns_remove_record($domain, '', 'TXT', $record);

		}

		public function addZone($mName, $mRR, $mTTL, $mParameter)
		{
			$status = $this->dns_add_record($this->spf_domain, '', $mRR, $mParameter, $mTTL);

			return $status;
		}

		public function getZoneData()
		{
			return $this->zoneCache;

		}

		public function getIPAddress()
		{
			return $this->authorative ? array_column(
				(array)$this->dns_get_records('', 'A', $this->spf_domain),
				'parameter'
			) : [];
		}

		public function getMXRecords()
		{

			return $this->authorative ? $this->dns_get_records_by_rr('MX', $this->spf_domain) : [];
		}

		public function getDomain()
		{
			return $this->spf_domain;
		}

		public function list_domains()
		{
			$domains = array_keys($this->aliases_list_shared_domains());
			array_unshift($domains, $this->common_get_service_value('siteinfo', 'domain'));

			return $domains;
		}

		public function deleteZone($mName, $mRR)
		{
			$status = $this->dns_remove_record($this->spf_domain, '', $mRR);

			return $status;
		}

	}

?>