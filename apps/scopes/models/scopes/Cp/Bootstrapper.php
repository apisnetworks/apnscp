<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace apps\scopes\models\scopes\Cp;

use apps\scopes\models\Scope;
use Illuminate\Support\Facades\View;
use Opcenter\Admin\Bootstrapper\Config;

class Bootstrapper extends Scope {

	protected $activeRole;
	protected $reader;
	private array $help;

	public function __construct(string $name)
	{
		if ($role = request()->post('role')) {
			$this->activeRole = $role;
		}
		parent::__construct($name);
		$this->reader = new Config(true);
		if ($role) {
			$vars = $this->getApnscpFunctionInterceptor()->scope_get('cp.bootstrapper', $role);
			$this->value = $vars;
		}
		if ($role) {
			$this->reader->loadRole($role);
		}
		$this->help = $this->reader->getComments();
	}

	public function getVariableHelp(string $var): string
	{
		return $this->help[$var] ?? '';
	}

	public function getRoles(): array
	{
		return array_map(function ($v) {
			$p2 = strrpos(dirname($v), '/');

			return substr($v, ++$p2);
		}, glob(resource_path('playbooks/roles/*/*', GLOB_ONLYDIR)));
	}

	public function activeRole(): ?string
	{
		return $this->activeRole;
	}

	public function set($val)
	{
		$var = $val[0];
		$val = $val[1] ?? 'None';
		if (false !== strpos($var, '[')) {
			$var = preg_replace('/\[([^]]+)\]/', '.$1', $var);
		}
		return $this->getApnscpFunctionInterceptor()->scope_set('cp.bootstrapper', $var, \Util_Conf::inferType($val));
	}

	public function render(array $vars = [])
	{
		if ($role = request()->post('role')) {
			// partial
			return View::make('partials.scopes.cp.bootstrapper.role-enumeration', [
				'role'        => $role,
				'scope'       => $this,
				'activeScope' => $this->getFullyQualifiedScope(),
				'value'       => $this->getValue()->raw(),
				'setting'     => $this->getSettings()
			]);
		}

		return parent::render($vars);
	}


}