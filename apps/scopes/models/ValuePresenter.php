<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace apps\scopes\models;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Support\Jsonable;

class ValuePresenter implements Htmlable, Jsonable {

	protected $val;

	public function __construct($val)
	{
		$this->val = $val;
	}

	public function toHtml()
	{
		switch (gettype($this->val)) {
			case 'integer':
				return $this->val === 0 ? "0" : $this->val;
			case 'boolean':
				return $this->val ? 'true' : 'false';
			case 'NULL':
				return 'null';
			case 'array':
				return "'" . \Opcenter\CliParser::collapse($this->val) . "'";
			default:
				return $this->val;
		}
	}

	public function raw()
	{
		return $this->val;
	}

	public function toJson($options = 0)
	{
		return json_encode($this->val, $options);
	}


	public function __toString()
	{
		return $this->toHtml();
	}
}

