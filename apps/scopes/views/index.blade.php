@extends('theme::layout')
@section('content')
	<div class="row align-content-around">
		<div class="col-12">
			<fieldset class="form">
				<form method="post" class="text-right" action="{{ \Template_Engine::app('scopes') }}/export">
					<div class="d-inline-block text-right mb-2">
						<button type="submit" name="get" value="bootstrapper"
						        class="d-flex-inline btn  btn-secondary ui-action-label ui-action-download"
						>Export Bootstrapper
						</button>
						<button type="submit" name="get" value="config"
						        class="d-flex-inline btn  btn-secondary ui-action-label ui-action-download"
						>Export Config
						</button>
					</div>
				</form>
				<select id="scopeIndex" class="custom-select" placeholder="Select Scope" autofocus>
					<option value=""></option>
					@foreach ($scopes as $scope)
						<option name="{{ $scope }}" @if ($activeScope === $scope) selected @endif data-help="{{ $Page->getScopeDescription($scope) }}"
							>{{$scope}}</option>
					@endforeach
				</select>
			</fieldset>

		</div>
	</div>
	<div class="row align-content-around" id="scopeMasterList">

	</div>
	<div id="scopeScene" class="mt-3  position-relative">
		@includeWhen(!$activeScope, 'partials.show-all')
	</div>
	@include('partials.scope-history')
@endsection