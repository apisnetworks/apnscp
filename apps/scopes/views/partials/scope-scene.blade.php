<div class="row">
	<div class="col-12 mb-3">
		<h4 class="d-flex-inline mb-0">{{ $scope->getGroup() }} &ndash; {{ $scope->getName()  }}</h4>
	</div>
</div>

@includeFirst(["partials.scopes.{$activeScope}.scope-help", "partials.scope-help"])

<form action="{{ \Template_Engine::app('scopes') }}/apply/{{ $activeScope }}" method="POST" id="scopeApply" class="mb-3 d-block">
	@include('partials.scope-edit-form', ['scope' => $scope])
</form>

<div class="cli-cmd mb-3" id="cliCmd">
	@includeFirst(["partials.scopes.{$activeScope}.scope-cli-invocation", 'partials.scope-cli-invocation'])
</div>

<div class="related row">
	<div class="col-12">
		@include('partials.scope-related', ['activeScope' => $scope->getFullyQualifiedScope()])
	</div>
</div>

@stack('after-render')