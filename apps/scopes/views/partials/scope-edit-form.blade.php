@includeFirst(["partials.scopes.$activeScope.scope-settings", 'partials.scope-settings'])
<div class="row">
	<div class="align-content-center col-12">
		@includeFirst([
			"partials.scopes.{$activeScope}.parse-type",
			'partials.scope-internals.parse-type'
		], [
			'value'    => $scope->getValue()->raw(),
			'setting'  =>  $scope->getSettings()
		])

		@includeFirst([
			"partials.scopes.{$activeScope}.scope-form-submit",
			'partials.scope-form-submit'
		])
	</div>
</div>