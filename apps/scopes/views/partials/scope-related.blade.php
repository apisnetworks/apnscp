<hr/>
<h5>Related Scopes</h5>
<ul class="list list-unstyled list-inline">
	@foreach($scope->getRelated() as $name)
		<li class="list-inline-item mr-2 mb-3">
			<a class="pr-2 pb-2 scope-related @if ($name === $activeScope) font-weight-bold @endif" href="{{ \Template_Engine::app('scopes') }}/{{ $name }}"
			   data-scope="{{ $name }}">{{ preg_replace('/\./',' ',$name, 1) }}</a>
		</li>
	@endforeach
</ul>