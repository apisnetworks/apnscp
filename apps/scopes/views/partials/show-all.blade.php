<div class="row">
	<div class="col-12 mb-3">
		@php
			$category = null;
		@endphp
		@foreach ($scopes as $scope)
			@php
				[$c, $name] = explode('.', $scope);
			@endphp
			@if ($c !== $category && $category = $c)
				<h3 class="mb-2 mt-3">{{ $category }}</h3>
			@endif
			<div class="list-group-item @if ($loop->index %2) bg-light @endif align-md-content-around d-flex flex-column flex-md-row align-content-start">
				<div class="align-items-center align-self-start d-flex">
					<button type="button" class="btn btn-secondary select ui-action ui-action-label ui-action-select mr-3"
					        data-scope-link="true" name="scope" value="{{ "{$c}.{$name}" }}">
						Select
					</button>
					<span class="font-weight-bold d">
						{{ $name }}
					</span>
				</div>
				<div class="ml-md-auto align-self-start align-self-md-center d-flex">
					{{ $Page->getScopeDescription("$c.$name") }}
				</div>
			</div>
		@endforeach
	</div>
</div>