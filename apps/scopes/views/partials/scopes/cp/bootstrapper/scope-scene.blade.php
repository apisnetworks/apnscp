@extends('partials.scope-scene')
@push('after-render')
<script>
	if (typeof(loadRole) === 'undefined') {
		function loadRole(role) {
			window.hashLoaded = true;
			if (role) {
				return apnscp.render({role: role}, "render/" + globalScopeName).done(function (html) {
					$('#roleOptions').html(html);
					window.location.hash = "#" + role;
				});
			}
			loadScope(globalScopeName, {'role': getActiveRole()}).done(function () {
				window.location.hash = "#" + role;
			});
		}
		registerListeners();
	} else {
		registerEvent('scope:changed', null, function (e, a1, a2) {
			// reapply listeners when Scope changes back
			if (a1 === a2) {
				return;
			}
			registerListeners();
		});
	}

	function registerListeners() {

		registerEvent('scope:popstate', null, function (e, sharedTriggerState) {
			sharedTriggerState.handled = true;
			var role = window.location.hash.match(/^#(.*)$/);
			loadRole(role[1]);
		});

		registerEvent('focus', '#roleOptions input, #roleOptions textarea', function () {
			$(this).data('original', $(this).val());
		}).registerEvent('blur', '#roleOptions input, #roleOptions textarea', function () {
			if ($(this).val() === $(this).data('original')) {
				$(this).removeData('original');
				return;
			}

			if (this.nodeName.toUpperCase() !== "TEXTAREA" && this.type.toUpperCase() !== "TEXT" &&
				this.type.toUpperCase() !== "NUMBER")
			{
				// double firing
				return true;
			}
			enqueueConfiguration(this);
		}).registerEvent('change', '#roleOptions :checkbox', function () {
			$(this).data('original', !$(this).prop('checked'));
			enqueueConfiguration(this);
		}).registerEvent('change', '#confirmChanges', function () {
			$('#scopeScene :submit[name="apply-changes"]').prop('disabled', !$(this).prop('checked'));
		}).registerEvent('click', '#pendingList .ui-action-delete', function () {
			$(this).parent().fadeOut('fast', function () {
				$(this).remove();
			})
		}).registerEvent('submit', '#scopeApply', function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			return runQueue(function(plays) {
				var args = ['system.integrity_check'], scopes = plays.filter(function (value, index, self) {
					return self.indexOf(value) === index;
				}).forEach(function (play) {
					args.push(play);
				});
				apnscp.cmd('scope_set', args);
			});
			return false;
		}).registerEvent('click', '#bootstrapperSubmission .btn-block', function (e) {
			if (e.target.name === 'apply-changes') {
				runQueue();
			} else if (e.target.name === 'run-bootstrapper') {
				runQueue(function () {
					apnscp.cmd('scope_set', ['cp.bootstrapper', true]);
				});

			}
			return false;
		});
	}

	$('#scopeScene').append($('<div id="' + DEFERRAL_ID + '"></div>'));

	function runQueue(postAction) {
		var plays = [], queue = $.Deferred();
		$('#pendingList li:visible').each(function (i, el) {
			var args = $(el).data('args');
			// abandon hope all ye who enter!
			// extract from apnscp.post() as that implicitly calls ajax
			queue = $.ajaxQueue(
				apnscp.call_app_ajax(null, null, [], {
					url: apnscp.appUrl(null, 'apply/' + globalScopeName),
					method: 'POST',
					dataType: 'json',
					data: {role: 'cp.bootstrapper', args: args}
				})).done(function () {
					var role = $(el).data('role');
					if (role) {
						plays.push(role);
					}
					$(el).fadeOut('fast', function () {
						this.remove();
					})
			});
		});

		queue.done(function () {
			$.ajaxQueue().done(function () {
				apnscp.addMessage("Changes submitted");
				if (typeof(postAction) === 'function') {
					postAction.apply(this, [plays]);
				}
			});
		});

		return queue;
	}

	function enqueueConfiguration(ref) {
		if (ref.type.toUpperCase() === 'CHECKBOX') {
			ref.value = $(ref).prop('checked');
		}

		$(ref).effect("transfer", {to: $("#pendingList")}, 500, function () {
			$('#pendingList').siblings('.heading').removeClass('hide');

			var $template = $('#pendingList .template').clone().removeClass('template hide');

			$('#pendingList').append(
				$template.data({args: [ref.name, ref.value], role: getActiveRole()}).
					find('.postaction').text(ref.value).end().
					find('.preaction').text($(ref).data('original')).end().
					find('.role').text(getActiveRole()).end().
					find('.var').text(ref.name).end()
			);
		});

	}

	function getActiveRole() {
		return window.location.hash.substring(1);
	}

	var hash = window.getNestedObject(window, 'location.hash');

	if (hash && typeof(loadRole) !== 'undefined')
	{
		if (!window.hashLoaded) {
			loadRole(getActiveRole());
		}
	}

	// always load
	$('#bootstrapperActions select[data-toggle="combobox"]').on('change' + SCOPE_EVENT_NS, function () {
		loadRole($(this).val());

	}).combobox().on('comboboxselect', function (event, item) {
		$(this).closest('select').triggerHandler('change');
	});

	$('#scopeScene :submit[name="scope"]').prop('disabled', true);
</script>
@endpush