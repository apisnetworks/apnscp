<div class="row">
	<div class=" col-xl-2 col-12 col-md-4">
		<div class="list-group" id="sectionList" role="tablist">
			@foreach (array_keys($value) as $section)
				<a class="list-group-item section-list list-group-item-action @if ($loop->index === 0) active @endif"
				   data-toggle="list" id="{{ $section }}-list" aria-controls="{{ $section }}"
				   href="#{{ $section }}" role="tab">{{ $section }}</a>
			@endforeach
		</div>
	</div>

	<div class="col-xl-10 col-12 col-md-8">
		<div class="tab-content">
			@foreach ($value as $section => $items)
				<div class="tab-pane @if ($loop->index === 0) active @endif" id="{{ $section }}"
				     role="tabpanel"
				     aria-labelledby="{{ $section }}-list">
					<h4>{{ $section }}</h4>
					<hr/>
					<div class="mb-3">
						@php ksort($items); @endphp
						@foreach ($items as $option => $value)
							<div class="row @if ($loop->index % 2) bg-light @endif">
								<div class="col-12 mb-3">
									<label class="form-control-label mb-0 d-flex flex-column flex-md-row justify-content-start justify-content-md-between">
										<span class="mr-3 d-md-inline d-flex ">{{ $option }}</span>
										<span class="ml-md-auto d-md-inline d-flex help text-light text-md-right ui-action-label small">
											{{ $scope->getVariableHelp($section, $option) }}
										</span>
									</label>
									@include('partials.scope-internals.parse-type', [
										'setting' => gettype($value),
										'value'   => $value,
										'name'    => $section . "[$option]",
										'attrs'   => $scope->isReadOnly($section, $option) ? 'READONLY' : ''
									])
								</div>
								<hr/>
							</div>
						@endforeach
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>