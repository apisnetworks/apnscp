@php ksort($value); @endphp
@foreach ($value as $section => $items)
	<h4>{{ $section }}</h4>
	<div class="mb-3">
		@php ksort($items); @endphp
		@foreach ($items as $option => $value)
			<div class="row @if ($loop->index % 2) bg-light @endif">
				<div class="col-12 mb-3">
					<label class="form-control-label mb-0 d-flex align-content-center">
						<span class="mr-3">{{ $option }}</span> <span
								class="ml-auto help text-light text-right ui-action-label small">{{ $scope->getVariableHelp($section, $option) }}</span>
					</label>
					@include('partials.scope-internals.parse-type', [
						'setting' => gettype($value),
						'value'   => $value,
						'name'    => $section . "[$option]"
					])
				</div>
				<hr/>
			</div>
		@endforeach
		<hr/>
	</div>
@endforeach