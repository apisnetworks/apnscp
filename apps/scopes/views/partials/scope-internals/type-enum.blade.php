<select {{ $attrs ?? '' }} name="args[{{ $name }}]" class="custom-select form-control">
	@foreach ($setting as $v)
		<option value="{{ $v }}" @if ($v == $value) SELECTED @endif
		>{{ is_bool($v) ? ($v ? 'true' : 'false') : $v }}</option>
	@endforeach
</select>