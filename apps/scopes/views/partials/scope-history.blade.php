<div class="cli-history mb-3" id="scopeHistory">
	<h4 class="hide heading">Scope History</h4>
	<ol class="list pl-3">
		<li class="mb-1 template hide">
			<button class="btn btn-sm btn-outline-secondary ui-action-label warn ui-action-restore ui-action mr-1" type="button"
			        data-scope="placeholder" data-undo-title="{{ _("Undo") }}" data-args="" data-redo-title="{{ _("Redo") }}" data-args="1"> {{ _("Undo") }}
			</button>
			<span class=""></span>
		</li>
	</ol>
</div>