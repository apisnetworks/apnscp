<div class="d-flex align-items-start">
@if ($scope->isChanged())
	<span class="d-flex mr-5 mt-1 badge badge-warning">Changed</span>
	<div class="d-flex mr-5 flex-column">
		<label class="d-block">Current setting</label>
			@if (is_array($scope->getValue()->raw()))
				<table class="table table-sm">
					<tbody>
				@foreach($scope->getValue()->raw() as $k => $v)
					<tr>
						@if (!is_int($k)) <th scope="row">{{ $k }}</th> @endif
						<td>{{ \Opcenter\CliParser::collapse($v) }}</td>
					</tr>
				@endforeach
					</tbody>
				</table>
			@else
				<span class="d-block">
					{{ $scope->getValue() }}
				</span>
			@endif
	</div>
	@else
		<span class="d-flex mr-5 mt-1 badge badge-success">Unchanged</span>
	@endif
	<div class="d-flex flex-column">
		<label class="d-block">Default setting</label>
		<span class="d-block">
			{{ $scope->getDefault() }}
		</span>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<hr />
	</div>
</div>