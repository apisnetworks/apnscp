<script defer>
	$(window).on('load', function() {
		$('.otp-input').on('paste', function (e) {
			e.preventDefault();
			var pastedData = e.originalEvent.clipboardData.getData('text');
			var el = $('.otp-input input[type=number]');
			var len = Math.min(pastedData.length, el.length);
			for (var i = 0; i < len; i++) {
				el[i].value = pastedData[i];
			}

			$('.otp-input').unbind('change keyup').closest('form').submit();
			return false;
		}).on('change keyup', function (e, keyCode) {
			if (!e || !e.originalEvent) {
				return true;
			}
			var keyCode = keyCode || e.which;
			if (keyCode >= 48 && keyCode <= 57 || keyCode >= 96 && keyCode <= 105) {
				var next = $(e.target).next();
				if (e.target.value.length > 1) {
					e.target.value = e.target.value[0];
					return false;
				}
				if (!next.get(0)) {
					$(this).closest('form').submit();
					return true;
				}
				$(e.target).next().focus();
				e.stopImmediatePropagation();
			} else if (e.type === 'keyup' && e.target.value) {
				e.target.value = "";
			}
		});
	});
</script>
<style>
	.otp-input input[type=number]::-webkit-inner-spin-button,
	.otp-input input[type=number]::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	.otp-input input[type=number] {
		max-width: 2.5em;
		height: auto;
		text-align: center;
		-moz-appearance: textfield;
	}
</style>
<div class="totp-container">
	<p>Enter the {{ \Auth\TOTP::TOTP_LENGTH }}-digit code to proceed</p>
	<div class="otp-input form-group form-inline">
		@foreach (range(1, \Auth\TOTP::TOTP_LENGTH) as $i)
			<input type="number" autocomplete="no" @if ($loop->first) autofocus="on" @endif pattern="\d" name="{{ \apps\login\Page::TOTP_FIELD }}[]" class="form-control px-1 m-1 form-control-lg" maxlength="1" min="0" max="9" required />
		@endforeach
	</div>
	<button class="btn btn-primary btn-lg my-1 ml-1" name="" id="totp-verify" type="submit"
	        value="">Verify
	</button>
</div>