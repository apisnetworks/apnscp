@php
	$pageMode = $Page->getMode();
	$username = $Page->getUsername();
	$domain = $Page->getDomain();
	$errors = $Page->errors_exist();
@endphp

<div class="gradient-primary col-12">
	<div class="row">
		<div class="col-12 text-center my-3">
			<h1 class="logo">{{ PANEL_BRAND }}</h1>
		</div>
	</div>
	@includeWhen(!empty($msgs = $Page->getResponse()), 'partials.postback-messages', ['msgs' => $msgs])
	<div class="row row-eq-height">
		<div class="login-container col-12 col-sm-10 col-md-6 px-0 content-container @if ($Page->view()->exists('partials.aside')) offset-sm-1 offset-md-0 offset-xl-1 col-xl-5 @else col-xl-4 offset-md-3 offset-sm-1 offset-xl-4 @endif">
			<div class="content clearfix">
				<div class="panel clearfix" id="login">
					@php
						$title = "default";
						if ($pageMode !== 'login') {
							$title = match(true) {
								!empty($_GET['challenge']) => "enter_token",
								isset($_GET['totp']) => "verify_totp",
								default => "request_login"
							};
						}
						$title = $Page->var("headers.{$title}");
					@endphp
					<h3 class="mt-3">
						@php eval('?>' . $title); @endphp
					</h3>

					<form action="{{ \HTML_Kit::page_url_params(['logout' => null]) }}" method="post"
					      autocomplete="off" class="mb-0"
					      id="{{ $pageMode . '-form' }}" role="form">
						@if ($pageMode === 'login')
							@include('partials.forms.login')
						@elseif ($pageMode === 'totp')
							@include('partials.forms.totp-challenge')
						@else
							@include('partials.forms.reset')
						@endif
					</form>

					<div class="row problem-container">
						<div class="col-6">
							@includeIf('partials.bottom-gutter')
						</div>
						<div class="col-6">
							@if ($pageMode === 'login')
								<a href="{{ \HTML_Kit::new_page_url_params('/forgot_info', ['domain' => $domain]) }}">
									Forgot your password?
								</a>
							@else
								<a href="{{ \HTML_Kit::new_page_url_params('/login', ['domain' => $domain, 'reset']) }}">
									Return to login
								</a>
							@endif
						</div>
						@includeWhen($Page->hasSystemStatus(), 'partials.status')
					</div>
				</div>
			</div>
		</div>

		@includeIf('partials.aside')
	</div>
</div>

<footer class="fixed-bottom footer row" id="">
</footer>