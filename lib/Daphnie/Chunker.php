<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2020
 */

namespace Daphnie;

use Opcenter\Apnscp;
use Opcenter\Process;

/**
 * Class Chunker
 *
 * Metric chunk management
 *
 * @package Daphnie
 */
class Chunker {
	/**
	 * ───▐▀▄──────▄▀▌───▄▄▄▄▄▄▄─────────────
	 * ───▌▒▒▀▄▄▄▄▀▒▒▐▄▀▀▒██▒██▒▀▀▄──────────
	 * ──▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄────────
	 * ──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▄▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄──────
	 * ▀█▒▒█▌▒▒█▒▒▐█▒▒▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
	 * ▀▌▒▒▒▒▒▀▒▀▒▒▒▒▒▀▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐───▄▄
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌▄█▒█
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐▒█▀─
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐▀───
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌────
	 * ─▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐─────
	 * ─▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
	 * ──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐──────
	 * ──▐▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▌──────
	 * ────▀▄▄▀▀▀▀▄▄▀▀▀▀▀▀▄▄▀▀▀▀▀▀▄▄▀────────
	 *
	 * Special notes on older/newer usage:
	 *
	 * $older<TYPE> parameters specify any record older than TS
	 * when $newer<TYPE> is provided, it brackets the range
	 *
	 * older: OLDER < RECORDTS
	 * older + newer: OLDER < RECORDTS AND RECORDTS > NEWER (or RECORDTS ε (NEWER, OLDER))
	 *
	 */

	protected $db;

	// @var string[] decompressed chunks
	private static $decompressed = [];

	// @var int[] job scheduling suspended
	private static $suspended = [];
	protected Connector $connector;

	public function __construct(\PDO $db)
	{
		$this->db = $db;
		$this->connector = new Connector($db);
	}

	public function __destruct()
	{
		if (!self::$decompressed && !self::$suspended) {
			return;
		}

		$this->release();
	}

	/**
	 * Release a paused compression routine
	 */
	public function release()
	{
		while (null !== ($chunk = array_shift(self::$decompressed))) {
			$this->compress($chunk);
		}

		while (null !== ($jobId = array_shift(self::$suspended))) {
			$this->resumeJob($jobId);
		}
	}

	/**
	 * List data segments
	 *
	 * @param string|null $olderInt
	 * @param string|null $newerInt
	 * @return array
	 */
	public function listChunks(?string $olderInt = null, string $newerInt = null): array
	{
		if ($olderInt && !$this->validInterval($olderInt)) {
			return [];
		} else if ($newerInt && !$this->validInterval($newerInt)) {
			return [];
		}

		$query = $this->connector->vendor()->showChunks($olderInt, $newerInt);

		$rs = $this->db->query($query);
		if (!$rs) {
			error('Chunk failed: %s', $this->db->errorInfo()[2]);
			return [];
		}

		return $rs->fetchAll(\PDO::FETCH_COLUMN);
	}

	/**
	 * Metric storage has compression
	 *
	 * @return bool
	 */
	public function hasCompression(): bool
	{
		$query = $this->connector->vendor()->hasCompression();
		$rs = $this->db->query($query);
		return $rs && $rs->rowCount() > 0;
	}

	/**
	 * Perform complete decompression on all metrics
	 *
	 * @return bool
	 */
	public function decompressAll(): bool
	{
		if (!$this->hasCompression()) {
			return true;
		}

		try {
			return null !== $this->decompressRange(null);
		} finally {
			// block automatic recompression
			if (!TELEMETRY_ARCHIVAL_COMPRESSION) {
				$this->clearTransientRecompressionChunks();
			}
		}
	}

	/**
	 * Synchronously decompress a range of data segments
	 *
	 * @param int|null $olderTs
	 * @param int|null $newerTs
	 * @return int|null
	 */
	public function decompressRange(?int $olderTs, int $newerTs = null): ?int
	{
		$pid = $this->suspendJobRunner();
		defer($_, function () use ($pid) {
			$this->resumeJobRunner($pid);
		});

		$this->getJobs();
		foreach ($this->getJobs() as $job) {
			$this->pauseJob($job['job_id']);
		}
		$chunks = $this->getChunksFromRange($olderTs, $newerTs);
		$cnt = 0;
		if (!$chunks) {
			return $cnt;
		}
		foreach ($chunks as $chunk) {
			if (!$this->isCompressed($chunk)) {
				continue;
			}
			debug("Decompressing $chunk");
			$cnt += (int)$this->decompress($chunk);
		}

		return $cnt;
	}

	/**
	 * Clear chunks marked for compression
	 */
	public function clearTransientRecompressionChunks(): void
	{
		self::$decompressed = [];
		self::$suspended = [];
	}

	/**
	 * Table was previously compressed
	 *
	 *
	 *
	 * @return bool
	 */
	public function hasCompressionArtifacts(): bool
	{
		$query = $this->connector->vendor()->hasCompressionArtifacts();
		$res = $this->db->query($query);
		if (!$res) {
			$err = $this->db->errorInfo();
			return error('Failed to query artifacts: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
		}

		return $res->rowCount() > 0;
	}

	/**
	 * Synchronously compress a range of data segments
	 *
	 * @param int|null $olderTs
	 * @param int|null $newerTs
	 * @return int|null
	 */
	public function compressRange(?int $olderTs, int $newerTs = null): ?int
	{
		$chunks = $this->getChunksFromRange($olderTs, $newerTs);
		$cnt = 0;
		if (!$chunks) {
			return $cnt;
		}
		foreach ($chunks as $chunk) {
			debug("Compressing $chunk");
			$cnt += (int)$this->compress($chunk);
		}
		$this->resumeJobRunner();

		return $cnt;
	}

	private function suspendJobRunner(): ?int {
		$path = run_path(Apnscp::HOUSEKEEPER_PID);
		if (!file_exists($path)) {
			return null;
		}
		$pid = (int)file_get_contents($path);
		if (!Process::pidMatches($pid, PHP_BINARY)) {
			return null;
		}
		posix_kill($pid, SIGTSTP);

		return $pid;
	}

	private function resumeJobRunner(int $pid = null): void {
		if (!$pid) {
			$path = run_path(Apnscp::HOUSEKEEPER_PID);
			if (!file_exists($path)) {
				return;
			}
			$pid = (int)file_get_contents($path);
		}

		if (!Process::pidMatches($pid, PHP_BINARY)) {
			return;
		}

		posix_kill($pid, SIGCONT);
	}

	/**
	 * Get chunks
	 *
	 * @link https://docs.timescale.com/latest/api#show_chunks
	 *
	 * Unlike show_chunks(), returned chunk list is inclusive of $olderTs
	 *
	 * @param int|null $olderTs older_than
	 * @param int|null $newerTs newer_than
	 * @return array
	 */
	public function getChunksFromRange(?int $olderTs, int $newerTs = null): array
	{
		$query = $this->connector->vendor()->getChunksFromRange($olderTs, $newerTs);
		$rs = $this->db->query($query);
		if (false === $rs) {
			error('Failed to fetch chunks from range: %s', $this->db->errorInfo()[2]);
			return [];
		}

		return array_column($rs->fetchAll(\PDO::FETCH_ASSOC), 'chunk_name');
	}
	/**
	 * Get active jobs on metrics
	 *
	 * @return array
	 */
	public function getJobs(): array
	{

		$query = $this->connector->vendor()->getJobs();
		$rs = $this->db->query($query);
		if (!$rs) {
			fatal('%s', $this->db->errorInfo()[2]);
		}

		return $rs->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Pause background compression job
	 *
	 * @param int $jobid
	 * @return bool
	 */
	public function pauseJob(int $jobid): bool
	{
		$query = $this->connector->vendor()->pauseJob($jobid);
		$rs = $this->db->exec($query);
		if ($rs === false) {
			return error('Job pause failed: %s', $this->db->errorInfo()[2]);
		}

		self::$suspended[] = $jobid;

		return true;
	}

	/**
	 * Resume suspended job
	 *
	 * @param int $jobid
	 * @return bool
	 */
	public function resumeJob(int $jobid): bool
	{
		$query = $this->connector->vendor()->resumeJob($jobid);
		$rs = $this->db->exec($query);
		if (false !== ($key = array_search($jobid, self::$suspended, true))) {
			unset(self::$suspended[$key]);
		}
		if ($rs === false) {
			return error('Job resume failed: %s', $this->db->errorInfo()[2]);
		}

		return true;
	}

	/**
	 * Synchronously decompress hypertable chunk
	 *
	 * decompress() does not lock. See {@link decompressRange()}
	 *
	 * @param string $chunk hypertable name
	 * @return bool
	 */
	public function decompress(string $chunk): bool {
		if (!$this->isCompressed($chunk)) {
			return debug("Chunk `%s' reported as decompressed", $chunk);
		}
		$query = $this->connector->vendor()->decompressChunk($chunk);
		$res = $this->db->exec($query);
		if ($res === false) {
			$err = $this->db->errorInfo();

			return error('Compression failed: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
		}

		self::$decompressed[] = $chunk;
		return true;
	}

	/**
	 * Disable archival compression in TimescaleDB
	 *
	 * From local testing, this left behind empty compressed hypertables
	 * @see hasCompressionArtifacts()
	 *
	 * @return bool
	 */
	public function disableArchivalCompression(): bool {
		$query = $this->connector->vendor()->disableArchivalCompression();
		$res = $this->db->exec($query);
		if ($res) {
			return true;
		}
		$err = $this->db->errorInfo();
		return error('Decompression flag failed: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
	}

	/**
	 * Synchronously compress hypertable chunk
	 *
	 * compress() does not lock. See {@link compressRange()} for locking
	 *
	 * @param string $chunk hypertable name
	 * @return bool
	 */
	public function compress(string $chunk): bool {
		if ($this->isCompressed($chunk)) {
			return debug("Chunk `%s' reported as compressed", $chunk);
		}
		$query = $this->connector->vendor()->compressChunk($chunk);
		$res = $this->db->exec($query);
		if ($res === false) {
			$err = $this->db->errorInfo();

			return error('Compression failed: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
		}

		if (false !== ($key = array_search($chunk, self::$decompressed, true))) {
			unset(self::$decompressed[$key]);
		}

		return true;
	}

	/**
	 * Data segment uses Timescale compression
	 *
	 * @param string $chunk chunk name
	 * @return bool
	 */
	public function isCompressed(string $chunk): bool
	{
		$query = $this->connector->vendor()->isCompressed($chunk);
		$rs = $this->db->query($query);

		return $rs->rowCount() > 0;
	}

	/**
	 * Get data segment compression statistics
	 *
	 * @param string $chunk
	 * @return array
	 */
	public function getCompressionStats(string $chunk): array
	{
		$query = $this->connector->vendor()->getChunkCompressionStats($chunk);
		$rs = $this->db->query($query);
		$res = array_get($rs->fetchAll(\PDO::FETCH_ASSOC), 0, []);
		foreach ($res as $k => $v) {
			if (substr($k, -6) === '_bytes') {
				$res[$k] = \Formatter::changeBytes($v);
			}
		}

		return $res;
	}

	/**
	 * Get data chunk information
	 *
	 * "ranges" is [min,max)
	 *
	 * @param string $chunk
	 * @return array
	 */
	public function getChunkStats(string $chunk = null): array
	{
		$query = $this->connector->vendor()->getChunkStats($chunk);
		$rs = $this->db->query($query);
		if (false === $rs) {
			$err = $this->db->errorInfo();

			error('Chunk retrieval failed: (%errcode)s) %(err)s',
				['errcode' => $err[0], 'err' => $err[2]]
			);

			return [];
		}

		$recs = $rs->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($recs as &$rec) {
			$ranges = array_map('\intval', explode(',', trim($rec['ranges'], '{"}[')));
			$rec['ranges'] = [
				$ranges[0]/1000000,
				$ranges[1]/1000000
			];
		}

		return $chunk ? array_get($recs, 0, []) : $recs;
	}

	/**
	 * Discard data
	 *
	 * Note: $after is used to clear accounting irregularities with clocks
	 *
	 * @param string|null $olderInt oldest interval to preserve
	 * @param string|null $newerInt newest future-dated interval to discard (NOW() + $after)
	 * @return bool
	 */
	public function removeData(?string $olderInt, string $newerInt = null): bool
	{
		if ($olderInt && !$this->validInterval($olderInt)) {
			return false;
		} else if ($newerInt && !$this->validInterval($newerInt)) {
			return false;
		}

		$query = $this->connector->vendor()->removeData($olderInt, $newerInt);

		if (false === $this->db->exec($query)) {
			$err = $this->db->errorInfo();

			return error('Metric data deletion failed: (%errcode)s) %(err)s',
				['errcode' => $err[0], 'err' => $err[2]]
			);
		}

		return true;
	}

	/**
	 * Apply TS compression policy
	 *
	 * @link https://docs.timescale.com/latest/using-timescaledb/compression
	 *
	 * @param string $timespec
	 * @return bool
	 */
	public function setCompressionPolicy(string $timespec): bool
	{
		if (!$this->validInterval($timespec)) {
			return false;
		}
		$q = $this->connector->vendor()->setCompressionPolicy($timespec);
		if (false === $this->db->exec($q)) {
			return error('Compression policy failed: %s', $this->db->errorInfo()[2]);
		}

		return true;
	}

	/**
	 * Given interval is valid
	 *
	 * @param string $interval
	 * @return bool
	 */
	private function validInterval(string $interval): bool
	{
		$sqlspec = $this->db->quote($interval);
		if (false === $this->db->exec("SELECT {$sqlspec}::interval")) {
			fatal("Invalid/unknown timespec `%(timespec)s': %(err)s",
				['timespec' => $interval, 'err' => $this->db->errorInfo()[2]]
			);

			return false;
		}

		return true;
	}
}
