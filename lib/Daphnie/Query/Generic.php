<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2021
 */

namespace Daphnie\Query;

abstract class Generic
{
	protected \PDO $db;

	public function __construct(\PDO $db)
	{
		$this->db = $db;
	}

	public function compressChunk(string $chunk): string
	{
		return 'SELECT compress_chunk(' . $this->db->quote($chunk) . ')';
	}

	public function decompressChunk(string $chunk): string
	{
		return 'SELECT decompress_chunk(' . $this->db->quote($chunk) . ')';
	}

	public function showChunks($olderThan = null, $newerThan = null): string
	{
		$newerSql = $olderSql = 'NULL';

		if (null !== $olderThan) {
			$olderSql = is_int($olderThan) ? "TO_TIMESTAMP({$olderThan})" : 'INTERVAL ' . $this->db->quote($olderThan);
		}

		if (null !== $newerThan) {
			$newerSql = is_int($newerThan) ? "TO_TIMESTAMP({$newerThan})" : 'INTERVAL ' . $this->db->quote($newerThan);
		}

		return "SELECT * FROM show_chunks('metrics', $olderSql, $newerSql)";
	}

	public function setCompressionPolicy($timespec): string
	{
		$sqlspec = $this->db->quote($timespec);

		return "SELECT add_compress_chunks_policy('metrics', INTERVAL $sqlspec);";
	}

	public function resumeJob(int $jobid): string
	{
		return "SELECT alter_job_schedule({$jobid}, next_start=>now())";
	}

	public function pauseJob(int $jobid): string
	{
		return "SELECT alter_job_schedule({$jobid}, next_start=>'infinity');";
	}

	/**
	 * @param int|null $olderTs time in seconds
	 * @param int|null $newerTs time in seconds
	 * @return string
	 */
	public function getChunksFromRange(?int $olderTs, int $newerTs = null): string
	{
		return "SELECT
				c.id AS id,
				int8range(range_start, range_end),
				c.schema_name||'.'||c.table_name AS chunk_name
			FROM
				_timescaledb_catalog.dimension_slice ds
				JOIN
					_timescaledb_catalog.chunk_constraint cc ON
					(cc.dimension_slice_id = ds.id)
				JOIN
					_timescaledb_catalog.chunk c ON
					(c.id = cc.chunk_id)
				JOIN
					_timescaledb_catalog.hypertable h ON
					(h.id = c.hypertable_id)
				JOIN
					_timescaledb_catalog.dimension d ON
					(ds.dimension_id = d.id)
				WHERE h.table_name = 'metrics' AND d.column_name = 'ts' AND
				 '[" . ($newerTs * 1000000) . ", " . ($olderTs * 1000000) . ")'::int8range && int8range(range_start, range_end)
		";
	}

	public function hasCompressionArtifacts(): string
	{
		return "SELECT compressed_hypertable_id 
			FROM _timescaledb_catalog.hypertable WHERE table_name = 'metrics' AND compressed_hypertable_id IS NOT NULL";
	}

	/**
	 * Reform chunk literal into v2+ format
	 *
	 * @param string $chunk
	 * @return string
	 */
	protected function canoncializeChunk(string $chunk): string
	{

		if (0 !== strpos($chunk, "_timescaledb_internal.")) {
			return $chunk;
		}

		return substr($chunk, strpos($chunk, '.') + 1);
	}

	protected function assertIdentifier(string $identifier): void
	{
		if (!preg_match('/^[a-z0-9_]+$/Di', $identifier)) {
			fatal("Invalid identifier %s", $identifier);
		}
	}

	/**
	 * Discard data
	 *
	 * Note: $after is used to clear accounting irregularities with clocks
	 *
	 * @param string|null $olderInt oldest interval to preserve
	 * @param string|null $newerInt newest future-dated interval to discard (NOW() + $after)
	 * @return bool
	 */
	public function removeData(?string $olderInt, string $newerInt = null): string
	{
		$window = [
			'older_than' => $olderInt ? 'interval ' . $this->db->quote($olderInt) : 'NOW()'
		];

		if ($newerInt) {
			$window['newer_than'] = 'NOW() + interval ' . $this->db->quote($newerInt);
		}

		$args = implode(',', array_key_map(static function ($k, $v) {
			return $k . ' => ' . $v;
		}, $window));

		return "SELECT drop_chunks('metrics', " . $args . ')';
	}

	public function refreshMaterializedView(string $view): string
	{
		$this->assertIdentifier($view);

		return "REFRESH MATERIALIZED VIEW " . $view;
	}
}