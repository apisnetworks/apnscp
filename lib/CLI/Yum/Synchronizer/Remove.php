<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;

	class Remove extends Synchronizer
	{

		protected $soft = false;

		public function __construct(...$args)
		{
			if ($args[0] === '--soft') {
				$this->soft = true;
				array_shift($args);
			}
			parent::__construct(...$args);
			// yum remove pkg
			if (!Utils::exists($this->package)) {
				$this->soft = false;
			}
		}

		public function run()
		{
			$pkg = $this->package;
			if (null === ($svc = Utils::getServiceFromPackage($pkg))) {
				// package not installed
				return info("Package `%s' is not installed", $pkg);
			}
			$svcpath = Utils::getServicePath($svc);
			$meta = Utils::getMetaFromPackage($pkg);
			$hardRemoval = !isset($this->version, $this->release) || ($this->version === $meta['version'] && $this->release === $meta['release']);
			if (!$this->soft) {
				// removed from system, kill it
				$files = (array)Utils::getFilesFromRPM($pkg);
				// files are directory first, then content
				foreach (array_reverse($files) as $file) {
					$this->removeFile($file, $svcpath);
				}
			}

			return $hardRemoval ? $this->postTransaction($pkg, $svc) : true;
		}

		public function postTransaction($package, $service)
		{
			Synchronizer\Plugins\Manager::run($package, 'remove');
			$db = \PostgreSQL::initialize();
			if ($this->soft) {
				$db->query("UPDATE site_packages SET deleted = 't' WHERE package_name = " .
					pg_escape_literal($db->getHandler(), $package));
			} else {
				$db->query('DELETE FROM site_packages WHERE package_name = ' . pg_escape_literal($db->getHandler(), $package));
				SynchronizerCache::get()->removePackage($package);
			}

			return $db->affected_rows() > 0;
		}

	}