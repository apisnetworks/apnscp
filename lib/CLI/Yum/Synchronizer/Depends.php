<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;

	/**
	 * Class Depends
	 *
	 * Get package dependencies
	 */
	class Depends extends Synchronizer
	{
		protected $depth = 2;
		protected $recursive = false;

		public function __construct(...$args)
		{
			foreach ($args as $a) {
				if ($a[0] === '-') {
					switch ($a) {
						case '-r':
							$this->recursive = true;
							break;
						default:
							if (preg_match('/^-d(\d+)$/', $a, $match)) {
								$this->depth = (int)$match[1];
								break;
							}
							fatal("Unrecognized flag `%s'", $a);
					}
					array_shift($args);
				}
			}
			if (\count($args) > 1) {
				fatal('Depends takes a single argument, package');
			}
			$this->package = $args[0];

		}

		public function run()
		{
			if (!array_get(\Util_Process_Safe::exec(['rpm', '-q', '%(rpm)s'], ['rpm' => $this->package]), 'success', false)) {
				// package not installed
				return error("Package `%s' is not installed", $this->package);
			}
			$depmap = new DependencyMap($this->package);
			$wants = [];
			if ($this->recursive) {
				$ret = $depmap->resolveRecursively($wants, $this->depth);
			} else {
				$ret = $depmap->resolve();
			}
			if (false === $ret) {
				return error("Failed to resolve `%s'", $this->package);
			}

			if (!$depmap->satisfied()) {
				warn("Package `%s' is not resolved. Install the following dependencies to resolve: %s",
					$this->package, implode(', ', $depmap->getWants())
				);

				return $depmap->getWants();
			}
			info("Package `%s' is resolved", $this->package);
			foreach ($depmap->listInstalledDependencies() as $dep => $svc) {
				info("Dependency `{$dep}' installed in `{$svc}'");
			}

			return true;
		}
	}