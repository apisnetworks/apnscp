<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use CLI\Yum\Synchronizer\Utils;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Database\MySQL;

	/**
	 * Class MariaDBClient
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * MariaDB <=> MySQL binary symlinks
	 *
	 */
	class MariaDBClient extends Trigger
	{
		public function install(string $package): bool
		{
			if (MySQL::version() < 110100) {
				return true;
			}
			$cfg = new Config();
			$links = array_get($cfg->loadRole('mysql/install'), 'mariadb_bin_links', []);
			$svc = Utils::getServiceFromPackage($package);
			$path = Utils::getServicePath($svc);
			foreach ($links as $compat => $realfile) {
				if (!file_exists("{$path}/{$realfile}")) {
					continue;
				}
				$compatfile = "{$path}" . dirname($realfile) . "/{$compat}";
				if (!file_exists($compatfile) || !is_link($compatfile)) {
					symlink(basename($realfile), "{$compatfile}");
				}
			}

			return true;
		}

		public function update(string $package): bool
		{
			return $this->install($package);
		}
	}