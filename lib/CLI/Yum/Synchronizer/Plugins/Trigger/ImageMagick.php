<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2024
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Alternatives;
	use CLI\Yum\Synchronizer\Plugins\AlternativesTrigger;
	use CLI\Yum\Synchronizer\Utils;
	use Opcenter\Versioning;

	/**
	 * Class Nss
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 *
	 */
	class ImageMagick extends AlternativesTrigger
	{
		protected $alternative;

		public function __construct()
		{
			// Discourse compatibility
			$this->alternatives = [
				[
					'name'     => 'magick',
					'src'      => '/usr/bin/magick',
					'dest'     => '/usr/bin/convert',
					'priority' => 10
				],
				[
					'name'     => 'magick',
					'src'      => '/usr/bin/magick',
					'dest'     => '/usr/bin/magick',
					'priority' => 20
				]
			];
		}

		public function update(string $package): bool
		{
			$version = array_get(Utils::getMetaFromRPM('ImageMagick'), 'version', '0.0');

			if (Versioning::compare($version, '7.0', '>=')) {
				return Versioning::compare(Utils::getFullVersionFromPackage('ImageMagick'), '7.0', '>=') ?:
					(new Alternatives(Utils::getServiceFromPackage($package), 'magick'))->removeAll();
			}

			return parent::update($package);
		}

		public function install(string $package): bool
		{
			$version = array_get(Utils::getMetaFromRPM('ImageMagick'), 'version', '0.0');

			if (Versioning::compare($version, '7.0', '>=')) {
				return true;
			}

			return parent::install($package);
		}


	}