<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */


	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	/**
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 */
	class Postgresql95 extends Postgresql10
	{
	}