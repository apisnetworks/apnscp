<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2024
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use CLI\Yum\Synchronizer\Utils;

	/**
	 * Class Nss
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 *
	 */
	class ImageMagickLibs extends Trigger
	{
		public function update(string $package): bool
		{
			$files = Utils::getFilesFromRPM('ImageMagick-libs');
			$file = array_first($files, fn($file) => fnmatch('*/libMagickCore-*.so.*', $file));
			\apnscpFunctionInterceptor::init()->misc_release_fsghost($file);
			return parent::update($package);
		}
	}