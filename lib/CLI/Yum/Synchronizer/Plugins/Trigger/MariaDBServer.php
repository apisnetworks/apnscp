<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use Opcenter\Database\MySQL;
	use Opcenter\Versioning;

	/**
	 * Class MariaDBServer
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * Remove "mysql" alias that can cause
	 * conflict with systemd forwarding
	 *
	 */
	class MariaDBServer extends Trigger
	{
		const BITMASK_UPGRADE_MAP = [
			'10.5.8' => 274877906943,
			'10.5.9' => 549755813887
		];

		public function install(string $package): bool
		{
			$svc = '/etc/init.d/mysql';
			if (file_exists($svc) && is_file($svc)) {
				unlink($svc);
			}
			return parent::install($package);
		}

		public function update(string $package): bool
		{
			$this->install($package);
			$proc = new \Util_Process();
			$proc->setEnvironment('HOME', '/root');
			// mysqld must be restarted first otherwise
			$bin = MySQL::version() >= 110100 ? 'mariadb_upgrade' : 'mysql_upgrade';
			$ret = $proc->run("/usr/bin/{$bin} -k --upgrade-system-tables");
			if (!$ret['success']) {
				warn('Failed to run %(bin)s: %(err)s', ['bin' => $bin, 'err' => coalesce($ret['stderr'], $ret['stdout'])]);
			}

			$this->hotfixAccessBitmask();

			return true;
		}

		/**
		 * 10.5.8 => 10.5.9 loses "ALL" meaning
		 */
		private function hotfixAccessBitmask()
		{
			$conn = \MySQL::initialize('localhost', \Mysql_Module::MASTER_USER, MySQL::rootPassword(), 'mysql');
			$version = Versioning::asPatch($conn->query("SELECT version() AS version;")->fetch_object()->version);

			if (!isset(self::BITMASK_UPGRADE_MAP[$version])) {
				return;
			}

			$query = sprintf(
				"UPDATE global_priv set Priv = JSON_SET(Priv, '\$.access', JSON_VALUE(Priv, '\$.access') | %s, '\$.version_id', %s) WHERE 
				User = 'root' AND Host = 'localhost'",
				self::BITMASK_UPGRADE_MAP[$version],
				sprintf("%02d%02d%02d", ...explode('.', $version)),
			);
			$conn->query($query);
			$conn->query("FLUSH PRIVILEGES;");
		}

	}