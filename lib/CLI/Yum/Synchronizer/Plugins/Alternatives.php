<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins;

	/**
	 * Limit plugin scope to package-level
	 *
	 * When used, a plugin will only apply per-package instead to all packages
	 *
	 * @package CLI\Yum\Synchronizer\Plugins
	 *
	 */

	use Opcenter\Filesystem;

	/**
	 * Class Alternatives
	 *
	 * Minimalist implementation of Debian's alternatives system
	 *
	 * @package CLI\Yum\Synchronizer\Plugins
	 */
	class Alternatives
	{
		use \FilesystemPathTrait;

		const ALTERNATIVE_CMD = '/usr/sbin/update-alternatives';

		private $name;
		private $service;

		public function __construct(string $service, string $name)
		{
			$path = $this->service_template_path($service);
			if (!file_exists($path)) {
				fatal("Unknown service name `%s' - service path does not exist", $service);
			}
			foreach (['/etc/alternatives', '/var/lib/alternatives'] as $dir) {
				if (!is_dir("{$path}/{$dir}") && !Filesystem::mkdir("{$path}/{$dir}")) {
					fatal("failed to create alternatives directory for `%s' service", $service);
				}
			}
			$this->name = $name;
			$this->service = $service;
		}

		/**
		 * Reconfigure alternative symlink maps
		 *
		 * @return bool
		 */
		public function auto(): bool
		{
			return \Util_Process::exec(array_merge($this->getCommand(), ['--auto', $this->name]))['success'];
		}

		/**
		 * Install alternative
		 *
		 * @param string $link
		 * @param string $path
		 * @param int    $priority
		 * @return bool
		 */
		public function install(string $link, string $path, int $priority = 10): bool
		{
			$dest = $this->service_template_path($this->service) . '/' . $link;
			$src = $path;
			$proc = \Util_Process::exec(array_merge($this->getCommand(false), ['--verbose', '--install',
				$dest,
				$this->name,
				$src,
				$priority
			]));
			// trust the system is correct
			$altpath = '/etc/alternatives/' . $this->name;
			$virtpath = $this->service_template_path($this->service) . $altpath;
			if (!file_exists($virtpath)) {
				symlink(readlink($altpath), $virtpath);
			}

			return $proc['success'];
		}

		private function getCommand($fullpath = false): array
		{
			return [
				self::ALTERNATIVE_CMD,
				'--altdir',
				($fullpath ? $this->service_template_path($this->service) : null) . '/etc/alternatives',
				'--admindir',
				$this->service_template_path($this->service) . '/var/lib/alternatives',
			];
		}

		/**
		 * Verify if an alternative is present
		 *
		 * @param string $link alternative link
		 * @param string $path target path
		 * @return bool
		 */
		public function verify(string $link, string $path): bool
		{
			$dest = ($this->service_template_path($this->service) . '/' . $link);

			return is_link($dest) && file_exists($dest);
		}

		public function remove(string $path): bool
		{
			return \Util_Process::exec(array_merge($this->getCommand(true), ['--remove', $this->name, $path]))['success'];
		}

		public function removeAll(): bool
		{
			return \Util_Process::exec(array_merge($this->getCommand(true),
				['--remove-all', $this->name]))['success'];
		}
	}