<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Filelist;

	use CLI\Yum\Synchronizer\Plugins\Filelist;

	/**
	 * Class PhpAnyVersion
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Filelist
	 *
	 * Override filelist
	 *
	 */
	class PhpAnyVersion extends Filelist
	{
		use \NamespaceUtilitiesTrait;

		const SCL_PREFIX = '/etc/scl/prefixes';
		/**
		 * Get updated filelist
		 *
		 * @param array $files
		 * @return array
		 */
		public function filterFiles(array $files): array
		{
			// SCL configuration covered by trigger
			return [];
		}
	}