<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */


	namespace CLI\Yum\Synchronizer\Plugins;

	class Manager
	{
		use \NamespaceUtilitiesTrait;

		const CACHE_KEY = 'yum.plugins';
		const GLOBAL_SCOPE = '.';

		protected function __construct()
		{

		}

		/**
		 * Run all plugin hooks
		 *
		 * @param string $package
		 * @param string $mode
		 */
		public static function run(string $package, string $mode): void
		{
			$runagainst = self::getPluginsFromPackage($package);
			/**
			 * Possible future optimization...
			 */
			$classes = [];
			foreach ($runagainst as $plugin) {
				$class = self::appendNamespace($plugin);
				if (!isset($classes[$plugin])) {
					$classes[$plugin] = new $class;
				}
				$instance = $classes[$plugin];
				if (!method_exists($instance, $mode)) {
					continue;
				}

				$ret = $instance->$mode($package);
				info("Running `%s' (mode: %s) plugin against `%s': (return `%s')",
					$plugin,
					$mode,
					$package,
					$ret ? 'true' : 'false'
				);
			}
		}

		/**
		 * Instantiate plugin
		 *
		 * @param string $package
		 * @param string $plugin
		 * @return PluginInterface
		 */
		public static function instantiatePlugin(string $package, string $plugin): PluginInterface
		{
			$class = self::appendNamespace(studly_case($plugin) . '\\' . studly_case($package));
			return new $class;
		}

		/**
		 * Calculate plugins to apply to package
		 *
		 * @param string $package
		 * @return array
		 */
		protected static function getPluginsFromPackage(string $package): array
		{
			$plugins = self::getPlugins();
			$packageplugin = studly_case($package);

			return array_merge(
				array_get($plugins, self::GLOBAL_SCOPE, []),
				array_get($plugins, $packageplugin, [])
			);
		}

		protected static function getPlugins()
		{
			$cache = \Cache_Global::spawn();
			$plugins = $cache->get(self::CACHE_KEY);
			if (is_debug() || false === $plugins) {
				$plugins = self::loadPlugins();
				$cache->set(self::CACHE_KEY, $plugins, 60);
			}

			return $plugins;
		}

		/**
		 * Load all installed Yum plugins
		 *
		 * @return array
		 */
		protected static function loadPlugins(): array
		{
			$plugins = [
				// global plugins
				self::GLOBAL_SCOPE => []
			];
			$dh = opendir(__DIR__);
			if (!$dh) {
				fatal('failed to load Synchronizer plugins');
			}
			while (false !== ($file = readdir($dh))) {
				if ($file === '.' || $file === '..') {
					continue;
				} else if (substr($file, -4) !== '.php') {
					continue;
				}
				$class = substr($file, 0, -4);
				$rfxn = new \ReflectionClass(self::appendNamespace($class));
				if (!$rfxn->implementsInterface(PluginInterface::class) || $rfxn->isInterface()) {
					continue;
				}
				if (!$rfxn->hasMethod('getScope')) {
					$plugins[self::GLOBAL_SCOPE][] = $class;
					continue;
				}
				$scope = self::appendNamespace($class)::getScope();
				switch ($scope) {
					case 'global':
						$plugins[self::GLOBAL_SCOPE][] = $class;
						continue 2;
					case 'package':
						$plugins = array_merge_recursive($plugins, self::loadPackagePlugin($class));
						continue 2;
					default:
						fatal("unknown plugin scope `%s' for plugin `%s'", $scope, $class);
				}
			}
			closedir($dh);
			return $plugins;
		}

		/**
		 * Plugin exists for package
		 *
		 * @param string $package
		 * @param string $plugin
		 * @return bool
		 */
		public static function hasPlugin(string $package, string $plugin): bool
		{
			$plugins = self::getPluginsFromPackage($package);
			$pluginName = studly_case($plugin) . '\\' . studly_case($package);
			return \in_array($pluginName, $plugins);
		}
		/**
		 * Load package-specific plugins
		 *
		 * @param string $plugin
		 * @return array
		 */
		protected static function loadPackagePlugin(string $plugin): array
		{
			$path = __DIR__ . DIRECTORY_SEPARATOR . ucwords($plugin);
			if (!is_dir($path)) {
				// No packages provided for plugin scope
				return [];
			}
			$dh = opendir($path);
			if (!$dh) {
				fatal("failed to open plugin scope directory `%s' for plugin `%s'", $path, $plugin);
			}
			$plugins = [];
			while (false !== ($file = readdir($dh))) {
				if ($file === '.' || $file === '..') {
					continue;
				} else if (substr($file, -4) !== '.php') {
					continue;
				}
				$pkgscope = substr($file, 0, -4);
				$class = self::appendNamespace($plugin . '\\' . $pkgscope);
				$rfxn = new \ReflectionClass($class);
				if (!$rfxn->implementsInterface(PluginInterface::class)) {
					error("problem loading package scope `%s' for plugin `%s' - missing PluginInterface",
						substr($plugin, 0, -4), $plugin);
					continue;
				}
				$plugins[$pkgscope] = [
					$plugin . '\\' . $pkgscope
				];
			}
			closedir($dh);

			return $plugins;
		}


	}