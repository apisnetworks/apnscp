<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2022
	 */

	namespace CLI\Transfer;

	use CLI\Output;
	use Opcenter\Provisioning\ConfigurationWriter;
	use Opcenter\SiteConfiguration;

	class Notify {
		use \ContextableTrait;
		use \apnscpFunctionInterceptorTrait;

		protected \CLI_Transfer $xfer;
		// view template
		protected string $template;
		protected string $subject;

		const TEMPLATES = [
			'initial',
			'completed',
			'failed'
		];
		private Output $log;

		protected function __construct(\CLI_Transfer $xfer)
		{
			$this->xfer = $xfer;
			$this->template = $xfer->migrated() ? 'completed' : 'initial';
		}

		public function setTemplate(string $template): self
		{
			$this->template = $template;
			return $this;
		}

		public function setSubject(string $subject): self
		{
			$this->subject = $subject;
			return $this;
		}

		public function setLog(Output $log): self
		{
			$this->log = $log;
			return $this;
		}

		public function getSubject(): string
		{
			return $this->subject;
		}

		public function send(array $bcc = null): bool
		{

			if (null === $bcc && \CLI_Transfer::STATUS_EMAIL) {
				$bcc = [\CLI_Transfer::STATUS_EMAIL];
			}
			$mail = \Illuminate\Support\Facades\Mail::to($this->common_get_email())->bcc($bcc);

			$domain = $this->xfer->getAuthContext()->domain;
			$msg = (new Simple('email.migration.completed',
				$vars))->setSubject(\ArgumentFormatter::format("Migration Completed - %s", [$domain]));
			$msg->attachData(implode("\r\n", $this->getBuffer()), 'migration-log.txt', [
				'mime' => 'text/plain'
			]);

			return true;
		}

		private function makeOldMigration() {
			$email = (array)$this->_cfg['siteinfo']['email'];
			$domain = $this->_domain ;
			$server = $this->getTargetServer();
			$oldserver = $this->getSourceServer();
			$proxy = null;

			$vars = [
				'panelurl'  => 'https://' . \Auth_Redirect::makeHostFromServer($server),
				'newserver' => $server,
				'oldserver' => $oldserver,
				'domain'    => $domain
			];

			$mail = \Illuminate\Support\Facades\Mail::to($email)->bcc(self::STATUS_EMAIL);

			$msg = (new Simple('email.migration.completed',
				$vars))->setSubject(\ArgumentFormatter::format("Migration Completed - %s", [$domain]));
			$msg->attachData(implode("\r\n", $this->getBuffer()), 'migration-log.txt', [
				'mime' => 'text/plain'
			]);
		}

		private function substitute(string $input): string
		{
			$server = $this->xfer->getTargetServer();
			$destServer = $server;
			$destServerShort = $server;

			return (new ConfigurationWriter($this->template, SiteConfiguration::shallow($this->getAuthContext())))->compile([
				'xfer'              => $this->xfer,
				'destinationServer' => $destServer,
				'destinationShort'  => $destServerShort,
				'domain'            => $this->xfer->getAuthContext()->domain,
				'sourceServer'      => $this->xfer->getSourceServer(),
				'sourceServerShort' => $this->xfer->getSourceServer(),
				'destinationIp'     => $this->xfer->getRemoteIP()
			]);
		}
	}