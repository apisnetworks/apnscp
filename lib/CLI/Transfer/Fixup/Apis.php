<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Adjustment filter for Apis Networks migration sources
	 *
	 * @author Matt Saladna <matt@apisnetworks.com>
	 */
	class CLI_Transfer_Fixup_Apis extends CLI_Transfer_Fixup
	{
		public function upgrade($cfg): array
		{
			$state = $this->getPlatformState();
			// @XXX always put smaller versions before in upgrade
			if ($state < 4.5) {
				if (isset($cfg['tomcat']['enabled'])) {
					$this->log('enabling pgsql config');
					$cfg['pgsql'] = array('enabled' => 1);
				}
				$this->setPlatformState(4.5);

				return $cfg;
			}

			if ($state < 5) {
				if (isset($cfg['tomcat4'])) {
					$this->log('renaming tomcat4 to tomcat config');
					$cfg['tomcat'] = $cfg['tomcat4'];
					unset($cfg['tomcat4']);
				}
				$this->setPlatformState(5);

				return $cfg;
			}

			if ($state < 6) {
				if (isset($cfg['scriptsmgr'])) {
					unset($cfg['scriptsmgr']);
				}

				$this->setPlatformState(6);

				return $cfg;
			}

			if ($state < 6.5) {
				if (isset($cfg['frontpage'])) {
					$cfg['frontpage']['enabled'] = 0;
				}
				$this->setPlatformState(6.5);

				return $cfg;
			}

			if ($state < 7) {
				if (isset($cfg['urchin'])) {
					unset($cfg['urchin']);
				}
				$this->setPlatformState(7);

				return $cfg;
			}

			if ($state < 7.5) {
				$defaults = (new \Opcenter\SiteConfiguration(''))->getDefaultConfiguration();
				foreach ($cfg as $svc => $vars) {
					if ($svc !== ($svcren = \Opcenter\SiteConfiguration::getModuleRemap($svc))) {
						// module remap
						$cfg[$svcren] = $cfg[$svc];
						unset($cfg[$svc]);
						$svc = $svcren;
					} else if (!isset($defaults[$svc])) {
						unset($cfg[$svc]);
						continue;
					}
					$cfg[$svc] = array_intersect_key($cfg[$svc], $defaults[$svc]);
					$cfg[$svc]['version'] = null;
				}
				unset($cfg['siteinfo']['tpasswd'], $cfg['siteinfo']['passwd'], $cfg['siteinfo']['cpasswd']);
				// Ensim defaulted to 1
				if (1 === array_get($cfg, 'mysql.dbasenum', null)) {
					$cfg['mysql']['dbasenum'] = null;
				}
				// let the server figure this out
				unset($cfg['apache']['jail']);
				// always bytes
				if (isset($cfg['users']['maxusers'])) {
					array_set($cfg, 'users.max', array_pull($cfg, 'users.maxusers'));
				}

				$cfg['bandwidth']['units'] = 'B';
				$cfg['users']['enabled'] = 1;
				$this->setPlatformState(7.5);

				return $cfg;
			}

			$state += self::MAX_STEP;
			$this->setPlatformState($state);

			return $cfg;
		}

		public function downgrade($cfg): array
		{
			$state = $this->getPlatformState();
			// @XXX always put larger versions before in downgrade
			if ($state > 7.5) {
				unset(
					$cfg['rampart'], $cfg['billing']['ctime'], $cfg['cgroup']['ioweight'],
					$cfg['cgroup']['writebw'], $cfg['cgroup']['readbw'],
					$cfg['mlist']['max'], $cfg['cgroup']['writeiops'],
					$cfg['cgroup']['readiops'], $cfg['apache']['subnum'],
					$cfg['apache']['jail'], $cfg['auth']['iprestrict']
				);
				foreach ($cfg as $svc => $svcs) {
					unset($cfg[$svc]['version']);
				}

				$this->setPlatformState(7.5);

				return $cfg;
			}

			if ($state > 7) {
				if (null === array_get($cfg, 'mysql.dbasenum', false)) {
					$cfg['mysql']['dbasenum'] = 1;
				}
				unset($cfg['ipinfo6']);
				foreach (\Opcenter\SiteConfiguration::MODULE_REMAP as $old => $new) {
					if (isset($cfg[$new])) {
						$cfg[$old] = $cfg[$new];
						unset($cfg[$new]);
					}
				}
				if (isset($cfg['users']['max'])) {
					array_set($cfg, 'users.maxusers', array_pull($cfg, 'users.max'));
				}
				$this->setPlatformState(7);

				return $cfg;
			}

			if ($state > 6) {
				if (isset($cfg['cgroup'])) {
					$this->log('removing cgroup service');
					unset($cfg['cgroup']);
				}
				$this->setPlatformState(6);

				return $cfg;
			}

			if ($state > 4.5) {
				if (isset($cfg['tomcat'])) {
					$this->log('renaming tomcat4 to tomcat config');
					$cfg['tomcat4'] = $cfg['tomcat'];
					unset($cfg['tomcat']);
				}

				$this->setPlatformState(4.5);

				return $cfg;
			}

			if ($state > 4) {
				if (isset($cfg['pgsql'])) {
					unset($cfg['pgsql']);
					$this->log('removing pgsql config');
				}
				$this->setPlatformState(4);

				return $cfg;
			}

			$state -= self::MAX_STEP;
			$this->setPlatformState($state);

			return $cfg;
		}


	}
