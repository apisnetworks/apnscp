<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, September 2024
	 */

	namespace CLI;

	use FilesystemPathTrait;

	class MockFilesystem {
		use FilesystemPathTrait;

		public function __construct(protected $site = 'site0') {}

		public function root(): string
		{
			return $this->domain_fs_path();
		}

		public function shadow(): string
		{
			return $this->domain_shadow_path();
		}

		public function info(): string
		{
			return $this->domain_info_path();
		}

		public function set(string $site): self
		{
			$new = clone $this;
			$new->site = $site;

			return $new;
		}

		public function get(string $service): string
		{
			return $this->domain_info_path("current/{$service}");
		}
	};