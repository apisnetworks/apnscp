<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Cache_Global extends Cache_Base
	{
		static public $key = 'G:';

		public static function autoload_init()
		{
			self::reset_prefix();
		}

		public static function reset_prefix()
		{
			self::set_key(self::$key);
		}

		public static function set_key($key)
		{
			self::$key = $key;
		}
	}