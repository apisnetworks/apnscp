<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2021
 */

namespace Frontend;

class Tour {
	const KEY = 'tour';
	const ALWAYS_SHOW = 'tour._always';

	protected \Page_Container $page;

	public function __construct(\Page_Container $page) {
		$this->page = $page;
	}

	public function embed(array $opts = []): void
	{
		$encoded = $this->build() + $opts;
		if (!$encoded) {
			return;
		}
		$this->page->add_javascript('apnscp.tour().onexit(function () {
				apnscp.cmd("common_set_preference", ["' .
				self::KEY . '.' . $this->page->getApplicationID() . '", Date.now()]);
			}).setOptions(' . json_encode($encoded) . ').start();', 'internal', true);
	}

	/**
	 * Tour feature exists for app
	 *
	 * @return bool
	 */
	public function exists(): bool
	{
		$help = $this->page->appHelp();
		return isset($help['tour']);
	}

	public function build(): array
	{
		if (!$this->exists()) {
			return [];
		}

		$help = $this->page->appHelp();

		$encoded = ['steps' => []];
		foreach ($help['tour'] as $element) {
			$step = $element + [
				'title' => '',
				'intro' => '',
				'position' => 'auto'
			];

			if (null !== ($when = $this->pluck($element, 'when')) &&
				!\Util_Conf::inferType(trim($when)))
			{
				continue;
			}
			unset($step['when']);

			if (null !== ($title = $this->pluck($element, 'title'))) {
				$step['title'] = $title;
			}

			if (null !== ($content = $this->pluck($element, 'content'))) {
				$step['intro'] = $content;
			}

			if (null !== ($selector = $this->pluck($element, 'selector'))) {
				$step['element'] = $selector;
			}

			$encoded['steps'][] = $step;
		}
		$encoded['buttonClass'] = 'btn';
		return $encoded;
	}

	/**
	 * Parse field from yaml
	 * @param array  $el
	 * @param string $key
	 */
	private function pluck(array $el, string $key) {
		/**
		 * @XXX ugly. Adding Blade to application payloads requires
		 *      two passes, first from Laravel to transpile to PHP
		 *      then eval()'ing for native JSON encoding.
		 */
		if (empty($el[$key])) {
			return null;
		}
		ob_start();
		eval('?>' . $el[$key]);
		return ob_get_clean();
	}
}