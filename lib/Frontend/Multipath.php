<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2024
 */

namespace Frontend;

class Multipath
{
	const MULTIPATH_PREFIX = 'id_';

	public static function enabled(): bool
	{
		return (int)FRONTEND_MULTIPATH_LENGTH > 0;
	}

	public static function prefix(): string
	{
		if ((int)FRONTEND_MULTIPATH_LENGTH < 1) {
			return '';
		}

		return  '/' . self::MULTIPATH_PREFIX . substr(session_id(), 0, (int)FRONTEND_MULTIPATH_LENGTH);
	}

	public static function strip(string $uri): string
	{
		if (!self::enabled() || !str_starts_with($uri, self::prefix())) {
			return $uri;
		}
		return substr($uri, (int)strpos($uri, '/', 1));
	}

	public function has(string $uri): bool
	{
		if (!self::enabled()) {
			return false;
		}

		return str_starts_with($uri, self::prefix());
	}
}