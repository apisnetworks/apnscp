<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, January 2024
 */


namespace Module\Support\Php;

use PhpParser\Node;
use PhpParser\NodeAbstract;
use PhpParser\NodeFinder;
use PhpParser\ParserFactory;
use PhpParser\PhpVersion;

abstract class TreeWalker
{
	use \apnscpFunctionInterceptorTrait;
	use \ContextableTrait;

	/**
	 * @var \PhpParser\Node\Stmt[]
	 */
	protected $ast;
	/**
	 * @var \PhpParser\NodeTraverser
	 */
	protected $traverser;

	/** @var string filename */
	protected $file;

	/**
	 * Util_AST constructor.
	 *
	 * @param string $file
	 * @throws \ArgumentError
	 * @throws \PhpParser\Error
	 */
	protected function __construct(string $file)
	{
		if (!$this->file_exists($file)) {
			throw new \ArgumentError(\ArgumentFormatter::format("Target file %s does not exist", [$file]));
		}
		$code = $this->file_get_file_contents($this->file = $file);

		$instance = (new \PhpParser\ParserFactory);
		if (method_exists($instance, 'create')) {
			$parser = $instance->create(ParserFactory::PREFER_PHP7);
		} else {
			$parser = $instance->createForVersion(PhpVersion::fromString($this->php_version()));
		}
		$this->ast = $parser->parse($code);
	}

	/**
	 * Replace matching define() rules
	 *
	 * @param string $var search variable
	 * @param mixed  $new replacement value
	 * @return self
	 */
	abstract public function replace(string $var, mixed $new): self;

	/**
	 * Set matching define() statements or add
	 *
	 * @param string $var search variable
	 * @param mixed  $new replacement value
	 * @return self
	 */
	abstract public function set(string $var, mixed $new): self;

	/**
	 * Get value from AST
	 *
	 * @param string $var
	 * @param mixed  $default
	 * @return mixed|null
	 */
	abstract public function get(string $var, mixed $default = null): mixed;

	protected function inferType($val): NodeAbstract
	{
		return \PhpParser\BuilderHelpers::normalizeValue($val);
	}

	/**
	 * Generate configuration
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (new \PhpParser\PrettyPrinter\Standard())->prettyPrint(
			$this->ast
		);
	}

	public function first(\Closure $expr): ?Node
	{
		return (new NodeFinder)->findFirst($this->ast, $expr);
	}

	/**
	 * Save configuration
	 *
	 * @return bool
	 */
	public function save(): bool
	{
		return $this->file_put_file_contents($this->file, '<?php' . "\n" . (string)$this);
	}
}