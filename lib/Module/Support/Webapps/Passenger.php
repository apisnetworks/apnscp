<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Module\Support\Webapps;

	use Opcenter\Net\Port;

	/**
	 * Class Passenger
	 *
	 * @see     https://www.phusionpassenger.com/library/config/standalone/intro.html Passenger Reference
	 * @package Module\Support\Webapps
	 */
	class Passenger
	{
		use \PropertyMutatorAccessorTrait;
		use \FilesystemPathTrait;
		use \apnscpFunctionInterceptorTrait;
		use \ContextableTrait;

		const PASSENGER_MODES = ['apache', 'standalone', 'nginx'];
		const PASSENGER_TYPES = ['ruby', 'python', 'nodejs', 'meteor'];
		const PASSENGER_DIRECTORIES = ['public', 'tmp', 'log'];
		/**
		 * @var array remap Passenger command-line options to internal properties
		 */
		private const VARIABLE_REMAP_DICT = [
			'force-max-concurrent-requests-per-process' => 'maxConcurrentProcesses',
			'envvars'                                   => 'environment',
			'environment'                               => 'applicationEnvironment'
		];
		protected $engine = 'apache';
		/**
		 * @var string startup file
		 */
		protected $startupFile;
		/**
		 * @var string passenger app type ruby, python, nodejs, or meteor
		 */
		protected $appType;
		/**
		 * @var string application root
		 */
		protected $appRoot;

		/**
		 * @var string interpreter version
		 */
		protected $interpreterVersion = 'lts';

		/**
		 * @var string application environment
		 */
		protected $applicationEnvironment = 'production';

		/**
		 * @var array additional environment variables
		 */
		protected $environment;

		/**
		 * @var string protocol
		 */
		protected $protocol = 'http';

		/**
		 * @var string Passenger address in nginx/standalone mode
		 */
		protected $address = '127.0.0.1';
		/**
		 * @var int port Passenger listens on in nginx/standalone mode
		 */
		protected $port;

		/**
		 * @var string $spawnMethod
		 */
		protected $spawnMethod = 'smart';

		/**
		 * @var int $maxConcurrentProcesses override maximum concurrent processes for multi-threaded applications
		 */
		protected $maxConcurrentProcesses;

		/**
		 * @var int minimum Passenger instances for app
		 */
		protected $minInstances = 3;

		/**
		 * @var int maximum Passenger instances
		 */
		protected $maxPoolSize = 6;

		/**
		 * @var array unknown vars
		 */
		protected $unknownVars = [];
		/**
		 * @var string Passenger interpreter
		 */
		protected $interpreter;

		/**
		 * Passenger constructor.
		 *
		 * @param string $appRoot
		 * @param string $appType
		 */
		public function __construct(string $appRoot, ?string $appType)
		{
			$this->bindPathContext($this->getAuthContext());
			$this->appRoot = $appRoot;
			$this->appType = $appType;

			if (null === $this->appType) {
				$this->inferFromJson();
			}

			if (!\in_array($this->appType, static::PASSENGER_TYPES, true)) {
				fatal("Unknown Passenger app type `%s'", $this->appType);
			}
			$this->startupFile = $this->getDefaultStartup();
			$this->protocol = $appType === 'meteor' ? 'ws' : 'http';
		}

		public function inferFromJson(string $filename = 'Passengerfile.json'): self
		{
			$vpath = $this->appRoot . "/{$filename}";
			if (!$this->file_exists($vpath)) {
				warn("Cannot load Passenger settings - `%s' missing", $vpath);

				return $this;
			}
			if (null === ($json = json_decode($this->file_get_file_contents($vpath), true))) {
				fatal("Corrupt/unsupported JSON syntax located in `%s'", $vpath);
			}
			foreach ($json as $k => $v) {
				if ($k === 'ruby' || $k === 'python' || $k === 'nodejs') {
					$this->appType = $k;
					$this->setInterpreter($v);
					continue;
				}
				if (isset(static::VARIABLE_REMAP_DICT[$k])) {
					$varname = static::VARIABLE_REMAP_DICT[$k];
				} else {
					$varname = camel_case($k);
				}
				if (!property_exists($this, $varname)) {
					warn("Unknown Passenger configuration var `%s', skipping", $k);
					$this->unknownVars[$k] = $v;
					continue;
				}
				$this->__set($varname, $v);
			}

			return $this;
		}

		/**
		 * Get default startup for app type
		 *
		 * @return string
		 */
		public function getDefaultStartup(): string
		{
			switch ($this->appType) {
				case 'meteor':
				case 'node':
					return 'app.js';
				case 'ruby':
					return 'config.ru';
				case 'python':
					return 'passenger_wsgi.py';
				default:
					return '';
			}
		}

		/**
		 * Load Passenger configuration from Passengerfile.json
		 *
		 * @param string          $path    approot
		 * @param \Auth_Info_User $context authentication context
		 * @return Passenger|null
		 */
		public static function load(string $path, \Auth_Info_User $context): self
		{
			return static::instantiateContexted($context, [$path, null]);
		}

		/**
		 * Passenger mode is valid
		 *
		 * @param string $mode
		 * @return bool
		 */
		public static function modeValid(string $mode): bool
		{
			return \in_array($mode, static::PASSENGER_MODES, true);
		}

		/**
		 * Create Passenger-compatible layout
		 *
		 * @return bool
		 */
		public function createLayout(): bool
		{
			$stat = $this->file_stat($this->appRoot);
			if (!$stat) {
				return error("App root `%s' does not exist", $this->appRoot);
			}
			if ($stat['uid'] < \User_Module::MIN_UID) {
				$user = $this->getAuthContext()->username;
			} else {
				$user = $stat['owner'];
			}
			foreach (['tmp', 'public', 'logs'] as $dir) {
				($this->file_create_directory($this->appRoot . "/{$dir}") &&
					$this->file_chown($this->appRoot . "/{$dir}", $user)
				) || warn("failed to create application directory `%s/%s'", $this->appRoot, $dir);
			}

			return true;
		}

		public function getDirectives(): string
		{
			if ($this->engine === 'apache') {
				// Dammit, Phusion.
				$typeDirective = 'Passenger' . ($this->appType === 'node' ? 'Nodejs' : ucwords($this->appType));

				return 'PassengerEnabled on' . "\n" .
					'PassengerStartupFile ' . $this->getStartupFile() . "\n" .
					'PassengerAppType ' . $this->appType . "\n" .
					'PassengerAppEnv production' . "\n" .
					$typeDirective . ' ' . $this->getInterpreter() . "\n" .
					'PassengerAppRoot ' . $this->appRoot . "\n";
			}

			return 'RewriteEngine On' . "\n" .
				'RewriteCond %{REQUEST_FILENAME} !-f' . "\n" .
				'RewriteRule ^(.*)$ ' . $this->getProtocol() . '://localhost:' . $this->getPort() . '/$1 [P,L,QSA]' . "\n";
		}

		/**
		 * Get startup file
		 *
		 * @return string
		 */
		public function getStartupFile(): string
		{
			return $this->startupFile;
		}

		/**
		 * Get interpreter
		 *
		 * @return string
		 */
		public function getInterpreter(): string
		{
			if (null !== $this->interpreter) {
				return $this->interpreter;
			}
			$ret = match($this->appType) {
				'nodejs', 'meteor' => $this->node_do($this->interpreterVersion, $this->appRoot, 'which node'),
				'ruby' => $this->ruby_direct('which', $this->interpreterVersion, $this->appRoot, 'ruby'),
				'python' => $this->python_direct('which', $this->interpreterVersion, $this->appRoot, 'python'),
				default => fatal("Unknown/unrecognized app type `%s'", $this->appType)
			};

			return $ret['success'] ? trim($ret['stdout']) : (string)error($ret['stderr']);
		}

		public function setVersion(string $version): self
		{
			$this->interpreterVersion = $version;
			return $this;
		}

		/**
		 * Set interpreter for app
		 *
		 * @param $path
		 */
		public function setInterpreter($path): self
		{
			$this->interpreter = $path;
			return $this;
		}

		/**
		 * Get protocol type
		 *
		 * @return string http or ws
		 */
		public function getProtocol(): string
		{
			return $this->protocol;
		}

		public function getPort(): ?int
		{
			if ($this->engine === 'apache') {
				return null;
			}
			if (null === $this->port) {
				// @todo possible race condition, listen on port, Port::reserve()?
				$port = Port::firstFree($this->getAuthContext());
				if (!$port) {
					error('Failed to locate free port on account');

					return null;
				}
				$this->port = $port;
			}

			return $this->port;
		}

		public function __debugInfo()
		{
			$json = \json_decode($this->getExecutableConfiguration(), true);
			if (!\is_array($json)) {
				return null;
			}

			return $json;
		}

		/**
		 * Save configration to startup
		 *
		 * @return bool
		 */
		public function getExecutableConfiguration(): ?string
		{
			if ($this->engine === 'apache') {
				// no use
				return null;
			}
			$options = $this->getPassengerOptions();
			$json = [];
			foreach ($options as $k => $v) {
				if (null === $v) {
					$v = true;
				}
				$json[str_replace('-', '_', $k)] = $v;
			}

			return \json_encode($json + $this->unknownVars, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
		}

		protected function getPassengerOptions(): array
		{
			$args = [
				$this->appType  => $this->getInterpreter(),
				'app-type'      => $this->appType,
				'startup-file'  => $this->appRoot . '/' . $this->getDefaultStartup(),
				'environment'   => $this->getApplicationEnvironment(),
				'auto'          => null,
				'engine'        => $this->engine === 'standalone' ? 'builtin' : $this->engine,
				'min-instances' => $this->minInstances,
				'max-pool-size' => $this->maxPoolSize,
				'daemonize'     => null,
				'spawn-method'  => $this->getSpawnMethod(),
				'address'       => '127.0.0.1',
				'port'          => $this->getPort(),
				'envvars'       => (array)$this->getEnvironment()
			];

			if (null !== $this->getProcessConcurrency()) {
				$args['force-max-concurrent-requests-per-process'] = $this->getProcessConcurrency();
			}
			if ($this->engine === 'nginx') {
				$args['static-files-dir'] = $this->getPublic();
			}

			return $args;
		}

		/**
		 * Get application environment (production, development)
		 *
		 * @return string
		 */
		public function getApplicationEnvironment(): string
		{
			return $this->applicationEnvironment;
		}

		/**
		 * Set application environment (production, development)
		 *
		 * @param string $name
		 */
		public function setApplicationEnvironment(string $name): void
		{
			$this->applicationEnvironment = $name;
		}

		public function getSpawnMethod(): string
		{
			return $this->spawnMethod;
		}

		public function setSpawnMethod(string $type): void
		{
			if ($type !== 'smart' && $type !== 'direct') {
				fatal("Unknown spawn method `%s'", $type);
			}
			$this->spawnMethod = $type;
		}

		public function getEnvironment(): ?array
		{
			return $this->environment;
		}

		/**
		 * Set environment variable
		 *
		 * @param mixed ...$args
		 */
		public function setEnvironment(...$args): void
		{
			$argcnt = \count($args);
			if ($argcnt === 2 && !array_filter('is_array', $args)) {
				// setEnvironment('FOO', 'BAR')
				$args = [$args[0] => $args[1]];
			} else if ($argcnt === 1 && !\is_array($args[0])) {
				// setEnvironment('FOO'), same as 'FOO' => null, no value but pass
				$args = [$args[0] => null];
			} else {
				$args = $args[0];
			}
			$this->environment = $args + (array)$this->environment;
		}

		/**
		 * Get maximum process concurrency
		 *
		 * @return int|null
		 */
		public function getProcessConcurrency(): ?int
		{
			return $this->maxConcurrentProcesses;
		}

		/**
		 * Get public path
		 *
		 * @return string
		 */
		public function getPublic(): string
		{
			return $this->appRoot . '/public';
		}

		public function __toString()
		{
			return (string)$this->getExecutableConfiguration();
		}

		public function setMinInstances(int $n): void
		{
			if ($n > $this->maxPoolSize) {
				error('Cannot set min instances greater than max pool size');

				return;
			}
			$this->minInstances = $n;
		}

		public function setMaxPoolSize(int $n): void
		{
			if ($n < $this->minInstances) {
				error('Cannot set pool size less than min instances');

				return;
			}
			$this->maxPoolSize = $n;
		}

		/**
		 * Set or clean maximum process concurrency
		 *
		 * @param int|null $n
		 */
		public function setProcessConcurrency(?int $n): void
		{
			if (null !== $n && $n < -1) {
				fatal('Process concurrency must be an integer greater or equal to -1');
			} else if ($n > 4096) {
				warn('Large concurrency provided (%d), capping at 4096', $n);
				$n = 4096;
			}
			$this->maxConcurrentProcesses = $n;
		}

		/**
		 * Restart Passenger application
		 *
		 * @return bool
		 */
		public function restart(): bool
		{
			return (bool)$this->file_touch($this->appRoot . '/tmp/restart.txt');
		}

		public function saveExecutableConfiguration(): bool
		{
			if ($this->getEngine() === 'apache') {
				return true;
			}

			return $this->file_put_file_contents($this->appRoot . '/Passengerfile.json',
				$this->getExecutableConfiguration(), true);
		}

		public function getEngine(): string
		{
			return $this->engine;
		}

		/**
		 * Set passenger mode
		 *
		 * @param string $engine mode type apache/standalone/nginx
		 * @return void
		 */
		public function setEngine(string $engine): void
		{
			if (!\in_array($engine, static::PASSENGER_MODES, true)) {
				fatal("Unknown Passenger mode `%s'", $engine);
			}
			$this->engine = $engine;
		}

		/**
		 * Start Passenger application
		 *
		 * @return bool
		 */
		public function start(): bool
		{
			if (null === ($exec = $this->getExecutable())) {
				// Apache
				return true;
			}
			$ret = $this->pman_run($exec);

			return $ret['success'];
		}

		/**
		 * Get executable
		 *
		 * @return null|string
		 */
		public function getExecutable(): ?string
		{
			if ($this->engine === 'apache') {
				return null;
			}

			return 'cd ' . escapeshellarg($this->appRoot) .
				' && /bin/bash -ic "env NOEXEC_EXCLUDE=passenger passenger start"';
		}

		/**
		 * Stop Passenger application
		 *
		 * @return bool
		 */
		public function stop(): bool
		{
			if ($this->engine === 'apache') {
				return true;
			}
			$ret = $this->pman_run('cd %(approot)s && /bin/bash -ic "passenger stop"', ['approot' => $this->appRoot]);

			return $ret['success'] ? true : warn('Failed to stop Passenger app: %s', $ret['stderr']);
		}
	}