<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Module\Support\Webapps;

	use Illuminate\Foundation\Bus\PendingDispatch;
	use Lararia\Jobs\Job;
	use Lararia\Jobs\Traits\NotifyJob;
	use Lararia\Jobs\UpdateAppJob;
	use Module\Support\Webapps;
	use Module\Support\Webapps\App\Loader;
	use Opcenter\Account\Enumerate;

	class Updater
	{
		use \apnscpFunctionInterceptorTrait, NotifyJob;
		/**
		 * @var Updater
		 */
		protected static $instance;
		/**
		 * @var array collected sites
		 */
		protected $sites = [];
		/**
		 * @var array collected app versions
		 */
		protected $appVersion = [];
		/**
		 * @var array app instance cache
		 */
		protected $appInstanceCache = [];
		/**
		 * Limit update in batches
		 *
		 * @param int|null
		 */
		protected $limit;
		/**
		 * Limit update to site
		 *
		 * @var string
		 */
		protected $site;

		/**
		 * Limit update to assets
		 *
		 * @var
		 */
		protected $updateAssets = false;

		/**
		 * Limit update to core only
		 *
		 * @var bool
		 */
		protected $updateCore = true;

		/**
		 * Default app selector
		 *
		 * @var array
		 */
		protected $filter = [Finder::class, 'filterActive'];

		protected function __construct(int $limit = null)
		{
			$this->limit = $limit;
			$this->queryApplicationVersions();
		}

		protected function queryApplicationVersions(): void
		{
			foreach (App\Loader::getKnownApps() as $app) {
				$instance = $this->loadInstanceFromApp($app);
				$versions = $instance->getVersions();
				$this->appVersion[$app] = $versions;
			}

		}

		protected function loadInstanceFromApp(string $app)
		{
			if (!isset($this->appInstanceCache[$app])) {
				$this->appInstanceCache[$app] = Loader::fromDocroot($app, null);
			}

			return $this->appInstanceCache[$app];
		}

		/**
		 * @param null $limit
		 * @return Updater|static
		 */
		public static function launch($limit = null)
		{
			if (null === self::$instance) {
				self::$instance = new static($limit);
			}

			return self::$instance;
		}

		public function batch(int $limit): self
		{
			if ($limit < 0) {
				fatal('nonsense batch limit `%d\'', $limit);
			}
			$this->limit = $limit;
			return $this;
		}

		/**
		 * Limit operation to a site
		 *
		 * @param string $anything
		 * @return self
		 */
		public function limitSite(string $anything): self
		{
			$id = \Auth::get_site_id_from_anything($anything);
			if (!$id) {
				fatal("unparseable site spec `%s' passed", $anything);
			}
			$this->site = 'site' . $id;
			return $this;
		}

		public function limitType(string $type): self
		{
			if (!App\Loader::fromDocroot($type, null)) {
				fatal("unknown web app type `%s'", $type);
			}
			$this->filter = static function ($meta) use ($type) {
				return Finder::filterActive($meta) &&
					$meta['type'] === $type;
			};
			return $this;
		}

		public function enableCoreUpdates(bool $flag): self
		{
			$this->updateCore = $flag;
			return $this;
		}

		/**
		 * Update assets
		 *
		 * @param bool $flag
		 */
		public function enableAssetUpdates(bool $flag): self
		{
			$this->updateAssets = $flag;
			return $this;
		}

		/**
		 * Run batch updater
		 *
		 * @return bool
		 */
		public function run(): bool
		{
			return \Error_Reporter::filter(
				fn() => $this->collectCandidates(),
				[':subdomain_is_dir']
			);

		}

		/**
		 * Collect all known webapps that may or may not be eligible for upgrade
		 */
		protected function collectCandidates(): ?bool
		{
			$counter = 0;
			foreach ($this->getSites() as $site) {
				$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
				try {
					$context = \Auth::context(null, $site);
				} catch (\apnscpException $e) {
					warn("Problems on %s. Skipping: %s", $site, $e->getMessage());
					continue;
				} finally {
					\Error_Reporter::exception_upgrade($oldex);
				}
				$afi = \apnscpFunctionInterceptor::factory($context);
				// ensure filesystem caches are current
				$afi->file_purge();
				foreach ($this->getApplications($context) as $fstPath => $candidateMeta) {
					if (!isset($candidateMeta['type'])) {
						warn("Junk preference detected on `%s' (%s), continuing", $fstPath, $site);
						var_dump($candidateMeta);
						continue;
					}
					$hostname = $candidateMeta['hostname'] ?? '';
					$path = $candidateMeta['path'] ?? '';
					if (empty($candidateMeta['hostname'])) {
						$hostname = $afi->web_get_hostname_from_docroot($fstPath);
						if ($hostname) {
							info("Found missing hostname `%s' for `%s'", $hostname, $fstPath);
							$pref = MetaManager::factory($context);
							$pref->replace($fstPath, ['hostname' => $hostname]);
							$pref = null;
						}
					}

					if (!$hostname || (!$path && !Finder::rootIsCurrent($fstPath, $hostname, $afi))) {
						$mgr = MetaManager::factory($context);
						$mgr->forget($fstPath);
						$mgr = null;
						info("Removing orphaned path `%s' formerly attached to `%s'", $fstPath, $hostname);
						continue;
					}

					if (($realpath = $afi->web_normalize_path($hostname, $path)) !== $fstPath) {
						info("Path `%s' for %s/%s does not match up with reported docroot `%s' - removing orphan path",
							$fstPath,
							$hostname,
							$candidateMeta['path'] ?? '',
							$realpath
						);
						MetaManager::factory($context)->forget($fstPath);
						continue;
					}


					$current = $this->candidateIsCurrent($candidateMeta, $afi);

					\Error_Reporter::print_buffer();
					\Error_Reporter::clear_buffer();

					if ((!$this->updateAssets && !$this->updateCore) || ($current && !$this->updateAssets)) {
						continue;
					}
					$type = $candidateMeta['type'];
					// second pass to confirm the app still exists
					if (!$this->appIsValid($afi, $type, $hostname, (string)array_get($candidateMeta, 'path', ''))) {
						// @TODO merge with Finder
						warn('App %(type)s under %(hostname)s%(path)s no longer valid - removing',
							compact('type', 'hostname', 'fstPath') + [
								'path' => (string)array_get($candidateMeta, 'path', '')
							]
						);
						MetaManager::factory($context)->forget($fstPath);
						continue;
					}

					if (Webapps::blacklisted($type)) {
						info("Skipping unsupported app `%(app)s' in %(docroot)s",
							['app' => $type, 'docroot' => $realpath]
						);
						continue;
					}

					// updating core updates all plugins/themes
					$concrete = !$current && $this->updateCore ? UpdateCandidate::class : UpdateAssetCandidate::class;
					/** @var UpdateCandidate $basecandidate */
					$basecandidate = new $concrete(
						$hostname,
						(string)array_get($candidateMeta, 'path', '')
					);
					$basecandidate->setContext($context);
					$basecandidate->parseAppInformation($candidateMeta);
					$gitHandler = Git::instantiateContexted($context, [
						Loader::fromDocroot($type, $fstPath, $context)->getAppRoot(),
						MetaManager::factory($context)->get($fstPath)
					]);
					$basecandidate->enableAssuranceMode($gitHandler->enabled());
					try {
						$basecandidate->initializeAssurance($gitHandler);
					} catch (\Throwable $e) {
						warn("Disabling assurance. Initialization failed: %s", $e->getMessage());
						$basecandidate->enableAssuranceMode(false);
					}

					if ($concrete === UpdateAssetCandidate::class) {
						$this->enumerateAssets($basecandidate);
					} else {
						$basecandidate->setAvailableVersions($this->getAppVersions($type));
						$this->processUpdateChain($basecandidate);
					}

					$counter++;
					if ($this->limit && $counter >= $this->limit) {
						warn("stopping batch update, limit `%d' reached", $this->limit);

						return true;
					}
				}
			}

			return true;
		}

		/**
		 * Get all active sites
		 *
		 * @return array
		 */
		protected function getSites(): array
		{
			if ($this->site) {
				return (array)$this->site;
			}

			return Enumerate::active();
		}

		/**
		 * List all applications that match the webapp
		 *
		 * @param \Auth_Info_User
		 * @return array
		 */

		protected function getApplications(\Auth_Info_User $context): array
		{
			$finder = new Finder($context);

			return $finder->getActiveApplications(\Closure::fromCallable($this->filter));
		}

		/**
		 * App candidate is most current version
		 *
		 * @param array                      $candidateInfo
		 * @param \apnscpFunctionInterceptor $afi
		 * @return bool
		 */
		protected function candidateIsCurrent(array $candidateInfo, \apnscpFunctionInterceptor $afi): bool
		{
			$instance = $this->loadInstanceFromApp($candidateInfo['type']);
			$mapping = $instance->getClassMapping();

			return (bool)$afi->call($mapping . '_is_current', [$candidateInfo['version']]);
		}

		protected function appIsValid(\apnscpFunctionInterceptor $afi, string $app, string $hostname, string $path = '') {
			$instance = $this->loadInstanceFromApp($app);
			$mapping = $instance->getClassMapping();

			return (bool)$afi->call($mapping . '_valid', [$hostname, $path]);
		}
		/**
		 * Check all assets and update as necessary
		 *
		 * @param UpdateAssetCandidate $candidate
		 */
		protected function enumerateAssets(UpdateAssetCandidate $candidate): void
		{
			$types = array_keys(UpdateAssetCandidate::KNOWN_ASSETS);
			foreach ($types as $type) {
				$candidate->setAssetType($type);
				foreach ($candidate->getAssets() as $assetName => $assetInfo) {
					if ($assetInfo['current']) {
						continue;
					}
					$updateCandidate = clone $candidate;
					$updateCandidate->setAssetName($assetName);
					$updateCandidate->setAvailableVersions([$assetInfo['version'], $assetInfo['max']]);
					$updateCandidate->setVersion($assetInfo['version']);
					$this->processUpdateChain($updateCandidate);
				}
			}
			//$basecandidate->setAssetName($plugin);
			//$versions = $basecandidate->getAssetVersions($versions);
		}

		/**
		 * Process a chained upgrade
		 *
		 * @param UpdateCandidate $basecandidate
		 */
		protected function processUpdateChain(UpdateCandidate $basecandidate): void
		{

			$chain = [];
			$firstVersion = $basecandidate->getVersion();

			do {
				$candidate = clone $basecandidate;
				$candidate->setVersion($version ?? $candidate->getVersion());
				if ($candidate->isLocked()) {
					warn("%s Candidate on `%s' (type: `%s') version locked to specific branch " .
						"`%s' (%s), newer versions available - skipping further upgrades",
						Job::ICON_ALERT,
						$candidate->getBaseUri(),
						$candidate->getAppType(),
						$candidate->getVersion(),
						$candidate->getLockType()
					);
					break;
				}
				if (!$candidate->isUpdatable()) {
					// most recent
					break;
				}
				$chain[] = $candidate;
				$version = $candidate->getNextVersion();
			} while ($candidate->isUpdatable());

			/**
			 * Chain failed to build by virtue of version lock or other issues
			 */
			if (!$chain) {
				return;
			}
			// @xxx this sucks, to fix
			$candidate->logJob($firstVersion);
			$this->dispatch($chain);
		}

		/**
		 * Run a series of upgrades
		 *
		 * @param UpdateCandidate[] $jobs
		 * @return PendingDispatch
		 */
		protected function dispatch(array $jobs): PendingDispatch
		{
			$job = Job::create(UpdateAppJob::class, $jobs[0]);
			$job->withFilters([':subdomain_is_dir'])->addToGroup('wa:' . $jobs[0]->getSite());
			$ret = $job->dispatch(array_map(static function (UpdateCandidate $candidate) {
				$job = Job::create(UpdateAppJob::class, $candidate);
				$job->addToGroup('wa:' . $candidate->getSite());
				if ($assurance = $candidate->getAssurance()) {
					$job->addSecondaryGroup($assurance->getTag());
				}
				return $job;
			}, \array_slice($jobs, 1)));

			info('%s Upgrading %s, %s - %s -> %s',
				$ret ? Job::ICON_SUCCESS : Job::ICON_FAILED,
				$jobs[0]->getBaseUri(),
				$jobs[0]->getAppType(),
				$jobs[0]->getVersion(),
				$jobs[\count($jobs) - 1]->getNextVersion()
			);

			return $ret;
		}

		protected function getAppVersions(string $app): array
		{
			if (!isset($this->appVersion[$app])) {
				fatal("unknown app `%s'", $app);
			}

			return $this->appVersion[$app];
		}
	}