<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2020
 */

namespace Module\Support\Webapps;

use Module\Support\Webapps;

class PathManager {
	const PATHKEY = 'webapps.uip';
	const CUSTOM_STORAGE_PATH = 'custom/webapps';

	/**
	 * Get all app paths
	 *
	 * @return array
	 */
	public static function applicationViewPaths(): array
	{
		$cache = \Cache_Global::spawn();
		if (false === ($appIncludes = $cache->get(self::PATHKEY))) {
			$appIncludes = [];
			foreach (\Module\Support\Webapps::allApps() as $app) {
				$home = rtrim(INCLUDE_PATH, '/') . '/' . self::providerToPath($app);
				if (0 === strpos($home, $limit = config_path(self::CUSTOM_STORAGE_PATH))) {
					$path = $home;
					while (0 === strpos($path, $limit)) {
						$path = dirname($path);
						$chk = $path . '/views';
						if (is_dir($chk)) {
							$path = $chk;
							break;
						}

					}
					$paths = [
						$path
					];
				} else {
					$paths = [
						config_path(self::CUSTOM_STORAGE_PATH) . "/{$app}/views",
						dirname($home) . '/views'
					];
				}
				$appIncludes[$app] = $paths;
			}
			$cache->set(self::PATHKEY, $appIncludes, 43200);
		}

		if (!is_array($appIncludes) || (!empty($appIncludes) && false !== strpos(current($appIncludes)[0], '//'))) {
			report('PathManager fix applied');
			// @XXX
			// initial inventory of apps via _housekeeping() properly sets path, but another unknown route
			// replaces first char in library path with forward slash -
			// e.g.
			// WRONG:
			//  /usr/local/apnscp//ib/Module/Support/Webapps/App/Type/Nextcloud/views
			// RIGHT:
			// /usr/local/apnscp/lib/Module/Support/Webapps/App/Type/Nextcloud/views
			//
			// Requires further examination to find origin of this bug
			self::flush();
			return self::applicationViewPaths();
		}
		return $appIncludes;
	}

	/**
	 * Convert application into path
	 *
	 * @param string $app
	 * @return string
	 * @throws \ReflectionException
	 */
	public static function providerToPath(string $app): string
	{
		static $cache = [];
		if (isset($cache[$app])) {
			return $cache[$app];
		}
		return $cache[$app] = ltrim(substr(
			(new \ReflectionClass(Webapps::handlerFromApplication($app)))->getFilename(),
			strlen(rtrim(INCLUDE_PATH, '/'))), '/'
		);
	}

	/**
	 * Get storehouse location for app
	 *
	 * @param string $app
	 * @return string
	 */
	public static function storehouse(string $app): string
	{
		return base_path(dirname(self::providerToPath($app)) . '/storehouse');
	}

	/**
	 * Flush path cache
	 */
	public static function flush(): void
	{
		\Cache_Global::spawn()->del(self::PATHKEY);
	}
}