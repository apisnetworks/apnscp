<?php
	declare(strict_types=1);
	/**
	 * Composer management
	 *
	 * Generic support for Composer-backed packages
	 *
	 * @package core
	 */

	namespace Module\Support\Webapps;

	use Opcenter\Contracts\VirtualizedContextable;

	class ComposerMetadata implements VirtualizedContextable, \IteratorAggregate, \ArrayAccess {
		use \ContextableTrait;
		use \apnscpFunctionInterceptorTrait;

		private array $data;
		private bool $dirty = false;

		public function __construct(private string $location)
		{
			$this->data = $this->readComposer();
		}

		public static function read(\Auth_Info_User $ctx, string $approot): self
		{
			return self::instantiateContexted($ctx, ["{$approot}/composer.json"]);
		}

		public static function readFrozen(\Auth_Info_User $ctx, string $approot): self
		{
			return self::instantiateContexted($ctx, ["{$approot}/vendor/composer/installed.json"]);
		}

		public function __destruct()
		{
			if (!$this->dirty) {
				return;
			}

			$this->sync();
		}

		public function sync(): bool
		{
			return !($this->dirty = !$this->file_put_file_contents(
				$this->location,
				json_encode($this->data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
			));
		}

		/**
		 * Parse composer.json|lock file
		 *
		 * @param string $approot
		 * @return array
		 */
		protected function readComposer(): array
		{
			$path = $this->location;
			if (!$this->file_exists($path)) {
				warn('missing %(name)s from project root - cannot load metadata', [
					'name' => basename($path)
				]);

				return [];
			}

			return json_decode($this->file_get_file_contents($path), true);
		}

		/**
		 * Configured Composer packages
		 *
		 * @return \ArrayObject
		 */
		public function packages(): \ArrayObject
		{
			// composer v1 stores top-level
			return new \ArrayObject($this->data['packages'] ?? $this->data);
		}

		public function offsetExists(mixed $offset): bool
		{
			return array_key_exists($offset, $this->data);
		}

		public function &offsetGet(mixed $offset): mixed
		{
			return $this->data[$offset];
		}

		public function offsetSet(mixed $offset, mixed $value): void
		{
			$this->dirty = true;
			$this->data[$offset] = $value;
		}

		public function offsetUnset(mixed $offset): void
		{
			$this->dirty = true;
			unset($this->data[$offset]);
		}

		public function getIterator(): \Traversable
		{
			return new \ArrayObject($this->data);
		}
	}