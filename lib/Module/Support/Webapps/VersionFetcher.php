<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2019
 */

namespace Module\Support\Webapps;

abstract class VersionFetcher
{
	public $versionField = 'name';

	abstract public function fetch(string $identifier, callable $cb = null): ?array;

	/**
	 * Set field containing "version" data
	 *
	 * @param string $field
	 * @return $this
	 */
	public function setVersionField(string $field): self
	{
		$this->versionField = $field;

		return $this;
	}

	/**
	 * Retrieve raw package data
	 *
	 * @param string $url
	 * @return array
	 */
	protected function downloadMeta(string $url): array
	{
		$opts = [
			'http' => [
				'method' => 'GET',
				'header' => [
					'User-Agent: ' . PANEL_BRAND . ' ' . APNSCP_VERSION,
				]
			]
		];

		$context = stream_context_create($opts);
		$contents = (string)file_get_contents($url, false, $context);

		return json_decode($contents, true) ?? [];
	}

	/**
	 * Transform release metadata into versionable data
	 *
	 * @param array         $results metadata results
	 * @param callable|null $cb      normalization function
	 * @return array
	 */
	protected function process(array $results, callable $cb = null): array
	{
		$cb ??= $this->filterRelease(...);

		$filtered = [];
		foreach ($results as $x) {
			if ($this->versionField) {
				$x['version'] = $x[$this->versionField];
			}

			if (false !== ($resp = $cb($x))) {
				$filtered[] = $resp ?? $x;
			}
		}

		usort($filtered, static function ($a, $b) {
			if (version_compare($a['version'], $b['version'], '<')) {
				return -1;
			}
			if (version_compare($a['version'], $b['version'], '>')) {
				return 1;
			}

			return 0;
		});

		return $filtered;
	}

	/**
	 * Filter non-versionable releases
	 *
	 * @param array $item
	 * @return bool|null
	 */
	protected function filterRelease(array $item): false|array
	{
		if (($item['version'][0] ?? null) === 'v') {
			$item['version'] = substr($item['version'], 1);
		}
		if (!preg_match('/^\d+\.\d+(?:\.\d+)?(?:-|$)/', $item['version'])) {
			return false;
		}

		return $item;
	}
}