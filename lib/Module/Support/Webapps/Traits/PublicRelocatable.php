<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Module\Support\Webapps\Traits;

	use Module\Support\Webapps;

	/**
	 * Trait PublicRelocatable
	 *
	 * Helper for apps that locate logic below docroot
	 *
	 * @package Module\Support\Webapps\Traits
	 */
	trait PublicRelocatable
	{
		/**
		 * Override to determine app root's depth
		 *
		 * May exist within a handler or module implementation
		 * @return int
		 */
		protected function getAppRootDepth(string $hostname, string $path = ''): int
		{
			$depth = property_exists($this, 'manifest') ? $this->manifest?->offsetGet('depth') : null;
			if ($depth !== null) {
				return $depth;
			}
			$handler = $this instanceof Webapps ? Webapps::handlerFromApplication($this->getAppName()) : static::class;
			return $handler::APP_ROOT_DEPTH;
		}

		protected function getAppRoot(string $hostname, string $path = ''): ?string
		{
			$docroot = $this->web_get_docroot($hostname, $path) ?: \Web_Module::MAIN_DOC_ROOT;
			$depth = $this->getAppRootDepth($hostname, $path);

			if (!$depth) {
				return $docroot;
			}

			// Ghost/Laravel app root resides 1 level down
			$normalized = $this->web_normalize_path($hostname, $path);
			if ($normalized && $docroot !== $normalized) {
				// symlinked...
				return \dirname($normalized, $depth);
			}
			return $docroot ? \dirname($docroot, $depth) : null;
		}
	}