<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2019
 */

namespace Module\Support\Webapps\VersionFetcher;

use Module\Support\Webapps\VersionFetcher;

class Github extends VersionFetcher
{
	const VERSION_CHECK_BASE = 'https://api.github.com/repos';
	protected $mode = 'releases';

	public function setMode(string $mode): self {
		if ($mode !== 'tags' && $mode !== 'releases') {
			fatal("Unknown fetch mode `%s'", $mode);
		}
		$this->mode = $mode;

		return $this;
	}

	/**
	 * Fetch versions from Github
	 *
	 * @param string $identifier
	 * @return array|null
	 */
	public function fetch(string $identifier, callable $cb = null): ?array
	{
		if (false === strpos($identifier, '/')) {
			error('Malformed identifier');
			return null;
		}
		$url = self::VERSION_CHECK_BASE . '/' . $identifier . '/' . $this->mode . '?per_page=100';

		return $this->process(
			$this->fetchPagination($url, 1),
			$cb
		);
	}

	private function fetchPagination(string $url, int $offset = 1): array
	{
		if ($offset > 10) {
			return [];
		}

		$versions = $this->downloadMeta("{$url}&page={$offset}");
		if (!$versions) {
			return [];
		}

		array_unshift($versions, ...$this->fetchPagination($url, ++$offset));
		return $versions;
	}

}