<?php declare(strict_types=1);

/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2024
 */

namespace Module\Support\Webapps\VersionFetcher;

class PackageJson extends Github
{
	const VERSION_CHECK_BASE = 'https://registry.npmjs.org/';
	protected $mode = 'releases';
	public $versionField = null;

	public function setMode(string $mode): self {
		debug("Mode setting ignored");
		return $this;
	}

	/**
	 * Fetch versions from npmjs
	 *
	 * @param string $identifier
	 * @return array|null
	 */
	public function fetch(string $identifier, callable $cb = null): ?array
	{
		if (str_contains($identifier, '/')) {
			error('Malformed identifier');
			return null;
		}

		$url = self::VERSION_CHECK_BASE . '/' . $identifier;
		$meta = $this->downloadMeta($url);

		return $this->process($meta['versions'], $cb);
	}

	protected function filterRelease(array $item): false|array
	{
		if (isset($item['deprecated'])) {
			return false;
		}

		return parent::filterRelease($item);
	}
}