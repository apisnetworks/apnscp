<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */


namespace Module\Support\Webapps\App;

use Module\Support\Webapps\App\Type\Unknown\Handler as Unknown;
use Module\Support\Webapps\App\UIPanel\Element;
use Module\Support\Webapps\MetaManager;

class UIPanel {
	use \ContextableTrait;

	const CACHE_HASH_KEY = 'wa:ss';
	private static $instances = [];

	protected $hostname;
	protected $path;
	/**
	 * @var MetaManager
	 */
	protected $meta;

	private function __construct(\Auth_Info_User $ctx)
	{
		$this->setContext($ctx);
		$this->meta = MetaManager::factory($ctx);
	}

	/**
	 * Remove app meta cache
	 *
	 * @param string      $hostname
	 * @param string|null $path
	 * @param bool        $screenshot
	 * @return void
	 */
	public function freshen(string $hostname, ?string $path = '', bool $screenshot = true): void
	{
		$cache = \Cache_Account::spawn($this->getAuthContext());
		// kill reference first
		$cache->hDel(...$this->cacheKey($hostname, $path));
		if ($screenshot) {
			// Deferred callback after domain/account removed
			\Error_Reporter::filter(
				fn() => $this->get($hostname, $path)->cleanScreenshot(),
				[\Web_Module::ERR_UNKNOWN_DOMAIN]
			);
		}
	}

	/**
	 * Purge all cached entries
	 *
	 * @return void
	 */
	public function purge(): void
	{
		$cache = \Cache_Account::spawn($this->getAuthContext());
		$cache->del(self::CACHE_HASH_KEY);
	}
	/**
	 * Delete meta pertaining to app
	 *
	 * @param string      $hostname
	 * @param string|null $path
	 * @return void
	 */
	public function forget(string $hostname, ?string $path = ''): void
	{
		$this->freshen($hostname, $path, true);
		$element = $this->get($hostname, $path);
		$this->getMeta()->forget($element->getDocumentRoot());
	}

	private function cacheKey(string $hostname, ?string $path = ''): array
	{
		return [self::CACHE_HASH_KEY, rtrim($hostname . '/' . $path, '/')];
	}

	/**
	 * Create new pane from hostname
	 *
	 * @param string $hostname
	 * @param string $path
	 * @return Element
	 */
	public function get(string $hostname, ?string $path = ''): Element
	{
		$cache = \Cache_Account::spawn($this->getAuthContext());
		$key = $this->cacheKey($hostname, $path);
		$element = $cache->hGet(...$key);
		if ($element instanceof Element) {
			return $element;
		}
		$element = UIPanel\Element::instantiateContexted($this->getAuthContext(),
			[$this, $hostname, (string)$path]);

		if (!$cache->exists($key[0])) {
			defer($_, static function() use($cache, $key) {
				$cache->expire($key[0], 600);
			});
		}
		try {
			$cache->hSet($key[0], $key[1], $element);
		} catch (\Throwable $e) {
			// serialization error - swap for Opis in 3.2.1+
			debug("Failed to serialize %s/%s - swap() used?", $hostname, $path);
		}
		return $element;
	}

	/**
	 * Create pane from existing webapp
	 *
	 * @param Unknown $app
	 * @return Element
	 */
	public function fromApp(Unknown $app)
	{
		return $this->get($app->getHostname(), $app->getPath());
	}

	public function getMeta(): MetaManager
	{
		return $this->meta;
	}

	public static function proxy(\Auth_Info_User $ctx)
	{
		return new static($ctx);
	}

	public static function instantiateContexted(\Auth_Info_User $context, array $constructorArgs = []): self
	{
		return static::proxy($context);
	}
}