<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	namespace Module\Support\Webapps\App;

	use Lararia\Jobs\InstallAppJob;
	use Lararia\Jobs\Job;
	use Module\Support\Webapps\App\Type\Unknown\Handler as Unknown;
	use Module\Support\Webapps\Messages;
	use Module\Support\Webapps\MetaManager;

	/**
	 * Class Installer
	 *
	 * An intermediary to force preference reload
	 *
	 *
	 * @package apps\webapps
	 *
	 */
	class Installer extends Unknown
	{
		const INSTALLING_VERSION = '';
		/**
		 * @var string
		 */
		protected $type;

		public const MSG_APP_INSTALLING = [
			':webapp_app_install_bg',
			"We're installing %(app)s in the background. We'll send an email to %(email)s when it's done!",

		];
		public function applicationPresent(): bool
		{
			return false;
		}

		public function getModuleName(): string
		{
			return $this->type ?? $this->getOption('type', $_GET['app'] ?? null);
		}

		public function getName(): string
		{
			return '';
		}

		public function dispatch(string $type): bool
		{
			/**
			 * @var $job InstallAppJob
			 */
			$this->type = $type;
			$email = array_get($this->getOptions(), 'email', $this->common_get_email());
			$this->lock($type);
			$job = Job::create(InstallAppJob::class, $this);
			$job->setContext($this->getAuthContext())->setEmail($email);
			$job->dispatch();

			return info(static::MSG_APP_INSTALLING, ['app' => ucwords($type), 'email' => $job->getEmail()]);
		}

		public function setOptions(?array $options): Unknown
		{
			// occurs whenever a new WA is installed
			if ($options !== null) {
				return parent::setOptions($options);
			}

			$saved = array_intersect_key(
				$this->getOptions()->toArray(),
				array_flip($this->webapp_reconfigurables($this->getHostname(), $this->getPath()))
			);
			return parent::setOptions(null)->setOptions($saved);
		}


		/**
		 * Create pending app
		 *
		 * @return Unknown
		 */
		public function initialize(): Unknown
		{
			$this->getPane()->freshen();

			return Loader::fromHostname(
				$this->getModuleName(),
				$this->getHostname(),
				$this->getPath(),
				$this->getAuthContext()
			);
		}

		public function getClassMapping(): string
		{
			return $_GET['app'] ?? parent::getClassMapping();
		}

		/**
		 * Lock a document root for installation
		 *
		 * @param string $type app type
		 */
		protected function lock(string $type): void
		{
			$map = MetaManager::factory($this->getAuthContext());
			$map->replace($this->getDocumentMetaPath(), ['type' => $type, 'version' => self::INSTALLING_VERSION]);
		}

		/**
		 * Unlock document root for installation
		 */
		public function unlock(): void
		{
			$map = MetaManager::factory($this->getAuthContext());
			$map->replace($this->getDocumentMetaPath(), ['type' => null, 'version' => null]);
		}

		public function uninstall(...$args): bool
		{
			return error(Messages::ERR_PATH_INVALID_WEBAPP, ['path' => $this->getDocumentMetaPath()]);
		}

		public function hasVersion(): bool
		{
			return false;
		}

		public function hasUninstall(): bool
		{
			return false;
		}

		public function hasAdmin(): bool
		{
			return false;
		}
	}