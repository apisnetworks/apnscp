<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */

namespace Module\Support\Webapps;

use Illuminate\Contracts\Support\Arrayable;
use Module\Support\Webapps;

/**
 * Class DatabaseGenerator
 *
 * @package Module\Support\Webapps
 * @property string $hostname
 * @property string $database
 * @property string $username
 * @property string $password
 * @property string $prefix
 * @property string $kind
 * @property int    $port
 * @property array  $dbArgs
 */
class DatabaseGenerator	implements Arrayable {
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	public const MUTABLES = [
		'hostname',
		'database',
		'username',
		'password',
		'prefix',
		'kind',
		'port'
	];

	protected string $base;

	private string $kind;
	private string $hostname = 'localhost';
	private string $database;
	private string $username;
	private string $password;
	private int    $port;
	private string $prefix = '';

	public $connectionLimit = \Mysql_Module::DEFAULT_CONCURRENCY_LIMIT;

	public bool $autoRollback = false;

	/** @var bool passed internal assertions */
	protected bool $ready = false;

	/** @var array additional database creation arguments */
	private $dbArgs = [];

	protected function __construct(string $kind, string $base)
	{
		if (!\apnscpFunctionInterceptor::module_exists($kind)) {
			fatal("Unknown database type `%s'", $kind);
		}
		$this->kind = $kind;
		$this->port = $this->kind === 'mysql' ? 3306 : 5432;
		$this->base = $base;
	}

	public function __destruct()
	{
		if ($this->autoRollback) {
			$this->rollback();
		}
	}

	/**
	 * Suggest database
	 *
	 * @return $this
	 */
	protected function suggest(): self
	{
		if (!serial(function () {
			$this->ready = false;
			$this->database = $this->rollDatabase();
			$this->username = $this->rollUsername();
			$this->password = $this->rollPassword();
			return $this->ready = true;
		})) {
			$this->rollback();
		}

		return $this;
	}

	public function __debugInfo()
	{
		return $this->toArray();
	}

	public function toArray() {
		return array_combine(self::MUTABLES, array_map(fn($x) => $this->$x, self::MUTABLES));
	}

	/**
	 * Generate database
	 *
	 * @return bool|string
	 */
	public function rollDatabase()
	{
		$suffix = '';
		$dbprefix = $this->{$this->kind . '_get_prefix'}();
		$maxlen = $this->sql_mysql_schema_column_maxlen('db');
		$normalizedname = substr(
			str_replace(array('.', '-'), '', $this->base), 0, $maxlen);

		// room for -##
		if ((\strlen($normalizedname) + 3) >= $maxlen) {
			$pos = null;
			if (false === ($pos = strpos($this->base, '.'))) {
				$pos = $maxlen - 5;
			}
			$normalizedname = substr($normalizedname, 0, $pos);
		}
		for ($i = 1; $i < 100; $i++) {
			$name = $dbprefix . $normalizedname . $suffix;
			if (\strlen($name) > $maxlen) {
				warn('db name generation exceeds maximum allowable bounds, generating a random db name');
				$name = $dbprefix . crc32(time());
			}
			if (!$this->{$this->kind . '_database_exists'}($name)) {
				return $name;
			}
			$suffix = '-' . $i;
		}

		return error('cannot generate a suitable database, pool exhausted');
	}

	/**
	 * Suggest a user given a database
	 *
	 * @return bool|string
	 */
	public function rollUsername() {
		$suffix = '';
		$dbprefix = $this->{$this->kind . '_get_prefix'}();
		if (!strncmp($this->database, $dbprefix, \strlen($dbprefix))) {
			$dbname = substr($this->database, \strlen($dbprefix));
		}
		$maxlen = $this->sql_mysql_schema_column_maxlen('user');
		$normalizedname = substr(
			str_replace(array('.', '-'), '', $dbname), 0, $maxlen);
		if ((\strlen($normalizedname) + 3) > $maxlen) {
			$pos = null;
			if (false === ($pos = strpos($dbname, '.'))) {
				$pos = $maxlen - 5;
			}
			$normalizedname = substr($normalizedname, 0, $pos);
		}
		for ($i = 1; $i < 20; $i++) {
			$name = $dbprefix . $normalizedname . $suffix;
			if (\strlen($name) > $maxlen) {
				warn('db username generation exceeds maximum allowable bounds, generating a random username');
				$name = crc32(time());
				$dblen = \strlen($dbprefix);
				$name = $dbprefix . substr($name, 0, $maxlen - $dblen);
			}
			if (!$this->{$this->kind . '_user_exists'}($name, $this->hostname)) {
				return $name;
			}
			$suffix = '-' . $i;
		}

		return error('cannot generate a suitable database, pool exhausted');
	}

	/**
	 * Create database
	 *
	 * Rollsback on failure implicitly
	 *
	 * @return bool
	 */
	public function create(): bool
	{
		if (!$this->ready) {
			return false;
		}

		$args = array_merge([$this->database], $this->dbArgs);

		if (!$this->{$this->kind . '_create_database'}(...$args)) {
			return error("Failed to create suggested db `%s'", $this->database);
		}

		switch ($this->kind) {
			case 'pgsql':
				$userargs = [
					$this->username,
					$this->password,
					$this->connectionLimit
				];
				break;
			default:
				$userargs = [
					$this->username,
					$this->hostname,
					$this->password,
					$this->connectionLimit
				];
				break;
		}

		if (!$this->{$this->kind . '_add_user'}(...$userargs)) {
			$this->{$this->kind . '_delete_database'}($this->database);

			return error("Failed to create suggested user `%s'", $this->username);
		}

		$oldex = \Error_Reporter::exception_upgrade();
		try {
			if ($this->kind === 'pgsql') {
				$this->pgsql_set_owner($this->database, $this->username);
			} else {
				$this->{$this->kind . '_set_privileges'}(
					$this->username,
					$this->hostname,
					$this->database,
					[
						'read' => true,
						'write' => true
					]
				);
			}
		} catch (\apnscpException $e) {
			return error("failed to set privileges on db `%(database)s' for user `%(user)s'",
				['database' => $this->database, 'user' => $this->username]
			);
		} finally {
			\Error_Reporter::exception_upgrade($oldex);
		}

		if ($this->{$this->kind . '_add_backup'}($this->database, 'zip', 5, 2)) {
			info("added database backup task for `%s'", $this->database);
		}

		return true;
	}

	/**
	 * Rollback a failed install
	 */
	public function rollback(): void
	{
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
		if ($this->{$this->kind . '_database_exists'}($this->database)) {
			$this->{$this->kind . '_delete_database'}($this->database);
		}
		if ($this->{$this->kind . '_user_exists'}($this->username, $this->hostname)) {
			$this->{$this->kind . '_delete_user'}($this->username, $this->hostname);
		}
		\Error_Reporter::exception_upgrade($oldex);
	}

	/**
	 * Suggest a password
	 *
	 * @return string
	 */
	protected function rollPassword(int $maxlength = 32): string
	{
		return \Opcenter\Auth\Password::generate($maxlength);
	}

	/**
	 * Create new MySQL data storage
	 *
	 * @param \Auth_Info_User $ctx
	 * @param string          $pattern base pattern
	 * @return static
	 */
	public static function mysql(\Auth_Info_User $ctx, string $pattern): self
	{
		return static::instantiateContexted($ctx, ['mysql', $pattern])->suggest();
	}

	/**
	 * Create new PostgreSQL data storage
	 *
	 * @param \Auth_Info_User $ctx
	 * @param string          $pattern base pattern
	 * @return static
	 */
	public static function pgsql(\Auth_Info_User $ctx, string $pattern): self
	{
		return static::instantiateContexted($ctx, ['pgsql', $pattern])->suggest();
	}

	public function connect(): \PDO {
		return Webapps::connectorFromCredentials($this);
	}

	public function __set($name, $value)
	{
		if (!$this->checkProperty($name)) {
			fatal("Unknown or invalid property `%s'", $name);
		}

		$this->{$name} = $value;
		$this->suggest();
	}

	public function __get($name)
	{
		if (!$this->checkProperty($name)) {
			fatal("Unknown or invalid property `%s'", $name);
		}

		return $this->{$name};
	}

	public function __isset($name)
	{
		return $this->checkProperty($name) && null !== $this->{$name};
	}

	/**
	 * Verify property exists
	 *
	 * @param string $name
	 * @return bool
	 */
	private function checkProperty(string $name): bool
	{
		try {
			if ((new \ReflectionProperty(self::class, $name))->isPrivate()) {
				return true;
			}
		} catch (\ReflectionException $e) { }

		return false;
	}


}