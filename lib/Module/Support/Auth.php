<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support;

	use Module_Skeleton;
	use Opcenter\Auth\Shadow;
	use Opcenter\Mail\Services\Dovecot;
	use Opcenter\Map;
	use Util_Geoip;

	/**
	 * Support structure for auth module
	 */
	abstract class Auth extends Module_Skeleton
	{
		// admin authentication file
		const ADMIN_AUTH = '/etc/opcenter/webhost/passwd';

		/**
		 * Remap siteinfo to auth on 7.5+ platforms
		 *
		 * @param null|string $platform_version optional platform version to compare against
		 * @return string
		 */
		public static function getAuthService($platform_version = PLATFORM_VERSION): string
		{
			return 'auth';
		}

		/**
		 * Send security notice
		 *
		 * @param       $what
		 * @param array $args
		 * @return bool notice generated
		 */
		protected function sendNotice($what, $args): bool
		{
			if (\Auth::client_ip() === '127.0.0.1') {
				// originated locally from server
				return true;
			}
			$key = 'notify.' . $what . 'change';
			$donotify = \Preferences::get($key, true);
			if (!$donotify) {
				return false;
			}

			$vars = array_replace_recursive([
				'what'      => $what,
				'username'  => $this->username,
				'domain'    => $this->domain,
				'config'    => [
					'geoip' => !empty($args['ip'])
				],
			], $args);

			$location = null;

			if (!empty($vars['config']['geoip'])) {
				$hostname = 'UNKNOWN';
				if ($tmp = $this->dns_gethostbyaddr_t($args['ip'], 3000)) {
					$hostname = $tmp;
				}
				$location = Util_Geoip::instantiateContexted($this->getAuthContext())->locate($args['ip']);
				$gmap = 'https://www.google.com/maps/preview/@' . $location['latitude'] . ',' . $location['longitude'] . ',8z';
				$vars += [
					'latitude'  => $location['latitude'],
					'longitude' => $location['longitude'],
					'city'      => $location['city'],
					'state'     => $location['state'],
					'country'   => $location['country'],
					'gmapurl'   => $gmap,
					'ip'        => $args['ip'],
					'hostname'  => $hostname,
				];
			}

			if (!$vars['email']) {
				return warn("no email configured - ignoring alert notification for %(what)s change for `%(user)s'", [
					'what' => $what,
					'user' => $vars['username']
				]);
			}

			\Lararia\Bootstrapper::minstrap();
			$mail = \Illuminate\Support\Facades\Mail::to($args['email']);
			$subject = \ArgumentFormatter::format(
				ucwords($what) . ' Changed - %(domain)s',
				[
					'domain' => $vars['domain']
				]
			);
			$mail->send((new \Lararia\Mail\Simple('email.auth.change', $vars))->setSubject($subject));

			return true;
		}

		/**
		 * Recreate TokyoCabinet map
		 */
		public static function rebuildMap(): bool
		{
			if (file_exists(Map::DOMAIN_MAP) && filesize(Map::DOMAIN_MAP) > 0) {
				return true;
			}

			// backwards compatible for older platforms that don't explicitly use TC
			$tcd = Map::load(Map::DOMAIN_MAP, 'cd');

			return $tcd->copy(Map::DOMAIN_TXT_MAP);
		}

		/**
		 * Direct update of crypted password
		 *
		 * @param string      $cpassword
		 * @param string      $user
		 * @param string|null $domain
		 * @return bool
		 */
		protected function setCryptedPassword(
			string $cpassword,
			string $user,
			string $domain = null
		): bool
		{
			if (!Shadow::valid_crypted($cpassword)) {
				return error("provided password for user `%s' is not crypted", $user);
			}
			if ($user !== $this->username && $this->permission_level & PRIVILEGE_USER) {
				return error('insufficient privileges to specify user');
			}
			if ($this->permission_level & PRIVILEGE_ADMIN && !$domain) {
				if (!($fp = fopen(static::ADMIN_AUTH, 'r+')) || !flock($fp, LOCK_EX | LOCK_NB)) {
					fclose($fp);

					return error("unable to gain exclusive lock on `%s'", static::ADMIN_AUTH);
				}
				$lines = [];
				while (false !== ($line = fgets($fp))) {
					$lines[] = explode(':', rtrim($line));
				}
				if (false === ($pos = array_search($user, array_column($lines, 0), true))) {
					flock($fp, LOCK_UN);
					fclose($fp);

					return error("user `%s' does not exist", $user);
				}
				$lines[$pos][1] = $cpassword;
				if (!ftruncate($fp, 0)) {
					flock($fp, LOCK_UN);
					fclose($fp);

					return error("failed to truncate `%s'", static::ADMIN_AUTH);
				}
				rewind($fp);
				fwrite($fp, implode("\n", array_map(static function ($a) {
					return join(':', $a);
				}, $lines)));

				return flock($fp, LOCK_UN) && fclose($fp);
			}

			if ($this->permission_level & (PRIVILEGE_SITE | PRIVILEGE_ADMIN)) {
				$afi = ($this->permission_level & PRIVILEGE_SITE) ? $this->getApnscpFunctionInterceptor() :
					\apnscpFunctionInterceptor::factory(\Auth::context(null, $domain));
				$users = $afi->user_get_users();
				if (!isset($users[$user])) {
					return error('%s: user not found', $user);
				}
			}

			return Shadow::bindTo($this->make_domain_fs_path($domain))->set_cpasswd($cpassword, $user) &&
				(!Dovecot::exists() || Dovecot::flushAuth());
		}
	}