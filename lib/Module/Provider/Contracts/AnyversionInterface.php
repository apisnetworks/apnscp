<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, December 2023
 */

namespace Module\Provider\Contracts;

	interface AnyversionInterface
	{
		public function do(?string $version, ?string $pwd, string $command, array $args = [], array $env = []): array;

		public function direct(string $directive, ?string $version, ?string $pwd, string $command, array $args = [], array $env = []): array;

		public function installed(string $version, string $comparator = '='): ?string;

		public function make_default(string $version, string $path = '~'): bool;

		public function version_from_path(string $path): string;

		public function install(string $version): ?string;

		public function get_available(): array;

		public function list(): array;

		public function uninstall(string $version): bool;
	}