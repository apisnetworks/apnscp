<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace Module\Skeleton;

	/**
	 * Class Webhooks
	 *
	 * Webhook interceptor for module calls
	 *
	 * @package Module\Skeleton
	 */
	class Webhooks extends Standard
	{
		protected $fnlog = [];

		public function _invoke($function, $args)
		{
			$this->fnlog[] = [
				'module'   => static::class,
				'function' => $function,
				'args'     => $args
			];

			return parent::_invoke($function, $args);
		}

		public function __destruct()
		{
			if (!$this->fnlog) {
				return;
			}

			unset($this->fnlog);
		}

	}