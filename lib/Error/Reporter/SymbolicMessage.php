<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, February 2024
 */

namespace Error\Reporter;

class SymbolicMessage implements \Stringable {

	public function __construct(
		private string $symbol,
		private string $msg,
		private array $args = [])
	{ }

	public function __toString()
	{
		return $this->msg;
	}

	public function translate(): self
	{
		$new = clone $this;
		$new->msg = _($this->msg);
		return $new;
	}

	public function symbol(): string
	{
		return $this->symbol;
	}

	public function is(string $symbol): bool
	{
		if ($symbol[0] !== ':') {
			$symbol = ":{$symbol}";
		}

		return $this->symbol === $symbol;
	}
}