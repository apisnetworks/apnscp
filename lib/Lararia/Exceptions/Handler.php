<?php

	namespace Lararia\Exceptions;

	use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
	use Symfony\Component\HttpKernel\Exception\HttpException;

	class Handler extends ExceptionHandler
	{
		/**
		 * A list of the exception types that are not reported.
		 *
		 * @var array
		 */
		protected $dontReport = [
			//
		];

		/**
		 * A list of the inputs that are never flashed for validation exceptions.
		 *
		 * @var array
		 */
		protected $dontFlash = [
			'password',
			'password_confirmation',
		];

		/**
		 * Report or log an exception.
		 *
		 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
		 *
		 * @param  \Throwable $exception
		 * @return void
		 */
		public function report(\Throwable $exception)
		{
			return \Error_Reporter::handle_exception($exception);
		}

		/**
		 * Render an exception into an HTTP response.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  \Throwable               $exception
		 * @return \Illuminate\Http\Response
		 */
		public function render($request, \Throwable $exception)
		{
			$code = 550;
			if ($exception instanceof HttpException) {
				$code = $exception->getStatusCode();
			}
			$Page = \Page_Container::init(\apps\error\Page::class, \Auth::profile(), [$code]);
			$Page->setException($exception);
			$Page->handle_request();
		}
	}