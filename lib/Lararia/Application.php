<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	declare(strict_types=1);

	namespace Lararia;

	class Application extends \Illuminate\Foundation\Application
	{
		protected $namespace = 'Lararia\\';

		/**
		 * Get the path to the application configuration files.
		 *
		 * @param string $path Optionally, a path to append to the config path
		 * @return string
		 */
		public function configPath($path = '')
		{
			return config_path('laravel' . ($path ? DIRECTORY_SEPARATOR . $path : $path));
		}

		public function bootstrapPath($path = '')
		{
			return INCLUDE_PATH . '/lib/Lararia'. ($path ? DIRECTORY_SEPARATOR . $path : $path);
		}

		/**
		 * Get Laravel app base path
		 *
		 * Overrides Laravel to locate under lib/Lararia/
		 *
		 * @param string $path
		 * @return string
		 */
		public function path($path = '')
		{
			return $this->basePath . DIRECTORY_SEPARATOR . 'lib/Lararia' . ($path ? DIRECTORY_SEPARATOR . $path : $path);
		}

		/**
		 * Relocated event cache
		 *
		 * @return string
		 */
		public function getCachedEventsPath()
		{
			return storage_path('cache/events.php');
		}

		/**
		 * Get the path to the cached services.php file.
		 *
		 * @return string
		 */
		public function getCachedServicesPath()
		{

			return storage_path('cache/services.php');
		}

		/**
		 * Get the path to the cached packages.php file.
		 *
		 * @return string
		 */
		public function getCachedPackagesPath()
		{
			return storage_path('cache/packages.php');
		}

		/**
		 * Determine if the application configuration is cached.
		 *
		 * @return bool
		 */
		public function configurationIsCached()
		{
			return file_exists($this->getCachedConfigPath());
		}

		/**
		 * Get the path to the configuration cache file.
		 *
		 * @return string
		 */
		public function getCachedConfigPath()
		{
			return storage_path('cache/config.php');
		}

		public function databasePath($path = '')
		{
			return resource_path('database' . ($path ? DIRECTORY_SEPARATOR . $path : $path));
		}

		/**
		 * Get the path to the routes cache file.
		 *
		 * @return string
		 */
		public function getCachedRoutesPath()
		{
			return storage_path('cache/routes.php');
		}

		public function boot()
		{
			// allow "router" facade to be overriden by apps such as Horizon
			if (!app()->resolved('router')) {
				app()->singleton('router', \Lararia\Http\DispatchCompatibilityHandler::class);
			}


			parent::boot();
		}
	}