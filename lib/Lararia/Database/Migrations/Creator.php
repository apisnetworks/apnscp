<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Lararia\Database\Migrations;

	use Illuminate\Database\Migrations\MigrationCreator;

	class Creator extends MigrationCreator
	{
		protected $type = 'db';

		public function setType(string $type)
		{
			if ($type !== 'db' && $type !== 'platform') {
				throw new \ArgumentError("Unknown type {$type}");
			}
			$this->type = $type;
		}

		/**
		 * Get the full path to the migration.
		 *
		 * @param  string $name
		 * @param  string $path
		 * @return string
		 */
		protected function getPath($name, $path)
		{
			if ($this->type !== 'platform') {
				return parent::getPath($name, $path);
			}

			return substr(parent::getPath($name, $path), 0, -4) . '.yml';
		}

		protected function getStub($table, $create)
		{
			if ($this->type !== 'platform') {
				return parent::getStub($table, $create);
			}

			return <<<EOF
			# Migration play
			# vim:et ts=2 sw=2 sts=2 syntax=yaml filetype=yaml
			---
			- block:
			  # Add tasks
			  - fail: msg="Remove me - migration step UP"
			  tags: ['up']
			- block:
			  # Add tasks
			  - fail: msg="Remove me - migration step DOWN"
			  tags: ['down']
			EOF;
		}


	}