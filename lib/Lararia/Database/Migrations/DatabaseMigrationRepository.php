<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */


	namespace Lararia\Database\Migrations;

	use Illuminate\Support\Facades\Schema;

	class DatabaseMigrationRepository extends \Illuminate\Database\Migrations\DatabaseMigrationRepository
	{
		protected $type = 'db';

		public function setType(string $type)
		{
			if ($type !== 'db' && $type !== 'platform') {
				throw new \ArgumentError("Invalid migration type {$type}");
			}
			$this->type = $type;

			return $this;
		}

		public function getRan()
		{
			if (!$this->hasColumn('type')) {
				return parent::getRan();
			}

			return $this->table()
				->where('type', $this->type)
				->orderBy('batch', 'asc')
				->orderBy('migration', 'asc')
				->pluck('migration')->all();
		}

		protected function hasColumn($name)
		{
			return Schema::hasColumn($this->table, $name);
		}

		/**
		 * Get list of migrations.
		 *
		 * @param  int $steps
		 * @return array
		 */
		public function getMigrations($steps)
		{
			if (!$this->hasColumn('type')) {
				return parent::getMigrations($steps);
			}
			$query = $this->table()->where('batch', '>=', '1');

			return $query->orderBy('batch', 'desc')
				->where('type', $this->type)
				->orderBy('migration', 'desc')
				->take($steps)->get()->all();
		}

		/**
		 * Get the last migration batch.
		 *
		 * @return array
		 */
		public function getLast()
		{
			if (!$this->hasColumn('type')) {
				return parent::getLast();
			}
			$query = $this->table()->where(['batch' => $this->getLastBatchNumber(), 'type' => $this->type]);

			return $query->orderBy('migration', 'desc')->get()->all();
		}

		/**
		 * Log that a migration was run.
		 *
		 * @param  string $file
		 * @param  int    $batch
		 * @return void
		 */
		public function log($file, $batch)
		{
			if (!$this->hasColumn('type')) {
				return parent::log($file, $batch);
			}
			$record = ['migration' => $file, 'batch' => $batch, 'type' => $this->type];

			$this->table()->insert($record);
		}

		/**
		 * Remove a migration from the log.
		 *
		 * @param  object $migration
		 * @return void
		 */
		public function delete($migration)
		{
			if (!$this->hasColumn('type')) {
				return parent::delete($migration);
			}
			$this->table()->where(['migration' => $migration->migration, 'type' => $this->type])->delete();
		}
	}
