<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2022
 */

namespace Lararia;

trait NamespaceResolverTrait {
	protected static array $resolverCallbacks = [];

	public static function registerDynamicNamespace(string $namespace, \Closure $cb): void
	{
		if ($namespace[0] !== '@') {
			fatal("Dynamic namespace must be prefixed with @");
		}
		self::$resolverCallbacks[$namespace] = $cb;
	}

	/**
	 * Register dynamic
	 * @param string $part
	 * @return mixed
	 */
	private function resolveDynamicNamespace(string $part, ?string &$arg = null)
	{
		if ($part[0] !== '@') {
			return null;
		}
		$ns = substr($part, 0, strpos($part, '('));
		$arg = trim(substr($part, strpos($part, '(') + 1, -1), '"\'');
		if (!isset(self::$resolverCallbacks[$ns])) {
			return null;
		}

		return call_user_func(self::$resolverCallbacks[$ns], $arg);
	}

}