<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs;

	use Illuminate\Contracts\Queue\ShouldQueue;
	use Lararia\Jobs\Traits\RunAs;
	use Lararia\Mail\JobFinished;
	use Module\Support\Webapps\App\Installer;
	use Module\Support\Webapps\App\Type\Unknown\Handler as Unknown;
	use Module\Support\Webapps\Messages;
	use Module\Support\Webapps\UpdateCandidate;

	class InstallAppJob extends Job implements ShouldQueue
	{
		use \apnscpFunctionInterceptorTrait;
		use RunAs;

		// only call on failure otherwise module handles
		const MAILABLE_TEMPLATE = JobFinished::class;
		const FAILED_VIEW = 'email.webapps.install.job-failed';

		/**
		 * @var int number of attempts
		 */
		public $tries = 1;

		public $timeout = 1800;
		/**
		 * @var UpdateCandidate candidate info
		 */
		protected $instance;

		/**
		 * @var Installer
		 */
		protected $installer;
		/**
		 * @var string
		 */
		protected $email;

		/**
		 * Create a new job instance.
		 *
		 * @param Installer $request
		 * @param string    $type
		 */
		public function __construct(Installer $request)
		{
			if (!$request->getModuleName()) {
				fatal(Messages::ERR_APP_UNSPECIFIED);
			}
			$instance = $request->initialize();
			$instance->setOptions(array_except($request->getOptions()->toArray(), ['password']));
			$this->instance = $instance;
			$this->installer = $request;
		}

		/**
		 * Execute the job.
		 *
		 * @return bool
		 */
		public function fire()
		{
			$ret = false;
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL, true);
			try {
				$this->instance->setContext($this->getAuthContext());
				\Preferences::factory($this->getAuthContext())->freshen();
				$ret = $this->instance->install();
			} finally {
				\Preferences::factory($this->getAuthContext())->sync();
				\Error_Reporter::exception_upgrade($oldex);
				$this->installer->getPane()->freshen();
				if (!$ret) {
					$this->installer->unlock();
				}

				gc_collect_cycles();
			}

			return $ret;
		}

		public function tags(): array
		{
			return [
				$this->getAuthContext()->site,
				$this->instance->getName(),
				$this->instance->getHostname(),
			];
		}

		public function getView()
		{
			if ($this->hasErrors()) {
				return static::FAILED_VIEW;

			}

			// notification handled per module
			return null;
		}

		public function getMailableClass()
		{
			if ($this->hasErrors()) {
				return static::MAILABLE_TEMPLATE;
			}

			return null;
		}

		public function getInstance(): Unknown
		{
			return $this->instance;
		}

		public function getEmail(): ?string
		{
			return $this->email ?? $this->instance->common_get_email();
		}

	}