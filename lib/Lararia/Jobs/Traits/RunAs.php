<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Traits;

	trait RunAs
	{
		use \ContextableTrait;

		public function __wakeup()
		{
			/**
			 * @XXX conflicts with SerializesModels. Needed to reduce job overhead in memory
			 */
			$auth = null;
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
			try {
				$auth = \Auth::profile();
			} catch (\apnscpException $e) {

			} finally {
				\Error_Reporter::exception_upgrade($oldex);
				if (!$auth || !\apnscpSession::init()->exists($auth->id)) {
					// long-running task has had its session wiped
					$context = \Auth::context(\Auth::get_admin_login(), null);
					\Auth::autoload()->setID($context->id);
					if ($auth) {
						\apnscpFunctionInterceptor::expire($auth->id);
					}
					\apnscpFunctionInterceptor::init(true)->set_session_context($context);
				}
			}

			$id = $this->getAuthContext()->id;
			if (!\apnscpSession::init()->exists($id)) {
				debug('session `%s\' no longer exists, generating new one for `%s`@`%s`',
					$id, $this->getAuthContext()->username, $this->getAuthContext()->domain);
				/** setApnscpFunctionInterceptor implicitly set by setContext */
			}

			// Generate new session IDs for each request
			// Install app (UI) -> job enqueued -> logout -> cold worker -> session ghost
			$this->setContext(
				\Auth::context($this->getAuthContext()->username, $this->getAuthContext()->site)
			);

		}
	}