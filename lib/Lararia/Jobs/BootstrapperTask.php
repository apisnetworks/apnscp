<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs;

	use Illuminate\Contracts\Queue\ShouldQueue;
	use Lararia\Jobs\Traits\RunAs;
	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Accelerator;
	use Opcenter\Apnscp;

	class BootstrapperTask extends Job implements ShouldQueue
	{
		use \apnscpFunctionInterceptorTrait;
		use RunAs;

		// only call on failure otherwise module handles
		const MAILABLE_TEMPLATE = \Lararia\Mail\BootstrapperFinished::class;
		const FAILED_VIEW = '';
		const DEFAULT_QUEUE_NAME = 'high';

		/**
		 * @var int number of attempts
		 */
		public $tries = 1;

		public $timeout = 0;

		protected $context;
		protected $task;
		protected $args;
		protected $proc;
		protected $tee;
		protected $timingLog;

		protected $withMitogen;

		/**
		 * Create a new job instance.
		 *
		 * @param string $task taskfile to run
		 * @param mixed  $args optional k => v to send to extra-vars
		 */
		public function __construct(string $task = 'bootstrap.yml', array $args = [])
		{
			\Auth::authenticated() || \Auth::get_driver()->authenticate();
			$context = \Auth::profile();
			$this->setContext($context);
			$this->task = $task;
			$this->args = $args;
			$this->proc = new \Util_Process_Safe();
			$this->tee = tempnam(storage_path('tmp'), 'bootstrap-job');
			chown($this->tee, APNSCP_SYSTEM_USER);
			// actionable logs without timestamp, run a separate log for timing (wasteful)
			$this->timingLog = tempnam(storage_path('tmp'), 'bootstrap-job.timing');
			chown($this->timingLog, APNSCP_SYSTEM_USER);
		}

		public function withoutMitogen(): self
		{
			$this->withMitogen = false;
			return $this;
		}

		/**
		 * Execute the job.
		 *
		 * @return bool
		 */
		public function fire()
		{

			$args = [
				'path' => resource_path('playbooks'),
				'task' => $this->task
			];
			$cmd = 'cd %(path)s && /usr/bin/ansible-playbook -c local %(task)s ';

			$accelerationFlags = [];

			if (!isset($this->withMitogen)) {
				$this->withMitogen = Accelerator::compatibleWithTags($this->tags);
			}

			if ($this->withMitogen) {
				$accelerationFlags = Accelerator::getEnvironment();
			}

			if ($this->tags) {
				$cmd .= '--tags=%(tags)s ';
				$args['tags'] = implode(',', $this->tags);
			}
			$cmd .= implode(' ', array_key_map(static function ($k, $v) {
				return '--extra-vars=' . (!\is_int($k) ? "{$k}=" : '') . escapeshellarg($v);
			}, $this->args));
			$old = null;
			pcntl_sigprocmask(SIG_SETMASK, [SIGABRT, SIGHUP], $old);
			$this->proc->addCallback(static function ($cmd, \Util_Process $self) {
				posix_setsid();
			}, 'open');
			$this->proc->addCallback(static function() use ($old) {
				pcntl_sigprocmask(SIG_SETMASK, $old);
			}, 'close');
			$this->proc->setTee($this->tee);
			$this->proc->setEnvironment([
				'HOME'                    => '/root',
				'ANSIBLE_LOG_PATH'        => $this->getTimingLog(),
				'ANSIBLE_STDOUT_CALLBACK' => is_debug() ? 'debug' : 'actionable_classic'
			] + $accelerationFlags);
			$ret = $this->proc->run($cmd, $args);

			return $ret['success'];
		}

		public function getProcess()
		{
			return $this->proc;
		}

		public function getEmail(): ?string
		{
			Apnscp::wait();
			return $this->common_get_email();
		}

		public function getTee()
		{
			return $this->tee;
		}

		public function getTimingLog()
		{
			return $this->timingLog;
		}

		public function getMailableClass()
		{
			if (Bootstrapper::running()) {
				// panel restarted, still processing
				return null;
			}
			return parent::getMailableClass();
		}


	}