<?php declare(strict_types=1);

namespace Lararia\Jobs\Job\Reports;

class BootstrapperTask {
	const DATE_FORMAT = 'Y-m-d H:i:s,u';
	// @var string $data
	protected $data;
	// @var array $stats
	protected $stats = [
		'ok'      => 0,
		'changed' => 0,
		'success' => false
	];

	public function __construct(string $data)
	{
		if ($data[0] === '/') {
			if (!file_exists($data)) {
				fatal("Requested Bootstrapper log `%s' does not exist", $data);
			}
			$data = file_get_contents($data);
		}
		$this->data = explode("\n", $data);

		$this->extract();
	}

	private function extract() {
		// fetch ending stats
		for ($i = 10; $i > 0; $i--) {
			if (null === ($line = array_pop($this->data))) {
				break;
			}
			if (str_contains($line, 'changed=') && preg_match(\Regex::BOOTSTRAPPER_STATUS, $line, $match)) {
				$this->stats = [
					'ok'          => (int)$match['ok'],
					'changed'     => (int)$match['changed'],
					'unreachable' => (int)$match['unreachable'],
					'failed'      => (int)$match['failed'],
					'end'         => \DateTime::createFromFormat(self::DATE_FORMAT, $match['end'])->getTimestamp(),
					'success'     => (int)$match['failed'] === 0
				];
				break;
			}
		}

		// fetch beginning timestamp
		for ($i = 0; $i < 20; $i++) {
			if (null === ($line = array_shift($this->data))) {
				break;
			}

			$words = explode(' ', $line);
			$ts = \DateTime::createFromFormat(self::DATE_FORMAT, implode(' ', \array_slice($words, 0, 2)));
			if ($ts) {
				$this->stats['begin'] = $ts->getTimestamp();
				break;
			}
		}
	}

	/**
	 * Task still running from restart
	 *
	 * @return bool
	 */
	public function pending(): bool
	{
		// halt reporting during panel restart
		return empty($this->stats['end']);
	}

	public function stats(): array
	{
		return $this->stats;
	}

	public function get(string $type) {
		return $this->stats[$type] ?? null;
	}
}