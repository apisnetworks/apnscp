<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace Lararia\Http;

use Illuminate\Contracts\Support\Responsable;

class JsonResponse implements Responsable {
	/**
	 * @var array
	 */
	protected $content = [
		'success' => false,
		'errors'  => [],
		'return'  => null
	];

	public function __construct($response)
	{
		$this->content = [
			'return'  => !$response instanceof Exception ? $response : null,
			'success' => !\Error_Reporter::is_error() && !$response instanceof Exception,
			'errors'  => $response instanceof Exception ? (array)$response->getMessage() : \Error_Reporter::get_errors()
		];
	}

	public function __toString()
	{
		return json_encode($this->content);
	}

	public function toArray(): array {
		return $this->content;
	}

	public function success(): bool
	{
		return $this->content['success'];
	}

	public function getStatusCode(): int
	{
		if ($this->success()) {
			return 200;
		}

		return 500;
	}

	public function toResponse($request)
	{
		return (string)$this;
	}


}