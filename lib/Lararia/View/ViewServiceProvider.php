<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2022
 */

namespace Lararia\View;

use Illuminate\Contracts\View\Factory as FactoryAlias;
use Illuminate\View\Compilers\Compiler;
use Illuminate\View\ViewServiceProvider as Base;

class ViewServiceProvider extends Base
{
	public function register()
	{
		parent::register();

		$this->registerNamespaces();

		$this->registerDirectives();
	}

	/**
	 * Register the view finder implementation.
	 *
	 * @return void
	 */
	public function registerViewFinder()
	{
		$this->app->bind('view.finder', function ($app) {
			return new NamespacedViewFinder($app['files'], $app['config']['view.paths']);
		});
	}

	protected function registerNamespaces()
	{
		$this->app->afterResolving('view', static function (FactoryAlias $c) {

			$c->addNamespace('master', [
				config_path('custom/resources/views'),
				resource_path('views')
			]);

			$c->addNamespace('theme', [
				config_path('custom/resources/views'),
				resource_path('views')
			]);

			foreach (\Module\Support\Webapps\PathManager::applicationViewPaths() as $appName => $paths) {
				$c->addNamespace("@webapp({$appName})", $paths);
			}
		});

		NamespacedViewFinder::registerDynamicNamespace('@app', static function ($name) {
			return \Frontend\Multipath::strip(\Template_Engine::init()->getPathFromApp($name) . '/views');
		});
	}

	protected function registerDirectives()
	{
		$this->app->afterResolving('blade.compiler', static function (Compiler $c) {
			$c->component('theme::partials.app.modal', 'modal');

			$c->directive('lang', static function ($expression) use ($c) {
				return "<?php echo ArgumentFormatter::format($expression) ?>";
			});

			$c->directive('inline', static function ($expression) use ($c) {
				if ($expression[0] == '(') {
					// weird, on helios the directive comes through as "(email.css)"
					$expression = trim($expression, '()');
				}
				if ($expression[0] != '/') {
					$paths = $c->container->config['view.paths'];
					foreach ($paths as $p) {
						if (file_exists("{$p}/{$expression}")) {
							$expression = "{$p}/{$expression}";
							break;
						}
					}
				}

				return file_get_contents($expression);
			});
		});
	}

}