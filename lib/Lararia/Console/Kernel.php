<?php

	namespace Lararia\Console;

	use Illuminate\Console\Scheduling\Schedule;
	use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Output\OutputInterface;

	class Kernel extends ConsoleKernel
	{
		/**
		 * The Artisan commands provided by your application.
		 *
		 * @var array
		 */
		protected $commands = [
			//
		];

		/**
		 * Bootstrap
		 *
		 * @param InputInterface  $input
		 * @param OutputInterface $output
		 * @return bool|int
		 */
		public function handle($input, $output = null)
		{
			if ($this->isArtisan()) {
				return parent::handle($input, $output);
			}

			$this->bootstrap();

			return true;
		}

		/**
		 * Console invocation is artisan or apnscp wrapper
		 *
		 * @return bool
		 */
		protected function isArtisan(): bool
		{
			if (!\defined('ARTISAN_BINARY')) {
				return false;
			}

			return basename($_SERVER['argv'][0]) === ARTISAN_BINARY;
		}

		/**
		 * Define the application's command schedule.
		 *
		 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
		 * @return void
		 */
		protected function schedule(Schedule $schedule)
		{
			// $schedule->command('inspire')
			//          ->hourly();
		}

		/**
		 * Register the commands for the application.
		 *
		 * @return void
		 */
		protected function commands()
		{
			$this->load(__DIR__ . '/Commands');

			require config_path('laravel-routes/console.php');
		}
	}
