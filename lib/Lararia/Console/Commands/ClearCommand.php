<?php declare(strict_types=1);

/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
 */

namespace Lararia\Console\Commands;

use Illuminate\Support\Facades\Redis;

class ClearCommand extends \Illuminate\Cache\Console\ClearCommand
{
	private const FLUSHABLE_ENTRIES = [
		'spec:*'
	];
	public function handle()
	{
		parent::handle();

		$cache = \Cache_Global::spawn();
		$keys = $cache->keys(...static::FLUSHABLE_ENTRIES);
		$cache->multi(\Redis::PIPELINE)->setOption(\Redis::OPT_PREFIX, '');
		foreach ($keys as $key) {
			$cache->del($key);
		}
		$cache->exec();
	}
}