<?php

	namespace Lararia\Console\Commands;

	use Illuminate\Console\Command;
	use Opcenter\Account\Ephemeral;
	use Opcenter\CliParser;
	use Opcenter\Map;
	use Opcenter\Service\ConfigurationContext;
	use Opcenter\Service\Plans;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;
	use Symfony\Component\Console\Helper\Table;
	use Symfony\Component\Console\Helper\TableSeparator;
	use Symfony\Component\Finder\Finder;

	/**
	 * @property Finder finder
	 */
	class OpcenterPlanCommand extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'opcenter:plan {plan?}
		{--list : Show available plans.}
    	{--new : Create a new plan.}
        {--edit : Edit existing plan.}
        {--service= : Named service to edit.}
    	{--base= : Used with --new to rebase plan.}
        {--c|config=* : Used with --new or --edit to set config defaults.}
        {--default : Set plan as default.}
    	{--remove : Remove named plan.}
    	{--verify : Verify plan is coherent.}
    	{--diff= : Show differences in plan against target.}';

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Manage account plans';

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			if ($this->option('list')) {
				$this->line('* marks default plan');
				foreach (Plans::list() as $plan) {
					if ($plan === OPCENTER_DEFAULT_PLAN) {
						$str = "<comment>*</comment> $plan";
					} else {
						$str = $plan;
					}
					$this->info("  $str");
				}

				return;
			}

			$plan = $this->argument('plan');
			if (null !== ($target = $this->option('diff'))) {
				if (!Plans::exists($target)) {
					throw new \ArgumentError("Plan {$target} does not exist");
				}
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Plan {$target} does not exist");
				}
				$dummy = (new SiteConfiguration(null))->setPlanName($target)->getDefaultConfiguration();

				$table = new Table($this->output);
				$table->setHeaders(['Service', 'Var', $plan , $target]);
				$skip = true;
				foreach (Plans::diff($plan, $target) as $svc => $vars) {
					if (!$skip) {
						$table->addRow(new TableSeparator());
					}
					$skip = false;
					foreach ($vars as $var => $val) {
						$table->addRow([
							$svc,
							$var,
							$val,
							array_get($dummy, "{$svc}.{$var}", null)
						]);
					}
				}
				$table->render();
				return;
			}

			if ($this->option('edit')) {
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Plan {$plan} does not exist");
				}
				if (!$this->option('config')) {
					if (!$this->input->isInteractive()) {
						throw new \RuntimeException("Terminal non-interactive");
					}
					$arg = $this->option('service') ? $this->option('service') : '*';
					$files = glob(Plans::path($plan) . "/{$arg}");
					$bin = getenv('EDITOR') ?: '/usr/bin/nano';
					if (!str_starts_with($bin, '/')) {
						foreach (explode(':', getenv('PATH') ?: '/usr/bin') as $component) {
							if (file_exists($tmp = "{$component}/{$bin}")) {
								$bin = $tmp;
								break;
							}
						}
					}
					pcntl_exec($bin, $files);
					exit;
				}

				$handler = (new SiteConfiguration(null))->setPlanName($plan);
				foreach ((array)$this->option('config') as $x) {
					[$parameter,$value] = explode('=', $x, 2);
					$value = \Util_Conf::inferType($value);
					if (str_contains($parameter, ',')) {
						[$service, $parameter] = explode(',', $parameter, 2);
					} else {
						$service = $this->option('service');
					}

					if (!$service) {
						throw new \ArgumentError("No service specified");
					}

					$ctx = (new ConfigurationContext($service, $handler));
					if (!$ctx->offsetExists($parameter)) {
						throw new \ArgumentError("Invalid service parameter {$parameter}");
					}

					$class = $ctx->getValidatorClass($parameter);

					/** @var ServiceValidator $validator */
					$validator = new $class($ctx, null);

					$chkval = $value;
					if (!$validator->valid($chkval)) {
						throw new \ArgumentError("Failed validation on {$service},{$parameter}");
					}

					$ini = Map::load(Plans::path($plan) . "/{$service}", 'w');
					$ini->set($parameter, rtrim(\Util_Conf::build_ini([$value])));
					$ini->save();
					unset($ini);
				}

			}

			if ($this->option('verify')) {
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Plan {$plan} does not exist");
				}

				Plans::refresh($plan);
				$site = (new SiteConfiguration(SiteConfiguration::RESERVED_SITE, [
					'siteinfo' => [
						'admin'       => 'admin9999',
						'admin_user'  => Ephemeral::random('username'),
						'domain'      => Ephemeral::random('domain')
					]
				]))->setPlanName($plan);
				$site->releaseOnShutdown();

				if ($this->verify($site)) {
					$this->info("Plan OK");
					return true;
				}

				$errors = \Error_Reporter::get_errors();
				throw new \RuntimeException($errors[0]);
			}

			if ($this->option('edit')) {
				return true;
			}

			if ($this->option('default')) {
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Unknown plan {$plan}");
				}
				if ($plan === OPCENTER_DEFAULT_PLAN) {
					$this->warn("Default plan already set to {$plan}");

					return;
				}
				Map::load(config_path('custom/config.ini'), 'cd')->
				section('opcenter')->set('default_plan', $plan);
				\Opcenter\Admin\Settings\Setting::load('apnscp.restart')->set(true);
				$this->info("Default plan changed to {$plan}");

				return;
			}

			if ($this->option('new')) {
				if (Plans::exists($plan)) {
					throw new \ArgumentError("Plan {$plan} already exists");
				}
				$oldPlan = $this->option('base') ?? OPCENTER_DEFAULT_PLAN;
				if (!Plans::exists($oldPlan)) {
					throw new \ArgumentError("Base plan {$oldPlan} does not exist");
				}

				if ($oldPlan === $plan) {
					throw new \RuntimeException('Old plan same as new plan');
				}

				$overrides = $this->option('config') ?? [];
				$files = new \Illuminate\Filesystem\Filesystem();
				$files->copyDirectory(
					Plans::path($oldPlan),
					$path = Plans::path($plan)
				);

				$newParams = CliParser::parseServiceConfiguration($overrides);
				$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
				try {
					$site = (new SiteConfiguration(null, [
							'siteinfo' => [
								'admin' => 'admin9999',
								'admin_user' => Ephemeral::random('username'),
								'domain'     => Ephemeral::random('domain')
							]
						] + $newParams));

					if ($oldPlan !== Plans::default()) {
						$site->setPlanName($oldPlan);
					}

					if (!$this->verify($site)) {
						throw new \RuntimeException('Failed to verify new plan');
					}
				} catch (\apnscpException|\RuntimeException $e) {
					Plans::remove($plan);
					throw $e;
				} finally {
					\Error_Reporter::exception_upgrade($oldex);
				}

				$defaults = array_replace_recursive($site->getDefaultConfiguration(), $newParams);

				if (!Plans::assignDefaults($plan, $defaults)) {
					$this->error("Failed to assign defaults to {$path}");
				}

				$this->info("Plan created in {$path}");

				return;
			}

			if ($this->option('remove')) {
				if ($plan === OPCENTER_DEFAULT_PLAN) {
					throw new \ArgumentError('Cannot remove default plan');
				}
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Unknown plan {$plan}");
				}
				if (!Plans::remove($plan)) {
					throw new \RuntimeException("Failed to remove plan directory {$plan}");
				}

				return;
			}
			return $this->error('Usage: ' . $this->getSynopsis());
		}

		/**
		 * Verify plan configuration
		 *
		 * @param SiteConfiguration $cfg
		 * @return bool
		 */
		private function verify(SiteConfiguration $cfg): bool
		{
			$euid = posix_geteuid();
			posix_seteuid(0);
			defer($_, static fn() => posix_seteuid($euid));
			$cfg->releaseOnShutdown();
			$old = \Error_Reporter::set_verbose(0);
			$ret =  $cfg->verifyAll();
			\Error_Reporter::set_verbose($old);

			// replay buffer errors/exceptions
			\Error_Reporter::merge_buffer(
				\Error_Reporter::get_buffer(\Error_Reporter::E_ERROR|\Error_Reporter::E_EXCEPTION)
			);

			return $ret;
		}
	}