<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Mail;

	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;
	use Opcenter\Bandwidth\Overage;

	class BandwidthOverageNotification extends Mailable
	{
		use SerializesModels;
		protected $args;
		/**
		 * @var Overage
		 */
		protected $overage;

		/**
		 * Create a new message instance.
		 *
		 * @param Overage $arg
		 */
		public function __construct(Overage $arg)
		{
			$this->overage = $arg;
		}

		public function build()
		{
			$subject = $this->getSubject();
			return $this->markdown($this->getMailable(),
				[
					'overage' => $this->overage,
				]
			)->subject($subject);
		}

		protected function getSubject(): string
		{
			return 'Bandwidth overage notification - ' .
				$this->overage->common_get_service_value('siteinfo','domain') ;
		}

		protected function getMailable(): string
		{
			return 'email.bandwidth.site.overage';
		}
	}