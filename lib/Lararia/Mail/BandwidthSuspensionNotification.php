<?php
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */

	declare(strict_types=1);

	namespace Lararia\Mail;

	class BandwidthSuspensionNotification extends BandwidthOverageNotification
	{
		protected function getSubject(): string
		{
			return 'Bandwidth suspension notification - ' .
				$this->overage->common_get_service_value('siteinfo', 'domain');
		}

		protected function getMailable(): string
		{
			return 'email.bandwidth.site.suspension';
		}
	}