<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Dav;

	use Sabre\DAV as Provider;

	class Server
	{
		const DAV_DB = '/tmp/dav.db';
		use \FilesystemPathTrait;
		public $dav_path;
		public $http_auth_realm = 'Secured Directory';
		public $dav_powered_by = PANEL_BRAND . ' WebDAV';
		protected $server;

		public function __construct(string $base = '')
		{
			$this->dav_path = $base;
			$publicDir = new Directory($base);
			$this->server = new Provider\Server($publicDir);
			$this->server->setBaseUri($base);
		}

		public static function handle()
		{
			if ($_SERVER['SERVER_PORT'] == 2077 || $_SERVER['SERVER_PORT'] == 2078) {
				$dav = new static('/');
			} else {
				$dav = new static('/dav');
			}

			$dav->run();

		}

		public function run()
		{
			$this->auth();
			$this->loadPlugins();

			return $this->server->exec();
		}

		/**
		 * Turn on authentication
		 */
		protected function auth()
		{
			$authBackend = new Provider\Auth\Backend\BasicCallBack(static function ($username, $password) {
				return \Auth::handle();
			});
			$authPlugin = new Provider\Auth\Plugin($authBackend);
			$this->server->addPlugin($authPlugin);
		}

		public function loadPlugins()
		{
			if (DAV_BROWSER) {
				$this->enableIndex();
			}
			// enable mount
			$this->server->addPlugin(new Provider\Mount\Plugin());

			$this->enableProperties();
			$this->enableLocks();
		}

		protected function enableIndex()
		{
			$plugin = new Provider\Browser\Plugin();
			$this->server->addPlugin($plugin);
			$this->server->addPlugin(new Provider\Browser\GuessContentType());
		}

		protected function enableProperties()
		{
			if (\extension_loaded('sqlite')) {
				$fsbase = $this->domain_fs_path();
				$storageBackend = new Provider\Locks\Backend\Sqlite("sqlite://{$fsbase}/" . static::DAV_DB);
			} else {
				$pdo = \MySQL::pdo();
				$storageBackend = new Backends\GlobalPropertyStorage($pdo);
			}
			$propertyStorage = new Provider\PropertyStorage\Plugin($storageBackend);
			$this->server->addPlugin($propertyStorage);
		}

		/**
		 * Enable lock support on DAV
		 *
		 * @return bool
		 */
		protected function enableLocks()
		{
			if (\extension_loaded('sqlite')) {
				$fsbase = $this->domain_fs_path();
				$locksBackend = new Provider\Locks\Backend\Sqlite("sqlite://{$fsbase}/" . static::DAV_DB);
			} else {
				$pdo = \MySQL::pdo();
				$locksBackend = new Backends\GlobalLockStorage($pdo);
			}

			// Add the plugin to the server.
			$locksPlugin = new Provider\Locks\Plugin(
				$locksBackend
			);

			return $this->server->addPlugin($locksPlugin);
		}
	}