<?php

	class Net_WSDL
	{
		const NAMESPACE = 'urn:net.apnscp.api';

		protected static $knownTraits = [];

		public static function generate()
		{
			$portTypeWSDL = $bindingWSDL = $serviceWSDL = $messageWSDL = $typesWSDL = array();
			$headerWSDL = "<?xml version=\"1.0\" ?>\n";
			$headerWSDL .= '<wsdl:definitions name="net.apnscp.api" targetNamespace="' . self::NAMESPACE . '" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" ' .
				"\t" . 'xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="' . self::NAMESPACE . '" xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' .
				"\t" . 'xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"  xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" ' .
				' xmlns="http://schemas.xmlsoap.org/wsdl/"  xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" ' .
				"\t" . 'xmlns:s="http://www.w3.org/2001/XMLSchema">' . "\n" .
				'<wsdl:types xmlns="http://schemas.xmlsoap.org/wsdl/">' . "\n";

			$afi = apnscpFunctionInterceptor::init();
			$exported_commands = $afi->get_loaded_modules();
			$reflection_cache = $afi->get_reflection_cache();
			$baseMethods = new ReflectionClass('Module_Skeleton');

			$inheritedMethods = array();
			foreach ($baseMethods->getMethods() as $method) {
				$inheritedMethods[$method->getName()] = 1;
			}

			foreach ($reflection_cache as $module => $class) {
				/** nothing to expose */
				$exports = $exported_commands->getModule($module)->getExportedFunctions();

				if (sizeof($exports) == 0) {
					continue;
				}
				$portTypeWSDL[$module] = '<portType name="' . $module . '_port">' . "\n";
				$bindingWSDL[$module] = '<binding name="' . $module . '_binding" type="tns:' . $module . '_port">' . "\n" .
					"\t" . '<soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http" />' . "\n";
				$serviceWSDL[$module] = "\t" . '<service name="' . $module . '_service">' . "\n" .
					"\t" . "\t" . '<documentation />' . "\n" .
					"\t" . "\t" . '<port name="' . $module . '_port" binding="tns:' . $module . '_binding">' . "\n" .
					"\t" . "\t" . "\t" . '<soap:address location="https://' . SERVER_NAME . ':' . Util_API::SECURE_PORT . '/soap" />' . "\n" .
					"\t" . "\t" . '</port>' . "\n" .
					"\t" . '</service>' . "\n";
				$typesWSDL[$module] = $messageWSDL[$module] = null;
				$default = isset($exports['*']) ? $exports['*'] : 0;

				$traitNames = [];
				foreach ($class->getTraits() as $trait) {
					$hash = $trait->getName();
					if (!isset(self::$knownTraits[$hash])) {
						self::$knownTraits[$hash] = array_flip(array_column($trait->getMethods(), 'name'));
					}
					$traitNames += self::$knownTraits[$hash];
				}
				foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
					$funcName = $method->getName();
					$perms = isset($exports[$funcName]) ? $exports[$funcName] : $default;
					// allow surrogate export
					$declaring = $method->getDeclaringClass()->getName();
					if ($funcName[0] === '_' || isset($traitNames[$funcName]) || !$method->isPublic() || 0 === strpos($declaring,
						apnscpFunctionInterceptor::MODULE_SKELETON_NAMESPACE) || 0 === strncmp($declaring, 'Module\\Support\\', 15)
						|| $perms & PRIVILEGE_SERVER_EXEC
					) {
						continue;
					}
					$portTypeWSDL[$module] .= "\t" . '<operation name="' . $module . '_' . $funcName . '">' . "\n" .
						"\t" . "\t" . '<input message="tns:' . $module . '_' . $funcName . '_request" />' . "\n" .
						"\t" . "\t" . '<output message="tns:' . $module . '_' . $funcName . '_response" />' . "\n" .
						"\t" . '</operation>' . "\n";
					$bindingWSDL[$module] .= "\t" . '<operation name="' . $module . '_' . $funcName . '">' . "\n" .
						"\t" . "\t" . '<soap:operation soapAction="' . self::NAMESPACE . '#' . $module . '_' . $funcName . '" />' . "\n" .
						"\t" . "\t" . '<input>' . "\n" .
						"\t" . "\t" . "\t" . '<soap:body use="encoded" namespace="' . self::NAMESPACE . '" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" />' . "\n" .
						"\t" . "\t" . '</input>' . "\n" .
						"\t" . "\t" . '<output>' . "\n" .
						"\t" . "\t" . "\t" . '<soap:body use="encoded" namespace="' . self::NAMESPACE . '" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" />' . "\n" .
						"\t" . "\t" . '</output>' . "\n" .
						"\t" . '</operation>' . "\n";

					$comments = $method->getDocComment();
					$parameters = $method->getParameters();
					$opt_params = (sizeof($parameters) - $method->getNumberOfRequiredParameters());

					// possibly busted variable parameter code...
					if (0 && $opt_params > 0) {

						$typesWSDL[$module] .= "\t" . '<xsd:element name="' . $module . '_' . $funcName . '_params">' . "\n" .
							"\t\t" . '<xsd:complexType>' . "\n" .
							"\t\t\t" . '<xsd:sequence>' . "\n";
						$i = 0;
						foreach ($parameters as $parameter) {
							$paramType = 'xsd:anyType';
							if (preg_match('/@param ([^\s]+) \$' . $parameter->getName() . '\b/', $comments,
								$paramMatch)) {
								$paramType = self::_param2WSDL($paramMatch[1]);
							}
							$min = (int)($i < (sizeof($parameters) - $opt_params));
							$nillable = $paramType[0] === '?' ? 'nillable="true"' : '';
							$typesWSDL[$module] .=
								"\t\t\t\t" . '<xsd:element name="' . $parameter->getName() . '" type="' . $paramType . '" maxOccurs="1" minOccurs="' . $min . '" ' . $nillable . '/>' . "\n";
							$i++;
						}
						$typesWSDL[$module] .= "\t\t\t" . '</xsd:sequence>' . "\n" .
							"\t\t" . '</xsd:complexType>' . "\n" .
							"\t" . '</xsd:element>' . "\n";
						$messageWSDL[$module] .= "\t" . '<message name="' . $module . '_' . $funcName . "_request\">\n" .
							"\t" . "\t" . '<part name="' . $parameter->getName() . '" type="tns:' . $module . '_' . $funcName . '_params" />' . "\n";
					} else {
						$messageWSDL[$module] .= "\t" . '<message name="' . $module . '_' . $funcName . "_request\">\n";
						$i = 0;
						foreach ($parameters as $parameter) {
							$paramType = 'xsd:anyType';
							if ($parameter->getType() === NULL && preg_match('/@param ([^\s]+) \$' . $parameter->getName() . '\b/', $comments,
								$paramMatch)) {
								$paramType = self::_param2WSDL($paramMatch[1]);
							}
							$nillable = ($i < (count($parameters) - $opt_params)) && $paramType[0] !== '?' ? 'false' : 'true';
							$messageWSDL[$module] .= "\t" . "\t" . '<part name="' . $parameter->getName() . '" nillable="' . $nillable . '" type="' . $paramType . '" />' . "\n";
							$i++;
						}
					}

					$messageWSDL[$module] .= "\t" . '</message>' . "\n" .
						"\t" . '<message name="' . $module . '_' . $funcName . '_response">' . "\n";

					// handle return value
					$returnType = 'xsd:anyType';
					if (!$method->getReturnType() instanceof ReflectionUnionType &&
						is_scalar($tmp = $method->getReturnType()?->getName()))
					{
						$returnType = self::_param2WSDL($tmp);
					} else if ( preg_match('/@return ([^\s]+)' . '\b/', $comments, $returnMatch)) {
						$returnType = self::_param2WSDL($returnMatch[1]);
						if ($returnType === false) {
							dlog('WARNING: Unrecognized return type ' . $returnMatch[1] . ' for ' . ucwords($module) . '_Module::' . $funcName);
						}
					}
					$nillable = $returnType && $returnType[0] === '?' ? 'nillable="true"' : '';
					// close off the message
					$messageWSDL[$module] .= "\t" . "\t" . '<part name="return" type="' . $returnType . '" ' . $nillable . ' />' . "\n" .
						"\t" . '</message>' . "\n";
				}

				$portTypeWSDL[$module] .= '</portType>' . "\n";
				$bindingWSDL[$module] .= '</binding>' . "\n";

			}
			$wsdl = str_replace(['\n', '\t', '\r'], '', sprintf('%s%s</wsdl:types>%s%s%s%s</wsdl:definitions>',
				$headerWSDL,
				implode('', $typesWSDL),
				implode('', $messageWSDL),
				implode('', $portTypeWSDL),
				implode('', $bindingWSDL),
				implode('', $serviceWSDL)));

			return $wsdl;
		}

		private static function _param2WSDL($mParam)
		{
			switch (ltrim($mParam, '?')) {
				case 'string':
				case 'str':
					return 'xsd:string';
				case 'mixed':
					return 'xsd:anyType';
				case 'object':
					return 'xsd:struct';
				case 'array':
					return 'xsd:array';
				case 'int':
				case 'integer':
					return 'xsd:int';
				case 'bool':
				case 'boolean':
					return 'xsd:boolean';
				case 'float':
				case 'double':
					return 'xsd:float';
				case 'void':
					return '';
				case 'null':
					return '';
				default:
					if (strpos($mParam, '|') === false) {
						error('Non-representable API type %s', $mParam);
					}
					return 'xsd:anyType';
			}
		}
	}