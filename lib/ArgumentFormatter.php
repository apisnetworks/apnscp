<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2020
 */

class ArgumentFormatter {

	/**
	 * Format named arguments into string
	 *
	 * @param string $str
	 * @param array  $args
	 * @return string
	 */
	public static function format(string|Stringable $str, array $args = []): string
	{
		if ($str instanceof Stringable) {
			$str = (string)$str;
		}

		if ($args === []) {
			return $str;
		}

		if (isset($args[0]) && is_array($args[0])) {
			// passed in from spread operator
			$args = $args[0];
		}

		// cmd commands symbolic format parameters
		// arguments always presented as a hash
		$pos = strpos($str, '%(');
		if ($pos !== false) {
			return static::setNamed($args, $str, $pos);
		}

		return static::synthesizeCommand($str, $args);
	}

	/**
	 * Format arguments into command
	 *
	 * @param string $cmd
	 * @param array  $args
	 * @return string|null
	 */
	protected static function synthesizeCommand(string $cmd, array $args = []): string
	{
		return vsprintf($cmd, $args);
	}

	/**
	 *  named arguments
	 *  first el in $args will be hash of arguments
	 *
	 * @param array  $args
	 * @param string $str
	 * @param int    $pos initial position to scan
	 * @return string
	 */
	protected static function setNamed(array &$args, string &$str, int $pos): string
	{
		// map each format param by its numeric location
		// to substitute later as %n$
		$table = array();
		$n = 1;

		foreach ($args as $k => $v) {
			$table[$k] = $n;
			$n++;
		}
		if ($str[$pos] !== '%') {
			fatal('Invalid positional offset usage');
		}
		$new = [];
		$start = 0;
		while (false !== $pos) {
			$len = $pos - $start;
			$new[] = substr($str, $start, $len);
			$pos += 2 /* %( */
			;
			$n = strpos($str, ')', $pos);
			if ($n === false) {
				warn('malformed format var name');
				break;
			}
			$symlen = $n - $pos;
			$sym = substr($str, $pos, $symlen);
			$pos += $symlen + 1 /* ) */
			;
			// lookup sym position
			if (isset($table[$sym])) {
				$new[] = '%' . $table[$sym] . '$';
			} else {
				warn("unknown format var `%s'", $sym);
			}
			$start = $pos;
			$pos = strpos($str, '%(', $pos);
		}
		$new[] = substr($str, $start);
		$str = implode('', $new);

		return static::synthesizeCommand($str, $args);
	}
}