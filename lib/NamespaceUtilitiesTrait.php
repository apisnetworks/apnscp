<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	trait NamespaceUtilitiesTrait
	{
		/**
		 * Get base name from fully-qualified namespace
		 *
		 * @param string $class optional class resolution override
		 * @return string
		 */
		public static function getBaseClassName(string $class = null): string
		{
			return (new \ReflectionClass($class ?? static::class))->getShortName();
		}

		/**
		 * Class parent
		 *
		 * @param string|null $class
		 * @return string|null
		 */
		public static function getParentClassName(string $class = null): ?string
		{
			if (false === ($parent = (new \ReflectionClass($class ?? static::class))->getParentClass())) {
				return null;
			}
			return $parent->getName();
		}

		/**
		 * Append a namespaced class to an existing namespace
		 *
		 * @param string $class
		 * @return string composite fully-qualified class name
		 */
		public static function appendNamespace(string $class): string
		{
			return self::getNamespace() . '\\' . $class;
		}

		/**
		 * Get namespace from class
		 *
		 * @param string $class optional class resolution override
		 * @return string
		 */
		public static function getNamespace(string $class = null): string
		{
			return (new \ReflectionClass($class ?? static::class))->getNamespaceName();
		}
	}