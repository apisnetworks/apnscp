<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 *
	 * Based on work by Jens Segers (jenssegers/blade)
	 */

	use Illuminate\Container\Container;
	use Illuminate\Contracts\Container\Container as ContainerInterface;
	use Illuminate\Contracts\Foundation\Application;
	use Illuminate\Contracts\View\Factory as FactoryContract;
	use Illuminate\Events\Dispatcher;
	use Illuminate\Filesystem\Filesystem;
	use Illuminate\Support\Facades\Facade;
	use Illuminate\View\Compilers\BladeCompiler;
	use Illuminate\View\Factory;

	class BladeLite implements FactoryContract
	{
		const CACHE_PATH = 'framework/cache';

		/**
		 * @var static $instance
		 */
		protected static $instance;

		/**
		 * @var Application
		 */
		protected $container;

		/**
		 * @var Factory
		 */
		private $factory;

		/**
		 * @var BladeCompiler
		 */
		private $compiler;
		private array $viewPaths;


		/**
		 * Instantiate a new Blade instance
		 *
		 * @param array|string $viewPaths
		 * @return static
		 */
		public static function factory($viewPaths = 'views'): self
		{
			return new static($viewPaths, storage_path(self::CACHE_PATH));
		}

		public function __construct($viewPaths, string $cachePath, ContainerInterface $container = null)
		{
			$this->container = $container ?: new Container;
			$this->setupContainer((array)$this->resolveViewPaths($viewPaths), $cachePath);
			(new \Lararia\View\ViewServiceProvider($this->container))->register();

			$this->factory = $this->container->get('view');
			$this->compiler = $this->container->get('blade.compiler');
		}

		/**
		 * @param string|array $viewPaths
		 * @return array
		 */
		private function resolveViewPaths($viewPaths): array
		{
			if (is_string($viewPaths) && $viewPaths[0] !== '/') {
				$viewPaths = str_replace('.', '/', $viewPaths);
				$paths = [resource_path('')];
				if (class_exists(\Lararia\Application::class, false)) {
					// if \Lararia\Bootstrapper::minstrap() called, use full config
					$paths = array_get(\Lararia\Application::getInstance()['config'], 'view.paths', []);
				} else if (file_exists($file = conf_path('laravel/view.php'))) {
					$paths = array_get((array)include($file), 'paths', $paths);
				}
				foreach ($paths as $key => $path) {
					// append path to lookups, may change in future
					if ($path && file_exists($tmp = $path . DIRECTORY_SEPARATOR . $viewPaths)) {
						$paths[$key] = $tmp;
						continue;
					}
					unset($paths[$key]);
				}
				$viewPaths = $paths;
			}

			return (array)$viewPaths;
		}

		public function component($path, $alias = null)
		{
			$alias = $alias ?: array_last(explode('.', $path));

			$this->compiler()->directive($alias, static function ($expression) use ($path) {
				return $expression
					? "<?php \$__env->startComponent('{$path}', {$expression}); ?>"
					: "<?php \$__env->startComponent('{$path}'); ?>";
			});

			$this->compiler()->directive('end' . $alias, static function ($expression) {
				return '<?php echo $__env->renderComponent(); ?>';
			});
		}

		/**
		 * Create or get a Blade instance and update paths as necessary
		 *
		 * @param string $viewPaths
		 * @return BladeLite
		 */
		public static function singleton($viewPaths = null)
		{
			if (null !== self::$instance && $viewPaths) {
				self::$instance->updateConfig($viewPaths);
			} else if (null === self::$instance) {
				self::$instance = new static($viewPaths, storage_path(self::CACHE_PATH));
			}

			return self::$instance;
		}

		public function emptyPaths()
		{
			$this->viewPaths = [];
			$this->setConfig();
		}

		protected function setConfig()
		{
			$this->container->bind('config', function () {
				return [
					'view.paths'    => (array)$this->viewPaths,
					'view.compiled' => $this->cachePath,
				];
			}, true);
		}

		public function updateConfig($paths)
		{
			$config = array_get($this->container->config, 'view.paths', []);
			$config = array_merge($config, (array)$paths);
			$this->viewPaths = $config;
			$this->setConfig();
		}

		public function exists($view): bool
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function file($path, $data = [], $mergeData = []): \Illuminate\Contracts\View\View
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function make($view, $data = [], $mergeData = []): \Illuminate\Contracts\View\View
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function composer($views, $callback): array
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function creator($views, $callback): array
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		protected function setupContainer(array $viewPaths, string $cachePath)
		{
			$accessor = Facade::getFacadeApplication();

			$this->container->bindIf('files', function () {
				return new Filesystem;
			}, true);

			$this->container->bindIf('events', function () {
				return new Dispatcher;
			}, true);

			$this->container->bindIf('config', function () use ($viewPaths, $cachePath) {
				return [
					'view.paths'    => $viewPaths,
					'view.compiled' => $cachePath,
				];
			}, true);

			Facade::setFacadeApplication($this->container);

			if (null !== $accessor) {
				Facade::setFacadeApplication($accessor);
			}
		}

		public function render(string $view, array $data = [], array $mergeData = []): string
		{
			return $this->make($view, $data, $mergeData)->render();
		}

		public function compiler(): BladeCompiler
		{
			return $this->compiler;
		}

		public function if($name, callable $callback)
		{
			$this->compiler->if($name, $callback);
		}

		public function share($key, $value = null)
		{
			return $this->factory->share($key, $value);
		}

		public function addNamespace($namespace, $hints): self
		{
			$this->factory->addNamespace($namespace, $hints);

			return $this;
		}

		public function replaceNamespace($namespace, $hints): self
		{
			$this->factory->replaceNamespace($namespace, $hints);

			return $this;
		}

		public function __call(string $method, array $params)
		{
			return call_user_func_array([$this->factory, $method], $params);
		}


		public static function parseString($data, array $args = []): string
		{
			$generated = view()->compiler()->compileString($data);

				ob_start() and extract($args, EXTR_SKIP);

				// We'll include the view contents for parsing within a catcher
				// so we can avoid any WSOD errors. If an exception occurs we
				// will throw it out to the exception handler.
				try {
					eval('?>' . $generated);
				}

				// If we caught an exception, we'll silently flush the output
				// buffer so that no partially rendered views get thrown out
				// to the client and confuse the user with junk.
				catch (\Exception $e) {
					ob_get_clean();
					throw $e;
				}

				$content = ob_get_clean();

				return $content;
		}

	}