<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2021
 */

class PingablePDO extends  PDO
{
	private array $params;

	public function __construct($dsn, $username = null, #[SensitiveParameter] $password = null, $options = null)
	{
		$this->params = func_get_args();
		try {
			parent::__construct(...$this->params);
		} catch (\PDOException $e) {
			static $ctr = 0;
			if (empty($e->errorInfo)) {
				Error_Reporter::report(var_export($e->errorInfo, true) . " " . $e->getMessage());
			}
			if ($e->getMessage() === "Constructor failed" || $e->errorInfo[0] === "08006" /* database system is in recovery mode */) {
				sleep(1);
				if (!$ctr) {
					warn("Connection failed: %s", $e->errorInfo[2] ?? $e->getMessage());
				}
				if ($ctr++ < 10) {
					$this->__construct(...$this->params);
				}

				return;
			}
			fatal($e->getMessage());
		}
	}

	public function ping(): PDO
	{
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
		try {
			$this->exec("SELECT 1");
		} catch (\PDOException $e) {
			if ($e->errorInfo[0] === 'HY000') {
				// disconnected
				return new static(...$this->params);
			}
			throw $e;
		} finally {
			\Error_Reporter::exception_upgrade($oldex);
		}

		return $this;
	}
}