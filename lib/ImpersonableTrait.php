<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */
	trait ImpersonableTrait
	{
		/**
		 * Login as requested credentials
		 *
		 * @param string      $site
		 * @param string|null $user
		 * @param string|null $gate authentication gate to hijack
		 * @return string
		 */
		protected function impersonateRole(string $site, string $user = null, string $gate = null): string
		{
			$parent = \session_id();
			$tv = \Session::get('auth_timeout', \Auth::TV_SEC);
			$prefs = \Preferences::factory($old = \Auth::profile());
			if ($prefs->dirty()) {
				$prefs->sync();
			}
			$context = \Auth::context($user, $site);
			if ($old->level & PRIVILEGE_EXTAUTH) {
				$context->overrideLevel(($context->level & PRIVILEGE_ALL) | PRIVILEGE_EXTAUTH);
			}
			\Auth::autoload()->beginImpersonation($context, $gate);
			if (!\apnscpSession::restore_from_id($context->id, false)) {
				fatal("role mockup failed, cannot impersonate `%s'@`%s'", $context->username, $context->domain);
			}

			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($context));
			/**
			 * Scoped authentication creates a session in the specific auth driver.
			 * If scope is current, ignore import() to prevent duplicate class loading
			 */
			if (!$gate || $gate === \Auth::autoload()->getDriver()) {
				$auth = \Auth::autoload()->setID($context->id);
				$auth->authInfo(true);
				$gate = $auth->getDriver();

				/**
				 * need to be doubly sure the session isn't overwritten by initializeUser
				 * impersonation ends the active session + forces redirect to stabilize state
				 */
				defer($_, static fn() => $context->setImpersonator($parent));
			} else {
				\Auth::import($gate)->setID($context->id)->authInfo(true);
			}
			// populate session vars
			$stub = (new \Auth_Stub())->setID($context->id);
			if ($stub->initializeStubSession($context->username, $context->domain, null, $tv)) {
				if ($old->level & PRIVILEGE_EXTAUTH) {
					$stub->getProfile()->overrideLevel(($context->level & PRIVILEGE_ALL) | PRIVILEGE_EXTAUTH);
				}
				$stub->setAuthenticationGate($gate);
				Session::set(\Auth::VALIDITY_MARKER, true);
			}
			// force sync for cron, which doesn't terminate session immediately
			// termination triggers write
			apnscpSession::init()->sync();
			return $context->id;
		}

		protected function restoreImpersonator(\Auth_Info_User $auth): void
		{
			\Preferences::factory(\Auth::profile())->sync();
			if (null === ($parent = $auth->getImpersonator())) {
				return;
			}
			if (!\apnscpSession::restore_from_id($parent)) {
				fatal("role mockup failed, cannot restore parent ID `%s'", $parent);
			}
			$oldCtx = clone $auth;
			$auth = \Auth::autoload()->resetID($parent);
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($auth->getProfile()));
			$auth->authInfo(true);
			\Auth::autoload()->endImpersonation($oldCtx);
		}
	}