<?php
	declare(strict_types=1);

	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use Opcenter\Crypto\Keyring;
	use Symfony\Component\Yaml\Yaml;

	/**
	 * Class Keyring_Module
	 *
	 * @package core
	 */
	class Keyring_Module extends \Module_Skeleton
	{
		protected $exportedFunctions = [
			'*' => PRIVILEGE_ADMIN
		];

		/**
		 * Dot-notation key to lookup
		 *
		 * @param string $key
		 * @return bool
		 */
		public function exists(string $key): bool
		{
			if (str_starts_with($key, Keyring::KEYRING_PREFIX)) {
				$key = substr($key, strlen(Keyring::KEYRING_PREFIX));
			}
			$yaml = $this->loadYaml();
			return array_has($yaml, $key) && Keyring::is((string)array_get($yaml, $key));
		}

		/**
		 * Set a keyring value
		 *
		 * @param string $key
		 * @param mixed $value
		 * @param bool   $raw
		 * @return bool
		 */
		public function set(string $key, mixed $value, bool $raw = false): bool
		{
			if (str_starts_with($key, Keyring::KEYRING_PREFIX)) {
				$key = substr($key, strlen(Keyring::KEYRING_PREFIX));
			}

			$yaml = $this->loadYaml();
			array_set($yaml, $key, $raw ? $value : Keyring::encode($value));

			return false !== file_put_contents(config_path('auth.yaml'), Yaml::dump($yaml));
		}

		public function forget(string $key): bool
		{
			if (str_starts_with($key, Keyring::KEYRING_PREFIX)) {
				$key = substr($key, strlen(Keyring::KEYRING_PREFIX));
			}

			if (!$this->exists($key)) {
				return false;
			}

			$yaml = $this->loadYaml();
			array_forget($yaml, $key);

			return false !== file_put_contents(config_path('auth.yaml'), Yaml::dump($yaml));
		}

		/**
		 * Re-encode value by previous secret
		 *
		 * @param string $value
		 * @param string $old_secret
		 * @return string|null
		 */
		public function reencode(string $value, string $old_secret): ?string
		{
			if ($old_secret === Keyring::SECRET) {
				warn("[%(section)s] => %(option)s is same as %(var)s", [
					'section' => 'auth', 'option' => 'secret', 'var' => 'old_secret'
				]);

				return $value;
			}

			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
			try {
				$value = Keyring::decode($value, $old_secret);
			} catch (\apnscpException) {
				return error("Invalid secret. Cannot decode keyring");
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}

			return Keyring::encode($value);
		}

		/**
		 * Create a keyring literal
		 *
		 * @param string $value
		 * @return string
		 */
		public function encode(mixed $value): string
		{
			return Keyring::encode($value);
		}

		public function dump(): array
		{
			$yaml = $this->loadYaml();
			$accepted = [];
			foreach (array_dot($yaml) as $k => $v) {
				if (is_string($v) && Keyring::is($v)) {
					array_set($accepted, $k, $v);
				}
			}

			return $accepted;
		}

		/**
		 * Fetch raw keyring value
		 *
		 * @param string $key
		 * @return string|null
		 */
		public function get(string $key): ?string
		{
			if (str_starts_with($key, Keyring::KEYRING_PREFIX)) {
				$key = substr($key, strlen(Keyring::KEYRING_PREFIX));
			}

			$yaml = $this->loadYaml();
			$val = array_get($yaml, $key);

			return null === $val || !Keyring::is((string)$val) ? null : $val;
		}

		/**
		 * Encoded value is valid for this digest
		 *
		 * @param string $value
		 * @return bool
		 */
		public function valid(string $value): bool
		{
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
			try {
				Keyring::decode($value);
				return true;
			} catch (\apnscpException $e) {
				return false;
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}
		}

		private function loadYaml(): array
		{
			return Yaml::parseFile(config_path('auth.yaml'));
		}
	}