<?php declare(strict_types=1);

	use Daphnie\Collector;
	use Module\Skeleton\Contracts\Tasking;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Telemetry_Module extends Module_Skeleton implements Tasking
	{
		// @var Collector
		private $collector;

		protected $exportedFunctions = [
			'*'         => PRIVILEGE_ADMIN,
			'metrics'   => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'enabled'   => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'get'       => PRIVILEGE_ADMIN | PRIVILEGE_SITE,
			'range'     => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'histogram' => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'has'       => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'interval'  => PRIVILEGE_SITE | PRIVILEGE_ADMIN
		];

		public function __construct()
		{
			parent::__construct();
			if (!TELEMETRY_ENABLED) {
				$this->exportedFunctions = [
					'enabled' => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
					'*'       => PRIVILEGE_NONE
				];
			}
		}

		/**
		 * Telemetry is enabled
		 *
		 * @return bool
		 */
		public function enabled(): bool
		{
			if ($this->permission_level & PRIVILEGE_ADMIN) {
				return (bool)TELEMETRY_ENABLED;
			}

			return TELEMETRY_ENABLED && (bool)$this->getServiceValue('metrics', 'enabled');
		}

		/**
		 * Get latest metric value
		 *
		 * Performs a partial scan in last 12 hours
		 *
		 * @param string|array $metric metric name (dot notation)
		 * @param int|null $site_id
		 * @return int|int[]|null
		 */
		public function get($metric, int $site_id = null)
		{
			if ($this->permission_level & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				if ($site_id && $site_id !== $this->site_id) {
					error('Cannot specify site ID');
					return null;
				}
				$site_id = $this->site_id;
			}

			return $this->getCollector()->get($metric, $site_id);
		}

		/**
		 * Metric exists
		 *
		 * @param string $metric
		 * @return bool
		 */
		public function has(string $metric): bool
		{
			return null !== \Daphnie\MetricBroker::resolve($metric);
		}

		/**
		 * Get metric range
		 *
		 * @param          $metric
		 * @param int      $begin  when negative, now minus $begin
		 * @param int|null $end
		 * @param int|null $site_id
		 * @param string|bool $summable sum (bool) or interval ranges to sum as (string)
		 * @return int[]|int|null
		 */
		public function range($metric, int $begin, ?int $end = null, int $site_id = null, $summable = true)
		{
			if ($this->permission_level & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				if ($site_id && $site_id !== $this->site_id) {
					error('Cannot specify site ID');

					return null;
				}
				$site_id = $this->site_id;
			}

			return $this->getCollector()->range($metric, $begin, $end, $site_id, $summable);
		}

		/**
		 * Aggregate metrics into periodic intervals
		 *
		 * @param string|int $metric metric name or ID
		 * @param int      $begin begin timestamp
		 * @param int|null $end ending timestamp
		 * @param int|null $site_id site ID
		 * @param int      $size interval size
		 * @return mixed
		 */
		public function interval($metric, int $begin, ?int $end = null, $site_id = null, int $size = 86400)
		{
			if ($this->permission_level & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				if ($site_id && $site_id !== $this->site_id) {
					error('Cannot specify site ID');

					return null;
				}
				$site_id = $this->site_id;
			}

			if ($size < 1 || $size > 2e31) {
				return error("Invalid grouping size");
			}

			if ($size < CRON_RESOLUTION) {
				warn("Interval smaller than [cron] => resolution time %d seconds", CRON_RESOLUTION);
			}

			return $this->getCollector()->interval($metric, $begin, $end, $site_id, $size);

		}

		/**
		 * Get metric histogram
		 *
		 * @param          $metric
		 * @param int      $begin
		 * @param int|null $end
		 * @param int|null $site_id
		 * @param int      $buckets number of buckets to bin
		 * @param int|null $min     minimum value to include
		 * @param int|null $max     maximum value to include
		 * @return int[]|int|null
		 */
		public function histogram($metric, int $begin, ?int $end = null, int $site_id = null, int $buckets = 5, int $min = 0, int $max = 1024)
		{
			if ($this->permission_level & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				if ($site_id && $site_id !== $this->site_id) {
					error('Cannot specify site ID');

					return null;
				}
				$site_id = $this->site_id;
			}

			if ($buckets > 50 || $buckets < 2) {
				return error("Buckets must be within [2,50]");
			}

			if ($min !== null && $max !== null && $min > $max) {
				error("Minimum value %(min)d may not exceed %(max)d", [
					'min' => $min,
					'max' => $max
				]);

				return null;
			}

			return $this->getCollector()->histogram($metric, $begin, $end, $site_id, $buckets, $min, $max);

		}

		/**
		 * Get collector instance
		 *
		 * @return Collector
		 */
		private function getCollector(): Collector
		{
			if (!isset($this->collector)) {
				$this->collector = new Collector(\PostgreSQL::pdo());
			}

			return $this->collector;
		}

		/**
		 * Drop metric value from database
		 *
		 * @param string $metric metric to discard
		 * @param bool   $rekey  rekey attribute metadata on next run
		 * @return bool
		 */
		public function drop_metric(string $metric, bool $rekey = false): bool
		{
			if (null === ($id = $this->getCollector()->metricAsId($metric))) {
				return false;
			}

			$db = \PostgreSQL::pdo();

			$table = $rekey ? 'metric_attributes' : 'metrics';
			$chunker = new \Daphnie\Chunker($db);
			$chunker->decompressRange(null);
			$stmt = $db->prepare("DELETE FROM $table WHERE attr_id = :attr_id");
			$ret = $stmt->execute([':attr_id' => $id]);
			$chunker->release();
			return $ret ?: error('Failed to drop metric %(metric)s: %(err)s',
				['metric' => $metric, 'err' => array_get($stmt->errorInfo(), 2, '')]
			);
		}

		/**
		 * Timescale chunk statistics
		 *
		 * @return array
		 */
		public function chunks(): array
		{
			return (new \Daphnie\Chunker(\PostgreSQL::pdo()))->getChunkStats();
		}

		/**
		 * Get all metric symbols
		 *
		 * @return array
		 */
		public function metrics(): array
		{
			return array_keys($this->getCollector()->all());
		}

		/**
		 * Get metric compression usage
		 *
		 * @return array
		 */
		public function db_compression_usage(): array {
			$pg = PostgreSQL::pdo();
			$query = (new \Daphnie\Connector($pg))->vendor()->getCompressionStats();
			$res = $pg->query($query);
			if (!$res) {
				return [];
			}

			$rec = array_get($res->fetchAll(\PDO::FETCH_ASSOC), 0, []);

			foreach ($rec as $k => $v) {
				if (substr($k, -6) === '_bytes') {
					$rec[$k] = \Formatter::changeBytes($v);
				}
			}

			return (array)$rec;
		}

		/**
		 * Get metric usage
		 *
		 * @return array
		 */
		public function db_usage(): array
		{
			$pg = PostgreSQL::pdo();
			$query = (new \Daphnie\Connector($pg))->vendor()->databaseUsage();
			$res = $pg->query($query);
			if (!$res) {
				return [];
			}

			$rec = array_get($res->fetchAll(\PDO::FETCH_ASSOC), 0);

			foreach ($rec as $k => $v) {
				if (substr($k, -6) === '_bytes') {
					$rec[$k] = \Formatter::changeBytes($v);
				}
			}

			return (array)$rec;
		}

		/**
		 * Decompress all chunks
		 *
		 * Note: reinitialize_compression() must be called after this
		 *
		 * @return bool
		 */
		public function decompress_all(): bool
		{
			$pg = PostgreSQL::pdo();
			$chunker = new \Daphnie\Chunker($pg);

			return $chunker->decompressAll();
		}

		/**
		 * Reinitialize suspended compression
		 *
		 * @return bool
		 */
		public function reinitialize_compression(): bool
		{
			$pg = PostgreSQL::pdo();
			$chunker = new \Daphnie\Chunker($pg);
			foreach ($chunker->getJobs() as $job) {
				if (!$chunker->resumeJob($job['job_id'])) {
					return false;
				}
			}

			return true;
		}

		public function collect(): void
		{
			$collector = $this->getCollector();
			foreach ($collector->getAnonymousCollections() as $collection) {
				$collection->log($collector);
			}

		}

		/**
		 * TimescaleDB version
		 *
		 * @return string|null
		 */
		public function version(): ?string
		{
			if (!TELEMETRY_ENABLED) {
				return null;
			}

			return (new \Daphnie\Connector(\PostgreSQL::pdo()))->getVersion();
		}

		public function _cron(Cronus $cron)
		{
			$this->collect();
			/**
			 * Prevent losing configuration settings in allkeys-lru purge
			 */
			$cache = \Cache_Global::spawn();
			$cache->get(CONFIGURATION_KEY);
			\Lararia\JobDaemon::snapshot();
		}
	}