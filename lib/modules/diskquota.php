<?php

use Opcenter\Filesystem\Quota\Project;

/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2017
	 */

	#[Route(
		prefix: 'quota'
	)]
	class Diskquota_Module extends Module_Skeleton
	{
		const DEPENDENCY_MAP = [
			'siteinfo',
		];

		const MIN_STORAGE_AMNESTY = QUOTA_STORAGE_WAIT;
		// time in seconds between amnesty requests
		const AMNESTY_DURATION = QUOTA_STORAGE_DURATION;
		// 24 hours
		const AMNESTY_MULTIPLIER = QUOTA_STORAGE_BOOST;
		const AMNESTY_JOB_MARKER = 'amnesty';

		const ERR_PROJECT_NAME_RESERVED = [':err_project_name_reserved', "Project name begins with reserved prefix"];

		public $exportedFunctions = [
			'*' => PRIVILEGE_SITE,
			'amnesty' => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'project_create' => PRIVILEGE_ADMIN,
			'projects'       => PRIVILEGE_ADMIN,
			'project_delete' => PRIVILEGE_ADMIN,
			'project_get'    => PRIVILEGE_ADMIN,
			'project_supported' => PRIVILEGE_ADMIN,
			'project_set'    => PRIVILEGE_ADMIN,
		];

		/**
		 * Request a temporary bump to account storage
		 *
		 * @param string $site optional site to apply amnesty
		 * @return bool
		 * @see MIN_STORAGE_AMNESTY
		 */
		#[Route(
			as: 'amnesty',
			methods: ['post']
		)]
		public function amnesty(string $site = null): bool
		{
			if (posix_getuid() && !IS_CLI) {
				return $this->query('diskquota_amnesty');
			}

			$last = $this->getServiceValue('diskquota', 'amnesty');
			$now = coalesce($_SERVER['REQUEST_TIME'], time());
			if (!$site) {
				$site = $this->site;
			} else if ($site && !($this->permission_level && PRIVILEGE_SITE)) {
				if (!($site = \Auth::get_site_id_from_anything($site))) {
					return error("Unknown site identifier `%s'", $site);
				}

				$site = "site" . $site;
			}

			if (($this->permission_level & PRIVILEGE_SITE) && self::MIN_STORAGE_AMNESTY > ($now - $last)) {
				$aday = self::MIN_STORAGE_AMNESTY / 86400;

				return error('storage amnesty may be requested once every %(period)d days, %(remaining)d days remaining', [
					'period'    => $aday,
					'remaining' => $aday - ceil(($now - $last) / 86400)
				]);
			}

			// @TODO handling dynamic quota overages with xfs?
			$ctx = \Auth::context(null, $site);
			$storage = $ctx->getAccount()->conf['diskquota']['quota'];
			$newstorage = $storage * self::AMNESTY_MULTIPLIER;
			$acct = new Util_Account_Editor($ctx->getAccount(), $ctx);
			$acct->setConfig('diskquota', 'quota', $newstorage)->setConfig('diskquota', 'amnesty', $now);
			$ret = $acct->edit();
			if ($ret !== true) {
				Error_Reporter::report(var_export($ret, true));
				return error('failed to set amnesty on account');
			}
			$acct->setConfig('diskquota', 'quota', $storage);
			$cmd = $acct->getCommand();
			$proc = new Util_Process_Schedule('+' . self::AMNESTY_DURATION . ' seconds');
			if (($this->permission_level & PRIVILEGE_ADMIN)) {
				if ($id = $proc->preempted(self::AMNESTY_JOB_MARKER, $ctx)) {
					// @xxx potential unbounded storage growth
					$proc->cancelJob($id);
					// @todo report duplicate request
				}

			}
			$proc->setID(self::AMNESTY_JOB_MARKER, $ctx);
			$ret = $proc->run($cmd);

			// @todo extract to event
			$msg = sprintf("Domain: %s\r\nSite: %d\r\nServer: %s", $this->domain, $this->site_id, SERVER_NAME_SHORT);
			Mail::send(Crm_Module::COPY_ADMIN, 'Amnesty Request', $msg);

			return $ret['success'];
		}

		/**
		 * Account is under amnesty
		 *
		 * @return bool
		 */
		#[Route(
			as: 'amnesty',
			methods: ['get']
		)]
		public function amnesty_active(): bool
		{
			$time = $_SERVER['REQUEST_TIME'] ?? time();
			$amnesty = $this->getServiceValue('diskquota', 'amnesty');
			if (!$amnesty) {
				return false;
			}

			return ($time - $amnesty) <= self::AMNESTY_DURATION;
		}

		/**
		 * Project feature supported
		 *
		 * @return bool
		 */
		public function project_supported(): bool
		{
			return Project::supported();
		}

		/**
		 * Get project quota
		 *
		 * @param string $name
		 * @return array
		 */
		public function project_get(string $name): array
		{
			if (!IS_CLI) {
				return $this->query('diskquota_project_get', $name);
			}

			return (new Project($name))->get();
		}

		/**
		 * Set project quota
		 *
		 * @param string $name
		 * @param int $bhard hard limit in KB
		 * @param int $ihard hard limit in inodes
		 * @param int $bsoft soft limit in KB (unenforced)
		 * @param int $isoft soft limit in inodes (unenforced)
		 * @return bool
		 */
		public function project_set(string $name, int $bhard, int $ihard, int $bsoft = 0, int $isoft = 0): bool
		{
			if (!IS_CLI) {
				return $this->query('diskquota_project_set', $name, $bhard, $ihard, $bsoft, $isoft);
			}

			foreach (['b', 'i'] as $prefix) {
				foreach (['soft', 'hard'] as $type) {
					if (${"{$prefix}{$type}"} < 0) {
						return error("Parameter `%(name)s' value out of bounds", ['name' => "\${$prefix}{$type}"]);
					}
				}
			}

			if (str_starts_with($name, Opcenter\Reseller::RESELLER_PREFIX)) {
				return error(self::ERR_PROJECT_NAME_RESERVED);
			}

			return (new Project($name))->set($bhard, $ihard, $bsoft, $isoft);
		}

		/**
		 * List known projects
		 *
		 * @return array
		 */
		public function projects(): array
		{
			if (!file_exists(Project::PROJID_MAP)) {
				return [];
			}

			$map = new Opcenter\Map\Textfile(Project::PROJID_MAP, 'r', ':');
			return array_keys($map->fetchAll());
		}

		/**
		 * Delete project
		 *
		 * @param string $name
		 * @return bool
		 */
		public function project_delete(string $name): bool
		{
			if (!IS_CLI) {
				return $this->query('diskquota_project_delete', $name);
			}

			if (str_starts_with($name, Opcenter\Reseller::RESELLER_PREFIX)) {
				return error(self::ERR_PROJECT_NAME_RESERVED);
			}

			return (new Project($name))->remove();
		}

		/**
		 * Create new project
		 *
		 * @param string $name
		 * @return bool
		 * @throws ReflectionException
		 */
		public function project_create(string $name): bool
		{
			if (!IS_CLI) {
				return $this->query('diskquota_project_create', $name);
			}

			if (Project::exists($name)) {
				return error(Project::ERR_PROJECT_EXISTS, ['name' => $name]);
			}

			if (str_starts_with($name, Opcenter\Reseller::RESELLER_PREFIX)) {
				return error(self::ERR_PROJECT_NAME_RESERVED);
			}

			return (bool)(new Project($name))->create();
		}
	}