<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use Opcenter\Bandwidth\Site;

	/**
	 * Bandwidth statistics
	 *
	 * @package core
	 */
	class Bandwidth_Module extends Module_Skeleton implements \Module\Skeleton\Contracts\Hookable
	{
		const DEPENDENCY_MAP = [
			'siteinfo',
			'apache'
		];
		const BW_DIR = '/var/log/bw';

		protected $exportedFunctions = [
			'*' => PRIVILEGE_SITE,
			'amnesty' => PRIVILEGE_ADMIN
		];

		/**
		 * Get bandwidth consumed by period
		 *
		 * @param $grouping string group bandwidth by 'month' or 'day'
		 * @return array|false
		 */
		public function get_all_composite_bandwidth_data($grouping = 'month')
		{
			if (!\in_array($grouping, ['month', 'day'])) {
				return error("invalid bw grouping `%s'", $grouping);
			}

			$cachekey = 'bandwidth.gacbd.' . substr($grouping, 0, 2);

			$cache = Cache_Account::spawn($this->getAuthContext());
			$bw = $cache->get($cachekey);
			if ($bw !== false) {
				return $bw;
			}
			$bandwidth = (new Site($this->site_id))->getByComposite($grouping);
			$cache->set($cachekey, $bandwidth, 1800);

			return $bandwidth;
		}

		/**
		 *  Get bandwidth ranges
		 *  Indexes:
		 *      begin: start starting rollover date
		 *      end:   ending rollover date
		 *
		 * @return array
		 */
		public function get_cycle_periods(): array
		{

			$cachekey = 'bandwidth.gcp';
			$cache = Cache_Account::spawn($this->getAuthContext());
			$periods = $cache->get($cachekey);
			if ($periods !== false) {
				return $periods;
			}

			$periods = (new Site($this->site_id))->rollovers();

			$cache->set($cachekey, $periods, 43200);

			return $periods;
		}

		/**
		 * Retrieve day on which banwidth rolls over to 0
		 *
		 * @return int
		 */
		public function rollover(): int
		{
			$rollover = (int)$this->getServiceValue('bandwidth', 'rollover');
			$localtime = localtime(time(), true);
			$today = date('j');
			$month = ($rollover < $today ? ($localtime['tm_mon'] + 1) : $localtime['tm_mon']);

			return mktime(0, 0, 0, ++$month, $rollover);
		}

		/**
		 * Get cumulative bandwidth consumption
		 *
		 * @privilege PRIVILEGE_SITE
		 * @param int $type type of bandwidth usage to retrieve
		 * @return array|bool indexes begin, rollover, and threshold
		 */
		public function usage(int $type = null)
		{
			if (!$site_id = $this->site_id) {
				Error_Reporter::report(var_export($this, true));

				return false;
			}
			if (!$this->bandwidth_enabled()) {
				return ['used' => 0, 'total' => -1];
			}
			$pgdb = \PostgreSQL::initialize();
			switch ($type) {
				case null:
					$bw_rs = $pgdb->query('SELECT
						begindate,
						rollover,
						threshold as threshold
						FROM bandwidth_spans
						JOIN bandwidth USING (site_id)
						WHERE
						bandwidth_spans.site_id = ' . $site_id . '
						AND
						bandwidth_spans.enddate IS NULL
						AND
						bandwidth.site_id = ' . $site_id);
					if (!$bw_rs->num_rows()) {
						if ($this->enabled()) {
							// no span present, hotfix for next request
							// ideally, recurse but bandwidth record isn't set through here
							$this->_autofix_bandwidth($site_id, (int)$this->getServiceValue('bandwidth', 'rollover'));
						}
						return array('used' => 0, 'total' => -1);
					}
					$bw_rs = $bw_rs->fetch_object();
					if (!$bw_rs) {
						return [
							'used'  => 0,
							'total' => -1
						];
					}
					// @BUG account has no bandwidth enddate
					if ($bw_rs && !$bw_rs->begindate && $this->enabled()) {
						$ret = $this->_autofix_bandwidth($site_id,
							(int)$this->getServiceValue('bandwidth', 'rollover'));
						if (!$ret) {
							return error("failed to autofix bandwidth for site `%d'", $this->site_id);
						}

						return $this->usage($type);
					}

					$used_rs = $pgdb->query('SELECT
						SUM(in_bytes)+SUM(out_bytes) AS sum
						FROM
						bandwidth_log
						WHERE
						site_id = ' . $this->site_id . "
						AND
						ts >= '" . $bw_rs->begindate . "'");

					return array(
						'used'  => (float)$used_rs->fetch_object()?->sum,
						'total' => (float)($bw_rs ? $bw_rs->threshold : -1)
					);
					break;
				default:
					return error("Unknown bandwidth classifier `%d'", $type);
			}
		}

		/* }}} */


		/**
		 * Fill in missing spans for bandwidth
		 *
		 * @param int $site_id
		 * @param int $rollover
		 * @return bool
		 * @throws PostgreSQLError
		 */
		private function _autofix_bandwidth(int $site_id, int $rollover): bool
		{
			$db = \PostgreSQL::initialize();
			$ts = mktime(0, 0, 0, (int)date('m'), $rollover);
			$ts2 = strtotime('last month', $ts);
			if ($ts > time()) {
				$ts = $ts2;
			}
			$db->query("INSERT INTO bandwidth_spans (site_id, begindate, enddate) VALUES($site_id, TO_TIMESTAMP($ts), NULL)
				ON CONFLICT (site_id,begindate) DO NOTHING;");

			return !(bool)pg_last_error($db);
		}

		/**
		 * Get bandwidth consumed during a time interval
		 *
		 * @param int $begin beginning date
		 * @param int $end   ending date
		 * @return array|bool
		 */
		public function get_by_date(int $begin, int $end = null)
		{
			if (!$begin) {
				return error('no begin period set');
			} else if ($begin < 0 || $end < 0) {
				return error('Invalid begin or end date');
			} else if ($end && $begin > $end) {
				return error('Begin date may not be before end date');
			}
			// there may be collisions, but sacrifice for key len
			$cachekey = 'bandwidth.gbd.' . crc32($begin . $end);
			$cache = Cache_Account::spawn($this->getAuthContext());
			$services = $cache->get($cachekey);
			if ($services !== false) {
				return $services;
			}
			$services = (new Site($this->site_id))->getByRange($begin, $end);
			$cache->set($cachekey, $services, 43200);

			return $services;
		}

		public function enabled(): bool
		{
			return (bool)$this->getServiceValue('bandwidth', 'enabled');
		}

		/**
		 * Grant bandwidth amnesty until closing date
		 *
		 * @param string $marker
		 * @return bool
		 */
		public function amnesty(string $marker): bool
		{
			if (null === ($siteId = \Auth::get_site_id_from_anything($marker))) {
				return error("Unable to resolve site from `%s'", $marker);
			}
			$bwhandler = new Site($siteId);
			info(
				'Amnesty granted on site%d until %s',
				$siteId,
				date('r', $bwhandler->getCycleEnd())
			);
			return $bwhandler->amnesty();
		}

		public function _delete()
		{
			$glob = self::BW_DIR . '/*/' . $this->site . '{,.?}';
			foreach (glob($glob) as $f) {
				unlink($f);
			}
		}

		public function _edit()
		{
			$conf_new = $this->getAuthContext()->getAccount()->new;
			$conf_old = $this->getAuthContext()->getAccount()->old;
			$user = array(
				'old' => $conf_old['siteinfo']['admin_user'],
				'new' => $conf_new['siteinfo']['admin_user']
			);
			if ($user['old'] !== $user['new']) {
				(new Site($this->site_id))->renameExtendedInfo($user['old'], $user['new']);
			}
		}

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{
			if ($userold === $usernew) {
				return;
			}
			// update
			(new Site($this->site_id))->renameExtendedInfo($userold, $usernew);

			return true;
		}

		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			return true;
		}

		public function _create()
		{
			// TODO: Implement _create() method.
		}

		public function _create_user(string $user)
		{
			// TODO: Implement _create_user() method.
		}

		public function _delete_user(string $user)
		{
			// TODO: Implement _delete_user() method.
		}


	}