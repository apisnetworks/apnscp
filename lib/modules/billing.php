<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	/**
	 * Billing and referral module
	 *
	 * @package core
	 */
	class Billing_Module extends Module_Skeleton implements \Module\Skeleton\Contracts\Hookable
	{
		// @ignore
		private static $BILLING_SERVER_HOST = BILLING_HOST;
		private static $BILLING_SERVER_USER = BILLING_USER;
		private static $BILLING_SERVER_PASSWORD = BILLING_PASSWORD;
		private static $BILLING_SERVER_DATABASE = BILLING_DB;

		/**
		 * Recognized preferences in [change,get]_payout_preferences
		 * tender:   tender method
		 * freeze:   account frozen
		 * balance:  credit hosting balance
		 *
		 * @var array
		 */
		private static $_PAYOUT_OPTIONS = array('tender', 'freeze', 'balance');

		/**
		 * void __construct(void)
		 *
		 * @ignore
		 */
		public function __construct()
		{
			parent::__construct();

			$this->exportedFunctions = array(
				'*'                       => PRIVILEGE_SITE,
				'add_referral'            => PRIVILEGE_ADMIN,
				'get_invoice_from_domain' => PRIVILEGE_ADMIN,
				'get_package_type'        => PRIVILEGE_SITE|PRIVILEGE_USER,
				'get_package_by_invoice'  => PRIVILEGE_ADMIN,

				/** necessary for sanity checks */
				'get_invoice'             => PRIVILEGE_ALL
			);
		}

		/**
		 * Get current payment method
		 *
		 * @return null|string current payment method, enum: [credit, check, paypal, cash, other]
		 */
		public function get_payment_method(): ?string
		{
			return 'other';

		}

		/**
		 * Next payment date and amount for the account
		 *
		 * Array fields-
		 * date:   date of next payment for account.  In the event the
		 *         information cannot be found (reseller account for example),
		 *         -1 is returned.
		 * amount: amount for next payment
		 *
		 * @return array
		 */
		public function get_next_payment(): array
		{
			return [
				'date'   => -1,
				'amount' => 0
			];
		}

		/**
		 * array list_payments
		 *
		 * Sample response:
		 * Array
		 * (
		 *  [0] => Array
		 *  (
		 *      [date] => 1469937612
		 *      [amount] => 65.00
		 *      [service] => Webhosting->Developer
		 *      [number] => C-2ALQUJ67SSQXIDJEESZFRMZ
		 *      [note] =>
		 *      [type] => credit
		 *  )
		 * )
		 *
		 * @return array
		 */
		public function list_payments(): array
		{
			$recs = [];

			return $recs;

		}

		/**
		 * Fetch billing status from backend billing server
		 *
		 * @param string $invoice
		 * @return int
		 */
		public function get_billing_status(string $invoice = ''): int
		{
			return $this->get_status($invoice);
		}

		/**
		 * int get_standing_status()
		 *
		 * Fetch recurring subscription status from billing server
		 *
		 *  1  - subscription in good standing
		 * -1 - subscription cancelled, account within 90 days of expiring
		 * 0  - subscription cancelled, account outside 90 days of expiring
		 * null - cannot find subscription
		 *
		 * @param string $invoice
		 * @return int
		 */
		public function get_status(string $invoice = ''): ?int
		{
			return 1;
		}

		/**
		 * bool is_billed()
		 *
		 * @return bool billing record exists
		 */
		public function is_billed(): bool
		{
			return (bool)$this->get_hosting_subscription();
		}

		/**
		 * Currently active subscription number attached to invoice
		 *
		 * @return mixed|NULL
		 */
		public function get_hosting_subscription(): ?string
		{
			return $this->getConfig('billing', 'invoice');
		}

		/**
		 * Account was referred by another
		 *
		 * @param $invoice
		 * @return bool
		 */
		public function is_referred(string $invoice): bool
		{
			return false;
		}

		/**
		 * array get_payment_information(string)
		 *
		 * Sample response:
		 * Array
		 * (
		 *  [date] => 1469937612
		 *  [amount] => 65
		 *  [domain] => apnscp.com
		 *  [service_level] => Developer
		 *  [name] => Tark Sammons
		 *  [email] => tark.sammons@apnscp.com
		 *  [note] =>
		 *  [service_type] => Webhosting
		 *  [method] => recurring
		 *  [payment_method] => credit
		 *  [reference] => C-2ALQUJ67SSQXIDJEESZFRMZ
		 *  [txn_id] => 11111
		 *  [invoice] => APNSCP-CJKFHECZO35
		 *  [cc_type] => visa
		 *  [cc_number] => 1111
		 *  [cc_exp] => 03/20
		 * )
		 *
		 * @param string $transnum transaction number
		 * @param int    $date     unix timestamp of date of transaction
		 * @param string $type     payment method type
		 * @return array
		 */
		public function get_payment_information(string $transnum, int $date, string $type = 'paypal'): array
		{
			return [];
		}

		/**
		 * Update profile information
		 *
		 * @param array $credentials
		 * @return bool
		 */
		public function edit_referral_profile(array $credentials): bool {
			return error('not implemented');
		}

		/**
		 * Check if referral profile exists
		 *
		 * @return bool
		 */
		public function referral_profile_exists(): bool
		{
			return false;
		}

		/**
		 * Get referral username
		 *
		 * @return string|null
		 */
		public function get_referral_username(): ?string
		{
			return null;
		}

		/**
		 * Set minimum balance for a payout
		 *
		 * @param float $amount
		 * @return bool
		 */
		public function edit_payout_amount(float $amount): bool
		{
			return error('not implemented');
		}

		/**
		 * float get_payout_amount()
		 *
		 * @return float|null
		 */
		public function get_payout_amount(): ?float
		{
			return null;
		}

		/**
		 * float get_minimum_payout_amount
		 *
		 * @return float
		 */
		public function get_minimum_payout_amount(): float
		{
			return (float)self::PAYOUT_MINBAL;
		}

		/**
		 * Get current referral balance
		 *
		 * @return float
		 */
		public function referral_balance(): float
		{
			return 0;
		}

		/**
		 * Get multiplier used in commission credit
		 *
		 * @return float
		 */
		public function referral_multiplier(): float
		{
			return 1;
		}

		/**
		 * Minimum requirement for next referral level
		 * array(2) {
		 *    ["method"] => 254
		 *  ["client"] => 2
		 * }
		 *
		 * @return array
		 */
		public function referral_upgrade_needed(): array
		{
			$next = array('method' => 'client', 'next' => 2);

			return $next;
		}

		/**
		 * Get earned commissions
		 *
		 * Sample response
		 * array(1) {
		 * [0]=>
		 * array(5) {
		 * ["domain"]=>
		 * string(8) "apnscp.com"
		 * ["accrued"]=>
		 * string(5) "30.00"
		 * ["level"]=>
		 * string(5) "Basic"
		 * ["maturity"]=>
		 * int(1172293200)
		 * ["status"]=>
		 * NULL
		 * }
		 *
		 * domain  (string):
		 * accrued (float)
		 *
		 * @return array
		 *
		 */

		public function get_referrals(): array
		{
			return [];
		}

		/**
		 * array get_customer_referral_information()
		 *
		 * @return null|array
		 */
		public function get_customer_referral_information(): ?array
		{
			return null;
		}

		/**
		 * Create initial referral profile
		 *
		 * @param string $username
		 * @param array $personal credentials
		 *
		 * @return bool
		 */
		public function create_referral_profile(string $username, array $personal = []): bool {
			return error('not implemented');
		}

		/**
		 * Change referral payout options
		 *
		 * @param string $pref preference ['frozen', 'tender']
		 * @param mixed  $val  payment preference ['paypal', 'check']
		 * @return bool
		 */
		public function change_payout_preference(string $pref, string $val): bool
		{
			return error('not implemented');
		}

		/**
		 * Fetch payout option
		 *
		 * @param  string $pref preference
		 * @return string|null
		 */
		public function get_payout_preference(string $pref): ?string
		{
			return null;
		}

		/**
		 * Record a new referral
		 *
		 * @param string $invoice  new account invoice
		 * @param string $pinvoice parent invoice (referral profile)
		 * @return bool
		 */
		public function add_referral(string $invoice, string $pinvoice): bool
		{
			return error('not implemented');
		}

		/**
		 * Get referral credit by invoice
		 *
		 * @param string $invoice
		 * @return float|null
		 */
		public function get_credit_from_invoice(string $invoice): ?float
		{
			return 0;
		}

		/**
		 * Get referral credit by package type
		 *
		 * @param string $package package type
		 * @return float
		 */
		public function get_credit_from_package_type(string $package): float
		{
			return 1;
		}

		/**
		 * Get hosting invoice by domain
		 *
		 * @param string $domain domain name
		 * @return string|null
		 */
		public function get_invoice_from_domain(string $domain): ?string
		{
			return null;
		}

		/**
		 * Retrieve latest billing renewal hash for service
		 *
		 * @return string|null
		 */
		public function get_renewal_hash(string $invoice = null): ?string
		{
			return null;
		}

		/**
		 * Get billing renewal link
		 *
		 * Link must provide direct access to billing portal
		 *
		 * @param string|null $invoice leave blank for current outstanding invoice
		 * @return string
		 */
		public function get_renewal_link(string $invoice = null): ?string
		{
			return null;
		}

		/**
		 * Get customer since
		 *
		 * @return int unix timestamp or -1 for connectivity issues
		 */
		public function get_customer_since(): int
		{
			return (int)($this->getConfig('billing', 'ctime') ??
				filectime($this->domain_fs_path()));
		}

		/**
		 * Get billing information attached to account
		 *
		 * Array
		 * (
		 *  [first_name] => Tark
		 *  [last_name] => Sammons
		 *  [city] => Atlanta
		 *  [state] => Georgia
		 *  [zip_code] => 30308
		 *  [country] => US
		 *  [company] =>
		 *  [address] => 123 Anywhere St
		 *  [phone] => 867-5309
		 *  [email] => tark.sammons@apnscp.com
		 * )
		 *
		 * @return array
		 */
		public function get_billing_information(): array
		{
			return array();
		}

		/**
		 * Get billing details
		 *
		 * @return array|false
		 */
		public function get_credit_card_information()
		{
			return [];
		}

		/**
		 * Update billing details
		 *
		 * @param int $expyear
		 * @param int $expmonth
		 * @param string|null $cvm cvm number, with or without leading zeroes
		 * @param int|null $number
		 * @param string|null $type
		 * @return bool
		 */
		public function change_credit_card_information(
			int $expyear,
			int $expmonth,
			?string $cvm = null,
			?string $number = null,
			string $type = null
		): bool {
			return error('not implemented');
		}

		/**
		 * Update billing information on account
		 *
		 * @param string      $firstname
		 * @param string      $lastname
		 * @param string|null $company
		 * @param string      $address
		 * @param string      $city
		 * @param string      $state
		 * @param string      $zip
		 * @param string      $country
		 * @param string|null $phone
		 * @param string|null $email
		 * @return bool
		 */
		public function change_billing_information(array $details): bool {
			return error('not implemented');
		}

		/**
		 * Get current package
		 *
		 * @return string|null
		 */
		public function get_package_type(): ?string
		{
			$invoice = $this->get_invoice();

			return $this->get_package_by_invoice($invoice);
		}

		/**
		 * Get package name from invoice
		 *
		 * @param $invoice
		 * @return mixed|null
		 */
		public function get_package_by_invoice(string $invoice): ?string
		{
			if ($invoice === $this->get_invoice()) {
				return $this->getServiceValue('siteinfo', 'plan');
			}

			$ids = \Auth::get_site_id_from_invoice($invoice);
			if (null === $ids) {
				return null;
			}

			foreach ($ids as $siteid) {
				$ctx = \Auth::context(null, "site{$siteid}");
				if ($ctx->getAccount()->cur['billing']['invoice'] === $invoice) {
					return $ctx->getAccount()->cur['siteinfo']['plan'] ?? null;
				}
			}

			return null;
		}

		/**
		 * Claim referral from token
		 *
		 * @param  $token string 40 character hash
		 * @return bool
		 */
		public function claim_referral(string $token): bool
		{
			return error('not implemented');
		}

		/**
		 * Get data about a referral
		 * Sample return:
		 *  Array(
		 *      [domain] => apnscp.com
		 *      [revenue] => 20.00
		 *      [maturity] => 1469937612
		 *  )
		 *
		 * @param string $token
		 * @return array|bool
		 */
		public function claim_metadata(string $token)
		{
			return error('not implemented');
		}

		/**
		 * Used by TemplateEngine to confirm module is setup
		 *
		 * @return bool
		 */
		public function configured(): bool
		{
			return static::class !== self::class;
		}

		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			return true;
		}

		public function _create()
		{
			// TODO: Implement _create() method.
		}

		public function _delete()
		{
			// TODO: Implement _delete() method.
		}

		public function _edit()
		{
			// TODO: Implement _edit() method.
		}

		public function _create_user(string $user)
		{
			// TODO: Implement _create_user() method.
		}

		public function _delete_user(string $user)
		{
			// TODO: Implement _delete_user() method.
		}

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{
			// TODO: Implement _edit_user() method.
		}

		/**
		 * Get all invoices
		 *
		 * @return array
		 */
		private function get_all_invoices(): array
		{
			$invoice = (array)$this->get_invoice();
			if (!$invoice) {
				return [];
			}
			$addons = (array)$this->getConfig('billing', 'addons');
			if (!$addons) {
				return $invoice;
			}

			return array_merge($invoice, $addons);
		}

		/**
		 * Invariant invoice tied to an account
		 *
		 * @return null|string
		 */
		public function get_invoice(): ?string
		{
			if ($this->permission_level & (PRIVILEGE_ADMIN | PRIVILEGE_RESELLER)) {
				return null;
			}
			$invoice = (string)$this->getConfig('billing', 'invoice');
			if ($invoice) {
				return $invoice;
			}
			if ($this->getConfig('billing', 'parent_invoice')) {
				return (string)$this->getConfig('billing', 'parent_invoice');
			}

			return null;
		}


	}