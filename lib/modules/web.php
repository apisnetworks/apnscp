<?php
	declare(strict_types=1);

	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use Daphnie\Collector;
	use Daphnie\Metrics\Apache as ApacheMetrics;
	use Module\Skeleton\Contracts\Hookable;
	use Module\Skeleton\Contracts\Reactive;
	use Module\Skeleton\Contracts\Tasking;
	use Module\Support\Webapps\App\UIPanel;
	use Module\Support\Webapps\MetaManager;
	use Opcenter\Filesystem;
	use Opcenter\Http\Apache;
	use Opcenter\Http\Apache\Map;
	use Opcenter\Http\Apache\Maps\HSTS\Mode as HSTSMode;
	use Opcenter\Http\Apache\Maps\Tls as TlsMap;
	use Opcenter\Provisioning\ConfigurationWriter;

	/**
	 * Web server and package management
	 *
	 * @package core
	 */
	class Web_Module extends Module_Skeleton implements Hookable, Reactive, Tasking
	{
		const DEPENDENCY_MAP = [
			'ipinfo',
			'ipinfo6',
			'siteinfo',
			'dns',
			// required for PHP-FPM cgroup binding
			'cgroup'
		];

		// primary domain document root
		const MAIN_DOC_ROOT = '/var/www/html';
		const WEB_USERNAME = APACHE_USER;
		const WEB_GROUPID = APACHE_GID;
		const PROTOCOL_MAP = '/etc/httpd/conf/http10';
		const SUBDOMAIN_ROOT = '/var/subdomain';

		protected $pathCache = [];
		protected $service_cache;
		protected $exportedFunctions = [
			'*'                 => PRIVILEGE_SITE,
			'add_subdomain_raw' => PRIVILEGE_SITE | PRIVILEGE_SERVER_EXEC,
			'host_html_dir'     => PRIVILEGE_SITE | PRIVILEGE_USER,
			'reload'            => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'status'            => PRIVILEGE_ADMIN,
			'get_sys_user'      => PRIVILEGE_ALL,
			'capture'           => PRIVILEGE_SERVER_EXEC | PRIVILEGE_SITE,
			'inventory_capture' => PRIVILEGE_ADMIN
		];
		protected $hostCache = [];

		public const ERR_UNKNOWN_DOMAIN = [':web_domain_unknown', 'Unknown domain `%s\''];
		/**
		 * void __construct(void)
		 *
		 * @ignore
		 */
		public function __construct()
		{
			parent::__construct();

			if (!DAV_APACHE) {
				$this->exportedFunctions += [
					'bind_dav' => PRIVILEGE_NONE,
					'unbind_dav' => PRIVILEGE_NONE,
					'list_dav_locations' => PRIVILEGE_NONE,
				];
			}

		}

		public function __wakeup()
		{
			$this->pathCache = [];
		}

		/**
		 * User capability is enabled for web service
		 *
		 * Possible values subdomain, cgi
		 *
		 * @param  string $user user
		 * @param  string $svc  service name possible values subdomain, cgi
		 * @return bool
		 */
		public function user_service_enabled(string $user, string $svc): bool
		{
			if (!IS_CLI) {
				return $this->query('web_user_service_enabled',
					array($user, $svc));
			}
			if ($svc != 'cgi' && $svc != 'subdomain') {
				return error('Invalid service name `%s\'', $svc);
			}

			return true;
		}

		/**
		 * Sweep all subdomains to confirm accessibility
		 *
		 * @return array list of subdomains invalid
		 */
		public function validate_subdomains(): array
		{
			$prefix = $this->domain_fs_path();
			$invalid = array();
			foreach (glob($prefix . self::SUBDOMAIN_ROOT . '/*/') as $entry) {
				$subdomain = basename($entry);
				if ((is_link($entry . '/html') || is_dir($entry . '/html')) && file_exists($entry . '/html')) {
					continue;
				}
				warn("inaccessible subdomain `%s' detected", $subdomain);
				$file = Opcenter\Filesystem::rel2abs($entry . '/html',
					readlink($entry . '/html'));
				$invalid[$subdomain] = substr($file, strlen($prefix));
			}

			return $invalid;
		}

		/**
		 * Check if hostname is a subdomain
		 *
		 * @param string $hostname
		 * @return bool
		 */
		public function is_subdomain(string $hostname): bool
		{
			if (false !== strpos($hostname, '.') && !preg_match(Regex::SUBDOMAIN, $hostname)) {
				return false;
			}

			return is_dir($this->domain_fs_path(self::SUBDOMAIN_ROOT . "/$hostname"));
		}

		public function subdomain_accessible($subdomain)
		{
			if ($subdomain[0] == '*') {
				$subdomain = substr($subdomain, 2);
			}

			return file_exists($this->domain_fs_path(self::SUBDOMAIN_ROOT . "/$subdomain/html")) &&
				is_executable($this->domain_fs_path(self::SUBDOMAIN_ROOT . "/$subdomain/html"));
		}

		/**
		 * Get assigned web user for host/docroot
		 *
		 * @see get_sys_user() for HTTPD user
		 *
		 * @param string $hostname hostname or path
		 * @param string $path
		 * @return string
		 */
		public function get_user(string $hostname, string $path = ''): string
		{
			// @TODO gross oversimplification of path check,
			// assert path is readable and if not default to apache,webuser
			if ($hostname[0] === '/' && $path) {
				warn('$path variable should be omitted when specifying docroot');
			}
			return $this->getServiceValue('apache', 'webuser', static::WEB_USERNAME);
		}

		/**
		 * Get HTTP system user
		 *
		 * HTTP user has limited requirements except read.
		 * In a PHP-FPM environment, no write access is permitted by this user.
		 *
		 * @return string
		 */
		public function get_sys_user(): string
		{
			return static::WEB_USERNAME;
		}

		/**
		 * Retrieve document root for given host
		 *
		 * Doubly useful to evaluate where documents
		 * will be served given a particular domain
		 *
		 * @param string $hostname HTTP host
		 * @param string $path     optional path component
		 * @return string document root path
		 */
		public function normalize_path(string $hostname, string $path = ''): ?string
		{
			if (!IS_CLI && isset($this->pathCache[$hostname][$path])) {
				return $this->pathCache[$hostname][$path];
			}
			$prefix = $this->domain_fs_path();
			if (false === ($docroot = $this->get_docroot($hostname, $path))) {
				return null;
			}

			$checkpath = $prefix . DIRECTORY_SEPARATOR . $docroot;
			clearstatcache(true, $checkpath);
			if (\Util_PHP::is_link($checkpath)) {
				// take the referent unless the path doesn't exist
				// let the API figure out what to do with it
				if (false === ($checkpath = realpath($checkpath))) {
					return $docroot;
				}
				if (0 !== strpos($checkpath, $prefix)) {
					error("docroot for `%s/%s' exceeds site root", $hostname, $path);

					return null;
				}
				$docroot = substr($checkpath, strlen($prefix));
			}
			if (!file_exists($checkpath)) {
				$subpath = dirname($checkpath);
				if (!file_exists($subpath)) {
					error("invalid domain `%s', docroot `%s' does not exist", $hostname, $docroot);

					return null;
				}
			}
			if (!isset($this->pathCache[$hostname])) {
				$this->pathCache[$hostname] = [];
			}

			$this->pathCache[$hostname][$path] = $docroot;

			return $docroot ?: null;
		}

		/**
		 * Get document root from hostname
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool|string
		 */
		public function get_docroot(string $hostname, string $path = '')
		{
			$domains = $this->list_domains();
			$path = ltrim($path, '/');
			if (isset($domains[$hostname])) {
				return rtrim($domains[$hostname] . '/' . $path, '/');
			}

			$domains = $this->list_subdomains();
			if (array_key_exists($hostname, $domains)) {
				// missing symlink will report as NULL
				if (null !== $domains[$hostname]) {
					return rtrim($domains[$hostname] . '/' . $path, '/');
				}
				$info = $this->subdomain_info($hostname);

				return rtrim($info['path'] . '/' . $path, '/');
			}

			if (0 === strncmp($hostname, "www.", 4)) {
				$tmp = substr($hostname, 4);

				return $this->get_docroot($tmp, $path);
			}
			if (false !== strpos($hostname, '.')) {
				$host = $this->split_host($hostname);
				if (!empty($host['subdomain']) && $this->subdomain_exists($host['subdomain'])) {
					return $this->get_docroot($host['subdomain'], $path);
				}

			}

			return error(self::ERR_UNKNOWN_DOMAIN, $hostname);
		}

		/**
		 * Import subdomains from domain
		 *
		 * @param string $target target domain to import into
		 * @param string $src    source domain
		 * @return bool
		 */
		public function import_subdomains_from_domain(string $target, string $src): bool
		{
			$domains = $this->web_list_domains();
			foreach ([$target, $src] as $chk) {
				if (!isset($domains[$chk])) {
					return error(self::ERR_UNKNOWN_DOMAIN, $chk);
				}
			}
			if ($target === $src) {
				return error('Cannot import - target is same as source');
			}
			foreach ($this->list_subdomains('local', $target) as $subdomain => $path) {
				$this->remove_subdomain($subdomain);
			}
			foreach ($this->list_subdomains('local', $src) as $subdomain => $path) {
				if ($src !== substr($subdomain, -\strlen($src))) {
					warn("Subdomain attached to `%s' does not match target domain `%s'??? Skipping", $subdomain, $target);
					continue;
				}
				$subdomain = substr($subdomain, 0, -\strlen($src)) . $target;
				$this->add_subdomain($subdomain, $path);
			}

			return true;
		}

		/**
		 * List subdomains on the account
		 *
		 * Array format- subdomain => path
		 *
		 * @param string       $filter  filter by "global", "local", "path"
		 * @param string|array $domains only show subdomains bound to domain or re for path
		 * @return array|false matching subdomains
		 */
		public function list_subdomains(string $filter = '', $domains = array())
		{
			if ($filter && $filter != 'local' && $filter != 'global' && $filter != 'path') {
				return error("invalid filter mode `%s'", $filter);
			}
			$subdomains = array();
			if ($filter == 'path') {
				$re = $domains;
				if ($re && $re[0] !== $re[-1]) {
					$re = '!' . preg_quote($re, '!') . '!';
				}
			} else {
				$re = null;
			}
			if ($domains && !is_array($domains)) {
				$domains = array($domains);
			}
			foreach (glob($this->domain_fs_path() . self::SUBDOMAIN_ROOT . '/*', GLOB_NOSORT) as $entry) {
				$subdomain = basename($entry);
				$path = '';
				if (is_link($entry . '/html') || is_dir($entry . '/html') /* smh... */) {
					if (!is_link($entry . '/html')) {
						warn(':subdomain_is_dir', "subdomain `%s' doc root is directory", $subdomain);
						$path = Opcenter\Http\Apache::makeSubdomainPath($subdomain);
					} else {
						$path = (string)substr(Opcenter\Filesystem::rel2abs($entry . '/html',
							readlink($entry . '/html')),
							strlen($this->domain_fs_path()));
					}
				}
				if ($filter && ($filter == 'local' && !strpos($subdomain, '.') ||
						$filter == 'global' && strpos($subdomain, '.'))
				) {
					continue;
				}
				if ($filter == 'path' && !preg_match($re, $path)) {
					continue;
				}

				if ($filter !== 'path' && strpos($subdomain, '.') && $domains) {
					$skip = 0;
					foreach ($domains as $domain) {
						$lendomain = strlen($domain);
						if (substr($subdomain, -$lendomain) != $domain) {
							$skip = 1;
							break;
						}
					}
					if ($skip) {
						continue;
					}
				}

				$subdomains[$subdomain] = $path;
			}

			asort($subdomains, SORT_LOCALE_STRING);

			return $subdomains;
		}

		/**
		 * Get detailed information on a subdomain
		 *
		 * Response:
		 *  path (string): filesystem location
		 *  active (bool): subdomain references accessible directory
		 *  user (string): owner of subdomain
		 *  type (string): local, global, or fallthrough
		 *
		 * @param  string $subdomain
		 * @return array
		 */
		public function subdomain_info(string $subdomain): array
		{
			if ($subdomain[0] == '*') {
				$subdomain = substr($subdomain, 2);
			}

			if (!$subdomain) {
				return error('no subdomain provided');
			}
			if (!$this->subdomain_exists($subdomain)) {
				return error($subdomain . ': subdomain does not exist');
			}

			$info = array(
				'path'   => null,
				'active' => false,
				'user'   => null,
				'type'   => null
			);

			$fs_location = $this->domain_fs_path() . self::SUBDOMAIN_ROOT . "/$subdomain";

			if (!strpos($subdomain, '.')) {
				$type = 'global';
			} else if (!array_key_exists($subdomain, $this->list_domains())) {
				$type = 'local';
			} else {
				$type = 'fallthrough';
			}

			$info['type'] = $type;
			$link = $fs_location . '/html';
			/**
			 * link does not exist
			 * test first if no symlink referent is present,
			 * then verify (is_link()) that the $link is not present
			 * file_exists() checks the referent
			 */
			if (!file_exists($link) && !is_link($link)) {
				return $info;
			}
			// case when <subdomain>/html is directory instead of symlink
			if (!is_link($link)) {
				$path = $link;
			} else {
				clearstatcache(true, $link);
				$path = Opcenter\Filesystem::rel2abs($link, readlink($link));
			}
			$info['path'] = $this->file_canonicalize_site($path);

			$info['active'] = file_exists($link) && is_readable($link);
			$stat = $this->file_stat($info['path']);
			if (!$stat) {
				return $info;
			}
			$info['user'] = $stat['owner'];

			return $info;
		}

		/**
		 * Check if named subdomain exists
		 *
		 * Fallthrough, local, and global subdomain patterns
		 * are valid
		 *
		 * @see add_subdomain()
		 *
		 * @param  string $subdomain
		 * @return bool
		 */
		public function subdomain_exists(string $subdomain): bool
		{
			if ($subdomain[0] === '*') {
				$subdomain = substr($subdomain, 2);
			}
			$path = $this->domain_fs_path(self::SUBDOMAIN_ROOT . "/$subdomain");

			return file_exists($path);
		}

		public function list_domains(): array
		{
			$domains = array_merge(
				array($this->getConfig('siteinfo', 'domain') => self::MAIN_DOC_ROOT),
				$this->aliases_list_shared_domains()
			);

			return $domains + array_fill_keys(
				array_keys(array_diff_key(array_flip($this->aliases_list_aliases()), $domains)),
				self::MAIN_DOC_ROOT
			);

		}

		/**
		 * Split hostname into subdomain + domain components
		 *
		 * @param string $hostname
		 * @return array|bool components or false on error
		 */
		public function split_host(string $host)
		{
			if (!preg_match(Regex::HTTP_HOST, $host)) {
				return error([':err_split_host_invalid', "can't split, invalid host `%s'"], $host);
			}
			$split = array(
				'subdomain' => '',
				'domain'    => $host
			);
			$domain_lookup = $this->list_domains();
			if (!$host || isset($domain_lookup[$host])) {
				return $split;
			}

			$offset = 0;
			$level_sep = strpos($host, '.');
			do {
				$subdomain = substr($host, $offset, $level_sep - $offset);
				$domain = substr($host, $level_sep + 1);
				if (isset($domain_lookup[$domain])) {
					break;
				}

				$offset = $level_sep + 1;
				$level_sep = strpos($host, '.', $offset + 1);
			} while ($level_sep !== false);

			if (!isset($domain_lookup[$domain])) {
				return $split;
			}
			$split['subdomain'] = (string)substr($host, 0, $offset) . $subdomain;
			$split['domain'] = $domain;

			return $split;
		}

		/**
		 * Get the normalized hostname from a global subdomain
		 *
		 * @param string $host
		 * @return string
		 */
		public function normalize_hostname(string $host): string
		{
			if (false !== strpos($host, '.')) {
				return $host;
			}

			// @todo track domain/entry_domain better in contexted roles
			return $host . '.' .
				($this->inContext() ? $this->domain : \Session::get('entry_domain', $this->domain));
		}

		// {{{ remove_user_subdomain()

		/**
		 * Get information on a domain
		 *
		 * Info elements
		 *    path (string): filesystem path
		 *  active (bool): domain is active and readable
		 *  user (string): owner of directory
		 *
		 * @param  string $domain
		 * @return array|false domain information
		 */
		public function domain_info(string $domain)
		{
			if (!$this->domain_exists($domain)) {
				return error($domain . ': domain does not exist');
			}

			$path = self::MAIN_DOC_ROOT;
			$info = array(
				'path'   => $path,
				'active' => false,
				'user'   => null
			);

			if ($domain !== $this->getConfig('siteinfo', 'domain')) {
				$domains = $this->aliases_list_shared_domains();
				// domain attached directly via aliases,aliases service value, not present in domain_map
				$path = $domains[$domain] ?? self::MAIN_DOC_ROOT;
			}
			$info['path'] = $path;
			$info['active'] = is_readable($this->domain_fs_path() . $path);

			$stat = $this->file_stat($path);
			if (!$stat) {
				return $stat;
			}
			$info['user'] = $stat['owner'];

			return $info;
		}

		/**
		 * Test if domain is attached to account
		 *
		 * aliases:list-aliases is used to check site configuration for fixed aliases
		 * aliases:list-shared-domains checks presence in info/domain_map
		 *
		 * @see aliases_domain_exists() to check against domain_map
		 *
		 * @param  string $domain
		 * @return bool
		 */
		public function domain_exists(string $domain): bool
		{
			return $domain == $this->getConfig('siteinfo', 'domain') ||
				in_array($domain, $this->aliases_list_aliases(), true);

		}

		// }}}

		/**
		 * Get hostname from location
		 *
		 * @param string $docroot document root as seen by server (does not resolve symlinks!)
		 * @return string|null
		 */
		public function get_hostname_from_docroot(string $docroot): ?string
		{
			$docroot = rtrim($docroot, '/');
			if ($docroot === static::MAIN_DOC_ROOT) {
				return $this->getServiceValue('siteinfo', 'domain');
			}
			$aliases = $this->aliases_list_shared_domains();
			if (false !== ($domain = array_search($docroot, $aliases, true))) {
				return $domain;
			}

			if ($subdomain = $this->list_subdomains('path', $docroot)) {
				return (string)key($subdomain);
			}

			return null;
		}

		/**
		 * Given a docroot, find all hostnames that serve from here
		 *
		 * @xxx expensive lookup
		 *
		 * @param string $docroot
		 * @return array
		 */
		public function get_all_hostnames_from_path(string $docroot): array
		{
			$hosts = [];
			if ($docroot === static::MAIN_DOC_ROOT) {
				$hosts[] = $this->getServiceValue('siteinfo', 'domain');
			}
			foreach ($this->aliases_list_shared_domains() as $domain => $path) {
				if ($docroot === $path) {
					$hosts[] = $domain;
				}
			}

			return array_merge($hosts, array_keys($this->list_subdomains('path', '!' . preg_quote($docroot, '!') . '$!')));
		}

		/**
		 * Decompose a path into its hostname/path components
		 *
		 * @param string $docroot
		 * @return null|array
		 */
		public function extract_components_from_path(string $docroot): ?array
		{
			$path = [];
			do {
				if (null !== ($hostname = $this->get_hostname_from_docroot($docroot))) {
					return [
						'hostname' => $hostname,
						'path' => implode('/', $path)
					];
				}
				array_unshift($path, \basename($docroot));
				$docroot = \dirname($docroot);
			} while ($docroot !== '/');

			return null;
		}

		/**
		 * Assign a path as a DAV-aware location
		 *
		 * @param string $location filesystem location
		 * @param string $provider DAV provider
		 * @return bool
		 */
		public function bind_dav(string $location, string $provider): bool
		{
			if (!IS_CLI) {
				return $this->query('web_bind_dav', $location, $provider);
			}

			if (!$this->verco_svn_enabled() && (strtolower($provider) == 'svn')) {
				return error('Cannot use Subversion provider when not enabled');
			} else if (!\in_array($provider, ['on', 'dav', 'svn'])) {
				return error("Unknown dav provider `%s'", $provider);
			}
			if ($provider === 'dav') {
				$provider = 'on';
			}
			if ($location[0] != '/') {
				return error("DAV location `%s' is not absolute", $location);
			}
			if (!file_exists($this->domain_fs_path() . $location)) {
				return error('DAV location `%s\' does not exist', $location);
			}

			$stat = $this->file_stat($location);
			if (!$stat) {
				return false;
			}

			if ($stat['file_type'] != 'dir') {
				return error("bind_dav: `$location' is not directory");
			} else if (!$stat['can_write']) {
				return error("`%s': cannot write to directory", $location);
			}

			$this->query('file_fix_apache_perms_backend', $location);
			$file = $this->site_config_dir() . '/dav';

			$locations = $this->parse_dav($file);
			if (null !== ($chk = $locations[$location] ?? null) && $chk === $provider) {
				return warn("DAV already enabled for `%s'", $location);
			}
			$locations[$location] = $provider;

			return $this->write_dav($file, $locations);
		}

		/**
		 * Parse DAV configuration
		 *
		 * @param string $path
		 * @return array
		 */
		private function parse_dav(string $path): array
		{
			$locations = [];
			if (!file_exists($path)) {
				return [];
			}
			$dav_config = trim(file_get_contents($path));

			if (preg_match_all(\Regex::DAV_CONFIG, $dav_config, $matches, PREG_SET_ORDER)) {
				foreach ($matches as $match) {
					$cfgpath = $this->file_unmake_path($match['path']);
					$locations[$cfgpath] = $match['provider'];
				}
			}
			return $locations;
		}

		/**
		 * Convert DAV to text representation
		 *
		 * @param string $path
		 * @param array  $cfg
		 * @return bool
		 */
		private function write_dav(string $path, array $cfg): bool
		{
			if (!$cfg) {
				if (file_exists($path)) {
					unlink($path);
				}
				return true;
			}
			$template = (new \Opcenter\Provisioning\ConfigurationWriter('apache.dav-provider',
				\Opcenter\SiteConfiguration::shallow($this->getAuthContext())))
				->compile([
					'prefix'    => $this->domain_fs_path(),
					'locations' => $cfg
				]);
			return file_put_contents($path, $template) !== false;
		}

		public function site_config_dir(): string
		{
			return Apache::siteStoragePath($this->site);
		}

		/**
		 * Permit a disallowed protocol access to hostname
		 *
		 * @param string $hostname
		 * @param string $proto only http10 is valid
		 * @return bool
		 */
		public function allow_protocol(string $hostname, string $proto = 'http10'): bool
		{
			if (!IS_CLI) {
				return $this->query('web_allow_protocol', $hostname, $proto);
			}
			if ($proto !== 'http10') {
				return error("protocol `%s' not known, only http10 accepted", $proto);
			}
			if (!$this->protocol_disallowed($hostname, $proto)) {
				return true;
			}
			if (!$this->split_host($hostname)) {
				// unowned domain/subdomain
				return error("Invalid hostname `%s'", $hostname);
			}
			$map = Map::open(self::PROTOCOL_MAP, Map::MODE_WRITE);
			$map[$hostname] = $this->site_id;

			return $map->sync();
		}

		/**
		 * Specified protocol is disallowed
		 *
		 * @param string $hostname
		 * @param string $proto
		 * @return bool
		 */
		public function protocol_disallowed(string $hostname, string $proto = 'http10'): bool
		{
			if ($proto !== 'http10') {
				return error("protocol `%s' not known, only http10 accepted", $proto);
			}
			$map = Map::open(self::PROTOCOL_MAP);

			return !isset($map[$hostname]);
		}

		/**
		 * Disallow protocol
		 *
		 * @param string $hostname
		 * @param string $proto
		 * @return bool
		 */
		public function disallow_protocol(string $hostname, string $proto = 'http10'): bool
		{
			if (!IS_CLI) {
				return $this->query('web_disallow_protocol', $hostname, $proto);
			}
			if ($proto !== 'http10') {
				return error("protocol `%s' not known, only http10 accepted", $proto);
			}

			if (!$this->get_docroot($hostname)) {
				return false;
			}

			if ($this->protocol_disallowed($hostname, $proto)) {
				return true;
			}
			$map = Map::open(self::PROTOCOL_MAP, Map::MODE_WRITE);
			if ((int)($map[$hostname] ?? -1) !== $this->site_id) {
				return warn("Site `%s' not found in map", $hostname);
			}

			unset($map[$hostname]);

			return $map->sync();

		}

		public function unbind_dav(string $location): bool
		{
			if (!IS_CLI) {
				return $this->query('web_unbind_dav', $location);
			}
			$file = $this->site_config_dir() . '/dav';
			$locations = $this->parse_dav($file);
			if (!isset($locations[$location])) {
				return warn("DAV not enabled for `%s'", $location);
			}
			unset($locations[$location]);

			return $this->write_dav($file, $locations);

		}

		public function list_dav_locations(): array
		{
			$file = $this->site_config_dir() . '/dav';
			$locations = [];
			foreach ($this->parse_dav($file) as $path => $type) {
				$locations[] = [
					'path' => $path,
					'provider' => $type === 'on' ? 'dav' : $type
				];
			}
			return $locations;
		}

		public function _edit()
		{
			$conf_new = $this->getAuthContext()->getAccount()->new;
			$conf_old = $this->getAuthContext()->getAccount()->old;
			// change to web config or ipconfig
			$ssl = \Opcenter\SiteConfiguration::getModuleRemap('openssl');
			if ($conf_new['apache'] != $conf_old['apache'] ||
				$conf_new['ipinfo'] != $conf_old['ipinfo'] ||
				$conf_new[$ssl] != $conf_old[$ssl] ||
				$conf_new['aliases'] != $conf_old['aliases']
			) {
				Apache::activate();
			}

		}

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{
			if ($userold === $usernew) {
				return;
			}
			/**
			 * @TODO
			 * Assert that all users are stored under /home/username
			 * edit_user hook is called after user is changed, so
			 * this is lost without passing user pwd along
			 */
			$userhome = $this->user_get_user_home($usernew);
			$re = '!^' . $oldpwd['home'] . '!';
			mute_warn();
			$subdomains = $this->list_subdomains('path', $re);
			unmute_warn();
			foreach ($subdomains as $subdomain => $path) {
				$newpath = preg_replace('!' . DIRECTORY_SEPARATOR . $userold . '!',
					DIRECTORY_SEPARATOR . $usernew, $path, 1);
				if ($subdomain === $userold) {
					$newsubdomain = $usernew;
				} else {
					$newsubdomain = $subdomain;
				}
				if ($this->rename_subdomain($subdomain, $newsubdomain, $newpath)) {
					info("moved subdomain `%s' from `%s' to `%s'", $subdomain, $path, $newpath);
				}
			}

			return true;
		}

		/**
		 * Rename a subdomain and/or change its path
		 *
		 * @param string $subdomain    source subdomain
		 * @param string $newsubdomain new subdomain
		 * @param string $newpath
		 * @return bool
		 */
		public function rename_subdomain(string $subdomain, string $newsubdomain = null, string $newpath = null): bool
		{
			if (!$this->subdomain_exists($subdomain)) {
				$parts = array_values($this->split_host($subdomain));
				if ($parts[0] && $this->subdomain_exists($parts[0])) {
					// global subdomain
					warn("Requested subdomain %(local)s is global subdomain %(global)s. Retrying %(fn)s.", [
						'local' => $subdomain,
						'global' => $parts[0],
						'fn' => 'web_' . __FUNCTION__
					]);
					return $this->rename_subdomain($parts[0], $newsubdomain, $newpath);
				}
				return error('%s: subdomain does not exist', $subdomain);
			}
			if ($newsubdomain && $subdomain !== $newsubdomain && $this->subdomain_exists($newsubdomain)) {
				return error("destination subdomain `%s' already exists", $newsubdomain);
			}
			if (!$newsubdomain && !$newpath) {
				return error('no rename operation specified');
			}
			if ($newpath && ($newpath[0] != '/' && $newpath[0] != '.')) {
				return error("invalid path `%s', subdomain path must " .
					'be relative or absolute', $newpath);
			}

			if (!$newsubdomain) {
				$newsubdomain = $subdomain;
			} else {
				$newsubdomain = strtolower($newsubdomain);
			}

			unset($this->hostCache[$subdomain], $this->hostCache[$newsubdomain]);
			$sdpath = Opcenter\Http\Apache::makeSubdomainPath($subdomain);
			$stat = $this->file_stat($sdpath);
			$old_path = $stat['link'] ? $stat['referent'] : $sdpath;

			if (!$newpath) {
				$newpath = $old_path;
			}
			if (!$newsubdomain) {
				$newsubdomain = $subdomain;
			}

			// add_subdomain() creates index.html placeholder
			if (!$this->file_exists($newpath . '/index.html')) {
				defer($_, fn() => $this->file_delete($newpath . '/index.html'));
			}

			if ($subdomain !== $newsubdomain) {
				if (!$this->remove_subdomain($subdomain) || !$this->add_subdomain($newsubdomain, $newpath)) {
					error("changing subdomain `%s' to `%s' failed", $subdomain, $newsubdomain);
					if (!$this->add_subdomain($subdomain, $old_path)) {
						error("critical: could not reassign subdomain `%(sub)s' to `%(path)s' after failed rename", [
							'sub' => $subdomain,
							'path' => $old_path
						]);
					}

					return false;
				}
			} else if (!$this->remove_subdomain($subdomain, true) || !$this->add_subdomain($subdomain, $newpath)) {
				error("failed to change path for `%s' from `%s' to `%s'",
					$subdomain,
					$old_path,
					$newpath);
				if (!$this->add_subdomain($subdomain, $old_path)) {
					error("failed to restore subdomain `%s' to old path `%s'",
						$subdomain,
						$old_path);
				}

				return false;
			}

			$this->file_purge(false);

			if ($subdomain !== $newsubdomain) {
				MetaManager::instantiateContexted($this->getAuthContext())
					->merge($newpath, ['hostname' => $newsubdomain])->sync();
			}
			return true;
		}

		/**
		 * Remove a subdomain
		 *
		 * @param string $subdomain fully or non-qualified subdomain
		 * @param bool   $keepdns   preserve DNS records for subdomain
		 * @return bool
		 */
		public function remove_subdomain(string $subdomain, bool $keepdns = false): bool
		{
			// clear both ends
			$this->purge();
			if (!IS_CLI) {
				// remove Web App first
				$docroot = $this->get_docroot($subdomain);
				if (false && $docroot) {
					// @TODO rename_subdomain calls remove + add, which would break renaming meta
					// this is how it should be done, but can't implement *yet*
					$mm = MetaManager::factory($this->getAuthContext());
					$app = \Module\Support\Webapps\App\Loader::fromDocroot(
						array_get($mm->get($docroot), 'type', 'unknown'),
						$docroot,
						$this->getAuthContext()
					);
					$app->uninstall();
				}

				if (!$this->query('web_remove_subdomain', $subdomain)) {
					return false;
				}

				if (false && $docroot) {
					$mm->forget($docroot)->sync();
				}
				return true;
			}

			$subdomain = strtolower((string)$subdomain);

			if (!preg_match(Regex::SUBDOMAIN, $subdomain) &&
				0 !== strncmp($subdomain, '*.', 2) &&
				!preg_match(Regex::DOMAIN, preg_replace('/^\*\./', '', $subdomain))
			)
			{
				return error('%s: invalid subdomain', $subdomain);
			}
			if ($subdomain[0] === '*') {
				$subdomain = substr($subdomain, 2);
			}
			if (!$this->subdomain_exists($subdomain)) {
				return warn('%s: subdomain does not exist', $subdomain);
			}

			$this->map_subdomain('delete', $subdomain);
			$path = $this->domain_fs_path() . self::SUBDOMAIN_ROOT . "/$subdomain";
			if (is_link($path)) {
				return unlink($path) && warn("subdomain `%s' path `%s' corrupted, removing reference",
						$subdomain,
						$this->file_unmake_path($path)
					);
			}

			$dh = opendir($path);
			while (false !== ($entry = readdir($dh))) {
				if ($entry === '..' || $entry === '.') {
					continue;
				}
				if (!is_link($path . '/' . $entry) && is_dir($path . '/' . $entry)) {
					warn("directory found in subdomain `%s'", $entry);
					Filesystem::rmdir($path . '/' . $entry);
				} else {
					unlink($path . '/' . $entry);
				}
			}
			closedir($dh);
			rmdir($path);

			if (!$this->dns_configured() || $keepdns) {
				return true;
			}
			$hostcomponents = [
				'subdomain' => $subdomain,
				'domain' => ''
			];
			if (false !== strpos($subdomain, '.')) {
				$hostcomponents = $this->split_host($subdomain);
			}
			if (!$hostcomponents['subdomain']) {
				return true;
			}
			if (!$hostcomponents['domain']) {
				$hostcomponents['domain'] = array_keys($this->list_domains());
			}
			$ret = true;

			$ips = [];
			if ($tmp = $this->dns_get_public_ip()) {
				$ips = (array)$tmp;
			}
			if ($tmp = $this->dns_get_public_ip6()) {
				$ips = array_merge($ips, (array)$tmp);
			}

			$components = [''];
			if (DNS_SUBDOMAIN_IMPLICIT_WWW) {
				$components[] = 'www';
			}
			foreach ((array)$hostcomponents['domain'] as $domain) {
				foreach ($components as $component) {
					$subdomain = ltrim("{$component}." . $hostcomponents['subdomain'], '.');
					foreach ($ips as $ip) {
						$rr = false === strpos($ip, ':') ? 'A' : 'AAAA';
						if ($this->dns_record_exists($domain, $subdomain, $rr, $ip)) {
							$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
							try {
								$ret &= $this->dns_remove_record($domain, $subdomain, $rr, $ip);
							} catch (\apnscpException $e) {
								warn($e->getMessage());
							} finally {
								\Error_Reporter::exception_upgrade($oldex);
							}
						}
					}
				}
			}

			return (bool)$ret;
		}

		/**
		 * Clear path cache
		 *
		 * @return void
		 */
		public function purge(): void
		{
			$this->pathCache = [];
			$this->hostCache = [];
			if (!IS_CLI) {
				$this->query('web_purge');
			}
		}

		/**
		 * Manage subdomain symlink mapping
		 *
		 * @todo   merge from Web_Module::map_domain()
		 * @param  string $mode      add/delete
		 * @param  string $subdomain subdomain to add/remove
		 * @param  string $path      domain path
		 * @param  string $user      user to assign mapping
		 * @return bool
		 */
		public function map_subdomain(string $mode, string $subdomain, string $path = null, string $user = null): bool
		{
			if (!IS_CLI) {
				return $this->query('web_map_subdomain',
					$mode,
					$subdomain,
					$path,
					$user);
			}

			$mode = substr($mode, 0, 3);
			if (!preg_match(Regex::SUBDOMAIN, $subdomain) &&
				!preg_match(Regex::DOMAIN, $subdomain))
			{
				return error($subdomain . ': invalid subdomain');
			}
			if ($mode != 'add' && $mode != 'del') {
				return error($mode . ': invalid mapping operation');
			}
			if ($mode == 'del') {
				$docroot = $this->get_docroot($subdomain);
				if ($docroot) {
					MetaManager::factory($this->getAuthContext())->forget($docroot)->sync();
				}

				return $this->file_delete('/home/*/all_subdomains/' . $subdomain);
			}
			if ($mode == 'add') {
				if (!$user) {
					$stat = $this->file_stat($path);
					if (!$stat) {
						return error("Cannot map subdomain - failed to determine user from `%s'", $path);
					}

					$user = $stat['uid'] >= \User_Module::MIN_UID ? $this->user_get_username_from_uid($stat['uid']) : $this->username;

				}
				$user_home = '/home/' . $user;
				$user_home_abs = $this->domain_fs_path() . $user_home;

				if (!file_exists($this->domain_fs_path() . $path)) {
					warn($path . ': path does not exist, creating link');
				}
				if (!file_exists($user_home_abs . '/all_subdomains')) {
					$this->file_create_directory($user_home . '/all_subdomains');
					$this->file_chown($user_home . '/all_subdomains', $user);
				}
				$this->file_symlink($path, $user_home . '/all_subdomains/' . $subdomain) &&
					$this->file_chown_symlink($user_home . '/all_subdomains/' . $subdomain, $user);
			}

			return true;
		}

		/**
		 * Add subdomain to account
		 *
		 * There are 3 types of subdomains:
		 * Local- subdomain includes subdomain + domain - foo.bar.com
		 * Fallthrough- subdomain is named after domain - bar.com
		 * Global- subdomain excludes domain - foo
		 *
		 * @param  string $subdomain
		 * @param  string $docroot document root of the subdomain
		 * @return bool
		 */
		public function add_subdomain(string $subdomain, string $docroot): bool
		{
			if (!IS_CLI) {
				return $this->query('web_add_subdomain', $subdomain, $docroot);
			}

			$subdomain = strtolower(trim((string)$subdomain));
			if ($subdomain === 'www') {
				return error('illegal subdomain name');
			}
			$subdomain = preg_replace('/^www\./', '', strtolower($subdomain));
			if (!$subdomain) {
				return error('Missing subdomain');
			}

			if (!preg_match(Regex::SUBDOMAIN, $subdomain) &&
				0 !== strncmp($subdomain, '*.', 2) &&
				!preg_match(Regex::DOMAIN, preg_replace('/^\*\./', '', $subdomain))
			) {
				return error($subdomain . ': invalid subdomain');
			}
			if ($this->subdomain_exists($subdomain)) {
				return error($subdomain . ': subdomain exists');
			} else if ($subdomain === gethostname()) {
				warn("Subdomain duplicates system hostname `%s'. Supplied document root will " .
					'never have precedence over system document root.', $subdomain);
			}

			$components = filter(fn() => $this->split_host(
				preg_replace('/^\*\./', '', $subdomain)
			), ':err_split_host_invalid');

			if (str_contains($subdomain, '.') &&
				(false === $components || !$this->domain_exists($components['domain'])))
			{
				return error("Global subdomain subdomains are unsupported");
			}

			if ($docroot[0] != '/' && $docroot[0] != '.') {
				return error("invalid path `%s', subdomain path must " .
					'be relative or absolute', $docroot);
			}
			/**
			 * This is particularly nasty because add_subdomain can provide
			 * either the subdomain or the subdomain + domain as the $subdomain
			 * argument.  We need to (1) loop through each domain to determine if
			 * a FQDN or subdomain, (2) query each DNS record to ensure
			 * it is provisioned correctly, (3) add missing records.
			 *
			 * A FQDN for a hostname on the other hand is  a bit easier; just
			 * add the record.  First we check to see if it's FQDN or not.  If
			 * FQDN, check DNS and add.
			 */
			$domains = array_keys($this->list_domains());
			if ($subdomain[0] === '*') {
				$subdomain = substr($subdomain, 2);
				$domain = '';
				if (!in_array($subdomain, $domains, true)) {
					return error("domain `%s' not attached to account (DNS > Addon Domains)", $domain);
				}
			}
			if ( null !== ($limit = $this->getConfig('apache', 'subnum', null) ) && ($limit <= count($this->list_subdomains())) ) {
				return error('Subdomain limit %d has been reached - cannot add %s', $limit, $subdomain);
			}
			// is it a fully-qualified domain name? i.e. www.apisnetworks.com or
			// a subdomain? e.g. "www"
			$FQDN = false;

			// hostnames to query and setup DNS records for
			$recs_to_add = array();
			foreach ($domains as $domain) {
				if (preg_match('/\.' . $domain . '$/', $subdomain)) {
					// local subdomain
					$FQDN = true;
					$recs_to_add = array(
						array(
							'subdomain' => substr($subdomain, 0, -strlen($domain) - 1),
							'domain'    => $domain
						)
					);
					break;
				} else if ($subdomain === $domain) {
					// subdomain is fallthrough
					$recs_to_add[] = array(
						'subdomain' => '*',
						'domain'    => $domain
					);

				}
			}
			if (!$recs_to_add) {
				// domain is global subdomain
				foreach ($domains as $domain) {
					$recs_to_add[] = array(
						'subdomain' => $subdomain,
						'domain'    => $domain
					);
				}
			}

			$ips = [];
			if ($tmp = $this->dns_get_public_ip()) {
				$ips = (array)$tmp;
			}
			if ($tmp = $this->dns_get_public_ip6()) {
				$ips = array_merge($ips, (array)$tmp);
			}

			foreach ($recs_to_add as $record) {
				foreach ($ips as $ip) {
					$rr = false === strpos($ip, ':') ? 'A' : 'AAAA';
					$this->dns_add_record_conditionally($record['domain'], $record['subdomain'], $rr, $ip);
					if ($record['subdomain'] !== '*') {
						$this->dns_add_record_conditionally(
							$record['domain'],
							'www.' . $record['subdomain'],
							$rr,
							$ip
						);
					}
				}
			}

			/**
			 * Home directories without subdomains explicitly enabled
			 * are created with 700.  This bears the side-effect of Apache
			 * being unable to descend past /home/<user>/.  Fix by giving
			 * the execute bit
			 */
			$realpath = $this->file_unmake_path(realpath($this->file_make_path($docroot)));

			if (preg_match('!^/home/([^/]+)!', $realpath, $user_home)) {
				$user = $user_home[1];
				$stat = $this->file_stat('/home/' . $user);
				if (!$stat) {
					return error("user `%s' does not exist", $user);
				}
				// give Apache access
				if (!$this->file_chmod("/home/{$user}", decoct($stat['permissions']) | 001)) {
					return false;
				}

				if ($this->php_jailed()) {
					// and FPM, DACs will match group rather than world
					$this->file_set_acls("/home/{$user}", $this->get_user($subdomain,''), 'x');
				}

			} else {
				$user = $this->getServiceValue('siteinfo', 'admin_user');
			}

			$prefix = $this->domain_fs_path();
			if (!$this->file_exists($docroot) || empty($this->file_get_directory_contents($docroot))) {
				if (\Util_PHP::is_link($prefix . $docroot)) {
					// fix cases where a client links the doc root to an absolute symlink outside the scope
					// of apache, e.g. /var/www/html -> /foo, apache would see <fst>/foo, not /foo
					$newlink = Opcenter\Filesystem::abs2rel($docroot, readlink($prefix . $docroot));
					warn('converted unfollowable absolute symlink to relative (document root): %s -> %s', $docroot,
						$newlink);
					unlink($prefix . $docroot);
					$ret = $this->file_symlink($newlink, $docroot);
				} else {
					$ret = $this->file_create_directory($docroot, 0755, true);
				}

				if (!$ret) {
					return $ret;
				}
				$this->file_chown($docroot, $user);
				$index = $prefix . $docroot . '/index.html';
				$template = (string)(new ConfigurationWriter('apache.placeholder',
					\Opcenter\SiteConfiguration::shallow($this->getAuthContext())))->compile([
						'hostname' => $subdomain,
						'docroot' => $docroot,
						'user' => $user
				]);
				file_put_contents($index, $template) &&
					Filesystem::chogp($index, (int)$this->user_get_uid_from_username($user), $this->group_id, 0644);
			}
			$subdomainpath = Opcenter\Http\Apache::makeSubdomainPath($subdomain);

			defer($_, function () use($subdomain) {
				UIPanel::instantiateContexted($this->getAuthContext())->freshen($subdomain, "",
					(bool)SCREENSHOTS_ENABLED);
			});

			return $this->add_subdomain_raw($subdomain,
					Opcenter\Filesystem::abs2rel($subdomainpath, $docroot)) &&
				$this->map_subdomain('add', $subdomain, $docroot);
		}

		public function add_subdomain_raw(string $subdomain, string $docroot): bool
		{
			$prefix = $this->domain_fs_path();
			$subdomain_path = Opcenter\Http\Apache::makeSubdomainPath($subdomain);
			$subdomain_parent = dirname($prefix . $subdomain_path);
			if (!file_exists($subdomain_parent)) {
				\Opcenter\Filesystem::mkdir($subdomain_parent, $this->user_id, $this->group_id);
			}
			$tmp = $docroot;
			if ($docroot[0] === '.' && $docroot[1] == '.') {
				$tmp = $subdomain_parent . DIRECTORY_SEPARATOR . $docroot;
			}
			clearstatcache(true, $tmp);
			$user = fileowner($tmp);
			if (!file_exists($tmp)) {
				Error_Reporter::print_debug_bt();
			}

			return symlink($docroot, $prefix . $subdomain_path) &&
				Util_PHP::lchown($prefix . $subdomain_path, $user) &&
				Util_PHP::lchgrp($prefix . $subdomain_path, $this->group_id);
		}

		/**
		 * Get Apache service status
		 *
		 * @return array
		 */
		public function status(): array
		{
			return Apache::getReportedServiceStatus();
		}

		/**
		 * Account created
		 */
		public function _create()
		{
			Apache::activate();
		}

		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			return true;
		}

		public function _reload(string $why = '', array $args = [])
		{
			if (in_array($why, ['', 'php', 'aliases', Ssl_Module::SYS_RHOOK, Ssl_Module::USER_RHOOK], true)) {
				return Apache::activate();
			}
		}

		public function _housekeeping() {
			// kill chromedriver if persisting between startups
			$class = new ReflectionClass(\Service\CaptureDevices\Chromedriver::class);
			$instance = $class->newInstanceWithoutConstructor();
			if ($instance->running()) {
				$instance->stop(true);
			}
		}

		public function _cron(Cronus $c) {
			if (SCREENSHOTS_ENABLED && !APNSCPD_HEADLESS) {
				$c->schedule(60 * 60, 'screenshots', static function () {
					// need Laravel 6+ for closure serialization support to Horizon
					$n = (int)sprintf('%u', SCREENSHOTS_BATCH);
					$job = (new \Service\BulkCapture(new \Service\CaptureDevices\Chromedriver));
					$job->batch($n);

				});
			}


			if (TELEMETRY_ENABLED) {
				$collector = new Collector(PostgreSQL::pdo());

				if ( !($status = $this->status()) ) {
					// service down, zero fill
					// gap filling via TSDB would lend itself to false positives
					// if the cron interval ever changes
					$status = array_fill_keys(array_values(ApacheMetrics::ATTRVAL_MAP), 0);
				}

				foreach (ApacheMetrics::ATTRVAL_MAP as $attr => $metric) {
					$collector->add("apache-{$attr}", null, (int)$status[$metric]);
				}
			}
		}

		public function _delete()
		{

		}

		public function http_config_dir(): string
		{
			deprecated_func('use site_config_dir');

			return $this->site_config_dir();
		}

		public function config_dir(): string
		{
			return Apache::CONFIG_PATH;
		}

		public function _delete_user(string $user)
		{
			$this->remove_user_subdomain($user);
		}

		/**
		 * Removes all subdomains associated with a named user
		 *
		 * @param string $user
		 * @return bool
		 */
		public function remove_user_subdomain(string $user): bool
		{
			$ret = true;
			$home = $this->user_get_user_home($user) . '/';
			foreach ($this->list_subdomains() as $subdomain => $dir) {
				if (!str_starts_with($dir, $home)) {
					continue;
				}
				$ret &= $this->remove_subdomain($subdomain);
			}

			return (bool)$ret;
		}

		/**
		 * Enable SSL/TLS redirection on hostname
		 *
		 * @param string       $hostname
		 * @param int|HSTSMode $redirection_mode value between 0 and 4
		 * @return bool
		 */
		public function set_ssl(string $hostname, int|HSTSMode $redirection_mode = HSTSMode::disabled): bool
		{
			if (!IS_CLI) {
				return $this->query('web_set_ssl', $hostname, $redirection_mode);
			}

			if (!($parts = filter(fn() => $this->split_host($hostname), ':err_split_host_invalid')) ||
				!$this->domain_exists($parts['domain']))
			{
				return error("Invalid hostname");
			}

			if (is_int($redirection_mode)) {
				try {
					$redirection_mode = HSTSMode::from($redirection_mode);
				} catch (ValueError) {
					return error("Invalid mode");
				}
			}

			if (!empty($parts['subdomain']) && $redirection_mode > HSTSMode::enabled) {
				return error("HSTS subdomain settings may not contain a subdomain");
			}

			$map = TlsMap::open(TlsMap::DOMAIN_MAP, TlsMap::MODE_WRITE);
			$map["{$this->site_id}:{$hostname}"] = $redirection_mode->value;
			$map->sync();

			return true;
		}

		/**
		 * Remove SSL/TLS redirection on hostname
		 */
		public function remove_ssl(string $hostname): bool
		{
			if (!IS_CLI) {
				return $this->query('web_remove_ssl', $hostname);
			}

			if (!filter(fn() => $this->split_host($hostname), ':err_split_host_invalid')) {
				return error("Invalid hostname");
			}

			$key = "{$this->site_id}:{$hostname}";

			$map = TlsMap::open(TlsMap::DOMAIN_MAP, TlsMap::MODE_WRITE);
			if (!$map->offsetExists($key)) {
				return warn("Hostname %(host)s does not exist", ['host' => $hostname]);
			}
			unset($map[$key]);

			return $map->sync();
		}

		/**
		 * List SSL-TLS upgrade hostnames
		 *
		 * @return array<string $hostname, int $mode>
		 */
		public function list_ssl(): array
		{
			$hosts = [];
			$map = TlsMap::open(TlsMap::DOMAIN_MAP, TlsMap::MODE_READ);
			$prefix = "{$this->site_id}:";
			foreach ($map as $key => $mode) {
				if (str_starts_with($key, $prefix)) {
					$hosts[substr($key, strlen($prefix))] = (int)$mode;
				}
			}

			return $hosts;
		}

		/**
		 * SSL redirection mode for hostname
		 *
		 * @param string $hostname
		 * @return int|null
		 */
		public function ssl_mode(string $hostname): ?int
		{
			if (!preg_match(Regex::HTTP_HOST, $hostname)) {
				return nerror("Invalid hostname");
			}

			$map = TlsMap::open(TlsMap::DOMAIN_MAP, TlsMap::MODE_READ);
			$key = "{$this->site_id}:{$hostname}";
			return $map[$key] ?? null;
		}

		/**
		 * Bulk screenshot of all sites
		 *
		 * @param array|null $sites
		 * @return void
		 */
		public function inventory_capture(array $sites = null): void {
			if (!$sites) {
				$sites = \Opcenter\Account\Enumerate::sites();
			}
			$driver = new \Service\BulkCapture(new \Service\CaptureDevices\Chromedriver);
			foreach ($sites as $site) {
				$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL|\Error_Reporter::E_ERROR);
				try {
					$ctx = \Auth::context(null, $site);
					$afi = \apnscpFunctionInterceptor::factory($ctx);
				} catch (\apnscpException $e) {
					continue;
				} finally {
					\Error_Reporter::exception_upgrade($oldex);
				}

				if (!posix_getuid()) {
					// hold ref to allow suspension at garbage collection
					$serviceRef = new \Opcenter\Http\Php\Fpm\StateRestore($ctx->site);
				}

				foreach ((new \Module\Support\Webapps\Finder($ctx))->getAllApplicationRoots() as $meta) {
					if (empty($meta['hostname'])) {
						continue;
					}

					debug('%(site)s: Capturing %(url)s (IP: %(ip)s)', [
						'site' => $ctx->site,
						'url' => rtrim(implode('/', [$meta['hostname'], $meta['path']]), '/'),
						'ip' => $afi->site_ip_address()
					]);
					$driver->snap($meta['hostname'], $meta['path'], $afi->site_ip_address());
				}

			}
		}

		/**
		 * Capture screenshot of site
		 *
		 * @XXX Restricted from backend.
		 *
		 * @param string                    $hostname
		 * @param string                    $path
		 * @param \Service\BulkCapture|null $service optional BulkCapture service
		 * @return bool
		 */
		public function capture(string $hostname, string $path = '', \Service\BulkCapture $service = null): bool
		{
			if (APNSCPD_HEADLESS) {
				return warn('Panel in headless mode');
			}

			$hostname = strtolower($hostname);
			if (!$this->normalize_path($hostname, $path)) {
				return error("Site `%s/%s' is not hosted on account", $hostname, $path);
			}
			if (!$service) {
				$service = new \Service\BulkCapture(new \Service\CaptureDevices\Chromedriver);
			}
			return $service->snap($hostname, $path, $this->site_ip_address());
		}

		public function _create_user(string $user)
		{
			// TODO: Implement _create_user() method.
		}
	}