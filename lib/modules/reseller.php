<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2018
	 */


	/**
	 * Class Reseller_Module
	 *
	 * Hierarchical account management
	 *
	 * Reserved for future use.
	 */
	class Reseller_Module extends Admin_Module
	{
		public $exportedFunctions = [
			'*' => PRIVILEGE_RESELLER,
			'read_map'         => PRIVILEGE_NONE,
			'activate_license' => PRIVILEGE_NONE,
			'renew_license'    => PRIVILEGE_NONE,
			'create_from_meta' => PRIVILEGE_NONE,
		];

		public function __construct()
		{
			parent::__construct();
		}

		protected function scopeRestrictor(): string
		{
			return 'JOIN resellers ON (reseller_id) WHERE reseller_id = ' .
				\Opcenter\Reseller::id($this->getAuthContext()->rgroup);
		}
		/**
		 * Reseller support enabled for server
		 *
		 * @return bool
		 */
		public function enabled(): bool
		{
			return \Opcenter\Reseller::enabled();
		}

		public function collect(?array $params = [], array $query = null, array $sites = []): array
		{
			$params['reseller.id'] = $this->getAuthContext()->rgroup;

			return parent::collect($params, $query, $sites);
		}

		public function _cron(Cronus $c)
		{
			return;
		}

		public function _housekeeping()
		{
			return;
		}
	}