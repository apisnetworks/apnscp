<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */


namespace Template;

abstract class Searchlet {
	abstract public function specs(): array;

	public function autofocus(): bool
	{
		return true;
	}

	public function operations(): array {
		return [
			'contains'    => 'contains',
			'notcontains' => 'does not contain',
			'is'          => 'is',
			'isnot'       => 'is not'
		];
	}

	public function formAction(): ?string
	{
		return \HTML_Kit::page_url();
	}

	public function filterClass(): string
	{
		return '';
	}

	public function filterLabel(): string
	{
		return 'Search';
	}

	public function resetFilterLabel(): string
	{
		return 'Reset Filters';
	}

	public function submitId(): string
	{
		return 'do_filter';
	}

	public function submitName(): string
	{
		return 'do_filter';
	}

	public function submitType(): string
	{
		return 'submit';
	}

	public function searchText(): string
	{
		return (string)\HTML_Kit::get_value('searchtext');
	}
}