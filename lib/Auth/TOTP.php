<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, September 2024
 */


namespace Auth;

use chillerlan\Authenticator\Authenticator;
use chillerlan\Authenticator\AuthenticatorOptions;
use chillerlan\Authenticator\Common\Base32;

class TOTP {
	use \PreferencesTrait;

	private const TOTP_ATTR = 'trusted.cp-totp';
	// ~162 bits
	private const SECRET_BITS = AUTH_TOTP_BITS;

	public const TOTP_LENGTH = 6;
	public const ERR_TOTP_INVALID = [
		':err_auth_totp_failed',
		'Failed to validate TOTP'
	];

	private function __construct() { }

	private function instantiateAuthenticator(?string $secret): Authenticator
	{
		$options = new AuthenticatorOptions([
			'adjacent'      => (int)AUTH_TOTP_ADJACENCY,
			'period'        => (int)AUTH_TOTP_VALIDITY_PERIOD,
			'secret_length' => $this->convertBitByteMask(self::SECRET_BITS)
		]);

		return new Authenticator($options, $secret ?? $this->read());
	}

	/**
	 * TOTP enabled for account
	 *
	 * @return bool
	 */
	public function exists(): bool
	{
		$ret = $this->exec('get');
		return $ret['return'] === 0 && !empty($ret['stdout']);
	}

	public function disable(): bool
	{
		$ret = $this->exec('set');
		return $ret['success'] ?: error([':err_auth_totp_disable', 'Failed to disable TOTP: %(err)s'], ['err' => $ret['stderr']]);
	}

	/**
	 * Create new TOTP secret
	 *
	 * @return string
	 */
	public function generate(): string
	{
		$options = new AuthenticatorOptions([
			'secret_length' => $this->convertBitByteMask(self::SECRET_BITS)
		]);

		return (new Authenticator($options))->createSecret();
	}

	/**
	 * Generate TOTP code from secret
	 *
	 * @param string $secret
	 * @return string|null
	 */
	public function code(string $secret): ?string
	{
		try {
			return $this->instantiateAuthenticator($secret)->code();
		} catch (\RuntimeException) {
			return null;
		}
	}

	/**
	 * Verify code matches secret
	 *
	 * @param string      $code
	 * @param string|null $secret
	 * @return bool
	 */
	public function verify(string $code, string $secret = null): bool
	{
		$code = preg_replace('/[^\d]/', '', $code);
		try {
			return $this->instantiateAuthenticator($secret)->verify($code);
		} catch (\RuntimeException) {
			return false;
		}
	}

	public function uri(string $secret): string
	{
		return $this->instantiateAuthenticator($secret)->getUri(
			rtrim($this->getAuthContext()->username . '@' . $this->getAuthContext()->domain, '@'),
			PANEL_BRAND
		);
	}

	public function set(string $secret): bool
	{
		if ($this->exists()) {
			return error([':err_auth_totp_exists', "TOTP secret already exists"]);
		}

		if ($this->convertBitByteMask(self::SECRET_BITS) !== strlen(Base32::decode($secret))) {
			return error([':err_auth_totp_secret_invalid', "Invalid secret"]);
		}

		$ret = $this->exec('set', $secret);
		return $ret['success'] ?: error([':err_auth_totp_set_failed', "Failed to set TOTP: %(err)s", ['err' => $ret['stderr']]]);
	}

	private function convertBitByteMask(int $bits): int
	{
		return (int)ceil($bits/8);
	}

	private function read(): ?string
	{
		$ret = $this->exec('get');
		return $ret['return'] === 0 ? $ret['stdout'] : null;
	}

	private function exec(string $mode, string $val = null): array
	{
		if ($mode !== 'get' && $mode !== 'set') {
			fatal("Unknown mode %s", $mode);
		}
		$binary = $mode === 'get' ? '/usr/bin/getfattr' : '/usr/bin/setfattr';

		return \Util_Process::exec([$binary, '-%s', self::TOTP_ATTR, '%s', '%s', '%s', '%s', '%s'], [
			$mode === 'set' && $val === null ? 'x' : 'n',
			$mode === 'get' ? '--absolute-names' : null,
			$mode === 'get' ? '--only-values' : null,
			$mode === 'set' && $val ? '-v' : null,
			$mode === 'set' ? $val : null,
			$this->preferencesPath()
		], [0, 1]);
	}
}