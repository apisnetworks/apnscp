<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Auth_DAV extends Auth
	{
		const DB_SYMBOL = 'dav';
		const TV_SEC = 1800;
		const AUTO_AUTH = false;
		private static $valid = false;

		/**
		 * Verify session is active
		 */
		public function session_valid()
		{
			// force authentication
			if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
				[$user, $domain] = self::_parse_login($_SERVER['PHP_AUTH_USER']);

				// do a cache fetch routine...
				return self::_check_auth_ttl($user, $domain) &&
					$this->_resume_session($user, $domain);
			}
			if (parent::session_valid() && $_SESSION[self::VALIDITY_MARKER] === \Auth::client_ip()) {
				return true;
			}
			\Auth_Anvil::anvil();

			return false;
		}

		private static function _check_auth_ttl($user, $domain)
		{
			$data = apcu_fetch('dav_ttl:' . $domain . ':' . $user);
			if (!$data) {
				return false;
			}
			$data = unserialize($data);

			return self::client_ip() == $data['ip'] &&
				(($_SERVER['REQUEST_TIME'] - $data['ttl']) < 600 ||
					self::_update_auth_ttl($user, $domain, $data)) &&
				self::_stat_check($data['id'], $data['stat']);
		}

		private static function _update_auth_ttl($user, $domain, $data)
		{
			$data['ttl'] = $_SERVER['REQUEST_TIME'];
			apcu_store('dav_ttl:' . $domain . ':' . $user, serialize($data), 3600);

			return true;
		}

		private static function _stat_check($site_id, $ttl = null)
		{
			if ($site_id) {
				$file = FILESYSTEM_VIRTBASE . '/site' .
					$site_id . '/fst/etc/shadow';
			} else {
				$file = '/etc/shadow';
			}
			if (!$ttl) {
				return filemtime($file);
			}

			return $ttl == filemtime($file);
		}

		private static function _resume_session($user, $domain)
		{
			$db = \apnscpSession::db();
			$q = $db->query("SELECT session_id FROM " . apnscpSession::TABLE . " WHERE username = " .
				$db->quote($user) . " " .
				" AND domain = " . $db->quote($domain) .
				" AND last_action >= NOW() - INTERVAL '" . self::TV_SEC . " SECONDS'" .
				" AND login_method = '" . self::DB_SYMBOL . "'");
			if ($q->rowCount() < 1) {
				\Auth_Anvil::anvil();

				return false;
			}
			$session_id = $q->fetchObject()->session_id;

			return apnscpSession::restore_from_id($session_id);
		}

		public function authenticate()
		{
			if (self::$valid) {
				return true;
			}

			if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
				return false;
			} else if (!DAV_ENABLED) {
				// controlled via config.ini
				return false;
			}
			$password = $_SERVER['PHP_AUTH_PW'];
			list($username, $domain) = self::_parse_login($_SERVER['PHP_AUTH_USER']);

			$lock = '/tmp/d' . md5($username . $domain);

			// auth in progress
			$i = 0;
			while (file_exists($lock)) {
				usleep(100);
				$i++;
				if ($i > 1000) {
					if ($_SERVER['REQUEST_TIME'] - filemtime($lock) > 300) {
						break;
					}

					return false;
				}
			}
			touch($lock);
			Auth_Anvil::anvil();
			$success = $this->initializeUser($username, $domain, $password);
			if (file_exists($lock)) {
				unlink($lock);
			}
			if ($success) {
				self::_save_auth_ttl($username, $domain);
				Auth_Anvil::remove();
			}
			$_SESSION[self::VALIDITY_MARKER] = \Auth::client_ip();

			return $success;
			/** unauthenticated, try */
		}

		private static function _save_auth_ttl($user, $domain)
		{
			$site_id = (string)self::get_site_id_from_domain($domain);

			$data = array(
				'id'   => $site_id,
				'stat' => self::_stat_check($site_id),
				'ip'   => self::client_ip(),
				'ttl'  => $_SERVER['REQUEST_TIME']
			);
			self::_update_auth_ttl($user, $domain, $data);
		}

		public function unauthorized()
		{
			Auth_Anvil::anvil();

			// handled through HTTP_WebDav_Server::ServeRequest()
			return false;
		}

		public function verify($username, $password, $domain)
		{
			return parent::verify($username, $password, $domain) &&
				DataStream::get()->setOption(apnscpObject::RESET)->query('auth_user_permitted', $username, 'dav');
		}
	}