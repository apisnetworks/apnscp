<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * @property string $site
	 * @property string $rgroup
	 */
	class Auth_Info_User
	{
		use FilesystemPathTrait {
			domain_fs_path     as private real_domain_fs_path;
			domain_info_path   as private real_domain_info_path;
			domain_shadow_path as private real_domain_shadow_path;
		}

		public $username;
		public $domain;
		// @var string site
		protected $site;
		public int $level = 0;
		public ?int $site_id = 0;
		public $group_id;
		public $user_id;
		public $id;
		// @var reseller group membership
		protected ?string $rgroup = null;

		public ?int $hotworker = null;

		// @var string langauge
		public string $language = LOCALE;
		// @var string timezone
		public $timezone;

		private bool $volatile = false;

		/**
		 * @var \Auth_Info_Account
		 */
		protected ?\Auth_Info_Account $conf;

		private function __construct()
		{}

		public function __sleep()
		{
			$keys = get_object_vars($this);
			unset($keys['conf']);
			return array_keys($keys);
		}

		public function __wakeup()
		{
			$this->importAccount();
		}

		public function __clone() {
			$this->volatile = true;
			$this->id = \apnscpSession::init()->create_sid();
			\Auth_Info_User::initializeUser($this->username, $this->domain, null, $this->id);
		}

		public function __destruct() {
			\apnscpFunctionInterceptor::expire($this->id);
		}

		public function __get($name)
		{
			if ($name === 'site') {
				return $this->site;
			}

			if ($name === 'rgroup') {
				return $this->rgroup;
			}

			fatal("Unknown user property `%s'", $name);
		}

		public function __set($name, $value)
		{
			if ($name === 'site') {
				$this->site = $value;
			} else if ($name === 'rgroup') {
				$this->rgroup = $value;
			}

			fatal("Unknown user property `%s'", $name);
		}

		public function __isset($name)
		{
			if ($name === 'site') {
				return isset($this->site);
			}

			if ($name === 'rgroup') {
				return isset($this->rgroup);
			}

			return false;
		}

		public function domain_fs_path(string $subpath = null): string
		{
			return $this->real_domain_fs_path($subpath);
		}

		public function domain_info_path(string $subpath = null): string
		{
			return $this->real_domain_info_path($subpath);
		}

		public function domain_shadow_path(string $subpath = null): string
		{
			return $this->real_domain_shadow_path($subpath);
		}

		public function mock(array $params): self {
			$self = clone $this;
			$metadata = clone $this->getAccount();
			$rfxn = new ReflectionProperty($self, 'conf');
			$rfxn->setAccessible(true);
			$metadata->cur = array_replace_recursive($metadata->cur, $params);
			$self->conf = $metadata;
			return $self;
		}

		public function getImpersonator(): ?string
		{
			return \Session::get('auth.impersonator');
		}

		public function setImpersonator(string $id): void
		{
			if (!\apnscpSession::init()->exists($id)) {
				fatal("Impersonator `%s' doesn't exist", $id);
			}
			// User object can be regenerated thus losing the impersonator ID
			\Session::set('auth.impersonator', $id);
		}

		/**
		 * Initialize a new user
		 *
		 * @param string      $user
		 * @param null|string $domain
		 * @param null|string $password
		 * @param null|string $session_id session identifier to use
		 * @return Auth_Info_User|null
		 */
		public static function initializeUser(
			string $user,
			?string $domain = null,
			#[\SensitiveParameter]
			string $password = null,
			string $session_id = null
		): ?\Auth_Info_User {
			$session_id = $session_id ?? session_id();
			if (!\apnscpSession::valid_id($session_id)) {
				fatal("invalid session id `%s'", $session_id);
			}
			$privilege_level = $user_id = $group_id = $site_id = null;
			if ($domain) {
				$site_id = \Auth::get_site_id_from_domain($domain);
				if (!$site_id) {
					error($domain . ': domain not found in lookup table');

					return null;
				}

				/** now lookup the main domain by site */
				$domain = \Auth::get_domain_from_site_id($site_id);
				$path = \Auth::get_domain_path('site' . $site_id) . '/etc/passwd';
				if (!file_exists($path) || !($fp = fopen($path, 'r'))) {
					error($domain . ': cannot access /etc/passwd');

					return null;
				}

				$admin = \Auth::get_admin_from_site_id($site_id);
				$privilege_level = $admin === $user ? PRIVILEGE_SITE : PRIVILEGE_USER;
				while (!feof($fp)) {
					$data = explode(':', trim(fgets($fp)));
					if ($data[0] === $user) {
						[$user_id, $group_id] = array_slice($data, 2, 2);
						break;
					}
				}
				fclose($fp);
				if (!$user_id) {
					return null;
				}
			} else {
				$pgdb = PostgreSQL::initialize();
				$group = $pgdb->query("SELECT reseller_id FROM resellers WHERE username = '" . pg_escape_string($pgdb->getHandler(), $user) . "'")->fetch_object();
				$path = \apnscpFunctionInterceptor::get_class_from_module('auth')::ADMIN_AUTH;
				if (false === ($fp = fopen($path, 'r'))) {
					fatal('Failed to open admin auth!');
				}

				while (!feof($fp)) {
					$data = explode(':', trim(fgets($fp)));
					if ($data[0] === $user) {
						$pwd = posix_getpwnam(APNSCP_SYSTEM_USER);
						[$user_id, $group_id] = [$pwd['uid'], $pwd['gid']];
						$privilege_level = $group ? PRIVILEGE_RESELLER : PRIVILEGE_ADMIN;
					}
				}

				/** to-do verify reseller password */
			}

			if (!$privilege_level) {
				return null;
			}
			$db = \apnscpSession::db();
			$stmt = $db->prepare("UPDATE " . apnscpSession::TABLE ."
                SET
                	username = :user,
                	domain = :domain,
                	level = :privilege_level,
                	login_ts = NOW(),
                	user_id = :user_id,
                	group_id = :group_id,
                	site_id = :site_id,
                	login_method = :method
            	WHERE
            		session_id = :session_id
			 ");
			$params = compact(['user', 'domain', 'privilege_level', 'user_id', 'group_id', 'site_id']) + [
				'method'     => \Auth::get_driver()->get_db_symbol(),
				'session_id' => $session_id
			];

			if (!$stmt->execute($params)) {
				\Error_Reporter::report($db->errorInfo()[0]);
				fatal('failed to access credentials database');
			}

			return static::factory($session_id);
		}

		/**
		 * Override user privilege level
		 *
		 * @param int $level
		 * @return self
		 */
		public function overrideLevel(int $level): self
		{
			$db = \apnscpSession::db();
			$stmt = $db->prepare("UPDATE " . apnscpSession::TABLE . " SET level = :level WHERE session_id = :session_id");
			$params = ['level' => $level, 'session_id' => $this->id];
			if (!$stmt->execute($params)) {
				\Error_Reporter::report($db->errorInfo()[0]);
				fatal('failed to access credentials database');
			}
			$this->level = $level;
			// release cached instance
			\Auth::autoload()->authDelete($this);
			return $this;
		}

		/**
		 * Role uses extended authentication
		 *
		 * @return bool
		 */

		public function extended(): bool
		{
			return $this->level & PRIVILEGE_EXTAUTH;
		}
		/**
		 * Contexted object is valid session
		 *
		 * @return bool
		 */
		public function valid(): bool
		{
			return !empty($this->username);
		}

		/**
		 * Terminate existing auth context
		 *
		 * @return void
		 */
		public function destroy(): void
		{
			\apnscpFunctionInterceptor::expire($this->id);
			$this->username = null;
			\apnscpSession::init()->destroy($this->id);
		}

		public static function factory($session): self
		{
			$db = \apnscpSession::db();
			$rs = $db->query("SELECT username,
                domain,
                level,
                user_id,
                group_id,
                reseller_id AS rgroup,
                site_id AS site_id 
                FROM " . apnscpSession::TABLE . " 
                LEFT JOIN reseller_sites USING(site_id)
                WHERE session_id = " . $db->quote($session));
			if (!$rs || $rs->rowCount() < 1) {
				fatal("invalid session `%s'", $session);
			}
			$rs = $rs->fetch(PDO::FETCH_ASSOC);
			$user = new static;
			foreach ($rs as $key => $val) {
				$user->$key = $val;
			}
			$user->user_id = (int)$user->user_id;
			$user->group_id = (int)$user->group_id;
			$user->site_id = (int)$user->site_id;
			$user->level = (int)$user->level;
			$user->rgroup = $user->rgroup ? \Opcenter\Reseller::RESELLER_PREFIX . $user->rgroup : null;
			$user->id = $session;
			if ($user->site_id) {
				$user->site = 'site' . $user->site_id;
			}

			return $user;
		}

		private function importAccount(): ?\Auth_Info_Account
		{
			return $this->conf = Auth_Info_Account::factory($this);
		}

		/**
		 * Reset contexted metadata
		 *
		 * @return Auth_Info_Account|null
		 */
		public function reset(): ?Auth_Info_Account
		{
			$this->conf = null;

			return $this->getAccount();
		}

		/**
		 * Get account metadata
		 *
		 * @return Auth_Info_Account|null
		 */
		public function getAccount(): ?Auth_Info_Account
		{
			if (!isset($this->conf)) {
				$this->importAccount();
			}

			return $this->conf;
		}

		/**
		 * Get account metadata configuration
		 *
		 * @param        $class
		 * @param string $type
		 * @return null|array
		 */
		public function conf($class, $type = 'cur'): ?array
		{
			return $this->getAccount()->$type[$class] ?? null;
		}
	}