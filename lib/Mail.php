<?php

	class Mail
	{
		private static $SMTP_USER = SMTP_USERNAME;
		private static $SMTP_PASSWORD = SMTP_PASSWORD;
		private static $SMTP_HOST = SMTP_HOST;
		private static $SMTP_PORT = SMTP_PORT;

		public static function send($to, $subject, $msg = '', $headers = '', $parameters = '')
		{
			if (!is_array($to)) {
				$new = array();
				$tmp = strtok($to, ' ,');
				while ($tmp !== false) {
					$new[] = $tmp;
					$tmp = strtok(' ,');
				}
				$to = $new;
			}
			if (is_array($headers)) {
				$tmp = null;
				foreach ($headers as $k => $v) {
					$tmp[] = "$k: $v";
				}
				$headers = join("\r\n", $tmp);
			}

			if (!$headers || false === strpos($headers, 'From:')) {
				$headers = 'From: ' . Crm_Module::FROM_NAME . ' <' . Crm_Module::FROM_ADDRESS . '>' . "\r\n" . $headers;
				$headers = rtrim($headers);
			}
			if (empty($to)) {
				return error('no valid smtp recipients');
			}
			if (!preg_match('/^Message-Id:/im', $headers)) {
				$headers = rtrim($headers) . "\r\n" . 'Message-Id: <' . time() . '-' .
					md5(uniqid()) . '@' . Util_HTTP::getHost() . '>';
			}
			if (!self::_sendSMTP($to, $subject, $msg, $headers, $parameters)) {
				return self::_sendSendmail($to, $subject, $msg, $headers, $parameters);
			}

			return true;


		}

		private static function _sendSMTP(array $to, $subject, $msg, $headers, $parameters)
		{
			$usetls = self::$SMTP_PORT === 465;
			if ($usetls && false === strpos(self::$SMTP_HOST, 'ssl://')) {
				$host = 'ssl://' . self::$SMTP_HOST;
			} else {
				$host = self::$SMTP_HOST;
			}
			if (!$host) {
				// use local mailer
				return false;
			}
			$smtp = new Net_SMTP($host, self::$SMTP_PORT, SERVER_NAME);
			if (Error_Reporter::isPEARError($e = $smtp->connect())) {
				warn('unable to use smtp: %s, falling back to local mailer',
					$e->getMessage());

				return false;
			}
			if (self::$SMTP_USER && Error_Reporter::isPEARError($e = $smtp->auth(self::$SMTP_USER, self::$SMTP_PASSWORD, '', $usetls))) {
				warn('smtp auth failed: %s, falling back to local mailer',
					$e->getMessage());

				return false;
			}
			$from = self::_extractFrom($headers);
			$smtp->mailFrom($from);
			foreach ($to as $addr) {
				$smtp->rcptTo($addr);
			}
			$data = 'To: ' . join(', ', $to) . "\r\n" .
				'Subject: ' . $subject . "\r\n" .
				'Date: ' . date('r') . "\r\n" .
				trim($headers) . "\r\n\r\n" . $msg;
			if (Error_Reporter::isPEARError($e = $smtp->data($data))) {
				warn('smtp data failed: %s, falling back to local mailer',
					$e->getMessage());

				return false;
			}

			$smtp->disconnect();

			return true;
		}

		private static function _extractFrom($headers)
		{

			$regex = sprintf('/^[Ff]rom:\s*(?:(?<email1>%s)|[^<]+<(?<email2>%s))>/m',
				Regex::recompose(Regex::EMAIL), Regex::recompose(Regex::EMAIL));
			if (!preg_match($regex, $headers, $match)) {
				return Crm_Module::FROM_ADDRESS;
			}
			if ($match['email1']) {
				return $match['email1'];
			}

			return $match['email2'];
		}

		private static function _sendSendmail(array $to, $subject, $msg, $headers, $parameters)
		{
			if (false === mail(implode(', ', $to), $subject, str_replace("\0", "<NUL>", $msg), $headers, $parameters)) {
				// @XXX PHP 7.1 returns false despite mail delivery?
				return warn('cannot send message via fallback');
			}

			return true;
		}

	}