<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class apnscpSession implements SessionHandlerInterface, SessionIdInterface, SessionUpdateTimestampHandlerInterface
	{
		const SESSION_ID = 'esprit_id';
		const TABLE = 'sessions';

		const ERR_INVALID_SESSION = [':err_session_invalid', 'Invalid session'];

		private static ?\PDO $db = null;
		private static self $handler;

		public static function restore_from_id(string $session_id, bool $destroy = true): bool
		{
			// current session is target fixation
			if ($session_id === \session_id()) {
				return true;
			}
			if (!static::valid_id($session_id)) {
				return false;
			}
			$db = self::db();
			$q = $db->query('SELECT 1 FROM ' . self::TABLE . ' ' .
				" WHERE session_id = " . $db->quote($session_id));

			if ($q->rowCount() < 1) {
				return false;
			}

			if (session_status() === PHP_SESSION_ACTIVE) {
				if (($module = Session::get('cache.modules')) instanceof ModuleCache) {
					// bug in PHP 7.4. Deep serialize not called on cache.modules
					$module->cleanDynamicCompositions();
				}

				$destroy ? session_destroy() : session_write_close();
			}

			session_id($session_id);
			if (!session_start()) {
				report('Failed to start session?');
			}
			if (session_status() !== PHP_SESSION_DISABLED) {
				return session_decode(static::init()->read($session_id));
			}

			return true;
		}

		public static function db(): PDO
		{
			if (!isset(self::$db)) {
				self::$db = \PostgreSQL::pdo(true);
			}

			return self::$db->ping();
		}

		public function read(string $session_id): string|false
		{
			$db = self::db();
			if (!self::valid_id($session_id)) {
				fatal(self::ERR_INVALID_SESSION);
			}
			$data = null;
			$qfrag = "WHERE session_id = '" . $session_id . "'";
			$stmt = $db->prepare('SELECT session_data FROM
                ' . self::TABLE . ' ' . $qfrag);
			if (false === $stmt->execute()) {
				fatal($session_id . ': unable to open session id (%s)', $stmt->errorInfo()[0]);
			}
			$stmt->bindColumn(1, $data, PDO::PARAM_LOB);

			if (!$stmt->fetch(PDO::FETCH_BOUND)) {
				return false;
			}

			return stream_get_contents($data);
		}

		/* Close session */

		public static function valid_id(string $id): bool
		{
			return ctype_alnum($id);
		}

		/* Read session data from database */

		public function create(string $session_id, string $data = null): string
		{
			$db = self::db();
			if (!self::valid_id($session_id)) {
				fatal(self::ERR_INVALID_SESSION);
			}
			$safe_data = $db->quote((string)$data);
			$ret = $db->query('INSERT INTO ' . self::TABLE . ' ' .
				"(session_data, session_id) VALUES(" . $safe_data . ", '" . $session_id . "')");
			if (!$ret) {
				fatal("Failed to create session ID `%s': %s", $session_id, $db->errorInfo()[2]);
			}

			return (string)$data;
		}

		public function sync(): bool
		{
			return $this->write(session_id(), session_encode());
		}

		public static function init(): self
		{
			if (isset(self::$handler)) {
				return self::$handler;
			}

			$handler = new self();
			if (session_status() === PHP_SESSION_NONE && (int)ini_get('session.use_cookies')) {
				session_set_cookie_params([
					'secure'   => \Util_HTTP::isSecure(),
					'httponly' => IS_ISAPI
				]);
			}

			/**
			 * @XXX close() must be called last
			 *
			 * When register_shutdown is true and a destructor updates a _SESSION
			 * var, session data is committed before __destruct() fires resuflting
			 * in an inconsistent state. Alternative is to move __destruct() _SESSION
			 * manipulation to register_shutdown_function or
			 * pass false to register_shutdown
			 */
			session_set_save_handler($handler, false);
			self::$handler = $handler;
			if (isset($_GET[self::SESSION_ID]) && static::valid_id($_GET[self::SESSION_ID])) {
				$session_id = $_GET[self::SESSION_ID];
				unset($_GET[self::SESSION_ID]);
				if ($session_id !== \session_id()) {
					\apnscpSession::restore_from_id($session_id);
				}
			}

			if (session_status() === PHP_SESSION_NONE && !(defined('SHIM') && SHIM)) {
				try {
					session_start();
				} catch (UnexpectedValueException $e) {
					if (!is_debug()) {
						report('Session encoding error');
						throw $e;
					}
					/**
					 * On 7.2, a class with a circular reference that implements Serializable can cause
					 * an unserialization error in PHP (ArrayObject in afi, $modules <=> $session['cache']['modules']
					 * unset($modules) fails to decode session data; $modules = null is OK
					 */
					dd($e);
				}
			}

			return $handler;
		}

		/**
		 * Delete all sessions on a site
		 *
		 * @param int         $site_id
		 * @param string|bool $keepid optional session id to preserve
		 * @return bool
		 */
		public static function invalidate_by_site_id(int $site_id, $keepid = null): bool
		{
			return self::invalidate($site_id, null, $keepid);
		}

		/**
		 * Delete filtered sessions
		 *
		 * @param null|int    $site_id
		 * @param string|null $user
		 * @param null        $keepid optional session id to preserve
		 * @return bool
		 */
		private static function invalidate(?int $site_id, ?string $user = null, $keepid = null): bool
		{
			if ($keepid) {
				if (is_bool($keepid)) {
					$keepid = session_id();
				} else if (!self::valid_id($keepid)) {
					fatal("invalid session id to preserve, `%s'", $keepid);
				}
			}

			$db = self::db();

			$query = 'DELETE FROM ' . self::TABLE . ' WHERE site_id ' .
				($site_id ? '= ' . (int)$site_id : 'IS NULL');

			if ($user) {
				if (!preg_match(Regex::USERNAME, $user)) {
					fatal("failed to invalidate session - invalid user `%s'", $user);
				}
				$query .= " AND username = " . $db->quote($user);
			}

			if ($keepid) {
				$query .= " AND session_id != '" . $keepid . "'";
			}

			$res = $db->query($query . " RETURNING session_id");

			if (!$res) {
				error_log('unable to delete session from database: ' . $db->errorInfo()[0]);

				return false;
			}

			foreach ($res->fetchAll(\PDO::FETCH_COLUMN) as $session_id) {
				\apnscpFunctionInterceptor::expire($session_id);
			}

			return true;
		}

		/**
		 * Report active sessions
		 *
		 * @param int|null    $site_id
		 * @param string|null $user
		 * @return array
		 */
		public static function active(?int $site_id, ?string $user = null): array
		{
			$db = self::db();

			$query = 'SELECT session_id FROM ' . self::TABLE . ' WHERE site_id ' .
				($site_id ? '= ' . (int)$site_id : 'IS NULL');

			if ($user) {
				if (!preg_match(Regex::USERNAME, $user)) {
					fatal("failed to invalidate session - invalid user `%s'", $user);
				}
				$query .= " AND username = " . $db->quote($user);
			}


			if (!($res = $db->query($query))) {
				error_log('unable to query session from database: ' . $db->errorInfo()[0]);

				return [];
			}

			return $res->fetchAll(\PDO::FETCH_COLUMN);
		}

		/* Write new data to database */

		/**
		 * @param int|null         $site_id site ID or null fo r
		 * @param string           $user
		 * @param bool|null|string $keepid  keep active ID or preserve an ID
		 * @return bool
		 */
		public static function invalidate_by_user(?int $site_id, string $user, $keepid = null): bool
		{
			return self::invalidate($site_id, $user, $keepid);
		}

		/* Destroy session record in database */

		/**
		 * Disable implicit use of Cookie: header
		 *
		 * @return void
		 */
		public static function disable_session_header(): void
		{
			if (session_status() === PHP_SESSION_NONE) {
				ini_set('session.use_only_cookies', '0');
				ini_set('session.use_cookies', '0');
				ini_set('session.cache_limiter', '');
			}
		}

		public function open(string $path, string $name): bool
		{
			self::db() or fatal('cannot access session db');

			return true;
		}

		public function close(): bool
		{
			// close session handler
			return true;
		}

		/**
		 * Release database connection for fork
		 *
		 * @return bool
		 */
		public function terminate(): bool
		{
			if (!is_null(self::$db)) {
				// if singleton
				try {
					self::$db->exec("SELECT pg_terminate_backend(pg_backend_pid())");
				} catch (\PDOException $e) { }
			}
			self::$db = null;
			return true;
		}

		public function write(string $session_id, string $data): bool
		{
			// ignore clobbering session data from backend if no data present
			if (IS_CLI || $data === '') {
				return true;
			}
			$db = self::db();
			$stmt = $db->prepare('UPDATE ' . self::TABLE . ' SET ' .
				"session_data = :data WHERE session_id = :id");
			$stmt->bindParam(':data', $data, PDO::PARAM_LOB);
			$stmt->bindParam(':id', $session_id);
			if (!$stmt->execute()) {
				error_log("Unable to write session data:\n" . $db->errorInfo()[0]);

				return false;
			}

			return true;
		}

		public function destroy(string $session): bool
		{
			$db = self::db();
			$res = $db->query("DELETE FROM ". self::TABLE . " WHERE session_id = " .
				$db->quote($session));
			if (!$res) {
				error_log('unable to delete session from database: ' . $db->errorInfo()[0]);

				return false;
			}
			// @TODO callback/decouple
			\apnscpFunctionInterceptor::expire($session);
			$_SESSION = [];

			return true;
		}

		public function gc(int $life): int|false
		{
			$tv_expiry = $_SERVER['REQUEST_TIME'] ?? time() - \Auth::TV_SEC;
			$db = self::db();
			try {
				return $db->exec('DELETE FROM ' . self::TABLE . ' ' .
					'WHERE auto_logout != 0::boolean AND ' .
					'last_action <  NOW() - INTERVAL \'' . $tv_expiry . ' SECONDS\' OR EXTRACT(EPOCH FROM AGE(NOW(),last_action))  >= ' . $life);
			} catch (\PDOException) {
				return false;
			}
		}

		public function create_sid(): string
		{
			$valid_chars = '0123456789' .
				'abcdefghijklmnopqrstuvwxyz' .
				'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$session_id = '';
			$maxlen = 32;
			if (PHP_VERSION_ID >= 70100) {
				$maxlen = (int)ini_get('session.sid_length');
			}
			for ($i = 0, $char_size = strlen($valid_chars) - 1; $i < $maxlen; $i++) {
				$session_id .= $valid_chars[random_int(0, $char_size)];
			}
			if ($this->exists($session_id)) {
				return $this->create_sid();
			}
			$this->create($session_id);
			return $session_id;
		}

		public function exists($session_id): bool
		{
			$db = self::db();
			if (!self::valid_id($session_id)) {
				fatal(self::ERR_INVALID_SESSION, $session_id);
			}
			$rs = $db->query("SELECT 1 FROM ". self::TABLE . " WHERE session_id = '" . $session_id . "'");

			return is_object($rs) && $rs->rowCount() >= 1;
		}

		public function validateId(string $id): bool
		{
			return $this->exists($id);
		}

		public function updateTimestamp(string $id, string $data): bool
		{
			$db = self::db();
			return $db->exec("UPDATE " . self::TABLE . ' SET last_action = NOW() WHERE session_id = ' . $db->quote($id)) > 0;
		}


	}