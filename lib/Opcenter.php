<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Class Opcenter
	 *
	 * Operations center
	 *
	 */
	class Opcenter
	{
		const PANEL_KEY = 'g:cp_rev';
		const OPCENTER_PATH = '/etc/virtualhosting';

		const ERR_RESPONSE_INVALID = [
			':opcenter_response_invalid',
			'Failed to read response - output received: %s'
		];

		const ERR_FAILED_TO_FETCH_GIT_LOG = 'failed to fetch git log';

		const DBG_COMMAND_STR = [
			':opcenter_command',
			'%(bin)s command: %(command)s'
		];

		/**
		 * Make a path for an Opcenter asset
		 *
		 * @param null|string $file filename
		 * @return string
		 */
		public static function mkpath(?string $file = '/'): string
		{
			return rtrim(self::OPCENTER_PATH . '/' .
				ltrim($file, '/'), '/');
		}

		/**
		 * Get apnscp version as string
		 *
		 * @return string
		 */
		public static function version(): string
		{
			$data = static::versionData();
			$version = self::versionShort();
			$revtag = '';
			if (!empty($data['revision'])) {
				$revtag = ' rev ' . substr($data['revision'], 0, 8) .
					' (' . date('Y/m/d', $data['timestamp']) . ')';
			}

			return PANEL_BRAND . ' v' . $version . $revtag;
		}

		/**
		 * Get short version MAJ.MIN.PATCH
		 * @return string
		 */
		public static function versionShort(): string
		{
			$data = static::versionData();
			return implode('.', array_where($data, static function ($key, $val) {
				return 0 === strncmp($key, 'ver_', 4) && $key !== 'ver_pre';
			}));
		}

		/**
		 * Get apnscp version data
		 *
		 * @param string $field
		 * @return string|array
		 */
		public static function versionData($field = '')
		{
			// @TODO convert to cache
			$cache = Cache_Global::spawn();

			if (false === ($cp = $cache->get(self::PANEL_KEY))) {
				$commit = ['commit' => null, 'date' => null];
				$version = APNSCP_VERSION;
				$proc = \Util_Process::exec('cd %(path)s && git log --submodule -n 1', ['path' => INCLUDE_PATH]);
				if (!$proc['success'] || !preg_match(Regex::CHANGELOG_COMMIT, $proc['output'], $commit)) {
					return error(self::ERR_FAILED_TO_FETCH_GIT_LOG);
				}
				$prerel = null;
				if (null !== ($tag = self::getReleaseTag())) {
					if ($tag[0] === 'v' && strspn($tag, '0123456789', 1, 1)) {
						$version = substr($tag, 1);
					} else {
						$prerel = $tag;
					}
				}

				$tokens = explode('.', $version);
				$n = 3 - count($tokens);
				while ($n > 0) {
					$tokens[] = 0;
					$n--;
				}
				if (false !== ($pos = strpos($tokens[2], '-'))) {
					$prerel = substr($tokens[2], $pos+1);
					$tokens[2] = substr($tokens[2], 0, $pos);
				}
				$cp = array(
					'revision'  => (string)$commit['commit'],
					'timestamp' => strtotime($commit['date']),
					'ver_maj'   => (int)$tokens[0],
					'ver_min'   => (int)$tokens[1],
					'ver_patch' => (int)$tokens[2],
					'ver_pre'   => (string)$prerel,
					'dirty'     => false !== strpos((string)$prerel, '-dirty')
				);

				$ts = mktime(3, 30, 0);
				if (time() >= $ts) {
					$ts += 86400;
				}
				$cache->set(self::PANEL_KEY, $cp, $ts - $_SERVER['REQUEST_TIME']);
			}

			return $field ? $cp[$field] : $cp;
		}

		/**
		 * Forget cached version
		 *
		 * @return bool key deleted
		 */
		public static function forgetVersion(): bool
		{
			$cache = Cache_Global::spawn();
			try {
				$cache->delete(self::PANEL_KEY);
			} catch (RedisException $e) {
				return false;
			}
			return true;
		}

		public static function getReleaseTag(): ?string
		{

			if (!is_dir(INCLUDE_PATH . '/.git')) {
				return null;
			}
			$proc = \Util_Process::exec('cd %(path)s && git describe --always --dirty', ['path' => INCLUDE_PATH], [0,128]);
			if (!$proc['success']) {
				return null;
			}
			return rtrim($proc['output']);
		}

		/**
		 * Get latest version
		 *
		 * @return null|string
		 */
		public static function latest(): ?string
		{
			if (!is_dir(INCLUDE_PATH . '/.git')) {
				return null;
			}
			$cache = \Cache_Global::spawn();
			$key = 'g:cp_tag';
			if (false === ($tags = $cache->get($key))) {
				$tags = Util_Process::exec('cd %s && git for-each-ref --sort=taggerdate --format "%s" refs/tags', [INCLUDE_PATH, '%(tag)']);
				if (!$tags['success']) {
					return null;
				}
				$tags = array_filter(explode("\n", rtrim($tags['output'])));
				$cache->set($key, $tags, 43200);
			}
			for ($i = \count($tags)-1; $i >= 0; $i--) {
				$tag = $tags[$i];
				if ($tag[0] !== 'v') {
					continue;
				}
				return $tag;
			}

			return null;
		}

		public static function updateTags(): bool
		{
			if (!is_dir(INCLUDE_PATH . '/.git')) {
				return false;
			}
			$marker = storage_path('.tag.marker');
			if (file_exists($marker) && (time() - filemtime($marker)) < 86400 ) {
				return false;
			}
			$ret = Util_Process::exec('cd %s && git fetch --tags', [INCLUDE_PATH]);
			touch($marker);
			return $ret['success'];
		}
	}