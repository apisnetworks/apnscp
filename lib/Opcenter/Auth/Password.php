<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Opcenter\Auth;

	class Password
	{
		/**
		 * Verify if password is strong
		 *
		 * @param string      $passwd plaintext password
		 * @param null|string $user   optional user to check against
		 * @return bool
		 */
		public static function strong(string $passwd, string $user = null): bool
		{
			if (!AUTH_PW_CHECK) {
				return true;
			}

			$disallowed = array(
				'test',
				'password',
				'letmein',
				'qwerty',
				'1234',
				'12345',
				'123456',
			);
			if ($user) {
				$disallowed[] = $user;
			}
			$passwd = (string)$passwd;
			if (\in_array($passwd, $disallowed)) {
				return error('password is weak');
			} else if (\strlen($passwd) < \Auth_Module::MIN_PW_LENGTH) {
				return error("minimum password length is `%d'", \Auth_Module::MIN_PW_LENGTH);
			} else if ($user && \strlen($user) >= 6 && preg_match('/.{0,1}' . $user . '.{0,1}/i', $passwd)) {
				return error("password cannot contain username `%s'", $user);
			} else if (ctype_digit($passwd)) {
				return error("password must contain at least one letter");
			}

			/**
			 * At least 7 characters long + require 2 of 3 classes:
			 * - at least 1 uppercase
			 * - at least 1 lowercase
			 * - at least 1 numeric
			 */
			return true;
		}

		/**
		 * Generate a random password
		 *
		 * @param int    $len     password length
		 * @param string $charset character set to use
		 * @return string
		 */
		public static function generate(int $len = 16, string $charset = 'a-z0-9A-Z'): string
		{
			// 5e28 combinations should be good enough!
			$charlist = count_chars(preg_replace_callback('#.-.#', static function (array $m) {
				return implode('', range($m[0][0], $m[0][2]));
			}, $charset), 3);
			$chLen = \strlen($charlist);

			if ($len < 1) {
				fatal('Length must be greater than zero.');
			} else if ($chLen < 2) {
				fatal('Character list must contain as least two chars.');
			}
			$res = '';
			for ($i = 0; $i < $len; $i++) {
				$res .= $charlist[random_int(0, $chLen - 1)];
			}

			return $res;
		}
	}