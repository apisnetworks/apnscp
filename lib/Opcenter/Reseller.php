<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Opcenter;

class Reseller {
	const RESELLER_PREFIX = 'group';

	/**
	 * Parse ID from path
	 *
	 * @param string $path
	 * @return Identity|null
	 */
	public static function idFromPath(string $path): ?int
	{
		$resellerBase = FILESYSTEM_VIRTBASE . '/' . self::RESELLER_PREFIX;
		if (0 !== strpos($path, $resellerBase)) {
			return null;
		}
		$id = substr($path, $len = strlen($resellerBase), strpos($path . '/', '/', $len) - $len);
		if (ctype_digit($id)) {
			return (int)$id;
		}

		return null;
	}

	public static function id(int|string $group): ?int
	{
		if (is_int($group)) {
			return $group;
		}

		if (!str_starts_with($group, self::RESELLER_PREFIX)) {
			return null;
		}
		$id = substr($group, strlen(self::RESELLER_PREFIX));
		if ($id != (int)$id) {
			return null;
		}

		return (int)$id;
	}

	public static function is(string $identifier): bool
	{
		return str_starts_with($identifier, self::RESELLER_PREFIX) && strspn(substr($identifier, strlen(self::RESELLER_PREFIX), 1), '123456789');
	}

	/**
	 * Reseller support enabled
	 *
	 * @return bool
	 */
	public static function enabled(): bool
	{
		return (bool)RESELLER_ENABLED;
	}

	public static function pathFromid(int $id): string
	{
		return FILESYSTEM_VIRTBASE . '/' . self::RESELLER_PREFIX . $id;
	}

	/**
	 * Given reseller id, name, or group exists
	 *
	 * @param int|string $identifier
	 * @return bool
	 */
	public static function exists(int|string $identifier): bool
	{
		if ($tmp = self::id($identifier)) {
			$identifier = $tmp;
		}

		$db = \PostgreSQL::initialize();
		if (is_int($identifier)) {
			$query = 'reseller_id = ' . $identifier;
		} else {
			$query = 'username = \'' . $db->escape_string($identifier) . '\'';
		}
		$rs = $db->query("SELECT 1 FROM resellers WHERE $query");
		return $rs->num_rows() > 0;
	}
}