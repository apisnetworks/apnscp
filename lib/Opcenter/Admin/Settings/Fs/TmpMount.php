<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

	namespace Opcenter\Admin\Settings\Fs;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\CliParser;
	use Opcenter\System\Memory;

	class TmpMount implements SettingsInterface
	{
		private const BOOTSTRAP_VAR_PREFIX = 'tmpfs_';

		public function set($val, ...$x): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			$settings = $this->get();
			$cfg = new Bootstrapper\Config();
			if (\is_scalar($val)) {
				if (\count($x) === 0) {
					$defaults = $this->getDefault();
					// unset
					$val = [$val => $defaults[$val] ?? null];
				} else if (\count($x) === 1 && is_scalar($x[0])) {
					$val = [$val => $x[0]];
				} else {
					return error('Non-sensical use of settings: %s %s', $val, CliParser::collapse($x));
				}
			} else if ($x) {
				// $val is array and $x provided
				return error('Non-sensical use of settings: %s %s', CliParser::collapse($val), CliParser::collapse($x));
			}

			if (array_intersect_key($settings, $val) === $val) {
				// no change
				return true;
			}

			// check
			if ($unknown = array_diff_key($val, $settings)) {
				return error('Unknown settings: %s', implode(', ', array_keys($unknown)));
			}
			$newVals = array_replace($settings, $val);
			foreach ($newVals as $k => $v) {
				$chk = 'check' . studly_case($k);
				if (!$this->$chk($v)) {
					return false;
				}
			}
			foreach (array_diff_assoc($newVals, $settings) as $k => $v) {
				$k = static::BOOTSTRAP_VAR_PREFIX . $k;
				$cfg[$k] = $v;
			}
			$cfg->sync();
			Bootstrapper::run('filesystem/make-mounts');
			return true;
		}

		private function checkSize($n) {
			if (!\is_int($n) || $n < 512) {
				return error('/tmp mount size is too small or not a number');
			}
			if ($n > ($max = Memory::stats()['memtotal']/1024)) {
				return error('Requested /tmp mount size %d is larger than system memory: %d MB', $n, $max);
			}
			return true;
		}

		private function checkInodes($n) {
			if (!\is_int($n) || $n < 512) {
				return error('/tmp inode count is too small or not a number');
			}
			$ret = \Util_Process::exec('df --output=iused /tmp');
			if (!$ret['success']) {
				return error('Failed to query inode statistics on /tmp: %s', $ret['stderr']);
			}
			strtok($ret['output'], "\n");
			$count = (int)strtok(' ');
			if ($count > $n) {
				return error('Resizing inode count would result in /tmp having too few inodes. Current count: %d', $count);
			} else if ($count * 1.15 > $n) {
				return warn('At least a 15% inode buffer is recommended. Current inode count: %d, recommended minimum: %d. Continuing anyway.', $count, $n);
			}
			return true;
		}

		private function checkAttrs($x) {
			return true;
		}

		public function get()
		{
			$cfg = new Bootstrapper\Config();
			$defaults = $cfg->loadRole('filesystem/make-mounts');
			$scopeDefaults = $this->getDefault();
			return array_filter(array_build($defaults, static function ($k, $v) use ($cfg, $scopeDefaults) {
				if (0 !== strpos($k, static::BOOTSTRAP_VAR_PREFIX)) {
					return null;
				}
				$newkey = substr($k, 6);
				if ($cfg->isDefault($k)) {
					if (!isset($scopeDefaults[$newkey])) {
						// filter out namespaced parameters like tmpfs_tune for Mitogen
						return null;
					}
					$v = $scopeDefaults[$newkey];
				}

				return [$newkey, $v];
			}));
		}

		public function getHelp(): string
		{
			return 'Set /tmp mount parameters';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			$size = min((int)(Memory::stats()['memtotal'] / 1024), 4096);
			$nrinodes = 5000000;
			return [
				'size' => $size,
				'inodes' => $nrinodes,
				'attrs' => 'mode=1777,strictatime,nosuid,nodev,noexec,noatime,nr_inodes=' . $nrinodes . ',size=' . $size . 'M'
			];
		}
	}