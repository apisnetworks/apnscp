<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;

	class Debug implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();

			return $cfg->set('core', 'debug', (bool)$val) && Apnscp::restart();
		}

		public function get()
		{
			return is_debug();
		}

		public function getHelp(): string
		{
			return 'Enable developer diagnostics in ApisCP';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}