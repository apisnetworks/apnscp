<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;
	use Opcenter\CliParser;
	use Opcenter\Map;

	class Config implements SettingsInterface
	{
		// @var bool automatically restart on change
		protected $restart = true;

		public function get(...$var)
		{
			$config = [];
			foreach (['config.ini', 'custom/config.ini'] as $c) {
				$path = config_path($c);
				if (file_exists($path)) {
					$config = array_replace_recursive($config,
						Map::load($path, 'r-', 'inifile')->section(null)->fetchAll());
				}
			}

			$fixer = static function (&$v) {
				$wasString = isset($v[1]) && ($v[0] === '"' || $v[0] === "'") && $v[-1] === $v[0];
				$v = \Util_Conf::inferType($v);
				if ($wasString) {
					$v = '"' . $v . '"';
				}
			};

			if (!$var) {
				array_walk_recursive($config, $fixer);
				return $config;
			}

			$value = array_get($config, implode('.', $var), null);
			$fixer($value);
			return $value;
		}

		/**
		 * Set apnscp configuration
		 *
		 * Format: config_set apnscp.config section option value
		 * Example: config_set apnscp.config core debug 1
		 *
		 * @param       $val
		 * @param mixed ...$args
		 * @return bool
		 */
		public function set($val, ...$args): bool
		{
			if (!$val) {
				return error("Missing section `%s' option", $val);
			} else if (!isset($args[0])) {
				return error("Missing option for section `%s'", $val);
			} else if (!\array_key_exists(1, $args)) {
				return error("Missing value parameter for option `%s'", $args[0]);
			}

			$val = strtolower((string)$val);
			$args[0] = strtolower((string)$args[0]);

			if (!preg_match('/^[a-z0-9_]+$/i', $val) || !preg_match('/^[a-z0-9_]+$/i', $args[0])) {
				return error('Section and option names must be alpha-numeric');
			}
			if (!AUTH_ADMIN_API && $val === 'auth' && $args[1] === 'admin_api') {
				return error('API access is revoked. Attempt to change [auth] => admin_api must be done manually');
			}
			if (\is_array($args[1])) {
				$args[1] = CliParser::collapse($args[1]);
			}
			if (null === $this->get($val, $args[0])) {
				warn("Value `%s' => `%s' has not been previously set before in apnscp", $val, $args[0]);
			}
			$map = Map::load(config_path('custom/config.ini'), 'c', 'inifile');
			if ($args[1] === null) {
				if (!$map->section($val)->exists($args[0])) {
					return warn('no override present for [%s] => %s in config.ini', $val, $args[0]);
				}
				$map->section($val)->delete($args[0]);
			} else {
				if ($map->section($val)->exists($args[0]) && $map->section($val)->fetch($args[0]) === $args[1]) {
					// no-op
					return true;
				}

				if (\is_bool($args[1])) {
					$args[1] = $args[1] ? 'true' : 'false';
				} else if ($args[1] === 'null') {
					// prevent clobbering "null" providers
					$map->quoted(true);
				}
				$map->section($val)->set($args[0], (string)$args[1]);
			}

			$map = null;

			if ($this->restart) {
				Apnscp::restart('1 minute');
			}

			return true;
		}

		/**
		 * Set restart on change
		 *
		 * @param bool $restart
		 */
		public function setRestart(bool $restart) {
			$this->restart = $restart;
		}

		public function getHelp(): string
		{
			return 'Manipulate config.ini directly';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			return [];
		}
	}