<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2024
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Map;

	class UpdateOffset implements SettingsInterface
	{

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if ($val < 0 || $val > 365*24) {
				return error("Value outside range");
			}

			if ((new UpdatePolicy)->get() === 'edge-major') {
				warn("Panel in edge-major mode. Update offset has no effect in this mode");
			}

			if (!is_int($val)) {
				warn("Converted %(val)s to %(newval)d", ['val ' => $val, 'newval' => (int)$val]);
			}

			$sysconf = Map::load('/etc/sysconfig/apnscp', 'w', 'inifile');
			$sysconf->section(null)->set('APNSCP_UPDATE_OFFSET', (string)intval($val));
			$sysconf->close();

			return true;
		}

		public function get()
		{
			$sysconf = Map::load('/etc/sysconfig/apnscp', 'r', 'inifile');
			return (int)($sysconf->section(null)->offsetGet('APNSCP_UPDATE_OFFSET') ?: 0);
		}

		public function getHelp(): string
		{
			return 'Set staggered update in hours';
		}

		public function getValues()
		{
			return '[0,8760]';
		}

		public function getDefault()
		{
			return 0;
		}
	}