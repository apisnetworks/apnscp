<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;

	class Workers implements SettingsInterface
	{
		const WORKER_VALUES = ['min', 'max', 'start'];

		public function get()
		{
			$config = new Config();

			return array_combine(self::WORKER_VALUES, array_map(static function ($k) use ($config) {
				return $config->get('apnscpd', $k . '_workers');
			}, self::WORKER_VALUES));
		}

		public function set($val): bool
		{
			if (!\is_array($val)) {
				return error('Value must be hash');
			}
			foreach ($val as $k => $v) {
				if (!\in_array($k, self::WORKER_VALUES)) {
					return error("Invalid worker setting `%s'", $k);
				}
				if ($v < 0 || !\is_int($v)) {
					return error("Invalid worker value `%s' for `%s'", $v, $k);
				}
			}

			$cfg = new Config();
			foreach ($val as $k => $v) {
				$cfg->set('apnscpd', "{$k}_workers", (int)$v);
			}
			unset($cfg);
			Apnscp::restart();

			return true;
		}

		public function getHelp(): string
		{
			return 'Configure apnscp backend workers';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			return ['min' => 1, 'max' => 5, 'start' => 1];
		}
	}