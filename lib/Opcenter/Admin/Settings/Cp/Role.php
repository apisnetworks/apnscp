<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Settings\Messages;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\License;

	class Role implements SettingsInterface
	{
		const MSG_SEE_ALSO_URL = [':msg_scope_url_cfg_see_further', "See %(url)s for additional configuration"];
		const MSG_FREE_LICENSE_GENERATION = [':msg_free_license_gen', "A free license may be generated within %(url)s"];
		public function set($val): bool
		{
			if ($val == ($old = $this->get())) {
				// no need to set the value again
				return true;
			}

			if (!in_array($val, $this->getValues(), true)) {
				return error(Messages::ERR_VALUE_INVALID, ['value' => $val]);
			}

			$bsconfig = new \Opcenter\Admin\Bootstrapper\Config;

			$newPairs = $this->roleSettings($val);
			$oldPairs = array_keys($this->roleSettings($old));

			$this->unsetPairs($bsconfig, $oldPairs);
			$this->setPairs($bsconfig, $newPairs);

			if ($val === 'data-center') {
				warn(self::MSG_SEE_ALSO_URL, [
					'url' => 'https://docs.apiscp.com/admin/Mass%20hosting/#enabling-data-center-mode'
				]);
			}

			$license = License::get();
			if ($license->isTrial() && in_array($val, ['dev', 'dns-only', 'proxy', 'supervisor'])) {
				info(self::MSG_FREE_LICENSE_GENERATION, ['url' => 'https://my.apiscp.com']);
			}

			if ($val === 'proxy') {
				info(self::MSG_SEE_ALSO_URL, [
					'url' => 'https://docs.apiscp.com/admin/Panel%20proxy/'
				]);
			}

			$bsconfig->sync();
			return \Opcenter\Admin\Bootstrapper::run();
		}

		private function setPairs(\Opcenter\Admin\Bootstrapper\Config $cfg, array $pairs): void
		{
			foreach ($pairs as $k => $v) {
				$cfg[$k] = $v;
			}
		}

		private function unsetPairs(\Opcenter\Admin\Bootstrapper\Config $cfg, array $values): void
		{
			array_forget($cfg, $values);
		}

		private function roleSettings(string $val): array {
			return match($val) {
				'supervisor' => [
					'webserver_type' => null,
					'ftp_enabled' => false,
					'has_low_memory' => true,
					'user_daemons' => false,
					'panel_headless' => true
				],
				'low-memory' => [
					'has_low_memory' => true
				],
				'data-center' => [
					'has_low_memory' => false,
					'user_daemons' => true,
					'data_center_mode' => true
				],
				'dns-only' => [
					'has_dns_only' => true
				],
				'dev' => [],
				'proxy' => [
					'has_low_memory' => true,
					'has_proxy_only' => true,
					'cp_proxy_ip' => '127.0.0.1',
					'anyversion_node' => true
				],
				'default' => []
			};
		}

		public function get()
		{
			$license = \Opcenter\License::get();
			$config = new \Opcenter\Admin\Bootstrapper\Config(true);

			return match(true) {
				$config['webserver_type'] === null => 'supervisor',
				$config['has_proxy_only'] === true => 'proxy',
				$license->isDnsOnly() || $config['has_dns_only'] === true => 'dns-only',
				$license->isDevelopment() => 'dev',
				$config['has_low_memory'] === true => 'low-memory',
				$config['data_center_mode'] === true => 'data-center',
				default => 'default'
			};
		}

		public function getHelp(): string
		{
			return 'Set server profile';
		}

		public function getValues()
		{
			return ['default','data-center','low-memory','dns-only','supervisor','dev','proxy'];
		}

		public function getDefault()
		{
			return 'default';
		}

	}
