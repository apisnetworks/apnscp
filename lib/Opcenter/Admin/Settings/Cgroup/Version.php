<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Cgroup;

	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Filesystem;
	use Opcenter\System\Cgroup;
	use Opcenter\System\Kernel;
	use Opcenter\Versioning;

	class Version implements SettingsInterface
	{
		public function set($val): bool
		{
			if ((int)$val === $this->get()) {
				return true;
			}

			if ($val !== 2 && version_compare(os_version(), '9.0', '>=')) {
				return error("cgroupv2 only version supported");
			}

			if (!in_array($val, $this->getValues(), false)) {
				return error("Unknown cgroup value");
			}

			if ($val === 2 && Versioning::compare($version = Kernel::version(), '5.0', '>=') && Versioning::compare($version, '5.8', '<')) {
				// memory.stat added to root in 5.8, cpu.stat added before
				warn("v5 kernels before 5.8 lack necessary v2 counters. All system-level counters will be approximate.");
			}

			$cfg = new Config();
			$cfg['cgroup_unified_hierarchy'] = (int)$val === 2;
			unset($cfg);

			info("Removing all prior configurations from %(dir)s", ['dir' => Cgroup::CGROUP_SERVER_DIR]);
			Filesystem::readdir(
				Cgroup::CGROUP_SERVER_DIR,
				fn($file) => str_ends_with($file, '.conf') && unlink(Cgroup::CGROUP_SERVER_DIR . "/{$file}")
			);

			\Opcenter\Admin\Bootstrapper::run('system/cgroup');
			return info("This server will reboot shortly. EditDomain --reconfig --all necessary.");
		}

		public function get()
		{
			return Cgroup::version();
		}

		public function getHelp(): string
		{
			return 'Control group type';
		}

		public function getValues()
		{
			return [1,2];
		}

		public function getDefault()
		{
			return version_compare(os_version(), '9.0', '>=') ? 2 : 1;
		}

	}