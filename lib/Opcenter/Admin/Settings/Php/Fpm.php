<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2021
	 */

	namespace Opcenter\Admin\Settings\Php;

	use Opcenter\Account\Enumerate;
	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\SiteConfiguration;

	class Fpm implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (!$val) {
				$this->warnIfAbandoningSites();
			}

			$cfg = new Config();
			$cfg['php_build_fpm'] = (bool)$val;
			$cfg->sync();
			return Bootstrapper::run('apnscp/bootstrap', 'php/install', 'apnscp/php-filesystem-template', ['force' => true]);
		}

		private function warnIfAbandoningSites(): void
		{
			$affected = [];
			foreach (Enumerate::sites() as $site) {
				if (null === ($ctx = \Auth::nullableContext(null, $site))) {
					continue;
				}

				$svc = SiteConfiguration::shallow($ctx);
				if ($svc->getServiceValue('apache', 'jail')) {
					$affected[] = $site;
				}
			}

			if ($affected) {
				warn("Disabling PHP-FPM will affect the following sites: %s \nAfterward run:  EditDomain --all -c apache,jail=0", implode(", ", $affected));
			}
		}

		public function get()
		{
			return \Opcenter\Http\Php\Fpm::hasCapability();
		}

		public function getHelp(): string
		{
			return 'Toggle use of PHP-FPM';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}
	}