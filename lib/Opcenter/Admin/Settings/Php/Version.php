<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Php;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Http\Php\Fpm\MultiPhp;
	use Opcenter\Php;
	use Opcenter\Versioning;

	class Version implements SettingsInterface
	{
		public function set($val): bool
		{
			$val = false === strpos((string)$val, '.') ? "{$val}.0" : (string)$val;

			$oldVal = $this->get();
			if (file_exists('/usr/bin/php') && $val === $oldVal) {
				// allow removal of /usr/bin/php to shortcircuit check
				return true;
			}

			self::versionCheck($val);

			$multiVersions = MultiPhp::list();
			if (array_key_exists(Versioning::asMinor($val), $multiVersions)) {
				warn("%s exists as MultiPHP. Removing from multiPHP build. Existing sites will continue to use multiPHP until updated.",
					$val);
				(new Multi)->set([Versioning::asMinor($val) => false]);
			}

			$cfg = new Config();

			$cfg['system_php_version'] = str_replace(',', '.', $val);
			unset($cfg);

			$roles = [
				'php/install',
				'php/install-extensions',
				'php/create-configuration',
				'apnscp/php-filesystem-template',
			];

			// moving up to 8.x or off from 8.x
			if (Versioning::asMajor($val) !== Versioning::asMajor($oldVal)) {
				$roles[] = 'mail/webmail-horde';
			}

			call_user_func_array([Bootstrapper::class, 'run'], array_merge($roles, [['php_force_build' => true]]));

			return true;
		}

		public function get()
		{
			$version = (new Config())->offsetGet('system_php_version');
			return false === strpos((string)$version, '.') ? Versioning::asMinor((string)$version) : (string)$version;
		}

		public function getHelp(): string
		{
			return 'Change system-wide PHP version';
		}

		public function getValues()
		{
			return Php::VERSIONS;
		}

		public function getDefault()
		{
			return '7.4';
		}

		/**
		 * Check if version EOL'd
		 *
		 * @param string $version
		 * @return bool version is recent
		 */
		public static function versionCheck(string $version): bool
		{
			if (version_compare($version, '8.1', '<')) {
				warn('Using PHP below 8.1 is not recommended. PHP 5.6/7.0 reached end-of-life December 2018. ' .
					'PHP 7.1 reached end-of-life December 2019. PHP 7.2 reached end-of-life November 2020. ' .
					'PHP 7.3 reached end-of-life December 2021. PHP 7.4 reached end-of-life November 2022. ' .
					'See also %s', 'http://php.net/supported-versions.php'
				);

				return false;
			}

			return true;
		}
	}