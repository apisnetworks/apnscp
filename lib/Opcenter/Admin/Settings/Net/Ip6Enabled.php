<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Net;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Net\Ip6;
	use Opcenter\System\Sysctl;

	class Ip6Enabled implements SettingsInterface
	{
		const PROCFS_KEY = 'net.ipv6.conf.all.disable_ipv6';
		public function set($val): bool
		{
			if ($val === $this->get()) {
				// no need to set the value again
				return true;
			}

			if ($val && Sysctl::read(self::PROCFS_KEY)) {
				// confirm not disabled in kernel
				return error("IPv6 disabled in procfs. Set %s=0 before using this scope", self::PROCFS_KEY);
			}

			if ($val && !Ip6::my_ip()) {
				return error("No IPv6 address detected on machine");
			}

			(new \Opcenter\Admin\Settings\Cp\Bootstrapper())->set('has_ipv6', (bool)$val);
			// see lib/Opcenter/Admin/Settings for all active scopes
			info("Run %s after Bootstrapper completion",
				sprintf("EditDomain --reconfig -c ipinfo6,enabled=%d --all", $val)
			);
			Sysctl::persist(self::PROCFS_KEY, (int)$val^1);

			return Bootstrapper::run(
				'apache/configure',
				'mail/configure-dovecot',
				'mail/configure-postfix',
				'software/haproxy',
				'apnscp/bootstrap',
				'vsftpd/configure',
				[
					'force' => true
				]
			);
		}

		public function get()
		{
			$hasIp6 = (new \Opcenter\Admin\Settings\Cp\Bootstrapper())->get('has_ipv6');
			if (is_bool($hasIp6)) {
				return $hasIp6;
			}

			return Ip6::enabled();
		}

		public function getHelp(): string
		{
			return 'Set IPv6 support on server';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}

	}
