<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Http\Apache;

	class Mutex extends SystemDirective
	{
		public function set($directive, ...$val): bool
		{
			$contents = file_get_contents(static::CONFIG_FILE);
			if (!in_array($directive, $this->getValues(), true)) {
				return error("Unknown value `%s'", $directive);
			}

			$count = 0;
			$contents = preg_replace('/^\s*mutex\s+[^\r\n]+$/im', "Mutex {$directive} default", $contents, -1, $count);
			if (!$count) {
				$contents = rtrim($contents) . "\nMutex {$directive} default\n";
			}

			return file_put_contents(static::CONFIG_FILE, $contents, LOCK_EX) > 0 && Apache::restart();
		}

		public function get(...$directive)
		{
			$contents = file_get_contents(static::CONFIG_FILE);
			if (preg_match('/^\s*Mutex ([^\s+])+/i', $contents)) {
				return $contents[1];
			}

			return $this->getDefault();
		}

		public function getHelp(): string
		{
			return 'Set mutex mechanism';
		}

		public function getValues()
		{
			return ['posixsem', 'pthread', 'sysvsem'];
		}

		public function getDefault()
		{
			return 'posixsem';
		}

	}
