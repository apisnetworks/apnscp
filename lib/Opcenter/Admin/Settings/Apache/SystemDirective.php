<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Http\Apache;
	use Opcenter\Map;

	class SystemDirective implements SettingsInterface
	{
		protected const CONFIG_FILE = Apache::HTTP_HOME . '/conf/httpd-custom.conf';

		public function get(...$directive)
		{
			$ini = Map::load(Apache::SYSCONFIG_FILE);
			if (!($opts = $ini->section(null)->offsetGet('OPTIONS'))) {
				return false;
			}
			if ($directive) {
				return false !== strpos($opts, '-D' . $directive[0]);
			}
			return array_map(static function ($val) {
				return trim($val, ' \'"');
			}, preg_split('/-D/', trim($opts, '\'"'), -1, PREG_SPLIT_NO_EMPTY));
		}

		public function set($directive, ...$val): bool
		{
			// avoid calling overwritten helper, e.g. apache.block10
			$directives = array_fill_keys(self::get(), true);
			$val = $val[0] ?? true;
			$ini = Map::write(Apache::SYSCONFIG_FILE);
			if (!is_array($directive)) {
				$directive = [$directive => $val];
			} else if (array_is_list($directive)) {
				warn("Converting list to hash");
				$directive = array_fill_keys($directive, true);
			}
			$directives = array_filter(array_merge($directives, $directive));
			$ret = $ini->section(null)->quoted(true)->set('OPTIONS', implode(' ', array_map(fn($x) => '-D' . $x, array_keys($directives))));

			return $ret && (Apache::run('stop') || true) && Apache::run('start');
		}

		public function getHelp(): string
		{
			return 'Set Apache directive (-DNAME)';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return '';
		}
	}