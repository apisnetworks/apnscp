<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\Cp\Config;

	class Dav extends SystemDirective
	{
		const DIRECTIVE = 'DAV';

		public function get(...$val)
		{
			return parent::get(self::DIRECTIVE);
		}

		public function set($val, ...$x): bool
		{
			return parent::set(self::DIRECTIVE, $val) &&
				(new Config())->set('dav', 'apache', (bool)$val);

		}

		public function getHelp(): string
		{
			return 'Enable DAV via HTTP';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}