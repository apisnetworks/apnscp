<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Apnscp;
	use Opcenter\Http\Apache;
	use Opcenter\Net\Port;

	class Ports extends SystemDirective
	{
		const DIRECTIVE = 'NO_SSL';

		public function set($val, ...$x): bool
		{
			$ports = $this->get();

			if (isset($val['nossl'])) {
				$ports[0] = $val['nossl'];
			} else if (isset($val[0])) {
				$ports[0] = $val[0];
			}

			if (isset($val['ssl'])) {
				$ports[1] = $val['ssl'];
			} else if (isset($val[1])) {
				$ports[1] = $val[1];
			}

			$defaults = $this->get();
			if ($ports == $defaults) {
				// no need to set the value again
				return true;
			}

			if (!$ports[0]) {
				return error('Non-SSL port must always be enabled');
			}
			foreach (array_diff_assoc($ports, $defaults) as $type => $port) {
				if ($port === false) {
					continue;
				} else if (null === $port) {
					$ports[$type] = false;
					continue;
				} else if ($port < 1 || $port > 65535) {
					return error('Port %d out of range', $port);
				}
				if (!is_numeric($port)) {
					return error("Port value `%s' must be numeric or false", $port);
				}
				if (HTTPD_ALL_INTERFACES) {
					if (!Port::free($port, null, 'tcp')) {
						return error("Port `%d' is in use", $port);
					}
				} else {
					foreach (['4', '6'] as $proto) {
						$family = "tcp{$proto}";
						$class = "\\Opcenter\\Net\\Ip$proto";
						if (!$class::enabled()) {
							continue;
						}
						foreach ($class::nb_pool() as $ip) {
							if (!Port::free($port, $ip, $family)) {
								return error("Port `%d' already bound on `%s'", $port, $ip);
							}
						}
					}
				}
			}
			if (!\count(array_filter($ports))) {
				return error('At least 1 port must be enabled');
			}

			$cfg = new Config();
			$cfg->setRestart(false);
			$cfg->set('httpd', 'nossl_port', $ports[0]);
			$cfg->set('httpd', 'ssl_port', $ports[1]);
			unset($cfg);
			info('Restarting %s...', MISC_PANEL_BRAND);
			Apnscp::restart() && Apnscp::wait();
			return $this->changePorts($ports[0], $ports[1]) &&
				info('Rebuild all accounts: EditDomain --all --reconfig');
		}

		/**
		 * Update ports in httpd-custom.conf
		 *
		 * @param int|bool $nonssl
		 * @param int|bool $ssl
		 */
		protected function changePorts($nonssl, $ssl): bool
		{
			$maps = [
				[HTTPD_NOSSL_PORT, $nonssl],
				[HTTPD_SSL_PORT ?: 0, $ssl ?: 0]
			];
			$cfgFile = Apache::CONFIG_PATH . '/httpd-custom.conf';
			if (!file_exists($cfgFile)) {
				fatal('Missing httpd-custom.conf - run upcp -sb');
			}
			$httpdConfig = file_get_contents($cfgFile);
			copy($cfgFile, "{$cfgFile}.save");

			foreach ($maps as $map) {
				if ($map[0] === $map[1]) {
					continue;
				}
				$re = \Regex::compile(
					'!(Listen\s+[^ ]*?)(?<=:| )%(port)s\b|((?:[0-9\.]{1,3}){4}:|\[[^]]+]:)%(port)d\b!',
					['port' => $map[0]]
				);

				if (!($httpdConfig = preg_replace($re, sprintf('{$1}{$2}%d', $map[1]), $httpdConfig))) {
					warn('Failed to replace %s to %s', $map[0], $map[1]);
				}
			}

			file_put_contents($cfgFile, $httpdConfig);

			$ret = true;
			if (!HTTPD_SSL_PORT && $ssl) {
				$ret &= parent::set(static::DIRECTIVE, true);
			} else if (HTTPD_SSL_PORT && !$ssl) {
				$ret &= parent::set(static::DIRECTIVE, false);
			}

			if (!Apache::reload('now')) {
				warn('Failed self-test - reverting configuration!');
				copy($cfgFile, "{$cfgFile}.bad");
				copy("{$cfgFile}.save", $cfgFile);
				return false;
			}

			return (bool)$ret;
		}

		public function get(...$val)
		{
			return [
				HTTPD_NOSSL_PORT,
				HTTPD_SSL_PORT
			];
		}

		public function getHelp(): string
		{
			return 'Set Apache ports';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			return [80, 443];
		}

	}
