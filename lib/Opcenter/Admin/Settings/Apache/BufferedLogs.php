<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Http\Apache;

	class BufferedLogs extends SystemDirective
	{
		public function set($directive, ...$val): bool
		{
			$contents = file_get_contents(static::CONFIG_FILE);
			if ($directive === false) {
				$contents = rtrim($contents) . "\nBufferedLogs off\n";
			} else {
				$contents = preg_replace('/^\s*bufferedlogs off\s*$[\r\n]{0,2}/im', '', $contents);
			}

			return file_put_contents(static::CONFIG_FILE, $contents, LOCK_EX) > 0 && Apache::restart();
		}

		public function get(...$directive)
		{
			return false === stripos(file_get_contents(static::CONFIG_FILE), 'BufferedLogs off');
		}

		public function getHelp(): string
		{
			return 'Disable log buffering';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}

	}
