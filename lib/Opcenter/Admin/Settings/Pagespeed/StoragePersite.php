<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2024
	 */

	namespace Opcenter\Admin\Settings\Pagespeed;

	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class StoragePersite implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (!\is_bool($val) && $val !== 0 && $val !== 1) {
				return error("Unknown value `%s'", $val);
			}

			$val = (bool)$val;

			$cfg = new Config;
			$cfg->set('pagespeed', 'storage_persite', $val);
			unset($cfg);

			return info("Account edit necessary");
		}

		public function get()
		{
			$config = new Config;

			return (bool)$config->get('pagespeed', 'storage_persite');
		}

		public function getHelp(): string
		{
			return 'Store Pagespeed optimized assets in account roots';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}