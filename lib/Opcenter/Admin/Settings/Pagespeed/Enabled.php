<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2024
	 */

	namespace Opcenter\Admin\Settings\Pagespeed;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Enabled implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (!\is_bool($val) && $val !== 0 && $val !== 1) {
				return error("Unknown value `%s'", $val);
			}

			$val = (bool)$val;

			$cfg = new Config();
			$cfg['pagespeed_enabled'] = $val;
			unset($cfg);
			Bootstrapper::run('apache/modpagespeed');

			return true;
		}

		public function get()
		{
			return PAGESPEED_ENABLED;
		}

		public function getHelp(): string
		{
			return 'Enable Pagespeed support';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}
	}
