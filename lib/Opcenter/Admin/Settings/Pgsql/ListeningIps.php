<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2024
	 */

	namespace Opcenter\Admin\Settings\Pgsql;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Net\IpCommon;

	class ListeningIps implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			foreach ((array)$val as $v) {
				if ($v === '*' || $v === '0.0.0.0') {
					continue;
				}

				if (!IpCommon::valid($v)) {
					return error("IP address %(ip)s is invalid", ['ip' => $v]);
				}

				if (!IpCommon::bound($v)) {
					return error("IP address %(ip)s is invalid", ['ip' => $v]);
				}
			}

			if (!$val) {
				warn("Cannot disable IP connectivity entirely");
				$val = $this->getDefault();
			}

			if (is_array($val)) {
				$val = implode(',', $val);
			}

			(new Bootstrapper\Config)->offsetSet('pgsql_bind_address', $val);

			return Bootstrapper::run('pgsql/install');
		}

		public function get()
		{
			$pgdb = \PostgreSQL::stub();
			$val = $pgdb->query("SELECT setting FROM pg_settings WHERE name = 'listen_addresses'")->fetch_object()?->setting;
			if (null === $val) {
				return $this->getDefault();
			}

			return array_map(trim(...), explode(',', $val));
		}

		public function getHelp(): string
		{
			return 'Bind PostgreSQL to named addresses';
		}

		public function getValues()
		{
			return 'array|string';
		}

		public function getDefault()
		{
			$bs = new Bootstrapper\Config(true);
			$bs->loadRole('pgsql/install');

			return $bs['pgsql_remote_connections'] ? '*' : ['127.0.0.1', '::1'];
		}

	}