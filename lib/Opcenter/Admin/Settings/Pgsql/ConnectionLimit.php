<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Pgsql;

	class ConnectionLimit extends \Opcenter\Admin\Settings\Mysql\ConnectionLimit
	{
		public function get()
		{
			$db = \PostgreSQL::initialize();
			return (int)$db->query("SHOW max_connections")->fetch_object()->max_connections;
		}

		public function getDefault()
		{
			return 50;
		}

	}
