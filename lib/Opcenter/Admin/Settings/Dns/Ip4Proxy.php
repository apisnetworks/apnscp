<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\Dns;

	use Lararia\Jobs\SimpleCommandJob;
	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;

	class Ip4Proxy implements SettingsInterface
	{
		use \NamespaceUtilitiesTrait;

		public function get(...$var)
		{

			return (new Config)->get('dns', $this->getVarName());

			return $pool::nb_pool();
		}

		/**
		 * Set pool
		 *
		 * @param       $val
		 * @return bool
		 */
		public function set($val): bool
		{
			$type = $this->getVarName() === 'proxy_ip4' ? 'Ip4' : 'Ip6';
			$class = '\\Opcenter\\Net\\' . $type;
			if ($val && !$class::valid($val)) {
				return error("Invalid public %s address `%s'", $type, $val);
			}
			$cfg = new Config();
			$oldval = $cfg->get('dns', $this->getVarName());
			if (!$cfg = $cfg->set('dns', $this->getVarName(), $val ?: null)) {
				return false;
			}
			Apnscp::wait();

			$context = \Auth::context(\Auth::get_admin_login());
			(new SimpleCommandJob($context, function () use ($context, $oldval, $val) {
				$afi = \apnscpFunctionInterceptor::factory($context);
				$serviceVar = static::class === self::class ? 'proxyaddr' : 'proxy6addr';
				$sites = $afi->admin_collect(["dns.$serviceVar" => $oldval]);
				foreach (array_keys($sites) as $site) {
					$afi->admin_edit_site($site, ["dns.$serviceVar" => $val]);
				}
			}))->dispatch();
			return true;
		}

		public function getHelp(): string
		{
			return 'Set public IP address. See docs/NAT.md';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return null;
		}


		/**
		 * Get configuration var name
		 *
		 * @return string
		 */
		private function getVarName(): string {
			return self::class === static::class ? 'proxy_ip4' : 'proxy_ip6';
		}
	}