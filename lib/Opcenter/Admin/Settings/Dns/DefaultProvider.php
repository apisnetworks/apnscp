<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace Opcenter\Admin\Settings\Dns;

	use Opcenter\Admin\Settings\Cp\Bootstrapper;
	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;
	use Opcenter\Dns;
	use Opcenter\Service\Validators\Dns\Provider;

	class DefaultProvider implements SettingsInterface
	{
		public function get(...$var)
		{
			return (new Config())->get('dns', 'provider_default');
		}

		/**
		 * Set provider
		 *
		 * @param       $val
		 * @return bool
		 */
		public function set($val): bool
		{
			if ($val === null) {
				$val = 'null';
			}
			if (!\Opcenter\Dns::providerValid($val)) {
				if ($val === 'powerdns') {
					return error('PowerDNS provider is not configured yet. ' .
						"Enable the module by running 'cpcmd scope:set cp.config powerdns_enabled true', " .
						"then setup the module with 'upcp -sb software/powerdns'"
					);
				}
				return error("Invalid DNS provider `%s'", $val);
			}
			$cfg = new Config();
			$cfg->setRestart(false);
			if ($val !== ($old = $cfg->get('dns', 'provider_default')) &&
				'' != ($oldkey = $cfg->get('dns', 'provider_key')))
			{
				warn("DNS provider changed from `%s' to `%s' - cleared provider key `%s'",
					$old, $val, $oldkey
				);
				$cfg->set('dns', 'provider_key', null);
			}
			return $cfg->set('dns', 'provider_default', $val) && (new Bootstrapper())->set('dns_default_provider', $val)
				&& Apnscp::restart();
		}

		public function getHelp(): string
		{
			return 'Default DNS provider assigned to accounts';
		}

		public function getValues()
		{
			return Dns::providers();
		}

		public function getDefault()
		{
			return 'builtin';
		}
	}