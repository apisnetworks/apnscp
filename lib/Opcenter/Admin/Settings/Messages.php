<?php declare(strict_types=1);

namespace Opcenter\Admin\Settings;

class Messages {
	public const ERR_VALUE_INVALID = [':err_scope_value_invalid', "Setting %(value)s unknown or unsupported"];
}