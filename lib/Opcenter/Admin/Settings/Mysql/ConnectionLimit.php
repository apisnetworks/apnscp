<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mysql;

	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class ConnectionLimit implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!is_int($val) || $val < 10 || $val > 100000) {
				return error("Invalid connection limit value");
			}

			$cfg = new Config();
			$cfg[$this->dbType() . '_max_connections'] = $val;
			unset($cfg);

			return \Opcenter\Admin\Bootstrapper::run($this->dbType() . '/install');
		}

		public function get()
		{
			$db = \MySQL::initialize();
			return (int)$db->query("SHOW VARIABLES LIKE 'max_connections'")->fetch_object()->Value;
		}

		public function getHelp(): string
		{
			return 'Set maximum concurrent connections';
		}

		public function getValues()
		{
			return 'int';
		}

		public function getDefault()
		{
			return 151;
		}

		private function dbType(): string
		{
			return self::class === static::class ? 'mysql' : 'pgsql';
		}

	}
