<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mysql;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Database\MySQL;

	class BigSelects implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			$setting = $this->readSetting();
			if ($setting['global_value_path'] !== MySQL\Service::SYSTEM_CONF) {
				warn("%(val)s is presently set in %(file)s; setting this scope likely has no effect", [
					'val' => 'max_join_size',
					'file' => $setting['global_value_path']
				]);
			}

			$cfg = new Config();
			$cfg['mysql_big_selects'] = (bool)$val;
			unset($cfg);

			return Bootstrapper::run('mysql/install');
		}

		public function get()
		{
			// @XXX blows up if 2^64 > val > 2^32
			return $this->readSetting()['global_value'] === '18446744073709551615';
		}

		private function readSetting(): array
		{
			return MySQL::systemVariable('max_join_size');
		}

		public function getHelp(): string
		{
			return 'Enable unbounded records';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}