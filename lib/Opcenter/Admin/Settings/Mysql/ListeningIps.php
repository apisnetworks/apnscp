<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mysql;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Database\MySQL;
	use Opcenter\Database\MySQL\Service;
	use Opcenter\Map;
	use Opcenter\Net\IpCommon;

	class ListeningIps implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				// no need to set the value again
				return true;
			}

			if (MySQL::version() < 101101 && \count($val) > 1) {
				return error("MariaDB before 10.11.1 does not support cherry-picked addresses");
			}

			foreach ((array)$val as $v) {
				if ($v === '*' || $v === '') {
					if (\count((array)$val) > 1) {
						return error("Wildcards are not permitted when multiple IPs are specified.");
					}
					continue;
				}

				if (!IpCommon::valid($v)) {
					return error("IP address %(ip)s is invalid", ['ip' => $v]);
				}

				if (!IpCommon::bound($v)) {
					return error("IP address %(ip)s is invalid", ['ip' => $v]);
				}
			}
			if (is_array($val)) {
				$val = implode(',', $val);
			}

			$map = Map::write(Service::SYSTEM_CONF, 'inifile')->section('mysqld');
			if (!$val) {
				unset($map['bind_address']);
			} else {
				$map['bind_address'] = (string)$val;
			}
			// @TODO since 8.0.13
			$map->save();

			return Service::restart();
		}

		public function get()
		{
			try {
				$db = \MySQL::initialize();
				return $db->query("SELECT VARIABLE_VALUE AS v FROM INFORMATION_SCHEMA.GLOBAL_VARIABLES WHERE VARIABLE_NAME = 'bind_address'")->fetch_object()?->v  ?? '';
			} catch (\mysqli_sql_exception) {
				return nerror("MySQL is down. Setting value anyway.");
			}
		}

		public function getHelp(): string
		{
			return 'Bind MySQL to the named addresses';
		}

		public function getValues()
		{
			return 'array|string';
		}

		public function getDefault()
		{
			return "";
		}

	}