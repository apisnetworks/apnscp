<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Argos;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Argos\Config;

	class Backend implements SettingsInterface
	{
		const BACKEND_NAME = \Argos_Module::DEFAULT_BACKEND;

		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			if (static::BACKEND_NAME === \Argos_Module::DEFAULT_BACKEND) {
				return \apnscpFunctionInterceptor::init()->argos_set_default_relay($val);
			}

			foreach ((array)$val as $v) {
				if (!\in_array($v, $this->getValues(), true)) {
					return error("Unsupported backend `%s'", $v);
				} else if ($v === static::BACKEND_NAME) {
					return error('Backend named after self');
				}
			}

			return \apnscpFunctionInterceptor::init()->argos_config_relay(static::BACKEND_NAME, ['backend' => $val]);
		}

		public function get()
		{
			if (!($cfg = Config::get()) ) {
				return $cfg;
			}

			if (self::BACKEND_NAME === static::BACKEND_NAME) {
				return $cfg->get()->backend('backends');
			}

			return $cfg->get()->backend(static::BACKEND_NAME)['backend'];
		}

		public function getValues()
		{
			if  ( !($cfg = \Opcenter\Argos\Config::get()) ) {
				return $cfg;
			}
			return $cfg->getBackends();
		}

		public function getHelp(): string
		{
			return 'Change standard Argos notification relay';
		}

		public function getDefault()
		{
			return 'pushover';
		}
	}