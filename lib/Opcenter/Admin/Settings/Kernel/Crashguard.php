<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, June 2023
 */

namespace Opcenter\Admin\Settings\Kernel;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\System\Memory;

	class Crashguard implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if ($val && Memory::stats()['memtotal'] < 1600*1024) {
				warn("Enabling kernel crashguard will consume more than 10% of memory");
			}

			$cfg = new Config();
			$cfg['kdump_enabled'] = (bool)$val;
			unset($cfg);
			Bootstrapper::run('system/kernel');

			info("A reboot is necessary once completed");

			return true;
		}

		public function get()
		{
			$cfg = new Config();
			return array_get($cfg, 'kdump_enabled', false);
		}

		public function getHelp(): string
		{
			return 'Enable kexec barrier';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
