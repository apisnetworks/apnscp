<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Backups;

	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class AutomaticDatabaseExports implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			$cfg = new Config;
			$cfg['manual_database_backups'] = !$val;
			unset($cfg);

			return \Opcenter\Admin\Bootstrapper::run('apnscp/crons');
		}

		public function get()
		{
			return !array_get(new Config(), 'manual_database_backups', false);
		}

		public function getHelp(): string
		{
			return 'Perform automatic database backups every night';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}

	}