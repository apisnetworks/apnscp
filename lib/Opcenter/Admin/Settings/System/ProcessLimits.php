<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2024
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class ProcessLimits implements SettingsInterface
	{
		private const OVERWRITE_KEY = 'ulimit_vfs_custom_config';
		private const DEFAULT_KEY = 'ulimit_vfs_config';

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			$cfg = (new Config);
			$built = $cfg[self::OVERWRITE_KEY] ?? [];
			foreach(array_dot($val) as $k => $v) {
				if ($v === null) {
					array_forget($built, $k);
				} else if (!$this->setField($k, $v, $built)) {
					return error("Failed to set limits. Invalid type or value for `%s'", $k);
				}
			}

			$cfg[self::OVERWRITE_KEY] = $built;

			$cfg->sync();
			return Bootstrapper::run('system/limits');
		}

		private function setField(string $field, mixed $val, array &$built): bool
		{
			if (!str_contains($field, '.')) {
				if (!$this->setField("{$field}.hard", $val, $built)) {
					return false;
				}
				$field .= ".soft";
			}
			[$name, $type] = explode('.', $field, 2);
			if ($type !== 'soft' && $type !== 'hard') {
				return error("Unknown enforcement type `%s'. Must be 'soft' or 'hard'", $type);
			}

			if (!$this->validate($name, $val)) {
				return error("Invalid value for `%s'", $name);
			}

			array_set($built, "{$name}.{$type}", $val);
			return true;
		}

		private function validate(string $field, mixed $val): bool
		{
			if (!match ($field) {
				"locks" =>
					$val === "unlimited" || is_int($val) && $val >= 0 && $val < 2 ** 63,
				"core", "data", "fsize", "rss", "as" =>
					$val === "unlimited" || is_int($val) && $val >= 0 && $val < 2 ** 32 - 1,
				"nproc", "priority", "sigpending", "rtprio", "memlock", "cpu", "maxlogins", "maxsyslogins" =>
					is_int($val) && $val >= 0 && $val < 2**63,
				"nofile", "msgqueue", "stack" =>
					is_int($val) && $val >= 0 && $val <= 2**20,
				"nice" =>
					is_int($val) && $val >= -20 && $val <= 19,
				default =>
					error("Unknown field `%s'", $field)
			}) {
				return false;
			}

			return true;
		}

		public function get()
		{
			$cfg = new Config;
			return (array)$cfg[self::OVERWRITE_KEY] + (array)$cfg->loadRole('system/limits')[self::DEFAULT_KEY];
		}

		public function getHelp(): string
		{
			return 'Set process limits within vfs';
		}

		public function getValues()
		{
			return 'array';

		}

		public function getDefault()
		{
			$role = (new Config)->loadRole('system/limits');
			return $role[self::DEFAULT_KEY];
		}
	}