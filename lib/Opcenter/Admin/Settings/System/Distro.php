<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2021
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Symfony\Component\Yaml\Yaml;

	class Distro implements SettingsInterface
	{
		public function set($val): bool
		{
			if (false === ($val = $this->canonicalize($val))) {
				return false;
			}

			if ($val === $this->get()) {
				return true;
			}

			$playbookCfg = $this->loadPlaybook();
			$pathways = array_get($playbookCfg, "0.vars.distro_pathways." . $this->canonicalize($this->get()), []);
			if (!in_array($val, $pathways, true)) {
				return error("Valid pathways from %(source)s are: %(target)s", [
					'source' => $this->canonicalize($this->get()),
					'target' => implode(',', $pathways)
				]);
			}

			$instance = $this->mockPlaybook();
			$instance::run(['distro' => $val]);
			info("Migrating to %(target)s. %(email)s will receive an email once completed. Migration will be logged to %(log)s", [
				'target' => $val,
				'email'  => \apnscpFunctionInterceptor::init()->common_get_email(),
				'log'    => array_get($playbookCfg, "0.vars.logfiles.$val")
			]);
			return true;
		}

		private function canonicalize(string $term)
		{
			switch (strtolower($term)) {
				case 'alma':
				case 'almalinux':
					return 'AlmaLinux';
				case 'rocky':
					return 'Rocky';
				case 'centos':
				case 'stream':
					return 'CentOS';
				default:
					return error("Unknown distro value: %s", $term);
			}
		}

		public function get()
		{
			return \apnscpFunctionInterceptor::init()->stats_release()['id'];
		}

		public function getHelp(): string
		{
			return 'Change OS type';
		}

		public function getValues()
		{
			return array_keys(array_get($this->loadPlaybook(), '0.vars.distro_pathways', []));
		}

		public function getDefault()
		{
			return 'CentOS';
		}

		private function loadPlaybook(): array
		{
			return Yaml::parseFile($this->mockPlaybook()::playbookPath());
		}

		private function mockPlaybook(): Bootstrapper
		{
			return new class extends Bootstrapper {
				public const PLAYBOOK = 'distro-convert.yml';
			};
		}
	}