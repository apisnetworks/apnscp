<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\System;

	use CLI\Yum\Synchronizer\Utils;
	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;
	use Opcenter\Filesystem;
	use Opcenter\Http\Apache;
	use Opcenter\Logging\Rsyslog;
	use Opcenter\Logging\SystemdJournald;
	use Opcenter\Map;

	class Timezone implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			$ret = \Util_Process::exec(['timedatectl', 'set-timezone', $val]);
			if (!$ret['success']) {
				return error($ret['stderr']);
			}
			if (!date_default_timezone_set($val)) {
				return error('Failed to set default timezone');
			}
			$iniPaths = [
				config_path('php.ini'),
				'/etc/php.ini',
				Utils::getServicePath('siteinfo') . '/etc/php.ini'
			];
			foreach ($iniPaths as $ini) {
				Map::write($ini)->section('apnscp')->set('date.timezone', $val);
			}
			if (file_exists($path = Utils::getServicePath('siteinfo') . '/etc/localtime')) {
				unlink($path);
			}
			symlink(readlink('/etc/localtime'), $path);
			// @TODO move to Utils::reloadFilesystem()?
			Filesystem\Vfs::reload();
			Apnscp::restart('next minute');
			Apache::restart();
			SystemdJournald::restart();
			Rsyslog::exists() && Rsyslog::restart();
			Bootstrapper::run('pgsql/install');
			return true;
		}

		public function get()
		{
			return date_default_timezone_get();
		}

		public function getHelp(): string
		{
			return 'Change system timezone';
		}

		public function getValues()
		{
			return 'Any timezone';
		}

		public function getDefault()
		{
			return date_default_timezone_get();
		}
	}