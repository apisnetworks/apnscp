<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class IntegrityCheck implements SettingsInterface
	{
		/**
		 * Invoke Bootstrapper
		 *
		 * Passed as scope:set system.integrity-check true or
		 * scope:set system.integrity-check tag1 tag2 tagn '[var1:val1,var2:val2]'
		 *
		 * @param       $var
		 * @param mixed ...$val
		 * @return bool
		 */
		public function set($var, ...$val): bool
		{
			Bootstrapper::run($var, ...$val);

			return true;
		}

		public function get()
		{
			return false;
		}

		public function getHelp(): string
		{
			return 'Run a thorough platform integrity check';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}