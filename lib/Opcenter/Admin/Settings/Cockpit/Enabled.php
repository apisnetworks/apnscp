<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2025
	 */

	namespace Opcenter\Admin\Settings\Cockpit;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Enabled implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if ($val !== null && !\is_bool($val) && $val !== 0 && $val !== 1) {
				return error("Unknown value `%s'", $val);
			}

			$val = $val === null ? $val : (bool)$val;

			$cfg = new Config();
			$cfg['cockpit_enabled'] = $val;
			if ($val && !\Auth::profile()->extended()) {
				warn("This feature requires TOTP to be enabled for the account. ".
					"It may be setup under Account > Settings > Security. See \"Cockpit.md\" in ".
					"the documentation for further information.");
			}

			unset($cfg);
			Bootstrapper::run('software/cockpit');

			return true;
		}

		public function get()
		{
			$config = new Config();
			return $config['cockpit_enabled'] ?? null;
		}

		public function getHelp(): string
		{
			return 'Enable integrated Cockpit management';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return null;
		}
	}