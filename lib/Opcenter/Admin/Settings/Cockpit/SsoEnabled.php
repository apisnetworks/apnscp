<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2025
	 */

	namespace Opcenter\Admin\Settings\Cockpit;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SsoEnabled implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (!\is_bool($val) && $val !== 0 && $val !== 1) {
				return error("Unknown value `%s'", $val);
			}

			if ($val && !(new Enabled)->get()) {
				return error("%(service)s must be enabled", ['service' => 'Cockpit']);
			}

			$val = (bool)$val;

			$cfg = new Config();
			$cfg['cockpit_sso'] = $val;

			unset($cfg);
			Bootstrapper::run('software/cockpit');

			return true;
		}

		public function get()
		{
			$config = new Config();
			return (bool)$config['cockpit_sso'];
		}

		public function getHelp(): string
		{
			return 'Enable Cockpit SSO behavior';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}