<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Settings\Dns\DefaultProviderKey as KeyParent;

	class DefaultProviderKey extends KeyParent
	{
		const TYPE = 'mail';

		public function getHelp(): string
		{
			return 'Default mail provider authentication token';
		}
	}
