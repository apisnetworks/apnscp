<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SpamThreshold implements SettingsInterface
	{
		public function set($val): bool
		{
			if (!(new Enabled)->get()) {
				return error("%(service)s is %(state)s", ['service' => 'mail', 'state' => 'disabled']);
			}

			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!is_int($val) && !is_float($val) || $val < 1) {
				return error("Invalid rewrite threshold");
			}

			if ($val > ($threshold = (new SpamDeletionThreshold)->get())) {
				return error("Value greater than deletion threshold `%d'", $threshold);
			}

			(new Config)->offsetSet($this->scoreParameter(), $val);
			Bootstrapper::run('mail/spamassassin', 'mail/rspamd');

			return true;
		}

		private function spamFilter(): string
		{
			return (new SpamFilter)->get();
		}

		private function scoreParameter(): string
		{
			return $this->spamFilter() === 'rspamd' ?
				'rspamd_rewrite_subject_score' : 'spamassassin_rewrite_subject_score';
		}

		public function get()
		{
			return (new Config)->offsetGet($this->scoreParameter()) ?: $this->getDefault();
		}

		public function getHelp(): string
		{
			return 'Set score limitation for spam subject';
		}

		public function getValues()
		{
			return 'int';
		}

		public function getDefault()
		{
			return (new SpamFilter)->get() === 'rspamd' ? 8 : 5;
		}

	}