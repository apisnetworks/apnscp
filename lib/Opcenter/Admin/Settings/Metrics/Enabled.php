<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Metrics;

	use Daphnie\Collector;
	use Lararia\Jobs\SimpleCommandJob;
	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\Cp\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;

	class Enabled implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg->setRestart((bool)$val);
			$cfg->set('telemetry', 'enabled', (bool)$val);

			if (!$val) {
				\Cronus::suspend();
				(new SimpleCommandJob(\Auth::profile(), static function () {
					Apnscp::cancelPendingRestart();
					defer($_, static function () {
						Apnscp::restart('now');
					});

					(new Collector(\PostgreSQL::pdo()))->deleteSince(time());
				}))->dispatch();
			}

			Bootstrapper::run('apnscp/install-services');

			return (bool)$val ?: info("Metric removal will occur shortly");
		}

		public function get()
		{
			return (bool)TELEMETRY_ENABLED;
		}

		public function getHelp(): string
		{
			return 'Enable long-term metrics storage';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}

	}