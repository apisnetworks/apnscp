<?php declare(strict_types=1);
	/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, May 2023
 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\System\Cgroup\Controller;
	use Opcenter\System\Cgroup\Group;

	abstract class Freezer extends Controller
	{
		protected const SERVICE_ATTRIBUTES = [
			'state' => 'freezer.state'
		];

		public const LOGGABLE_METRICS = [];

		public function __construct(Group $group, array $permissions)
		{
			// don't let users freeze themselves
			array_set($permissions, 'task.fperm', 0640);
			parent::__construct($group, $permissions);
		}

		public function stats(): array
		{
			return [];
		}
	}