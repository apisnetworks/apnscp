<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, May 2023
 */

namespace Opcenter\System\Cgroup\Controllers;

use Opcenter\Filesystem\Mount;
use Opcenter\System\Cgroup;
use Opcenter\System\Cgroup\Attributes\Blkio\Weight;
use Opcenter\System\Cgroup\Contracts\ControllerAttribute;
use Opcenter\System\Cgroup\Controller;
use Opcenter\System\Kernel;

abstract class Blkio extends Controller
{
	// @var \Auth_Info_User $ctx
	private \Auth_Info_User $ctx;

	protected const SERVICE_ATTRIBUTES = [
		'writeiops' => 'blkio.throttle.write_iops_device',
		'readiops'  => 'blkio.throttle.read_iops_device',
		'writebw'   => 'blkio.throttle.write_bps_device',
		'readbw'    => 'blkio.throttle.read_bps_device',
		'ioweight'  => 'blkio.weight'
	];

	protected const BLKIO_TEMPLATE = [
		'bw-read'     => 0,
		'bw-write'    => 0,
		'bw-rlimit'   => 0,
		'bw-wlimit'   => 0,
		'iops-read'   => 0,
		'iops-write'  => 0,
		'iops-rlimit' => 0,
		'iops-wlimit' => 0,
		'dynamic'     => false,
	];

	/**
	 * @var array metric counters for blkio
	 * NOTE: fields are capitalized to reflect formatting in counter
	 */
	public const LOGGABLE_METRICS = [
		'bw-read'  => [
			'label'   => 'Read (KB)',
			'type'    => 'monotonic',
			'counter' => 'blkio.throttle.io_service_bytes:Read'
		],
		'bw-write' => [
			'label'   => 'Write (KB)',
			'type'    => 'monotonic',
			'counter' => 'blkio.throttle.io_service_bytes:Write'
		],
		'bk-read'  => [
			'label'   => 'Read (blocks)',
			'type'    => 'monotonic',
			'counter' => 'blkio.throttle.io_serviced:Read'
		],
		'bk-write' => [
			'label'   => 'Write (blocks)',
			'type'    => 'monotonic',
			'counter' => 'blkio.throttle.io_serviced:Write'
		],
	];

	public function import(\Auth_Info_User $ctx = null): int
	{
		$this->ctx = $ctx ?? \Auth::profile();

		return parent::import($ctx);
	}

	public function stats(): array {
		if (!$this->exists()) {
			return self::BLKIO_TEMPLATE;
		}

		return $this->statsWrapper(self::BLKIO_TEMPLATE);
	}

	abstract protected function statsWrapper(array $stats): array;

	public function createAttribute(string $parameter, mixed $value): Cgroup\Attributes\BaseAttribute
	{
		if ($value === null) {
			$value = 0;
		}
		return parent::createAttribute($parameter, $value);
	}

	private function qualifiedNamespace(): string
	{
		$elevator = implode(':', Mount::getDeviceFromPath(FILESYSTEM_VIRTBASE));;
		$namespace = version_compare(Kernel::version(), '4.0', '>=') ? '.throttle' : '';

		return $this->getPath() . '/blkio' . ($elevator === 'bfq' ? '.bfq' : $namespace);
	}

	protected function getElevatorNamespacedPath(string $counter): string
	{
		$prefix = $this->qualifiedNamespace();

		return "{$prefix}.{$counter}";
	}

	/**
	 * {@inheritDoc}
	 */
	public function reset(): bool
	{
		return file_put_contents($this->getPath() . '/blkio.reset_stats', '1') > 0;
	}


	public function setAttribute(ControllerAttribute $attr): bool
	{
		$attr->setContext($this->ctx);
		if (\get_class($attr) === Weight::class && !$attr->canUse()) {
			debug('Skipping weight - non CFQ/BFQ elevator detected');

			return false;
		}

		return parent::setAttribute($attr);
	}

	protected function readCounter(string $path, string $parameter = null): string
	{
		[$path, $type] = $this->tokenize($path);
		$raw = parent::readCounter($path, $parameter);
		if (!$type) {
			return $raw;
		}
		$str = strtok($raw, "\n");
		do {
			if (false === strpos((string)$str, " $type ")) {
				continue;
			}
			$counter = substr($str, strrpos($str, ' '));
			if (substr($path, -6) !== '_bytes') {
				return $counter;
			}

			return (string)((int)$counter / 1024);
		} while (false !== ($str = strtok("\n")));
		debug("Unknown/invalid blkio counter attr `%s' in %s", $type, $path);

		return '0';
	}

	protected function tokenize(string $path): array
	{
		$type = null;
		$delimiter = $path[strcspn($path, '=:')] ?? null;
		if ($delimiter) {
			$pos = strrpos($path, $delimiter);
			$type = substr($path, $pos + 1);
			$path = substr($path, 0, $pos);
		}

		return [$path, $type];
	}


}