<?php declare(strict_types=1);
	/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, May 2023
 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\System\Cgroup\Controller;

	abstract class Cpuacct extends Controller
	{
		protected const SERVICE_ATTRIBUTES = [];

		public const LOGGABLE_METRICS = [
			'usage' => [
				'label'   => 'CPU Usage (sec)',
				'type'    => 'monotonic',
				'counter' => 'cpuacct.usage'
			],
			'user' => [
				// centiseconds!
				'label'   => 'CPU User Usage (sec)',
				'type'    => 'monotonic',
				'counter' => 'cpuacct.ustat'
			],
			'system' => [
				// centiseconds!
				'label'   => 'CPU System Usage (sec)',
				'type'    => 'monotonic',
				'counter' => 'cpuacct.sstat'
			]
		];
	}
