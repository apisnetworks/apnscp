<?php declare(strict_types=1);
	/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, May 2023
 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\System\Cgroup\Controller;

	abstract class Pids extends Controller
	{
		protected const SERVICE_ATTRIBUTES = [
			'proclimit' => 'pids.max'
		];

		public const LOGGABLE_METRICS = [
			'used' => [
				'label'   => 'Processes',
				'type'    => 'value',
				'counter' => 'pids.current'
			],
		];

		public function stats(): array
		{
			$pids = [
				'max'     => -1,
				'current' => 0,
				'dynamic' => false
			];
			if (!$this->exists()) {
				return $pids;
			}

			['used' => $used, 'proclimit' => $max] = $this->readMetrics(['used', 'proclimit']);

			return [
				'current' => $used,
				// technically it's pid_max from /proc/sys/kernel/pid_max
				'max'     => $max ?? 4096,
				'dynamic' => $this->group->delegated()
			];
		}


		/**
		 * Read PID count
		 *
		 * @param string      $path
		 * @param string|null $parameter
		 * @return string
		 */
		protected function readCounter(string $path, string $parameter = null): string
		{
			if (file_exists($path)) {
				return parent::readCounter($path, $parameter);
			}

			// probably admin!
			if (false === ($dh = opendir('/proc'))) {
				return '';
			}

			if ($parameter === 'proclimit') {
				return (string)\Opcenter\System\Sysctl::read('kernel.pid_max');
			}
			$cnt = 0;
			while (false !== ($entry = readdir($dh))) {
				if (!is_dir("/proc/{$entry}")) {
					continue;
				}
				if (\strlen($entry) === strspn($entry, '0123456789')) {
					$cnt++;
				}
			}
			closedir($dh);
			return (string)$cnt;
		}


	}
