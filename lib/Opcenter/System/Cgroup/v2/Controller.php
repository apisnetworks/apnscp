<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, March 2023
 */

namespace Opcenter\System\Cgroup\v2;

use Opcenter\System\Cgroup\Attributes;
use Opcenter\System\Cgroup\BaseController;
use Opcenter\System\Cgroup\Contracts\ControllerAttribute;
use Opcenter\System\Cgroup\v2\Contracts\NestedKey;
use Opcenter\System\Cgroup\v2\Controllers\Freezer;

abstract class Controller extends BaseController
{

	protected const V2_REMAPS = [];
	protected const V2_METRIC_LABELS = [];

	/**
	 * Read cgroup counter from path
	 *
	 * @param string      $path
	 * @param string|null $parameter
	 * @return string
	 */
	protected function readCounter(string $path, string $parameter = null): string
	{
		if (isset(static::V2_METRIC_LABELS[$parameter])) {
			return $this->readCounterLabel($path, static::V2_METRIC_LABELS[$parameter]);
		}

		return parent::readCounter($path, $parameter);
	}

	public function create(): bool
	{
		if ($this->group->delegated() && !$this->exists()) {
			$parent = $this->group->delegator();
			file_put_contents((new static($parent, []))->getPath() . '/cgroup.subtree_control', "+{$this->getName()}", FILE_APPEND);
		}
		return parent::create();
	}


	/**
	 * Get controller path
	 *
	 * @return string
	 */
	public function getPath(): string
	{
		return CGROUP_HOME . '/' . $this->group;
	}

	public function exists(): bool
	{
		if (!file_exists($path = $this->getPath() . '/cgroup.controllers')) {
			return false;
		}

		$controller = strtok(file_get_contents($path), " \n");
		do {
			if ($controller === $this->getName()) {
				return true;
			}
		} while ($controller = strtok(" \n"));
		return false;
	}

	protected function getCounter(string $metric): string
	{
		$counter = parent::getCounter($metric);

		return static::V2_REMAPS[$counter] ?? $counter;
	}

	public function tasks(): array
	{

		if (!file_exists($path = $this->getPath() . '/cgroup.procs')) {
			return [];
		}

		return array_map('intval', file($path, FILE_IGNORE_NEW_LINES));
	}

	public function charge(int $pid): bool
	{
		return file_put_contents($this->getPath() . '/cgroup.procs', $pid, FILE_APPEND) > 0;
	}

	public function delete(): bool
	{
		return file_put_contents($this->getPath() . '/cgroup.subtree_control', '-' . $this->getName(), FILE_APPEND) > 0;
	}

	public function createAttribute(string $parameter, mixed $value): Attributes\BaseAttribute
	{
		$parameter = strtok($parameter, '=');
		$field = strtok(' ');
		$attr = parent::createAttribute($parameter, $value);
		if (false !== $field && $attr instanceof NestedKey) {
			$attr->setSubattribute($field, $value);
		}

		return $attr;
	}

	public function setAttribute(ControllerAttribute $attr): bool
	{
		$this->dirty = true;
		if ($attr instanceof NestedKey && isset($this->attributes[(string)$attr])) {
			$attr = $this->attributes[(string)$attr]->mergeSubattributes($attr);
		}

		return parent::setAttribute($attr);
	}
}