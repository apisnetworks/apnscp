<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, April 2024
	 */

	namespace Opcenter\System\Cgroup\v2\Controllers;

	use Opcenter\System\Cgroup\v2\Controller;

	class Cgroup extends Controller
	{
		protected const SERVICE_ATTRIBUTES = [
			'descendents' => 'cgroup.stat=nr_descendents',
			'dying'       => 'cgroup.stat=nr_dying_descendents',
		];

		public function stats(): array
		{
			return [];
		}

		public function build(): string
		{
			return '';
		}

		public function exists(): bool
		{
			return file_exists($this->getPath() . '/cgroup.controllers');
		}
	}