<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, March 2023
	 */

	namespace Opcenter\System\Cgroup\v2\Controllers;

	use Opcenter\System\Kernel;

	class Memory extends \Opcenter\System\Cgroup\v1\Controllers\Memory
	{
		protected const SERVICE_ATTRIBUTES = [
			'memory' => 'memory.max',
		];

		protected const V2_REMAPS = [
			'memory.usage_in_bytes' => 'memory.stat',
			'memory.limit_in_bytes' => 'memory.max',
			'memory.failcnt'        => 'memory.events',
			'memory.max_usage_in_bytes' => 'memory.peak'
		];

		protected const V2_METRIC_LABELS = [
			'used' => 'file',

		];

		public function read(string $parameter, string $field = null): ?string
		{
			if ($field === 'peak' && !$this->kernelSupportsPeakCounter()) {
				// via commit 8e20d4b3
				return null;
			}

			if (!file_exists($this->getPath() . '/' . $parameter)) {
				switch ($field) {
					case 'used':
						// v5.0+
						$stats = \Opcenter\System\Memory::stats();
						return (string)(1024 * ($stats['memtotal'] - $stats['memavailable']));
					case 'limit':
					case 'memory':
					// KB
					return (string)\Opcenter\System\Memory::stats()['memtotal'];
					case 'oom':
						return parent::read('system.slice/' . $parameter, $field);
				}

			}

			return parent::read($parameter, $field);
		}

		public function reset(): bool
		{
			if (!$this->kernelSupportsPeakCounter()) {
				return false;
			}

			return $this->write("memory.peak", 0);
		}

		private function kernelSupportsPeakCounter(): bool
		{
			return version_compare(Kernel::version(), '5.19.0', '>=');
		}
	}