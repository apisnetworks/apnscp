<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2024
	 */

	namespace Opcenter\System\Cgroup\v2\Attributes\Cgroup;

	use Opcenter\System\Cgroup\v2\Attributes\NestedKeyAttribute;

	class Stat extends NestedKeyAttribute
	{

	}