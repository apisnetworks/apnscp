<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2023
	 */

	namespace Opcenter\System\Cgroup\v2\Attributes\Io;

	use Opcenter\Filesystem\Mount;
	use Opcenter\System\Cgroup\v2\Attributes\NestedKeyAttribute;
	use Opcenter\System\Cgroup\v2\Controller;

	class Max extends NestedKeyAttribute
	{
		use \ContextableTrait;
		const UNLIMITED = 'max';
		protected $path = FILESYSTEM_VIRTBASE;

		public function __construct($value, Controller $controller)
		{
			parent::__construct($value ?? self::UNLIMITED, $controller);
		}

		public function getValue()
		{
			$path = $this->getAuthContext()->domain_shadow_path() ?: FILESYSTEM_VIRTBASE;
			$majmin = Mount::getPhyBlockFromPath($path);

			return $majmin . ' ' .
				implode(
					' ',
					array_key_map(static fn ($k, $v) => "{$k}=" . $v ?: static::UNLIMITED, $this->getSubattributes()));
		}

		public function setSubattribute(string $attr, mixed $val): bool
		{
			// @TODO moving from counter read
			if ($attr === 'wbps' || $attr === 'rbps') {
				$val *= 1024**2;
			}
			return parent::setSubattribute($attr, $val ?: static::UNLIMITED);
		}
	}