<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, July 2023
	 */


	namespace Opcenter\System\Cgroup\v2\Attributes\Freezer;

	use Opcenter\System\Cgroup\Controller;

	class State extends \Opcenter\System\Cgroup\Attributes\Freezer\State
	{
		const STATE_FROZEN = 1;
		const STATE_THAWED = 0;
		const STATE_FREEZING = 1;

		const STATES = [
			'FREEZING',
			'FROZEN',
			'THAWED'
		];

		public function __construct($value, Controller $controller)
		{
			if (!is_int($value)) {
				parent::__construct($value, $controller);
			}

			$value = $value === 'FREEZING' || $value === 'FROZEN' ? 1 : 0;
			if ($value !== 0 && $value !== 1) {
				fatal("Unknown %(controller)s cgroup value `%(var)s'", [
					'controller' => strtolower(static::getBaseClassName(static::class)),
					'var'        => $value
				]);
			}
			$this->value = $value;
			$this->controller = $controller;
		}

		protected function getAttributePath(): string
		{
			return $this->controller->getPath() . '/cgroup.freeze';
		}
	}