<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, July 2023
	 */


	namespace Opcenter\System\Cgroup\Attributes\Freezer;

	use Opcenter\System\Cgroup\Attributes\BaseAttribute;
	use Opcenter\System\Cgroup\Controller;

	class State extends BaseAttribute
	{
		const STATE_FROZEN = 'FROZEN';
		const STATE_THAWED = 'THAWED';
		const STATE_FREEZING = 'FREEZING';
		const STATES = [
			self::STATE_FREEZING,
			self::STATE_FROZEN,
			self::STATE_THAWED
		];

		public function __construct($value, Controller $controller)
		{
			if ($value && !in_array($value, static::STATES, true)) {
				fatal("Unknown %(controller)s cgroup value `%(var)s'", [
					'controller' => strtolower(static::getBaseClassName(static::class)),
					'var' => $value
				]);
			}
			parent::__construct($value, $controller);
			$this->value = $value;
		}

		public function frozen(): bool
		{
			$state = $this->read();
			return $state == static::STATE_FROZEN || $state == static::STATE_FREEZING;
		}

		protected function getAttributePath(): string
		{
			return $this->controller->getPath() . '/freezer.state';
		}
	}