<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, July 2023
	 */

	namespace Opcenter\System\Cgroup\Attributes\Memory;

	use Opcenter\System\Cgroup\Attributes\BaseAttribute;
	use Opcenter\System\Cgroup\Controller;

	class LimitInBytes extends BaseAttribute
	{
		const UNLIMITED = -1;

		public function __construct($value, Controller $controller)
		{
			if ($value !== null && static::UNLIMITED !== $value) {
				$value *= 1024 ** 2;
			}

			parent::__construct($value ?? self::UNLIMITED, $controller);
		}

		public function hasLimit(): bool
		{
			if (static::class !== self::class) {
				return parent::hasLimit();
			}
			return $this->read() != (PHP_INT_MAX & ~0xFFF);
		}
	}