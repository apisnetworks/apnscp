<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, July 2023
	 */

	namespace Opcenter\System\Cgroup\Attributes;

	use Opcenter\System\Cgroup\Contracts\ControllerAttribute;
	use Opcenter\System\Cgroup\Controller;

	abstract class BaseAttribute implements ControllerAttribute
	{
		use \NamespaceUtilitiesTrait;

		// value that must be specified for unlimited, if null the attribute or controller is ignored
		const UNLIMITED = 0;
		protected $value;
		protected $controller;

		public function __construct($value, Controller $controller)
		{
			$this->controller = $controller;
			$this->value = $value;
		}

		public function __toString(): string
		{
			$parts = [substr($ns = static::getNamespace(), strrpos($ns, '\\')+1), static::getBaseClassName()];
			return strtolower($parts[0]) . (isset($parts[1]) ? '.' . snake_case($parts[1]) : '');
		}

		/**
		 * Activate configuration change
		 *
		 * @return bool
		 */
		public function activate(): bool
		{
			$path = $this->getAttributePath();
			$value = $this->getValue();
			if (null === $value && static::UNLIMITED !== $this->read()) {
				// null consistently deactivates in API
				// check converted value against cgroup representation
				debug('Skipping cgroup %s => %s', [(string)$this->controller, $path]);
				$this->deactivate();

				return false;
			}

			return $this->write($path, $value);
		}

		protected function getAttributePath(): string
		{
			return $this->controller->getPath() . '/' . (string)$this;
		}

		protected function write(string $path, $value): bool
		{
			return file_put_contents($path, (string)$value) > 0;
		}

		public function read()
		{
			if (!file_exists($this->getAttributePath())) {
				return null;
			}
			return trim(file_get_contents($this->getAttributePath()));
		}

		public function getValue()
		{
			return $this->value;
		}

		public function deactivate(): bool
		{
			$path = $this->getAttributePath();
			return file_put_contents($path, static::UNLIMITED) > 0;
		}

		/**
		 * Controller value has a limit
		 *
		 * @return bool
		 */
		public function hasLimit(): bool
		{
			return $this->read() != static::UNLIMITED;
		}
	}