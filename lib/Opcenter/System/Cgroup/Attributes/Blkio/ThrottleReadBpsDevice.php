<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, July 2023
	 */

	namespace Opcenter\System\Cgroup\Attributes\Blkio;

	use Opcenter\Filesystem\Mount;
	use Opcenter\System\Cgroup\Attributes\BaseAttribute;

	class ThrottleReadBpsDevice extends BaseAttribute
	{
		use \ContextableTrait;

		const UNLIMITED = '';

		protected $path = FILESYSTEM_VIRTBASE;

		public function getValue()
		{
			$path = $this->getAuthContext()->domain_shadow_path() ?: FILESYSTEM_VIRTBASE;
			$majmin = Mount::getPhyBlockFromPath($path);
			$val = (int)parent::getValue();
			if (static::class === ThrottleReadBpsDevice::class || static::class === ThrottleWriteBpsDevice::class) {
				$val *= 1024*1024;
			}
			if (!$val) {
				return null;
			}
			return $majmin . ' ' . $val;
		}

		public function __toString(): string
		{
			$str = parent::__toString();
			if (false === ($pos = strpos($str, '_'))) {
				return $str;
			}
			return substr_replace($str, '.', $pos, 1);
		}
	}
