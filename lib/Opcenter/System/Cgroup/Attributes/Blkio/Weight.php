<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, July 2023
	 */
	namespace Opcenter\System\Cgroup\Attributes\Blkio;

	use Opcenter\Filesystem\Mount;

	class Weight extends ThrottleReadBpsDevice
	{
		const ELEVATORS = [
			// elevators that support some form of weighted IO
			'cfg', 'bfq'
		];

		public function getValue()
		{
			return $this->value;
		}

		/**
		 * Elevator-friendly notation
		 *
		 * @return string
		 */
		public function __toString(): string
		{
			$elevator = $this->getElevator($this->path);
			if ($elevator === 'bfq') {
				return 'blkio.bfq.weight';
			}
			return 'blkio.weight';
		}

		/**
		 * cgroup attribute may be used
		 *
		 * @param string $path
		 * @return bool
		 */
		public function canUse(string $path = FILESYSTEM_VIRTBASE): bool
		{
			$this->path = $path;
			if ( !($elevator = $this->getElevator($path)) ) {
				return false;
			}
			return \in_array($elevator, self::ELEVATORS, true);
		}

		/**
		 * Get elevator from path
		 *
		 * @param string $path
		 * @return bool|string
		 */
		private function getElevator(string $path)
		{
			static $pathCache;

			if (!isset($pathCache[$path])) {
				if (!($device = Mount::getDeviceFromPath($path))) {
					return error('Failed to get device from %s', $path);
				}
				if (!($block = Mount::getBlockFromDevice(implode(':', $device)))) {
					return error('Failed to get block from major:minor %s', implode(':', $device));
				}
				$pathCache[$path] = Mount::getScheduler($block);
			}

			return $pathCache[$path];
		}
	}