<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup;

	use Opcenter\System\Cgroup;
	use Opcenter\System\Cgroup\Attributes\Freezer\State;
	use Opcenter\System\Cgroup\Contracts\ControllerAttribute;

	abstract class BaseController implements Cgroup\Contracts\Renderable
	{
		use \NamespaceUtilitiesTrait;
		// @var array account meta <=> controller attributes
		protected const SERVICE_ATTRIBUTES = [];
		// remapping metric storage ns
		protected const METRIC_NS_OVERRIDE = null;
		// @var string $type
		protected $type;
		protected $permissions;
		// @var ControllerAttribute $attributes
		protected $attributes = [];
		protected Group $group;
		protected bool $dirty = false;

		// @var array metrics logged by crontab::_cron()
		public const LOGGABLE_METRICS = [];

		protected function __construct(Group $group, array $permissions)
		{
			$this->group = $group;
			$this->type = strtolower(static::getBaseClassName());
			$this->permissions = new Permissions($permissions);
		}

		/**
		 * Get controller type
		 *
		 * @return string
		 */
		public function getName(): string
		{
			return $this->type;
		}

		public function getMetricSymbol(string $attribute): string
		{
			return 'c-' . (static::METRIC_NS_OVERRIDE ?? $this->getName()) . '-' . $attribute;
		}

		public static function make(Group $group, ?string $type, array $permissions = []): Controller
		{
			if (null === $type && ($type = static::getBaseClassName()) === static::getBaseClassName(self::class)) {
				fatal('$type must be specified when calling %s directly', self::class);
			}
			$controllerName = $type;
			try {
				$controllerName = static::getBaseClassName($type);
			} catch (\ReflectionException $e) {}

			$c = (Cgroup::version() === 2 ? v2\Controller::class : v1\Controller::class) . 's\\' . ucwords($controllerName);

			return new $c($group, array_merge($group->getPermissions(), $permissions));
		}

		/**
		 * Reset controller counters
		 *
		 * @return bool
		 */
		public function reset(): bool
		{
			return true;
		}

		/**
		 * Controller may be bound at startup
		 *
		 * @return bool
		 */
		public function immmediateBinding(): bool
		{
			return true;
		}

		public function __destruct()
		{
			if ($this->dirty) {
				$this->create();
			}
		}

		public function create(): bool
		{
			$this->dirty = false;
			$args = $this->permissions->get();
			$args['controller'] = $this->getName();
			$args['group'] = (string)$this->group;
			if (Cgroup::version() !== 2 || !is_dir($this->getPath())) {
				// v2 has single writer
				$ret = \Util_Process::exec([
					'cgcreate', '--dperm=%(admin.dperm)o', '-a', '%(admin.uid)s:%(admin.gid)s', '--fperm=%(admin.fperm)o', '--tperm=%(task.fperm)o',
					'-g', '%(controller)s:%(group)s', '-t', '%(task.uid)s:%(task.gid)s'], $args);
				if (!$ret['success']) {
					return error("failed to create cgroup `%s:%s': %s", $args['controller'], $args['group'],
						$ret['stderr']);
				}
			}

			foreach ($this->getAttributes() as $attr) {
				$attr->activate();
			}

			return true;
		}

		/**
		 * Delete controller from group
		 *
		 * @return bool
		 */
		public function delete(): bool
		{
			return array_get(\Util_Process::exec(['cgdelete', $this->getName() . ':' . $this->group]), 'success', false);
		}

		/**
		 * Reset all controller statistics to 0
		 *
		 * @return bool
		 */
		public function reinitialize(): bool
		{
			if (!(string)$this->group) {
				return debug("Cannot reinitialize root controller");
			}

			$procs = $this->processes();
			$controller = Controller::make($this->group, 'freezer');
			if ($controller->exists()) {
				$controller->createAttribute('state', State::STATE_FROZEN)->activate();
				defer($_, static fn() => $controller->createAttribute('state', State::STATE_THAWED)->activate());
			} else {
				warn("freezer controller missing - reassigning processes real-time");
			}

			$this->delete();
			$config = Cgroup::configurationFromGroup($this->group);

			if (file_exists($config)) {
				\Util_Process::exec(['cgconfigparser', '-l' . $config]);
			} else {
				warn("Missing configuration `%s' - instantiating empty controller", $config);
				$this->create();
			}

			$ret = true;
			foreach ($procs as $p) {
				$ret &= $this->charge($p);
			}

			return (bool)($ret ?: warn("Failed to reassign all pids"));
		}

		/**
		 * Get configured attributes
		 *
		 * @return ControllerAttribute[]
		 */
		public function getAttributes(): array
		{
			return $this->attributes;
		}

		abstract public function exists(): bool;

		/**
		 * Log resources for pid
		 *
		 * @param int $pid
		 * @return bool
		 */
		abstract public function charge(int $pid): bool;

		/**
		 * Get controller path
		 *
		 * @return string
		 */
		abstract public function getPath(): string;

		/**
		 * Import account meta into cgroup vars
		 *
		 * @param \Auth_Info_User $afi
		 * @return int
		 */
		public function import(\Auth_Info_User $ctx = null): int
		{
			$ctx = $ctx ?? \Auth::profile();
			$afi = \apnscpFunctionInterceptor::factory($ctx);
			$cnt = 0;

			foreach (array_intersect_key($afi->cgroup_get_limits(), static::SERVICE_ATTRIBUTES) as $cgparam => $value) {
				$name = static::SERVICE_ATTRIBUTES[$cgparam];
				$this->setAttribute($this->createAttribute($name, $value));
				$cnt++;
			}

			return $cnt;
		}

		public function createAttribute(string $parameter, mixed $value): Attributes\BaseAttribute
		{
			if (static::class !== self::class && !str_contains($parameter, '.')) {
				$parameter = strtolower(static::getBaseClassName()) . ".$parameter";
			}
			$attribute = $this->makeAttribute($parameter);

			return new $attribute($value, $this);
		}

		/**
		 * Convert a service value into cgroup attribute
		 *
		 * @param string $attribute
		 * @return Controller
		 */
		public function createAttributeFromServiceParameter(string $parameter, mixed $val = null): Attributes\BaseAttribute
		{
			$file = static::SERVICE_ATTRIBUTES[$parameter];
			$c = $this->makeAttribute($file);
			return new $c($val ?? $this->read($file), $this);
		}

		/**
		 * Controller has cgroup service parameter
		 *
		 * @param string $parameter
		 * @return bool
		 */
		public function hasParameter(string $parameter): bool
		{
			return isset(static::SERVICE_ATTRIBUTES[$parameter]);
		}

		/**
		 * Get class representation of attribute
		 *
		 * @param string $param
		 * @return string
		 */
		private function makeAttribute(string $param)
		{
			[$controller, $param] = explode('.', $param, 2);

			$class = implode('\\',
				[
					substr($tmp = self::getNamespace(static::class), 0, strrpos($tmp, '\\')),
					'Attributes',
					ucwords($controller),
					studly_case(str_replace('.', '-', $param))
				]);
			// @TODO move attributes to global/variant controllers
			return class_exists($class) ? $class : implode('\\', [self::getNamespace(__CLASS__), 'Attributes',
				ucwords($controller), studly_case(str_replace('.', '-', $param))]);
		}

		public function setAttribute(ControllerAttribute $attr): bool
		{
			$this->dirty = true;
			$this->attributes[(string)$attr] = $attr;
			return true;
		}

		public function __toString()
		{
			return $this->getName();
		}

		public function build(): string
		{
			$config = $this->getName() . ' { ';

			foreach ($this->getAttributes() as $attribute) {
				if (null === ($value = $attribute->getValue())) {
					continue;
				}
				if (\is_string($value) && false !== strpos($value, ' ')) {
					$value = '"' . str_replace('"', '\\"', $value) . '"';
				}
				$attribute = strtok((string)$attribute, '=:');
				$config .= "{$attribute}=" . $value . '; ';
			}

			return $config . '} ';
		}


		/**
		 * Get logged cgroup metrics
		 *
		 * @return array
		 */
		public function getMetrics(): array
		{
			return array_keys(static::LOGGABLE_METRICS);
		}

		/**
		 * Get metric type
		 *
		 * @param string $metric
		 * @return string monotonic or value
		 */
		public function getMetricDataType(string $metric): string
		{
			return static::LOGGABLE_METRICS[$metric]['type'] ?? 'value';
		}

		/**
		 * Get metric label
		 *
		 * @param string $metric metric name
		 * @return string label
		 */
		public function getMetricLabel(string $metric): string
		{
			return static::LOGGABLE_METRICS[$metric]['label'] ?? "Resource $metric";
		}

		/**
		 * Get metric counter cgroup parameter
		 *
		 * @param string $metric
		 * @return string|null
		 */
		protected function getCounter(string $metric): string
		{
			$counter = static::LOGGABLE_METRICS[$metric]['counter'] ?? (static::SERVICE_ATTRIBUTES[$metric] ?? null);
			if ($counter === null) {
				fatal("Unknown counter for metric `%s' in %s", $metric, $this->getName());
			}

			return $counter;
		}

		/**
		 * Read metrics from controller
		 *
		 * @param array $params
		 * @return array
		 */
		public function readMetrics(array $params): array
		{
			$read = [];

			if (!is_dir($this->getPath())) {
				return [];
			}
			foreach ($params as $p) {
				$read[$p] = $this->normalizeMetricForStorage($p, $this->read($this->getCounter($p), $p));
			}
			return $read;
		}

		protected function normalizeMetricForStorage(string $metric, ?string $val): ?int
		{
			return null === $val ? null : (int)$val;
		}

		/**
		 * Controller userland statistics
		 *
		 * @return array
		 */
		abstract public function stats(): array;

		/**
		 * Direct counter read
		 *
		 * @param string      $path
		 * @return string
		 */
		protected function readCounter(string $path, string $parameter = null): string
		{
			return file_get_contents($path);
		}

		/**
		 * cgroup counter prefixed by named label
		 *
		 * Do not call directly, overridden by readCounter() as needed
		 *
		 * @param string $path
		 * @param string $label case-sensitive label name
		 * @return string|null
		 */
		protected function readCounterLabel(string $path, string $label): ?string
		{
			$content = file_get_contents($path);
			if (false === ($offset = strpos($content, "{$label} "))) {
				return null;
			}

			$offset += strlen("{$label} ");
			return substr($content, $offset, strpos($content, "\n", $offset) - $offset);
		}

		/**
		 * Return all labeled counter values
		 *
		 * @param string        $path
		 * @param \Closure|null $filter
		 * @return array
		 */
		protected function readAllCounterLabels(string $path): array
		{
			$lines = file($this->getPath() . "/{$path}", FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
			return array_build($lines, static fn($line) => explode(" ", $line, 2));
		}

		public function read(string $parameter, string $field = null): ?string
		{
			return $this->readCounter($this->getPath() . '/' . $parameter, $field);
		}

		/**
		 * Task IDs (process ID + thread IDs) monitored by controller
		 * @return array
		 */
		abstract public function tasks(): array;

		public function processes(): array
		{
			if (!file_exists($path = $this->getPath() . '/cgroup.procs')) {
				return [];
			}

			return array_map(intval(...), file($path, FILE_IGNORE_NEW_LINES));
		}

		/**
		 * Write arbitrary value to controller
		 *
		 * @param string $parameter
		 * @param        $value
		 * @return bool
		 */
		public function write(string $parameter, $value): bool
		{
			return false !== file_put_contents($this->getPath() . '/' . $parameter, $value);
		}
	}