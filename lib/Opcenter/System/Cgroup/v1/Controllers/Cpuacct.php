<?php declare(strict_types=1);
	/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, May 2023
 */

	namespace Opcenter\System\Cgroup\v1\Controllers;

	class Cpuacct extends \Opcenter\System\Cgroup\Controllers\Cpuacct
	{
		public function stats(): array
		{
			$stats = array(
				'used'     => null,
				'system'   => null,
				'free'     => null,
				'limit'    => null,
				'user'     => null,
				'procs'    => [],
				'maxprocs' => null,
				'dynamic'  => false
			);
			if (!$this->exists()) {
				return $stats;
			}

			// nano to seconds
			$stats['used'] = (float)$this->read('cpuacct.usage');
			$tmp = file($this->getPath() . '/cpuacct.stat', FILE_IGNORE_NEW_LINES);
			$stats['user'] = (float)substr($tmp[0], strpos($tmp[0], ' ') + 1) / CPU_CLK_TCK;
			$stats['system'] = (float)substr($tmp[1], strpos($tmp[1], ' ') + 1) / CPU_CLK_TCK;
			// CPU counters are delta counters, which requires knowing the previous val
			$stats['free'] = null;
			$stats['procs'] = $this->tasks();

			return $stats;
		}

		/**
		 *
		 * @param string      $path
		 * @param string|null $parameter
		 * @return string
		 */
		protected function readCounter(string $path, string $parameter = null): string
		{
			$ext = substr($path, -6);
			if ($ext === '.ustat' || $ext === '.sstat') {
				$path = \dirname($path) . '/cpuacct.stat';
			}
			$val = parent::readCounter($path, $parameter);
			if ($ext === '.usage') {
				// convert nano to seconds
				$val = (int)$val / 1e9;
			} else {
				$ctr = strtok($val, ' ');
				do {
					$val = strtok("\n");
					if (($ext === '.ustat' && $ctr === 'user') ||
						($ext === '.sstat' && $ctr === 'system')) {
						break;
					}
				} while (false !== ($ctr = strtok(' ')));
				$val = (int)$val / CPU_CLK_TCK;
			}
			return (string)$val;
		}


		protected function normalizeMetricForStorage(string $metric, ?string $val): ?int
		{
			return (int)((float)$val * 100);
		}
	}
