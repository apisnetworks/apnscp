<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, May 2023
 */

	namespace Opcenter\System\Cgroup\v1\Controllers;

	use Opcenter\System\Kernel;
	use Opcenter\Versioning;

	class Blkio extends \Opcenter\System\Cgroup\Controllers\Blkio
	{
		static bool $buggedKernel;

		/**
		 * {@inheritDoc}
		 */
		public function reset(): bool
		{
			if (!isset(self::$buggedKernel)) {
				$kernelVersion = Kernel::version();
				self::$buggedKernel = Versioning::compare($kernelVersion, '4.18.0-425', '>=') &&
					Versioning::compare($kernelVersion, '4.18.0-514', '<');
			}
			if (self::$buggedKernel) {
				debug("Recreating blkio controller due to bugged kernel");
				return $this->reinitialize();
			}

			return file_put_contents($this->getPath() . '/blkio.reset_stats', '1') > 0;
		}

		protected function statsWrapper(array $stats): array
		{
			$stats = [];
			foreach (['iops' => 'io_serviced', 'bw' => 'io_service_bytes'] as $key => $counter) {
				$raw = file($this->getElevatorNamespacedPath($counter),
					FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
				$stats += array_build($raw,
					static function ($k, $v) use ($key) {
						strtok($v, ' ');
						if (false === strpos($v, ':')) {
							return null;
						}
						$statKey = strtolower(strtok(' '));
						$statVal = strtok(' ');

						return [$key . '-' . $statKey, (int)$statVal];
					});
			}
			$stats['iops-rlimit'] = (int)strstr($this->read('blkio.throttle.read_iops_device'), ' ');
			$stats['iops-wlimit'] = (int)strstr($this->read('blkio.throttle.write_iops_device'), ' ');
			$stats['bw-rlimit'] = (int)strstr($this->read('blkio.throttle.read_bps_device'), ' ');
			$stats['bw-wlimit'] = (int)strstr($this->read('blkio.throttle.write_bps_device'), ' ');


			return $stats;
		}


	}
