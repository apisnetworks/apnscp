<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup\Contracts;

	use Opcenter\System\Cgroup\Controller;

	interface ControllerAttribute
	{
		public function __construct($value, Controller $controller);

		public function __toString(): string;

		/**
		 * Get attribute value
		 *
		 * @return mixed|null null to remove attribute setting
		 */
		public function getValue();

		public function read();

		public function activate(): bool;

		public function deactivate(): bool;
	}