<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2020
 */


namespace Opcenter\System\Cgroup;

use Opcenter\System\Cgroup;
use Opcenter\System\Cgroup\Contracts\ControllerAttribute;

/**
 * Class VolatileAttribute
 *
 * A cgroup override that persists during lifetime
 *
 * @package Opcenter\System\Cgroup
 */
class VolatileAttribute {
	use \ContextableTrait;
	use \NamespaceUtilitiesTrait;

	/**
	 * @var ControllerAttribute[]
	 */
	protected $saved = [];

	protected function __construct() {

	}

	public function __destruct() {
		foreach ($this->saved as $attribute) {
			$attribute->activate();
		}
	}

	/**
	 * Remove limit briefly for resource
	 *
	 * @param ControllerAttribute $resource
	 */
	public function resource(ControllerAttribute $resource): void
	{
		$c = clone $resource;
		$rfxn = new \ReflectionProperty($c, 'value');
		$rfxn->setAccessible(true);
		$rfxn->setValue($c, $c->read());
		$this->saved[] = $c;
		$resource->deactivate();
	}

	/**
	 * Create volatile resource from mounted attribute
	 *
	 * @param string $attribute
	 * @param int    $value optional value
	 * @return ControllerAttribute
	 */
	public function fromAttribute(string $attribute, int $value = 0): ControllerAttribute
	{
		$abstract = Cgroup::resolveParameterController($attribute);
		/**
		 * @var Controller $controller
		 */
		$controller = $abstract::make(new Group($this->getAuthContext()->site), $attribute);

		return $controller->createAttributeFromServiceParameter($attribute);
	}
}