<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
	 */

	declare(strict_types=1);


	namespace Opcenter\System;

	class Memory
	{

		/**
		 * Get system memory information
		 *
		 * @return array memory values in KB
		 *
		 * Sample response:
		 * Array (
		 * [memtotal] => 4109456
		 * [memfree] => 1855804
		 * [memavailable] => 2147596
		 * [buffers] => 133408
		 * [cached] => 284760
		 * [swapcached] => 15768
		 * [active] => 1020180
		 * [inactive] => 991524
		 * [active(anon)] => 679268
		 * [inactive(anon)] => 927192
		 * [active(file)] => 340912
		 * [inactive(file)] => 64332
		 * [unevictable] => 16436
		 * [mlocked] => 16436
		 * [swaptotal] => 2097148
		 * [swapfree] => 1792948
		 * [dirty] => 1256
		 */
		public static function stats(): array
		{
			return array_build(file('/proc/meminfo', FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES),
				static function ($key, $line) {
					return [strtolower(strtok($line, ':')), (int)strtok(' ')];
				}
			);

		}
	}