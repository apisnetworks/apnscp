<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, March 2024
 */


namespace Opcenter\System\Systemd;

class SyslogGenerator
{
	use \ContextableTrait;

	public function __construct()
	{ }

	/**
	 * syslog identifier
	 *
	 * @param string      $service service name
	 * @param string|null $suffix  service suffix
	 * @return string
	 */
	public function name(string $service, string $suffix = null): string
	{
		return $service . '/' . $this->getAuthContext()->site . ($suffix ? '-' . $suffix : '');
	}
}