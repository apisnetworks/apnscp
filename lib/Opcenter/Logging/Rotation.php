<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2023
	 */

	namespace Opcenter\Logging;

	use Opcenter\Contracts\Virtualizable;
	use Opcenter\Contracts\VirtualizedContextable;
	use Opcenter\Logging\Rotation\ConfigFile;

	class Rotation implements VirtualizedContextable, Virtualizable
	{
		use \ContextableTrait;

		const LOGROTATE_CONF = '/etc/logrotate.conf';

		private bool $dirty = false;

		public function __construct(private string $root = '/')
		{
		}

		public static function bindTo(string $root = '/')
		{
			return new static($root);
		}

		private function configPath(string $file = self::LOGROTATE_CONF): string
		{
			return $this->root . $file;
		}

		public function read(string $file = self::LOGROTATE_CONF): ConfigFile
		{
			return new ConfigFile($this->configPath($file));
		}
	}