<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2023
	 */

	namespace Opcenter\Logging\Rotation;

	use Opcenter\Map\Textfile;

	class ConfigSection
	{

		const LOGROTATE_WHITELIST = [
			'compress',
			'compressext',
			'copy',
			'copytruncate',
			'create',
			'daily',
			'dateext',
			'dateformat',
			'delaycompress',
			'ifempty',
			'mail',
			'mailfirst',
			'maillast',
			'maxage',
			'minsize',
			'missingok',
			'monthly',
			'nocompress',
			'nocopy',
			'nocopytruncate',
			'nocreate',
			'nodelaycompress',
			'nodateext',
			'nomail',
			'nomissingok',
			'noolddir',
			'nosharedscripts',
			'noshred',
			'notifempty',
			'rotate',
			'size',
			'sharedscripts',
			'shred',
			'shredcycles',
			'start',
			'weekly',
			'yearly'
		];

		const EXCLUSIVE_DIRECTIVES = [
			['weekly', 'daily', 'monthly', 'size'],
		];

		private Textfile $map;
		/**
		 * @var array|false|string[]|null
		 */
		private array|null $logPattern;
		private array $computed;

		public function __construct(?string $log, string $lines)
		{
			$this->logPattern = $log ? \preg_split("!\s+!", $log) : null;
			$this->map = Textfile::fromString($lines);
			$this->computed = array_fill_keys(static::LOGROTATE_WHITELIST, 1);
		}

		public function set(array|string $parameter, mixed $value = null): bool
		{
			if (!is_array($parameter)) {
				$parameter = [$parameter => $value];
			}

			$banned = array_diff_key($parameter, $this->computed);
			if ($banned) {
				return error("Unknown/unrecognized directives found: %(directives)s",
					['directives' => implode(', ', array_keys($banned))]);
			}

			foreach ($parameter as $k => $v) {
				if (str_starts_with($k, 'no') && isset($this->map[$inverse = substr($k, 2)])) {
					info("Unsetting %(directive)s", ['directive' => $inverse]);
					unset($this->map[$inverse]);
				}

				foreach (self::EXCLUSIVE_DIRECTIVES as $set) {
					if (in_array($k, $set, true)) {
						foreach ($set as $remove) {
							if ($remove === $k) {
								continue;
							}
							unset($this->map[$remove]);
						}
					}
				}

				$this->map[$k] = $v;
			}

			return true;
		}

		/**
		 * Log scope for config section
		 *
		 */
		public function logName(): ?string
		{
			return $this->logPattern ? implode(" ", $this->logPattern) : null;
		}

		public function match(string $pattern): bool
		{
			return (bool)array_map(static fn ($x) => fnmatch($pattern, $x), $this->logPattern);
		}

		public function __toString(): string
		{
			$config = (string)$this->map;
			$merge = [];
			// shift include directives to end
			$config = preg_replace_callback(
				'!^\s*(include .+$)\R!m',
				static function (array $match) use (&$merge) {
					$merge[] = $match[1];
					return '';
				},
				$config
			);
			$config .= "\n" . implode('', $merge);
			if ($this->logPattern) {
				return $this->logName() . " {\n" . $config . "\n}";
			}

			return $config;
		}
	}