<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, October 2024
 */

namespace Opcenter\Http\Apache\Maps;

use Opcenter\Http\Apache;
use Opcenter\Http\Apache\Map;

class Tls extends Map {
	const DOMAIN_MAP = 'ssl';

	public static function open(string $mapFile = self::DOMAIN_MAP, string $mode = self::MODE_READ): Map
	{
		if (!str_starts_with($mapFile, '/')) {
			$mapFile = Apache::CONFIG_PATH . "/{$mapFile}";
		}
		return parent::open($mapFile, $mode);
	}

	public static function fromSiteDomainMap(string $infoPath, string $site, string $mode = self::MODE_READ): ?Map
	{
		return nerror("fromSiteDomainMap unsupported");
	}

	public function removeSite(int $site_id): void
	{
		$this->dirty = true;

		foreach ($this->lines as $key => $mode) {
			if (str_starts_with($key, "{$site_id}:")) {
				unset($this->lines[$key]);
			}
		}
	}
}