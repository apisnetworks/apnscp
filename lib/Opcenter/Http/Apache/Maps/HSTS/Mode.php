<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, October 2024
	 */

	namespace Opcenter\Http\Apache\Maps\HSTS;

	enum Mode: int
	{
		const AUTOWIRE_PREF = 'http.hsts';

		case disabled = 0;
		case enabled = 1;
		case subdomains = 2;
		case preload = 3;

		public static function fromPreference(\Preferences $pref): self
		{
			// allow clearing preference by nulling it
			$value = \Util_Conf::inferType(array_get($pref, self::AUTOWIRE_PREF) ?? HTTPD_HSTS_MODE);

			if (!is_int($value)) {
				$value = match($value) {
					self::disabled->name => 0,
					self::enabled->name  => 1,
					self::subdomains->name => 2,
					self::preload->name => 3,
					default => 0
				};

			}

			return self::from($value);
		}

		public function enabled(): bool
		{
			return $this !== self::disabled;
		}
	}