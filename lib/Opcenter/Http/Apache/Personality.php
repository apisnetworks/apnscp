<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Http\Apache;

	use Opcenter\Http\Apache;

	class Personality
	{
		/**
		 * Additional personality exists
		 *
		 * @param string $personality
		 * @return bool
		 */
		public static function personalityExists(string $personality): bool
		{
			return file_exists(static::getPersonalityPath($personality));
		}

		/**
		 * Get personality path
		 *
		 * @param string $personality
		 * @return string
		 */
		public static function getPersonalityPath(string $personality): string
		{
			return Apache::CONFIG_PATH . "/personalities/{$personality}";
		}
	}