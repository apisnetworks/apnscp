<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2020
	 */

	namespace Opcenter\Http\Php\Fpm;

	use Opcenter\Account\Enumerate;
	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Filesystem;
	use Opcenter\Http\Php\Fpm;
	use Opcenter\Php;
	use Opcenter\Versioning;

	class MultiPhp
	{

		const MULTIBASE = FILESYSTEM_SHARED . '/php/multiphp';

		// @var string system PHP-FPM
		const SYSTEM_BINARY = '/usr/sbin/php-fpm';

		/**
		 * List all PHP builds
		 * @return array
		 */
		public static function list(): array
		{
			if (posix_geteuid()) {
				$cfg = \apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login()))->scope_get('cp.bootstrapper');
			} else {
				$cfg = new Config();
			}
			return [
				Versioning::asMinor((string)$cfg['system_php_version']) => 'system'
			] + array_fill_keys(static::listNative(), 'native') + array_fill_keys(static::listPackaged(), 'package');
		}

		/**
		 * Get native builds
		 *
		 * @return array
		 */
		public static function listNative(): array
		{
			return self::readWrapper(static::getMultiPhpPath('native'));
		}

		/**
		 * Remove native version
		 *
		 * @param string $version
		 * @param bool   $force
		 * @return bool
		 */
		public static function removeNative(string $version, bool $force = false): bool
		{
			if (!in_array($version, static::listNative(), true)) {
				return error("Unknown native PHP version `%s'", $version);
			}

			if (!$force && ($sites = static::uses($version))) {
				return error("Cannot remove version `%(version)s'. Sites using this: %(sites)s", [
					'version' => $version,
					'sites' => implode(', ', $sites)
				]);
			}

			if (null === ($path = static::getVersionHome($version))) {
				return warn("multiPHP version `%(version)s' path `%(path)s' missing - already removed?", [
					'version' => $version,
					'path' => $path
				]);
			}

			// build directory gets purged during platform scrub
			Bootstrapper::run('php/build-from-source', ['remove_only' => true, 'php_version' => $version]);
			return Filesystem::rmdir($path);
		}

		/**
		 * List of sites that use PHP version
		 *
		 * @param string $version
		 * @return array
		 */
		public static function uses(string $version): array
		{
			if (false === strpos($version, '.')) {
				$version = Versioning::asMinor($version);
			}

			$sites = [];
			foreach (Enumerate::sites() as $site) {
				if (null === ($ctx = \Auth::nullableContext(null, $site))) {
					continue;
				}
				$policy = PoolPolicy::instantiateContexted($ctx);
				$pools = Fpm::getPoolsFromGroup($ctx->getAccount()->site);
				foreach ($pools as $pool) {
					if ($policy->getPoolVersion($pool) === $version) {
						$sites[] = $site;
						break;
					}
				}
			}

			return $sites;
		}

		/**
		 * PHP version is Remi
		 *
		 * @param string $version
		 * @return bool
		 */
		public static function isPackage(string $version): bool
		{
			return file_exists(
				static::getMultiPhpPath('package') . '/php' .
				str_replace('.', '', Versioning::asMinor($version))
			);
		}

		/**
		 * Get packaged builds
		 *
		 * @return array
		 */
		public static function listPackaged(): array
		{
			return self::readWrapper(static::getMultiPhpPath('package'));
		}

		private static function readWrapper(string $path): array
		{
			if (!file_exists($path)) {
				return [];
			}

			return Filesystem::readdir($path, static function ($f) use($path) {
				if (is_numeric($f)) {
					return file_exists("{$path}/{$f}/sbin/php-fpm") ? $f : null;
				}

				if (0 !== strncmp($f, 'php', 3)) {
					// garbage entry
					return null;
				}

				if (!file_exists("{$path}/{$f}/root/usr/sbin/php-fpm")) {
					return null;
				}

				$f = substr($f, 3);
				$splitOffset = 1;
				if ($f[0] === '1') {
					++$splitOffset;
				}

				return substr($f, 0, $splitOffset) . '.' . substr($f, $splitOffset);
			});
		}

		/**
		 * Get multiPHP location
		 *
		 * @param string $type native or package
		 * @return string
		 */
		protected static function getMultiPhpPath(string $type): string
		{
			if ($type === 'native') {
				return static::MULTIBASE . '/native';
			}

			return static::MULTIBASE . '/opt/remi';
		}

		/**
		 * Get PHP-FPM command from version
		 *
		 * @param string $version
		 * @return string|null path or null if version is not installed
		 */
		public static function fpmFromVersion(string $version): ?string
		{
			$versions = static::list();
			if (!array_key_exists($version, $versions)) {
				return null;
			}

			switch ($versions[$version]) {
				case 'system':
					return static::SYSTEM_BINARY;
				case 'native':
					$path = static::getVersionHome($version) . '/sbin/php-fpm';

					return file_exists($path) ? $path : null;
				case 'package':
					$verprefix = 'php' . str_replace('.', '', $version);
					$path = static::getVersionHome($version) . '/root/usr/sbin/php-fpm';

					return file_exists($path) ? '/usr/bin/scl enable ' . $verprefix . ' -- php-fpm' : null;
				default:
					fatal("Bogus versioning data `%s' for `%s'", $versions[$version], $version);
			}
		}

		/**
		 * Get PHP CLI from version
		 *
		 * @param string $version
		 * @return string|null path or null if version is not installed
		 */
		public static function cliFromVersion(string $version): ?string
		{
			$versions = static::list();
			if (!array_key_exists($version, $versions)) {
				return null;
			}

			switch ($versions[$version]) {
				case 'system':
					return Php::PHP_BIN;
				case 'native':
					$path = static::getVersionHome($version) . '/bin/php';

					return file_exists($path) ? $path : null;
				case 'package':
					$verprefix = 'php' . str_replace('.', '', $version);
					$path = static::getVersionHome($version) . '/root/usr/bin/php';

					return file_exists($path) ? '/usr/bin/scl enable ' . $verprefix . ' -- php' : null;
				default:
					fatal("Bogus versioning data `%s' for `%s'", $versions[$version], $version);
			}
		}

		/**
		 * PHP base directory
		 *
		 * @param string $version
		 * @return string|null
		 */
		protected static function getVersionHome(string $version): ?string
		{
			$versions = static::list();
			if (!array_key_exists($version, $versions)) {
				return null;
			}

			switch ($versions[$version]) {
				case 'system':
					return dirname(static::SYSTEM_BINARY);
				case 'native':
					$path = static::getMultiPhpPath('native') . '/' . $version;
					return file_exists($path) ? $path : null;
				case 'package':
					$verprefix = 'php' . str_replace('.', '', $version);
					$path = static::getMultiPhpPath('package') . '/' . $verprefix;
					return file_exists($path) ? $path : null;
				default:
					fatal("Bogus versioning data `%s' for `%s'", $versions[$version], $version);
			}
		}
	}