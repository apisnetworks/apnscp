<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	namespace Opcenter\Http\Php\Fpm;

	use Opcenter\Contracts\Virtualizable;
	use Opcenter\Http\Php\Fpm;
	use Opcenter\Http\Php\Vfs;
	use Opcenter\Map;
	use Opcenter\Provisioning\Apache;
	use Opcenter\Role\User;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Systemd\SyslogGenerator;
	use Opcenter\Versioning;

	class Configuration implements Virtualizable
	{
		const CONFIGURATION_BASE_PATH = '/etc/php-fpm.d/sites';

		// @var string group name
		protected $group;

		// @var string worker name
		protected $name;

		// @var string
		protected $sysuser = APACHE_UID;

		// @var string
		protected $sysgroup = \Web_Module::WEB_USERNAME;

		protected $phpConfiguration = [
			'memory_limit' => -1
		];

		// @var string
		protected $binary = MultiPhp::SYSTEM_BINARY;

		// @var ResourceManager
		protected $resourceManager;
		/**
		 * @var string account root
		 */
		protected $root;
		/**
		 * @var SiteConfiguration
		 */
		protected $svc;
		/**
		 * @var PoolPolicy
		 */
		protected $policy;

		public function __construct(string $root = '/')
		{
			$this->root = $root;
		}

		public static function bindTo(string $root = '/')
		{
			return new static($root);
		}

		/**
		 * Set site configuration template
		 *
		 * @param SiteConfiguration $svc
		 * @return Configuration
		 */
		public function setServiceContainer(SiteConfiguration $svc): self
		{
			$this->svc = $svc;

			// @TODO messy, decorator?
			$ctx = $svc->getAuthContext();
			$this->root = $svc->getAccountRoot();
			$this->setGroup($ctx->site)->setName($ctx->domain)->
				setSysGroup($ctx->group_id)->setSysUser($svc->getServiceValue('apache', 'webuser') ?? $ctx->username);

			if ($svc->getServiceValue('cgroup', 'enabled')) {
				// cgroup resource accounting when enabled
				$this->useResourceManager()->setGroup($svc->getSite());
			}

			$this->policy = PoolPolicy::instantiateContexted($ctx);
			$version = $this->getPolicy()->getPoolVersion($this->getName());
			if (!MultiPhp::fpmFromVersion($version)) {
				$new = $this->getPolicy()->getSystemVersion();
				warn("Requested PHP-FPM version %(requested)s unavailable. Defaulting to `%(system)s'",
					['requested' => $version, 'system' => $new]);
				$version = $new;
			}
			$this->setVersion($version);
			return $this;
		}

		/**
		 * Set PHP-FPM version
		 *
		 * @param string $version
		 * @return $this
		 */
		public function setVersion(string $version): self
		{
			if (!$path = MultiPhp::fpmFromVersion($version)) {
				fatal("Unknown version specified %s", $version);
			}

			$this->binary = $path;
			if ($this->getPolicy()->get(null, 'version') !== $version) {
				// force write
				$this->getPolicy()->set(null, 'version', $version);
			}

			return $this;
		}

		public function getArguments(): array
		{
			return [
				'--nodaemonize',
				'--fpm-config=' . $this->getFpmConfigurationPath(true)
			];
		}

		/**
		 * Get policy
		 */
		public function getPolicy(): PoolPolicy
		{
			return $this->policy;
		}

		/**
		 * Get site service container
		 *
		 * @return SiteConfiguration
		 */
		public function getServiceContainer(): SiteConfiguration
		{
			return $this->svc;
		}

		/**
		 * Get system root
		 *
		 * @return string
		 */
		public function getRoot(): string
		{
			return $this->root;
		}

		/**
		 * Set group name
		 *
		 * @param string $group
		 * @return self
		 */
		public function setGroup(string $group): self
		{
			if (!preg_match('/^[a-zA-Z0-9_-]{3,}$/', str_replace(['.', '_'], '-', strtolower($group)))) {
				fatal("Invalid worker group `%s'", $group);
			}
			$this->group = $group;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getGroup(): string
		{
			return $this->group;
		}

		/**
		 * Get system owner
		 *
		 * @return int
		 */
		public function getSysUser(): int
		{
			return $this->sysuser;
		}

		/**
		 * Set system owner
		 *
		 * @param int|string $sysuser
		 * @return self
		 */
		public function setSysUser($sysuser): self
		{
			if (!\is_int($sysuser)) {
				$pwd = User::bindTo($this->getRoot())->getpwnam($sysuser);
				if (null === $pwd) {
					// graceful recovery for sites missing "apache" user
					if (!Apache::createWebUser($this->svc, $sysuser)) {
						fatal("System user `%s' creation failed", $sysuser);
					}
					return $this->setSysUser($sysuser);
				}

				$sysuser = $pwd['uid'];
			}
			$this->sysuser = (int)$sysuser;

			return $this;
		}

		/**
		 * @param mixed $name
		 * @return Configuration
		 */
		public function setName($name): self
		{
			if (!preg_match('/^[a-zA-Z0-9_-]{3,}$/', str_replace(['.','_'], '-', strtolower($name)))) {
				fatal("Invalid worker name `%s'", $name);
			}
			$this->name = $name;

			return $this;
		}

		/**
		 * Get worker name
		 *
		 * @return string
		 */
		public function getName(): string
		{
			return $this->name;
		}

		/**
		 * Set worker group
		 *
		 * @param mixed $sysgroup
		 * @return self
		 */
		public function setSysGroup($sysgroup): self
		{
			if (\is_int($sysgroup)) {
				if (false === ($grp = posix_getgrgid($sysgroup))) {
					fatal("Unknown sysgroup specified `%d'", $sysgroup);
				}
				$sysgroup = $grp['name'];

			} else if (false === posix_getgrnam($sysgroup)) {
				fatal("Unknown sysgroup `%s'", $sysgroup);
			}

			$this->sysgroup = $sysgroup;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getSysGroup(): string
		{
			return $this->sysgroup;
		}

		/**
		 * Get PHP-FPM socket path
		 *
		 * @return string
		 */
		public function getSocketPath(): string
		{
			// Prefer /run globally, an ephemeral mount
			return '/var/run/php-fpm/' . $this->getGroup() . '-' . $this->getName() . '.socket';
		}

		/**
		 * Number of threads per server dedicated to proxy
		 *
		 * @return int
		 */
		public function getProxyThreadsPerServer(): int
		{
			return $this->getPolicy()->get($this->getName(), 'threads') ?? 3;
		}

		/**
		 * Get accept() timeout to proxy backend
		 *
		 * @return string|int
		 */
		public function getProxyConnectionTimeout() {
			$timeout = $this->getPolicy()->get($this->getName(), 'connect') ?? '5s';

			return is_int($timeout) ? "{$timeout}s" : $timeout;
		}


		/**
		 * Get PHP-FPM pid path
		 *
		 * @return string
		 */
		public function getPidPath(bool $jailed = false): string
		{
			return ($jailed ? '' : $this->getRoot()) . '/var/run/php-fpm/' . $this->getName() . '.pid';
		}

		/**
		 * Get systemd service name
		 *
		 * @return string
		 */
		public function getServiceName(): string
		{
			return $this->getPoolName() . '.service';
		}

		public function getPoolName(): string
		{
			return Fpm::SERVICE_NAMESPACE . $this->getGroup() . '-' . $this->getName();
		}

		/**
		 * Get service group name
		 *
		 * @return string
		 */
		public function getServiceGroupName(): string
		{
			if (!$this->getGroup()) {
				fatal('Group not defined for FPM configuration!');
			}
			return Fpm::SERVICE_NAMESPACE . $this->getGroup() . '.service';
		}

		/**
		 * Get systemd service name
		 *
		 * @return string
		 */
		public function getSocketServiceName(): string
		{
			return Fpm::SERVICE_NAMESPACE . $this->getGroup() . '-' . $this->getName() . '.socket';
		}

		/**
		 * Get PHP-FPM configuration path
		 *
		 * @param bool $jailed
		 * @return string
		 */
		public function getFpmConfigurationPath(bool $jailed = false): string {
			return ($jailed ? '' : $this->getRoot()) . static::CONFIGURATION_BASE_PATH . '/' . $this->getName() . '.conf';
		}

		/**
		 * Get PHP-FPM binary
		 *
		 * @return string
		 */
		public function getBinary(): string
		{
			return $this->binary;
		}

		/**
		 * Enable resource accounting
		 *
		 * @return bool
		 */
		public function hasResourceAccounting(): bool
		{
			return null !== $this->resourceManager;
		}

		/**
		 * Get cgroup resource manager for PHP-FPM instance
		 *
		 * @return ResourceManager|null
		 */
		public function getResourceManager(): ?ResourceManager
		{
			return $this->resourceManager;
		}

		/**
		 * Create a resource manager, implicitly assigning it
		 *
		 * @return ResourceManager
		 */
		public function useResourceManager(): ResourceManager
		{
			if (!isset($this->resourceManager)) {
				$this->resourceManager = new ResourceManager($this->svc);
			}
			return $this->resourceManager;
		}

		/**
		 * Get log path
		 *
		 * @return string|null
		 */
		public function getLog(): ?string
		{
			if (!$this->getServiceContainer()->getServiceValue('logs', 'enabled')) {
				return null;
			}

			if (!$this->getName()) {
				fatal('name must be set first');
			}
			return '/var/log/php-fpm/' . $this->getName() . '.log';
		}

		/**
		 * Enable PrivateTmp= usage
		 *
		 * @return bool
		 */
		public function usePrivateTmp(): bool
		{
			if (null !== ($use = $this->getPolicy()->get($this->getName(), 'privatetmp'))) {
				return (bool)$use;
			}

			return (bool)HTTPD_FPM_PRIVATETMP;
		}

		public function read(): Map
		{
			return Map::load($this->getFpmConfigurationPath(true), 'wd', 'ini');
		}

		/**
		 * Pool is default for unassigned paths
		 *
		 * @return bool
		 */
		public function isDefault(): bool
		{
			if (null === $this->svc) {
				fatal("Cannot verify pool status without ServiceContainer binding");
			}
			return $this->getName() === $this->svc->getAuthContext()->domain;
		}

		public function shimBinaries(): bool
		{
			return Vfs::bindTo($this->svc->getAuthContext()->domain_fs_path())->shim(
				Versioning::asMinor($this->getPolicy()->get(null,'version'))
			);
		}

		/**
		 * Identifier reported to syslog
		 *
		 * @return string
		 */
		public function syslogIdentifier(): string
		{
			return SyslogGenerator::instantiateContexted($this->svc->getAuthContext())->name('php-fpm', $this->getName());
		}
	}