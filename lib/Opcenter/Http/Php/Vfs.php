<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2024
	 */

	namespace Opcenter\Http\Php;

	use Opcenter\Contracts\Virtualizable;
	use Opcenter\Http\Php\Fpm\MultiPhp;

	class Vfs implements Virtualizable
	{
		const SHIM_ROOT = '/usr/local/bin';

		const BINS = [
			'phar',
			'php',
			'php-cgi',
			'php-config',
			'phpize',
			'phpdbg'
		];

		public function __construct(private string $root = '/')
		{ }

		public static function bindTo(string $root = '/')
		{
			return new static($root);
		}

		/**
		 * Link active pool PHP binaries into /usr/local/bin
		 *
		 * @param string $version
		 * @return bool
		 */
		public function shim(string $version): bool
		{
			$path = rtrim($this->root, '/') . '/' . self::SHIM_ROOT;
			if (!is_dir($path)) {
				return error("Path `%s' does not exist", $path);
			}

			$packageType = array_get(MultiPhp::list(), $version, 'system');
			// @xxx race condition on unlink/link
			foreach (self::BINS as $bin) {
				if (file_exists($path . '/' . $bin)) {
					unlink($path . '/' . $bin);
				}
			}

			if ($packageType === 'system') {
				return true;
			}

			if ($packageType === 'package') {
				return warn("Enabling PHP %(ver)s requires SCL usage: %(command)s", [
					'ver' => $packageType, 'command' => MultiPhp::cliFromVersion($version)
				]);
			}

			$baseDirectory = dirname(MultiPhp::cliFromVersion($version));
			foreach (self::BINS as $bin) {
				$src = $baseDirectory . '/' . $bin;
				if (!file_exists($src)) {
					debug("PHP binary `%(bin)s' missing in %(path)s", ['bin' => $bin, 'path' => $baseDirectory]);
					continue;
				}
				symlink($src, $path . '/' . $bin);
			}

			return true;
		}


	}