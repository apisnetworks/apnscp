<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Http;

	use Opcenter\Account\Create;
	use Opcenter\Account\Enumerate;
	use Opcenter\System\Contracts\SystemdStatusReporting;
	use Opcenter\System\GenericSystemdService;

	class Apache extends GenericSystemdService implements Contracts\Http, SystemdStatusReporting
	{
		protected const STATUS_REGEX = '!^Total requests:\s*(?<total>\d+);\s*Idle/Busy workers (?<idle>\d+)/(?<busy>\d+);' .
			'\s*Requests/sec:\s*(?<reqsec>[\d\.]+);\s*Bytes served/sec:\s*(?<bw>[^/]+)!';
		const SERVICE = 'httpd';

		const BATCH_KEY = 'apachecfg';

		const HTTP_BUILD_CONFIG = '/etc/systemd/user/httpd.init buildconfig';
		const SYSCONFIG_FILE = '/etc/sysconfig/httpd';
		const HTTP_HOME = '/etc/httpd';
		const CONFIG_PATH = self::HTTP_HOME . '/conf';
		const DOMAIN_PATH = self::CONFIG_PATH . '/domains';

		const HTTP_NOSSL_PORT = HTTPD_NOSSL_PORT;
		const HTTPD_SSL_PORT  = HTTPD_SSL_PORT;

		const CONFLICT_MASK = '/^[0-9]{1,10}-site[0-9]{1,10}-[A-F0-9]{8}$/';


		public static function makeSubdomainPath(string $subdomain): string
		{
			return '/var/subdomain/' . $subdomain . '/html';
		}

		public static function getServiceStatus(): ?array
		{
			return parent::getServiceStatus();
		}


		/**
		 * Get site configuration storage path
		 *
		 * @param string $site
		 * @return string
		 */
		public static function siteStoragePath(string $site): string
		{
			return static::CONFIG_PATH . "/{$site}";
		}

		public static function configurationIsInactive(string $site, string $personality = ''): bool
		{
			return file_exists(static::siteInactiveConfigurationPath($site, $personality));
		}

		/**
		 * Get inactive configuration path
		 *
		 * @param string $site
		 * @param string $personality
		 * @return string
		 */
		public static function siteInactiveConfigurationPath(string $site, string $personality = ''): string
		{
			return static::siteConfigurationPath('.' . ltrim($site, '.'), $personality);
		}

		public static function siteConfigurationPath(string $site, string $personality = ''): string
		{
			return static::CONFIG_PATH . '/virtual' .
				($personality ? '-' . $personality : '') . "/{$site}";
		}

		/**
		 * Find domains that would block domain from resolving first
		 *
		 * Does not take into account lexicographic sequence
		 *
		 * @param string     $domain
		 * @param array|null $universe domains to check against
		 * @return array
		 */
		public static function conflicts(string $domain, array $universe = null): array
		{
			// given a.foo.com
			$len = \strlen($domain);
			$blocking = [];
			$universe = $universe ?? Enumerate::domains();
			foreach ($universe as $blocker) {
				$blockerLen = \strlen($blocker);
				// with a.foo.com...
				if (substr($domain, -$blockerLen) !== $blocker) {
					// a.foo.com doesn't match ab.foo.com
					continue;
				}
				// a.foo.com contains foo.com
				// ignoring case when A == B
				if ($blockerLen >= $len) {
					continue;
				}

				if ($domain[-$blockerLen-1] === '.') {
					$blocking[] = $blocker;
				}
			}

			return $blocking;
		}

		/**
		 * Convert domain lexicographically higher hash
		 *
		 * @see Apache::conflicts()
		 * @param string $domain
		 * @param int    $generation
		 * @return string
		 */
		public static function hashDomain(string $domain, int $generation = 0): string
		{
			// invert ordering such that successive generations rank higher
			return sprintf("%d-site%s-%X", Create::MAX_SITES - $generation, \Auth::get_site_id_from_domain($domain), crc32($domain));
		}

		/**
		 * HTTP service is in failed state
		 *
		 * @return bool
		 */
		public static function irregular(): bool
		{
			if (!platform_is('6')) {
				return false;
			}
			return parent::irregular();
		}

		/**
		 * Reset failed state
		 *
		 * @return bool
		 */
		public static function reset(): bool
		{
			if (!platform_is('6')) {
				return true;
			}

			return parent::reset();
		}

		/**
		 * Schedule build + reload
		 *
		 * @param string $timespec
		 * @return bool
		 */
		public static function activate(string $timespec = HTTPD_RELOAD_DELAY): bool
		{
			return static::buildConfig($timespec, true);
		}

		/**
		 * Rebuild webserver config
		 *
		 * @param string $timespec window to coalesce multiple requests
		 * @param bool   $activate atomically reload HTTP after successful rebuild
		 * @return bool
		 */
		public static function buildConfig(string $timespec = HTTPD_RELOAD_DELAY, bool $activate = false): bool
		{
			$proc = new \Util_Process_Schedule($timespec);
			$proc->setID(self::BATCH_KEY);
			if ($id = $proc->preempted(self::BATCH_KEY)) {
				return true;
			}

			// nasty hack, let's assume a reload is issued before a build, e.g. site deleted
			// then config will rebuild after HTTP reload, but no HTTP reload is issued
			// following rebuild
			if ($id = $proc->preempted(static::getScheduleKey('reload'))) {
				$proc->cancelJob($id);
			}

			$command = self::getCommand('build');
			if ($activate) {
				$form = static::irregular() ? 'restart' : 'reload';
				// when processed as two separate commands systemctl can fire
				// before a lock is acquired from the first command
				$command .= ' && ' . self::getCommand($form);
			}
			$res = $proc->run(
				$command,
				array('mute_stdout' => true)
			);

			return $res['success'];
		}

		/**
		 * Wait for rebuild to conclude
		 *
		 * @return bool
		 */
		public static function waitRebuild(): bool
		{
			$proc = new \Util_Process_Schedule();
			$proc->setID(self::BATCH_KEY);
			for ($i = 0; $i < 300; $i++) {
				if (!$proc->preempted(self::BATCH_KEY)) {
					return self::waitFor(30);
				}
				sleep(1);
			}

			return false;
		}

		/**
		 * Cancel rebuild if pending
		 */
		public static function cancelRebuild(): bool
		{
			$proc = new \Util_Process_Schedule('next year');
			$proc->setID(self::BATCH_KEY);
			if ($id = $proc->preempted(self::BATCH_KEY)) {
				return $proc->cancelJob($id);
			}

			return false;
		}

		/**
		 * Reload Apache service
		 *
		 * @param string|null $timespec
		 * @return bool
		 */
		public static function reload(string $timespec = null): bool
		{
			if ($timespec === null) {
				$timespec = HTTPD_RELOAD_DELAY;
			}
			$proc = new \Util_Process_Schedule($timespec);
			$proc->setID(self::BATCH_KEY);
			if ($id = $proc->preempted(self::BATCH_KEY)) {
				// rebuild includes reload
				return true;
			}

			return parent::reload($timespec);
		}

		public static function logPath(): string
		{
			return \Log\Apache::LOCATION;
		}

		/**
		 * Restart Apache service
		 *
		 * @param string|null $timespec
		 * @return bool
		 */
		public static function restart(string $timespec = null): bool
		{
			if ($timespec === null) {
				$timespec = HTTPD_RELOAD_DELAY;
			}
			if (!platform_is('6')) {
				// no effect on older platforms
				return static::reload($timespec);
			}
			// @XXX when service in failed/inactive, reload-or-restart instead?
			return parent::restart($timespec);
		}

		/**
		 * Test Apache configuration
		 *
		 * @return bool
		 */
		public static function test(): bool
		{
			$command = static::getCommand('test');
			$ret = \Util_Process::exec($command);
			return $ret['success'];
		}

		/**
		 * Get system command
		 *
		 * @param string $type build, reload, or restart
		 * @return null|string
		 */
		protected static function getCommand(string $type): ?string {
			switch (strtolower($type)) {
				case 'build':
					return self::HTTP_BUILD_CONFIG;
				case 'reload':
					return parent::getCommand($type);
				case 'test':
					// @TODO update httpd to add "test" option
					return '/usr/sbin/httpd -t';
			}
			return parent::getCommand($type);
		}

		public static function getReportedServiceStatus(): ?array
		{
			if (! ($status = parent::getReportedServiceStatus()) ) {
				return $status;
			}
			if (isset($status['bw'])) {
				$status['bw'] = \Formatter::changeBytes($status['bw'], 'B');
			}
			array_walk($status, static function (string &$val, string $key) {
				if ($key !== 'reqsec') {
					$val = (int)$val;
				} else {
					$val = (float)$val;
				}
			}, $status);

			return $status;
		}
	}