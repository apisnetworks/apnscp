<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2018
	 */

	namespace Opcenter\Http;

	use Auth\UI\SecureAccessKey;
	use Opcenter\Net\Ip6;
	use Opcenter\Process;

	class Apnscp
	{
		private const PID_FILE = 'httpd.pid';
		public const CHECK_URL = 'http://localhost:' . \Auth_Redirect::CP_PORT;

		private static bool $hasRemoteIp;

		/**
		 * Verify frontend is processing requests
		 *
		 * @return bool
		 */
		public static function check(): bool
		{
			return silence(static function () {
				$ctx = stream_context_create(array(
					'http' =>
						array(
							'timeout' => 15,
							'method'  => 'HEAD',
							'header'  => [
								'User-agent: apnscp Internal check'
							]
						)
				));

				return (bool)get_headers(static::CHECK_URL, PHP_VERSION_ID >= 80000 ? false : 0, $ctx);
			});

		}

		/**
		 * Stop frontend
		 *
		 * @return bool
		 */
		public static function stop(): bool
		{
			if (!static::running()) {
				info('Frontend is not running - attempting stop anyway');
			}
			$ret = static::command('stop');

			return $ret['return'] > 0;
		}

		/**
		 * apnscpd frontend is running
		 *
		 * @return bool
		 */
		public static function running(): bool
		{
			if (!($pid = static::pid())) {
				return false;
			}

			return Process::pidMatches($pid, 'httpd') && posix_kill($pid, 0);
		}

		/**
		 * Kill HTTP workers exceeding memory
		 *
		 * @return int
		 */
		public static function cull(): int
		{
			if (APNSCPD_HEADLESS || !static::running()) {
				return 0;
			}
			$pid = static::pid();

			if (!$children = Process::children($pid)) {
				return 0;
			}

			$killed = 0;
			foreach (Process::statm($children) as $pid => $child) {
				if ($child['resident']/1024 > APNSCPD_HTTP_HIGH_WATERMARK) {
					dlog("Killed pid `%(pid)d' (RSS %(high)lu MB > %(mark)lu MB)", [
						'pid' => $pid,
						'high' => $child['resident']/1024,
						'mark' => APNSCPD_HTTP_HIGH_WATERMARK
					]);
					Process::kill($pid, SIGTERM) && $killed++;
				}
			}

			return $killed;
		}

		public static function pid(): ?int
		{
			if (!file_exists($file = run_path(self::PID_FILE))) {
				return null;
			}

			return (int)file_get_contents($file);
		}

		/**
		 * Process apnscpd frontend command
		 *
		 * @param string $mode
		 * @return array
		 */
		private static function command(string $mode): array
		{
			if (!\in_array($mode, ['start', 'restart', 'graceful', 'graceful-stop', 'stop'], true)) {
				fatal("Unknown command type `%s'", $mode);
			}

			$procClass = \Util_Process_Fork::class;
			if ($mode === 'stop') {
				$procClass = \Util_Process::class;
			}
			$proc = new $procClass();
			// otherwise Apache won't process
			$proc->setOption('leader', true);
			// 384 MB limit per process
			$proc->setOption('resource', [
				'rss' => (int)APNSCPD_RLIMIT_RSS*1024**2
			]);

			$accessKey = new SecureAccessKey;
			if (!APNSCPD_HEADLESS && !($key = $accessKey->get())) {
				$accessKey->reset();
				return self::command($mode);
			}
			$proc->setEnvironment('HOSTNAME', getenv('HOSTNAME'));
			$proc->setEnvironment(SecureAccessKey::SECKEY_ENV, $key ?? null);
			$parts = self::commandArgs();
			if ($mode === 'start' || $mode === 'restart') {
				// reinitialize module listing
				self::modules(true);
			}

			return $proc->run($parts['command'] . ' -k ' . $mode, $parts['args']);
		}

		/**
		 * Restart Apache process if defines change
		 * @return bool
		 */
		public static function restartIfEnvironmentChanged(): bool
		{
			if (!self::running()) {
				return self::start();
			}

			$expectedArgs = self::commandArgs();
			$actualArgs = array_filter(array_get(Process::stat(self::pid()), 'args', []), fn($a) => str_starts_with($a, "-D"));
			if ($expectedArgs['args']['args'] === implode(" ", $actualArgs)) {
				return false;
			}
			self::command('stop');

			return self::command('start')['success'];
		}

		private static function commandArgs(): array
		{
			$env = ['', 'APNSCP_ROOT=' . INCLUDE_PATH];
			if (getenv('PREFORK')) {
				$env[] = 'PREFORK';
			}
			if (FRONTEND_DISPATCH_HANDLER === 'fpm') {
				$env[] = 'FPM';
			}

			if (COCKPIT_ENABLED) {
				$env[] = 'COCKPIT';
			}

			if (!Ip6::bound("::1")) {
				$env[] = 'NO_IPV6';
			}

			if (FRONTEND_HTTPS_ONLY) {
				$env[] = 'SECURE';
			}

			return [
				'command' => '%(process)s -f %(config)s %(args)s',
				'args' => [
					'process' => '/usr/sbin/httpd',
					'config'  => config_path('httpd.conf'),
					'args'    => ltrim(implode(' -D', $env)),
				]
			];
		}

		/**
		 * Start apnscpd frontend
		 *
		 * @return bool
		 */
		public static function start(): bool
		{
			if (APNSCPD_HEADLESS) {
				return warn('Cannot start apnscp frontend - running in headless mode');
			}

			if (static::running()) {
				return warn('%s already running. Ignoring start command.', PANEL_BRAND);
			}

			if (file_exists($pidFile = run_path(self::PID_FILE))) {
				unlink($pidFile);
			}
			$ret = static::command('start');
			if ($ret['return']) {
				info("Frontend %(type)s started with PID `%(pid)d'", ['type' => 'HTTP', 'pid' => $ret['return']]);
			}

			return $ret['return'] > 0;
		}

		/**
		 * Reload frontend
		 *
		 * @return bool
		 */
		public static function reload(): bool
		{
			if (APNSCPD_HEADLESS) {
				return true;
			}
			$ret = static::command('graceful');

			return $ret['return'] > 0;
		}

		/**
		 * Modules built into Apache
		 *
		 * @param bool $force
		 * @return array
		 */
		public static function modules(bool $force = false): array
		{
			$key = 'ap.apache:modules';
			$cache = \Cache_Global::spawn();
			if (!$force && false !== ($modules = $cache->get($key))) {
				return $modules;
			}

			$command = \ArgumentFormatter::format(...array_values(self::commandArgs()));
			$ret = \Util_Process::exec($command . ' -DDUMP_MODULES');
			$modules = [];
			foreach (explode("\n", $ret['stdout']) as $line) {
				if (isset($line[0]) && $line[0] === ' ' && false !== ($pos = strpos($line, ' (s'))) {
					$modules[] = substr($line, 1, strpos($line, ' ', 1) - 1);
				}
			}
			$cache->set($key, $modules);

			return $modules;

		}

		/**
		 * HTTP server replaces REMOTE_ADDR with proxied IP
		 *
		 * @return bool
		 */
		public static function replacesRemoteAddress(): bool
		{
			if (isset(static::$hasRemoteIp)) {
				return self::$hasRemoteIp;
			}

			return self::$hasRemoteIp = in_array('remoteip_module', static::modules(), true);
		}
	}