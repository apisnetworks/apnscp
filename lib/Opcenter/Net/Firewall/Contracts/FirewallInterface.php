<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Net\Firewall\Contracts;

	use Opcenter\Net\Firewall\Rule;


	/**
	 * Class Firewall
	 *
	 * A simple firewall to block incoming connections
	 *
	 * @package Opcenter\Net
	 */
	interface FirewallInterface
	{
		/**
		 * Get all firewall groups
		 *
		 * @return array
		 */
		public static function groups(): array;

		/**
		 * Get members from group
		 *
		 * @param null|string $group optional chain to filter
		 * @return Rule[]
		 */
		public static function getEntriesFromGroup(string $group = null): array;

		/**
		 * Append a rule to the end of a chain
		 *
		 * @param Rule $rule
		 * @return bool
		 */
		public static function append(Rule $rule): bool;

		/**
		 * Insert a rule to the beginning of a chain
		 *
		 * @param Rule $rule
		 * @return bool
		 */
		public static function insert(Rule $rule): bool;

		/**
		 * Remove rule by index
		 *
		 * @param Rule $rule
		 * @return bool
		 */
		public static function remove(Rule $rule): bool;
	}