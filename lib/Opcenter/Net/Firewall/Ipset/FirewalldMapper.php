<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2018
	 */


	namespace Opcenter\Net\Firewall\Ipset;

	/**
	 * Class Firewalld
	 *
	 * Firewalld permanent configuration connector
	 *
	 * @package Opcenter\Net
	 */
	class FirewalldMapper
	{
		// @var string IP address or CIDR
		protected $address;

		public function __construct(string $address) {
			$this->address = $address;
		}


		/**
		 * Get named ipset configuration
		 *
		 * @param string $list ipset list
		 * @return string|null file if exists or null if not present
		 */
		private function getConfiguration(string $list): ?string {
			$file = '/etc/firewalld/ipsets/' . $list . '.xml';
			if (!file_exists($file)) {
				return null;
			}
			return $file;
		}

		/**
		 * Opportunistically remove address
		 *
		 * @param string $list
		 * @return bool
		 */
		public function removeFrom(string $list): bool {
			return $this->atomic($list, 'remove');
		}

		/**
		 * Opportunistically add address
		 *
		 * @param string $list
		 * @return bool
		 */
		public function addTo(string $list): bool
		{
			return $this->atomic($list, 'add');
		}

		/**
		 * Atomically alter Firewalld configuration
		 *
		 * @param string $list list name
		 * @param string $mode operation ["add", "remove"]
		 * @return bool
		 */
		private function atomic(string $list, string $mode): bool {
			if (null === ($cfg = $this->getConfiguration($list))) {
				return false;
			}
			if (!$fp = fopen($cfg, 'r+')) {
				return error("Unable to open `%s' for access", $cfg);
			}
			$wb = true;
			for ($i = 0; $i < 100; $i++) {
				if (flock($fp, LOCK_EX | LOCK_NB, $wb)) {
					break;
				}
				usleep(300000);
			}

			if ($wb) {
				fclose($fp);
				fatal("Failed to acquire lock on `%s'", $cfg);
			}

			if (false === ($xml = simplexml_load_file($cfg))) {
				return error('%s: unable to load XML', $cfg);
			}
			$removal = [];
			foreach ($xml->entry as $entry) {
				// @var \SimpleXMLElement $entry
				if ((string)$entry !== $this->address) {
					continue;
				}
				$removal[] = &$entry[0];
			}
			foreach ($removal as $r) {
				unset($r[0]);
			}
			if ($mode === 'add') {
				$xml->addChild('entry', $this->address);
			}
			if (false === ftruncate($fp, 0) || -1 === fseek($fp, 0)) {
				fatal('%s: failed to truncate', $cfg);
			}
			fwrite($fp, $xml->asXML());
			fclose($fp);
			return true;
		}
	}