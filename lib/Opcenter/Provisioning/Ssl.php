<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;
	use Opcenter\Provisioning\Traits\MapperTrait;
	use Opcenter\SiteConfiguration;

	/**
	 * Class Ssl
	 *
	 * @package Opcenter\Provisioning
	 */
	class Ssl
	{
		use FilesystemPopulatorTrait;
		use MapperTrait;
		use GroupCreationTrait;
		const TEMPLATE_FILES = [];
		const TEMPLATE_DIRECTORIES = [
			'/etc/httpd/conf/ssl.key' => [null, 'root', 0710],
			'/etc/httpd/conf/ssl.csr' => [null, 'root', 0710],
			'/etc/httpd/conf/ssl.crt' => [null, 'root', 0710],
		];

		/**
		 * Site may install/remove certificates
		 *
		 * @param SiteConfiguration $container
		 * @return bool
		 */
		public static function enabled(SiteConfiguration $container): bool
		{
			return (bool)$container->getServiceValue('ssl', 'enabled');
		}

		/**
		 * Certificate data present on account
		 *
		 * @param SiteConfiguration $container
		 * @return bool
		 */
		public static function installed(SiteConfiguration $container): bool
		{
			/** @var \Ssl_Module $module */
			$module = \a23r::get_class_from_module('ssl');

			return file_exists($container->getAccountRoot() . '/' . $module::CRT_PATH .
				'/' . $module::DEFAULT_CERTIFICATE_NAME . '.crt');
		}
	}