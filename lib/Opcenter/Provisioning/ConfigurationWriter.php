<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\SiteConfiguration;

	class ConfigurationWriter
	{
		/**
		 * @var \BladeLite
		 */
		protected $view;
		/**
		 * @var string $template template name
		 */
		protected $template;

		/**
		 * @var SiteConfiguration service configuration
		 */
		protected $container;

		private $compiled;

		public function __construct($view, ?SiteConfiguration $container)
		{
			$this->container = $container;
			$path = 'templates' . (false !== strpos($view, '/') ? '/' . \dirname($view) : '');
			$this->view = \BladeLite::factory($path);
			$this->template = basename($view);
			if ($this->container) {
				$this->view->compiler()->directive('svc', static function ($arguments) {
					return '<?php echo $svc->getServiceValue(' . $arguments . '); ?>';
				});
			}
			// share service template name
			$this->view->composer('*', function ($view) {
				$this->view->share('templatePath', $view->getPath());
			});
		}

		public function __toString()
		{
			if (!$this->compiled) {
				$this->compile();
			}

			return $this->compiled;
		}

		/**
		 * Compile template
		 *
		 * @param array $vars
		 * @return mixed
		 */
		public function compile(array $vars = [])
		{
			if ($this->container) {
				$vars['svc'] = $this->container;
			}

			$this->compiled = $this->view->render($this->template, $vars);

			return $this;
		}

		/**
		 * Get path from template
		 *
		 * @return string
		 */
		public function getTemplatePath(): ?string
		{
			if (!$this->view->exists($this->template)) {
				return null;
			}

			return $this->view->getFinder()->find($this->template);
		}

		/**
		 * Template is newer than compiled output
		 *
		 * @param string $output
		 * @return bool
		 */
		public function shouldRefresh(string $output): bool
		{
			if (!file_exists($output)) {
				return true;
			}

			if (!$file = $this->getTemplatePath()) {
				return false;
			}

			$compiled = $this->view->compiler()->getCompiledPath($file);
			return !file_exists($compiled) || filemtime($output) <= filemtime($compiled);
		}

		/**
		 * Write template
		 *
		 * @param string $path
		 * @return bool
		 */
		public function write(string $path): bool
		{
			return file_put_contents($path, (string)$this) >= 0;
		}
	}


