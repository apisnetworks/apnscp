<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Provisioning\Traits\ApacheModule;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Cgroup\Controller;
	use Opcenter\System\Cgroup\Group;

	/**
	 * Class Cgroup
	 *
	 * Categorize all jailed activity to cgroup
	 *
	 * @package Opcenter\Provisioning
	 */
	class Cgroup
	{
		use ApacheModule;
		const CONFIG_NAME = 'cgroup';

		/**
		 * Bind all controller activity to named group
		 *
		 * @param SiteConfiguration $svc service configuration (derives group)
		 * @param string            $controller
		 * @return bool
		 */
		public static function bindToController(SiteConfiguration $svc, string $controller = '*'): bool
		{
			$path = static::getConfigPath($svc);
			if (!file_exists($path)) {
				touch($path);
			}

			$group = new Group(self::groupFromConfiguration($svc));
			if ($controller === '*') {
				$controllers = $svc->getSiteFunctionInterceptor()->cgroup_get_controllers();
				foreach ($controllers as &$controller) {
					$controller = Controller::make($group, $controller);
					if (!$controller->immmediateBinding()) {
						$controller = null;
					}
				}
				return file_put_contents($path, '* ' . implode(',', array_filter($controllers)) . ' ' . $group . "\n") > 0;
			}
			$contents = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			$found = false;
			if (!$found) {
				$contents[] = "* {$controller} " . $group;
			}

			return file_put_contents($path, join("\n", $contents)) > 0;
		}

		/**
		 * Get localized account cgconfig path
		 *
		 * @param SiteConfiguration $svc
		 * @return string
		 */
		private static function getConfigPath(SiteConfiguration $svc = null): string
		{
			return ($svc ? $svc->getAccountRoot() : '') . \Opcenter\System\Cgroup::CGROUP_SITE_CONFIG;
		}

		/**
		 * Remove controller binding from site
		 *
		 * @param SiteConfiguration $svc
		 * @param string            $controller
		 * @return bool
		 */
		public static function removeController(SiteConfiguration $svc, string $controller = '*'): bool
		{
			$path = static::getConfigPath($svc);
			if (!is_file($path)) {
				return warn("cgroup configuration not active on site, `%s' missing?", static::getConfigPath());
			}
			$contents = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			foreach ($contents as $k => $v) {
				strtok($v, ' ');
				if ($controller === '*') {
					unset($contents[$k]);
				} else if (strtok(' ') === $controller) {
					unset($contents[$k]);
				}
			}
			if ($controller === '*') {
				$controller = $svc->getSiteFunctionInterceptor()->cgroup_get_controllers();
			}
			$group = new Group($svc->getSite());
			foreach ((array)$controller as $c) {
				$controller = Controller::make($group, $c);
				if (!$controller->exists()) {
					continue;
				}
				\Opcenter\System\Cgroup::delete($group, $controller);
			}
			file_put_contents($path, implode("\n", $contents));

			return true;
		}

		public static function createControllerConfiguration(SiteConfiguration $svc, bool $circular = null): bool
		{
			$basePerms = [
				'uid' => $svc->getSiteFunctionInterceptor()->php_jailed() ? 'root' : \Web_Module::WEB_USERNAME,
				'gid' => $svc->getServiceValue('siteinfo', 'admin'),
			];
			$name = Cgroup::groupFromConfiguration($svc, (bool)$circular);
			$group = new Group($name, [
				'task' => $basePerms + [
					'fperm' => 0660
				],
				'admin' => $basePerms + [
					'fperm' => 0750 | ($svc->getServiceValue('cgroup', 'delegator') ? 1 : 0)
				]
			]);

			if (null === $circular && !str_ends_with((string)$group, Group::CIRCULAR_IDENTIFIER) && $group->hasGroups()) {
				debug("Group `%(name)s' is circular", ['name' => (string)$group]);
				static::createControllerConfiguration($svc, true);
			}

			foreach ($svc->getSiteFunctionInterceptor()->cgroup_get_controllers() as $cname) {
				$controller = Controller::make($group, $cname);
				if (!$circular) {
					// resources enforced under siteXX, not siteXX/self
					$controller->import($svc->getAuthContext());
				}
				$group->add($controller);
				$controller->create();
			}

			return \Opcenter\System\Cgroup::create($group);
		}

		/**
		 * @param SiteConfiguration $svc
		 * @param bool|null         $circular require circular reference, auto-detect if null
		 * @return bool
		 */
		public static function groupFromConfiguration(SiteConfiguration $svc, bool $circular = null): string
		{
			$group = new Group($svc->getSite());
			$name = (string)$group;
			$circular ??= $group->hasGroups();

			if ($circular && !str_ends_with($name, Group::CIRCULAR_IDENTIFIER)) {
				return "{$name}/" . Group::CIRCULAR_IDENTIFIER;
			}

			return $name;
		}
	}