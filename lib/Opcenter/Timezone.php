<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, February 2023
 */

namespace Opcenter;

use Opcenter\Contracts\VirtualizedContextable;

class Timezone implements VirtualizedContextable
{
	use \FilesystemPathTrait;
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	const TIMEZONE_FILE = '/etc/localtime';
	const ZONE_REPOSITORY = '/usr/share/zoneinfo';

	const RELOAD_HOOK = 'timezone';

	private string $tz;

	protected function __construct()
	{
		$this->tz = $this->getAuthContext()->timezone ?? date_default_timezone_get();
	}

	public function get(): string
	{
		return $this->tz;
	}

	public function set(string $timezone): bool
	{
		if (posix_getuid()) {
			fatal("Must be executed from backend");
		}

		if (!$this->valid($timezone)) {
			return error("invalid timezone `%s'", $timezone);
		}

		if (!file_exists($tzsrc = self::ZONE_REPOSITORY . '/' . $timezone)) {
			return error("Cannot locate timezone `%(tz)s' within `%(dir)s",
				['tz' => $timezone, 'dir' => self::ZONE_REPOSITORY]);
		}

		defer($_, static fn() => date_default_timezone_set($timezone));

		$this->common_set_preference('timezone', $timezone);
		$this->getAuthContext()->timezone = $timezone;
		if ($this->getAuthContext()->level & PRIVILEGE_ADMIN) {
			return $this->scope_set('system.timezone', $timezone);
		}
		// update shell prefs...
		$bashrc = $this->user_get_home() . '/.bashrc';
		if (!$this->file_exists($bashrc)) {
			$this->file_touch($bashrc);
		}

		// possible race condition
		$contents = $this->file_get_file_contents($bashrc);
		$contents = rtrim(preg_replace(\Regex::COMMON_BASH_TZ, '', $contents)) .
			"\nTZ=\"" . $timezone . "\"\nexport TZ\n";
		$this->file_put_file_contents($bashrc, $contents);

		if ($this->getAuthContext()->level & PRIVILEGE_SITE) {
			if (file_exists($this->domain_fs_path(self::TIMEZONE_FILE))) {
				unlink($this->domain_fs_path(self::TIMEZONE_FILE));
			}
			symlink($tzsrc, $this->domain_fs_path(self::TIMEZONE_FILE));
		}
		\Util_Account_Hooks::instantiateContexted($this->getAuthContext())->run('reload', [self::RELOAD_HOOK, ['timezone' => $timezone]]);

		return true;
	}

	public function valid(string $timezone): bool
	{
		if (!timezone_open($timezone)) {
			return false;
		}

		try {
			return (bool)(new \IntlDateFormatter(
				null,
				\IntlDateFormatter::NONE,
				\IntlDateFormatter::NONE,
				$timezone))->format(0);
		} catch (\IntlException $e) {
			return error([':err_timezone_setting', 'Timezone setting invalid: %s'], $e->getMessage());
		}
	}
}