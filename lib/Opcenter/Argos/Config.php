<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos;

	use Opcenter\Provisioning\ConfigurationWriter;
	use Symfony\Component\Yaml\Yaml;

	class Config
	{
		public const CONFIGURATION_FILE = '/root/.argos.conf';
		// special tokens top-level that aren't backends
		public const ARGOS_KEYWORDS = [
			'backends',
			'high_priority_tokens'
		];

		/**
		 * @var Yaml YAML config
		 */
		protected $yml;
		/**
		 * @var string YAML configuration path
		 */
		protected $cfgfile;

		protected $dirty = false;

		private function __construct($config = Config::CONFIGURATION_FILE)
		{
			if (!file_exists($config)) {
				$this->yml = new Yaml();
			} else {
				$this->yml = Yaml::parseFile($config);
			}
			foreach ($this->getBackends() as $backend) {
				$be = array_get($this->yml, "{$backend}.backend", $backend);
				$this->yml[$backend] = Backend::load(
					$this->yml[$backend],
					$this->resolve(((array)$be)[0])
				);
				if (\is_array($be) && \count($be) > 1) {
					warn("Multiple backends present for `%s' - editing not possible", $backend);
					$this->yml[$backend]->lock();
				}
			}
			$this->cfgfile = $config;
		}

		/**
		 * Get configured backends
		 *
		 * @return array
		 */
		public function getBackends(): array
		{
			return array_keys(array_except($this->yml, self::ARGOS_KEYWORDS));
		}

		/**
		 * Resolve cardinal backend from a list of backends
		 *
		 * @param string $backend
		 * @return null|string
		 */
		protected function resolve(string $backend): ?string
		{
			$backends = Backend::getBackends();
			if (\in_array($backend, $backends, true)) {
				return $backend;
			}
			if (isset($this->yml[$backend])) {
				return $this->resolve($this->yml[$backend]['backend']);
			}

			return null;
		}

		/**
		 * Get Argos configuration instance
		 *
		 * @return array|Config
		 */
		public static function get(): ?self
		{
			if (!file_exists(self::CONFIGURATION_FILE)) {
				return null;
			}

			return new static();
		}

		/**
		 * Initialize Argos configuration file
		 *
		 * @return bool
		 */
		public static function initialize(): bool
		{
			return file_put_contents(
					self::CONFIGURATION_FILE,
					(new ConfigurationWriter('argos.argos-conf', null))->compile(),
					LOCK_EX
				) > 0;
		}

		public function setDefault($backends): bool
		{
			$knownBackends = $this->getBackends();
			foreach ((array)$backends as $backend) {
				if (!\in_array($backend, $knownBackends, true)) {
					return error("Unknown backend `%s'", $backend);
				}
			}
			$this->yml['backends'] = (array)$backends;
			$this->sync();

			return true;
		}

		public function sync(): bool
		{
			if ($this->dirty) {
				return false;
			}

			return file_put_contents($this->cfgfile, (string)$this, LOCK_EX) > 0;
		}

		/**
		 * Create new Argos backend
		 *
		 * @param string $provider
		 * @param string $alias
		 * @return Backend
		 */
		public function createBackend(string $provider, string $alias = null): ?Backend
		{
			if (!$alias) {
				$alias = $provider;
			}
			if (isset($this->yml[$alias]) || isset($this->yml[$provider])) {
				error("Named backend `%s' or alias `%s' already exists", $provider, $alias);

				return null;
			}
			$backend = Backend::load(['backend' => $provider], $provider);
			array_add($this->yml, "backends.{$alias}", $provider);
			$this->yml[$alias] = $backend;
			$this->sync();

			return $backend;
		}

		/**
		 * Delete Argos backend
		 *
		 * @param string $backend
		 * @return bool
		 */
		public function deleteBackend(string $backend): bool
		{
			if (!$this->backendExists($backend)) {
				return error("Backend `%s' does not exist", $backend);
			}
			unset($this->yml[$backend]);

			return $this->sync();
		}

		/**
		 * Backend name exists
		 *
		 * @param string $name name or alias
		 * @return bool
		 */
		public function backendExists(string $name): bool
		{
			return isset($this->yml[$name]);
		}

		public function __toString()
		{
			$yaml = $this->yml;
			foreach ($this->getBackends() as $backend) {
				$yaml[$backend] = $yaml[$backend]->__debugInfo();
			}

			return Yaml::dump($yaml, 2, 2, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
		}

		public function __debugInfo()
		{
			if (null === $this->yml) {
				return null;
			}

			return (array)$this->yml;
		}

		public function __destruct()
		{
			foreach ($this->getBackends() as $backend) {
				if ($this->backend($backend)->dirty()) {
					$this->sync();

					return;
				}
			}
		}

		/**
		 * Get backend configuration
		 *
		 * @param string $name
		 * @return Backend
		 */
		public function backend(?string $name)
		{
			if (!isset($this->yml[$name])) {
				error("Unknown backend `%s'", $name);

				return Backend::load([], 'pushover');
			}

			return $this->yml[$name];
		}
	}