<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Crypto\Ssl;

	class Certificate implements \ArrayAccess
	{
		// @var string
		protected $certificate;

		// @var array
		protected $info;

		public function __construct(\OpenSSLCertificate|string $certificate)
		{
			$this->info = openssl_x509_parse($certificate);
			if ($certificate instanceof \OpenSSLCertificate) {
				$tmp = '';
				if (!openssl_x509_export($certificate, $tmp)) {
					fatal("Failed certificate extraction");
				}
				$certificate = $tmp;
			}
			$this->certificate = $certificate;
		}

		public function authority(): ?string
		{
			if (!$authority = $this->field('extensions.authorityKeyIdentifier')) {
				return null;
			}

			$prefix = 'keyid:';
			if (!strncmp($authority, $prefix, strlen($prefix))) {
				$authority = substr($authority, strlen($prefix));
			}

			return trim($authority);
		}

		/**
		 * Fetch certificate field
		 *
		 * @param string $name field name dot-notation
		 * @return mixed
		 */
		public function field(string $name, mixed $default = null): mixed
		{
			return array_get($this->info, $name, $default);
		}

		public function __toString()
		{
			return $this->certificate;
		}

		#[\ReturnTypeWillChange]
		public function offsetExists($offset)
		{
			return array_has($this->info, $offset);
		}

		#[\ReturnTypeWillChange]
		public function offsetGet($offset)
		{
			return array_get($this->info, $offset);
		}

		#[\ReturnTypeWillChange]
		public function offsetSet($offset, $value)
		{
			fatal("Cannot modify private key resource");
		}

		#[\ReturnTypeWillChange]
		public function offsetUnset($offset)
		{
			fatal("Cannot modify private key resource");
		}
	}