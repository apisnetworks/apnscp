<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, April 2024
 */

namespace Opcenter\Crypto;

trait KeyringTrait {
	use \ContextableTrait;

	protected function readKeyringValue(string $value): mixed
	{
		if (Keyring::reference($value)) {
			$class = (\apnscpFunctionInterceptor::get_class_from_module('keyring'))::instantiateContexted($this->getAuthContext());
			$tmp = $class->get($value);
			if ($tmp === null) {
				error("Keyring value `%(name)s' empty or missing", ['name' => $value]);
			}

			$value = $tmp;
		}

		return Keyring::decode($value);
	}
}