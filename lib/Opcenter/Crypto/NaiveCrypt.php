<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Crypto;

use Frontend\Multipath;

/**
 * Class SimpleCrypt
 *
 * Encrypt/decrypt
 *
 * @package Opcenter\Crypto
 */
class NaiveCrypt extends SymmetricCrypt {
	const TAG_COOKIE_NAME = 'sso-tag';

	private string $tag = '';

	public function __construct(string $token)
	{
		parent::__construct($token);
		$encoded = base64_encode($this->tag);
		if (!headers_sent()) {
			\Auth_UI::setCookie(static::TAG_COOKIE_NAME, $encoded, path: Multipath::prefix() ?: '/');
		}
		$_COOKIE[static::TAG_COOKIE_NAME] = $encoded;
	}

	protected function encrypt(string $token, string &$tag = null): string
	{
		return parent::encrypt($token, $this->tag);
	}

	/**
	 * Get decrypted string
	 *
	 * @return string|null
	 */
	public function get(): ?string {
		if (!isset($_COOKIE[static::TAG_COOKIE_NAME])) {
			return null;
		}
		$tag = base64_decode($_COOKIE[static::TAG_COOKIE_NAME], true);
		if (false === $tag) {
			return null;
		}
		$password = openssl_decrypt(
			$this->crypted,
			static::CIPHER,
			substr($this->secret, 0, $this->digestLength),
			0,
			$this->iv,
			$tag
		);
		if (false === $password) {
			// bogus tag
			return null;
		}

		return $password;
	}
}