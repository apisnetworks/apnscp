<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2024
 */

namespace Opcenter\Crypto;

/**
 * Class SymmetricCrypt
 *
 * Encrypt/decrypt
 *
 * @package Opcenter\Crypto
 */
abstract class SymmetricCrypt implements \Serializable
{
	const SECRET = AUTH_SECRET;
	const CIPHER = 'aes-256-gcm';

	/**
	 * @var string crypted data
	 */
	protected string|SymmetricCrypt $crypted;
	/**
	 * @var string IV
	 */
	protected string $iv;

	protected string $secret = self::SECRET;

	readonly int $digestLength;

	public function __construct(string $token)
	{
		$ivlen = openssl_cipher_iv_length(static::CIPHER);
		$this->digestLength = \openssl_cipher_key_length(static::CIPHER);
		$this->iv = $this->ivInit($ivlen);
		$this->crypted = $this->encrypt($token);
	}

	protected function encrypt(string $token, string &$tag = null): string
	{
		return openssl_encrypt($token, static::CIPHER, substr($this->secret, 0, $this->digestLength), 0, $this->iv, $tag);
	}

	public function __serialize(): array
	{
		return [
			'crypted' => $this->crypted,
			'iv'      => base64_encode($this->iv)
		];
	}

	public function __unserialize(array $data)
	{
		$this->crypted = $data['crypted'];
		$this->iv = base64_decode($data['iv']);
		$this->digestLength = \openssl_cipher_key_length(static::CIPHER);
	}

	public function serialize(): ?string
	{
		return serialize($this->__serialize());
	}

	public function unserialize(string $data): void
	{
		$this->__unserialize(\Util_PHP::unserialize($data, [SymmetricCrypt::class]));
	}

	public function __debugInfo()
	{
		return [];
	}

	/**
	 * Get IV bytes
	 *
	 * @param int $length
	 * @return string
	 */
	private function ivInit(int $length): string
	{
		return openssl_random_pseudo_bytes($length);
	}

	/**
	 * Get decrypted string
	 *
	 * @return string|null
	 */
	abstract public function get(): ?string;
}