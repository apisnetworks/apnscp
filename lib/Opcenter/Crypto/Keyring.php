<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2024
 */

namespace Opcenter\Crypto;

/**
 * Class SimpleCrypt
 *
 * Encrypt/decrypt
 *
 * @package Opcenter\Crypto
 */
class Keyring extends SymmetricCrypt
{
	const KEYRING_PREFIX = 'keyring:';

	/**
	 * @var string IV
	 */
	protected $tag;

	public function __construct(string|array $token)
	{
		if (!is_string($token)) {
			$token = serialize($token);
		}
		parent::__construct($token);
	}

	protected function encrypt(string $token, string &$tag = null): string
	{
		return parent::encrypt($token, $this->tag);
	}

	public function serialize(): ?string
	{
		return self::KEYRING_PREFIX . implode('#', array_map(
			base64_encode(...), [$this->crypted, $this->iv, $this->tag]
			)
		);
	}

	/**
	 * Create new encrypted keyring value
	 *
	 * @param mixed $value
	 * @return string
	 */
	public static function encode(mixed $value): string
	{
		return (new static($value))->serialize();
	}

	/**
	 * Decrypt value
	 *
	 * @param Keyring|string $raw
	 * @param string|null    $secret
	 * @return mixed
	 */
	public static function decode(Keyring|string $raw, string $secret = null): mixed
	{
		if ($raw instanceof Keyring) {
			if ($secret) {
				$raw->secret = $secret;
			}

			$raw = $raw->get();
			return str_starts_with($raw, 'a:') ? \Util_PHP::unserialize($raw) : $raw;
		}
		$instance = new static($raw);
		// toss out results
		$instance->crypted = $raw;
		if ($secret) {
			$instance->secret = $secret;
		}
		$instance->unserialize($raw);

		if (null === ($raw = $instance->get())) {
			return nerror("Failed to decode string");
		}

		if (!str_starts_with($raw, 'a:') || false === ($decoded = \Util_PHP::unserialize($raw))) {
			return $raw;
		}

		return $decoded;
	}

	public function unserialize(string $serialized): void
	{
		[$crypted, $iv, $tag] = array_map(base64_decode(...),
			explode('#', substr($serialized, strlen(self::KEYRING_PREFIX)), 3)
		);

		$this->crypted = $crypted;
		$this->tag = $tag;
		$this->iv = $iv;
	}

	/**
	 * Get decrypted string
	 *
	 * Return requires unserialization
	 *
	 * @return mixed|null
	 */
	public function get(): ?string
	{
		if (false === $this->tag) {
			return null;
		}

		$value = openssl_decrypt(
			$this->crypted,
			static::CIPHER,
			substr($this->secret, 0, $this->digestLength),
			0,
			$this->iv,
			$this->tag
		);

		if (false === $value) {
			// bogus tag
			return null;
		}

		return $value;
	}

	public static function is(mixed $str): bool
	{
		if ($str instanceof Keyring) {
			return true;
		}

		return is_string($str) && str_starts_with($str, self::KEYRING_PREFIX);
	}

	public static function reference(string $str): bool
	{
		return str_starts_with($str, self::KEYRING_PREFIX) && \count(explode('#', $str)) !== 3;
	}
}