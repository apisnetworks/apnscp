<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt\DispatchServices\Acmephp;

use AcmePhp\Ssl\Generator\EcKey\EcKeyOption;
use AcmePhp\Ssl\Generator\KeyPairGenerator;
use AcmePhp\Ssl\Generator\RsaKey\RsaKeyOption;
use AcmePhp\Ssl\KeyPair;
use AcmePhp\Ssl\PrivateKey;
use AcmePhp\Ssl\PublicKey;
use Opcenter\Crypto\Letsencrypt;
use Opcenter\Filesystem;

class KeypairFinder {
	// @var string default key type
	public const DEFAULT_KEY = 'EC';

	// @var \Auth_Info_User $ctx
	protected $ctx;
	/**
	 * @var string key type
	 */
	protected $type;

	public function __construct(\Auth_Info_User $ctx, $type = null)
	{
		$this->ctx = $ctx;
		if ($type === null) {
			$default = static::DEFAULT_KEY;
			if ($ctx->level & PRIVILEGE_ADMIN || !$ctx->level) {
				// MAIN server certificate should remain for compatibility across services
				// @FIXME housekeeping uses no authorization data
				$default = 'RSA';
			}
			$type = \Preferences::factory($ctx)->get(Letsencrypt\Preferences::KEY_TYPE, $default);
		}
		if ($type !== 'EC' && $type !== 'RSA') {
			fatal("Unknown key option `%s'", $type);
		}

		$this->type = $type;

	}

	/**
	 * Get certificate key
	 *
	 * @return KeyPair
	 */
	public function getCertificateKey(): ?KeyPair
	{
		$path = Letsencrypt::acmeSiteStorageDirectory($this->ctx->site ?? 'MAIN', Letsencrypt::activeServer());
		if (!is_dir($path) || !file_exists($path . '/key.pem')) {
			return null;
		}
		$pem = file_get_contents("{$path}/key.pem");

		return new KeyPair(
			new PublicKey($pem),
			new PrivateKey($pem)
		);
	}

	/**
	 * Get registration keypair
	 *
	 * @return KeyPair
	 */
	public function getRegistrationKey(): KeyPair
	{
		$path = $this->getRegistrationKeyPath();

		if (!$this->valid()) {
			if (!is_dir($parent = \dirname($path))) {
				Filesystem::mkdir($parent, 'root', 'root', 0711);
			}
			$keys = $this->create();
			file_put_contents($path,
				(string)$keys->getPublicKey()->getPEM() . "\n" . (string)$keys->getPrivateKey()->getPEM());
			// key generation triggers registration
			if (!Letsencrypt\DispatchServices\Acmephp::register($this->ctx, null)) {
				fatal("Registration failed");
			}
		}
		$pem = file_get_contents($path);

		return new KeyPair(
			new PublicKey($pem),
			new PrivateKey($pem)
		);
	}

	/**
	 * Account is registered with Let's Encrypt
	 *
	 * @return bool
	 */
	public function registered(): bool
	{
		return file_exists($this->getRegistrationKeypath());
	}

	/**
	 * Registered key is valid for type
	 *
	 * @return bool
	 */
	public function valid(): bool
	{
		if (!$this->registered()) {
			return false;
		}
		$path = $this->getRegistrationKeypath();

		if (false === ($res = openssl_pkey_get_private(file_get_contents($path)))) {
			return false;
		}
		$details = openssl_pkey_get_details($res);
		switch ($this->type) {
			case 'EC':
				return array_get($details, 'type') === OPENSSL_KEYTYPE_EC;
			case 'RSA':
				return array_get($details, 'type') === OPENSSL_KEYTYPE_RSA;
			default:
				return false;
		}
	}

	/**
	 * Get registration key path
	 *
	 * @return string
	 */
	private function getRegistrationKeypath(): string
	{
		if (!LETSENCRYPT_UNIFY_REGISTRATION && $this->ctx->site) {
			$path = Letsencrypt::acmeSiteStorageDirectory($this->ctx->site);
		} else {
			$path = Letsencrypt::acmeDataDirectory() . '/accounts';
		}
		return $path . DIRECTORY_SEPARATOR . str_replace(['/'], '.', Letsencrypt::activeServer()) . '.pem';
	}

	/**
	 * Create new key pair
	 *
	 * @return KeyPair
	 */
	public function create(): KeyPair
	{
		// @link https://www.keylength.com/en/4/
		$option = $this->type === 'EC' ? new EcKeyOption('prime256v1') : new RsaKeyOption(2048);
		return (new KeyPairGenerator())->generateKeyPair($option);
	}
}