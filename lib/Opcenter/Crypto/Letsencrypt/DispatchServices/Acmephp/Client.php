<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt\DispatchServices\Acmephp;

use AcmePhp\Core\AcmeClient;
use AcmePhp\Core\Http\Base64SafeEncoder;
use AcmePhp\Core\Http\SecureHttpClientFactory;
use AcmePhp\Core\Http\ServerErrorHandler;
use AcmePhp\Ssl\KeyPair;
use AcmePhp\Ssl\Parser\KeyParser;
use AcmePhp\Ssl\Signer\DataSigner;
use Opcenter\Crypto\Letsencrypt;

class Client {
	/**
	 * AcmeClient $client
	 */
	protected $client;

	public function __construct(KeyPair $pair)
	{
		$factory = new SecureHttpClientFactory(
			new \GuzzleHttp\Client(),
			new Base64SafeEncoder(),
			new KeyParser(),
			new DataSigner(),
			new ServerErrorHandler()
		);
		$uri = 'https://'. Letsencrypt::activeServer();
		$this->client = new AcmeClient($factory->createSecureHttpClient($pair), $uri);
	}

	public function get(): AcmeClient
	{
		return $this->client;
	}
}

