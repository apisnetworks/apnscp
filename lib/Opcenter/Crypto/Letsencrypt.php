<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, April 2019
 */

namespace Opcenter\Crypto;

/**
 * Class Letsencrypt
 *
 * Helper utilities working with Let's Encrypt certificates
 *
 * @package Opcenter\Crypto
 */
class Letsencrypt
{
	const MAX_EXPIRY_DAYS = LETSENCRYPT_LOOKAHEAD_DAYS; /* 10 days */
	const MIN_EXPIRY_DAYS = LETSENCRYPT_LOOKBEHIND_DAYS;

	/** @param string HTTP URI prefix for acme validation */
	public const SYSCERT_NAME = 'MAIN';

	/**
	 * Active Let's Encrypt issuing server
	 *
	 * @return string
	 */
	public static function activeServer(): string
	{
		return static::debug() ? \Letsencrypt_Module::LETSENCRYPT_TESTING_SERVER : \Letsencrypt_Module::LETSENCRYPT_SERVER;
	}

	/**
	 * Issuing in debug mode
	 *
	 * @return bool
	 */
	public static function debug(): bool
	{
		return (bool)LETSENCRYPT_DEBUG;
	}

	/**
	 * Canonicalize server name to filesystem-compatible name
	 *
	 * @param string $server
	 * @return string
	 */
	public static function canonicalizeServer(string $server): string
	{
		return str_replace('/', '.', $server);
	}

	/**
	 * ACME home directory
	 *
	 * @return string
	 */
	public static function acmeDirectory(): string
	{
		return realpath(INCLUDE_PATH . \Module\Support\Letsencrypt::ACME_CERTIFICATE_BASE);
	}

	/**
	 * ACME storage Directory for all certificates
	 *
	 * @return string
	 */
	public static function acmeDataDirectory(): string
	{
		return self::acmeDirectory() . '/data';
	}

	/**
	 * ACME host-specific storage directory
	 *
	 * @param string $host
	 * @param null|string $server ACME server
	 * @return string
	 */
	public static function acmeSiteStorageDirectory($host, $server = null): string
	{
		return self::acmeDataDirectory() . '/certs/' .
			self::canonicalizeServer($server ?? self::activeServer()) . '/' . $host;
	}

	/**
	 * Get raw certificate components
	 *
	 * @param $site
	 * @return array|bool
	 */
	public static function getCertificateComponentData($site)
	{
		$acmePrefix = self::acmeSiteStorageDirectory($site);
		$files = array(
			'chain' => $acmePrefix . '/chain.pem',
			'crt'   => $acmePrefix . '/cert.pem',
			'key'   => $acmePrefix . '/key.pem'
		);

		foreach ($files as $k => $name) {
			if (!file_exists($name)) {
				return error("missing necessary LE component `%s'", basename($name));
			}
			$files[$k] = file_get_contents($name);
		}

		return $files;
	}

	/**
	 * Certificate expires within renewal window
	 *
	 * @param array     $x509
	 * @param \DateTime $now
	 * @return bool
	 */
	public static function expiring(array $x509): bool {
		$days = static::daysUntilExpiry($x509);
		// prevent Let's Encrypt-generated reminder, kicks off with 19 days remaining
		if ($days === 30 || $days === 20) {
			return true;
		}

		if ($days > self::MAX_EXPIRY_DAYS) {
			return false;
		}
		if ($days < self::MIN_EXPIRY_DAYS) {
			warn("Certificate for `%s' expired %d days ago. Maximum lookbehind is %d days ([letsencrypt] => lookbehind_days)",
				$x509['name'], $days, self::MIN_EXPIRY_DAYS
			);

			return false;
		}

		return true;
	}

	/**
	 * @param array $x509
	 * @return int
	 * @throws \Exception
	 */
	public static function daysUntilExpiry(array $x509): int
	{
		$dt = new \DateTime();
		$dt->setTimestamp($x509['validTo_time_t']);
		return (int)(new \DateTime())->diff($dt)->format('%R%a');
	}

	/**
	 * Let's Encrypt configured and present for server via internal subrequest
	 *
	 * @param bool $verify_peer verify peer certificate
	 * @return bool
	 */
	public static function bootstrapped(bool $verify_peer = true): bool
	{
		$context = stream_context_create(
			[
				'http' => [
					'method'           => 'GET',
					'timeout'          => 5,
					'protocol_version' => 1.1
				],
				'ssl'  => [
					'verify_peer' => $verify_peer
				]
			]
		);
		$try = silence(static function () use ($context) {
			return file_get_contents('https://' . SERVER_NAME, false, $context);
		});

		return $try !== false;
	}
}