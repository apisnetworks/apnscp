<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database\PostgreSQL\Generic;

	class Ssh
	{
		/**
		 * Assign port indexes to database
		 *
		 * @param int       $site_id
		 * @param int|array $ports
		 * @return string
		 */
		public function assignPortIndex(int $site_id, $ports): string
		{
			$str = 'INSERT INTO ssh_port_indexes (site_id, port_index) VALUES';
			$pstr = [];
			foreach ((array)$ports as $port) {
				$port = (int)$port;
				$pstr[] = "({$site_id}, {$port})";
			}

			return $str . implode(',', $pstr);
		}

		/**
		 * Remove port indexes from database
		 *
		 * @param int       $site_id
		 * @param int|array $ports
		 * @return string
		 */
		public function deletePortIndex(int $site_id, $ports): string
		{
			return "DELETE FROM ssh_port_indexes WHERE site_id = {$site_id} AND port_index IN(" .
				implode(',', array_map('\intval', (array)$ports)) . ')';
		}
	}