<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2024
	 */

	namespace Opcenter\Database\PostgreSQL;

	use Opcenter\Database\PostgreSQL;
	use Opcenter\System\GenericSystemdService;

	class Service extends GenericSystemdService
	{
		protected const SERVICE = 'postgresql';

		public static function configuration(): string
		{
			return '/usr/pgsql-' . PostgreSQL::version() . '/config/postgresql.conf';
		}
	}