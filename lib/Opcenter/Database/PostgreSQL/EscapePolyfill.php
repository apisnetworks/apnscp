<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, January 2023
 */

namespace Opcenter\Database\PostgreSQL\EscapePolyfill;

function pg_escape_string($str) {
	return \pg_escape_string(\PostgreSQL::initialize()->getHandler(), $str);
}

function pg_escape_literal($str) {
	return \pg_escape_literal(\PostgreSQL::initialize()->getHandler(), $str);
}

function pg_escape_bytea($str) {
	return \pg_escape_bytea(\PostgreSQL::initialize()->getHandler(), $str);
}

function pg_escape_identifier($str) {
	return \pg_escape_identifier(\PostgreSQL::initialize()->getHandler(), $str);
}

function pg_query($str) {
	return \pg_query(\PostgreSQL::initialize()->getHandler(), $str);
}