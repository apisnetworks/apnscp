<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
 */

namespace Opcenter\Database\MySQL;

class DefinerRemap {
	/**
	 * @var string
	 */
	private $db;

	/**
	 * @var \PDO
	 */
	private $connection;

	/**
	 * DefinerRemap constructor.
	 *
	 * @param string    $db database
	 * @param \PDO|null $conn
	 */
	public function __construct(string $db, \PDO $conn = null)
	{
		$this->db = $db;

		if ($conn) {
			$this->setPrivilegedConnection($conn);
		}
	}

	/**
	 * Set connection
	 *
	 * @param \mysqli $conn
	 * @return self
	 */
	public function setPrivilegedConnection(\PDO $conn): self
	{
		$this->connection = $conn;
		$this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		return $this;
	}

	/**
	 * Remap definer across all resources
	 *
	 * @param string $db
	 * @param string $newuser
	 * @param string $newhost
	 * @return bool
	 */
	public function remapAll(string $newuser, string $newhost = 'localhost'): bool
	{
		foreach (['trigger','event', 'procedure', 'view'] as $type) {
			foreach ($this->fetchAll($type) as $res) {
				if (!$this->{'remap' . ucwords($type)}($res['name'], $newuser, $newhost, $res)) {
					return error("Failed remap on `%s' named `%s' to `%s' - aborting", $type, $res['name'], $newuser);
				}
			}
		}
		return true;
	}

	/**
	 * Fetch all resource types on database
	 *
	 * @param string $resource
	 * @return array
	 */
	protected function fetchAll(string $resource): array
	{
		$schemaDatabaseField = $this->databaseColumnFromResource($resource);
		$schemaNameField = $this->resourceColumnFromResource($resource);
		$tableName = $this->resourceAsTable($resource);
		// * avoids an extra trip...
		$query = "SELECT *, 
			CHARACTER_SET_CLIENT AS charset, 
			COLLATION_CONNECTION AS collation, 
			DEFINER AS definer, 
			$schemaNameField AS name 
			FROM {$tableName} 
			WHERE {$schemaDatabaseField} = ?";
		return $this->wrapQuery($query, [$this->db])->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Fetch resource information
	 *
	 * @param string $resource
	 * @param string $rname
	 * @return array
	 */
	private function fetchResourceRow(string $resource, string $rname): array
	{
		$schemaDatabaseField = $this->databaseColumnFromResource($resource);
		$schemaNameField = $this->resourceColumnFromResource($resource);
		$tableName = $this->resourceAsTable($resource);
		// * avoids an extra trip...
		$query = "SELECT *, 
			CHARACTER_SET_CLIENT AS charset, 
			COLLATION_CONNECTION AS collation, 
			DEFINER AS definer, 
			$schemaNameField AS name 
			FROM {$tableName} 
			WHERE {$schemaDatabaseField} = ? AND {$schemaNameField} = ?";

		return array_get($this->wrapQuery($query, [$this->db, $rname])->fetchAll(\PDO::FETCH_ASSOC), 0, []);
	}

	/**
	 * Get IS table from resource
	 *
	 * @param string $resource
	 * @return string
	 */
	private function resourceAsTable(string $resource): string {
		switch (strtolower($resource)) {
			case 'procedure':
			case 'function':
				return 'INFORMATION_SCHEMA.ROUTINES';
			case 'trigger':
				return 'INFORMATION_SCHEMA.TRIGGERS';
			case 'event':
				return 'INFORMATION_SCHEMA.EVENTS';
			case 'view':
				return 'INFORMATION_SCHEMA.VIEWS';
		}
		fatal("Unknown resource `%s'", $resource);
	}

	/**
	 * Get database column for resource
	 *
	 * @param string $resource
	 * @return string
	 */
	private function databaseColumnFromResource(string $resource): string
	{
		switch (strtolower($resource)) {
			case 'procedure':
			case 'function':
				return 'ROUTINE_SCHEMA';
			case 'trigger':
				return 'TRIGGER_SCHEMA';
			case 'event':
				return 'EVENT_SCHEMA';
			case 'view':
				return 'TABLE_SCHEMA';
		}
		fatal("Unknown resource `%s'", $resource);
	}

	/**
	 * Get resource column name
	 *
	 * @param string $resource
	 * @return string
	 */
	private function resourceColumnFromResource(string $resource): string
	{
		switch (strtolower($resource)) {
			case 'procedure':
			case 'function':
				return 'ROUTINE_NAME';
			case 'trigger':
				return 'TRIGGER_NAME';
			case 'event':
				return 'EVENT_NAME';
			case 'view':
				return 'TABLE_NAME';
		}
		fatal("Unknown resource `%s'", $resource);
	}

	public function remapTrigger(string $rname, string $newuser, string $newhost = 'localhost', array $res = null): bool
	{
		if (!$res && !($res = $this->fetchResourceRow('trigger', $rname))) {
			fatal("Unknown trigger `%s'", $rname);
		}

		$this->setSessionEncoding($res);
		debug('Remap trigger `%s`.`%s`', $this->db, $rname);
		$this->connection->exec('USE ' . $this->db);

		$row = $this->connection->query('SHOW CREATE TRIGGER `' . $res['TRIGGER_SCHEMA'] . '`.`' . $rname . '`')->fetch(\PDO::FETCH_ASSOC);
		$body = $row['SQL Original Statement'];
		$createSql = preg_replace(
			'|CREATE DEFINER=[^ ]+|',
			'CREATE DEFINER=' . $this->connection->quote($newuser) . '@' . $this->connection->quote($newhost),
			$body,
			1
		);

		$this->connection->beginTransaction();
		$this->connection->exec('LOCK TABLES `' . $res['EVENT_OBJECT_TABLE'] . '` WRITE');
		$this->connection->exec('DROP TRIGGER `' . $rname . '`');
		$this->connection->exec($createSql);
		$this->connection->exec('UNLOCK TABLES');
		return $this->connection->commit();
	}

	/**
	 * Remap event definer
	 *
	 * @param string     $rname
	 * @param string     $newuser
	 * @param string     $newhost
	 * @param array|null $res
	 * @return bool
	 */
	public function remapEvent(string $rname, string $newuser, string $newhost = 'localhost', array $res = null): bool
	{
		if (!$res && !($res = $this->fetchResourceRow('event', $rname))) {
			fatal("Unknown event `%s'", $rname);
		}

		$this->setSessionEncoding($res);
		debug('Remap event `%s`.`%s`', $this->db, $rname);
		$this->connection->exec('USE ' . $this->db);

		$this->wrapQuery(
			'ALTER DEFINER=?@? EVENT `' . $res['EVENT_SCHEMA'] . '`.`' . $rname . '` ON COMPLETION ' . $res['ON_COMPLETION'],
			[
				$newuser,
				$newhost
			]
		);

		return true;
	}


	/**
	 * Remap stored procedure
	 *
	 * @param string     $rname
	 * @param string     $newuser
	 * @param string     $newhost
	 * @param array|null $res
	 * @return bool
	 */
	public function remapProcedure(string $rname, string $newuser, string $newhost = 'localhost', array $res = null): bool
	{
		if (!$res && !($res = $this->fetchResourceRow('procedure', $rname))) {
			fatal("Unknown procedure `%s'", $rname);
		}

		$this->setSessionEncoding($res);
		debug('Remap procedure `%s`.`%s`', $this->db, $rname);
		$this->connection->exec('USE ' . $this->db);

		$row = $this->connection->query('SHOW CREATE ' . $res['ROUTINE_TYPE'] . ' `' . $res['ROUTINE_SCHEMA'] . '`.`' . $rname . '`')->fetch(\PDO::FETCH_ASSOC);
		$body = $row['Create ' . ucwords(strtolower($res['ROUTINE_TYPE']))];
		$createSql = preg_replace(
			'|CREATE DEFINER=[^ ]+|',
			'CREATE DEFINER=' . $this->connection->quote($newuser) . '@' . $this->connection->quote($newhost),
			$body,
			1
		);
		$this->connection->exec('DROP ' . $res['ROUTINE_TYPE'] . ' `' . $res['ROUTINE_SCHEMA'] .'`.`' . $rname . '`');
		$this->connection->exec($createSql);

		return true;
	}

	public function remapView(string $rname, string $newuser, string $newhost = 'localhost', array $res = null): bool
	{
		if (!$res && !($res = $this->fetchResourceRow('view', $rname))) {
			fatal("Unknown view `%s'", $rname);
		}

		$this->setSessionEncoding($res);
		debug('Remap view `%s`.`%s`', $this->db, $rname);
		$this->connection->exec('USE ' . $this->db);

		$query = 'ALTER DEFINER=?@? VIEW `' . $res['TABLE_SCHEMA'] . '`.`' . $rname . '` AS ' . $res['VIEW_DEFINITION'];
		$this->wrapQuery($query, [$newuser, $newhost]);
		return true;
	}

	/**
	 * Set session encoding for following query
	 *
	 * @param array $res resource from fetchResourceRow()
	 */
	private function setSessionEncoding(array $res): void
	{
		$query = 'SET SESSION
			character_set_client = ' . $this->connection->quote($res['charset']) . ',
			collation_connection = ' . $this->connection->quote($res['collation']);
		$this->connection->exec($query);
	}

	/**
	 * Apply query
	 *
	 * @param string $query
	 * @return mixed
	 */
	private function wrapQuery(string $query, array $args = []): \PDOStatement
	{
		if (null === $this->connection) {
			fatal('Master connection is not set!');
		}

		if (!($stmt = $this->connection->prepare($query)) || !$stmt->execute($args)) {
			fatal('Failed to run query: %s', $query);
		}

		return $stmt;
	}
}
