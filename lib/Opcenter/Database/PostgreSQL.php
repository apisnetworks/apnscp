<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database;

	class PostgreSQL extends DatabaseCommon implements DatabaseInterface
	{
		const PREFIX_MAP = 'pgsql.prefixmap';
		const USER_MAP = 'pgsql.usermap';

		protected static $vendors = [];

		/**
		 * Move a database
		 *
		 * @param string $old
		 * @param string $new
		 * @return bool
		 */
		public static function moveDatabase(string $old, string $new): bool
		{
			if (self::databaseExists($new)) {
				return error("target database `%s' exists");
			} else if (!self::databaseExists($old)) {
				return error("source datatabase `%s' does not exist", $old);
			}
			$query = self::vendor()->moveDatabase($old, $new);
			static::revokeAndTerminate($old);
			$db = \PostgreSQL::initialize();

			$rs = $db->query($query);
			static::resumeConnections($new);

			return (bool)$rs;
		}

		public static function resumeConnections(string $db): bool
		{
			$query = self::vendor()->permitConnections($db);
			$db = \PostgreSQL::initialize();

			return (bool)$db->query($query);
		}

		/**
		 * Verify if database exists
		 *
		 * @param string      $dbname database name
		 * @param string|null $user   optional user to confirm against
		 * @return bool
		 */
		public static function databaseExists(string $dbname, string $user = null): bool
		{
			$db = \PostgreSQL::initialize();
			$query = self::vendor()->databaseExists($dbname, $user);
			$rs = $db->query($query);

			return $rs && $rs->num_rows() >= 1;
		}

		/**
		 * Load a version-specific PostgreSQL handler
		 *
		 * @param string $class type of handler to vendor
		 * @return mixed PostgreSQL\Generic\<$class> instance
		 */
		public static function vendor(string $class = 'Query')
		{
			if (isset(self::$vendors[$class])) {
				return new self::$vendors[$class];
			}
			$version = self::version();
			$maj = floor($version / 10000);
			$min = floor(($version % 1000) / 100);
			$namespace = __CLASS__ . '\\Generic';
			$class = ucwords($class);
			$try = null;
			for ($i = (int)$min; $i >= 0; $i--) {
				$try = __CLASS__ . '\\v' . $maj . str_pad((string)$i, 2, '0', STR_PAD_LEFT) . '\\' . $class;
				if (class_exists($try)) {
					$namespace = $try;
					break;
				}
				$try = null;
			}
			if (null === $try) {
				$namespace .= '\\' . $class;
			}
			self::$vendors[$class] = $namespace;

			return new $namespace();
		}

		public static function version(): ?int
		{
			static $version;
			if (!empty($version)) {
				return $version;
			}
			$db = \PostgreSQL::initialize();
			$handle = $db->getHandler();
			if ((PHP_MAJOR_VERSION === 7 && !\is_resource($handle)) || (PHP_MAJOR_VERSION === 8 && !is_object($handle)))
			{
				// weird bug in PHP, $handle becomes unreferenced
				return null;
			}

			$version = array_get(pg_version($handle), 'server');
			$varr = explode('.', $version);
			$version = $varr[0] * 10000;
			if (isset($varr[1])) {
				$version += $varr[1] * 100;
			}
			if (isset($varr[2])) {
				$version += $varr[2];
			}

			return $version;
		}

		/**
		 * Rename a tablespace
		 *
		 * @param string $old
		 * @param string $new
		 * @return bool
		 */
		public static function moveTablespace(string $old, string $new): bool
		{
			if (!self::tablespaceExists($old)) {
				return error("tablespace `%s' does not exist", $old);
			} else if (self::tablespaceExists($new)) {
				return error("tablespace `%s' exists", $new);
			}
			$query = self::vendor()->moveTablespace($old, $new);
			$db = \PostgreSQL::initialize();
			$rs = $db->query($query);
			if (!$rs) {
				return error("failed to move tablespace `%s'", $old);
			}

			return true;
		}

		/**
		 * Verify a tablespace exists
		 *
		 * @param string $tablespace
		 * @return bool
		 */
		public static function tablespaceExists(string $tablespace): bool
		{
			$db = \PostgreSQL::initialize();
			$query = self::vendor()->tablespaceExists($tablespace);
			$rs = $db->query($query);
			if (!$rs) {
				fatal("failed to query if tablespace `%s' exists", $tablespace);
			}

			return $db->num_rows() > 0;
		}

		/**
		 * Rename a PostgreSQL user
		 *
		 * @param string $old old name
		 * @param string $new new name
		 * @return bool
		 */
		public static function renameUser(string $old, string $new): bool
		{
			if (!preg_match(\Regex::SQL_USERNAME, $new)) {
				return error("invalid user `%s'", $new);
			} else if (self::userExists($new)) {
				return error("cannot rename user - new user `%s' exists", $new);
			} else if (!self::userExists($old)) {
				return error("cannot rename user - source user `%s' does not exist", $old);
			} else if (\strlen($new) > \Pgsql_Module::IDENTIFIER_MAXLEN) {
				return error('cannot rename user - user length exceeds max length %d',
					\Pgsql_Module::IDENTIFIER_MAXLEN);
			}
			$db = \PostgreSQL::initialize();
			$query = self::vendor()->renameUser($old, $new);
			$rs = $db->query($query);

			return (bool)$rs;
		}

		/**
		 * PostgreSQL User exists
		 *
		 * @param string $user
		 * @return bool
		 */
		public static function userExists(string $user): bool
		{
			$conn = \PostgreSQL::initialize();
			$query = self::vendor()->userExists($user);
			$rs = $conn->query($query);

			return $rs && $conn->num_rows() > 0;
		}

		public static function changeTablespaceOwner(string $tblspace, string $owner): bool
		{
			$db = \PostgreSQL::initialize();
			if (self::userExists($owner)) {
				return error("cannot change tablespace owner - user `%s' does not exist", $owner);
			} else if (!self::tablespaceExists($tblspace)) {
				return error("cannot change tablespace owner - tablespace `%s' does not exist", $tblspace);
			}
			$query = self::vendor()->changeTablespaceOwner($tblspace, $owner);
			$rs = $db->query($query);

			return (bool)$rs;
		}

		/**
		 * Initialize a PostgreSQL tablespace
		 *
		 * @param string $name     tablespace name
		 * @param string $location tablespace location
		 * @param string $owner    tablespace owner
		 * @return bool
		 */
		public static function initializeTablespace(
			string $name,
			string $location = \Sql_Module::PGSQL_DATADIR,
			string $owner = 'root'
		): bool {
			$db = \PostgreSQL::initialize();
			$name = self::normalizeTablespace($name);
			if (!self::userExists($owner)) {
				return error("cannot prep tablespace, user `%s' does not exist", $owner);
			}
			$query = self::vendor('query')->createTablespace($name, $location, $owner);
			$db->query($query);

			if ($db->error()) {
				return error("unable to create tablespace `%s'", $name);
			}

			return true;
		}

		/**
		 * Normalize a tablespace
		 *
		 * @param string $tablespace
		 * @return string
		 */
		public static function normalizeTablespace(string $tablespace): string
		{
			return str_replace('.', '_', $tablespace);
		}

		/**
		 * Verify if user has a tablespace provisioned
		 *
		 * @param string $user
		 * @return bool
		 */
		public static function userHasTablespace(string $user): bool
		{
			$db = \PostgreSQL::initialize();
			$query = self::vendor()->userHasTablespace($user);
			$rs = $db->query($query);

			return $rs->num_rows() >= 1;
		}

		public static function flushTables(): bool
		{
			return true;
		}

		/**
		 * Drop a database
		 *
		 * @param string $database database name to drop
		 * @return bool
		 */
		public static function dropDatabase(string $database): bool
		{
			$dbconn = \PostgreSQL::initialize();
			$query = self::vendor()->dropDatabase($database);
			if (!self::revokeAndTerminate($database)) {
				return error("Failed to revoke database access from `%s'", $database);
			}
			$rs = $dbconn->query($query);

			return (bool)$rs;
		}

		/**
		 * Revoke access and terminate connections prior to dropping
		 *
		 * @param string $database
		 * @return bool
		 */
		public static function revokeAndTerminate(string $database): bool
		{
			return self::revokeDatabaseAccess($database) && self::terminateActiveConnectionsOnDatabase($database);
		}

		/**
		 * Path for database tablespace
		 *
		 * @param string $database
		 * @return string|null null if not found or not allocated into tablespace
		 * @throws \PostgreSQLError
		 */
		public static function tablespaceLocationFromDatabase(string $database): ?string
		{
			$dbconn = \PostgreSQL::initialize();
			$query = self::vendor()->tablespaceLocation($database);
			$rs = $dbconn->query($query);
			if (!$rs->num_rows()) {
				return null;
			}

			return $rs->fetch_object()->location ?: null;
		}

		/**
		 * Revoke access on database to all
		 *
		 * @param string $database
		 * @return bool
		 * @throws \PostgreSQLError
		 */
		public static function revokeDatabaseAccess(string $database): bool
		{
			$pg = \PostgreSQL::initialize();
			$query = self::vendor()->denyAllDatabaseConnections($database);

			return (bool)$pg->query($query);
		}

		public static function terminateActiveConnectionsOnDatabase(string $database): bool
		{
			$pg = \PostgreSQL::initialize();
			$q = 'SELECT pid, pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = ' .
				pg_escape_literal($pg->getHandler(), $database) . ' AND pid <> pg_backend_pid()';

			return (bool)$pg->query($q);
		}

		public static function terminateUser(string $user): bool
		{
			$pg = \PostgreSQL::initialize();
			$q = 'SELECT pid, pg_terminate_backend(pid) FROM pg_stat_activity WHERE "usename" = ' .
				pg_escape_literal($pg->getHandler(), $user);

			return (bool)$pg->query($q);
		}

		/**
		 * Delete a user ("role")
		 *
		 * @param string $user
		 * @param string $host
		 * @return bool
		 */
		public static function deleteUser(string $user, string $host = null)
		{
			$dbconn = \PostgreSQL::initialize();
			$query = self::vendor()->deleteUser($user, $host);
			$rs = $dbconn->query($query);

			return (bool)$rs;
		}

		/**
		 * Remove database grants from user
		 *
		 * @param string      $db
		 * @param string      $user
		 * @param string|null $hostname
		 * @return bool
		 */
		public static function dropDatabaseGrants(string $db, string $user, string $hostname = null): bool
		{
			return true;
		}

		public static function createUser(string $user, string $password, string $hostname = 'localhost'): bool
		{
			if (!preg_match(\Regex::SQL_USERNAME, $user)) {
				return error("invalid user `%s'", $user);
			} else if (\strlen($user) > \Pgsql_Module::IDENTIFIER_MAXLEN) {
				return error('user length exceeds identifier max %d', \Pgsql_Module::IDENTIFIER_MAXLEN);
			}
			$pghandler = \PostgreSQL::initialize();
			$vendor = static::vendor();
			$rs = $pghandler->query($vendor->createUser($user, $password));

			if ($rs->error) {
				return error("user creation for `%s' failed", $user);
			}

			return true;
		}

		/**
		 * Get active connections to named database
		 *
		 * @param string|null $database optional database to restrict
		 * @return array
		 * @throws \PostgreSQLError
		 */
		public static function getActiveConnectionsOnDatabase(string $database = null): array
		{
			$pghandler = \PostgreSQL::initialize();
			$query = 'SELECT pid AS process_id, usename AS username, datname AS database_name,
				client_addr AS client_address, application_name, 
				EXTRACT(EPOCH FROM backend_start) AS backend_start, state, 
				EXTRACT(EPOCH FROM state_change) AS state_change FROM pg_stat_activity 
				WHERE datname';
			if ($query) {
				$query .= " = '" . $pghandler->escape_string($database) . "'";
			}
			$rs = $pghandler->query($query);
			$results = [];
			while (null !== ($record = $rs->fetch_object())) {
				$record->process_id = (int)$record->process_id;
				$record->state_change = (float)$record->state_change;
				$record->backend_start = (float)$record->backend_start;
				$results[] = $record;
			}

			return $results;
		}

		/**
		 * Grant access on database after a revoke
		 *
		 * @param string $database
		 * @param string $role
		 * @return bool
		 * @throws \PostgreSQLError
		 */
		public static function permitDatabaseAccess(string $database, string $role): bool
		{
			$pg = \PostgreSQL::initialize();
			$q = 'GRANT CONNECT ON DATABASE ' . pg_escape_identifier($pg->getHandler(), $database) . ' TO GROUP ' . pg_escape_identifier($pg->getHandler(), $role);

			return (bool)$pg->query($q);
		}

		public static function setRole(string $user, string $role): bool
		{
			if (!preg_match(\Regex::SQL_USERNAME, $user)) {
				return error("invalid user `%s'", $user);
			} else if (\strlen($user) > \Pgsql_Module::IDENTIFIER_MAXLEN) {
				return error('user length exceeds identifier max %d', \Pgsql_Module::IDENTIFIER_MAXLEN);
			}
			$vendor = static::vendor();
			$pghandler = \PostgreSQL::initialize();
			$rs = $pghandler->query($vendor->placeInRole($user, $role));

			if ($rs->error) {
				return error("unable to place user `%s' in role `%s'", $user, $role);
			}

			return true;
		}

		/**
		 * Dump the tablespace
		 *
		 * @param string $user
		 * @param string $host
		 * @return bool
		 */
		public static function deleteMainUser(string $user, string $host = null)
		{
			$tablespace = self::getTablespaceFromUser($user);
			if ($tablespace) {
				static::dropTablespace($tablespace);
			}

			return parent::deleteMainUser($user, $host);
		}

		/**
		 * Get named tablespace from user
		 *
		 * @param string $user
		 * @return null|string
		 */
		public static function getTablespaceFromUser(string $user): ?string
		{
			$db = \PostgreSQL::initialize();
			$query = self::vendor()->getTablespaceFromUser($user);
			$db->query($query);
			if ($db->num_rows() < 1) {
				return null;
			}

			return $db->fetch_object()->spcname;
		}

		/**
		 * Get all tables from database
		 *
		 * @param string $db
		 * @return array
		 */
		public static function getTablesFromDatabase(string $db): array
		{
			// reset DB
			$conn = \PostgreSQL::pdo(true);
			$conn->exec("USE " . $conn->quote($db));
			$query = self::vendor()->tablesFromDatabase($db);
			$dbs = [];
			if ($rs = $conn->query($query)) {
				$dbs = $rs->fetchAll(\PDO::FETCH_COLUMN);
			}
		 	return $dbs;
		}

		/**
		 * Drop a tablespace
		 *
		 * @param string $tablespace
		 * @return bool
		 */

		public static function dropTablespace(string $tablespace): bool
		{
			$dbconn = \PostgreSQL::initialize();
			$query = self::vendor()->dropTablespace($tablespace);
			$rs = $dbconn->query($query);
			return (bool)$rs;
		}

		/**
		 * Set .pgpass field value
		 *
		 * @param string       $path
		 * @param string|array $field
		 * @param mixed        $val
		 * @return bool
		 */
		public static function setUserConfigurationField($path, $field, $val = null): bool
		{
			$old = static::getUserConfiguration($path);
			if (!\is_array($field)) {
				$field = [$field => $val];
			}
			$struct = array_replace([
				'hostname' => '*',
				'port'     => '*',
				'database' => '*',
				'username' => '*',
				'password' => null
			], $old, $field);

			return file_put_contents($path,
					$struct['hostname'] . ':' .
					$struct['port'] . ':' .
					$struct['database'] . ':' .
					$struct['username'] . ':' .
					$struct['password']
				) > 0 && chmod($path, 0600);
		}

		public static function getUserConfiguration(string $path): array
		{
			if (!file_exists($path)) {
				return [];
			}
			$pgpass = file_get_contents($path);
			/**
			 * @link http://wiki.postgresql.org/wiki/Pgpass
			 */
			if (preg_match(\Regex::SQL_PGPASS, $pgpass, $matches)) {
				return array_filter($matches, static function ($k) {
					return \is_string($k);
				}, ARRAY_FILTER_USE_KEY);
			}

			// localhost uses SO_PEERCRED validation
			// old format, single token (password)
			return [
				'hostname' => '127.0.0.1',
				'port'     => '*',
				'database' => '*',
				'username' => '*',
				'password' => $pgpass
			];
		}
	}