<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, April 2022
 */

namespace Opcenter\Filesystem;

use Opcenter\Contracts\Virtualizable;

class Whiteout implements Virtualizable {
	// @var string filesystem root directory
	private string $root;

	public function __construct(string $root = '/')
	{
		$this->root = $root;
	}

	public static function bindTo(string $root = '/')
	{
		return new static($root);
	}

	/**
	 * Block file from appearing is upperdir
	 *
	 * @param string $file
	 * @return bool
	 */
	public function whiteout(string $file): bool
	{
		if ($file[0] !== '/') {
			fatal("Path must be absolute");
		}

		if (is_dir($this->root . $file)) {
			return error("Opaque directories not supported");
		}

		$params = $this->getFilesystemLayersFromPath(dirname($this->root . $file));
		foreach ($params['lowerdir'] as $dir) {
			// exists in immutable layer
			if (file_exists($dir . $file)) {
				return unlink($this->root . $file);
			}
		}

		// @TODO convert to FFI
		return debug("File `%s' does not exist in lowerdir", $file);

	}

	/**
	 * Expose a previously opaque file or directory
	 *
	 * @param string $file
	 * @return bool
	 */
	public function expose(string $file): bool
	{
		if ($file[0] !== '/') {
			fatal("Path must be absolute");
		}

		if (file_exists($this->root . $file)) {
			return error("File `%s' already exposed", $file);
		}

		$params = $this->getFilesystemLayersFromPath(dirname($this->root . $file));

		if (!file_exists($params['upperdir'] . $file)) {
			return warn("File `%s' does not exist in upperdir", $file);
		}

		if ('char' !== filetype($params['upperdir'] . $file)) {
			return error("Invalid special device file in `%(dir)s/%(file)s'", [
				'dir' => $params['upperdir'], 'file' => $file
			]);
		}

		$devId = Mount::getDeviceId($params['upperdir'] . $file);
		if ($devId[0] !== 0 || $devId[1] !== 0) {
			return error("Invalid major:minor device");
		}

		return unlink($params['upperdir'] . $file);
	}


	/**
	 * File is accessible on upperdir
	 *
	 * @param string $file
	 * @return bool
	 */
	public function blocked(string $file): bool
	{
		if (null === ($params = $this->getFilesystemLayersFromPath(dirname($this->root . $file)))) {
			// account removed
			return false;
		}

		if (!file_exists($params['upperdir'] . $file)) {
			return false;
		}

		if ('char' !== filetype($params['upperdir'] . $file)) {
			return false;
		}

		$devId = Mount::getDeviceId($params['upperdir'] . $file);
		return $devId[0] === 0 && $devId[1] === 0;
	}

	/**
	 * OverlayFS layers from path
	 *
	 * @param string $path
	 * @return array|null
	 */
	private function getFilesystemLayersFromPath(string $path): ?array {
		if (!($mountPath = Mount::getMount($path))) {
			return null;
		}
		$options = Mount::getMountOptions($mountPath);
		return array_build(array_filter($options, static function ($opt) {
			return false !== strpos($opt, '=');
		}), static function ($key, $opt) {
			$parts = explode('=', $opt, 2);
			if ($parts[0] !== 'lowerdir') {
				return $parts;
			}
			return [$parts[0], explode(':', $parts[1])];
		});
	}

}