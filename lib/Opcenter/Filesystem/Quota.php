<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Filesystem;

	class Quota
	{
		/**
		 * Set quota
		 *
		 * string|int $user
		 *
		 * @param string|int $user username or uid
		 * @param int        $bhard
		 * @param int        $ihard
		 * @param int        $bsoft
		 * @param int        $isoft
		 * @return bool
		 */
		public static function setUser($user, int $bhard = 0, int $ihard = 0, int $bsoft = 0, int $isoft = 0): bool
		{
			return static::set($user, 'user', $bhard, $ihard, $bsoft, $isoft);
		}

		/**
		 * Quota wrapper
		 *
		 * @param        $item
		 * @param string $type
		 * @param int    $bhard
		 * @param int    $ihard
		 * @param int    $bsoft
		 * @param int    $isoft
		 * @return bool
		 */
		private static function set(
			$item,
			string $type,
			int $bhard,
			int $ihard = 0,
			int $bsoft = 0,
			int $isoft = 0
		): bool {
			if ($type !== 'group' && $type !== 'user') {
				fatal("unknown quota specifier `%s'", $type);
			}
			$flags = ['-a'];
			if ($type === 'group') {
				$flags[] = '-g';
			} else {
				$flags[] = '-u';
			}
			$ret = \Util_Process::exec(['setquota', ...$flags, '%s', '%u', '%u', '%u', '%u'], $item, $bsoft, $bhard, $isoft, $ihard);

			return $ret['success'] ?: error("failed to set quota for `%s': %s", $item, $ret['stderr']);
		}

		/**
		 * Get user(s) quota(s)
		 *
		 * @param int|string|array $user
		 * @param string           $device mount point
		 * @return array|null
		 */
		public static function getUser($user, string $device = FILESYSTEM_MOUNTPOINT): ?array
		{
			return static::quotaWrapper('user', $user, $device);
		}

		/**
		 * Get group quota
		 *
		 * @param array|string|int $group
		 * @param string     $device mount point
		 * @return array|null
		 */
		public static function getGroup($group, string $device = FILESYSTEM_MOUNTPOINT): ?array
		{
			return static::quotaWrapper('group', $group, $device);
		}

		/**
		 * @param string $mode
		 * @param        $usrgrp
		 * @param string $device
		 * @return array|null
		 */
		private static function quotaWrapper(string $mode, $usrgrp, string $device = FILESYSTEM_MOUNTPOINT): ?array
		{
			if (!\is_array($usrgrp) || \count($usrgrp) === 1) {
				// occasionally a stale .socket mountpoint is reflected in quota
				// and emits stderr, squelch this
				$quota_rep = \Util_Process::exec(['quota', '-w', '-v', '%s', ...array_values((array)$usrgrp)], [
					$mode === 'user' ? '-u' : '-g'
				], [0, 1/** over quota */], [
					'mute_stderr' => true
				]);

				if ($quota_rep['return'] === 1) {
					if (strpos($quota_rep['stderr'], 'does not exist')) {
						fatal('One or more specified %s does not exist during quota query', $mode);
					}

					if (empty($quota_rep['stdout'])) {
						if (!self::supported()) {
							$features = FILESYSTEM_TYPE === 'xfs' ? ['usrquota', 'grpquota'] : [
								'usrjquota',
								'grpjquota'
							];

							return nerror([
								':err_quota_filesystem_incomplete',
								'Filesystem %(path)s is missing %(features)s. Run `%(cmd)s\' then reboot server'
							], [
								'path' => Mount::getMount(FILESYSTEM_VIRTBASE),
								'features' => implode(', ', $features),
								'cmd' => 'upcp -sbf filesystem/make-mounts'
							]);
						}

						fatal('Failed to query quota');
					}
				}

				$regex = \Regex::compile(\Regex::QUOTA_USRGRP, ['device' => $device]);
				preg_match_all($regex, $quota_rep['stdout'], $quotas, PREG_SET_ORDER);
			} else {
				$quota_rep = \Util_Process_Safe::exec(['repquota', '-v', '-%s', '-n', '%s'], [
					$mode === 'user' ? 'u' : 'g',
					$device
				]);
				$re = \Regex::compile(\Regex::QUOTA_REPQUOTA_C, implode('|', $usrgrp));
				preg_match_all($re, $quota_rep['stdout'], $quotas, PREG_SET_ORDER);
			}


			$quota_stat = [];
			foreach ($quotas as $quota) {
				if (!empty($quota['device']) && $quota['device'] !== $device) {
					continue;
				}
				$uid = (int)($quota['id'] ?? ($quota['uid'] ?: $quota['gid']));
				$quota_stat[$uid] = array(
					'qused' => (int)$quota['qused'],
					'qsoft' => (int)$quota['qsoft'],
					'qhard' => (int)$quota['qhard'],
					'fused' => (int)$quota['fileused'],
					'fsoft' => (int)$quota['filesoft'],
					'fhard' => (int)$quota['filehard']
				);
			}
			if (\is_array($usrgrp) && $quota_stat) {
				// backfill any missing values
				foreach (array_diff_key(array_flip($usrgrp), $quota_stat) as $missing) {
					$quota_stat[$usrgrp[$missing]] = array_fill_keys(array_keys(current($quota_stat)), 0);
				}
				return $quota_stat;
			}

			return $quota_stat ? array_pop($quota_stat) : null;
		}

		/**
		 * Set group quota
		 *
		 * @param     $group
		 * @param int $bhard
		 * @param int $ihard
		 * @param int $bsoft
		 * @param int $isoft
		 * @return bool
		 */
		public static function setGroup($group, int $bhard = 0, int $ihard = 0, int $bsoft = 0, int $isoft = 0): bool
		{
			return static::set($group, 'group', $bhard, $ihard, $bsoft, $isoft);
		}

		/**
		 * Quota features supported by filesystem
		 *
		 * @return bool
		 */
		public static function supported(string $path = null): bool
		{
			static $enabled;
			if (!$path && isset($enabled)) {
				return $enabled;
			}

			$fstype = !$path ? FILESYSTEM_TYPE : Mount::getFilesystemType($path);
			$features = (array)Mount::getMountOptions($path ?? FILESYSTEM_VIRTBASE);
			$found = match($fstype) {
				'xfs' => array_intersect(['usrquota', 'grpquota'], $features),
				'ext4' => array_filter(
					$features,
					static fn($x) => str_starts_with($x, 'usrjquota=') || str_starts_with($x, 'grpjquota=')
					),
				default => []
			};

			if ($path) {
				return \count($found) === 2;
			}

			return $enabled = \count($found) === 2;
		}
	}