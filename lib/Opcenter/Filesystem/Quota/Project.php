<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2020
	 */

	namespace Opcenter\Filesystem\Quota;

	use Opcenter\Filesystem\Mount;
	use Opcenter\Map\Textfile;
	use Opcenter\Reseller;

	class Project
	{
		const XFS_PROG = '/usr/sbin/xfs_quota';
		const FLAG_MAP = [
			'expert'   => 'x',
			'foreign'  => 'f',
			'projfile' => 'D',
			'projid'   => 'd'
		];
		const PROJID_MAP = '/etc/projid';
		const PROJECT_MAP = '/etc/projects';

		const ERR_FEATURE_UNSUPPORTED = [':err_quota_project_unsupported', 'Project quotas unsupported on filesystem'];
		const ERR_PROJECT_MISSING = [':err_quota_project_missing', "Project `%(name)s' does not exist"];
		const ERR_PROJECT_EXISTS = [':err_quota_project_exists', 'Project `%(name)s\' already exists'];
		const ERR_PROJECT_EXISTS_ID = [':err_quota_project_exists_id', "Project `%(name)s' already exists with ID `%(id)d'"];
		/**
		 * Block device for path
		 *
		 * @var string|null
		 */
		private string $mount;

		/**
		 * Project ID
		 *
		 * @var null
		 */
		private ?int $projectId;

		private ?string $project;

		/**
		 * Project constructor.
		 *
		 * @param string $project base directory
		 */
		public function __construct(string $project)
		{
			if (!static::supported()) {
				fatal(self::ERR_FEATURE_UNSUPPORTED);
			}

			if ($project[0] !== '/') {
				$this->projectId = $this->projectId($project);
				$this->project = $project;
			} else {
				debug("Expensive path -> id reverse lookup");
				$this->projectId = $this->guessId($project);
				$this->project = $this->idToProject($this->projectId);
			}

			$this->mount = Mount::getMount(FILESYSTEM_VIRTBASE);
		}

		/**
		 * Project quotas supported on server
		 *
		 * @return bool
		 */
		public static function supported(string $path = null): bool
		{
			static $enabled;
			if (!$path && isset($enabled)) {
				return $enabled;
			}

			$fstype = !$path ? FILESYSTEM_TYPE : Mount::getFilesystemType($path);
			$test = ($fstype === 'xfs' &&
				in_array('prjquota', (array)Mount::getMountOptions($path ?? FILESYSTEM_VIRTBASE), true));

			return !$path ? ($enabled = $test) : $test;
		}

		/**
		 * Set project quota
		 *
		 * @param string|<string> $project
		 * @param int $bhard
		 * @param int $ihard
		 * @param int $bsoft
		 * @param int $isoft
		 * @return bool
		 */
		public static function setQuota(string|array $project, int $bhard = 0, int $ihard = 0, int $bsoft = 0, int $isoft = 0): bool
		{
			$ret = true;
			foreach ($project as $p) {
				$ret &= (new static($p))->set($bhard, $ihard, $bsoft, $isoft);
			}

			return $ret;
		}

		/**
		 * Retrieve quota for multiple projects
		 *
		 * @param array|string $project
		 * @return array|null
		 */
		public static function getQuota(array|string $project): ?array
		{
			$ret = [];
			foreach ((array)$project as $p) {
				 $ret[$p] = (new static($p))->get();
			}

			return is_array($project) ? $ret : array_pop($ret);
		}

		/**
		 * Get quota for project
		 *
		 * @return array|null
		 */
		public function get(): ?array
		{
			$resp = $this->exec('quota -pNbi %s', [$this->project]);

			$regex = '!^\s*/dev/' . Mount::getBlockFromPath(FILESYSTEM_VIRTBASE) .
				'\d+\s+(?<qused>\d+)\s+(?<qsoft>\d+)\s+(?<qhard>\d+)\s+(?<qwarn>\d+)\s+\[(?<qtime>[^]]+)\]+' .
				'\s+(?<fused>\d+)\s+(?<fsoft>\d+)\s+(?<fhard>\d+)\s+(?<fwarn>\d+)\s+\[(?<ftime>[^]]+)\]+!mx';
			if (!preg_match_all($regex, $resp['output'], $matches, PREG_SET_ORDER)) {
				return null;
			}

			if (\count($matches) > 1) {
				// quota may exist on multiple mount points?
				report("Multiple matches found for %s", $this->project);
			}

			return array_map(
				fn($v) => (int)$v,
				array_filter($matches[0], fn($k) => !is_int($k), ARRAY_FILTER_USE_KEY),
			);
		}

		/**
		 * Set project quota
		 *
		 * @param int $bhard
		 * @param int $ihard
		 * @param int $bsoft
		 * @param int $isoft
		 * @return bool
		 */
		public function set(int $bhard = 0, int $ihard = 0, int $bsoft = 0, int $isoft = 0): bool
		{
			$ret = $this->exec('limit -p bhard=%d ihard=%d bsoft=%d isoft=%d %s', [
				$bhard * 1024, $ihard, $bsoft * 1024, $isoft, $this->projectId
			]);

			return $ret['success'] ?: error($ret['stderr']);
		}

		/**
		 * Project quotas enabled for site
		 *
		 * @return bool
		 */
		public function enabled(): bool
		{
			return $this->projectId !== null;
		}

		/**
		 * Create new project
		 *
		 * @return self
		 */
		public function create(): self
		{
			$this->projectId = $this->mapProject($this->project);

			return $this;
		}

		/**
		 * Add path to named project
		 *
		 * @param string $path
		 * @return bool
		 */
		public function add(string $path): bool
		{
			$exec = $this->exec('project -s -p %s %d', [
				$path,
				$this->projectId
			]);

			$this->mapPath($path, $this->project);

			return $exec['success'] ?: error($exec['stderr']);
		}

		/**
		 * Run command in xfs_progs
		 *
		 * @param string       $command
		 * @param array        $args    optional command arguments
		 * @param array|bool[] $flags   optional xfs_quota flags
		 * @return array
		 */
		private function exec(string $command, array $args = [], array $flags = ['expert' => true]): array
		{
			$cflags = [];
			foreach ($flags as $k => $v) {
				if (!isset(self::FLAG_MAP[$k])) {
					fatal("Unknown flag `%s'", $k);
				}
				$flag = self::FLAG_MAP[$k];
				if (is_bool($v) && !$v) {
					continue;
				}
				$cflags[] = '-' . $flag;
				if (!is_bool($v)) {
					$cflags[] = $v;
				}
			}

			return \Util_Process::exec([self::XFS_PROG, ...$cflags, '-c', $command, $this->mount], $args);
		}

		/**
		 * Map a project to a symbolic name
		 *
		 * @param string $name
		 * @return int
		 */
		public function mapProject(string $name): int
		{
			if (null !== ($id = $this->projectId($name))) {
				fatal(self::ERR_PROJECT_EXISTS_ID, ['name' => $name, 'id' => $id]);
			}

			if (str_starts_with($name, '/')) {
				fatal("Project name may not be a path");
			}

			$map = new Textfile(self::PROJID_MAP, 'c+', ':');
			$map->lock();

			$list = array_flip($map->fetchAll());

			for ($i = 1; $i <= 4096; $i++) {
				if (isset($list[(string)$i])) {
					continue;
				}
				$id = $i;
				break;
 			}

			$map[$name] = $id;
			$map->save();

			return $id;
		}

		/**
		 * Named project exists
		 *
		 * @param string $project
		 * @return bool
		 * @throws \ReflectionException
		 */
		public static function exists(string $project): bool
		{
			if (!file_exists(self::PROJID_MAP)) {
				return false;
			}

			return null !== (new \ReflectionClass(self::class))->newInstanceWithoutConstructor()->projectId($project);
		}

		/**
		 * Report quota project name
		 *
		 * @param string $path
		 * @return string|null
		 * @throws \ReflectionException
		 */
		public static function projectFromPath(string $path): ?string
		{
			if (false === ($path = realpath($path))) {
				return null;
			}

			$map = new Textfile(self::PROJECT_MAP, 'r', ':', true);
			foreach ($map->fetchAll() as $group => $proot) {
				if (str_starts_with("{$path}/", "{$proot}/", )) {
					return (new \ReflectionClass(self::class))->newInstanceWithoutConstructor()->idToProject($group);
				}
			}

			return null;
		}

		/**
		 * Remove quota support
		 *
		 * @return bool
		 */
		public function remove(string $path = null, bool $force = false): bool
		{
			if (null === $this->projectId) {
				$msg = [':err_quota_project_missing', "Specified path not found in projects"];
				if (!$force) {
					return error($msg);
				}

				if (null === ($this->projectId = $this->guessId($path))) {
					warn($msg);
				}
			}

			// prevent blitzing all related directories
			$exec = $this->exec('project -C -p %s %d', [
				$path, $this->projectId
			], ['projfile' => '/dev/null', 'expert' => true]);

			return $exec['success'] && $this->unmapPath($path) ?: error($exec['stderr']);
		}

		/**
		 * Unmap project
		 *
		 * @param string $name
		 * @return bool
		 */
		public function unmapPath(string $path): bool
		{
			$map = new Textfile(self::PROJECT_MAP, 'c+', ':', true);
			$map->lock();
			if (!$this->projectId || !isset($map[(string)$this->projectId])) {
				warn("Path `%(path)s' not found in `%(file)s',",
					['path' => $path, 'file' => self::PROJECT_MAP]
				);
				return true;
			}
			$val = $map->fetch($this->projectId);

			if ($val instanceof Textfile\Collection) {
				$val->is($path)->forget();
			} else if ($val->value() === $path) {
				$val->forget();
			}

			return $map->save();
		}

		/**
		 * Reassign directory new project
		 *
		 * @param string $owner
		 * @return bool
		 */
		public function reassign(string $newproject, string $path = null): bool
		{
			if (!self::exists($newproject)) {
				return error(self::ERR_PROJECT_MISSING, ['name' => $newproject]);
			}

			$concrete = new static ($newproject);
			if (!$concrete->projectId) {
				$concrete->create();
				debug("Creating new project ID for %(project)s", ['project' => $newproject]);
			}

			return $concrete->remove($path, true) && $concrete->add($path);
		}

		/**
		 * Map a path to project
		 *
		 * @param string $path directory
		 * @param id|string $id optional id to map
		 */
		private function mapPath(string $path, int|string $id = null): void
		{
			$map = new Textfile(self::PROJID_MAP, 'r', ':');
			$id ??= $this->projectId;

			if (is_string($id)) {
				// translate group to id
				$id = (int)$map[$id]->value();
			}

			if (false === array_search((string)$id, $map->fetchAll(), true)) {
				fatal("Project ID `%(projid)d' not found in %(path)s", [
					'projid' => $id, 'path' => self::PROJID_MAP
				]);
			}

			if (!is_dir($path)) {
				fatal("Project path `%(path)s' does not exist", ['path' => $path]);
			}

			$map = new Textfile(self::PROJECT_MAP, 'c+', ':', true);
			$map[$id] = $path;
			$map->save();
		}

		/**
		 * Lookup a project returning its identifier
		 *
		 * @param string $project
		 * @return int|null
		 */
		private function projectId(string $project): ?int
		{
			$map = new Textfile(self::PROJID_MAP, 'c+', ':');
			if (!isset($map[$project])) {
				return null;
			}

			return (int)$map[$project]->value();
		}

		/**
		 * Lookup a project identifier returning its first path
		 *
		 * @param int $id
		 * @return string|null
		 */
		private function idToPath(int $id): ?string
		{
			$map = new Textfile(self::PROJECT_MAP, 'r', ':');
			return $map[$id] ?? null;
		}

		/**
		 * Convert id to project
		 *
		 * @param int $id
		 * @return string|null
		 */
		private function idToProject(int $id): ?string
		{
			$map = new Textfile(self::PROJID_MAP, 'r', ':');
			foreach ($map as $key => $val) {
				if ($val->value() === (string)$id) {
					return $key;
				}
			}

			return null;
		}

		/**
		 * Guess project ID from path
		 *
		 * @param string $path
		 * @return int|null
		 */
		private function guessId(string $path): ?int
		{
			if (null !== ($id = Reseller::idFromPath($path))) {
				$chk = $this->idToPath($id);
				if (0 === strpos($path . '/', $chk . '/')) {
					return $id;
				}
			}

			// expensive full scan
			$entries = (new Textfile(self::PROJECT_MAP, 'r', ':', true))->fetchAll();

			$id = array_search($path, $entries, true);
			return $id !== false ? $id : null;
		}
	}