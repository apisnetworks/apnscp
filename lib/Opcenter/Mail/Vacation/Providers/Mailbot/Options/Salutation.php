<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation\Providers\Mailbot\Options;

	use Opcenter\Mail\Vacation\GenericOption;

	class Salutation extends GenericOption
	{
		const VACATION_FILE = '.vacation.msg';
		const FLAG = '-S %value%';
		const DEFAULT = '%F writes:';

		public function getFlag(): string
		{
			if (!$this->container->getOption('includemessage')->getValue()) {
				return '';
			}

			return str_replace('%value%', escapeshellarg($this->getValue()), parent::getFlag());
		}
	}
