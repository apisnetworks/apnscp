<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation\Providers\Mailbot\Options;

	use Opcenter\Mail\Vacation\GenericOption;

	class ContentType extends GenericOption
	{
		const FLAG = '-t $HOME/' . Message::VACATION_FILE;
		const DEFAULT = 'text';

		/**
		 * Set content type
		 *
		 * @param mixed $value
		 * @return bool
		 */
		public function setValue($value): bool
		{
			if ($value !== 'html' && $value !== 'text') {
				return error("unknown vacation content type `%s'", $value);
			}

			return parent::setValue($value);
		}

		public function getFlag(): string
		{
			$flag = parent::getFlag();
			if ($this->getValue() === 'html') {
				$flag .= ' -A \'Content-Type: text/html; charset=utf-8\'';
			}

			return $flag;
		}
	}
