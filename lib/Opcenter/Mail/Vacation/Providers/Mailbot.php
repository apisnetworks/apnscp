<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation\Providers;

	use Opcenter\Mail\Vacation\Contracts\VacationOption;
	use Opcenter\Mail\Vacation\Contracts\VacationService;
	use Opcenter\Mail\Vacation\Providers\Mailbot\Options\Message;

	/**
	 * Class Mailbot
	 * Vacation driver for Courier Mailbot
	 *
	 * @package Opcenter\Mail\Vacation\Providers
	 */
	class Mailbot implements VacationService
	{
		use \FilesystemPathTrait;
		use \apnscpFunctionInterceptorTrait;

		/**
		 * Vacation Maildrop recipe
		 */
		const VACATION_RE = '^\s*include "\$HOME/\.vacationrc"';
		const VACATION_RECIPE = '# Change to 1 to auto-reply to forwarded emails or BCCs' . "\n" .
		'REPLYALL=0' . "\n" .
		'MBFLAGS="%FLAGS%"' . "\n" .
		'/^(?:X-Original-To|Delivered-To): (.+)/' . "\n" .
		'TOADDR=$MATCH1' . "\n" .
		'if (!/^X-Spam-Flag: YES/ && ($REPLYALL || /^(?:To|Cc):.*${TOADDR}/)) # Make sure we are not BCC\'d' . "\n" .
		'{' . "\n" .
		"\t" . 'cc "| %COMMAND%"' . "\n" .
		'}' . "\n";
		protected const OPTCACHE_KEY = 'vaca.opt.mb';

		/**
		 * @var array known options
		 */
		protected $options = [];
		/**
		 * @var bool autocommit on context loss
		 */
		protected $dirty = false;
		/**
		 * @var string active user
		 */
		protected $user;

		/**
		 * @var \Auth_Info_User authentication context
		 */
		protected $auth;

		/**
		 * Mailbot constructor.
		 *
		 * @param \Auth_Info_User|null $ctx bind to user
		 */


		public function __construct(\Auth_Info_User $ctx = null)
		{
			// needed for \FilesystemPathTrait
			if (!$ctx) {
				$ctx = \Auth::profile();
			}
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($ctx));
			$this->auth = $ctx;
			$this->site = $ctx->site;
			$this->user = $ctx->username;
			if (!$this->user_exists($ctx->username)) {
				fatal("cannot set vacation options for unknown user `%s'", $ctx->username);
			}
			$cache = \Cache_Global::spawn();
			$res = $cache->get(self::OPTCACHE_KEY);
			if (false === $res) {
				$res = $this->enumerateOptions();
				$cache->set(self::OPTCACHE_KEY, $res, 300);
			}

			$this->options = array_combine(
				array_map('strtolower', $res),
				array_map(static function ($item) {
					return __CLASS__ . '\\Options\\' . $item;
				}, $res)
			);
			$this->sync();
		}

		/**
		 * Forcefully enumerate all driver options
		 *
		 * @return array
		 */
		protected function enumerateOptions(): array
		{
			try {
				$rfx = new \ReflectionClass(__CLASS__);
			} catch (\ReflectionException $e) {
				fatal('awesome - the world has turned asunder: %s', $e->getMessage());
			}
			$path = __DIR__ . '/' . $rfx->getShortName() . '/Options/';
			$dh = opendir($path);
			if (false === $dh) {
				fatal('failed to enumerate preferences for vacation driver, %s', __CLASS__);
			}
			$files = [];
			while (false !== $f = readdir($dh)) {
				if ($f === '.' || $f === '..') {
					continue;
				}

				if (substr($f, -4) === '.php') {
					$files[] = substr($f, 0, -4);
				}
			}
			closedir($dh);

			return $files;
		}

		/**
		 * Save and reload vacation preferences
		 */
		public function sync(): bool
		{
			$prefs = array_get(
				\Preferences::factory($this->auth),
				\Email_Module::VACATION_PREFKEY,
				[]
			);
			foreach ($prefs as $opt => $val) {
				if (!$this->hasOption($opt)) {
					continue;
				}
				$this->getOption($opt)->setValue($val);
			}

			return true;
		}

		public function hasOption(string $option)
		{
			return \array_key_exists(strtolower($option), $this->options);
		}

		/**
		 * Get configured option value
		 *
		 * @param string $option
		 * @return null|VacationOption
		 */
		public function getOption(string $option): ?VacationOption
		{
			if (!$this->hasOption($option)) {
				error("Option `%s' not present in Vacation driver `%s'", $option, static::class);

				return null;
			}

			return $this->loadOptionClass($option);
		}

		protected function loadOptionClass(string $option): VacationOption
		{
			$key = strtolower($option);
			/** @var $kname VacationOption */
			$kname = $this->options[$key];
			if (!$kname instanceof VacationOption) {
				$this->options[$key] = $kname::instantiateContexted($this->auth);
				$this->options[$key]->setContainer($this);
			}

			return $this->options[$key];
		}

		public function __destruct()
		{
			if ($this->dirty) {
				$this->save();
			}
		}

		/**
		 * Update vacation recipe
		 *
		 * @return bool
		 */
		public function save(): bool
		{
			$recipe = self::VACATION_RECIPE;
			$command = $this->getCommand();
			$flags = $this->getFlags();
			$recipe = str_replace(['%FLAGS%', '%COMMAND%'], [$flags, $command], $recipe);
			$pwd = $this->user_getpwnam($this->user);
			$vacationrc = $pwd['home'] . '/.vacationrc';

			return $this->file_put_file_contents($vacationrc, $recipe) &&
				$this->file_chown($vacationrc, $pwd['uid']) &&
				$this->file_chmod($vacationrc, 600);
		}

		public function getCommand(): string
		{
			$sendmail = $this->getSendmailCommand();
			if ($this->getOption('contenttype')->getValue() === 'html') {
				// block piping to sendmail if mailbot exits at DSN discretion
				$sendmail = '/usr/bin/reformail -u"content-type:" | (read -t5 -n0 && ' . $sendmail . ' || true)';
				// read always consumes at least 1 line, -t0 would allow read to possibly fire too soon
				// adding a "sleep 1; read -t0" is hacky. Use a sacrificial header that gets consumed by read
				// and only present if mailbot will auto-respond
				$command = 'mailbot -A"Sacrificial-Header: true" $MBFLAGS -A "From: $TOADDR" ' . $sendmail;
			} else {
				$command = 'mailbot $MBFLAGS -A "From: $TOADDR" ' . $sendmail;

			}

			return $command;
		}

		/**
		 * Consolidate Mailbot arguments into flags
		 *
		 * @return string
		 */
		protected function getFlags(): string
		{
			$options = $this->getOptions();
			$flags = [];
			foreach ($options as $opt) {
				if (!$this->hasOption($opt)) {
					continue;
				}
				$flag = $this->getOption($opt)->getFlag();
				if (!$flag) {
					continue;
				}
				$flags[] = $flag;
			}

			return str_replace('"', '\\"', join(' ', $flags));
		}

		/**
		 * Get option attributes
		 *
		 * @return array
		 */
		public function getOptions(): array
		{
			return array_keys($this->options);
		}

		public function getDefaults(): array
		{
			$opts = [];
			foreach ($this->getOptions() as $option) {
				$opts[$option] = $this->getDefault($option);
			}

			return $opts;
		}

		/**
		 * Get default value for a given option
		 *
		 * @param string $option
		 * @return mixed
		 */
		public function getDefault(string $option)
		{
			if (!$this->hasOption($option)) {
				return null;
			}

			return $this->loadOptionClass($option)->getDefault();
		}

		public function setOption(string $option, $value): bool
		{
			if (!$this->hasOption($option)) {
				return error("vacation provider %s does not implement `%s'", __CLASS__, $option);
			}
			$this->dirty = true;

			return $this->loadOptionClass($option)->setValue($value);
		}

		/**
		 * Get option value or default
		 *
		 * @param string $option
		 * @return null|VacationOption
		 */
		public function elideOption(string $option): ?VacationOption
		{
			return $this->getOption($option) ?? $this->getDefault($option);
		}

		/**
		 * Enable vacation service
		 *
		 * @return bool
		 */
		public function enable(): bool
		{
			$pwd = $this->user_getpwnam($this->user);
			if (!$pwd) {
				return error("getpwnam() failed for user `%s'", $this->user);
			}

			if ($this->enabled($this->user)) {
				return info("vacation already enabled for user `%s'", $this->user);
			}

			$home = $pwd['home'];
			$filter = $home . '/.mailfilter';
			$fstfilter = $this->domain_fs_path($filter);
			$recipe = '';
			if (!file_exists($fstfilter)) {
				$this->file_touch($filter) && $this->file_chown($filter, $this->user);
			}
			$recipe = trim(file_get_contents($fstfilter));

			if (!preg_match('!' . self::VACATION_RE . '!m', $recipe)) {
				file_put_contents($fstfilter, $recipe . "\n" .
					'include "$HOME/.vacationrc"' . "\n", LOCK_EX
				);
			}

			$vacationMessage = $this->domain_fs_path($home . '/' . Message::VACATION_FILE);
			if (!file_exists($vacationMessage)) {
				$this->setOption('message', $this->getDefault('message'));
			}

			return $this->save();
		}

		/**
		 * Verify Vacation service enabled for user
		 *
		 * @return bool
		 */
		public function enabled(): bool
		{
			$user = $this->user;
			$filter = $this->user_get_home($user) . '/.mailfilter';

			return $this->file_exists($filter) &&
				preg_match('!' . self::VACATION_RE . '!m', $this->file_get_file_contents($filter));
		}

		public function test(): ?string
		{
			$home = $this->user_get_home();

			$oldvalue = $this->getOption('nodupe')->getValue();
			$this->getOption('nodupe')->setValue(false);
			$flags = str_replace('$HOME', $home, $this->getFlags()) . ' -n';
			$email = $this->auth->username . '@' . $this->auth->domain;
			$command = str_replace(
				['$TOADDR', '$MBFLAGS', '%', $this->getSendmailCommand()],
				[$email, $flags, '%%', '/bin/cat -'],
				$this->getCommand()
			);
			$tmpfile = tempnam($this->domain_fs_path() . '/tmp/', 'apnscp');
			if (!$tmpfile) {
				return error('cannot test vacation auto-responder: failed to create temporary file');
			}
			/**
			 * @todo update this with resource_path() once artisan done
			 */
			copy(webapp_path('vacation/resources/autoreply-test.email'), $tmpfile);
			chmod($tmpfile, 0644);
			$ret = $this->pman_run('cat  %(file)s | ' . $command,
				['file' => $this->file_unmake_path($tmpfile)],
				[
					'TOADDR' => $this->auth->username . '@' . $this->auth->domain
				]
			);
			file_exists($tmpfile) && unlink($tmpfile);
			$this->getOption('nodupe')->setValue($oldvalue);
			if (!$ret['success']) {
				error('Vacation test failed, response: %s', coalesce($ret['stderr'], $ret['stdout']));

				return null;
			}
			$this->dirty = false;
			return $ret['stdout'];
		}

		/**
		 * Get mailer
		 *
		 * @return string
		 */
		protected function getSendmailCommand(): string {
			return "/usr/sbin/sendmail -t -i -f ''";
		}

		/**
		 * Disable Vacation service for a user
		 *
		 * @return bool
		 */
		public function disable(): bool
		{
			$filter = $this->domain_fs_path() . $this->user_get_home($this->user) . '/.mailfilter';
			if (!file_exists($filter)) {
				return warn("vacation not enabled for user `%s'", $this->user);
			}

			file_put_contents($filter,
				preg_replace('!' . self::VACATION_RE . "\r?\n?!m", '',
					file_get_contents($filter)));
			$files = ['.vacation.db', '.vacation.db.gdbm', '.vacation.db.lock'];
			$home = \dirname($filter);
			foreach ($files as $f) {
				$file = "{$home}/{$f}";
				if (file_exists($file)) {
					unlink($file);
				}
			}

			return true;
		}

	}
