<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Mail\Contracts\ServiceDaemon;
	use Opcenter\System\GenericSystemdService;

	class Dovecot extends GenericSystemdService implements ServiceDaemon
	{
		protected const SERVICE = 'dovecot';

		/**
		 * Flush authentication cache
		 *
		 * @return bool
		 */
		public static function flushAuth(): bool {
			$ret = \Util_Process::exec(['/usr/bin/doveadm', 'auth', 'cache', 'flush']);
			return $ret['success'];
		}

		/**
		 * Reload Dovecot service
		 *
		 * @param string|null $timespec
		 * @return bool
		 */
		public static function reload(string $timespec = null): bool
		{
			if (!static::exists()) {
				return true;
			}

			return parent::reload($timespec);
		}

		/**
		 * Restart Dovecot
		 *
		 * @param string|null $timespec
		 * @return bool
		 */
		public static function restart(string $timespec = null): bool
		{
			if (!static::exists()) {
				return true;
			}

			return parent::restart($timespec);
		}

		/**
		 * @inheritDoc
		 */
		public static function present(): bool
		{
			static $status;

			return $status ?? $status = static::exists();

		}
	}