<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Mail\Contracts\ServiceDaemon;
	use Opcenter\System\GenericSystemdService;

	class Postfix extends GenericSystemdService implements ServiceDaemon
	{
		protected const SERVICE = 'postfix';
		public const HOME = '/etc/postfix';
		protected const VIRTUAL_TRANSPORT = 'vmaildrop';

		/**
		 * Get path to mail aliases
		 *
		 * @return string
		 */
		public static function getAliasesPath(): string
		{
			return '/etc/aliases';
		}

		/**
		 * Get directory for Postfix overrides
		 *
		 * @return string
		 */
		public static function getMasterOverridePath(): string
		{
			return self::HOME . '/master.d';
		}

		/**
		 * Get overrides for site
		 *
		 * @param string $site
		 * @return array
		 */
		public static function siteOverrides(string $site): array
		{
			$pattern = Postfix::getMasterOverridePath() . '/' . $site . '{-*,}.*';

			return glob($pattern, GLOB_NOSORT | GLOB_BRACE);
		}

		public static function freeze(string $domain): \Deferred
		{
			if (!preg_match(\Regex::DOMAIN, $domain)) {
				fatal("Invalid domain %s", $domain);
			}


			\Util_Process::exec('postconf -e defer_transports=%s', self::VIRTUAL_TRANSPORT);
			$deferred = new \Deferred;
			$deferred->push(
				static fn() => \Util_Process::exec('postconf -e defer_transports=""')
			);
			return $deferred;
		}
	}