<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Mail\Services\Postfix\Maps;

	use Opcenter\Mail\Services\Postfix;
	use Opcenter\Map\Textfile;

	abstract class Map extends Textfile
	{
		use \NamespaceUtilitiesTrait;
		protected bool $dirty = false;
		protected \WeakReference $map;

		public function __construct()
		{
			if (null === ($dbh = $this->map->get())) {
				$this->map = \WeakReference::create($dbh = \Opcenter\Map::write($this->path(), 'textfile'));
			}

			$this->dbh = $dbh;
		}

		protected function path(): string
		{
			return Postfix::HOME . '/' . strtolower(static::getBaseClassName());
		}

		public function __destruct() {
			if ($this->dirty) {

			}
		}

		public function offsetExists(mixed $offset)
		{
			// TODO: Implement offsetExists() method.
		}

		public function offsetGet(mixed $offset)
		{
			// TODO: Implement offsetGet() method.
		}

		public function offsetSet(mixed $offset, mixed $value)
		{
			// TODO: Implement offsetSet() method.
		}

		public function offsetUnset(mixed $offset)
		{
			// TODO: Implement offsetUnset() method.
		}


	}