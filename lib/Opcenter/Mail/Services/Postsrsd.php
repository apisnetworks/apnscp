<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2024
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Auth\Password;
	use Opcenter\Filesystem;
	use Opcenter\Map;
	use Opcenter\Process;
	use Opcenter\System\GenericSystemdService;

	class Postsrsd extends GenericSystemdService
	{
		protected const SERVICE = 'postsrsd';

		public static function roll(): bool
		{
			$secretFile = self::readIniValue('SRS_SECRET') ?? '/etc/postsrsd.secret';
			if (!file_exists($secretFile)) {
				warn("Secret file `%s` does not exist", $secretFile);
			}

			file_put_contents($secretFile, Password::generate(24));
			Filesystem::chogp($secretFile, self::activeUser(), 'root', 0600);

			return self::restart();
		}

		private static function activeUser(): string
		{
			$map = Map::read('/etc/default/postsrsd', 'inifile')->section(null);
			if ($user = $map->offsetGet('RUN_AS')) {
				return $user;
			}

			$stat = Process::stat(static::status('MainPID'));
			$pwd = posix_getpwuid($stat['user']);

			return $pwd['name'] ?? 'nobody';
		}

		protected static function readIniValue(string $name): mixed
		{
			$map = Map::read('/etc/default/postsrsd', 'inifile')->section(null);
			if (!$map->exists($name)) {
				return null;
			}

			return $map->fetch($name);
		}
	}