<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */


	namespace Opcenter\Map;

	use Opcenter\Map;
	use Opcenter\Map\Textfile\Line;
	use Opcenter\MapInterface;

	/**
	 * Manage a non-equal delimited file
	 *
	 * @package Opcenter\Map
	 */
	class Textfile implements MapInterface, \ArrayAccess, \Iterator
	{
		protected array $tokens = [];
		protected array $lines = [];
		// @var string map filename
		protected $file;
		// @var resource map fp
		protected $fp;
		// quoted style
		protected $quoted;
		// @var bool display comments
		protected $comments = true;
		/**
		 * @var string
		 */
		protected $delimiter;
		private bool $reiterable = false;
		private string $mode;

		public function __construct(string $file, string $mode = 'r', string $delimiter = ' ', bool $reiterable = false)
		{
			if ($file[0] !== '/') {
				$file = Map::home() . '/' . $file;
			}
			$this->delimiter = $delimiter;
			$this->file = $file;
			if (!$this->fp = fopen($file, $mode)) {
				fatal("Failed to open file `%s'", $file);
			}
			$this->mode = $mode;
			$this->reiterable = $reiterable;
			$this->readFile();
		}

		public static function fromString(string $content, string $delimiter = ' ', bool $reiterable = false): self
		{
			$handler = (new \ReflectionClass(static::class))->newInstanceWithoutConstructor();
			$handler->delimiter = $delimiter;
			$handler->reiterable = $reiterable;
			array_map($handler->addLine(...), preg_split("/\R/", $content));
			return $handler;
		}

		public function __destruct()
		{
			$this->close();
		}

		/**
		 * Lock textfile
		 *
		 * @param int $mode
		 */
		public function lock(int $mode = LOCK_EX): void
		{
			if ($mode !== LOCK_EX && $mode !== LOCK_UN && $mode !== LOCK_SH) {
				fatal("Unknown lock mode %d", $mode);
			}
			flock($this->fp, LOCK_EX);
		}

		/**
		 * Process textfile
		 */
		protected function readFile()
		{
			while (false !== ($line = fgets($this->fp))) {
				$this->addLine($line);
			}
		}

		/**
		 * Create line from input
		 *
		 * @param string $line
		 */
		private function addLine(string $line): void
		{
			$line = new Line($line, $this->delimiter);
			if (!$line->isComment() && !$line->key()) {
				// garbage value
				return;
			}

			$head = &$this->tokens[$line->key()];
			if (!($hasKey = isset($head)) || !$this->reiterable) {
				if ($hasKey && !$this->reiterable && !$line->isComment()) {
					debug("Key `%s' already present, overwriting", $line->key());
				}
				$this->tokens[$line->key()] = $line;
				$this->lines[] = &$this->tokens[$line->key()];
				return;
			}

			if (!$head instanceof Map\Textfile\Collection) {
				debug("Key `%s' already present, creating collection", $line->key());
				$head = (new Map\Textfile\Collection)->add($head);
			}
			$head[] = $line;
		}

		public function __debugInfo()
		{
			return $this->lines;
		}

		/**
		 * Merge duplicate lhv into arrays
		 *
		 * @param bool $flag
		 * @return $this
		 */
		public function reiterable(bool $flag = true): self
		{
			$this->reiterable = $flag;
			$this->lines = [];
			$this->readFile();

			return $this;
		}

		public function close()
		{
			if (is_resource($this->fp)) {
				flock($this->fp, LOCK_UN);
				fclose($this->fp);
			}
		}

		public function __toString()
		{
			return implode("\n", array_filter(array_map(function ($v) {
				if (!(string)$v || !$this->comments && $v->isComment()) {
					return null;
				}

				return (string)$v;
			}, $this->lines), static function ($v) {
				return null !== $v;
			}));
		}

		public function hideComments(bool $hide)
		{
			$this->comments = $hide;
		}

		public function section(?string $term): MapInterface
		{
			return $this;
		}

		public function quoted(bool $val = null)
		{
			if (!$val) {
				return $this->quoted;
			}
			$this->quoted = $val;
		}

		/**
		 * @return false|Line|Map\Textfile\Collection
		 */
		public function fetch($key)
		{
			return $this->offsetGet($key);
		}

		#[\ReturnTypeWillChange]
		public function offsetGet($offset)
		{
			return $this->tokens[$offset];
		}

		public function delete($key): bool
		{
			if (!$this->offsetExists($key)) {
				return false;
			}
			$this->offsetUnset($key);

			return true;
		}

		#[\ReturnTypeWillChange]
		public function offsetExists($offset)
		{
			return isset($this->tokens[$offset]);
		}

		#[\ReturnTypeWillChange]
		public function offsetUnset($offset)
		{
			$this->tokens[$offset] = null;
		}

		public function insert(string $key, string $value): bool
		{
			if ($this->exists($key)) {
				return false;
			}
			$this->offsetSet($key, $value);

			return true;
		}

		public function exists(string $key): bool
		{
			return $this->offsetExists($key);
		}

		#[\ReturnTypeWillChange]
		public function offsetSet($offset, $value)
		{
			if (!$this->offsetExists($offset)) {
				$this->addLine($offset . $this->delimiter . $value);
				return;
			} else if ($this->reiterable) {
				if (!($head = $this->tokens[$offset]) instanceof Map\Textfile\Collection) {
					debug("Key `%s' already present, creating collection", $offset);
					$this->tokens[$offset] = (new Map\Textfile\Collection)->add($head);
				}
			}

			$this->tokens[$offset]->set($value);
		}

		public function set(string $key, string $value): bool
		{
			$this->offsetSet($key, $value);

			return true;
		}

		public function fetchAll(): array
		{
			return array_combine(array_keys($this->tokens),
				array_map(static function (Line|Map\Textfile\Collection $v) {
					return $v instanceof Line ? $v->value() : $v;
				}, $this->tokens)
			);
		}

		public function replace(string $key, string $value): bool
		{
			$this->offsetSet($key, $value);

			return true;
		}

		public function save(): bool
		{
			return ftruncate($this->fp, 0) && rewind($this->fp) && fwrite($this->fp, (string)$this) !== false;
		}

		public function truncate()
		{
			$this->tokens = [];

			return ftruncate($this->fp, 0) && fseek($this->fp, 0);
		}

		public function copy($contents)
		{
			// hold off for now, potential to duplicate comment lines
			return error('Copy operation not supported on textfile');
		}

		public function optimize()
		{
			return true;
		}

		#[\ReturnTypeWillChange]
		public function current()
		{
			return current($this->tokens);
		}

		#[\ReturnTypeWillChange]
		public function rewind()
		{
			reset($this->tokens);
		}

		#[\ReturnTypeWillChange]
		public function key()
		{
			return key($this->tokens);
		}

		public function valid(): bool
		{
			return null !== key($this->tokens);
		}

		public function next(): void
		{
			next($this->tokens);
		}
	}