<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2024
 */

namespace Opcenter\Map\Textfile;

class Collection extends \ArrayObject implements \Stringable
{

	/**
	 * Add line to collection
	 *
	 * @param Line $line
	 * @return $this
	 */
	public function add(Line $line): self
	{
		parent::append($line);
		return $this;
	}

	/**
	 * Set line from raw value
	 *
	 * @param string $value
	 * @return $this
	 */
	public function set(string $value): self
	{
		if ($this->is($value)) {
			return $this;
		}
		$line = clone $this->getIterator()->current();
		$line->set($value);

		return $this->add($line);
	}

	/**
	 * @return array<Line>
	 */
	public function all(): array
	{
		return array_filter($this->getArrayCopy(), fn($line) => !$line->isDeleted());
	}

	public function replace(string|Line $element, string|Line $new): self
	{
		if ($element instanceof Line) {
			$element = $element->value();
		}
		foreach ($this as &$line) {
			if ($line->value() === $element && !$line->isDeleted()) {
				$line->set(is_string($new) ? $new : $new->value());
			}
		}

		return $this;
	}

	/**
	 * Collection contains literal rhv
	 *
	 * @param string $literal
	 * @return Line|null
	 */
	public function is(string $literal): ?Line
	{
		foreach ($this as $line) {
			if (!$line->isDeleted() && $line->value() === $literal) {
				return $line;
			}
		}

		return null;
	}

	/**
	 * Collection rhv matches regular expression
	 *
	 * @param string $expression
	 * @return Line|null
	 */
	public function match(string $expression): ?Line
	{
		foreach ($this as $line) {
			if (!$line->isDeleted() && preg_match($expression, $line->value())) {
				return $line;
			}
		}

		return null;
	}

	public function __toString()
	{
		return implode("\n", $this->all());
	}
}