<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */


	namespace Opcenter\Map\Textfile;

	class Line
	{
		const IS_COMMENT = 1;
		const IS_DIRECTIVE = 2;
		const IS_UNKNOWN = 3;
		const IS_DELETED = 4;

		protected $line;
		protected $lhv;
		protected $rhv;
		protected $type = self::IS_UNKNOWN;
		protected $delimiter = ' ';

		public function __construct($line, string $delimiter = ' ')
		{
			$this->line = trim($line);
			$this->delimiter = $delimiter;
			$this->lhv = rtrim((string)strtok($line, $this->delimiter));
			$this->rhv = strtok("\n");


			if ($this->isComment()) {
				$this->rhv = $this->lhv . $this->delimiter . $this->rhv;
				$this->lhv = null;
				$this->type = self::IS_COMMENT;
			} else if ($this->isDirective()) {
				$this->type = self::IS_DIRECTIVE;
			} else {
				$this->type = self::IS_UNKNOWN;
			}
		}

		public function isComment()
		{
			return strspn($this->line, '#;') > 0;
		}

		public function isDirective(): bool
		{
			return false !== strpos($this->line, $this->delimiter);
		}

		public function isDeleted(): bool
		{
			return (bool)($this->type & self::IS_DELETED);
		}

		public function set($value)
		{
			$this->rhv = $value;
		}

		public function key()
		{
			return $this->hasKey() ? $this->lhv : null;
		}

		public function hasKey()
		{
			return (bool)$this->lhv;
		}

		public function forget()
		{
			$this->type = self::IS_DELETED;
		}

		public function value()
		{
			return $this->rhv;
		}

		public function delimiter(): string
		{
			return $this->delimiter;
		}

		public function __toString()
		{
			if ($this->isDeleted()) {
				return '';
			}

			return ltrim($this->lhv . $this->delimiter . $this->rhv);
		}
	}