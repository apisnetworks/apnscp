<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Migration;

use Opcenter\Filesystem;
use ValueError;

/**
 * Class Remediator
 *
 * @package Opcenter\Migration
 */
abstract class Remediator {
	use \NamespaceUtilitiesTrait;
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	protected $strategy;

	// @var array data to consider
	protected $data = [];

	// @var array conflicts
	protected $conflicts = [];

	public static function create(string $name, \Auth_Info_User $context): self
	{
		$c = static::appendNamespace('Remediators\\' . studly_case($name));
		return $c::instantiateContexted($context);
	}

	/**
	 * Get all known remediators
	 *
	 * @return array
	 */
	public static function list(): array
	{
		return Filesystem::readdir(__DIR__ . DIRECTORY_SEPARATOR . '/Remediators', static function ($f) {
			if (substr($f, -4) !== '.php') {
				return null;
			}
			return strtolower(substr($f, 0, -4));
		});
	}

	public function getStrategy(): string
	{
		return strtolower(static::getBaseClassName());
	}

	/**
	 * Attempt to resolve a conflict
	 *
	 * @param string $key
	 * @param        $val
	 * @return bool
	 */
	public function resolve(string &$key, &$val): bool
	{
		return false;
	}

	/**
	 * Item conflicts
	 *
	 * @param string $key
	 * @param        $val
	 * @return bool
	 */
	public function conflict(string $key, $val): bool
	{
		return true;
	}

	/**
	 * Get resolved data
	 *
	 * @return array
	 */
	public function replay(): array
	{
		return $this->data;
	}

	/**
	 * Track data for conflicts
	 *
	 * @param string $key
	 * @param        $val
	 * @return mixed resolved value
	 * @throws ValueError
	 */
	public function store(string $key, $val)
	{
		if ($this->conflict($key, $val) && !$this->resolve($key, $val)) {
			$this->conflicts[$key] = $this->data[$key];
			throw new ValueError(\ArgumentFormatter::format('Conflict detected: %s/%s', [$key, var_export($val, true)]));
		}
		$this->data[$key] = $val;
		return $key;
	}

	/**
	 * Force outcome on conflict
	 *
	 * @param string $key
	 * @param string $xtra
	 * @param        $newval
	 * @return mixed
	 */
	public function forceResolve(string $key, string $xtra, $newval)
	{
		// walk through resolution steps
		$this->resolve($key, $xtra);
		// then override suggested value
		$this->data[$key] = $newval;

		return $newval;
	}

	/**
	 * Get conflict
	 *
	 * @param string $key
	 * @return mixed|null
	 */
	public function getConflict(string $key)
	{
		return $this->conflicts[$key] ?? null;
	}
}