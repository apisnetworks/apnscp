<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration;

use Opcenter\Migration\Contracts\BackupStreamInterface;
use Opcenter\Migration\Formats\StreamReaders\ArchiveStream;
use Opcenter\Migration\Formats\StreamReaders\DirectoryStream;

class FormatType {
	// @var BackupStreamInterface
	protected $stream;
	// @var Bill $bill information about the migration
	protected $bill;

	// @var Migration $migration migration task information
	protected $migration;

	public function __construct(BaseMigration $migration)
	{
		$this->migration = $migration;
	}

	/**
	 * Check consistency
	 *
	 * @return bool
	 */
	public function prerun(): bool
	{
		return true;
	}

	public function run(): bool
	{
		return true;
	}

	public function getBill(): Bill
	{
		return $this->bill;
	}

	/**
	 * Requested migration is valid for format
	 *
	 * @return bool
	 */
	public function valid(): bool
	{
		return false;
	}

	/**
	 * Initiate restore task
	 *
	 * @param Import $task locked backup resource
	 *
	 * @return bool
	 */
	public static function import(Import $task): bool
	{
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
		try {
			$job = new static($task);
			if (!$job->prerun()) {
				return error('Import aborted. Pre-run checks failed');
			}
			if ($ret = $job->run()) {
				$task->setMigrationJob($job->getBill());
			}

			return $job->getBill()->commit($task);
		} catch (\Throwable $e) {
			\Error_Reporter::handle_exception($e);

			return false;
		} finally {
			\Error_Reporter::exception_upgrade($oldex);
		}
	}

	/**
	 * List available migration tasks
	 *
	 * @return array
	 */
	protected function migrationTasks(): array
	{
		return [];
	}

	/**
	 * Order migration tasks for processing
	 *
	 * @return array
	 */
	protected function buildMigrationOrder(): array
	{
		$maps = array_fill_keys(
			static::appendNamespace(static::getBaseClassName() . "\\ConfigurationBuilder")::DEPENDENCY_MAP,
			[]
		);

		$tasks = $this->migrationTasks();
		if (($do = (array)$this->migration->getOption('do'))) {
			$components = $this->components();
			$invalidComponents = array_diff_key(array_flip($do), $components);
			if ($invalidComponents) {
				fatal(
					"Unknown or invalid components listed: %s",
					implode(",", array_keys($invalidComponents))
				);
			}
			$tasks = array_intersect_key($this->components(), array_flip($do));
		}

		foreach ($tasks as $class) {
			$maps[$class] = $class::DEPENDENCY_MAP;
			foreach ($class::DEPENDENCY_MAP as $required) {
				// unconditionally add required deps to map
				if (!isset($maps[$required])) {
					$maps[$required] = static::appendNamespace(static::getBaseClassName() . "\\ConfigurationBuilder")::DEPENDENCY_MAP;
				}
			}
		}

		return (new DependencyMap($maps))->order();
	}

	/**
	 * Hashed name and concrete implementation
	 *
	 * @return array
	 */
	public function components(): array
	{
		$components = [];
		foreach ($this->migrationTasks() as $impl) {
			$task = strtolower(substr($impl, strrpos($impl, '\\') + 1));
			$components[$task] = $impl;
		}

		return $components;
	}

	/**
	 * Get cPanel archive
	 *
	 * @return BackupStreamInterface
	 */
	public function getStream(): BackupStreamInterface
	{
		if (!$this->stream) {
			if (is_dir($this->migration->getFile())) {
				$this->stream = new DirectoryStream($this->migration->getFile());
			} else {
				try {
					if (!$this->migration->getOption('builtin')) {
						throw new \PharException('--no-builtin specified');
					}
					// PharData implements star. This is useless for most situations
					// Attempt to read tar, if POSIX.2001 used, then extract and treat as DirectoryStream
					$this->stream = new ArchiveStream($this->migration->getFile(),
						\Phar::CURRENT_AS_FILEINFO | \Phar::KEY_AS_FILENAME);
					$this->stream->validateArchive();
				} catch (\PharException|\RuntimeException $e) {
					$this->stream = DirectoryStream::fromArchive($this->migration->getFile(),
						$this->migration->getLock());
				}
			}
			// @XXX sloppy, but need way to work around callbacks holding onto ArchiveStream and Locker firing early
			$this->stream->bind($this->migration->getLock());
		}

		return $this->stream;
	}
}