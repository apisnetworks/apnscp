<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration\Formats\Apiscp;

use Opcenter\Migration\Formats\Contracts\ConsistencyValidationInterface;
use Opcenter\Migration\Formats\Cpanel;
use Opcenter\Migration\Import;

class ConsistencyValidation implements ConsistencyValidationInterface {
	/** @var \PharData */
	protected $handler;
	/**
	 * @var Import
	 */
	protected $task;

	/**
	 * @inheritDoc
	 */
	public function __construct(Import $task, Cpanel $type)
	{
		$this->handler = $type->getStream();
		$this->task = $task;
		info('Checking archive consistency');
		if (!$this->handler->isCompressed()) {
			throw new \RuntimeException("Archive {$task->getFile()} isn't an archive");
		}
	}

	public function __destruct()
	{
	}

	/**
	 * Validate cPanel archive
	 *
	 * @return bool
	 */
	public function valid(): bool
	{
		$path = $this->handler->getPathname();
		/**
		 * cPanel stores backups in NAME.tar.gz with an accompanying NAME/ base directory
		 */
		if (!\is_dir($path) || !\is_dir("{$path}/info") || !\is_dir("{$path}/shadow")) {
			return error('Backup possibly not a apnscp backup. Expecting info/ and shadow/ subdirectories');
		}

		info('Verifying files...');
		$count = \count($this->handler)*100;
		$incr = $count/100;
		$i = 0;
		fwrite(STDOUT, 'Progress [  0%]');
		try {
			foreach (new \RecursiveIteratorIterator($this->handler) as $f) {
				if ($i && ($count % $i) === 0) {
					fwrite(STDOUT, "\033[5D");
					fwrite(STDOUT, str_pad((string)($i/$count*100), 3, ' ', STR_PAD_LEFT) . '%]');
				}
				if (!$f->isCRCChecked()) {
					return error("CRC mismatch on `%s'", $f->getPath());
				}
				$i+=$incr;
			}
		} catch (\RuntimeException $e) {
			warn('Archive uses POSIX.2001 format. Extracting first to process');
			fwrite(STDOUT, " WORKAROUND DETECTED\n");
			throw $e;
		}
		fwrite(STDOUT, " COMPLETE\n");

		return true;
	}

	public static function check(Import $task, array $ctor = []): bool
	{
		return (new static($task, ...$ctor))->valid();
	}
}
