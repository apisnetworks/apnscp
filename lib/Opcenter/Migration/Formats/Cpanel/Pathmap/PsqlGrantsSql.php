<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;

	class PsqlGrantsSql extends PsqlUsersSql {
		const PATHMAP_PATH = 'psql_grants.sql';
	}

