<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Note;
	use Opcenter\Migration\Notes\NetInfo;
	use Opcenter\SiteConfiguration;
	use Regex;

	class Dnszones extends ConfigurationBuilder
	{
		const DEPENDENCY_MAP = [
			Userdata::class
		];

		public function parse(): bool
		{
			if (false === ($path = $this->getDirectoryEnumerator()->getPathname())) {
				return true;
			}
			if (!$this->task->getOption('dns')) {
				return info("DNS import disabled - skipping");
			}
			Cardinal::register([\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $p) use ($path) {
					$oldip = $this->bill->getNote(Note::symbolic(NetInfo::class))->getIp();
					$ip6bin = $oldip6 = $this->bill->getNote(Note::symbolic(NetInfo::class))->getIp6();
					if ($oldip6) {
						$ip6bin = inet_pton((string)$oldip6);
					}
					$newip = $p->getSiteFunctionInterceptor()->dns_get_public_ip();
					$newip6 = $p->getSiteFunctionInterceptor()->dns_get_public_ip6();
					$afi = $p->getSiteFunctionInterceptor();
					foreach (array_keys($afi->web_list_domains()) as $domain) {
						$dbPath = \dirname($path) . '/' . $domain . '.db';
						if (!file_exists($dbPath)) {
							warn("No DNS zone for `%s' found, skipping", $domain);
							continue;
						}

						if (!$afi->dns_zone_exists($domain)) {
							warn("Missing zone for `%s'. Ignoring import", $domain);
							return true;
						}
						$zoneData = $this->zoneFixup(file_get_contents($dbPath), $domain);
						if (!$afi->dns_import($domain, $zoneData)) {
							warn('Failed to import zone data into %s', $domain);
						}

						if ($this->task->isDryRun()) {
							info("Activation disabled, not changing `%(old)s' to `%(new)s'",
								['old' => $oldip, 'new' => $newip]
							);
							continue;
						}

						$records = $afi->dns_provisioning_records($domain) +
							append_config($afi->email_provisioning_records($domain));
						foreach ($records as $rec) {
							// merge apnscp records if they do not exist yet
							$afi->dns_add_record_conditionally($rec->getZone(), $rec['name'], $rec['rr'],
								$rec['parameter'], $rec['ttl']);
						}

						// If --do=dnszones is specified, $oldHostname will be null (requires "meta")
						$oldHostname = $this->bill->getNote(Note::symbolic(NetInfo::class))->getHostname() ?? $this->bill->getDomain();
						$records = $afi->dns_get_zone_information($domain);
						foreach ($records as $rr => $recs) {
							if ($rr === 'SOA' || $rr === 'NS') {
								continue;
							}
							foreach ($recs as $r) {
								$newrec = $oldrec = $r['parameter'];
								if (($rr === 'A' && !$this->bill->getNote(Note::symbolic(NetInfo::class))->hasIp4() && $oldrec === $oldip) ||
									($rr === 'AAAA' && !$this->bill->getNote(Note::symbolic(NetInfo::class))->hasIp6() && $oldrec === $oldip6)) {
									warn("Dropping %(rr)s record on %(hostname)s for %(ip)s - system does not support %(stack)s",
										[
											'rr'       => $rr,
											'hostname' => ltrim($r['subdomain'] . '.' . $r['domain'], '.'),
											'ip'       => $oldrec,
											'stack'    => $rr === 'AAAA' ? 'IPv6' : 'IPv4'
										]);
									$afi->dns_remove_record($r['domain'], $r['subdomain'], $rr, $oldrec);
									continue;
								}
								if (false !== strpos($r['parameter'], $oldHostname)) {
									$newrec = str_replace($oldHostname, SERVER_NAME, $r['parameter']);
								} else if ($r['parameter'] === $oldip) {
									$newrec = $newip;
								} else if ($oldip6 && $newip6 && $rr === 'AAAA' && inet_pton($r['parameter']) === $ip6bin) {
									$newrec = $newip6;
								} else {
									continue;
								}

								$hostname = ltrim($r['subdomain'] . '.' . $r['domain'], '.');
								$ret = $afi->dns_modify_record($r['domain'], $r['subdomain'], $rr, $oldrec,
									['parameter' => $newrec]);
								if (!$ret) {
									error("failed to modify record for `%s'", $hostname);
									continue;
								}
								info('Changed %(hostname)s (%(rr)s) from %(old)s to %(new)s',
									['hostname' => $hostname, 'rr' => $rr, 'old' => $oldrec, 'new' => $newrec]);
							}
						}
					}
					return true;
				});

			return true;
		}

		private function zoneFixup(string $data, string $domain): string
		{
			$parsed = preg_split('/\R/', $data, -1, PREG_SPLIT_NO_EMPTY);
			$hash = [];

			return implode("\n", array_filter(array_map(static function ($v) use ($domain, &$hash) {
				$v = trim($v);
				if (!$v) {
					return null;
				}
				if (\in_array($v[0], [';', '$'])) {
					return null;
				} else if (preg_match('/^\s*default\._domainkey(?:\.\S+\s+|\s+)\d+(?:\s+IN)?\s+TXT/', $v)) {
					warn('Dropped DKIM record: %s...', substr($v, 0, 128));

					return null;
				} else if (preg_match('/^\s*_cpanel-dcv-/', $v)) {
					warn('Dropped DCV record: %s...', substr($v, 0, 128));

					return null;
				}
				$line = preg_replace('/^\s*(\S+)(?<!\.)(?=\s)/', "\\1.{$domain}.", $v, 1);
				if (preg_match(Regex::compile(Regex::DNS_AXFR_REC, ['rr' => '\S+']), $line, $match)) {
					$entry = implode('|', [$match[1], $match[4], $match[5]]);
					if (isset($hash[$entry])) {
						info("z'huh? garbage duplicate record discarded: %s", $line);
						return null;
					}
					$hash[$entry] = 1;
				}

				return $line;
			}, $parsed)));
		}

	}