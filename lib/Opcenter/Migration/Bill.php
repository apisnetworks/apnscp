<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Migration;

	use Illuminate\Contracts\Support\Arrayable;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\Service\Validators\Siteinfo\Plan;

	/**
	 * Class Bill
	 *
	 * Attributes pertaining to a migration task
	 *
	 * @package Opcenter\Migration
	 */
	class Bill implements Arrayable
	{
		// @var string $domain domain of the migration
		protected $domain;

		// @var Create $creator account creation
		protected $creator;
		/**
		 * @var array
		 */
		protected $config = [];

		/**
		 * @var array
		 */
		protected $notes = [];

		/**
		 * Bill constructor.
		 *
		 * @param string $domain domain name
		 * @param string $plan   optional base plan to inherit
		 */
		public function __construct(string $domain, string $plan = '')
		{
			if (!preg_match(\Regex::DOMAIN, $domain)) {
				fatal("Unparseable domain specified `%s'", $domain);
			}
			$this->addNote('domain', new Note($domain));
			if ($plan) {
				[$service, $name] = explode('.', ServiceValidator::configize(Plan::class), 2);
				$this->set($service, $name, $plan);
			}
		}

		/**
		 * Merge configuration
		 *
		 * @param BaseMigration $migration
		 * @return bool
		 */
		public function commit(BaseMigration $migration): bool {
			foreach ($migration->getConfigurationOverrides() as $service => $svcoverrides) {
				foreach ($svcoverrides as $name => $val) {
					$this->set($service, $name, $val);
				}
			}
			return $migration->commit();
		}

		/**
		 * Set configuration
		 *
		 * @param string $service
		 * @param string $property
		 * @param        $value
		 */
		public function set(string $service, string $property, $value): void
		{
			array_set($this->config, "{$service}.{$property}", $value);
		}

		/**
		 * Get service value
		 *
		 * @param string $service
		 * @param string $property
		 * @param null   $default
		 * @return mixed
		 */
		public function get(string $service, string $property, $default = null)
		{
			return array_get($this->config, "{$service}.{$property}", $default);
		}

		/**
		 * Add note
		 *
		 * @param string|Note $name
		 * @param Note   $note
		 */
		public function addNote($name, Note $note = null): void
		{
			if ($name instanceof Note) {
				$note = $name;
				$name = Note::symbolic($note);
			}
			if (isset($this->notes[$name])) {
				warn("Note `%s' already exists - overwriting", $name);
			}
			$this->notes[$name] = $note;
		}

		/**
		 * Get note
		 *
		 * @param string $name
		 * @return Note|null
		 */
		public function getNote(string $name): ?Note
		{
			if (0 === strpos($name, Note::class)) {
				$name = Note::symbolic($name);
			}
			return $this->notes[$name] ?? null;
		}

		/**
		 * Get domain for bill
		 *
		 * @return string
		 */
		public function getDomain(): string
		{
			if (null === ($domain = $this->getNote('domain'))) {
				fatal('Domain not set?');
			}
			return (string)$domain;

		}

		public function toArray(): array
		{
			return $this->config;
		}


	}
