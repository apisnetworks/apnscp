<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter;

	use Opcenter\Map\Inifile;
	use Opcenter\Map\Textfile;

	class Map implements \ArrayAccess, \Iterator, MapInterface
	{
		const DOMAIN_MAP = 'domainmap.tch';
		const DOMAIN_TXT_MAP = 'domainmap';
		const META_MAPS = [
			'apache.domainmap'  => 'apache.webserver',
			'billing.domainmap' => 'billing.invoice',
			'billing.parentmap' => 'billing.parent_invoice',
			'domainmap'         => 'siteinfo.domain',
			'ipmap'             => 'ipinfo.ipaddrs',
			'mysql.prefixmap'   => 'mysql.dbaseprefix',
			'mysql.usermap'     => 'mysql.dbaseadmin',
			'pgsql.prefixmap'   => 'pgsql.dbaseprefix',
			'pgsql.usermap'     => 'pgsql.dbaseadmin',
			'ssh.portmap'       => 'ssh.port_index',
			'reseller'          => 'reseller.id',
			'whitelist'         => 'rampart.whitelist'
			// reserved for potential future use
			//'ftp.domainmap'     => 'ftp.ftpserver',
			//'mail.domainmap'    => 'mail.mailserver',
			//'smtp.domainmap'    => 'mail.smtpserver',
		];
		protected $mapPath;
		protected $dbh;
		protected $type;
		protected $keyPosition;
		// filter DEFAULT in inifile k=>v setups
		protected $filter = 'DEFAULT';
		protected $quoted = false;

		protected function __construct(string $map, string $mode = 'r', ?string $type = null, $args = [])
		{
			$this->mapPath = $map;
			if (!$type) {
				$type = self::detectType($map);
			}
			$this->type = $type;
			$this->dbh = dba_open($this->mapPath, $mode, $this->type, ...$args);
			if (!$this->dbh) {
				fatal("failed to open db `%s'", $this->mapPath);
			}
		}

		/**
		 * Get map home
		 *
		 * @param string|null $subpath
		 * @return string
		 */
		public static function home(string $subpath = null)
		{
			return \Opcenter::OPCENTER_PATH . '/mappings' . ($subpath ? '/' . $subpath : '');
		}

		/**
		 * Detect map type
		 *
		 * @param $path
		 * @return string
		 */
		protected static function detectType($path)
		{
			$basename = basename($path);
			if (false === ($pos = strrpos($basename, '.'))) {
				return 'inifile';
			}
			$ext = substr($basename, ++$pos);
			if (false !== ($pos = strrpos($ext, '#'))) {
				// strip out TokyoCabinet-specific directives
				$ext = substr($ext, 0, $pos);
			}
			if ($ext === 'mdb') {
				return 'lmdb';
			} else if ($ext === 'ini' || (substr($ext, -3) === 'map' && \strlen($ext) > 3)) {
				// domainmap, prefixmap, dbmap etc
				return 'inifile';
			} else if ($ext === 'tch') {
				return 'tcadb';
			} else if ($ext === 'tcb' || $ext === 'tcf' || $ext === 'tct') {
				return 'tcadb';
			} else if ($ext === 'db') {
				return 'db4';
			} else if ($ext === 'gdbm') {
				return 'gdbm';
			}
			fatal("unknown database type `%s'", $path);
		}

		/**
		 * Wrapper to open with read-only access
		 *
		 * @param string      $name
		 * @param string|null $type
		 * @return Map
		 */
		public static function read(string $name, string $type = null)
		{
			return self::load($name, 'r-', $type);
		}

		/**
		 * Wrapper to open with write-access
		 *
		 * @param string $name
		 * @param string $type
		 * @return Map
		 */
		public static function write(string $name, string $type = null)
		{
			return self::load($name, 'wd', $type);
		}

		/**
		 * Filter term in key/value pairs by section
		 *
		 * @param string $term
		 * @return self
		 */
		public function section(?string $term): MapInterface
		{
			$this->filter = $term;

			return $this;
		}

		/**
		 * Set quotation type
		 *
		 * @param bool|null $val
		 * @return $this|bool
		 */
		public function quoted(bool $val = null)
		{
			if ($val === null) {
				return $this->quoted;
			}
			$this->quoted = $val;

			return $this;
		}

		public function __destruct()
		{
			$this->close();
		}

		/**
		 * Close opened database
		 */
		public function close(): void
		{
			if (\is_resource($this->dbh)) {
				dba_close($this->dbh);
			}
		}

		/**
		 * Get all values
		 *
		 * @XXX inifile maps: specify '' to section() to fetch all top-level
		 *      values without section grouping
		 *
		 * @return array
		 */
		public function fetchAll(): array
		{
			$arr = [];
			$key = dba_firstkey($this->dbh);
			for (; false !== $key; $key = dba_nextkey($this->dbh)) {
				$value = dba_fetch($key, $this->dbh);
				if ($this->quoted && $value && ($value[0] === '\'' || $value[0] === '"') && $value[-1] === $value[0]) {
					$value = substr($value, 1, -1);
				}
				if ($this->type === 'inifile') {
					$key = dba_key_split($key);
					if (null !== $this->filter) {
						if ($key[1] !== '' && $key[0] === $this->filter) {
							$arr[$key[1]] = $value;
						}
						continue;
					} else if ($key[1] === '') {
						$arr[$key[0]] = [];
						continue;
					} else if ($key[1][0] !== ';') {
						$arr[$key[0]][$key[1]] = $value;
					}
					continue;
				}
				$arr[$key] = $value;
			}

			return $arr;
		}

		/**
		 * Copy database contents into database
		 *
		 * @param string|array $contents file or contents to copy
		 * @return bool
		 */
		public function copy($contents): bool
		{
			if (!\is_array($contents)) {
				$copy = self::load($contents, 'rd');
				$contents = $copy->fetchAll();
				$copy->close();
			}
			$this->truncate();
			foreach ($contents as $k => $v) {
				$this->set($k, $v);
			}

			if (!$this->save() || !$this->optimize()) {
				return false;
			}

			$this->close();
			return true;
		}

		/**
		 * Non-specific map loader
		 *
		 * @param string      $name
		 * @param string      $mode
		 * @param string|null $type
		 * @param mixed       $args map-specific arguments
		 * @return Map
		 */
		public static function load(string $name, string $mode = 'r', string $type = null, $args = []): MapInterface
		{
			if ($name[0] !== '/') {
				$name = self::home($name);
			}

			if (file_exists($name) && is_link($name)) {
				$lstat = lstat($name);
				$stat = stat($name);
				if ($lstat['uid'] !== $stat['uid'] || $lstat['gid'] !== $stat['gid']) {
					fatal("Referent ownership must match link");
				}
			}

			if ($type === 'textfile') {
				return new Textfile($name, $mode);
			}

			if ($type === 'inifile' || (!$type && static::detectType($name) === 'inifile'))
			{
				return new Inifile($name, $mode, 'inifile', $args);
			}

			return new self($name, $mode, $type, $args);
		}

		/**
		 * Truncate map
		 */
		public function truncate()
		{
			$this->close();
			$this->dbh = dba_open($this->mapPath, 'nd', self::detectType($this->mapPath));
		}

		/**
		 * Insert or replace key depending upon presence
		 *
		 * @param string $key
		 * @param string $value
		 * @return bool
		 */
		public function set(string $key, string $value): bool
		{
			return $this->exists($key) ? $this->replace($key, $value) :
				$this->insert($key, $value);
		}

		/**
		 * Check if key exists
		 *
		 * @param string $key
		 * @return bool
		 */
		public function exists(string $key): bool
		{
			$args = $key;
			if ($this->type === 'inifile') {
				$args = [$this->filter, $key];
			}

			return dba_exists($args, $this->dbh);
		}

		/**
		 * Update a key with new value
		 *
		 * @param string $key
		 * @param string $value
		 * @return bool
		 */
		public function replace(string $key, string $value): bool
		{
			$args = $key;
			if ($this->type === 'inifile') {
				$args = [$this->filter, $key];
			}

			if ($this->quoted) {
				$value = '"' . str_replace('"', '\\"', $value) . '"';
			}

			return dba_replace($args, $value, $this->dbh);
		}

		/**
		 * Insert a new key into the database
		 *
		 * @param string $key
		 * @param string $value
		 * @return bool
		 */
		public function insert(string $key, string $value): bool
		{
			$args = $key;
			if ($this->type === 'inifile') {
				$args = [$this->filter, $key];
			}
			if ($this->quoted) {
				$value = '"' . str_replace('"', '\\"', $value) . '"';
			}

			return dba_replace($args, $value, $this->dbh);
		}

		/**
		 * Synchronize contents to disk
		 *
		 * @return bool
		 */
		public function save(): bool
		{
			return dba_sync($this->dbh);
		}

		/**
		 * Optimize database
		 */
		public function optimize()
		{
			return dba_optimize($this->dbh);
		}

		/**
		 * Split an ini_file key
		 *
		 * @link http://php.net/manual/en/function.dba-key-split.php
		 *
		 * @return array|false
		 */
		public function split()
		{
			return dba_key_split($this->key());
		}

		#[\ReturnTypeWillChange]
		public function key()
		{
			if ($this->keyPosition === null) {
				$this->rewind();
			}

			return $this->keyPosition;
		}

		#[\ReturnTypeWillChange]
		public function rewind()
		{
			$this->keyPosition = dba_firstkey($this->dbh);
		}

		#[\ReturnTypeWillChange]
		public function offsetExists($offset)
		{
			return $this->exists($offset);
		}

		#[\ReturnTypeWillChange]
		public function offsetGet($offset)
		{
			return $this->fetch($offset);
		}

		/**
		 * Retrieve a key from database
		 *
		 * @param string $key
		 * @return string|false
		 */
		public function fetch($key)
		{
			if ($this->type === 'inifile' && $this->filter) {
				$val = dba_fetch([$this->filter, $key], $this->dbh);
			} else {
				$val = dba_fetch($key, $this->dbh);
			}
			if (!$this->quoted) {
				return $val;
			}
			if ($this->quoted && ($val[0] === '\'' || $val[0] === '"') && $val[-1] === $val[0]) {
				return substr($val, 1, -1);
			}

			return $val;
		}

		#[\ReturnTypeWillChange]
		public function offsetSet($offset, $value)
		{
			$this->set($offset, $value);
		}

		#[\ReturnTypeWillChange]
		public function offsetUnset($offset)
		{
			$this->delete($offset);
		}

		/**
		 * Remove a key from database
		 *
		 * @param $key
		 * @param
		 * @return bool
		 */
		public function delete($key): bool
		{
			$args = $key;
			if ($this->type === 'inifile') {
				$args = [$this->filter, $key];
			}

			return dba_delete($args, $this->dbh);
		}

		#[\ReturnTypeWillChange]
		public function current()
		{
			if ($this->keyPosition === null) {
				$this->rewind();
			}

			return dba_fetch($this->keyPosition, $this->dbh);
		}

		public function valid(): bool
		{
			return $this->keyPosition !== false;
		}

		public function next(): void
		{
			$this->keyPosition = dba_nextkey($this->dbh);
		}
	}