<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2021
 */

namespace Opcenter\Reseller;

use MikeRoetgers\DependencyGraph\DependencyManager;
use MikeRoetgers\DependencyGraph\Exception\CycleException;
use MikeRoetgers\DependencyGraph\GenericOperation;
use MikeRoetgers\DependencyGraph\Node;
use Opcenter\Map;
use Opcenter\Reseller\Exceptions\HierarchyCycleException;
use Opcenter\Reseller\Exceptions\OrphanedNode;

class Hierarchy implements \ArrayAccess
{
	/**
	 * @var Node[] map child -> parent
	 */
	protected array $relations;
	/**
	 * @var DependencyManager
	 */
	protected DependencyManager $manager;

	private \Throwable $lastException;

	private array $operationCounter;

	private string $map;

	public function __construct(string $map)
	{
		if ($map[0] !== '/') {
			$map = Map::home($map);
		}

		if (!file_exists($this->map = $map)) {
			file_put_contents($this->map, '[DEFAULT]' . "\n");
		}

		$this->relations = $this->getMap('r')->fetchAll();
		$this->reset();
	}

	/**
	 * Write acyclic hierarchy map
	 *
	 * @throws HierarchyCycleException cycle detected
	 *
	 * @return bool
	 */
	public function write(): bool
	{
		if (!$this->check()) {
			throw new HierarchyCycleException($this->lastException->getMessage());
		}

		$transform = array_build($this->relations, static function ($key, $value) {
			if ($key instanceof GenericOperation) {
				$key = $key->getId();
			}
			if ($value instanceof GenericOperation) {
				$value = $value->getId();
			}
			if (!$value) {
				return [null, null];
			}
			return [$key, $value];
		});
		return $this->getMap('cd')->copy($transform);
	}

	/**
	 * Get hierarchy map
	 *
	 * @param string $flags
	 * @return Map
	 */
	protected function getMap(string $flags = 'cd'): Map
	{
		return Map::load($this->map, $flags);
	}

	/**
	 * Reset graph
	 */
	private function reset(): void
	{
		$this->manager = new DependencyManager();
		$this->operationCounter = [];
		array_key_map(function ($site, $parent) {
			$this->assignDependency(
				$this->registerOperation($site),
				$this->registerOperation($parent)
			);

		}, $this->relations);
	}

	/**
	 * Ensure reseller hierachy is acyclic
	 *
	 * @return bool
	 */
	public function check(): bool
	{
		$this->operationCounter = [];

		if (!$this->relations) {
			return true;
		}

		try {
			foreach ($this->manager->getExecutableOperations() as $op) {
				$this->manager->markAsExecuted($op);
				if (!isset($this->operationCounter[(string)$op->getId()])) {
					// head/parent
					continue;
				}
				$this->operationCounter[(string)$op->getId()][0]--;
			}
		} catch (CycleException $e) {
			$this->lastException = $e;
			return false;
		} catch (\Throwable $e) {
			dd($e->getMessage());
		}

		$this->reset();

		return true;
	}

	/**
	 * Get last failure reason
	 *
	 * @return \Throwable|null
	 */
	public function getLastException(): ?\Throwable
	{
		return $this->lastException;
	}

	/**
	 * Designate site => parent site relationship
	 *
	 * @param string|GenericOperation $site
	 * @param string|GenericOperation $parent
	 */
	private function assignDependency($site, $parent): void
	{
		if (!$site instanceof GenericOperation) {
			$site = $this->registerOperation($site);
		}
		if (!$parent instanceof GenericOperation) {
			$parent = $this->registerOperation($parent);
		}

		$this->operationCounter[(string)$parent->getId()][0] += 1;

		$this->relations[$site->getId()] = $parent;
		$this->manager->addDependencyByOperation($parent, $site);
	}

	/**
	 * Convert marker into vertex
	 *
	 * @param string $site
	 * @return GenericOperation
	 */
	private function registerOperation($site): GenericOperation
	{
		if ($site instanceof GenericOperation) {
			$site = $site->getId();
		}
		if (!isset($this->operationCounter[$site])) {
			$this->operationCounter[$site] = [
				null, // has no dependents
				$op = new GenericOperation($site)
			];
			$this->manager->addOperation($op);
		}

		return $this->operationCounter[$site][1];
	}

	/**
	 * Site has dependents
	 *
	 * @param string $site
	 * @return bool
	 */
	public function hasDependents(string $site): bool
	{
		if (!isset($this->operationCounter)) {
			throw new \LogicException("Call check() first");
		}

		return isset($this->operationCounter[$site]) && null !== $this->operationCounter[$site][0];
	}

	/**
	 * Get dependents of site
	 *
	 * @param string $site
	 * @param int    $depth depth to find
	 * @return array
	 */
	public function getDependents(string $site, int $depth = 1): array
	{
		$deps = $this->manager->getOperations()[$site] ?? null;
		if (!$deps || --$depth === 0) {
			return $deps ? $deps->getDependents() : [];
		}

		$deps = $deps->getDependents();
		foreach ($deps as $dep) {
			$deps = array_merge($deps, $this->getDependents($dep, $depth));
		}
		return $deps;
	}

	#[\ReturnTypeWillChange]
	public function offsetExists($site)
	{
		return isset($this->relations[$site]);
	}

	#[\ReturnTypeWillChange]
	public function offsetGet($site)
	{
		return $this->relations[$site];
	}

	#[\ReturnTypeWillChange]
	public function offsetSet($site, $parent)
	{
		$this->assignDependency($site, $parent);
	}

	#[\ReturnTypeWillChange]
	public function offsetUnset($site)
	{
		if ($this->hasDependents($site)) {
			throw new OrphanedNode("{$site} removal orphans " . implode(', ', $this->getDependents($site)));
		}
		unset($this->relations[$site]);
	}
}