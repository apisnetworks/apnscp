<?php declare(strict_types=1);
	/*
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2023
	 */

namespace Opcenter\Reseller\Hierarchies;

use Opcenter\Map;
use Opcenter\MapInterface;
use Opcenter\System\Cgroup\Hierarchy;

class Cgroup extends Hierarchy
{
	public const MAP_OPTIMIZED = 'cgroup.parentmap.tch';
	public const MAP_FILE = 'cgroup.parentmap';

	public function __construct(string $map = self::MAP_FILE)
	{
		parent::__construct($map);
	}

	public static function reader(): MapInterface
	{
		if (!file_exists(Map::home(self::MAP_OPTIMIZED))) {
			(new static)->write();
		}

		return Map::load(self::MAP_OPTIMIZED, 'r');
	}

	public function write(): bool
	{
		if (!parent::write()) {
			return false;
		}

		$map = Map::load(self::MAP_OPTIMIZED, 'cd');
		return $map->copy($this->getMap('r')->fetchAll());
	}


}