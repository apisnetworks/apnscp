<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */


	namespace Opcenter;

	class Php
	{
		const PHP_BIN = '/usr/bin/php';
		const PEAR_HOME = '/usr/share/pear';

		public const VERSIONS = [
			'5.6',
			'7.0',
			'7.1',
			'7.2',
			'7.3',
			'7.4',
			'8.0',
			'8.1',
			'8.2',
			'8.3',
			'8.4'
		];

		/**
		 * Verify PHP extension enabled in system build
		 *
		 * @param string $extension
		 * @return bool
		 */
		public static function extensionEnabled(string $extension): bool
		{
			if (!preg_match('/^[a-z0-9-_]+$/', $extension)) {
				fatal("Invalid extension %s", $extension);
			}
			$code = '$ret = extension_loaded("' . $extension . '"); echo  $ret;';
			$ret = \Util_Process_Safe::exec(self::PHP_BIN . ' -r %(code)s', ['code' => $code]);

			return $ret['output'] !== '';
		}

		/**
		 * Get PHP version
		 *
		 * @return string
		 */
		public static function version(): ?string
		{
			static $version;
			if (null === $version) {
				$cmd = \Util_Process::exec('/usr/bin/php -nv 2> /dev/null');
				preg_match('/^PHP (\d+\.\d+\.\d+)/m', $cmd['output'], $match);
				$version = $match[1];
			}
			return $version;
		}
	}