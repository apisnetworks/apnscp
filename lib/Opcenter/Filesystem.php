<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Contracts\Subscriber;
	use Event\Events;
	use Opcenter\Account\Create;

	class Filesystem implements Subscriber, Publisher
	{
		const HOOK_ID = 'FILESYSTEM';

		use \FilesystemPathTrait;

		protected $site;

		protected function __construct(string $site, ?string $rgroup = null)
		{
			$this->site = $site;
			$this->rgroup = $rgroup;
		}

		/**
		 * Create filesystem skeleton
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function create(string $site, string $rgroup = null): bool
		{
			$class = new static($site, $rgroup);

			return $class->populate();
		}

		/**
		 * Populate filesystem
		 *
		 * @return bool
		 */
		public function populate(): bool
		{
			// hook in to clean up in case things fail
			Cardinal::register([Create::HOOK_ID, Events::FAILURE], [$this, 'depopulate']);
			$paths = $this->getBasePaths();
			foreach ($paths as $p => $perms) {
				if (file_exists($p)) {
					warn("path `%s' already exists", $p);
					continue;
				}
				if (!static::mkdir($p, ...$perms)) {
					return error("populate filesystem failure couldn't mkdir `%s'", $p);
				}
			}

			if ($this->rgroup) {
				debug("Site belongs to %(group)s, linking %(src)s to %(dest)s", [
					'group' => $this->rgroup,
					'src'   => $this->domain_fs_path(),
					'dest'  => FILESYSTEM_VIRTBASE . '/' . $this->site
				]);
				symlink(\dirname($this->domain_fs_path()), FILESYSTEM_VIRTBASE . '/' . $this->site);
			}

			if (false === Cardinal::fire([self::HOOK_ID, Events::CREATED], $this)) {
				fatal('failed to create account');
			}


			return true;

		}

		/**
		 * Get preliminary paths
		 *
		 * @return array
		 */
		protected function getBasePaths(): array
		{
			$base = \dirname($this->domain_fs_path());

			return [
				$base                       => ['root', 'root', 0711],
				$this->domain_fs_path()     => ['root', 'root', 0751],
				$this->domain_info_path()   => ['root', 'root', 0700],
				$this->domain_shadow_path() => ['root', 'root', 0751],
			];
		}

		/**
		 * Create a directory
		 *
		 * @param string     $path
		 * @param string|int $owner
		 * @param string|int $group
		 * @param int|string $perm octal permissions
		 * @param bool       $recursive
		 * @return bool
		 */
		public static function mkdir($path, $owner = 'root', $group = 'root', $perm = 0755, bool $recursive = true)
		{
			if ($recursive && !is_dir(\dirname($path))) {
				static::mkdir(\dirname($path), $owner, $group, $perm, $recursive);
			}
			if (!mkdir($path) && !is_dir($path)) {
				return error("failed to create directory `%s'", $path);
			}

			return self::chogp($path, $owner, $group, $perm);
		}

		/**
		 * Change owner, group, permissions of a file
		 *
		 * @param string     $path
		 * @param string|int $owner user name or user id
		 * @param string|int $group group name or group id
		 * @param int        $perm  octal permission (leading 0)
		 * @return bool
		 */
		public static function chogp(string $path, $owner = 'root', $group = 'root', int $perm = 0755): bool
		{
			$perm = decoct($perm);

			return chown($path, $owner) && chgrp($path, $group) && chmod($path, octdec($perm))
				|| error("failed to chogp `%s'", $path);
		}

		/**
		 * Transform relative path into an absolute path
		 *
		 * @param string $file     absolute location of a path
		 * @param string $referent relative path
		 * @return string
		 */
		public static function rel2abs(string $file, string $referent): string
		{
			$file = strtr($file, ['//' => '/']);
			if (false !== ($file_rel = strstr($file, '..'))) {
				$file = self::rel2abs(substr($file, 0, strpos($file, '..')), $file_rel);
			}

			// fetch the rest of the string
			$file_com = explode('/', dirname($file));
			$token = strtok($referent, '/');

			if ($token !== '..') {
				return "{$file}/{$referent}";
			}
			array_pop($file_com);
			while (false !== ($token = strtok('/'))) {
				if ($token === '..') {
					array_pop($file_com);
				} else if ($token) {
					$file_com[] = $token;
					break;
				}
			}

			$path = join('/', $file_com);

			while (false !== ($token = strtok('/'))) {
				if (!$token)               // path: /
				{
					$path .= '/';
				} else if ($token === '..') { // path: ..
					return self::rel2abs($path, strtok(''));
				} else {
					$path .= '/' . $token;
				}
			}

			return $path . strtok('');
		}

		/**
		 * Transform absolute path into relative path
		 *
		 * @param string $cwd  current working directory
		 * @param string $path target symlink
		 * @return string
		 */
		public static function abs2rel(string $cwd, string $path): string
		{
			if (dirname($cwd) === rtrim($path, '/')) {
				return '../' . basename($path);
			}

			if ($cwd === $path) {
				return '.';
			}

			$cwd = array_values(array_filter(explode('/', $cwd)));
			$path = array_values(array_filter(explode('/', $path)));
			// just in case PHP changes scoping rules in the future...
			$idx = 0;
			for ($idxMax = sizeof($cwd); $idx < $idxMax; $idx++) {
				if (!isset($path[$idx]) || ($path[$idx] !== $cwd[$idx])) {
					break;
				}
			}

			return str_repeat('../', max(0, sizeof($cwd) - ($idx + 1))) . implode('/', array_slice($path, $idx));
		}

		/**
		 * Delete site filesystem
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function delete(string $site, string $rgroup = null): bool
		{
			$class = new static($site, $rgroup);

			return $class->depopulate();
		}

		public function depopulate(): bool
		{
			if (!FILESYSTEM_VIRTBASE ||
				0 !== strpos($this->domain_shadow_path(), FILESYSTEM_VIRTBASE) ||
				0 !== strpos($this->domain_info_path(), FILESYSTEM_VIRTBASE)) {
				fatal('filesystem virtbase corrupted?');
			}

			\Util_Process::exec(['rm', '--one-file-system', '-r', '-f',  '%(shadow)s', '%(info)s'], [
				'shadow' => $this->domain_shadow_path(),
				'info'   => $this->domain_info_path()
			]);

			(new Service\ServiceLayer($this->site))->unmountAll();
			// ensure everything is unmounted
			if (is_dir($this->domain_fs_path()) && !@rmdir($this->domain_fs_path())) {
				// @XXX leaky ext4 file descriptor. Problem appears idiosyncratic to 3.10 kernels
				$orphanPath = static::getOrphanPath();
				\is_dir($orphanPath) || Filesystem::mkdir($orphanPath, 'root', 'root', 0700);
				$relocatedPath = tempnam($orphanPath, $this->site . '-');
				unlink($relocatedPath);
				return rename(\dirname($this->domain_fs_path()), $relocatedPath) ||
					warn('Failed to relocate orphan path %s!', $this->domain_fs_path());
			}
			// this should be empty, so to be safe
			if (is_dir(\dirname($this->domain_fs_path()))) {
				rmdir(\dirname($this->domain_fs_path()));
			} else if ($this->rgroup && is_link(FILESYSTEM_VIRTBASE . "/{$this->site}")) {
				unlink(FILESYSTEM_VIRTBASE . "/{$this->site}");
			}
			return true;
		}

		/**
		 * Requested site ID is a pending orphan
		 *
		 * @param int $siteid
		 * @return bool
		 */
		public static function pendingOrphan(int $siteid): bool
		{
			return \count(glob(static::getOrphanPath() . "/site{$siteid}-*", GLOB_NOSORT)) > 0;
		}

		private static function getOrphanPath(): string
		{
			return FILESYSTEM_VIRTBASE . '/.orphans';
		}

		/**
		 * Touch a file creating or altering its mtime
		 *
		 * @param string     $path
		 * @param string|int $owner user name or user id
		 * @param string|int $group group name or group id
		 * @param int        $perm  octal permission (leading 0)
		 * @return bool
		 */
		public static function touch(string $path, $owner, $group, int $perm = 0755): bool
		{
			return touch($path) && static::chogp($path, $owner, $group, $perm);
		}

		/**
		 * Remove a directory and its descendents
		 *
		 * @param string $path
		 * @return bool
		 */
		public static function rmdir(string $path): bool
		{
			return null !== static::readdir($path, static function ($file) use ($path) {
					$file = $path . DIRECTORY_SEPARATOR . $file;
					if (!is_link($file) && is_dir($file)) {
						return static::rmdir($file);
					}
					unlink($file);
				}) && rmdir($path);
		}

		/**
		 * Perform write followed by rename
		 *
		 * @param string $file
		 * @param string $contents
		 * @return bool
		 */
		public static function atomicWrite(string $file, string $contents): bool
		{
			$tmp = tempnam('/tmp', 'atomic');
			$fp = fopen($tmp, 'w');
			if (!flock($fp, LOCK_EX)) {
				unlink($tmp);
				return false;
			}

			fwrite($fp, $contents);
			fdatasync($fp);
			fclose($fp);
			$stat = is_file($file) ? stat($file) : null;
			Filesystem::chogp($tmp, $stat['uid'] ?? 0, $stat['gid'] ?? 0, ($stat['mode'] ?? 0600) & 0xFFF);
			return rename($tmp, $file) || unlink($tmp) && false;
		}

		/**
		 * Read a directory ignoring self-referential and parent directory hard links
		 *
		 * @param string   $directory
		 * @param \Closure $cb optional callback for each file
		 * @return array|null
		 */
		public static function readdir(string $directory, \Closure $cb = null): ?array
		{
			$dh = opendir($directory);
			if (!$dh) {
				error("failed to open directory `%s'", $directory);

				return null;
			}
			$files = [];
			while (false !== ($file = readdir($dh))) {
				if ($file === '.' || $file === '..') {
					continue;
				}
				$ret = $file;
				if ($cb && null === ($ret = $cb($file))) {
					continue;
				}
				$files[] = $ret;
			}
			closedir($dh);

			return $files;
		}

		public function update($event, Publisher $caller)
		{
			// doesn't matter, callbacks are registered anonymously
			$event = explode('.', $event);
			if ($event[0] === Create::HOOK_ID) {
				switch ($event[1]) {
					case Events::FAILURE:
						return $this->depopulate();
				}
			}
			fatal("unknown event `%s'", join('.', $event));
		}

		public function getEventArgs()
		{
			return [];
		}
	}