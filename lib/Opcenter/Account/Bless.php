<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2024
	 */

	namespace Opcenter\Account;

	class Bless
	{
		use \ContextableTrait;

		public function __construct(private \Auth_Info_Account $requestor, string $primaryDomain) {

		}

		public function addDomain(string $domain, string $path = null): self
		{

		}

		public function create(): bool
		{

		}

	}