<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */


	namespace Opcenter\Account;

	class State
	{
		/**
		 * Enable account
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function enable(string $site): bool
		{
			if (!self::disabled($site)) {
				return warn("site `%s' is not disabled", $site);
			}
			$marker = self::disableMarker($site);
			unlink($marker);

			return true;
		}

		public static function disabled(string $site): bool
		{
			if (!file_exists(\dirname(self::disableMarker($site)))) {
				return warn("site `%s' missing from filesystem metadata, marking as disabled", $site);
			}

			return file_exists(self::disableMarker($site));
		}

		/**
		 * Get site disable marker
		 *
		 * @param string $site
		 * @return string
		 */
		public static function disableMarker(string $site): string
		{
			return FILESYSTEM_VIRTBASE . "/{$site}/info/disabled";
		}

		/**
		 * Disable account
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function disable(string $site, string $reason = ''): bool
		{
			if (self::disabled($site)) {
				return warn("site `%s' is already disabled", $site);
			}
			$marker = self::disableMarker($site);
			file_put_contents($marker, $reason);

			return true;
		}

		/**
		 * Verify site is enabled
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function enabled(string $site): bool
		{
			return !self::disabled($site);
		}
	}