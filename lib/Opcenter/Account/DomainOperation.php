<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace Opcenter\Account;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\SiteConfiguration;

	/**
	 * Class StatusReport
	 *
	 * @package Opcenter\Account
	 */
	abstract class DomainOperation
	{
		use \NamespaceUtilitiesTrait;

		const RC_SUCCESS = 0;
		const RC_FAILURE = 1;
		const RC_PARTIAL_SUCCESS = 2;

		// @var int creation status, used to release site id on failure
		protected $status = self::RC_FAILURE;

		// @var bool fire service journal
		protected $dirty = true;

		public function getStatus(): int
		{
			return $this->status;
		}

		public function fireCleanup(SiteConfiguration $handler): void
		{
			if ($this->dirty) {
				$this->dirty = false;
				Cardinal::fire(['*', Events::END], $handler);
			}
		}

		public static function preExecutionHook(array &$args): void
		{
			// @TODO delegate to general routine that parses/initializes CLI args
			register_shutdown_function(static function () use (&$args) {
				if (!\Error_Reporter::is_error()) {
					return;
				}
				if (!is_debug() && !array_get($args, 'options.output') === 'json') {
					// already listed or to be listed
					foreach (\Error_Reporter::get_errors() as $e) {
						fwrite(STDERR, \Error_Reporter::errno2str(\Error_Reporter::E_ERROR) . ": $e\n");
					}
				}
			});
		}
		public function runUserHooks(SiteConfiguration $s, ...$args): bool
		{
			if ( !($hook = $this->getHook()) ) {
				return true;
			}

			$bin = '';

			if (substr($hook, -4) === '.php') {
				$bin = PHP_BINARY . ' ';
			}
			$bin .= $hook;
			if (!$args) {
				$args = null;
 			} else {
				$args = implode(' ', array_map('escapeshellarg', $args));
			}
			$ret = \Util_Process::exec(
				$bin . ' %s %s',
				$s->getSite(),
				$args
			);

			if (!$ret['success']) {
				warn("Failed to execute hook `%s': %s", basename($hook), $ret['stderr']);
			} else {
				debug("STDOUT: %s\nSTDERR: %s", $ret['stdout'], $ret['stderr']);
			}

			return true;
		}

		/**
		 * Get hook file
		 *
		 * @return string|null
		 */
		protected function getHook(): ?string
		{
			$name = strtolower(static::getBaseClassName());
			if ($name === 'create') {
				// consistency with binary
				$name = 'add';
			}
			$path = config_path('custom/hooks/' . $name . 'Domain');
			foreach (['sh', 'php'] as $ext) {
				if (file_exists("{$path}.{$ext}")) {
					return "{$path}.{$ext}";
				}
			}
			return null;
		}

		protected function setProcessTitle(string $title): void
		{
			if (!function_exists('cli_set_process_title')) {
				// phpdbg
				return;
			}
			$oldTitle = cli_get_process_title() ?: implode(" ", $_SERVER['argv']);
			Cardinal::register(Events::END, static fn() => cli_set_process_title($oldTitle));
			cli_set_process_title(basename($_SERVER['argv'][0]) . " $title");
		}
	}