<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */


	namespace Opcenter\Account;

	use Opcenter\Auth\Password;

	/**
	 * Class Ephemeral
	 *
	 * Temporary accounts that vanish out of scope
	 *
	 * @package Opcenter\Account
	 */
	class Ephemeral
	{
		protected $site;
		protected $domain;
		protected $params;
		protected $context;
		protected $buffer = [];
		protected bool $deferred = false;

		protected function __construct(array $params)
		{
			/**
			 * builtin for faster setup/teardown
			 */
			$configs = [
				'siteinfo.admin_user' => self::random('user'),
				'siteinfo.domain'     => self::random('domain'),
				'dns.provider'        => 'null',
				'mail.provider'       => 'null',
				'ssl.enabled'         => 0,
				'metrics.enabled'     => 0,
				'cgroup.enabled'      => 0,
				'apache.jail'         => (int)HTTPD_USE_FPM
			];
			foreach ($configs as $k => $v) {
				if (array_has($params, $k)) {
					continue;
				}
				array_set($params, $k, $v);
			}

			$this->domain = array_pull($params, 'siteinfo.domain');
			$this->params = $params;
		}

		public function setDeferred(bool $val): self
		{
			$this->deferred = $val;
			return $this;
		}

		/**
		 * Generate random test string
		 *
		 * @param string $what
		 * @return string
		 */
		public static function random(string $what): string
		{
			switch ($what) {
				case 'domain':
					return 'apiscp-int-' . Password::generate(16, 'a-z') . '.test';
				case 'user':
				case 'username':
					return Password::generate(16, 'a-z');
				default:
					fatal("Unknown what `%s'", $what);
			}
		}

		public function edit(array $args = [], array $flags = []): self
		{
			defer($_, static fn() => \Error_Reporter::clear_buffer());
			$editor = new Edit($this->site, \Util_Conf::hydrate($args));
			array_key_map(fn($k, $v) => $editor->setOption($k, $v), $flags);
			$editor->exec();
			return $this;
		}

		/**
		 * Create a new account
		 *
		 * @param array $params dot notation
		 * @return Ephemeral|null
		 */
		public static function create(array $params = []): ?self
		{
			defer($_, static fn() => \Error_Reporter::clear_buffer());
			return (new static(\Util_Conf::hydrate($params)))->exec();
		}

		protected function exec(): ?self
		{
			$oldBuffer = \Error_Reporter::get_buffer();
			$account = new Create($this->domain, $this->params);
			$ex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL, true);
			try {
				if (!$res = $account->exec()) {
					return null;
				}
				$this->context = \Auth::context(null, $account->getDomain());
				$this->site = $this->context->site;
			} catch (\apnscpException $e) {
				throw $e;
			} finally {
				\Error_Reporter::exception_upgrade($ex);
			}

			$this->context->getAccount()->reset($this->context);
			$this->buffer = \Error_Reporter::flush_buffer();
			// empty creation log
			\Error_Reporter::set_buffer($oldBuffer);

			return $this;
		}

		/**
		 * Create new invocation proxy
		 *
		 * @return \Module_Skeleton
		 */
		public function getApnscpFunctionInterceptor(): \apnscpFunctionInterceptor
		{
			return \apnscpFunctionInterceptor::factory($this->getContext());
		}

		public function getContext(): \Auth_Info_User
		{
			if (!\apnscpSession::init()->exists($this->context->id)) {
				// regenerate if session ID gets collected
				$this->context = \Auth::context(null, $this->site);
			}

			return $this->context;
		}

		public function getBuffer(): array
		{
			return $this->buffer;
		}

		public function __destruct()
		{
			if (env("DEBUG_PRESERVE_SITE")) {
				return;
			}

			if ($this->deferred) {
				defer($_, fn() => $this->destroy());
				return;
			}

			$this->destroy();
		}

		public function destroy()
		{
			// bypass deletion at end, for use with debugging unit tests
			if (!$this->site) {
				return true;
			}

			try {
				// force-close any deferred events
				gc_collect_cycles();
				return (new Delete($this->site, ['force' => true]))->exec();
			} catch (\apnscpException) {
				return false;
			} finally {
				$this->site = null;
			}
		}
	}