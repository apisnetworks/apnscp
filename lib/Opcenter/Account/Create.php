<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Account;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Lararia\Mail\Simple;
	use Opcenter\Database\PostgreSQL\Opcenter;
	use Opcenter\Filesystem;
	use Opcenter\Map;
	use Opcenter\SiteConfiguration;

	/**
	 * Class Create
	 *
	 * Create a new domain. Account creation adheres to -c service,conf=val format
	 *
	 * Exit codes:
	 *  - 0: success
	 *  - 1: fatal error
	 *  - 2: creation succeeded but hooks failed. --force not set.
	 *
	 * @package Opcenter\Account
	 */
	class Create extends DomainOperation implements Publisher
	{
		const MAX_SITES = 32767;
		const HOOK_ID = 'vdcreate';

		use \FilesystemPathTrait;

		// @var array configuration passed at runtime
		protected $runtimeConfiguration;
		// @var misc options passed at runtime
		protected $options;
		// @var string domain name
		protected $domain;
		// @var int site id
		protected $site_id;
		// @var string "site" + id
		protected $site;

		/**
		 *
		 * @param string $domain
		 * @param array  $configuration  runtime site configuration
		 * @param array  $runtimeOptions optional runtime options
		 */
		public function __construct(string $domain, array $configuration = [], array $runtimeOptions = [])
		{
			// avoid firing callbacks on previously registered iterations
			Cardinal::purge();
			$this->domain = strtolower($domain);
			array_set($configuration, 'siteinfo.domain', $domain);
			$this->site_id = $this->findFreeSiteId();
			$this->site = 'site' . $this->site_id;
			array_set($configuration, 'siteinfo.admin', 'admin' . $this->site_id);

			$this->setProcessTitle($domain);
			$this->runtimeConfiguration = $configuration;
			$this->options = $runtimeOptions;
		}

		/**
		 * Find free site ID
		 *
		 * @return int|null site id or fatal
		 */
		protected function findFreeSiteId(): ?int
		{
			if (!is_dir(FILESYSTEM_VIRTBASE)) {
				fatal('virtual base not found - is apnscp installed?', FILESYSTEM_VIRTBASE);
			}
			$sites = array_map('basename', glob(FILESYSTEM_VIRTBASE . '/site*', GLOB_ONLYDIR));
			if (!is_int(OPCENTER_SITE_ID_OFFSET) || OPCENTER_SITE_ID_OFFSET < 0 || OPCENTER_SITE_ID_OFFSET >= self::MAX_SITES) {
				fatal("Assertion failure: [opcenter] => site_id_offset outside limit");
			}
			$start = 1 + (int)OPCENTER_SITE_ID_OFFSET;
			for ($i = $start; $i < static::MAX_SITES; $i++) {
				if (!\in_array("site{$i}", $sites, true) && !posix_getgrnam("admin{$i}") && !Filesystem::pendingOrphan($i)) {
					// make sure we clean up the site_id if for whatever reason it craps out
					pcntl_signal(SIGTERM, [$this, '__destruct']);

					return $i;
				}
			}
			fatal('failed to find a free site id!');

			return null;
		}

		public function __destruct()
		{
			$status = $this->getStatus();
			switch ($status) {
				case self::RC_PARTIAL_SUCCESS:
				case self::RC_SUCCESS:
					return;
			}

			if (is_debug() && \Error_Reporter::is_verbose(9999)) {
				pause('Any key to destroy %s', $this->site);
			}

			$this->undo();
		}

		protected function undo(): void
		{
			/**
			 * If siteinfo fails it's possible these
			 * can be left, dangling behind
			 *
			 * fatal needs to be converted to a callback mechanism
			 * to allow use of fatal with automatic cleanup -
			 * even as bad of an idea as it may be
			 */
			$map = Map::write(Map::DOMAIN_MAP);
			if ($map->fetch($this->domain) === $this->site) {
				$map->delete($this->domain);
				(new Opcenter(\PostgreSQL::pdo()))->deleteSite(
					$this->site_id,
					$this->domain
				);
			}
			$map->close();
			Filesystem::delete($this->site, $this->rgroup);
		}

		/**
		 * Run account creation
		 */
		public function exec(): bool
		{
			// switchback and populate account configuration markers before
			// mount is called
			Cardinal::register(
				[Filesystem::HOOK_ID, Events::CREATED],
				[$this, 'installServices']
			);
			if (!\Opcenter\Filesystem::create($this->site, $this->rgroup)) {
				return false;
			}

			$this->status = self::RC_SUCCESS;

			return true;
		}

		public function getEventArgs()
		{
			return [
				'site'    => $this->site,
				'domain'  => $this->domain,
				'options' => $this->runtimeConfiguration
			];
		}

		/**
		 * Populate siteXX/info/ after initial FS population
		 *
		 * @return bool
		 */
		public function installServices(): bool
		{
			$handler = new SiteConfiguration($this->site, $this->runtimeConfiguration, $this->options);
			if (isset($this->options['plan'])) {
				$handler->setPlanName($this->options['plan']);
			}
			if (!$handler->verifyAll()) {
				return false;
			}
			Cardinal::preempt([Create::HOOK_ID, Events::FAILURE], static function () use ($handler) {
				// cascade to rollback services bound in \Opcenter\Service\Validators\Enabled
				Cardinal::fire([SiteConfiguration::HOOK_ID, Events::FAILURE], $handler);
				return false;
			});
			$handler->writePendingConfiguration();
			if ($this->domain !== ($domain = $handler->getServiceValue('siteinfo', 'domain'))) {
				warn("Domain changed from `%(old)s' to `%(new)s'", [
					'old' => $this->domain,
					'new' => $domain
				]);
				$this->domain = $domain;
			}
			// ensure we cleanup journaled data that completed
			defer($_, function () use ($handler) {
				$this->fireCleanup($handler);
			});

			Cardinal::register([SiteConfiguration::HOOK_ID, Events::FAILURE], fn() => (new Delete($this->site, ['force' => true]))->exec());

			try {
				$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
				// run populate()/depopulate() on each service
				if (!Cardinal::fire([SiteConfiguration::HOOK_ID, Events::SUCCESS], $handler)) {
					throw new \RuntimeException('Module provisioning failed - removing site');
				}
			} catch (\Throwable $e) {
				debug("Attempting deletion of %(site)s", ['site' => $this->site]);
				Cardinal::fire([SiteConfiguration::HOOK_ID, Events::FAILURE], $handler);
				return false;
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}

			// @XXX failed hooks throw exceptions on create/edit?
			try {
				$c = \Auth::context(null, $this->getDomain());
				if (!\is_object($c)) {
					fatal('Failed to initialize creation hooks');
				}
				$this->status = self::RC_SUCCESS;
				if (!\Util_Account_Hooks::instantiateContexted($handler->getAuthContext())->run('create')) {
					throw new \LogicException('Hook failed to process');
				}
			} catch (\Throwable $e) {
				// if hooks fail they are considered a "soft fail" and can be rerun or removed via DeleteDomain
				if (!\array_get($this->options, 'force', false)) {
					error('Domain creation hooks failed - removing site without --force option: %(msg)s', ['msg' => $e->getMessage()]);
					Cardinal::fire([SiteConfiguration::HOOK_ID, Events::FAILURE], $handler);
					throw $e;
				} else {
					$this->status = self::RC_PARTIAL_SUCCESS;
					warn('Creation hooks failed but --force set - bypassing automatic removal');
				}
			}
			$this->runUserHooks($handler);
			$handler->getAuthContext()->getAccount()->reset($handler->getAuthContext());
			Cardinal::deregisterAll([SiteConfiguration::HOOK_ID, Events::FAILURE]);

			if (!empty($this->options['notify']) && ($email = $handler->getServiceValue('siteinfo', 'email')))
			{
				$subject = \ArgumentFormatter::format("Welcome to %s", [PANEL_BRAND]);
				\Illuminate\Support\Facades\Mail::to($email)->
					send((new Simple('email.opcenter.account-created', ['svc' => $handler]))->asMarkdown()->setSubject($subject));
			}
			return true;
		}

		public function getDomain(): string
		{
			return $this->domain;
		}
	}