<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service;

	use FFI;
	use Opcenter\Filesystem;
	use Opcenter\Filesystem\Mount;
	use Opcenter\Process;
	use Opcenter\System\Cgroup;
	use Opcenter\System\Kernel;

	class ServiceLayer
	{
		use \FilesystemPathTrait;
		const MIN_VERSION = '3.10.1';
		const HOOK_ID = 'svclyr';
		const MOUNT_CMD = '/etc/systemd/user/fsmount.init';
		const BIND_MOUNTS = [
			'proc',
			'cgroup'
		];
		// control to ensure we always cleanup the mount
		private $remounted = true;

		public function __construct(?string $site)
		{
			$this->site = $site;
		}

		public function __destruct()
		{
			$this->fixSocketPost();
		}

		private function fixSocketPost(): bool
		{
			if ($this->remounted || !static::bugged()) {
				return true;
			}
			$this->remounted = true;
			foreach (self::BIND_MOUNTS as $mount) {
				if (!$this->{'mount' . ucwords($mount)}()) {
					warn("unable to mount /%s from `%s', a reboot is " .
						'necessary to restore procfs functionality data to the server',
						$mount,
						FILESYSTEM_SHARED
					);

					return false;
				}
			}

			return true;
		}

		public static function available(): array
		{
			return Filesystem::readdir(
				FILESYSTEM_TEMPLATE,
				static fn($path) => is_dir(FILESYSTEM_TEMPLATE . "/{$path}") ? $path : null
			);
		}

		public static function bugged(): bool
		{
			return version_compare(Kernel::version(), static::MIN_VERSION, '<');
		}

		/**
		 * Install service layer for site
		 *
		 * @param string $svc
		 * @return bool
		 */
		public function installServiceLayer(string $svc): bool
		{
			$path = $this->domain_info_path("services/{$svc}");
			if (!is_dir(\dirname($path))) {
				Filesystem::mkdir(\dirname($path));
			}

			return touch($path);
		}

		/**
		 * Remove service layer for site
		 *
		 * @param string $svc
		 * @return bool
		 */
		public function uninstallServiceLayer(string $svc): bool
		{
			$path = $this->domain_info_path("services/{$svc}");
			if (!file_exists($path)) {
				return true;
			}

			return unlink($path);
		}

		/**
		 * Service layer enabled on site
		 *
		 * @param string $svc
		 * @return bool
		 */
		public function serviceLayerEnabled(string $svc): bool
		{
			return file_exists($this->domain_info_path("services/{$svc}"));
		}

		/**
		 * Mount all services for a site
		 *
		 * @return bool
		 */
		public function mount(): bool
		{
			$ret = \Util_Process::exec([self::MOUNT_CMD, 'mount', '%s', 'ALL'], $this->site);
			return $ret['success'] ?: error("failed to mount services for `%s': %s", $this->site, $ret['stderr']);
		}

		/**
		 * Remount all services for a site
		 *
		 * @return bool
		 */
		public function reload(): bool
		{
			if ( !($this->unmountAll() && $this->mount()) ) {
				return warn("Failed to remount all services for `%s'", $this->site);
			}

			$gid = \Auth::context(null, $this->site)->group_id;
			return Process::killGroup($gid);
		}

		/**
		 * Flush filesystem cache for site
		 *
		 * @return bool
		 */
		public function flush(): bool
		{
			$mountOptions = Mount::getMountOptions($this->domain_fs_path());
			$ffi = FFI::cdef(
				"int mount(const char *source, const char *target,
				const char *filesystemtype, unsigned long mountflags,
                 const void *data);",
				"libmount.so.1");
			$ret = $ffi->mount(
				"none",
				$this->domain_fs_path(),
				"overlay",
				32 /* MS_REMOUNT */ | (1 << 21) /* MS_RELATIME */,
				implode(',', $mountOptions)
			);

			return $ret === 0 ?: error("failed to flush services for `%s': %d", $this->site, $ret);
		}

		/**
		 * Reload all virtual filesystem caches
		 *
		 * @return bool
		 */
		public function dropVirtualCache(): bool
		{
			$ret = \Util_Process::exec([self::MOUNT_CMD, 'reload']);

			return $ret['success'] ?: error('failed to purge virtual filesystem cache');
		}

		/**
		 * Unmount individual service
		 *
		 * @param string $svc
		 * @return bool
		 */
		public function unmount(string $svc): bool
		{
			$this->fixSocketPre();
			$ret = \Util_Process::exec([self::MOUNT_CMD, 'unmount', '%s', '%s'], $this->site, $svc);
			$this->fixSocketPost();

			return $ret['success'] ?: error("failed to unmount service %s for `%s': %s",
				$svc, $this->site, $ret['stderr']);
		}

		private function fixSocketPre(): bool
		{
			////////////////////////////////////////////////////////////////////////////////////////
			// @XXX RHEL 7.4 bug workaround:
			// 3.10 kernels with a recursive bind mount (/.socket/proc -> siteXX/fst/.socket/proc)
			// doesn't clean up properly on an umount request - unmount /.socket/proc first, delete
			// then link back
			////////////////////////////////////////////////////////////////////////////////////////
			if (!static::bugged()) {
				return true;
			}
			$this->remounted = false;
			foreach (self::BIND_MOUNTS as $mount) {
				if (method_exists($this, 'unmount' . ucwords($mount))) {
					if (!$this->{'unmount' . ucwords($mount)}()) {
						warn("unable to unmount /%s from `%s', a reboot is " .
							"necessary to delete the account data for `%s' fully",
							$mount,
							FILESYSTEM_SHARED,
							$this->site
						);
					}
				} else if (!Mount::unmount(FILESYSTEM_SHARED . "/{$mount}")) {
					warn("unable to unmount /%s from `%s', a reboot is " .
						"necessary to delete the account data for `%s' fully",
						$mount,
						FILESYSTEM_SHARED,
						$this->site
					);

					return false;
				}
			}

			return true;
		}

		/**
		 * Unmount all services
		 *
		 * @return bool
		 */
		public function unmountAll(): bool
		{
			return $this->unmount('ALL');
		}

		/**
		 * Bugged kernel fixup
		 *
		 * @return bool
		 */
		private function mountProc(): bool
		{
			if (Mount::mounted(FILESYSTEM_SHARED . '/proc')) {
				return true;
			}

			return Mount::mount(FILESYSTEM_SHARED . '/proc', 'proc', ['hidepid' => 2], ['--make-slave']);
		}

		/**
		 * Bugged kernel fixup
		 *
		 * @return bool
		 */
		private function mountCgroup(): bool
		{
			return Cgroup::mountAll();
		}

		/**
		 * Bugged kernel fixup
		 *
		 * @return bool
		 */
		private function unmountCgroup(): bool
		{
			return Cgroup::unmountAll();
		}

	}