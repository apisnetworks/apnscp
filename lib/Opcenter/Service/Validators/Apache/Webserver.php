<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Apache;

	use Opcenter\Provisioning\Apache;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Validators\Common\GenericDomainMap;
	use Opcenter\SiteConfiguration;

	class Webserver extends GenericDomainMap implements ServiceReconfiguration, ServiceInstall, AlwaysValidate, ServiceExplicitReconfiguration
	{
		const MAP_FILE = 'apache.domainmap';
		const DESCRIPTION = 'Web server prefix';

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure('', $this->ctx['webserver'], $svc);
		}

		public function reconfigure($oldvalue, $newvalue, SiteConfiguration $svc): bool
		{
			if ($oldvalue) {
				Apache::unmap($oldvalue, $svc->getSite());
			}
			if (!$newvalue) {
				return $this->depopulate($svc);
			}
			Apache::map($svc->getSite(), $newvalue);
			Apache::createConfiguration($svc);

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			if (!Apache::removeConfiguration($svc->getSite())) {
				return false;
			}

			if ($this->ctx['webserver']) {
				Apache::unmap($this->ctx['webserver'], $svc->getSite());
			}

			return true;
		}

		public function rollback($oldvalue, $newvalue, SiteConfiguration $svc): bool
		{
			if ($oldvalue) {
				return $this->reconfigure($newvalue, $oldvalue, $svc);
			}

			return true;
		}


	}
