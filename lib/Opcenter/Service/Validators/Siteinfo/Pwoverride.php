<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Opcenter\Service\ServiceValidator;

	class Pwoverride extends ServiceValidator
	{
		const DESCRIPTION = 'Legacy emulation (not implemented)';

		public function valid(&$value): bool
		{
			if ($value) {
				return error('use auth,pwoverride now');
			}

			return true;
		}

	}

