<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Account\Create;
	use Opcenter\Account\Delete;
	use Opcenter\Account\State;
	use Opcenter\Database\PostgreSQL\Opcenter;
	use Opcenter\Map;
	use Opcenter\Provisioning\ConfigurationWriter;
	use Opcenter\Provisioning\Pam;
	use Opcenter\Provisioning\Siteinfo;
	use Opcenter\Role\User;
	use Opcenter\Service\Contracts\MountableLayer;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceToggle;
	use Opcenter\Service\Validators\Common\MustBeEnabled;
	use Opcenter\SiteConfiguration;


	/**
	 * Class Enabled
	 *
	 * @package Opcenter\Service\Validators\Siteinfo
	 *
	 * Critical account bootstrapping
	 */
	class Enabled extends MustBeEnabled implements MountableLayer, ServiceInstall, ServiceToggle
	{
		use \FilesystemPathTrait;

		public const DESCRIPTION = 'Core account attributes';

		const PAM_SERVICES = ['cp', 'dav'];
		const SITE_ID_MARKER = '/etc/site_id';

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */

		public function populate(SiteConfiguration $svc): bool
		{
			if (!Siteinfo::map($svc->getSite(), $this->ctx['domain'])) {
				return error("failed to map `%s' in %s", $this->ctx['domain'], Map::DOMAIN_TXT_MAP);
			}
			$domain = $this->ctx['domain'];
			$site = $svc->getSite();
			Cardinal::register([Create::HOOK_ID, Events::FAILURE], static function () use ($domain, $site) {
				Siteinfo::unmap($domain, $site);
			});
			if (!(new Opcenter(\PostgreSQL::pdo()))->createSite(
				$svc->getSiteId(),
				$this->ctx['domain'],
				$this->ctx['email'],
				$this->ctx['admin_user']
			)) {
				return error('failed to populate initial admin database');
			}

			$this->site = $svc->getSite();
			Siteinfo::createSysAdmin($this->ctx['admin'], $this->domain_fs_path());

			if (!Siteinfo::populateFilesystem($svc, 'siteinfo')) {
				return false;
			}

			file_put_contents($this->domain_fs_path(self::SITE_ID_MARKER), $svc->getSiteId());

			Siteinfo::createAdmin(
				$this->ctx['admin'],
				$this->ctx['admin_user'],
				$this->domain_fs_path(),
				'/home/' . $this->ctx['admin_user']
			);

			// populate database entries
			$pwd = posix_getpwnam($this->ctx['admin']);
			(new Opcenter(\PostgreSQL::pdo()))->createGroup(
				$svc->getSiteId(),
				$pwd['gid']
			);

			(new Opcenter(\PostgreSQL::pdo()))->createUser(
				$svc->getSiteId(),
				$pwd['uid'],
				$this->ctx['admin_user']
			);

			$svcs = [
				'cp'  => true,
				'dav' => DAV_ENABLED
			];

			foreach ($svcs as $pamsvc => $enabled) {
				if (!$enabled) {
					continue;
				}
				(new Pam($svc))->enable($pamsvc);
			}
			Siteinfo::createCron($svc);

			Cardinal::register([Create::HOOK_ID, Events::END], function () {
				$markerpath = $this->domain_fs_path(self::SITE_ID_MARKER);
				// do this last since immutable can break automatically depopulating a site
				file_exists($markerpath) && \Util_Process::exec(['chattr', '+i', '%s'], $markerpath);
			});

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$proc = new \Util_Process_Schedule;
			do {
				if ($pending = $proc->idPending(null, $svc->getAuthContext())) {
					$proc->cancelJob($pending['job']);
					info('Canceling job %(id)d scheduled for %(date)s',
						['id' => $pending['job'], 'date' => date('r', $pending['ts'])]
					);
				}
			} while ($pending);

			/**
			 * If a site fails before user can be created, then afi creation will fail
			 * leaving a mess behind - check if we can make an afi before removing PAM
			 */
			if ($svc->canInstantiateInterceptor()) {
				(new Pam($svc))->disable('cp')->terminate(null);
				if (DAV_ENABLED) {
					(new Pam($svc))->disable('dav');
				}
			}
			if (file_exists($marker = $this->domain_shadow_path(self::SITE_ID_MARKER))) {
				\Util_Process::exec(['chattr', '-i', '%s'], $marker, [0, 1]);
				unlink($marker);
			}

			Siteinfo::deleteAdmin($this->ctx['admin']);
			if (!(new Opcenter(\PostgreSQL::pdo()))->deleteSite(
				$svc->getSiteId(),
				$this->ctx['domain']
			)) {
				warn("failed to remove site id `%d' from %s", $svc->getSiteId(), Opcenter::DATABASE);
			}

			Siteinfo::removeCron($svc);
			Siteinfo::removeSiteLinks($svc);

			Cardinal::register([Delete::HOOK_ID, Events::END],
				static function ($event, SiteConfiguration $s) {
					$cache = \Cache_Account::spawn($s->getAuthContext());
					$prefix = $cache->_prefix('');
					foreach ($cache->keys('*') as $key) {
						if (0 === strpos($key, $prefix)) {
							$key = substr($key, \strlen($prefix));
						}
						$cache->del($key);
					}

					User::bindTo($s->getAuthContext()->domain_fs_path())->flushCache($s->getAuthContext());
					\Auth_Info_Account::flushCache($s->getAuthContext());
				}
			);

			return Siteinfo::unmap($this->ctx['domain'], $svc->getSite()) || warn("failed to remove `%s' from `%s'",
					$this->ctx['domain'],
					Map::DOMAIN_MAP
				);
		}

		public function suspend(SiteConfiguration $svc): bool
		{
			$files = [
				$this->domain_fs_path('/etc/nologin'),
				$marker = State::disableMarker($svc->getSite())
			];
			$ret = true;
			foreach ($files as $file) {
				$ret &= touch($file);
			}

			if ($svc->hasValidatorOption('reason')) {
				$reason = $svc->getValidatorOption('reason');
				$view = $svc->getValidatorOption('template', 'default');
				$compiled = (string)(new ConfigurationWriter("opcenter.suspension.$view", $svc))->compile(['reason' => $reason]);
				file_put_contents($marker, $compiled) > 0 ?: warn("No suspension reason specified");
			}

			return (bool)$ret;
		}

		public function activate(SiteConfiguration $svc): bool
		{
			$files = [
				$this->domain_fs_path('/etc/nologin'),
				State::disableMarker($svc->getSite())
			];
			$ret = true;
			foreach ($files as $file) {
				if (file_exists($file)) {
					$ret &= unlink($file);
				}
			}

			return (bool)$ret;
		}

		public function getLayerName(): string
		{
			return 'siteinfo';
		}
	}