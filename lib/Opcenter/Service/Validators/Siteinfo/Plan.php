<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Illuminate\Support\Str;
	use Opcenter\Lock;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Plans;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Plan extends ServiceValidator implements DefaultNullable
	{
		const DESCRIPTION = 'Plan assignment marker';

		public function valid(&$value): bool
		{
			if ($value === null) {
				return true;
			}

			if ($value === self::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}
			if (!\in_array($value, Plans::list(), true)) {
				warn("Plan `%s' not found in list. Defaulting to `%s'", $value, Plans::default());
				$value = Plans::default();
			}
			if ($this->ctx->isEdit()) {
				return $this->assignChanges($this->ctx->getOldServiceValue(null, 'plan', Plans::default()), $value);
			}

			return true;
		}

		/**
		 * Reprocess site edit with new defaults
		 *
		 * @param string $old old plan
		 * @param string $new new plan
		 * @return bool
		 */
		private function assignChanges(string $old, string $new): bool
		{
			if ($old === $new) {
				return true;
			}

			/**
			 * ConfigurationContexts do not have access to alter every variable, just
			 * values within the same service. With plan changes (--plan=foo), extract
			 * the plan, re-run the command as siteinfo,plan=foo
			 */

			$args = array_get($_SERVER, 'argv', []);
			$offset = null;
			foreach ($args as $k => $v) {
				if ($v === '-p' || 0 === strncmp($v, '--plan=', 7)) {
					unset($args[$k]);
					if ($v[1] === 'p') {
						unset($args[++$k]);
					}
					$offset = $k;
				}
			}

			if (null === $offset) {
				// already spliced, this is the transformed command
				return true;
			}

			$changes = Plans::diff($new, $old);

			array_set($changes, 'siteinfo.plan', $new);
			$spliceargs = [];
			$prevdefaults = (new SiteConfiguration($this->site))->getDefaultConfiguration();
			foreach (array_dot($changes) as $k => $v) {
				if ($k !== 'siteinfo.plan') {
					[$service, $name] = explode('.', $k, 2);
					$myval = $this->ctx->getServiceValue($service, $name);
					$default = $prevdefaults[$service][$name] ?? null;
					if ($myval != $default) {
						info("Detected non-default value on `%s',`%s'. Found %s, expected %s. Skipping",
							$service, $name, $myval, $default);
						continue;
					}
				}
				$spliceargs[] = '-c';
				$spliceargs[] = Str::replaceFirst('.', ',',$k) .
					'=' . (string)\Util_Conf::build_ini($v);
			}

			$command = array_shift($args);
			$args = $spliceargs + append_config($args);
			Lock::unlock();
			pcntl_exec($command, $args, $_ENV);
			fatal('pcntl_exec failed!');
			return false;
		}

		public function getDefault()
		{
			return Plans::default();
		}


	}

