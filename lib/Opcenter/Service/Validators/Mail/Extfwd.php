<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Mail;

	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;

	class Extfwd extends \Opcenter\Service\Validators\Common\Enabled implements ServiceReconfiguration
	{
		use \FilesystemPathTrait;

		public function valid(&$value): bool
		{
			if (null === $value) {
				return true;
			}

			return parent::valid($value);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($new || $new === $old) {
				return true;
			}

			// @TODO check existing addresses for potential violations on disablement

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function getDescription(): string
		{
			return 'Permit external forwarding of addresses';
		}

		public function getValidatorRange()
		{
			return '[0,1,null]';
		}

	}