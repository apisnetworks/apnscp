<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */


	namespace Opcenter\Service\Validators\Auth;

	use Auth\IpRestrictor;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;

	class Iprestrict extends \Opcenter\Service\Validators\Common\Enabled implements ServiceReconfiguration, DefaultNullable
	{
		public function getDescription(): string
		{
			return 'Set IP-based login restrictions';
		}

		public function valid(&$value): bool
		{
			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = (int)$this->getDefault();
			} else if ($value > IpRestrictor::MODULE_HARD_LIMIT) {
				warn('Restriction limit may not exceed module hard limit of %d - capping to %d',
					IpRestrictor::MODULE_HARD_LIMIT,
					IpRestrictor::MODULE_HARD_LIMIT
				);
				$value = IpRestrictor::MODULE_HARD_LIMIT;
			} else if ($value === -1) {
				// rely on MODULE_HARD_LIMIT
				return true;
			} else if ($this->ctx->getOldServiceValue(null, $this->getServiceName()) > $value) {
				warn('Reducing maximum number IP entries may prevent further additions');
			}

			return $value >= 0 && \is_int($value);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$new) {
				return $this->depopulate($svc);
			}
			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$prefs = \Preferences::factory($svc->getAuthContext());
			$prefs->unlock($svc->getSiteFunctionInterceptor());
			array_forget($prefs, IpRestrictor::IP_RESTRICTION_PREF);
			$prefs->sync();
			return true;
		}

		public function getDefault()
		{
			return (int)AUTH_IP_RESTRICTION_LIMIT;
		}

		public function getValidatorRange()
		{
			return '-1, 0, n >= 1';
		}
	}
