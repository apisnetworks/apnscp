<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Mysql;

	use Opcenter\Database\DatabaseCommon;
	use Opcenter\Database\Mapper;
	use Opcenter\Database\MySQL;
	use Opcenter\Service\Contracts\AlwaysRun;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;
	use function strlen;

	class Dbaseadmin extends ServiceValidator implements ServiceInstall, ServiceReconfiguration, AlwaysValidate, AlwaysRun
	{

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue($this->ctx->getModuleName(), 'enabled') &&
				!$this->ctx->getServiceValue($this->ctx->getModuleName(), 'dbaseprefix'))
			{
				$value = null;

				return true;
			}

			if ($value && $this->ctx->isEdit() && (!$this->ctx->serviceValueChanged('siteinfo',
						'admin_user') && !$this->ctx->serviceValueChanged($this->ctx->getModuleName(), 'dbaseadmin')))
			{
				return true;
			}

			$admin = $this->ctx->getServiceValue('siteinfo', 'admin_user');
			if (!$value) {
				$value = $admin;
			}

			if ($this->ctx->isEdit() && !$this->ctx->getNewServiceValue($this->ctx->getModuleName(),
					'dbaseadmin') && $this->ctx->serviceValueChanged('siteinfo', 'admin_user')) {
				info("Detected admin_user change - updating %s dbaseadmin from `%s' to `%s'",
					DatabaseCommon::canonicalizeBrand($this->ctx->getModuleName()),
					$value,
					$admin
				);
				$value = $admin;
			}
			if ($value !== $admin) {
				return error('database admin username must be same as account admin');
			}

			if (strlen($value) > ($len = MySQL::fieldLength('user'))) {
				return error("database administrator max length is %d characters, %d provided in `%s'",
					$len,
					strlen($value),
					$value
				);
			}
			$normalized = DatabaseCommon::canonicalizeBrand($this->ctx->getModuleName());
			$class = '\\Opcenter\\Database\\' . $normalized;
			if ((!$this->ctx->isEdit() || $this->ctx->serviceValueChanged(null, 'dbaseadmin')) &&
				($this->mapKeyInUse($value, $this->ctx->getModuleName() . '.usermap', $this->site) || $class::userExists($value)))
			{
				return error("%s database admin `%s' already in use", $normalized, $value);
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure(
				$this->ctx->getOldServiceValue(null, $this->getServiceName()),
				$this->ctx->getServiceValue(null, $this->getServiceName()),
				$svc
			);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$this->ctx['enabled'] && !$this->ctx['dbaseadmin'] && $this->ctx->isDelete()) {
				return true;
			}
			$mapper = new Mapper($this->ctx->getModuleName(), 'user');
			if ($old) {
				$mapper->unmap($old, $svc->getSite());
			}
			if ($new) {
				$mapper->map($new, $svc->getSite());
			}
			$mapper = null;
			if ($old === $new) {
				return true;
			}

			if (!$this->ctx->isEdit() || !$old) {
				return true;
			}
			$type = strtolower($this->ctx->getModuleName());
			$class = '\Opcenter\Database\\' . DatabaseCommon::canonicalizeBrand($type);

			if (!$class::userExists($old)) {
				return warn("User `%(user)s' does not exist in %(brand)s", ['user' => $old,
					'brand' => DatabaseCommon::canonicalizeBrand($type)]);
			}
			if ($old && $new && $old !== $new && !$class::renameUser($old, $new)) {
				return false;
			}

			return $svc->getSiteFunctionInterceptor()->mysql_set_option('user', $new);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx['dbaseadmin'], null, $svc);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function getDescription(): ?string
		{
			return 'Set ' . $this->ctx->getModuleName() . ' admin user';
		}
	}