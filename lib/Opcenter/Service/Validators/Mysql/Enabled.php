<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Mysql;

	use Opcenter\Database\DatabaseCommon;
	use Opcenter\Provisioning\MySQL;
	use Opcenter\Role\User;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall
	{
		const DESCRIPTION = 'MySQL database access. Required for Web App usage.';

		public function valid(&$value): bool
		{
			if (!parent::valid($value)) {
				return false;
			}
			if ($this->ctx->isEdit() && $value !== $this->ctx->getOldServiceValue(null, 'enabled')) {
				// update prefixes as necessary
				$this->forceValidation();
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			MySQL::populateFilesystem($svc);
			MySQL::createGroups($svc->getAccountRoot());
			if (!$pwd = posix_getpwnam('mysql')) {
				fatal('mysql system user not found!?');
			}


			User::bindTo($svc->getAccountRoot())->mirror('mysql');

			if ($this->ctx->isEdit() && null !== $this->ctx->getOldServiceValue(null, 'dbaseprefix')) {
				return true;
			}

			return MySQL::install($svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			// still handled by mysql module
			$userHandler = User::bindTo($svc->getAccountRoot());
			if ($userHandler->exists('mysql')) {
				$userHandler->delete('mysql');
			}
			MySQL::removeGroups($svc->getAccountRoot());

			if ($this->ctx->isEdit() && $this->ctx['dbaseprefix'] !== null) {
				return warn("Databases and grants will not be removed without setting %s,%s=None", $this->getServiceName(), 'dbaseprefix');
			}

			$type = strtolower($this->ctx->getModuleName());
			$class = '\Opcenter\Database\\' . DatabaseCommon::canonicalizeBrand($type);
			if ($databases = $svc->getSiteFunctionInterceptor()->{"{$type}_list_databases"}()) {
				foreach ($databases as $db) {
					if (!$class::databaseExists($db)) {
						// bogus/ghosted grant
						if ($type === 'mysql') {
							warn("Dropping bogus/ghosted grant for `%s'", $db);
							$class::dropDatabaseGrants($db);
							continue;
						}
					}

					$svc->getSiteFunctionInterceptor()->{"{$type}_delete_database"}($db);
				}
			}

			if ($users = $svc->getSiteFunctionInterceptor()->{"{$type}_list_users"}()) {
				// DeleteDomain --force with corrupted metadata
				foreach (array_keys($users) as $user) {
					if ($user === $this->ctx['dbaseadmin']) {
						continue;
					}
					$hosts = array_keys($users[$user]);
					if (isset($users[$user]['max_connections'])) {
						// not keyed by host, e.g. PostgreSQL
						$hosts = ['%'];
					}
					foreach ($hosts as $host) {
						if (!$class::deleteUser($user, $host)) {
							warn("Failed to delete user `%s@%s'", $user, $host);
						}
					}
				}
			}

			return MySQL::uninstall($svc);
		}

		protected function getUser(SiteConfiguration $svc): ?string
		{
			if (null !== ($user = $this->ctx->getOldServiceValue($this->ctx->getModuleName(), 'dbaseadmin'))) {
				//@TODO extract to dbaseadmin
				return $user;
			}
			if (null !== ($user = $this->ctx->getServiceValue($this->ctx->getModuleName(), 'dbaseadmin'))) {
				return $user;
			}

			return $svc->getServiceValue('mysql', 'dbaseadmin');
		}
	}