<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Pgsql;

	use Opcenter\Auth\Password;
	use Opcenter\Database\PostgreSQL;
	use Opcenter\Role\User;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Mysql\Enabled implements ServiceInstall
	{
		const DESCRIPTION = 'Enable PostgreSQL database access.';

		public function valid(&$value): bool
		{
			if (!parent::valid($value)) {
				return false;
			}
			if ($this->ctx->isEdit() && $value !== $this->ctx->getOldServiceValue(null, 'enabled')) {
				// update prefixes as necessary
				$this->forceValidation();
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			if ($this->ctx->isEdit() && null !== $this->ctx->getOldServiceValue(null, 'dbaseprefix')) {
				return true;
			}

			\Opcenter\Provisioning\PostgreSQL::populateFilesystem($svc);
			$admin = $this->getUser($svc);
			$password = $this->getPassword($svc);
			if (!PostgreSQL::createUser($admin, $password)) {
				return error("failed to create pgsql user `%s'", $admin);
			}
			$svc->getSiteFunctionInterceptor()->pgsql_set_username($admin);
			$svc->getSiteFunctionInterceptor()->pgsql_set_password($password);

			$path = $svc->getAccountRoot() . DIRECTORY_SEPARATOR .
				\a23r::get_class_from_module('pgsql')::PGSQL_DATADIR;
			if (!PostgreSQL::tablespaceExists($svc->getSite()) &&
				!PostgreSQL::initializeTablespace($svc->getSite(), $path, $admin))
			{
				return error("failed to initialize tablespace `%s'", $svc->getSite());
			}
			$pwd = posix_getpwnam('postgres');
			if (false === $pwd) {
				fatal('unable to retrieve postgres system user?');
			}
			\Opcenter\Provisioning\PostgreSQL::createGroups($svc->getAccountRoot());
			User::bindTo($svc->getAccountRoot())->mirror('postgres');

			return true;

		}

		protected function getPassword(SiteConfiguration $configuration): string
		{
			$afi = \apnscpFunctionInterceptor::factory($configuration->getAuthContext());
			if ($p = $afi->pgsql_get_password()) {
				return (string)$p;
			}
			if ($p = $configuration->getServiceValue('pgsql', 'password')) {
				return $p;
			}
			if ($p = $configuration->getServiceValue('siteinfo', 'tpasswd')) {
				return $p;
			}

			return Password::generate();
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			if (null === ($user = $this->getUser($svc))) {
				return warn("unable to lookup postgresql user for `%s'", $svc->getSite());
			}

			if ($this->ctx->isEdit() && $this->ctx['dbaseprefix'] !== null) {
				return warn("Databases and grants will not be removed without setting %s,%s=None",
					$this->getServiceName(), 'dbaseprefix');
			}

			$tblspace = PostgreSQL::getTablespaceFromUser($user);
			foreach ($svc->getSiteFunctionInterceptor()->pgsql_list_databases() as $db) {
				$svc->getSiteFunctionInterceptor()->pgsql_delete_database($db);
			}
			if ($tblspace && PostgreSQL::tablespaceExists($tblspace) && !PostgreSQL::dropTablespace($tblspace)) {
				return error("failed to delete tablespace `%s'", $tblspace);
			}

			if (PostgreSQL::userExists($user) && !PostgreSQL::deleteMainUser($user)) {
				return error("Failed ot delete database user `%s'", $user);
			}
			$userHandler = User::bindTo($svc->getAccountRoot());
			if ($userHandler->exists('postgres')) {
				$userHandler->delete('postgres');
			}
			\Opcenter\Provisioning\PostgreSQL::removeGroups($svc->getAccountRoot());

			return true;
		}

	}
