<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Bandwidth;

	use Opcenter\Service\ServiceValidator;

	class Rollover extends ServiceValidator
	{
		const DESCRIPTION = 'Day of month when bandwidth resets. Leave blank for today';
		const VALUE_RANGE = '[null,0-31]';

		public function valid(&$value): bool
		{
			if (null === $value) {
				$value = (int)date('j');
				info("no bandwidth rollover date specified, using day of month, `%s'", $value);
			}
			if (!\is_int($value) || $value < 0 || $value > 31) {
				return error("rollover date must be between [0, 31], `%s' given", $value);
			}

			return $value >= 0;
		}

	}