<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ipinfo;

	class Namebased extends \Opcenter\Service\Validators\Common\Enabled
	{
		public function getDescription(): string
		{
			return 'Site uses shared IP address (unique otherwise, see ipaddrs)';
		}
	}