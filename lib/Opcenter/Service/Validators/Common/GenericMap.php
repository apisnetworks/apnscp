<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Common;

	use Opcenter\Map;
	use Opcenter\Service\ServiceValidator;

	abstract class GenericMap extends ServiceValidator
	{
		use \NamespaceUtilitiesTrait;
		const MAP_FILE = '';

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue($this->ctx->getModuleName(), 'enabled')) {
				$value = null;

				return true;
			}

			$svcname = strtolower(static::getBaseClassName());
			if (static::MAP_FILE === null) {
				fatal("configure MAP_FILE in `%s'", $svcname);
			}

			if (!$this->ctx->hasOld() || ($this->ctx->hasOld() && $value !== $this->ctx->getOldServiceValue(null,
						$this->ctx->key()))) {
				if ($this->mapKeyInUse((string)$value, static::MAP_FILE)) {
					return error("%s name `%s' already in use", $svcname, $value);
				}
			}

			return true;
		}

		/**
		 * Remove key from map
		 *
		 * @param string $key
		 * @return bool
		 */
		protected function removeMap(string $key): bool
		{
			$map = Map::load(static::MAP_FILE, 'cd');

			return $map->delete($key);
		}

		/**
		 * Get value from map
		 *
		 * @param string $key
		 * @return mixed
		 */
		protected function getMapValue(string $key)
		{
			return Map::load(static::MAP_FILE, 'r')->fetch($key);
		}

		/**
		 * Map new key to value
		 *
		 * @param string $key
		 * @param string $val
		 * @return bool
		 */
		protected function addMap(string $key, string $val): bool
		{
			$map = Map::load(static::MAP_FILE, 'cd');

			return $map->insert($key, $val);
		}

		/**
		 * Key exists in map
		 *
		 * @param string $key
		 * @return bool
		 */
		protected function keyExists(string $key): bool
		{
			$map = Map::load(static::MAP_FILE, 'cd');

			return $map->exists($key);

		}
	}
