<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Common;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Opcenter\Service\Contracts\MountableLayer;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\ServiceLayer;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Enabled extends ServiceValidator
	{
		const DESCRIPTION = null;
		/**
		 * Add AlwaysValidate since "enabled" acts as the service trigger
		 */
		use \NamespaceUtilitiesTrait;

		public function valid(&$value): bool
		{
			if ($value === null) {
				$value = 0;
			} else if ($value === true) {
				$value = 1;
			} else if ($value === false) {
				$value = 0;
			} else if ($value !== 1 && $value !== 0) {
				return false;
			}
			if (!$this->ctx->isRevalidation() && $this->ctx->isEdit() &&
				$value !== $this->ctx->getOldServiceValue(null, 'enabled'))
			{
				$this->ctx->revalidateService();
			}
			if ($this instanceof MountableLayer) {
				// mount service layer early in case other validators
				// depend upon mount-specific meta from FILESYSTEMTEMPLATE/<service>
				if ($this->hasChange()) {
					// only needs to be called once
					$site = $this->site;
					Cardinal::register([ServiceLayer::HOOK_ID, Events::SUCCESS],
						static function () use ($site) {
							(new ServiceLayer($site))->unmountAll();
							(new ServiceLayer($site))->mount();
						}, Cardinal::OPT_SINGLE);
					$this->manageLayer($value, $this->getLayerName() ?? $this->ctx->getModuleName());
				}

			}

			return true;
		}

		/**
		 * Manage mounting of layer
		 *
		 * @param int    $value
		 * @param string $layer
		 * @return void
		 */
		protected function manageLayer(int $value, string $layer): void
		{
			$layercls = new ServiceLayer($this->site);
			$value ? $layercls->installServiceLayer($layer) :
				$layercls->uninstallServiceLayer($layer);
		}
		/**
		 * Event listener
		 *
		 * @param string    $event
		 * @param Publisher $caller
		 * @return bool
		 * @throws \TypeError
		 * @throws \Exception
		 */
		public function update($event, Publisher $caller): bool
		{
			$time = microtime(true);
			try {
				if (false === ($ret = $this->fire($event, $caller))) {
					return $ret;
				}
			} catch (\Exception | \TypeError $e) {
				if (!Cardinal::is($event, Events::SUCCESS)) {
					// try to gracefully handle success event propagation that fails
					// catastrophically because of an exception bubble up or strict-typing malady
					throw $e;
				}

				pause(">>> \033[31mWARNING WARNING WARNING EXCEPTION REPORTED: %s\e[0m\n\e[33m%s\e[0m",
					$e->getMessage(), $e->getTraceAsString());

				return error("Exception raised on `%s': `%s'", \get_class($this), $e->getMessage());
			}
			debug(sprintf('%.5f %s %s enabled', microtime(true) - $time, $event, \get_class($this)));

			return true;
		}

		public function fire($event, SiteConfiguration $caller): bool
		{
			if (!$this instanceof ServiceInstall || !$this->hasChange()) {
				return true;
			}
			$enabled = $this->ctx->getServiceValue(null, 'enabled');
			$event = explode('.', $event, 2);
			switch (array_pop($event)) {
				case Events::SUCCESS:
					$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR, true);
					$ret = false;
					try {
						if ($enabled && ($ret = $this->populate($caller))) {
							return true;
						}

					} catch (\apnscpException $e) {
						// terminate early before commitService() callback is encountered
						error("failed to commit service configuration for `%(service)s': %(msg)s",
							['service' => $this->ctx->getModuleName(), 'msg' => $e->getMessage()]
						);

					} finally {
						\Error_Reporter::exception_upgrade($oldex);

						return $ret || $this->depopulateUnmount($caller);
					}
				case Events::FAILURE:
					if ($enabled) {
						return $this->depopulateUnmount($caller);
					}

					return true;
				default:
					fatal("unrecognized event `%s'", $event);
			}
		}

		private function depopulateUnmount(SiteConfiguration $caller): bool
		{
			$ret = $this->depopulate($caller);
			if ($this instanceof MountableLayer) {
				$module = $this->getLayerName() ?? $this->ctx->getModuleName();
				$layercls = new ServiceLayer($this->site);
				$layercls->uninstallServiceLayer($module);
				if (!$layercls->unmount($module)) {
					warn("failed to unmount service `%s' from `%s'", $module, $this->site);
				}
			}

			return $ret;
		}

		public function getDescription(): string
		{
			try {
				$rfxn = new \ReflectionClassConstant(static::class, 'DESCRIPTION');
				if (null !== ($val = $rfxn->getValue())) {
					return $val;
				}
			} catch (\ReflectionException $e) {}

			return 'Enable ' . $this->ctx->getModuleName() . ' service';
		}

		public function getValidatorRange()
		{
			return '[0,1]';
		}
	}