<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Spamfilter;

	use Opcenter\Filesystem;
	use Opcenter\Mail\Services\Spamassassin;
	use Opcenter\Mail\Services\Spamassassin\Filter;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Validators\Common\MustBeEnabled;
	use Opcenter\SiteConfiguration;

	class Enabled extends MustBeEnabled implements ServiceInstall
	{
		use \FilesystemPathTrait;

		public const DESCRIPTION = 'Mail filtering';

		public function populate(SiteConfiguration $svc): bool
		{
			if (!Spamassassin::present()) {
				return true;
			}

			if (!is_dir($path = $svc->getAccountRoot() . '/etc/skel/.spamassassin')) {
				Filesystem::mkdir($path);
			}

			if (!file_exists($path . '/user_prefs')) {
				Filesystem::touch($path . '/user_prefs', $svc->getAuthContext()->user_id, $svc->getAuthContext()->group_id, 0644);
				$filter = Filter::instantiateContexted($svc->getAuthContext());
				$svc->getSiteFunctionInterceptor()->file_put_file_contents(
					'/etc/skel/.spamassassin/user_prefs',
					$filter->template()
				);

				$filter->create();
			}

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			if (!Spamassassin::present()) {
				return true;
			}

			if (file_exists($path = $svc->getAccountRoot() . '/etc/skel/.spamassassin')) {
				Filesystem::rmdir($path);
			}

			return true;
		}
	}

