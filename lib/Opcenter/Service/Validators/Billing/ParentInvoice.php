<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Billing;

	use Opcenter\Reseller\Hierarchies\Billing;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;
	use Opcenter\SiteConfiguration;

	class ParentInvoice extends Invoice implements ServiceExplicitReconfiguration
	{
		const DESCRIPTION = 'Account is subordinate to parent whose "invoice" matches this';
		const MAP_FILE =Billing::MAP_FILE;

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('billing', 'enabled')) {
				$value = null;
				$hierarchy = new Billing;
				if ($this->ctx->isDelete()) {
					unset($hierarchy[$this->site]);
				} else {
					$hierarchy[$this->site] = null;
				}
				return $hierarchy->check() ?: error("Billing dependency cycle: %s", $hierarchy->getLastException()->getMessage());
			}

			if (ctype_digit($value)) {
				return error("Invoice may not only consist of numbers");
			}

			if (RESELLER_ATOMIC_INVOICES && !$this->mapKeyInUse($value, Invoice::MAP_FILE)) {
				// accounts perform platform migration, parent remains behind
				return error("Invoice `%s' is not attached to an account yet - attach to an account by setting billing,invoice first",
					$value);
			}

			if (0 === strncmp($value, 'site', 4) && ctype_digit(substr($value, 4))) {
				return error('billing identifier cannot follow form siteXX');
			}

			if ($value === $this->ctx['invoice']) {
				return error("%(service1)s,%(value1)s references itself as %(service2)s,%(value2)s", [
					'service1' => 'billing', 'value1' => 'parent_invoice',
					'service2' => 'billing', 'value2' => 'invoice'
				]);
			}
			$parent = (new Invoice($this->ctx, $this->site))->getMapValue($value);
			if (!$parent) {
				// missing on server
				return RESELLER_ATOMIC_INVOICES ? error("Parent invoice %s not found", $parent) : true;
			}

			$hierarchy = new Billing;
			$hierarchy[$this->site] = $parent;
			return $hierarchy->check() ?: error("Billing dependency cycle: %s", $hierarchy->getLastException()->getMessage());
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$hierarchy = new Billing;
			if ($this->ctx->isDelete()) {
				// validate loops
				if (isset($hierarchy[$this->site])) {
					$hierarchy->check();
					unset($hierarchy[$this->site]);
				}
			} else {
				$parent = $new ? (new Invoice($this->ctx, $this->site))->getMapValue($new) : null;
				if ($parent !== false || RESELLER_ATOMIC_INVOICES) {
					$hierarchy[$this->site] = $parent;
				}
			}
			return $hierarchy->write();
		}

		public function populate(SiteConfiguration $svc): bool
		{
			$site = $this->ctx['parent_invoice'];
			return $this->reconfigure(null, $site, $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure(null, null, $svc);
		}
	}