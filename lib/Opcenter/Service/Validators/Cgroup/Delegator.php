<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2023
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Reseller;
	use Opcenter\Reseller\Hierarchies\Cgroup as CgroupMap;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Validators\Common\GenericDuplicateMap;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Cgroup;

	class Delegator extends GenericDuplicateMap implements ServiceExplicitReconfiguration, ServiceInstall
	{
		const MAP_FILE = CgroupMap::MAP_FILE;
		const DESCRIPTION = 'Delegating site or group (cgroupv2)';

		public function valid(&$value): bool
		{
			if (!$value && Cgroup::version() === 1) {
				return true;
			}

			if (!$value && !$this->ctx['enabled'] && !$this->ctx->isRevalidation()) {
				return true;
			}

			if (Cgroup::version() === 1) {
				return error("Delegative authority requires cgroupv2");
			}

			if ($value && !$this->ctx['enabled']) {
				return warn("%(service)s,%(param)s disabled for site. Ignoring value",
					['service' => 'cgroup', 'param' => 'enabled']);
			}

			$hierarchy = new CgroupMap;
			if ($this->ctx->isDelete() || (!$this->ctx['enabled'] && $this->ctx->isRevalidation())) {
				$hierarchy->check();
				// validate loops
				if ($hierarchy->hasDependents($this->site)) {
					return error("Removing resource group `%(site)s' would orphan the following groups: %(groups)s", [
						'site'  => $this->site,
						'groups' => implode(', ', $hierarchy->getDependents($this->site))
					]);
				}
			}

			if (!$value) {
				return true;
			}

			if (!\Auth::site_exists($value) && !Reseller::exists($value)) {
				return error("Unknown delegative authority `%(name)s'", ['name' => $value]);
			}

			if ($value === $this->site) {
				return error("Group cannot reference itself");
			}

			$group = new Cgroup\Group($value);
			if (!$group->exists()) {
				return error("%(service)s is not enabled for %(site)s", [
					'service' => 'cgroup',
					'site' => $value
				]);
			}

			$hierarchy[$this->site] = $value;

			return $hierarchy->check() ?: error("%(type)s dependency cycle: %(err)s",
				['type' => 'cgroup', 'err' => $hierarchy->getLastException()->getMessage()]);
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure(null, $this->ctx['delegator'], $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx['delegator'], null, $svc);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$hierarchy = new CgroupMap;
			$hierarchy->check();
			if ($this->ctx->isDelete()) {
				// validate loops
				if (!isset($hierarchy[$this->site])) {
					return true;
				}
				if ($hierarchy->hasDependents($this->site)) {
					return error("Removing %(site)s would orphan the following resource groups: %(groups)s", [
						'site' => $this->site, 'groups' => implode(', ', $hierarchy->getDependents($this->site))
					]);
				}
			}

			if (!$old && !$new) {
				return true;
			}

			if ($new) {
				if (!(new Cgroup\Group($new))->exists()) {
					return error("Delegator `%s' does not have existing cgroup structure", $new);
				}
			} else if (!$this->ctx->isDelete() && $hierarchy->hasDependents($this->site)) {
				// allow moving to top-level or elsewhere
				warn("%(site)s is no longer constrained by resources of %(delegator)s",
					['site' => $this->site, 'delegator' => $old]);
			}

			$oldGroup = new Cgroup\Group($this->site);
			if ($new) {
				$hierarchy[$this->site] = $new;
			} else {
				unset($hierarchy[$this->site]);
			}

			$hierarchy->write();
			$newGroup = new Cgroup\Group($new ?: $this->site);

			if ($old && !array_first(CgroupMap::reader()->fetchAll(), static fn($v, $k) => $v === $old)) {
				debug("%(site)s has no more children. Moving %(site)s cgroup to non-nested layout", ['site' => $old]);
				defer($_, static fn() => (new Cgroup\Group($old . '/' . Cgroup\Group::CIRCULAR_IDENTIFIER))->migrate($old));
			}

			if (!in_array($this->site, $newGroup->descendants(true))) {
				$oldGroup->migrate($newGroup);
			}
			return \Opcenter\Provisioning\Cgroup::createControllerConfiguration($svc) && \Opcenter\Provisioning\Cgroup::bindToController($svc);
		}

	}