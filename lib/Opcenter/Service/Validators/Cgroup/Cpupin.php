<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Cgroup;

	class Cpupin extends ServiceValidator implements ServiceReconfiguration
	{
		const DESCRIPTION = 'CPU pinning';

		public function valid(&$value): bool
		{
			if (null === $value || $value === [] || !$this->ctx->getServiceValue('cgroup', 'enabled')) {
				$value = null;

				return true;
			}

			if (!in_array('cpuset', Cgroup::getControllers(), true)) {
				return error("%s cgroup must be enabled", 'cpuset');
			}

			foreach ((array)$value as $val) {
				if (!\is_int($val) || $value < 0) {
					return error('CPU pinning must be whole number >= 0');
				}

			}

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old === $new) {
				return true;
			}

			if (null !== $old && $new === null) {
				$controller = Cgroup\Controller::make(new Cgroup\Group($svc->getSite()), 'cpuset');
				$controller->exists() && $controller->delete();
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}
	}