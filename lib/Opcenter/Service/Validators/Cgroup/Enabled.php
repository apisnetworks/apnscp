<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Provisioning\Cgroup;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Cgroup\Group;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements AlwaysValidate, ServiceInstall, ServiceReconfiguration
	{
		use \FilesystemPathTrait;
		public const DESCRIPTION = 'Resource enforcement';

		public function valid(&$value): bool
		{
			if (!$this->ctx->isRevalidation() && $this->ctx->isEdit()) {
				if ($value && !$this->ctx->getOldServiceValue(null, 'enabled')) {
					$this->ctx->applyDefaults();
				}
				if (!$value && $descendants = ($group = new Group($this->site))->descendants()) {
					return error("Disabling cgroup on %(name)s would orphan the following descendants: %(other)s", [
						'name' => (string)$group, 'other' => implode(', ', $descendants)
					]);
				}
			}
			return parent::valid($value);
		}

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			Cgroup::bindToController($svc);

			return $this->reconfigure('', 1, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$afi = \apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login()));
			if ($afi->cgroup_frozen($this->site)) {
				warn("Unfreezing previously-frozen site");
				$afi->cgroup_thaw($this->site);
			}

			if (!$new) {
				return $old ? $this->depopulate($svc) : true;
			}

			$changed = array_diff_assoc((array)$this->ctx->getNewServiceValue(), (array)$this->ctx->getOldServiceValue());
			unset ($changed['version']);
			if ($old && !$changed && !$svc->hasValidatorOption('reconfig')) {
				return true;
			}

			// propagate new cgroup values from cache
			$svc->getAuthContext()->getAccount()->reset($svc->getAuthContext());

			if (!$this->ctx->isEdit() || $this->ctx->getOldServiceValue('cgroup', 'delegator') === $this->ctx->getNewServiceValue('cgroup', 'delegator')) {
				Cgroup::createControllerConfiguration($svc);
				Cgroup::bindToController($svc);
			}

			Cgroup::createModuleConfiguration($svc, 'cgroup.apache-config');

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$group = new Group($svc->getSite());
			foreach ($group->descendants(true) as $descendant) {
				info("Removing descendant cgroup `%(name)s' within `%(parent)s'", [
					'name' => $descendant,
					'parent' => (string)$group
				]);
				rmdir(\Opcenter\System\Cgroup::CGROUP_HOME . '/' . $descendant);
			}
			Cgroup::deleteModuleConfiguration($svc->getSite());
			Cgroup::removeController($svc);
			\Opcenter\System\Cgroup::removeConfiguration($group);

			return true;
		}


		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}


	}