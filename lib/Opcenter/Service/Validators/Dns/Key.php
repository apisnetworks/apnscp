<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Dns;

	use Opcenter\CliParser;
	use Opcenter\Crypto\Keyring;
	use Opcenter\Crypto\KeyringTrait;
	use Opcenter\Dns;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Validators\Common\ProviderKey;

	class Key extends ProviderKey
	{
		use KeyringTrait;

		const DESCRIPTION = 'DNS provider key';

		public function valid(#[\SensitiveParameter] &$value): bool
		{
			if (!$this->ctx['enabled']) {
				return parent::valid($value);
			}

			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$default = DNS_PROVIDER_KEY;
				$value = $this->ctx['provider'] && $default ? CliParser::parseArgs($default) : null;
			}

			if ($value && Keyring::is($value)) {
				$oldval = &$value;
				unset($value);
				$value = $this->readKeyringValue($oldval);

				if (!Keyring::reference($oldval)) {
					defer($_, static function () use (&$value, &$oldval) {
						// update encoding if module changes it
						if ($value !== $oldval) {
							debug("Keyring value changed, updating");
							$value = Keyring::encode($value);
							$oldval = $value;
						}
					});
				}
			}
			$provider = $this->ctx['provider'] ?? 'null';

			if ($provider === DefaultNullable::NULLABLE_MARKER) {
				$provider = Dns::default();
			}
			if (Dns::providerHasHelper($provider)) {
				return Dns::getProviderHelper($provider)->valid($this->ctx, $value);
			}

			return parent::valid($value);
		}

		public function getDefault()
		{
			return DNS_PROVIDER_KEY ?? '';
		}

	}