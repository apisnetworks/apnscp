<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Dns;

	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Validators\Common\IpValidation;
	use Opcenter\SiteConfiguration;

	class Proxyaddr extends IpValidation implements AlwaysValidate, DefaultNullable
	{
		const VALUE_RANGE = 'address | null';

		public function valid(&$value): bool
		{
			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$type = 'DNS_PROXY_' . ($this->is4()  ? 'IP4' : 'IP6');
				$value = \constant($type);
			}

			if (!$value) {
				return true;
			}

			$checker = '\\Opcenter\\Net\\' . ($this->is4() ? 'Ip4' : 'Ip6');

			if ($value && !\is_array($value)) {
				$value = [$value];
			}

			foreach ((array)$value as $v) {
				if (!$checker::valid($v)) {
					return error("Invalid IPv%d value: `%s'",
						$this->is4() ? 4 : 6,
						$v
					);
				}

				if (\in_array($v, $checker::nb_pool(), true)) {
					return error("Remote IP address `%(ip)s' defined in ".
						"[%(section)s] => [%(option)s] already part of IP address pool", [
							'ip' => $v,
							'section' => 'dns',
							'option' => 'proxy_ip' . ($this->is4() ? '4' : '6')
					]);
				}
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function getDescription(): ?string
		{
			$is4 = $this->is4();
			return 'External ' . ($is4 ? 'IPv4' : 'IPv6') . ' address for DNS records if behind NAT';
		}

		protected function is4() {
			return false === strpos(static::class, '6addr');
		}

		public function getDefault()
		{
			return \constant('DNS_PROXY_IP' . ($this->is4() ? '4' : '6'));
		}


	}