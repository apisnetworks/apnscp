<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2024
	 */


	namespace Opcenter\Service\Validators\Diskquota;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Filesystem\Quota\Project;
	use Opcenter\Reseller;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Validators\Common\GenericDuplicateMap;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Kernel;

	class Group extends GenericDuplicateMap implements ServiceReconfiguration, ServiceExplicitReconfiguration, ServiceInstall, DefaultNullable
	{
		use \FilesystemPathTrait;

		const DESCRIPTION = 'Quota group support (project, reseller)';

		public function valid(&$value): bool
		{
			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}

			if (!$value) {
				return true;
			}

			if (!Project::supported()) {
				return error(Project::ERR_FEATURE_UNSUPPORTED);
			}

			if (!Project::exists($value)) {
				return error(Project::ERR_PROJECT_MISSING, ['name' => $value]);
			}

			return true;
		}

		public function getDefault()
		{
			$id = $this->ctx->getServiceValue('reseller', 'id');
			if (!$id) {
				return null;
			}
			return Reseller::RESELLER_PREFIX . $id;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old === $new) {
				return true;
			}

			if (!$old) {
				$project = new Project($new);
				if (version_compare(Kernel::version(), '6', '<')) {
					// possibly delated to
					// https://patchwork.kernel.org/project/linux-fsdevel/patch/20240304155013.115334-2-aalbersh@redhat.com/#25745744
					Cardinal::register(['*', Events::END], fn(string $event, SiteConfiguration $svc) => $project->add($this->domain_shadow_path()));
					return debug("Deferring project quota activation until end of provisioning");

				}

				return $project->add($this->domain_shadow_path());
			}

			$project = new Project($old);
			return !$new ? $project->remove($this->domain_shadow_path(), $svc->hasValidatorOption('force')) : $project->reassign($new, $this->domain_shadow_path());
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure(null, $this->ctx['group'], $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx->getOldServiceValue(null, 'group'), null, $svc);
		}

	}