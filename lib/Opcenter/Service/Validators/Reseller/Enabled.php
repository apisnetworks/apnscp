<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Opcenter\Service\Validators\Reseller;

use Opcenter\Service\Validators\Common\Enabled as Base;

class Enabled extends Base {
	public function valid(&$value): bool
	{
		if (!parent::valid($value)) {
			return false;
		}

		if (!RESELLER_ENABLED && $this->ctx['id']) {
			return error("[reseller] => enabled must be enabled in config.ini");
		}

		return true;
	}
}