<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter;

	class Terminal
	{
		public static function formatPortRange($portindex): array
		{
			$fmt = array('4%03u0', '4%03u9');

			return array_map(static function ($p) use ($fmt) {
				return array(
					(int)sprintf($fmt[0], $p),
					(int)sprintf($fmt[1], $p)
				);
			}, $portindex);
		}
	}
