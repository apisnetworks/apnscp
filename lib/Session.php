<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Session helper
	 */
	class Session
	{
		public static function append($param, $value)
		{
			$p = (array)self::get($param);

			return self::set($param, array_merge($p, $value));
		}

		public static function get($key, $default = null)
		{
			return array_get($_SESSION, $key, $default);
		}

		public static function set($key, $value)
		{
			return array_set($_SESSION, $key, $value);
		}

		public static function forget($keys)
		{
			return array_forget($_SESSION, $keys);
		}

		/**
		 * Key exists and is not null
		 *
		 * @param $name
		 * @return bool
		 */
		public static function has($name): bool
		{
			$placeholder = new stdClass;
			$item = array_get($_SESSION, $name, $placeholder);
			return $item !== $placeholder;
		}

		/**
		 * Key exists
		 *
		 * @param $key
		 * @return bool
		 */
		public static function exists($key)
		{
			return array_has($_SESSION, $key);
		}

		public static function token()
		{
			return array_get($_SESSION, \Auth_UI::CSRF_KEY);
		}
	}