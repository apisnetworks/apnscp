<?php

	use Opcenter\Filesystem;
	use Opcenter\Net\Port;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Service_Terminal
	{
		const SOCK_PATH = 'run/terminal.sock';
		const PIDFILE = 'shellinabox.pid';
		const PROCNAME = 'shellinabox';
		const THEME_PATH = 'terminal/shellinabox/share/css';
		// prevent Util_Process::decompose() from breaking down theme name
		const SPACING_EQUIVALENCY_MARKER = "\xc2\xa0";

		/**
		 * Conditionally shellinabox if not running
		 *
		 * @return bool
		 */
		public static function autostart(): bool
		{
			if (self::running()) {
				$port = self::port()['local_port'] ?? 22;
				$stat = \Opcenter\Process::stat(self::_get_pid());
				$filtered = array_filter($stat['args'] ?? [], static function ($arg) use ($port) {
					return false !== strpos($arg, ":$port");
				});

				if (\count($filtered) > 0) {
					return true;
				}
			}

			self::stop();

			return self::start();
		}

		/**
		 * shellinabox is running
		 *
		 * @return bool
		 */
		public static function running(): bool
		{
			$pid = self::_get_pid();
			$procfile = '/proc/' . $pid . '/cmdline';
			if (!$pid || !file_exists($procfile)) {
				return false;
			}
			$cmd = file_get_contents($procfile);

			return false !== strpos($cmd, self::PROCNAME);
		}

		/**
		 * Get shellinabox PID
		 *
		 * @return int|null
		 */
		private static function _get_pid(): ?int
		{
			$path = self::_pid_path();
			if (!file_exists($path)) {
				return null;
			}

			return (int)file_get_contents($path);
		}

		/**
		 * PID path
		 *
		 * @return string
		 */
		private static function _pid_path(): string
		{
			$path = storage_path('run') . '/' . self::PIDFILE;

			return $path;
		}

		/**
		 * Stop shellinaboxd
		 *
		 * @return bool
		 */
		public static function stop(): bool
		{
			$pidpath = self::_pid_path();
			if (self::running()) {
				$pid = trim(file_get_contents($pidpath));
				dlog('Found terminal backend resident... killing');
				// specify negative pid to kill children as well
				if (!posix_kill('-' . $pid, SIGKILL)) {
					error("! Failed to kill terminal backend with pid `%d'", $pid);
				}
				unlink($pidpath);
			}
			// kill dangling shellinabox instances
			$script = realpath(INCLUDE_PATH . '/bin/scripts/kill_terminals.sh');
			if (file_exists($script)) {
				Util_Process_Fork::exec($script);
			}

			return true;
		}

		/**
		 * Get listening port/protocol
		 */
		public static function port(): ?array
		{
			$pid = (int)file_get_contents('/run/sshd.pid');
			$ports = Port::fromPid($pid);
			return array_pop($ports);
		}

		/**
		 * Start shellinaboxd
		 *
		 * @return mixed
		 */
		public static function start(): bool
		{
			$base = INCLUDE_PATH;
			$pidpath = self::_pid_path();

			$path = realpath($base . '/apps/terminal/shellinabox/bin');
			// can't grab pty on systemd/RHEL7 unless root, *sigh*
			$cmd = '%(path)s/shellinaboxd -t --unixdomain-only=%(sockpath)s:%(suser)s:%(sgroup)s:600 --background=%(pid)s -s /:SSH:localhost:%(port)d --no-beep --localhost-only --user=root --static-file=styles.css:%(css)s %(has-style)s%(styles)s';
			$hasStyle = $themeList = null;
			if (SSH_THEME) {
				$themes = static::compileThemeList();
				ksort($themes);
				$hasStyle = '--user-css=';
				$themeList = escapeshellcmd(implode(',', array_key_map(static function ($k, $v) {
					$chk = str_replace(static::SPACING_EQUIVALENCY_MARKER, ' ', $k);
					$v = (\strtolower($chk) === SSH_THEME ? '+' : '-') . $v;

					return str_replace(' ', '\ ', $k) . ':' . str_replace(' ', '\ ', $v);
				}, $themes)));
			}

			$ret = Util_Process_Fork::exec($cmd, [
				'sockpath' => storage_path(self::SOCK_PATH),
				'suser' => APNSCP_SYSTEM_USER,
				'sgroup' => APNSCP_SYSTEM_USER,
				'port' => self::port()['local_port'] ?? 22,
				'pid' => $pidpath,
				'path' => $path,
				'css' => static::getBaseTheme(),
				'has-style' => $hasStyle,
				'styles' => $themeList
			]);

			// @TODO do a better job reaping old ssh processes
			return $ret['success'];
		}

		/**
		 * Get path for base theme
		 *
		 * @return string
		 */
		private static function getBaseTheme(): string
		{
			if (!file_exists($path = config_path('/custom/apps/' . static::THEME_PATH  . '/shellinabox.css'))) {
				$path = webapp_path(static::THEME_PATH . '/shellinabox.css');
			}
			return $path;
		}

		/**
		 * Merge default + theme overrides
		 *
		 * @return array
		 */
		private static function compileThemeList(): array
		{
			$list = static::buildFromDirectory(webapp_path(static::THEME_PATH)) + append_config(static::buildFromDirectory(config_path('custom/apps/' . static::THEME_PATH)));
			return array_build($list, static function ($k, $v) { return [key($v), array_pop($v)]; });
		}

		/**
		 * Generate theme list from directory
		 *
		 * @param string $path
		 * @return array
		 */
		private static function buildFromDirectory(string $path): array
		{
			if (!is_dir($path)) {
				return [];
			}
			$dir = $path;
			return \Opcenter\Filesystem::readdir($path, static function ($file) use ($dir) {
				if (substr($file, -4) !== '.css' || 0 !== strncmp($file, 'theme-', 6)) {
					return null;
				}

				$title = str_replace(' ', static::SPACING_EQUIVALENCY_MARKER, ucwords(str_replace(array('-', '_'), ' ', substr($file, 6, -4))));
				return [$title => $dir . DIRECTORY_SEPARATOR . $file];
			});
		}

		/**
		 * Restart shellinabox
		 *
		 * @return bool
		 */
		public static function restart(): bool
		{
			self::stop();

			return self::start();
		}


	}