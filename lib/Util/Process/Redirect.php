<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2024
	 */

	class Util_Process_Redirect extends Util_Process
	{
		public function run($cmd, ...$args)
		{
			if (empty($this->callbacks['redirect'])) {
				fatal("Redirect requires '%s' callback", 'redirect');
			}

			return parent::run($cmd, $args);
		}


		protected function addOutput(object|string $pipe, string $data): void
		{
			if (is_resource($pipe)) {
				$pipe = $this->resource2Descriptor($pipe);
			}
			$this->callback($pipe, $data);
		}
	}