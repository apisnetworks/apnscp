<?php

	/**
	 * Scheduled process support via at
	 *
	 * MIT License
	 *
	 * @author  Matt Saladna <matt@apisnetworks.com>
	 * @license http://opensource.org/licenses/MIT
	 * @version $Rev: 2567 $ $Date: 2016-11-19 23:19:37 -0500 (Sat, 19 Nov 2016) $
	 */
	class Util_Process_Schedule extends Util_Process
	{
		// atd spool dir
		const SPOOL_DIR = '/var/spool/at';
		const AT_CMD = 'at';
		const AT_ID_VAR = '__APNSCP_ATD_ID';

		/**
		 * @var DateTime
		 */
		private $_time;
		private $_id;

		/**
		 * Schedule a process to be run at
		 * a specific time
		 */

		public function __construct($arg1 = 'now', $arg2 = null, $arg3 = null)
		{
			if (!is_null($arg2)) {
				// signature 1: $format, $time, DateTimeZone $tz = null
				$this->__constructSig1($arg1, $arg2, $arg3);
			} else if (!is_object($arg1)) {
				// signature 2: $time
				$this->__constructSig2($arg1);
			} else if ($arg1 instanceof DateTime) {
				// signature 3: DateTime $d
				$this->__constructSig3($arg1);
			} else {
				fatal('unparseable timespec arguments provided %s/%s/%s', $arg1, $arg2, $arg3);
			}
			$d = $this->_time;
			$this->_parse($d);
			parent::__construct();
		}

		public function __constructSig1($format, $time, DateTimeZone $tz = null)
		{
			$this->_time = DateTime::createFromFormat($format, $time, $tz);
			if (!$this->_time) {
				fatal("unparseable date/time spec `%s' from format `%s'",
					$time, $format);
			}
		}

		public function __constructSig2($time)
		{
			$this->_time = new DateTime($time);
			if (!$this->_time) {
				fatal("unparseable date/time spec `%s'", $time);
			}
		}

		public function __constructSig3(DateTime $d)
		{
			$this->_time = $d;
		}

		private function _parse(DateTime $d)
		{
			/**
			 * atd only accepts UTC for timezone
			 * ensure tz is converted to compatible UTC zone
			 *
			 * **NOTE** there is a bug with 3.1.8 < atd <= 3.1.13-22
			 * in RedHat with parsing UTC timezones:
			 * https://bugzilla.redhat.com/show_bug.cgi?id=1328832
			 */
			return $d->setTimezone(new DateTimeZone('UTC'))
				->format("H:i \U\T\C m/d/Y");
		}

		final public static function exec($cmd, $args = null, $exits = array(0), $opts = array())
		{
			return error('cannot statically call exec() with Util_Proc_Schedule');
		}

		public function setID(string $id, \Auth_Info_User $ctx = null): self
		{
			if (!is_readable(self::SPOOL_DIR)) {
				warn("ID support unavailable: cannot access atd spool `%s', verify permissions?");
				return $this;
			}
			$id = $this->formatID($id, $ctx ?? null);
			$this->setEnvironment(self::AT_ID_VAR, $id);
			$this->_id = $id;

			return $this;
		}

		/**
		 * Format job marker
		 * @param string $id
		 * @param string|null $site
		 * @return string
		 */
		private function formatID(string $id, ?\Auth_Info_User $ctx): string
		{
			if (null === $ctx || null === $ctx->site) {
				return $id;
			}

			return $ctx->site . '-' . $id;
		}

		public function run($cmd, ...$args)
		{
			if ($this->_id && ($procid = $this->idPending($this->_id, null))) {
				return error("pending process `%s' already scheduled with job id `%s'",
					$procid['job'],
					$this->_id
				);
			}
			$spec = $this->_parse($this->_time);
			if (false === $spec) {
				return error("unparseable timespec `%s'", $this->_time);
			}
			if (static::AT_CMD == 'batch') {
				$spec = null;
			}
			$safecmd = sprintf('echo %s | ' . static::AT_CMD . '  %s 2> /dev/null',
				escapeshellarg($cmd),
				$spec
			);

			$cmd = $safecmd;
			$safe = new Util_Process_Safe();
			$safe->setEnvironment($this->getEnvironment());

			return $safe->run($cmd, ...$args);
		}

		/**
		 * Program already exists in atd queue with ID
		 *
		 * When $id is null all pending jobs for $ctx are cancelled
		 *
		 * @param null|string          $id
		 * @param null|\Auth_Info_User $ctx
		 * @return array job data
		 */
		public function idPending(?string $id, ?\Auth_Info_User $ctx): array
		{
			$search = $this->formatID((string)$id, $ctx ?? null);
			// a* is at
			// b* is batch
			// =* is running
			$files = glob(self::SPOOL_DIR . '/[ab=]*', GLOB_NOSORT);
			foreach ($files as $f) {
				if (!file_exists($f) && 0 !== strncmp($basename = basename($f), '=', 1)) {
					// race condition
					$f = \dirname($f) . "/={$basename}";
					if (!file_exists($f)) {
						continue;
					}
				}
				$contents = file_get_contents($f);
				if (!preg_match('/^' . self::AT_ID_VAR . '=(.*?)(?:; export ' . self::AT_ID_VAR . ')?$/m', $contents, $matches)) {
					continue;
				}
				if ($matches[1] === $search || (!$id && 0 === strpos($matches[1], $search))) {
					return $this->parseName(basename($f));
				}
			}

			return [];
		}

		/**
		 * Parse job meta from filename
		 *
		 * @param string $name
		 * @return array queue, job, ts elements
		 */
		private function parseName(string $name): array
		{
			[$queue, $jobno, $timestamp] = sscanf($name, '%c%5lx%8lx');

			return [
				'queue' => $queue,
				'job'   => (int)$jobno,
				'ts'    => (int)$timestamp * 60
			];
		}

		/**
		 * Another job exists in the batch with an earlier runtime
		 *
		 * @param string $key optional job ID to examine
		 * @return null|int
		 */
		public function preempted(string $key = null, \Auth_Info_User $ctx = null): ?int
		{
			$key = $key ?? $this->_id;
			if (null === $key) {
				error('No job ID set - set with setID()');

				return null;
			}
			if (!$id = $this->idPending($key, $ctx)) {
				return null;
			}
			// allow a sooner reload to preempt
			if ($id['ts'] > $this->getTimestamp()) {
				$this->cancelJob($id['job']);

				return null;
			}

			return $id['job'];
		}

		/**
		 * Get job run timestamp
		 *
		 * @return int
		 */
		public function getTimestamp(): int
		{
			return $this->_time->getTimestamp();
		}

		/**
		 * Cancel a queued job
		 *
		 * @param int $id
		 * @return bool
		 */
		public function cancelJob(int $id): bool
		{
			$ret = \Util_Process::exec(['atrm', '%d'], $id);

			return $ret['success'];
		}
	}