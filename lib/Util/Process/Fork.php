<?php

	/**
	 * A forking implementation for Util_Process
	 *
	 * MIT License
	 *
	 * @author  Matt Saladna <matt@apisnetworks.com>
	 * @license http://opensource.org/licenses/MIT
	 * @version $Rev: 2141 $ $Date: 2016-04-14 12:57:23 -0400 (Thu, 14 Apr 2016) $
	 */
	class Util_Process_Fork extends Util_Process
	{

		/**
		 * Set user to drop permissions on fork
		 *
		 * @param int|string $user
		 */
		public function setUser($user): void {
			$this->setUserWrapper($user, 'user');
		}

		public function setEuser($user): void {
			$this->setUserWrapper($user, 'euser');
		}

		public function setPriority(int $prio)
		{
			if (!function_exists('pcntl_setpriority')) {
				fatal('pcntl extension not loaded');
			}
			if ($prio < -20 || $prio > 19) {
				return error("invalid priority specified `%d'", $prio);
			}
			$this->opts['priority'] = $prio;
		}

		/**
		 * Wrapper to set user
		 * @param        $user
		 * @param string $type
		 */
		private function setUserWrapper($user, string $type) {
			if (ctype_digit($user)) {
				$user = (int)$user;
			} else if (is_string($user)) {
				if (null === ($tmp = array_get(posix_getpwnam($user), 'uid'))) {
					fatal('Unknown user %s passed', $user);
				}
				$user = $tmp;
			}
			if (!posix_getpwuid($user)) {
				fatal('Unknown uid passed: %d', $user);
			}
			$this->opts[$type] = $user;
		}

		public function run($cmd, ...$args)
		{
			if (!function_exists('pcntl_fork')) {
				fatal("can't fork! posix functions missing");
			}
			$this->_init($cmd, $args);


			if (is_array($this->getCommand(true)[0])) {
				fatal("execve-format not supported yet");
			}
			if ($this->getCommand(true)[0] !== '/') {
				// pcntl_fork barfs if an absolute path isn't given
				// path discovery is part of the shell
				return error('command path must be absolute path');
			}

			$pid = pcntl_fork();
			// zombie prevention

			if ($pid === -1) {
				fatal('fork failed!');
			}
			if ($pid) {
				$status = null;
				if ($this->getOption('leader')) {
					// setsid immediately spins off process, no longer a child so SIGCHLD is meaningless
					// prevent zombie by fetching wait status
					pcntl_waitpid($pid, $status);
				} else {
					// still associated with parent, use SIGCHLD callback
					$oldhandler = pcntl_signal_get_handler(SIGCHLD);
					if (!$oldhandler) {
						pcntl_signal(SIGCHLD, static function ($signal) use ($oldhandler, $pid) {
							do {
								$wpid = pcntl_waitpid(-1, $status);
								if ($pid === $wpid) {
									// direct descendent finally exited, restore handler
									pcntl_signal(SIGCHLD, $oldhandler);
								}
							} while ($wpid > 0);
						});
					}
				}
				$ret = parent::format();
				$ret['success'] = ($pid > 0);
				$ret['return'] = $pid;
				$this->callback('close', $pid > 0 ? 0 : 1);
				return $ret;
			}

			$this->callback('open', $cmd);

			$parts = self::decompose($this->getCommand());

			// setsid detaches from terminal, closes fd
			if ($this->getOption('leader')) {
				posix_setsid();
			} else {
				if ($this->getOption('mute_stdin') && is_resource(STDIN)) {
					fclose(STDIN);
				}
				if ($this->getOption('mute_stderr') && is_resource(STDERR)) {
					fclose(STDERR);
				}
				if ($this->getOption('mute_stdout') && is_resource(STDOUT)) {
					fclose(STDOUT);
				}
			}

			$this->restoreHandlers();
			if (null !== ($uid = $this->getOption('user'))) {
				if (false === posix_setuid((int)$uid)) {
					fatal('setuid failed!');
				}
			} else if (null !== ($uid = $this->getOption('euser'))) {
				if (false === posix_seteuid((int)$uid)) {
					fatal('seteuid failed!');
				}
			} else {
				// voluntarily drop uid unless specified
				\Opcenter\Process::dropEffectiveUid();
			}
			pcntl_setpriority(array_get($this->opts, 'priority', 0));
			pcntl_exec($parts['cmd'], $parts['args'], $this->getEnvironment());
			exit();
		}

		private function restoreHandlers()
		{
			/**
			 * Signals that are set to be ignored in the existing process image
			 * are also set to be ignored in the new process image.
			 * All other signals are set to the default action in the new process image.
			 *
			 * @link https://www.gnu.org/software/libc/manual/html_node/Executing-a-File.html
			 */
			$signals = [
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				8,
				10,
				11,
				12,
				13,
				14,
				15,
				16,
				17,
				18,
				20,
				21,
				22,
				23,
				24,
				25,
				26,
				27,
				28,
				29,
				30,
				31
			];
			foreach ($signals as $signal) {
				pcntl_signal($signal, SIG_DFL);
			}
		}

		/**
		 * Permit open callbacks
		 *
		 * @inheritDoc
		 */
		public function addCallback($function, $when = 'read', ...$args)
		{
			$tmp = false !== ($pos = strpos($when, '.')) ? substr($when, 0, $pos) : $when;
			if ($tmp === 'open' || $tmp === 'close') {
				return parent::addCallback($function, $when, ...$args);
			}

			fatal('callbacks not supported in forking mode');
		}
	}