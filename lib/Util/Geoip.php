<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use GeoIp2\Model\City;
	use GeoIp2\WebService\Client;

	class Util_Geoip
	{
		use ContextableTrait;

		const DB_NAME = 'GeoLite2-City.mmdb';

		private static $_cache;
		protected ?string $key;
		protected ?int $id;

		public function __construct(int $id = null, string $key = null)
		{
			$this->key = $key ?? constant('MAXMIND_GEOIP_ID');
			$this->id  = (int)($id ?? constant('MAXMIND_GEOIP_KEY'));
		}

		/**
		 * Fetch geolocation data on IP
		 *
		 * @param string $ip
		 * @return array|bool
		 */
		public function locate(string $ip)
		{
			if (false === ($ipkey = inet_pton($ip))) {
				return error("invalid ip address `%s'", $ip);
			}

			$cachekey = 'geoip:' . $this->getLocale() . ':' . $ipkey;
			$cache = Cache_Super_Global::spawn();
			if (false !== ($resp = $cache->get($cachekey))) {
				self::$_cache[$ipkey] = $resp;

				return $resp;
			}

			$raw = [];
			try {
				$raw =  $this->raw($ip);
			} catch (Exception $e) {
				// pass
			}

			$ret = array_merge(array(
				'country'    => 'UNKNOWN',
				'isocountry' => null,
				'state'      => 'UNKNOWN',
				'isostate'   => null,
				'city'       => 'UNKNOWN',
				'postalcode' => 0,
				'latitude'   => 0,
				'longitude'  => 0,
			), array_filter((array)$raw));

			$cache->set($cachekey, $ret);
			return $ret;
		}

		/**
		 * Send raw query using best data source
		 *
		 * @param string $ip
		 * @return City|null
		 * @throws \GeoIp2\Exception\AddressNotFoundException
		 * @throws \GeoIp2\Exception\AuthenticationException
		 * @throws \GeoIp2\Exception\GeoIp2Exception
		 * @throws \GeoIp2\Exception\HttpException
		 * @throws \GeoIp2\Exception\InvalidRequestException
		 * @throws \GeoIp2\Exception\OutOfQueriesException
		 */
		public function raw(string $ip): ?array
		{
			$handler = $this->autoload();
			$locale = $this->getLocale();

			if ($handler instanceof  \MaxMind\Db\Reader) {
				$resp = $handler->get($ip);

				$fieldMaps = [
					'country'    => "country.names.$locale",
					'isocountry' => 'country.iso_code',
					'state'      => "subdivisions.0.names.$locale",
					'isostate'   => "subdivisions.0.iso_code",
					'city'       => "city.names.$locale",
					'postalcode' => 'postal.code',
					'latitude'   => 'location.latitude',
					'longitude'  => 'location.longitude'
				];
			} else {
				$resp = $handler->city($ip);
				$fieldMaps = [
					'country'    => "country.names.$locale",
					'isocountry' => "country.isoCode",
					'state'      => "mostSpecificSubdivision.names.$locale",
					'isostate'   => "mostSpecificSubdivision.isoCode",
					'city'       => "city.names.$locale",
					'postalcode' => "postal.code",
					'latitude'   => "location.latitude",
					'longitude'  => "location.longitude"
				];
			}

			if (!$resp) {
				return null;
			}

			return array_combine(array_keys($fieldMaps), array_map(static function ($k) use ($resp) {
				return data_get($resp, $k);
			}, $fieldMaps));
		}

		/**
		 * Local database or remote API
		 *
		 * @return Client|\MaxMind\Db\Reader|Client
		 * @throws \MaxMind\Db\Reader\InvalidDatabaseException
		 */
		protected function autoload() {
			$dbPath = resource_path('storehouse/' . self::DB_NAME);

			if (file_exists($dbPath)) {
				return new MaxMind\Db\Reader($dbPath);
			}

			if (!$this->id || !$this->key) {
				debug("Cannot use Maxmind - both GeoLite2 city database and API key missing");
			}

			return new Client($this->id, $this->key);
		}

		/**
		 * Desired locale
		 *
		 * @return string
		 */
		private function getLocale(): string
		{
			return 'en';
		}
	}