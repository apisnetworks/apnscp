<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Util_API extends SoapClient
	{
		use apnscpFunctionInterceptorTrait {
			__call as callProtected;
		}

		const DEFAULT_SERVER = 'localhost';
		const PORT = 2082;
		const SECURE_PORT = 2083;
		const WSDL_PATH = SOAP_WSDL;

		/**
		 * Create new API client
		 *
		 * @param array|string $key API key or [key, totp] pair
		 * @param string|null  $server
		 * @param int|null     $port
		 * @param array        $ctor additional constructor arguments to SoapClient
		 * @return self
		 */
		public static function create_client(array|string $key, string $server = null, int $port = null, array $ctor = []): self
		{
			if (!$server) {
				$server = self::DEFAULT_SERVER . ':' . self::PORT;
			} else {
				if (!$port) {
					$port = $server === self::DEFAULT_SERVER ? self::PORT : self::SECURE_PORT;
				}
				$server .= ':' . $port;
			}
			$proto = $port === self::PORT || !$port ? 'http' : 'https';
			$uri = $proto . '://' . $server . '/soap';
			$wsdl = str_replace('/soap', '/' . self::WSDL_PATH, $uri);

			$streamCtx = [];
			if (\Opcenter\Net\IpCommon::valid(strtok($server, ':'))) {
				// connecting by IP, no public CAs provide DNS SNI so this is already trusted in pki
				$streamCtx = ['ssl' => ['verify_peer_name' => false]];
			}

			if (is_array($key)) {
				[$key, $totp] = array_values($key);
				$streamCtx = array_merge_recursive($streamCtx, [
					'http' => [
						'header' => \Auth_SOAP::EXTENDED_AUTH_HEADER . ': ' . $totp
					]
				]);
			}
			$connopts = $ctor + array(
				'connection_timeout' => 30,
				'location'           => $uri,
				'uri'                => Net_WSDL::NAMESPACE,
				'trace'              => is_debug(),
				'stream_context'     => stream_context_create($streamCtx)
			);
			$connopts['location'] = $uri . '?authkey=' . $key;

			return new static($wsdl, $connopts);
		}

		public static function create_key($comment = null)
		{
			$afi = apnscpFunctionInterceptor::init();
			$key = $afi->auth_create_api_key($comment);

			return $key;
		}

		#[\ReturnTypeWillChange]
		public function __call(string $function_name, array $arguments)
		{
			static $ctr = 0;
			$ret = @parent::__call($function_name, $arguments);
			if ($ret !== null || $ctr >= 5) {
				$ctr = 0;

				return $ret;
			}
			// 50 ms sleep
			usleep(50000);
			$ctr++;

			return $this->__call($function_name, $arguments);
		}

		public static function get_key($search = '')
		{
			$afi = apnscpFunctionInterceptor::init();
			$keys = $afi->auth_get_api_keys();
			foreach ($keys as $k) {
				if (false !== strpos($k['comment'], $search)) {
					return $k;
				}
			}

			return null;
		}

		public function sessionId(): ?string
		{
			try {
				$this->common_whoami();
			} catch (\SoapFault) { }

			return array_get($this->__getCookies(), \apnscpSession::SESSION_ID . '.0');
		}
	}