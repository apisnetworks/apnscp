<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Account + user hook subsystem
	 */
	class Util_Account_Hooks
	{
		use \ContextableTrait;

		private static $_hooks = array(
			'create',
			'edit',
			'disable',
			'delete',
			'delete_user',
			'edit_user',
			'create_user',
			// ssl certificate install
			'reload'
		);

		// triggering mode
		private static $_mode;

		/**
		 * Process a hook
		 *
		 * @param string $type
		 * @param array  $args
		 * @return bool
		 */
		public function run($type, array $args = array()): bool
		{
			if (!in_array($type, self::$_hooks)) {
				return error("unknown hook name `%s'", $type);
			}

			self::$_mode = $type;
			$ret = $this->_process($type, (array)$args);
			self::$_mode = null;

			if ($ret !== true) {
				return warn("%s hook failed on module `%s'", $type, $ret);
			}

			return $ret;
		}

		private function _process($type, $hookargs)
		{
			$auth = $this->getAuthContext();
			if ($auth->level & (PRIVILEGE_SITE | PRIVILEGE_USER) && substr($type, -5) !== '_user') {
				// reset profile to avoid cache assume editing a user makes no difference
				// NB: edit happens after data is committed on v7.5 platforms,
				// so cur will always be merge: new -> old when _edit is called
				$auth->getAccount()->reset($auth);
			}

			// ensure auth information is properly reset before calling hooks
			DataStream::get($auth)->setOption(apnscpObject::RESET)->query('common_get_base_path');
			debug("Running hooks for `%s' (user: `%s')", $auth->domain, $auth->username);
			$afi = apnscpFunctionInterceptor::factory($auth);
			$modules = $afi->list_all_modules();
			/**
			 * Build a listing first of known service configurations that have corresponding modules
			 * then merge all unmapped modules without a corresponding service config
			 */
			$depmap = \Opcenter\SiteConfiguration::prioritizeAllModules();
			$ordered = array_unique(array_merge($depmap,
				array_diff($modules, array_map('\Opcenter\SiteConfiguration::getModuleFromService', $depmap))));
			if (0 === strncmp(self::$_mode, 'delete', 6)) {
				// make sure siteinfo comes last for delete operations
				$ordered = array_reverse($ordered);
			}
			$called = [];

			foreach ($ordered as $module) {
				if (null === ($internalName = \Opcenter\SiteConfiguration::getModuleFromService($module))) {
					debug("Skipping reconfiguration on service config `%s', no known module mapping", $module);
					continue;
				}

				$class = apnscpFunctionInterceptor::get_class_from_module($internalName);
				$hookname = '_' . $type;
				$rfxn = new ReflectionClass($class);
				if (!$rfxn->implementsInterface(\Module\Skeleton\Contracts\Hookable::class)) {
					continue;
				} else if (!$rfxn->hasMethod($hookname)) {
					// reload is not part of formal Hookable definition
					debug("missing `%s' hook on `%s' module", $type, $module);
					continue;
				}
				if (isset($called[$class])) {
					continue;
				}
				/**
				 * certain modules (siteinfo => site, web => apache) can have
				 * a corresponding "enabled" field - to avoid unnecessary
				 * module interaction, let site config metadata determine
				 * invocation
				 */

				if (null !== ($state = $afi->common_get_service_value($module, 'enabled'))) {
					if ($type === 'edit') {
						// v7 platforms don't propagate old correctly
						$old = $auth->getAccount()->old[$module];
						$new = $auth->getAccount()->new[$module];
						if ($new['enabled'] !== $old['enabled']) {
							if ($new['enabled']) {
								// disable -> enable
								$hookname = '_create';
							} else {
								// enable -> disable
								$hookname = '_delete';
							}
						}
					} else if (!$state && (0 === strncmp($type, 'create', 6) || 0 === strncmp($type, 'delete', 6))) {
						debug("Service config `%s' disabled for site - disabling calling hook `%s' on `%s'", $module,
							$hookname, $class);
						continue;
					}
				}

				$start = microtime(true);
				$called[$class] = 1;
				// set params in the event that this is called from backend
				$c = $class::autoloadModule($auth);

				$response = call_user_func_array(array($c, $hookname), $hookargs);
				debug('%.5f: %s -> %s', microtime(true) - $start, $class, $hookname);
				if ($response === false) {
					// @XXX aliased services can return false positives on which service
					// triggered the failure
					// @see \Opcenter\SiteConfiguration::MODULE_CLASS_MAP
					return $internalName;
				}
				unset($c);
			}

			return true;
		}

		public static function active()
		{
			return !is_null(self::$_mode);
		}

		public static function is_mode($mode)
		{
			return self::$_mode === $mode;
		}
	}