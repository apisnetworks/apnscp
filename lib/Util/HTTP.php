<?php

use Opcenter\Net\IpCommon;

/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Util_HTTP
	{
		public static function forwardNoProxy()
		{
			// disallow cp.apisnetworks.com proxy usage
			header('No-Proxy: true');
		}

		public static function getClientIP()
		{
			return \Auth::client_ip();
		}

		/**
		 * Create a URL
		 *
		 * @param string $path
		 * @return string
		 */
		public static function makeUrl($path = '')
		{
			$proto = 'http' . (self::isSecure() ? 's' : '');
			$host = self::getHost();

			return "{$proto}://{$host}/" . ltrim($path, '/');
		}

		public static function isSecure()
		{
			return isset($_SERVER['HTTPS']) || 'https' === ($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? 'http');
		}

		public static function getHost()
		{
			if (MISC_CP_PROXY) {
				if (false !== ($pos = strpos(MISC_CP_PROXY, '//'))) {
					return substr(MISC_CP_PROXY, $pos + 2);
				}

				return MISC_CP_PROXY;
			}

			if (isset($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_X_FORWARDED_HOST']) && HTTP_TRUSTED_FORWARD) {
				foreach (HTTP_TRUSTED_FORWARD as $fwd) {
					if (IpCommon::in($_SERVER['REMOTE_ADDR'], $fwd)) {
						return true;
					}
					return $_SERVER['HTTP_X_FORWARDED_HOST'];
				}
			}

			if (isset($_SERVER['HTTP_HOST'])) {
				return $_SERVER['HTTP_HOST'];
			}
			if (isset($_SERVER['SERVER_ADDR'])) {
				return $_SERVER['SERVER_ADDR'];
			}
			if (defined('SERVER_NAME')) {
				return constant('SERVER_NAME');
			}

			return '';
		}

		/**
		 * Download a file
		 *
		 * @param $url
		 * @param $savepath
		 * @return bool|string path file saved to or false on error
		 * @throws Exception
		 */
		public static function download(string $url, string $savepath): bool
		{
			if (\extension_loaded('curl')) {
				$adapter = new \HTTP_Request2_Adapter_Curl();
			} else {
				$adapter = new \HTTP_Request2_Adapter_Socket();
			}

			$http = new \HTTP_Request2(
				$url,
				\HTTP_Request2::METHOD_GET,
				array(
					'adapter'    => $adapter,
					'store_body' => false
				)
			);

			$observer = new \HTTP_Request2_Observer_SaveDisk($savepath);
			$http->attach($observer);
			try {
				$response = $http->send();
				$code = $response->getStatus();
				switch ($code) {
					case 200:
						break;
					case 403:
						return error("URL `%s' request forbidden by server", $url);
					case 404:
						return error("URL `%s' not found on server", $url);
					case 301:
					case 302:
						$newLocation = $response->getHeader('location');
						return self::download($newLocation, $savepath);
					default:
						return error("URL request failed, code `%d': %s",
							$code, $response->getReasonPhrase());
				}
				$content = $response->getHeader('content-type');
				$okcontent = ['application/octet-stream', 'application/zip', 'application/x-bzip2'];
				if (!\in_array($content, $okcontent, true)) {
					\Error_Reporter::report($content);
				}
				// this returns nothing as xfer is saved directly to disk
				$http->getBody();
			} catch (\HTTP_Request2_Exception $e) {
				return error("fatal error retrieving URL: `%s'", $e->getMessage());
			}

			return true;
		}

		/**
		 * Verify if given filename came in through upload
		 *
		 * @param $filename
		 * @return bool
		 */
		public static function upload_safe($filename)
		{
			$uploaddir = ini_get('upload_tmp_dir');
			if (!$uploaddir) {
				$uploaddir = sys_get_temp_dir();
			}

			return 0 === strpos($filename, $uploaddir . '/') &&
				file_exists($filename) &&
				fileowner($filename) === WS_UID;
		}
	}