<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class apnscpException extends \Exception
	{
		protected string $class;

		public function __toString(): string
		{
			$result = '[Exception, message="' . $this->message . '", ' .
				'code="' . $this->code . '", ' .
				'file="' . $this->file . '", ' .
				'line="' . $this->line . '"]';

			return $result;
		}

		function getBacktrace($html = IS_ISAPI)
		{
			$str = ($html ? '<h3>[Backtrace]</h3><code><pre>' : "[Backtrace]\n");
			foreach (debug_backtrace() as $bc) {
				if (isset($bc['class'])) {
					$s = ($html ? '<b>%s</b>' : '%s') . '::';
					$str .= sprintf($s, $bc['class']);
				}
				if (isset($bc['function'])) {
					$s = ($html ? '<b>%s</b>' : '%s');
					$str .= sprintf($s, $bc['function']);
				}

				$str .= ' (';

				if (isset($bc['args'])) {
					foreach ($bc['args'] as $arg) {
						$s = ($html ? '<i>%s</i>, ' : '%s, ');
						$str .= sprintf($s, gettype($arg));
					}
					$str = substr($str, 0, -2);
				}

				$str .= ')';
				$str .= ': ';
				$str .= '[ ';
				if (isset($bc['file'])) {
					$dir = substr(dirname($bc['file']), strrpos(dirname($bc['file']), '/') + 1);
					$file = basename($bc['file']);
					if ($html) {
						$str .= '<a href="file:/' . $bc['file'] . '">';
					}
					$str .= $dir . '/' . $file;
					if ($html) {
						$str .= '</a>';
					}
				}
				$str .= isset($bc['line']) ? ', ' . $bc['line'] : '';
				$str .= ' ] ';
				$str .= ($html ? "</pre></code>\n" : "\n");
			}

			return ($str);
		}

		function printBacktrace($html = IS_ISAPI)
		{
			if (!is_debug()) {
				Error_Reporter::report("BT:\n" . $this->class . ' error: ' . $this->message . "\n" . "[Backtrace]\n" . $this->getTraceAsString() . "\n\n" . "Context:\n" . $this->getContext(false));
				if ($html) {
					if (ob_get_length()) {
						ob_end_clean();
					}
					include(INCLUDE_PATH . '/htdocs/html/apps/error/500.php');
				}
				exit;
			} else {
				print ($html ? '<code><pre>' : '') . $this->class . ' error: ' . $this->message . "\n";
				print "[Backtrace]\n" . $this->getTraceAsString() . "\n" . ($html ? '</pre></code>' : '');
				print($this->getContext() . "\n");
				print $html ? '</pre></code>' : '';
			}
		}

		function getContext($html = IS_ISAPI)
		{
			$ctx = ($html ? "<code><pre>[Context]\n" : "[Context]\n");
			$plusMinus = 5; /* go five lines forward and back */
			/*foreach ($this->getTrace() as $stack) {*/
			$fp = fopen($this->getFile(), 'r');
			for ($i = 1, $target = $this->getLine(); $i <= $target + $plusMinus; $i++) {
				if ($i < ($target - $plusMinus)) {
					fgets($fp);
					continue;
				}
				$ctx .= "$i " . ($html ? htmlentities(fgets($fp)) : fgets($fp));
			}
			/*}*/
			$ctx .= ($html) ? '</pre></code>' : '';
			fclose($fp);

			return $ctx;
		}
	}

	class ArgumentError extends apnscpException
	{
		public function __construct($mMessage = null, $mCode = 0)
		{
			$this->class = __CLASS__;
			$this->message = $mMessage;
			$this->code = $mCode;
		}
	}

	class PermissionError extends apnscpException
	{
		public function __construct($mMessage = null, $mCode = 0)
		{
			$this->class = __CLASS__;
			$this->message = $mMessage;
			$this->code = $mCode;
		}
	}

	class FileError extends apnscpException
	{
		public function __construct($mMessage = null, $mCode = 0)
		{
			$this->class = __CLASS__;
			$this->message = $mMessage;
			$this->code = $mCode;
		}
	}

	class IOError extends apnscpException
	{
		public function __construct($mMessage = null, $mCode = 0)
		{
			$this->class = __CLASS__;
			$this->message = $mMessage;
			$this->code = $mCode;
		}
	}

	if (!class_exists(ValueError::class, false)) {
		class ValueError extends apnscpException
		{
			public function __construct($mMessage = null, $mCode = 0)
			{
				$this->class = __CLASS__;
				$this->message = $mMessage;
				$this->code = $mCode;
			}
		}
	}

	class SocketError extends apnscpException
	{
		public function __construct($mMessage = null, $mCode = 0)
		{
			$this->class = __CLASS__;
			$this->message = $mMessage;
			$this->code = $mCode;
		}
	}

	class MySQLError extends apnscpException
	{
		public function __construct($mMessage = null, $mCode = 0)
		{
			$this->class = __CLASS__;
			if (is_object($mMessage)) {
				$repmsg = var_export($mMessage, true);
			} else {
				$repmsg = $mMessage;
			}
			Error_Reporter::report($repmsg);
			$this->message = 'General DB error';
			$this->code = $mCode;

		}

		public function __call($func, $args)
		{
			deprecated_func("exception __call() - $func (%s)",
				join("\n", $args));
		}
	}

	class PostgreSQLError extends apnscpException
	{
		public function __construct($mMessage = null, $mCode = 0)
		{
			$this->class = __CLASS__;
			$this->message = $mMessage;
			$this->code = $mCode;
		}
	}

?>