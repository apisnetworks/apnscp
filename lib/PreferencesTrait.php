<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, September 2024
 */


trait PreferencesTrait {
	use ContextableTrait;

	private function preferencesPath(\Auth_Info_User $ctx = null): string
	{
		$ctx ??= $this->getAuthContext();

		return match ($ctx->level & PRIVILEGE_ALL) {
			$ctx->level & (PRIVILEGE_SITE | PRIVILEGE_USER) => $ctx->domain_info_path("users/{$ctx->username}"),
			$ctx->level & PRIVILEGE_ADMIN => implode(DIRECTORY_SEPARATOR,
				[\Admin_Module::ADMIN_HOME, \Admin_Module::ADMIN_CONFIG, $ctx->username]),
			$ctx->level & PRIVILEGE_RESELLER => implode(DIRECTORY_SEPARATOR,
				[\Reseller_Module::ADMIN_HOME, \Reseller_Module::ADMIN_CONFIG, $ctx->username])
		};
	}
}