RewriteEngine On
RewriteRule ^{{ \Module\Support\Letsencrypt::ACME_URI_PREFIX }}/(.*)$ {{ \Module\Support\Letsencrypt::ACME_WORKDIR . \Module\Support\Letsencrypt::ACME_URI_PREFIX }}/$1 [L]
RewriteRule ^ /var/www/html/suspended.html [L]