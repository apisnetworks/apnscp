@foreach ($ips as $ip)
	Listen {{ $ip }}:{{ $port }} {{ $proto ?? 'http' }}
@endforeach