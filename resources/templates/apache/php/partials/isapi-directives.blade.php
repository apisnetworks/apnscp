<IfDefine PHP_INSTALLED>
	php_value sendmail_path "/usr/sbin/sendmail -t -i -f postmaster@@svc("siteinfo","domain")"
	php_value mail.force_extra_parameters "-f postmaster@@svc("siteinfo","domain")"
	php_value include_path ".:{!! $svc->getAccountRoot() !!}/usr/local/share/pear:/usr/share/pear"
	php_admin_value open_basedir "{!! $svc->getAccountRoot() !!}:/etc:/usr/local:/usr/bin:/usr/sbin:/tmp:/proc:/dev:{{ FILESYSTEM_SHARED }}
</IfDefine>

{{-- Protection in event mod_php missing from server --}}
<IfDefine !PHP_INSTALLED>
	<Files "*.php">
		Require all denied
	</Files>
</IfDefine>