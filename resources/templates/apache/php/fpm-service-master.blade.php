# via {{ $templatePath }}
[Unit]
Description=PHP worker master control
@if (version_compare(os_version(), '8', '<'))
DefaultDependencies=no
Requires=sysinit.target
@endif
After=network.target

[Service]
ExecStart=/bin/true
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
