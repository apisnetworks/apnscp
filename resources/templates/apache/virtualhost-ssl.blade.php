{{-- HTTP configuration that appears for SSL requests --}}
<IfModule ssl_module>
    @includeWhen(!HTTPD_ALL_INTERFACES && !$svc->getServiceValue('ipinfo','namebased'), 'listen-directive', ['ips' => $ips, 'port' => $port, 'proto' => $proto])

    <VirtualHost @include('common.ip-addresses', ['ips' => $ips, 'port' => $port])>
        @include("common.virtualhost-config")

        SSLEngine On
        SSLCertificateKeyFile {!! $svc->getAccountRoot() !!}{!! \a23r::get_class_from_module('ssl')::KEY_PATH !!}/{!! \a23r::get_class_from_module('ssl')::DEFAULT_CERTIFICATE_NAME !!}.key
        SSLCertificateFile {!! $svc->getAccountRoot() !!}{!! \a23r::get_class_from_module('ssl')::CRT_PATH !!}/{!! \a23r::get_class_from_module('ssl')::DEFAULT_CERTIFICATE_NAME !!}.crt

        Include {{ \Opcenter\Http\Apache::CONFIG_PATH }}/{{ $svc->getSite() }}.ssl

        {{-- locate in config/custom/resources/templates/apache/, affects SSL virtualhost --}}
        @includeIf("virtualhost-ssl-custom")
    </VirtualHost>
</IfModule>
