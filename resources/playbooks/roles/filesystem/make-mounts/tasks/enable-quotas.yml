# Possible to put FST on separate mount than apnscp_account_root
# Cannot check apnscp_filesystem_template/apnscp_is_crosslinked as with copy-link.yml
- name: Check if / and {{ apnscp_account_root }} on same mount point
  block:
    - stat: path=/
      register: s1
    - stat: path={{ apnscp_account_root }}
      register: s2
    - set_fact:
        _separate_home_mount: "{{ s1.stat.dev != s2.stat.dev }}"
        cacheable: yes
    - name: Get device of {{ apnscp_account_root }}
      command: "findmnt -n -ro TARGET,FSTYPE --target {{ apnscp_account_root | quote }}"
      register: o
      changed_when: false
      when: _separate_home_mount | bool
    - set_fact:
        _home_mount_path: "{{ o.stdout.split(' ') | first }}"
        _home_fs_type: "{{ o.stdout.split(' ') | last }}"
      when: _separate_home_mount | bool

- name: Set mount options
  block:
    - name: Alter mount options for / (ext)
      include_tasks: file=set-mount-features.yml
      vars:
        mount: /
        options: "{{ ext4_fs_features + ext4_quota_features }}"
        notify: Rebuild quotas
  when: xfs.rc != 0 and ext4.rc == 0

- name: Alter mount options for {{ apnscp_account_root }} (ext)
  include_tasks: file=set-mount-features.yml
  vars:
    mount: "{{ _home_mount_path }}"
    fstype: ext4
    notify: Rebuild quotas
    options: "{{ ext4_fs_features + ext4_quota_features }}"
  when:
    - _separate_home_mount | bool
    - _home_fs_type == "ext4"

- name: Alter mount options for / (xfs)
  include_tasks: file=set-mount-features.yml
  vars:
    mount: /
    options: "{{ xfs_fs_features + xfs_quota_features }}"
    state: present
  when: xfs.rc == 0
- name: Alter mount options for {{ _home_mount_path }} (xfs)
  include_tasks: file=set-mount-features.yml
  vars:
    mount: "{{ _home_mount_path }}"
    fstype: xfs
    options: "{{ xfs_fs_features + xfs_quota_features }}"
    state: present
  when:
    - _separate_home_mount | bool
    - _home_fs_type == "xfs"

- name: Alter mount options for xfs
  block:
    - name: Remove incompatible xfs features
      set_fact:
        xfs_quota_features: "{{ xfs_quota_features | reject('search', 'prjquota') | list }}"
      when:
        - "-1 == lookup('pipe', 'xfs_info ' + ansible_mounts | json_query('[?mount==`/`] | [0].device')).find('crc=1')"

    - name: Set xfs kernel flags
      include_role: name=system/kernel tasks_from=set-kernel-opts.yml
      vars:
        rootflags: "{{ xfs_quota_features }}"

    # Minimize reboot count
    - include_role: name=system/cgroup tasks_from=configure-cgroupv2.yml
      when: cgroup_unified_hierarchy

  when: xfs.rc == 0

- name: Verify quota package installed
  yum: name=quota state=present

- meta: flush_handlers
