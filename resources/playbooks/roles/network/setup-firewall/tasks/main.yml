---
- name: "Disable iptables"
  systemd:
    name: "{{ iptsvc }}"
    state: stopped
    masked: yes
    enabled: no
  with_items:
    - iptables
    - ip6tables
    - ebtables
  loop_control:
    loop_var: iptsvc
  ignore_errors: true
- name: Update firewalld packages
  yum:
    name: firewalld
    state: latest
    disablerepo: "*"
    enablerepo: apnscp-testing
  when: ansible_distribution_version is version('8.4', '>=')
- name: Verify firewalld running
  systemd: name=firewalld state=started enabled=yes

  # nftables doesn't provide direct translation to ipset yet, which is relied on by whitelist/blacklist features
- name: Use iptables backend
  ini_file:
    path: /etc/firewalld/firewalld.conf
    section: null
    option: FirewallBackend
    value: iptables
    no_extra_spaces: yes
  when: ansible_distribution_major_version != '7'
  notify: Restart firewalld
  register: r
- name: Retain fail2ban rules on reload
  ini_file:
    path: /etc/firewalld/firewalld.conf
    section: null
    option: FlushAllOnReload
    value: "no"
    no_extra_spaces: yes
  notify: Restart firewalld
- meta: flush_handlers
- name: Drop nftables drop policy
  command: "nft delete table inet {{ table }}"
  ignore_errors: true
  when: r.changed
  with_items:
    - firewalld_policy_drop
    - firewalld
  loop_control:
    loop_var: table

- name: Set firewall state tracker
  set_fact: network_setup_firewall_marker=true
- name: Set firewall rules
  include_tasks: "add-firewall.yml"
  vars:
    immediate: "{{ item.immediate | default(None) }}"
    port: "{{ item.port | default(None) }}"
    service: "{{ item.service | default(None) }}"
    state: '{{ item.state | default(None) }}'
    zone: '{{ item.zone | default(None) }}'
    rich_rule: '{{ item.rich_rule | default(None) }}'
    permanent: '{{ item.permament | default(None) }}'
  with_items:
    "{{ rules }}"
  when: item.when is not defined or item.when
  loop_control:
    label: "Setting rule {{ item.port | default(item.rule is defined | ternary(item.rule, item.service)) }} => {{ item.state | default('enabled') }}"
- name: Create ipset list
  include_tasks: create-ipset.yml
  with_items:
    - whitelist
    - blacklist
    - whitelist6
    - blacklist6
    - name: ignorelist
      timeout: 0
    - name: ignorelist6
      timeout: 0
  loop_control:
    loop_var: ipset_name
    label: "Create ipset {{ ipset_name }}"
- name: "Apply direct firewalld rules"
  include_tasks: "set-direct-rule.yml"
  vars:
    permanent: "{{ item.permanent | default(None) }}"
    mode: "{{ item.mode | default('add') }}"
    net: "{{ item.net | default(default_net) }} "
    table: "{{ item.table | default(default_table) }}"
    chain: "{{ item.chain | default(default_chain) }}"
    priority: "{{ item.priority | default('0') }}"
    rule: "{{ item.rule }}"
    notify:
      - Restart firewalld
      - Reload fail2ban
  with_items: "{{ direct_rules }}"
  loop_control:
    label: "Setting {{ item.net | default(default_net, true) }} {{ item.rule }}"
# Defer reloading fail2ban until after fail2ban/configure-jails
# Missing fail2ban.log will cause reload to fail on first run
