---
- include_role: name=apnscp/initialize-filesystem-template tasks_from=relocate-shared-directory
  vars:
    service: "siteinfo"
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    logrotate_vars: "{{ item.logrotate_vars | default(None) }}"
    force: "{{ item.force | default(false) }}"
  with_items: "{{ dovecot_relocatable_paths }}"
- include_vars:
    file: ../configure-postfix/defaults/main.yml
  when: mail_configure_postfix_marker is not defined
- include_vars:
    file: ../rspamd/defaults/main.yml
  when: mail_rspamd_marker is not defined and spamfilter == "rspamd"

- name: Verify virtual system  directories exists
  file:
    path: "{{ item }}"
    state: directory
    owner: dovecot
  with_items:
    - "{{ dovecot_virtual_rundir }}"
    - "{{ dovecot_shared_root }}/run"
  loop_control:
    label: "Creating {{ item }} as needed"
  notify: Reload filesystem template

- include_vars:
    file: ../spamassassin/defaults/main.yml
  when: mail_spamassassin_marker is not defined
- include_vars:
    file: ../../software/haproxy/defaults/main.yml
  when: software_haproxy_marker is not defined and haproxy_enabled

- name: Verify Dovecot starts on boot
  systemd:
    name: dovecot
    enabled: yes
    state: started
  failed_when: false

- name: Remove conflicting Dovecot configuration
  file:
    path: "{{ dovecot_conf_dir }}/{{ item }}"
    state: absent
  with_items: "{{ dovecot_conflicting_conf }}"
  loop_control:
    label: "Removing conflicting configuration {{ dovecot_conf_dir}}/{{item}}"
  notify: Restart dovecot

- name: Query Dovecot version
  command: >
    rpm -q --queryformat="%{VERSION}" dovecot
  register: release
  changed_when: false
- name: Set dovecot_version
  set_fact:
    dovecot_version: "{{ release.stdout }}"
- name: Copy Dovecot configuration
  template:
    src: "apnscp.conf.j2"
    dest: "{{ dovecot_conf_dir }}/apnscp.conf"
    force: yes
  notify: Restart dovecot

- name: Create Dovecot Sieve directory
  file: path="{{ dovecot_sieve_bindir }}" state=directory

- name: Install Dovecot Sieve helpers
  template:
    src: learn.sh.j2
    dest: "{{ dovecot_sieve_bindir }}/learn-{{ item }}.sh"
    mode: 0755
  vars:
    type: "{{ item }}"
  with_items:
    - ham
    - spam

- name: Install Dovecot Sieve
  template:
    src: "{{ item }}"
    dest: '{{ dovecot_sieve_bindir }}/{{ item | basename | regex_replace("\.j2$", "")}}'
    force: yes
  with_fileglob: "{{role_path }}/templates/*.sieve.j2"
  notify:
    - Restart dovecot
    - Compile sieved applications

- meta: flush_handlers

- name: Link indexer service to "{{ dovecot_shared_root }}"
  file:
    path: "{{ dovecot_virtual_rundir }}/indexer"
    state: link
    src: "{{ dovecot_shared_root }}/run/indexer"
  notify: Reload filesystem template

- name: Link hibernate service to "{{ dovecot_shared_root }}"
  file:
    path: "{{ dovecot_virtual_rundir }}/imap-hibernate"
    state: "{{ dovecot_use_hibernate | bool | ternary('link', 'absent') }}"
    src: "{{ dovecot_use_hibernate | bool | ternary(dovecot_shared_root + '/run/imap-hibernate', omit) }}"
  notify: Reload filesystem template

- name: Link stats-mail to "{{ dovecot_shared_root }}"
  file:
    path: "{{ dovecot_virtual_rundir }}/stats-mail"
    state: "{{ dovecot_enable_stats | bool | ternary('link', 'absent') }}"
    src: "{{ dovecot_enable_stats | bool | ternary(dovecot_shared_root + '/run/stats-mail', omit) }}"
  notify: Reload filesystem template

- name: Override Dovecot service
  include_role: name=systemd/override-config
  vars:
    service: dovecot
    config:
      - group: Service
        vars:
          Delegate: "yes"
    notify: Restart dovecot
