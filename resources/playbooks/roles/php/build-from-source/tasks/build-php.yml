- stat: path="{{ build_dir }}"
  register: st
- name: Download PHP {{ __php_version }}
  unarchive:
    dest: "{{ work_dir }}"
    src: "http://php.net/get/php-{{ __php_version }}.tar.bz2/from/this/mirror"
    remote_src: yes
    owner: "nobody"
    group: "nobody"
  register: result
  retries: "{{ network_max_retries }}"
  until: result is succeeded
  when: not st.stat.exists or remove_all_versions

- name: Apply PHP patches
  patch:
    basedir: "{{ build_dir }}"
    src: "{{ patch.name }}"
    strip: 1
  when: patch.when | default(True)
  with_items: "{{ php_patches.get(__php_version | regex_replace('^([0-9]+)\\.([^\\.]*).*$', '\\1.\\2'), []) }}"
  loop_control:
    loop_var: patch
    label: "Apply {{ patch.name }}"

- name: Configure PHP {{ __php_version }}
  shell: >-
    {{ php_configure }} {{ _extra_flags_composite }} \
    {{ php_build_flags }} {{ lookup('vars', 'php' + (__php_version | regex_replace('^([0-9]+)\.([^\.]*).*$', '\1\2')) + '_build_flags', default='') }}
  args:
    chdir: "{{ build_dir }}"
  environment:
    SCANDIR: "{{ scan_dir | replace(multiphp_php_prefix, '', 1) }}"
    PREFIX: "{{ multiphp_build | ternary('/', '') }}"
    FD_SIZE: "{{ php_has_isapi | ternary('2048', '1024') }}"
    MYSQL_DRIVER: >-
      {{ lookup('vars', 'php' + (__php_version | regex_replace('^([0-9]+)\.([^\.]*).*$', '\1\2')) + '_native_mysql', default=php_native_mysql) | bool | ternary('mysqlnd', 'libmysqlclient') }}
  register: configuration
  # make can potentially fail in low memory environments, force stop clamd
- name: Verify clamd stopped (OOM)
  systemd: name=clamd@scan state=stopped
  when:
    - clamav_enable | bool
    - ansible_memfree_mb < 768
  failed_when: false
  ignore_errors: true
  notify: Restart clamd
- name: Compile PHP {{ __php_version }} (10+ minute wait)
  make:
    chdir: "{{ build_dir }}"
    params:
      '--jobs': "{{ compile_max_jobs }}"
      '--max-load': '{{ ansible_processor_vcpus * 1.25 }}'
  environment:
    TMPDIR: "{{ build_tmpdir }}"
- command: "strip {{ build_dir }}/.libs/libphp{{(__php_version is version('8.0', '<')) | ternary(__php_version | regex_replace('^(\\d+)\\..*$', '\\1'), '') }}.so"
  when:
    - not php_debug
    - not multiphp_build
    - php_has_isapi
- command: "strip {{ build_dir }}/sapi/fpm/php-fpm"
  when: not php_debug

- command: "strip {{ build_dir }}/sapi/cli/php"
  when: not php_debug

- include_tasks: sysphp.yml
  when:
    - not multiphp_build

- include_tasks: multiphp.yml
  when:
    - multiphp_build | bool
    - not panel_build

- name: Clean PHP code
  make:
    chdir: "{{ build_dir }}"
    target: clean
  when: php_clean
