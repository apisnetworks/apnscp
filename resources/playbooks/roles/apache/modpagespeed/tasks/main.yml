---
- name: Verify mod-pagespeed installed
  yum:
    name: "{{ pagespeed_package }}"
    state: "{{ pagespeed_enabled | ternary('present', 'absent') }}"
- name: Remove Pagespeed configuration
  file:
    path: "{{ item }}"
    state: absent
  with_items:
    - "{{ pagespeed_config }}"
    - "/etc/tmpfiles/modpagespeed.conf"
    - "/var/cache/mod_pagespeed"
  when: not pagespeed_enabled
  notify: Reload apache
- block:
  - name: Check if Pagespeed installed
    stat: path={{ pagespeed_config }}
    register: st
  - name: Set Pagespeed defaults
    blockinfile:
      path: "{{ pagespeed_config }}"
      block: |
          ModPagespeedRewriteLevel PassThrough
          ModPagespeedXHeaderValue "Powered By pagespeed"
          ModPagespeedCriticalImagesBeaconEnabled on
          ModPagespeedDisableFilters add_instrumentation
          # Necessary for background fetch rate-limiting
          ModPagespeedStatistics on
          ModPagespeedStatisticsLogging off
          ModPagespeedMessageBufferSize 0
          {% if pagespeed_direct_purge %}
          #  Permit purging cache
          # curl -X PURGE 'https://mydomain.com/*'
          ModPagespeedPurgeMethod PURGE
          ModPagespeedEnableCachePurge on
          {% endif %}
          # Future deprecation
          ModPagespeedPreserveUrlRelativity on
          # Interrupts cache control headers set by WP for example
          # @XXX May remove if problematic
          ModPagespeedModifyCachingHeaders off
          <IfDefine BROTLI>
          ModPagespeedHttpCacheCompressionLevel 0
          </IfDefine>
          {% if pagespeed_offline_cleanup %}
          ModPagespeedFileCacheCleanIntervalMs -1
          {% endif %}
      marker: '#{mark} APNSCP MANAGED BLOCK'
      insertbefore: "</[Ii]f[mM]odule>"
    when: st.stat.exists
    notify: Reload apache
  - name: Update tmpfiles configuration
    include_role: name=software/tmpfiles
    vars:
      src_files: "../../apache/modpagespeed/templates/modpagespeed.conf.j2"
  when: pagespeed_enabled
- name: Record pagespeed presence
  include_role: name="apnscp/bootstrap" tasks_from="set-config.yml"
  vars:
    section: pagespeed
    option: "enabled"
    value: "{{ pagespeed_enabled | bool }}"
