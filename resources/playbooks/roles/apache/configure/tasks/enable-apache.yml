- include_tasks: configure-fcgid.yml
- include_tasks: apnscp/bootstrap/tasks/register-vhost-ips.yml
  when: not vhost_addresses is defined

# Setting a hostname through Vultr's panel sets 127.0.0.1 <hostname>
# instead of its IP
- name: "Add 127.0.0.1 to address list"
  set_fact:
    # '' for join to work below
    addr_list: "{{ vhost_addresses + ['127.0.0.1', ''] }}"
  when: '"127.0.0.1" not in addr_list'
  # Fix when DO sets hostname to ::1, 127.0.0.1
- name: "Add ::1 to address list"
  set_fact:
    # '' for join to work below
    addr_list: "{{ (addr_list | reject('match', '^\\s*$') | list) + ['[::1]', ''] }}"
  when:
    - '"[::1]" not in addr_list'
    - '"ipv6" in ansible_lo'
    - (ansible_lo.ipv6 | length) > 0
- name: "Update ServerName in httpd-custom.conf"
  replace:
    path: /etc/httpd/conf/httpd-custom.conf
    regexp: '\bServerName\s+\S+'
    replace: "ServerName {{ ansible_nodename }}"
  register: r
- name: "Set ServerName in httpd-custom.conf"
  lineinfile:
    path: /etc/httpd/conf/httpd-custom.conf
    regexp: '\bServerName\b'
    line: "ServerName {{ ansible_nodename }}"
    insertbefore: BOF
    state: present
  when: not r is changed
- name: "Update VirtualHost container settings"
  replace:
    path: /etc/httpd/conf/httpd-custom.conf
    regexp: '<VirtualHost.*(:[^> ]+).*$'
    replace: '<VirtualHost {{ addr_list | join("\1 ") }}>'
    validate: 'echo %s && httpd -t'
  notify: Restart apache
- name: Correct CACHE_ROOT in htcacheclean
  ini_file:
    path: /etc/sysconfig/htcacheclean
    no_extra_spaces: yes
    section: null
    option: "{{ item.key }}"
    value: "{{ item.value }}"
  with_dict:
    CACHE_ROOT: /var/cache/httpd/cache-root
    OPTIONS: "{{ htcacheclean_options }}"
  register: c
- name: Add rlimit resources
  include_role: name=systemd/override-config
  vars:
    service: httpd
    config:
      - group: Service
        vars:
          MemoryAccounting: "true"
          MemoryLimit: "{{ (ansible_distribution_major_version == '7') | ternary(apache_memory_limit, None) }}"
          MemoryMax: "{{ (ansible_distribution_major_version != '7') | ternary(apache_memory_limit, None) }}"
          Delegate: "yes"
- name: "{{ c.changed | ternary('Restart', 'Start') }} htcacheclean"
  systemd:
    name: htcacheclean
    enabled: yes
    state: "{{ c.changed | ternary('restarted', 'started') }}"
