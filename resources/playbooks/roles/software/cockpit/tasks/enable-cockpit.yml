- name: Install cockpit RPM
  dnf:
    name: "{{ cockpit_packages.cockpit }}"
    state: present

- name: Move Cockpit to local socket
  include_role: name=systemd/override-config
  vars:
    service: cockpit.socket
    config:
      - group: Socket
        vars:
          # @XXX always reports changed
          ListenStream: |-

            ListenStream=/run/cockpit.sock
          SocketMode: "0660"
          SocketUser: cockpit-ws
          SocketGroup: "{{ apnscp_system_user }}"
    notify:
      - Reload systemd
      - Restart cockpit.socket

- name: Enable Cockpit
  systemd: name=cockpit.socket enabled=True state=started

- name: Remove sssd Cockpit configuration
  file:
    path: "{{ cockpit_sso_configuration}}"
    state: absent
  notify: Restart sssd
  when: not cockpit_sso

- name: "{{ cockpit_sso | ternary('Enable', 'Disable') }} SSO for panel admin"
  user:
    name: "{{ cockpit_sso_user }}"
    group: wheel
    local: True
    create_home: True
    password: "!"
    system: true
    state: "{{ cockpit_sso | ternary('present', 'absent') }}"

- name: "{{ cockpit_sso | ternary('Install', 'Remove') }} sssd RPM"
  dnf:
    name: "{{ cockpit_packages.sso }}"
    state: "{{ cockpit_sso | ternary('present', 'absent') }}"

- name: Enable SSSD
  systemd: name=sssd enabled=True state=started
  when: cockpit_sso

- name: Configure sssd
  ini_file:
    path: "{{ cockpit_sso_configuration }}"
    section: "{{ item.section }}"
    option: "{{ item.option }}"
    value: "{{ item.value }}"
    owner: root
    group: root
    mode: 0600
    state: "{{ (item.value != None) | ternary('present','absent') }}"
  with_items: "{{ cockpit_sso_vars }}"
  loop_control:
    label: "Setting [{{ item.section }}] {{item.option}} => {{ item.value }}"
  when: cockpit_sso
  notify: Restart sssd

- name: Add PAM SSSD rule to Cockpit
  lineinfile:
    path: /etc/pam.d/cockpit
    line: "auth            sufficient      pam_sss.so use_first_pass"
    regexp: '^\s*auth\s+sufficient\s+pam_sss\.so\s*'
    insertafter: ^\s*auth\s+substack\s+
    state: "{{ cockpit_sso | ternary('present', 'absent') }}"

- name: Configure Cockpit
  ini_file:
    path: "{{ cockpit_configuration }}"
    section: "{{ item.section }}"
    option: "{{ item.option }}"
    value: "{{ item.value }}"
    state: "{{ (item.value != None) | ternary('present','absent') }}"
  with_items: "{{ cockpit_vars }}"
  loop_control:
    label: "Setting [{{ item.section }}] {{item.option}} => {{ item.value }}"
  notify: Reload cockpit

- block:
  - name: "Check if X.509 necessary"
    stat:
      path: "{{ cockpit_sso_ca_certificate }}"
    register: st

  - name: Generate SSSD CA
    include_role: name=common tasks_from=create-self-signed-certificate.yml
    vars:
      pkey_format: pkcs1
      crt: "{{ cockpit_sso_ca_certificate }}"
      system_hostname: Local SSSD CA
      key_usage:
        - keyCertSign
      key_usage_critical: true
      basic_constraints: CA:TRUE
      notify: Restart sssd
    when: not st.stat.exists or force | default(False)
  - name: "Check if X.509 client auth necessary"
    stat:
      path: "{{ cockpit_sso_client_certificate }}"
    register: st

  - name: "Generate X.509 client certificate"
    include_role: name=common tasks_from=create-self-signed-certificate.yml
    vars:
      pkey_format: pkcs1
      crt: "{{ cockpit_sso_client_certificate }}"
      system_hostname: "cockpit-user"
      extended_key_usage:
        - clientAuth
      ca_path: "{{ cockpit_sso_ca_certificate }}"
      crt_provider: ownca
      notify: Restart apnscp
    when: not st.stat.exists or force | default(False)
  when: cockpit_sso
