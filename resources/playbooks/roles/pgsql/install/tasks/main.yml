---
- include_role: name=packages/install tasks_from=configure-default-vars.yml
- name: Install psycopg module
  yum:
    name: "python{{ ansible_python_version | regex_replace('\\..+$','')}}-psycopg2 < 2.9.9"
    state: present
- name: "{{ pgsql_has_postgis | ternary('Install', 'Remove') }} PostGIS extension"
  yum:
    name: "{{ postgis_package }}"
    state: "{{ pgsql_has_postgis | ternary('present', 'absent') }}"
  environment:
    LANGUAGE: en_US
- name: Record PostGIS version
  include_role: name=common tasks_from=record-runtime-setting.yml
  vars:
    setting: postgis_version
  when: pgsql_has_postgis
- name: Symlink include/pgsql
  file: src=/usr/pgsql-{{ pgversion }}/include  dest=/usr/include/pgsql state=link
- name: alias pgsql
  file:
    src: /usr/lib/systemd/system/postgresql-{{pgversion}}.service
    dest: /etc/systemd/system/postgresql.service
    state: link
    force: yes
- name: Override postgresql.service
  include_role: name=systemd/override-config
  vars:
    service: "postgresql-{{pgversion}}"
    config:
      - group: Service
        vars:
          RuntimeDirectory: "postgresql"
    notify: Restart postgresql
- stat: path="/var/lib/pgsql/{{pgversion}}/data/base"
  register: ddir
- name: Initialize PostgreSQL data dictionary
  command: "{{ pg_init_bin }}"
  args:
    creates: "{{ pgsql_config_file }}"
  when: not ddir.stat.exists
- stat: path="/var/lib/pgsql/{{pgversion}}/data/pg_hba.conf"
  register: pghba
- block:
  - name: copy pg_hba.conf
    copy:
      src: templates/pg_hba.conf
      dest: /var/lib/pgsql/{{pgversion}}/data/pg_hba.conf
  - systemd: name=postgresql state=restarted
  when: not ddir.stat.exists or not pghba.stat.exists

- set_fact:
    password: "{{ lookup('password', '/dev/null chars=ascii_letters length=15') }}"
  no_log: True
- name: "Override postgresql-{{pgversion}} service"
  include_role: name=systemd/override-config
  vars:
    service: "postgresql-{{pgversion}}"
    config:
      - group: Service
        vars:
          # Disable group read/write
          Environment: "UMASK=0077"
          TimeoutStopSec: "{{ pgsql_shutdown_timeout }}"

- name: Enable PostgreSQL
  systemd:
    daemon_reload: yes
    enabled: yes
    state: started
    name: postgresql-{{pgversion}}

- set_fact: root_valid=false
- block:
  - name: Validate if root valid
    postgresql_user:
      name: root
      login_user: root
      db: template1
  - name: Update "root valid" marker variable
    set_fact: root_valid=true
  rescue:
    - name: Temporarily open up permissions on {{ apnscp_root }} for Mitogen/sudo bug
      file:
        path: "{{ apnscp_root }}"
        mode: "o+x"

- name: Enable root PostgreSQL user
  postgresql_user:
    name: root
    role_attr_flags: SUPERUSER
    no_password_changes: yes
    password: "{{password}}"
    db: template1
    encrypted: yes
  register: user_changed
  become_user: postgres
  become: True
  no_log: True
  when: not root_valid | bool
- name: Lock {{ apnscp_root }} down
  file:
    path: "{{ apnscp_root }}"
    mode: "o-x"
  when: not root_valid | bool
- name: "Save PostgreSQL password"
  lineinfile:
    path: "{{ passwdfile }}"
    regexp: '^([^:]+:){3}root:'
    create: yes
    line: localhost:*:*:root:{{password}}
    mode: 0600
  when: user_changed.changed
  no_log: True
- name: "Set PostgreSQL listen addresses"
  ini_file:
    path: "{{ pgsql_config_file }}"
    section: null
    option: listen_addresses
    value: "'{{ pgsql_bind_address }}'"
  notify:
    - Restart postgresql
- name: Install TimescaleDB
  include_tasks: timescaledb.yml

# Timescale guarantees shared_preload_libraries will be set
- name: "{{ ((pgsql_auth_delay | int) > 0) | ternary('Enable', 'Disable') }} auth delay"
  include_tasks: manage-library.yml
  vars:
    library: auth_delay
    state: "{{ ((pgsql_auth_delay | int) > 0) | bool | ternary('present', 'absent') }}"
- name: "Set pgsql_auth_delay"
  ini_file:
    path: "{{ pgsql_config_file }}"
    section: null
    option: "auth_delay.milliseconds"
    value: "{{ pgsql_auth_delay | int }}"
    state: "{{ ((pgsql_auth_delay | int) > 0) | bool | ternary('present', 'absent') }}"
  notify:
    - Restart postgresql

- name: Create FST socket directory
  file:
    path: "{{ pgsql_fst_path }}"
    owner: postgres
    group: root
    mode: 0711
    state: directory

- include_role: name=common tasks_from=create-self-signed-certificate.yml
  vars:
    crt: "{{ pgsql_ssl_pem }}"
    owner: postgres
    mode: '0600'
    notify: Restart postgresql

- name: Merge custom config
  include_tasks: "{{ playbook_dir }}/roles/common/tasks/implicitly-import-overrides.yml"
  vars:
    base: "{{ pgsql_config }}"
    varname: pgsql_config
    prefix: pgsql
    name: ""

- name: Alter postgresql.conf settings
  ini_file:
    path: "{{ pgsql_config_file }}"
    section: null
    option: "{{ item.key }}"
    value: "{{ item.value }}"
  loop_control:
    label: "Setting {{ item.key }} => {{ item.value }}"
  with_dict: "{{ pgsql_config }}"
  notify: Restart postgresql

- name: Check if pg_log_authfail exists
  stat:
    path: "/usr/pgsql-{{ pgversion }}/lib/pg_log_authfail.so"
  register: authstat
- name: "Build pg_log_authfail"
  make:
    chdir: "{{ role_path }}/files/pg_log_authfail"
    target: "{{ item | default(omit, true) }}"
    params:
      CLANG: "/bin/true"
      PG_CONFIG: "/usr/pgsql-{{pgversion}}/bin/pg_config"
      with_llvm: no
  with_items:
    - ""
    - "install"
    - "clean"
  when:
    - not authstat.stat.exists
    - (pgsql_remote_connections | bool)
  # Ignore llvm errors
  ignore_errors: true

- name: "{{ (pgsql_remote_connections | bool) | ternary('Enable', 'Disable') }} pg_log_authfail"
  include_tasks: manage-library.yml
  vars:
    library: pg_log_authfail
    state: "{{ (pgsql_remote_connections | bool) | ternary('present', 'absent') }}"

- name: Update pg_log_authfail config
  ini_file:
    path: "{{ pgsql_config_file }}"
    section: null
    option: "{{ item.key }}"
    value: "{{ item.value }}"
    state: "{{ (pgsql_remote_connections | bool) | ternary('present', 'absent') }}"
  with_dict: "{{ pg_authlog_settings }}"
  notify:
    - Restart postgresql

- name: Link PostgreSQL log directory to /var/log
  file:
    path: /var/log/pgsql
    state: link
    src: /var/lib/pgsql/{{ pgversion }}/data/log

- name: Record PostgreSQL version
  include_role: name=common tasks_from=record-runtime-setting.yml
  vars:
    setting: pgsql_version
