#######################
# apnscp Bootstrapper #
#######################
#
# A utility for apnscp to provision a CentOS/RHEL 7.4+ platform
# for use with apnscp
#
# Part of apnscp core playbooks
# https://github.com/apisnetworks/apnscp-playbooks
#
---
- hosts: localhost
  module_defaults:
    yum:
      lock_timeout: "{{ yum_lock_max_wait }}"
    mysql_user:
      login_unix_socket: /var/lib/mysql/mysql.sock
    mysql_db:
      # Packet out of order error on CentOS 8
      login_unix_socket: /var/lib/mysql/mysql.sock
  become: yes
  connection: local
  gather_facts: yes
  vars_files:
    - 'apnscp-vars.yml'
    - 'roles/common/vars/apnscp-internals.yml'
    - ['{{ apnscp_user_defaults }}', '/dev/null']
    - ['{{ apnscp_last_run_vars }}', '/dev/null']
  roles:
    # Hook: before running playbooks
    - common/acquire-lock
    - custom/preflight
    - check-requirements
    - common/update-config
    - packages/configure-rhel
    - filesystem/make-mounts
    - system/kernel
    - system/selinux
    - packages/epel
    - systemd/mask-services
    - packages/install
    - system/compiler
    - apnscp/testing
    - pgsql/install
    - mysql/install
    - system/rsyslog
    - php/browscap
    - php/install
    - role: php/composer
      when: php_enabled
    - php/create-configuration
    - java/tomcat
    - apnscp/install-services
    - apnscp/bootstrap
    - apnscp/build-php
    - apnscp/install-extensions
    - apnscp/install-vendor-library
    - software/powerdns
    - apnscp/initialize-db
    # Checkpoint: admin should work fine at this point
    - apnscp/create-admin
    - apnscp/assert-admin-works
    # Hook: apnscp is minimally viable
    - custom/bootstrapped
    # Onto provisioning a test account
    - network/hostname
    - apache/configure
    - apache/fcgid
    - apache/modpagespeed
    - apnscp/link-bins
    - apnscp/initialize-filesystem-template
    # Prevent systemd picking up groupadd in cron
    - system/nss
    - php/multiphp
    - apnscp/php-filesystem-template
    - php/install-extensions
    # Role alias to php/install-extensions
    - role: php/install-pecl-module
      when: False
    - apnscp/service-template
    - apnscp/storage-log
    - apnscp/admin-helper
    - apnscp/dev
    - system/nscd
    - system/sssd
    - system/pam
    - system/limits
    - system/cgroup
    - system/sysctl
    - system/tuned
    - system/sshd
    - vsftpd/configure
    - network/setup-firewall
    - fail2ban/configure-jails
    - fail2ban/whitelist-self
    - mail/configure-dovecot
    - mail/configure-postfix
    - software/haproxy
    - mail/configure-courier-authlib
    - mail/maildir
    - mail/spamassassin
    - mail/rspamd
    - apnscp/register-ssl
    - network/optimizations
    # Odds and ends
    - apnscp/crons
    - system/yum
    - mysql/phpmyadmin
    - pgsql/phppgadmin
    - mail/webmail-squirrelmail
    - mail/webmail-roundcube
    - mail/webmail-horde
    - software/chrony
    - software/nvm
    - software/pyenv
    - software/rbenv
    - software/goenv
    - software/git
    - software/scl
    - software/imagick
    - software/passenger
    - software/etckeeper
    - software/tmpfiles
    - software/watchdog
    - software/cockpit
    - system/logs
    - clamav/setup
    - apache/modsecurity
    - system/misc-logrotate
    - software/argos
    - apnscp/bandwidth-log
    - apnscp/filesystem-checks
    # Hook: before creating account (AddDomain)
    # Perform additional unit tests
    - custom/validate-account
    # Last checkpoint, validate a mock account
    - apnscp/assert-account-works
    - apnscp/notify-installed
    ####################################
    # Add post-provisioning roles here #
    ####################################
    - custom/installed

    #
    # Thanks for playing!
    #
    # (c) 2018 Apis Networks
