@if (!$job->hasErrors())
    @component('email.indicator', ['status' => 'success'])
        Your application has installed!
    @endcomponent
    @else
    @component('email.indicator', ['status' => 'error'])
        Your application did not install!
    @endcomponent
@endif

@component('mail::message')

{{-- Body --}}
# Howdy!

Here are the results from your request to install {{ $job->getInstance()->getName() }} under **{{ $job->getInstance()->getHostname() }}**:

@foreach ($job->getLog(\Error_Reporter::E_ERROR|\Error_Reporter::E_WARNING) as $log)
- **{!! \Error_Reporter::errno2str($log['severity']) !!}:** {!!  $job->escapeMarkdown(substr(str_replace(["\r\n", "\n", "\r"], ' ', $log['message']), 0, 1024)) !!}
@endforeach

@include('email.webapps.common-footer')
@endcomponent