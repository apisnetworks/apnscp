@extends("email.auth.auth-common")
@section("title", $subject)
@section("notice")
    @parent
    Hello,
    <br/><br/>
    <p>
		Recently a user logged in to your <a href="{{ $url }}">control panel</a> from
		an unrecognized browser.
		<br /><br />
		@isset($Page, $msg)
		{{ $Page->var($msg) }}
		@endisset
    </p>
    <br/><br/>
    <h5>Login Information</h5>
    <p>
        <b>Username</b>: {{ $username }} <br/>
        <b>Domain</b>: {{ $domain }} <br/><br/>
    </p>
@endsection
