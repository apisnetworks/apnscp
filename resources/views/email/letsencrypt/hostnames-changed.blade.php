@component('mail::message')
The following hostnames were dropped to satisfy SSL renewal for {{ $domain }} on {{ SERVER_NAME }}:

- {{ implode("\n- ", $pruned)  }}

---

If this was in error, then you may reattach these hostnames through {{ PANEL_BRAND }} by visiting {{  \Auth_Redirect::getPreferredUri() }},
then re-adding the hostnames via **Web** > **SSL Certificates**.

This action was taken because *strict mode* is disabled. To enable, go to **Account** > **Settings** > **SSL**, then enable
**Strict Mode**. When enabled, a failure of any hostname will deny renewal of all hostnames.

A log has been attached with additional information from the request.
@endcomponent