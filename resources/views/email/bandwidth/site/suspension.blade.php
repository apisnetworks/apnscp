@php
	/** @var \Opcenter\Bandwidth\Overage $overage */
@endphp

@component('email.indicator', ['status' => 'warning'])
	Your attention is required.
@endcomponent

@component('mail::message')
Hello,

The following domain has exceeded its bandwidth limit for the period beginning **{{ date('Y-m-d', $overage->getBegin()) }}**
and ending **{{ date('Y-m-d', $overage->getEnd()) }}**.

Your site has consumed {{ sprintf('%.2f', Formatter::changeBytes($overage->getTotal(), 'GB', 'B')) }} GB total. Your account limit
is {{ Formatter::changeBytes($overage->getThreshold(), 'GB', 'B') }} GB. This is {{ sprintf('%.2f%%', $overage->getTotal()/$overage->getThreshold()) }}
of your bandwidth. @if (BANDWIDTH_STOPGAP) Your website will be suspended automatically if it exceeds {{ sprintf("%d%%", BANDWIDTH_STOPGAP) }} of its
bandwidth allowance before {{ date('Y-m-d', $overage->getEnd()) }}. @endif

For detailed information, see **Reports** > **Bandwidth Breakdown** within the [control panel]({{ \Auth_Redirect::getPreferredUri() }}).
Examining individual log entries in `{{ \Opcenter\Http\Apache::logPath() }}/access_log` may be of use as well.

@if ($overage->triggersSuspension())
Your site has been suspended automatically to prevent further abuse of service. The suspension
will automatically end in {{ (new DateTime())->setTimestamp($overage->getEnd())->diff(new DateTime())->days+1 }} days
when the new bandwidth cycle begins.
@endif

@endcomponent