@if ($stats->get("success"))
@component('email.indicator', ['status' => 'success'])
	Bootstrapper completed
@endcomponent
@else
@component('email.indicator', ['status' => 'error'])
	Bootstrapper failed
@endcomponent
@endif
@component('mail::message')
# Hello,

Your recent Bootstrapper request finished.

---
**Server:** {{ SERVER_NAME }} ({{ \Opcenter\Net\IpCommon::my_ip() }})<br />
**Duration:** {{ floor(($stats->get("end") - $stats->get("begin"))/60) }} minutes {{ (floor($stats->get("end") - $stats->get("begin")) % 60) }} seconds<br />
**Items checked:** {{ $stats->get("ok") + $stats->get("changed") }}<br />
**Items changed:** {{ $stats->get("changed") }}<br />

**Tags processed**:
@foreach ($job->tags() as $tag)
- {{ $tag }}
@endforeach

---
@endcomponent

