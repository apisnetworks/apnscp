@component('mail::message')
# Hello,

It's your control panel, {{ PANEL_BRAND }}. Everything is setup on this server!
You can access the panel using the following information.

---

**SERVER ADDRESS**: {{ $ip }}<br />
**HOSTNAME**: {{ $hostname }}<br />
**URL**: [{{ $secure_link }}]({{ $secure_link }})

## Admin Credentials
**LOGIN**: `{{ $admin_user }}`<br />
**PASSWORD**: `{{ $admin_password }}`

@component('mail::button', ['url' => $secure_link])
	Access {{ PANEL_BRAND }}
@endcomponent

---

You should change your password as soon as possible in the panel through
**Account** > **Settings** or from the command-line:

`sudo {{ $apnscp_root }}/bin/cmd auth_change_password NEWPASSWORD`

Thanks for making {{ PANEL_BRAND }} a part of your hosting experience!

\- Matt Saladna<br />
Owner + Lead Developer

[Community Chat](https://discord.gg/wDBTz6V) · [Documentation](https://docs.apiscp.com) · [Blog](https://hq.apiscp.com)

@endcomponent

