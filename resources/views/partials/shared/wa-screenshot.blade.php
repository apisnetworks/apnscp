<div class="col-12 item hostname screenshot mb-2"
     data-hostname="{{ $pane->getHostname() }}" data-type="{{ $pane->getApplicationType() }}"
     data-docroot="{{ $pane->getDocumentRoot() }}" @if ($pane->getPath()) data-subsite @endif>
	<div class="card">
		<div class="flex-row align-content-around d-flex banner  py-1 px-1 card-title mb-0 bg-light">
			<h4 class="title align-self-center mb-0 app-title text-left position-absolute @if ($pane->isSubdomain()) subdomain @else domain @endif">
				@if ($pane->getMeta('options.affixed'))
					<i class="fa fa-thumb-tack" class="stick-item" title="Site is pinned"
					   data-target="{{ $pane->getLocation() }}"></i>
				@endif
				{{ $pane->getLocation() }}
			</h4>
			@isset($actions)
				<div class="ml-auto">
					@include($actions['view'], array_except($actions, 'view'))
				</div>
			@else
				&nbsp;
			@endisset
		</div>
		<div style="@if ($pane->hasScreenshot()) background-image:url({{ $pane->getScreenshot() }}); @endif"
		     class="outer card-body position-absolute w-100 h-100 @if ($pane->pendingScreenshot()) pending @endif">
			@if ($pane->pendingScreenshot())
				<div class="pending-label">
					{{ str_repeat(strtoupper(_("Screenshot pending ")), 50) }}
				</div>
			@endif
		</div>
		<div style="background-image:url({{ $pane->getScreenshot() }});"
		     class="inner card-body d-flex align-items-end position-relative">
			<div class="position-absolute w-100 h-100 banner align-items-center d-none">
				@isset($href)
					<a href="{{ $href }}" class="d-flex text-white h-100 w-100 align-items-center btn-lg btn btn-block">
							<span class="align-content-center m-auto @empty($hoverText) ui-action-select @endempty ui-action  ui-action-label">
								{{ $hoverText ?? _('Select') }}
							</span>
					</a>
				@endisset
			</div>
			<div class="d-flex mt-auto w-100 flex-row align-self-end">
				<div class="mt-auto app-meta banner align-content-between w-100 py-0 px-1 d-flex flex-row align-content-around card-footer">
					<div class="align-self-start mr-1">
						@include('master::partials.shared.wa-meta-gutter')
					</div>
					<div class="ml-auto attributes d-none d-lg-inline-flex align-items-center">
							<span class="docroot ml-3 d-none attribute">
								<i class="fa fa-folder"></i>
								{{ $pane->getDocumentRoot() }}
							</span>

						<span class="type ml-3 d-none attribute">
								<i class="fa fa-question"></i>
								{{ $pane->getApplicationType() }}
							</span>

						<span class="hostname ml-3 d-none attribute">
								<i class="fa fa-globe"></i>
								{{ $pane->getHostname()  }}
							</span>
					</div>

					@if ($pane->hasFortification())
						<div class="fortification badge-light font-weight-bold ml-auto text-uppercase">{{ $pane->getOption('fortify') }}</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<!--

</div>-->