@php
	/** @var \Template\Searchlet $template */
@endphp
@if (null !== $filter->formAction())<form method="get" action="{{ $filter->formAction() }}">@endif
	<div class="form-group form-inline {{ $filter->filterClass() }} align-items-end d-block d-sm-inline-flex">
		@if (\count($filter->specs()) > 1)
		<div class="flex-column mr-0 mr-sm-3 mb-2">
			<label class="ui-action ui-action-label ui-filter justify-content-start">
				{{ _("Filter By") }}
			</label>
			<select class="form-control custom-select" name="searchby" id="filter_spec">
				@foreach ($filter->specs() as $name => $label)
					<option value="{{ $name }}" {{  HTML_Kit::print_selected('searchby', $name) }}>{{ _($label) }}</option>
				@endforeach
			</select>
		</div>
		@endif

		@if ($operations = $filter->operations())
			<div class="flex-column mr-0 mr-sm-3 mb-2">
				<label class=" justify-content-start">
				</label>
				<select class="form-control custom-select" name="searchop" id="searchop">
					@foreach ($operations as $opSymbol => $opLabel)
						<option value="{{ $opSymbol }}" {{\HTML_Kit::print_selected('searchop', $opSymbol)}}>{{ _($opLabel) }}</option>
					@endforeach
				</select>
			</div>
		@endif

		<div class="flex-column mr-0 mr-sm-3 mb-2">
			<label class="justify-content-start">Filter Term</label>
			<input type="text" class="form-control" @if ($filter->autofocus()) autofocus @endif name="searchtext" size="20" id="filter"
			       value="{{ $filter->searchText() }}"/>
		</div>

		<button type="{{ $filter->submitType() }}" name="{{ $filter->submitName() }}" id="{{ $filter->submitId() }}"
		        class="form-control btn btn-secondary mr-0 mr-sm-3 mb-2">
			<i class="fa fa-search"></i>
			{{ _($filter->filterLabel()) }}
		</button>
		<button type="reset" name="reset_filter" id="reset_filter"
		        class="warn ui-quiet-warn btn btn-secondary mr-3 mb-2 hide">
			{{ _($filter->resetFilterLabel()) }}
		</button>

		<ul class="ui-active-filters d-flex align-self-center mt-4 list-unstyled list-inline mb-2"></ul>

	</div>
@if ($filter->formAction())</form>@endif