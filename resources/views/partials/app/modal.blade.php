<div class="modal fade" id="{{ $id ?? 'modal' }}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<form method="POST" action="{{ $action ?? HTML_KIT::page_url_params() }}">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					@unless ($header ?? false)
						<h4 class="modal-title" id="modalLabel">
							{{ $title ?? PANEL_BRAND }}
						</h4>
					@endunless
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>

				</div>
				<div class="modal-body">
					{{ $slot ?? '' }}
				</div>
				<div class="modal-footer">
					{{ $buttons ?? '' }}
					<button type="button" class="btn btn-link ui-warn ui-close-dialog" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</form>
</div>