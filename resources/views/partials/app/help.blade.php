<div id="ui-help-container" class="row">
	<h2 id="ui-overview" class="col-12 ui-overview title mb-0 d-none d-md-block ui-menu-category ui-menu-category-{{ strtolower(\Template_Engine::init()->getActiveCategoryName())  }}">
		{{ $Page->getApplicationTitle() }}
	</h2>
	<div class="col-12">
		@if (Page_Renderer::do_help())
			@php
				$help = (string)$Page->getHelp();
				$blurbmore = false;

				if (isset($help[0])) {
					$blurbmore = 'ui-help-blurb-more';
				}
			@endphp
			<div id="ui-help-blurb" class="row">
				<div class="{{ $blurbmore }} float-left col-sm-12 ui-help-blurb mt-2" id="ui-help-tag">
					@php eval('?>' . $Page->getHelpTagline()); @endphp
				</div>
				@php
					if ($blurbmore) {
						$showcls = 'ui-collapsed';
						$showlabel = 'more';
						$helpcls = 'hide';
						if (Page_Renderer::do_full_help()) {
							$showcls = 'ui-expanded';
							$showlabel = 'less';
							$helpcls = 'show';
						}
					}
				@endphp
				@if ($blurbmore)
					<a href="#"
					   class="ui-overview-link fa col-sm-2 pr-0 ui-expandable text-right {{ $showcls }} hidden-xs-down"
					   id="ui-overview-link">
					</a>
					<div class="ui-help mt-1 @if ($showlabel == 'more') hide @endif hidden-xs-down col-sm-12"
					     id="ui-help">
						@php eval('?>' . $help); @endphp
					</div>
				@endif
			</div>
		</div>
	</div> <!--  end #ui-help-blurb -->
@endif