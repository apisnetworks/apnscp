@includeWhen(floor($storage['total']), 'theme::partials.shared.gauge', [
	'gaugeTitle' => _('Storage'),
	'gaugeClass' => $Page->getGaugeClass($storage, 'storage'),
	'gaugeId'    => 'ui-storage-cluster',
	'fill'       => $storage['used']/1024,
	'max'        => $storage['total']/1024
])

@includeWhen(UCard::get()->hasPrivilege('site') && \cmd('bandwidth_enabled'), 'theme::partials.shared.gauge', [
	'gaugeTitle' => _('Bandwidth'),
	'gaugeClass' => $Page->getGaugeClass($bw, 'bandwidth'),
	'gaugeId'    => 'ui-bandwidth-cluster',
	'fill'       => $bw['used'] ?? 0,
	'max'        => $bw['total'] ?? 0,
	'unit'       => 'GB'
])
<a class="ui-refresh" data-tooltip="refresh gauges" id="ui-gauge-refresh"></a>