@php
	$notifications = \count(\cmd('crm_get_notifications'));
@endphp
<div id="ui-notification-indicator" class="ui-indicator position-relative" class="dropdown" data-toggle="collapse"
     data-target="#ui-notifications" aria-expanded="false"
     aria-controls="ui-notifications">
	<i class="ui-action-notification ui-action ui-action-d-compact @if (!$notifications) inactive @else animated @endif"></i>
	<span class="count badge">{{ $notifications ?: '' }}</span>
</div>