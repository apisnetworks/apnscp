<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Schema;

	class ResellerSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    /* XXX */
	    Schema::connection('pgsql')->hasTable('reseller_info') &&
	    Schema::connection('pgsql')->rename('reseller_info', 'resellers');

	    Schema::connection('pgsql')->table('resellers', function (Blueprint $table) {
		    /* XXX */
		    if (Schema::connection('pgsql')->hasColumn('resellers', 'password')) {
			    $table->dropColumn('password');
			    $table->dropColumn('fullname');
		    }
		    DB::connection('pgsql')->getPdo()->exec("ALTER TABLE resellers ALTER enabled TYPE BOOLEAN USING enabled::boolean");
			$table->boolean('enabled')->nullable(false)->default(true)->change();
			DB::connection('pgsql')->getPdo()->exec("DROP SEQUENCE IF EXISTS resellers_reseller_id_seq CASCADE");
			DB::connection('pgsql')->getPdo()->exec("CREATE SEQUENCE resellers_reseller_id_seq CYCLE MAXVALUE " . \Opcenter\Reseller\Create::MAX_SITES);
		    DB::connection('pgsql')->getPdo()->exec("ALTER TABLE resellers ALTER COLUMN reseller_id SET DEFAULT nextval('resellers_reseller_id_seq')");

	    });

		Schema::connection('pgsql')->table('siteinfo', function (Blueprint $table) {
			if (Schema::connection('pgsql')->hasColumn('siteinfo', 'reseller_id')) {
				$table->dropColumn('reseller_id');
			}
		});

	    Schema::connection('pgsql')->table('siteinfo', function (Blueprint $table) {
			$table->integer('reseller_id', false, true)->nullable()->default(null);
		    $table->foreign('reseller_id')->references('reseller_id')->on('resellers')->onDelete('restrict');
	    });


	    /* XXX */
	    Schema::connection('pgsql')->dropIfExists('reseller_sites');

		Schema::connection('pgsql')->create('reseller_sites', function (Blueprint $table) {
			$table->integer('reseller_id', false, true);
			$table->integer('site_id', false, true);
			$table->foreign('site_id')->references('site_id')->on('siteinfo')->onDelete('cascade');
			$table->foreign('reseller_id')->references('reseller_id')->on('resellers')->onDelete('restrict');
		});



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}