<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Support\Facades\DB;

	class MetricUniqueConstraint extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			$chunker = (new \Daphnie\Chunker(DB::connection('pgsql')->getPdo()));
			try {
				$chunker->decompressRange(null);
				$chunker->disableArchivalCompression();
			} catch (\PDOException $e) {
				if (false !== strpos($e->getMessage(), 'Feature not supported:')) {
					// older, broken Timescale metadata
					return;
				}
			}
			if (!$chunker->hasCompressionArtifacts()) {
				DB::connection('pgsql')->getPdo()->exec('TRUNCATE TABLE metrics');
				DB::connection('pgsql')->getPdo()->exec('ALTER TABLE metrics DROP CONSTRAINT 
				IF EXISTS unique_metric_check');
				DB::connection('pgsql')->getPdo()->exec("ALTER TABLE metrics ADD CONSTRAINT
				unique_metric_check UNIQUE (attr_id, ts, site_id)");
			}
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			//
		}
	}
