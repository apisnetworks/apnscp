#!/bin/sh

# Block IPs with over 100 connections open to server
function find_spam {
        netstat -nptu | egrep 'TIME_W|SYN_' | awk '{print $5}' | sort -n | cut -d: -f1 | sort -n \
                | uniq -c | sort -n | tail -n 5 \
                | egrep '^[[:space:]]*[[:digit:]][[:digit:]][[:digit:]][[:digit:]]*'
}

# ban excessive apache connections
function ban_spam {
	MYIPS=($(ip -o addr | awk '!/^[0-9]*: ?link\/ether/ {gsub("/", " "); print $4}'))
        find_spam | awk '{print $2}' | while read IP ; do
		[[ "${MYIPS[@]}" =~ "${IP}" ]] && continue
		echo "Banning $IP"
                /sbin/iptables -A INPUT -s $IP  -j DROP
        done
}

ban_spam
