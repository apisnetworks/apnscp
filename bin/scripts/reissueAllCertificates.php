#!/usr/bin/env apnscp_php
<?php
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
 */

/**
 * Reissue all Let's Encrypt certificates for a given location.
 * Useful if the LE storage location changes in a later release.
 */

use Opcenter\Crypto\Letsencrypt as LEService;
use Opcenter\Crypto\Ssl\Certificate;

define('INCLUDE_PATH', dirname(__DIR__, 2));
include(INCLUDE_PATH . '/lib/CLI/cmd.php');
$old = \Error_Reporter::set_verbose();
\Error_Reporter::set_verbose(max($old, 2));
// initialize user, root
\cli\cmd();

	$optind = null;
	$longopts = [
		'force',
		'dry-run',
		'import',
		'help',
	];
	$opts = ['force' => false, 'dry-run' => false, 'import' => true];
	$optind = 0;
	foreach (getopt('', $longopts, $optind) as $opt => $val) {
		switch ($opt) {
			case 'help':
				usage();

			case 'dry-run':
				$opts['dry-run'] = true;
				break;
			case 'force':
				$opts['force'] = true;
				break;
			case 'no-import':
				$opts['import'] = false;
				break;
			case '--':
				break 2;
			default:
				fatal("Unknown option `%s'", $opt);
		}
	}

$dir = $_SERVER['argv'][$optind] ?? LEService::acmeSiteStorageDirectory('');

if (!is_dir($dir)) {
    fatal("`%s' is not a directory", $dir);
}

function usage() {
	echo "usage: " . basename($_SERVER['argv'][0]) . " (--force | --dry-run) [<PATH>]" . "\n" . "\n";
	echo " --force\t\t Perform renewal if certificate outside renewal window" . "\n" .
		" --no-import\t\t Disable automatic importation of missing certificates" . "\n" .
 		" --dry-run\t\t Show what would be renewed" . "\n";
	exit(1);
}

function tryRenewal($site, $certFile, $force = false): void {
	global $opts;

	$sslModule = new Ssl_Module();

	$x509 = \Opcenter\Crypto\Ssl::parse($raw = file_get_contents($certFile));
	if (!(new Letsencrypt_Module())->is_ca($raw)) {
		debug(\Module\Support\Letsencrypt::ERR_AUTHORITY_UNKNOWN,
			['fingerprint' => (new Certificate($raw))->authority() ?? 'INVALID']);

		return;
	}

	if (!\Opcenter\Crypto\Letsencrypt::expiring($x509) && !$force) {
		debug(\Module\Support\Letsencrypt::MSG_CERT_OUTSIDE_EXPIRY,
			[
				'name' => $site,
				'days' => \Opcenter\Crypto\Letsencrypt::daysUntilExpiry($x509),
				'min'  => LETSENCRYPT_LOOKBEHIND_DAYS,
				'max'  => LETSENCRYPT_LOOKAHEAD_DAYS
			]);
		debug("Run with --force to process renewal anyway");
		return;
	}

	$san = $sslModule->get_alternative_names($x509);

	if (!empty($opts['dry-run'])) {
		debug("DRY RUN: renew %s (%s)", $site, implode(', ', $san));
		return;
	}

	info("Renewing %s, with names: %s\n", $site, join(",", $san));
	$ctx = $site === \Opcenter\Crypto\Letsencrypt::SYSCERT_NAME ? \Auth::profile() : \Auth::context(null, $site);

	if (!$force && !\apnscpFunctionInterceptor::factory($ctx)->letsencrypt_renew()) {
		error("! Failed to renew `%s'", $site);
	} else if ($force) {
		$renewal = new class extends Letsencrypt_Module {
			public function __invoke($site, $x509)
			{
				return (new static)->_renew($site, $x509);
			}
		};
		$renewal($site, $x509);
	}

	// sleep a little to avoid triggering rate-limiter
	usleep(500000);
}

$dh = opendir($dir) or fatal("failed to open directory `%s'", $dir);
$seen = [];
while (false !== ($entry = readdir($dh))) {
	$site = basename($entry);
	$seen[] = $site;
	$certFile = "{$dir}/{$entry}/cert.pem";
	if (!file_exists($certFile)) {
	    continue;
    }
	tryRenewal($site, $certFile, $opts['force']);
}
closedir($dh);

debug("Scanning account filesystems");

foreach (array_diff(\Opcenter\Account\Enumerate::active(), $seen) as $site) {
	debug("Scanning $site");
	$filesystem = new CLI\MockFilesystem($site);
	$crtPath = $filesystem->root() . Ssl_Module::CRT_PATH . '/' . Ssl_Module::DEFAULT_CERTIFICATE_NAME . '.crt';
	$pkeyPath = $filesystem->root() . Ssl_Module::KEY_PATH . '/' . Ssl_Module::DEFAULT_CERTIFICATE_NAME . '.key';
	if (!file_exists($crtPath) || !file_exists($pkeyPath)) {
		debug("No SSL detected for $site");
		continue;
	}

	if (!(new Letsencrypt_Module())->is_ca($crt = file_get_contents($crtPath))) {
		debug(\Module\Support\Letsencrypt::ERR_AUTHORITY_UNKNOWN,
			['fingerprint' => (new Certificate($crt))->authority() ?? 'INVALID']);
		debug("Skipping %s due to unknown CA", $site);
		continue;
	}

	if ($opts['import']) {
		debug("Internal storage missing for %s. Importing", $site);
		$ctx = \Auth::context(null, $site);
		$afi = \apnscpFunctionInterceptor::factory($ctx);
		$crtConfig = $afi->ssl_get_certificates();
		if (empty($crtConfig)) {
			warn("No certificate configured for web server - ignoring");
			continue;
		}
		['crt' => $crt, 'chain' => $chain, 'key' => $key] = array_pop($crtConfig);
		LEService\AcmeDispatcher::instantiateContexted(\Auth::context(null, $site), [$site])->store(
			$afi->ssl_get_certificate($crt),
			$afi->ssl_get_certificate($chain),
			$afi->ssl_get_private_key($key)
		);
	} else {
		warn("Certificate for `%s' is missing from internal storage. Run with --import to include this certificate", $site);
	}
	tryRenewal($site, $crtPath, $opts['force']);
}
debug("Done scanning account filesystems");
exit((int)\Error_Reporter::is_error());