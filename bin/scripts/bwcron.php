#!/usr/bin/env apnscp_php
<?php declare(strict_types=1);

	use Carbon\Carbon;
	use Lararia\Mail\BandwidthDigest;
	use Lararia\Mail\BandwidthOverageNotification;
	use Lararia\Mail\BandwidthSuspensionNotification;
	use Opcenter\Account\State;
	use Opcenter\Bandwidth\Bulk;
	use Opcenter\Bandwidth\Overage;
	use Opcenter\Bandwidth\Site;

	error_reporting(E_ALL);
	define('INCLUDE_PATH', realpath(dirname(__FILE__, 3)));
	include INCLUDE_PATH . '/lib/CLI/cmd.php';

	/**
	 * Step 0: synchronize continuous aggregate
	 * Step 1a: create new rollovers
	 * Step 1b: for each new rollover unsuspend
	 * Step 1c: for each rollover window, zip bandwidth, for each prior rollover window zip even further
	 * Step 2: find sites over bandwidth,
	 */

	if (!Bulk::sync()) {
		fatal("Failed to synchronize aggregate table - aborting!");
	}

	$sites = Bulk::findRollovers();
	foreach ($sites as $siteId => $spans) {
		if (dangling($siteId)) {
			continue;
		}

		/** @var Site $bwHandler */
		$bwHandler = new Site($siteId);
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
		try {
			$rollovers = $bwHandler->rollovers();
			/**
			 * Loop will terminate after 1 iteration unless bwcron hasn't run for an extended
			 * period of time
			 */
			$begin = array_get(array_pop($rollovers), 'begin', null);
			do {
				$rolloverBoundary = $bwHandler->getCycleEnd();
				$rolloverEnd = $rolloverBoundary;
				$bwHandler->closeRollover($rolloverBoundary);
				$bwHandler->createRollover($rolloverBoundary);
				$begin = $rolloverBoundary;
				// further collapse historic entries
				$bwHandler->zip(
					// mutability...
					(new Carbon())->setTimestamp($rolloverBoundary)->subMonthNoOverflow(2)->getTimestamp(),
					(new Carbon())->setTimestamp($rolloverBoundary)->subMonthNoOverflow(1)->getTimestamp(),
					'1 hour',
					true // preserve bandwidth meta up to 1 year
				);

				$bwHandler->zip(
					(new Carbon())->setTimestamp($rolloverBoundary)->subMonthNoOverflow(13)->getTimestamp(),
					(new Carbon())->setTimestamp($rolloverBoundary)->subMonthNoOverflow(12)->getTimestamp(),
					'1 day'
				);
			} while ($bwHandler->requiresRollover());
		} catch (\apnscpException $e) {
			report("Failed rotation on site%d: %s\n%s", $siteId, $e->getMessage(), $e->getTraceAsString());
		} finally {
			\Error_Reporter::exception_upgrade($oldex);
		}
	}

	$bulkNotices = [];
	foreach (Bulk::findOverages() as $site) {
		$siteId = $site['site_id'];
		if (dangling($siteId)) {
			continue;
		}
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
		try {
			$overage = new Overage($site);
		} catch (\apnscpException $e) {
			warn("Failed to create auth object for site%d, continuing", $siteId);
			continue;
		} finally {
			\Error_Reporter::exception_upgrade($oldex);
		}
		$bwHandler = $overage->getHandler();
		$bulkNotices[] = $overage;
		if ($bwHandler->amnestied() || State::disabled("site{$siteId}")) {
			continue;
		}
		if ($overage->triggersSuspension()) {
			// suspended
			if (!$bwHandler->suspended()) {
				$bwHandler->suspend();
			}
			$noticeType = BandwidthSuspensionNotification::class;
		} else if ($overage->triggersNotice()) {
			// notice
			$noticeType = BandwidthOverageNotification::class;
		} else {
			// nothing
			continue;
		}
		sendNotice($overage->site_get_admin_email(), $noticeType, $overage);
	}
	if ($bulkNotices && ($email = \apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login()))->common_get_email())) {
		sendNotice($email, BandwidthDigest::class, $bulkNotices);
	}

	function dangling(int $siteId): bool
	{
		if (!\Auth::get_domain_from_site_id($siteId)) {
			return warn("Dangling site ID found - %d, skipping", $siteId);
		}

		return false;
	}

	/**
	 * Send overage notice to user
	 *
	 * @param string   $email
	 * @param string   $mailable
	 * @param mixed    $args mailable args
	 */
	function sendNotice(string $email, string $mailable, $args)
	{
		$email = preg_split('/\s*,\s*/', $email, PREG_SPLIT_NO_EMPTY);
		$mail = Illuminate\Support\Facades\Mail::to($email);
		return $mail->send(new $mailable($args));
	}


