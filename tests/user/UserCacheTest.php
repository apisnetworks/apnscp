<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class UserCacheTest extends TestFramework
	{

		public function testUserCache()
		{
			if (\Opcenter\Service\ServiceLayer::bugged()) {
				return $this->markTestSkipped('Kernel retains site identifier after deletion');
			}

			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$this->assertTrue($afi->user_add('bad-user-123', \Opcenter\Auth\Password::generate()));
			$siteId = $account->getContext()->site_id;
			$this->assertArrayHasKey('bad-user-123', $afi->user_get_users());
			$this->assertTrue($account->destroy(), 'Destroy account');
			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$this->assertSame($siteId, $account->getContext()->site_id, 'Site recreated on same site identifier');
			$this->assertArrayNotHasKey('bad-user-123', $afi->user_get_users(), 'User is ghosted');
		}
	}

