<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
 */

class TestHelpers {
	/**
	 * @var array lock accounts to prevent garbage collection
	 */
	protected static $accountLockup = [];
	protected static $userPairs = [];
	protected static $contextCache = [];

	/**
	 * Get configured admin username
	 *
	 * Ripped from cmd...
	 *
	 * @return string
	 */
	public static function getAdmin(): string
	{
		$uid = posix_getuid();
		$pwd = posix_getpwuid($uid);
		$file = \apnscpFunctionInterceptor::get_class_from_module('auth')::ADMIN_AUTH;
		return strtok(file_get_contents($file), ':') ?: $pwd['name'];
	}

	public static function create(string $domain = null, array $params = []): \Auth_Info_User
	{
		$domain ??= \Opcenter\Account\Ephemeral::random('domain');

		if (!isset(self::$contextCache[$domain])) {
			if (!\Auth::domain_exists($domain)) {
				$account = \Opcenter\Account\Ephemeral::create(
					array_replace_recursive($params,[
						'siteinfo' => ['domain' => $domain]
					])
				);
				self::$accountLockup[] = $account;
				$context = $account->getContext();
			} else {
				$context = \Auth::context(null, $domain);
			}
			self::$contextCache[$domain] = $context;
		}
		$context = self::$contextCache[$domain];
		if (!\apnscpSession::init()->exists($context->id)) {
			self::$contextCache[$domain] = null;
			\apnscpFunctionInterceptor::expire($context->id);
			return static::create($domain, $params);
		}
		return $context;
	}

	/**
	 * Return singleton account
	 *
	 * @param array $params service values, dimensional or dot-separated
	 * @return Auth_Info_User
	 */
	public static function with(array $params = []): \Auth_Info_User
	{
		if (!is_array(current($params))) {
			// passed as $query = ['siteinfo.foo' => 'bar', 'baz' => 'abc']
			// instead of $query = ['siteinfo' => ['foo' => 'bar']]
			$params = Util_Conf::hydrate($params);
		}

		$context = self::create(array_get(Definitions::get(), 'auth.site.domain'));
		$diffs = \Util_PHP::array_diff_assoc_recursive($params, $context->getAccount()->conf);
		if (!$diffs) {
			return $context;
		}

		\Util_Account_Editor::instantiateContexted($context)->setConfig($diffs)->edit();

		return $context;
	}

	/**
	 * Reset singleton account
	 *
	 * @return Auth_Info_User
	 */
	public static function reset(): \Auth_Info_User
	{
		$context = self::create(array_get(Definitions::get(), 'auth.site.domain'));
		$afi = \apnscpFunctionInterceptor::factory($context);
		$afi->site_wipe($afi->site_wipe());

		return $context;
	}

	/**
	 * Create a user for the account
	 *
	 * @param Auth_Info_Account $account
	 * @param null              $user
	 * @return Auth_Info_User
	 */
	public static function createUser(\Auth_Info_Account $account, $user = null): \Auth_Info_User
	{
		$context = static::create($account->conf['siteinfo']['domain']);
		$afi = \apnscpFunctionInterceptor::factory($context);
		if (!$user || !$afi->user_exists($user)) {
			$user = \Opcenter\Auth\Password::generate(16, 'a-z');
			if (!$afi->user_add($user, \Opcenter\Auth\Password::generate(16, 'a-z'), 'sample user')) {
				throw new RuntimeException("Failed to generate random user {$user} for " . $context->domain);
			}
		}
		return \Auth::context($user, $context->site);

	}
}