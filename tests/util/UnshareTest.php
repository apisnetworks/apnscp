<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	require_once dirname(__FILE__) . '/../TestFramework.php';

	class UnshareTest extends TestFramework
	{
		public function testMount() {
			$proc = new Util_Process();
			$unshare = new \Util\Process\Unshare();
			$file = '/' . \Opcenter\Auth\Password::generate(32);
			$content = md5($file);
			$unshare->add((new \Util\Process\Unshared\Resources\Mount($file, true))->fromContent('' . $content,APNSCP_SYSTEM_USER));
			$proc->setOption('unshare', $unshare);
			$proc->addCallback(function () use ($file, $content) {
				$this->assertFileExists($file);
				$this->assertStringNotContainsString($content, file_get_contents($file));
			}, 'exec');
			$ret = $proc->run("cat $file");
			$this->assertTrue($ret['success']);
			$this->assertStringContainsString($content, $ret['stdout']);
		}

		public function testPid() {
			$proc = new Util_Process();
			$unshare = new \Util\Process\Unshare();
			$unshare->add(
				new \Util\Process\Unshared\Resources\Pid()
			);
			$proc->setOption('unshare', $unshare);
			$ret = $proc->run('cat /proc/1/comm');
			$this->assertTrue($ret['success']);
			$this->assertSame('cat', trim($ret['stdout']));
		}
	}