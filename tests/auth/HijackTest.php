<?php

	use GuzzleHttp\Client;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';
	require_once(__DIR__ . '/../soap/SoapTestClient.php');

	class HijackTest extends TestFramework
	{
		public function testHijack()
		{
			\Auth::use_handler_by_name((new Auth_UI)->getDriver());
			$this->assertSame('UI', \Auth::get_driver()->getDriver());

			$adminCtx = \Auth::profile();
			$adminAfi = \apnscpFunctionInterceptor::factory($adminCtx);
			$this->assertTrue(\Auth::autoload()->session_valid(), 'Session valid');
			$this->assertNotEmpty($_SESSION);
			$ctx = TestHelpers::with();

			$this->assertNotEmpty($_SESSION['valid']);
			$this->assertNotEmpty($hijackSession = $adminAfi->admin_hijack($ctx->getAccount()->site),
				'Hijack ID generated');
			$adminAfi = \apnscpFunctionInterceptor::init();

			$this->assertNotEmpty($_SESSION['valid']);

			$overrodeContext = \Auth::profile();
			$this->assertSame($ctx->username, $adminAfi->common_whoami(), 'Hijack contexted into user');
			$this->assertSame($overrodeContext->username, $ctx->username);


			$this->assertTrue(\Auth::autoload()->session_valid(), 'Hijack session valid');
			$this->assertTrue(\Auth::authenticated());

			$this->assertNotEmpty($_SESSION['valid']);
			$auth = \Auth::autoload()->resetID($adminCtx->id);
			$this->assertNotEmpty($_SESSION['valid']);
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($auth->getProfile()));
			$auth->authInfo(true);
			$this->assertNotEmpty($_SESSION);
			$this->assertNotEmpty($_SESSION['valid']);

			\Auth::autoload()->endImpersonation($overrodeContext);

			$adminAfi = \apnscpFunctionInterceptor::factory($adminCtx);
			$this->assertNotEmpty($_SESSION['valid']);
			\Auth::autoload()->setID($adminCtx->id);
			$this->assertNotEmpty($_SESSION);
			$this->assertNotEmpty($_SESSION['valid']);
			$this->assertSame($adminCtx->username, $adminAfi->common_whoami(), 'Hijack contexted into user');
			$this->assertTrue(\Auth::autoload()->session_valid(), 'Hijack session valid');
		}

		public function testSSOInstantiation()
		{
			// reference: forum post #709
			// "Admin Hijack via WHMCS Module fails"
			$this->assertTrue((bool)(\Auth::profile()->level & PRIVILEGE_ADMIN), 'Global context is admin');
			$ctx = TestHelpers::with();
			\Error_Reporter::clear_buffer();
			$this->admin_hijack($ctx->site, null, 'UI');
			$this->assertEmpty(\Error_Reporter::get_errors(), 'Template engine loaded without permission errors');
		}

		public function testRecursiveLogout()
		{
			if (APNSCPD_HEADLESS) {
				$this->markTestSkipped('Headless mode');
			}
			\Auth::use_handler_by_name('UI');
			$this->assertSame((new \Auth_UI)->getDriver(), \Auth::get_driver()->getDriver());

			$adminCtx = \Auth::context(\TestHelpers::getAdmin());
			$this->assertTrue((bool)($adminCtx->level & PRIVILEGE_ADMIN), 'User is admin');
			$ids = ['admin' => session_id()];
			$adminAfi = \apnscpFunctionInterceptor::factory($adminCtx);
			$this->assertTrue(\Auth::autoload()->session_valid(), 'Session valid');
			$this->assertNotEmpty($_SESSION);
			$ctx = TestHelpers::with();

			$this->assertNotEmpty($_SESSION['valid']);
			$this->assertNotEmpty($hijackSession = $adminAfi->admin_hijack($ctx->getAccount()->site),
				'Hijack ID generated');
			$adminAfi = \apnscpFunctionInterceptor::init();
			$ids['site'] = session_id();
			$this->assertSame($hijackSession, session_id(), 'Session updated');
			$this->assertNotEmpty($_SESSION['valid']);

			$overrodeContext = \Auth::profile();
			$this->assertSame($ctx->username, $adminAfi->common_whoami(), 'Hijack contexted into user');
			$this->assertSame($overrodeContext->username, $ctx->username);

			$this->assertTrue(\Auth::autoload()->session_valid(), 'Hijack session valid');
			$this->assertTrue(\Auth::authenticated());

			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($overrodeContext));

			$userCtx = TestHelpers::createUser($overrodeContext->getAccount());
			\Error_Reporter::clear_buffer();

			$userId = $this->site_hijack($userCtx->username);
			$this->assertNotSame($userCtx->id, $userId);
			$ids['user'] = session_id();
			$this->assertSame($userId, session_id(), 'Session updated');
			$this->assertSame($userId, \Auth::profile()->id);
			$this->assertCount(3, array_unique($ids));
			$this->assertSame($ids['user'], \Auth::profile()->id);
			Page_Container::init(apps\login\Page::class, \Auth::profile())->logout();

			foreach (array_reverse($ids) as $label => $id) {
				$this->assertFalse(apnscpSession::init()->exists($id), "$label does not exist");
			}
			$this->assertEmpty(array_filter($_SESSION));

		}

	}
