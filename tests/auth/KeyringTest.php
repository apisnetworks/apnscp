<?php

	namespace auth;

	use Opcenter\Auth\Password;
	use Opcenter\Crypto\Keyring;
	use TestFramework;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class KeyringTest extends TestFramework
	{
		private string $index;

		public function setUp(): void
		{
			$this->index = implode('.', [Password::generate(8, 'a-z'), Password::generate(8, 'a-z')]);
			$afi = \apnscpFunctionInterceptor::factory(\Auth_Info_User::initializeUser(\Auth::get_admin_login()));
			$this->setApnscpFunctionInterceptor($afi);
			parent::setUp();
		}

		public function tearDown(): void
		{
			$this->keyring_forget($this->index);
			parent::tearDown();
		}

		public function testFetch()
		{
			$this->assertFalse($this->keyring_exists($this->index));
			$this->keyring_set($this->index, $val = 'testval');
			$this->assertSame($val, Keyring::decode($this->keyring_get($this->index)));
		}

		public function testReencode()
		{
			$oldSecret = Password::generate(12);
			$rfxn = new \ReflectionClass(Keyring::class);
			$keyring = $rfxn->newInstanceWithoutConstructor();
			$rfxn->getProperty('secret')->setValue($keyring, $oldSecret);
			$keyring->__construct('foobar');
			$encoded = $keyring->serialize();
			$this->assertSame('foobar', Keyring::decode($encoded, $oldSecret));
		}

		public function testComplex()
		{
			$this->assertSame($val = ['foo' => 'bar'], Keyring::decode(Keyring::encode($val)));
			$this->assertTrue($this->keyring_set($this->index, $val));
			$this->assertSame($val, Keyring::decode($this->keyring_get($this->index)));
		}
	}