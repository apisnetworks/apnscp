<?php
require_once dirname(__DIR__, 1) . '/TestFramework.php';
require_once(__DIR__ . '/SoapTestClient.php');

class DomainManagementTest extends TestFramework
{
	public function testShim()
	{
		if (!SOAP_ENABLED) {
			return $this->markTestSkipped('API support disabled');
		} else if (APNSCPD_HEADLESS) {
			return $this->markTestSkipped('Panel running in headless mode');
		}
		/**
		 * @var SoapClient $client
		 */
		$ephemeral = \Opcenter\Account\Ephemeral::create();
		$client = SoapTestClient::create($ephemeral->getContext());
		$e = null;
		$domain = \Opcenter\Auth\Password::generate(16, 'a-z') . '.test';
		try {
			$client->aliases_shared_domain_exists($domain);
		} catch (\SoapFault $f) {
			var_dump($client->__getLastResponse());
			throw $f;
		}
		if (!$client->aliases_shared_domain_exists($domain)) {
			$this->assertTrue($client->aliases_add_shared_domain($domain, '/var/www/html'));
		}
		if ($client->aliases_shared_domain_exists($domain)) {
			$this->assertTrue($client->aliases_remove_shared_domain($domain));
		}
		// clean up pending edit
		$this->assertTrue($client->aliases_synchronize_changes(), 'Synchronize changes');
	}
}