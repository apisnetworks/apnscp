<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
 */

class SoapTestClient {

    protected $client;

	/**
	 * Create API instance from context
	 *
	 * @param Auth_Info_User|string $token authentication context to derive key or existing key
	 * @param array                 $ctor SOAP constructor arguments
	 * @return SoapClient
	 */
    public static function create(array|string|\Auth_Info_User $token, array $ctor = []) {
    	$key = $token;
    	if ($token instanceof \Auth_Info_User) {
			$afi = \apnscpFunctionInterceptor::factory($token);
			if (!($key = $afi->auth_create_api_key())) {
				throw new RuntimeException("Failed to generate API key");
			}
			register_shutdown_function(static function () use ($key, $token) {
				if ($token->valid()) {
					\apnscpFunctionInterceptor::factory($token)->auth_delete_api_key($key);
				}
			});
		}
		return Util_API::create_client($key, null, null, $ctor);
	}
}