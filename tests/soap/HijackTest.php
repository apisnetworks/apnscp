<?php

use GuzzleHttp\Client;

require_once dirname(__DIR__, 1) . '/TestFramework.php';
	require_once(__DIR__ . '/SoapTestClient.php');

	class HijackTest extends TestFramework
	{
		public function testHijack()
		{
			if (!SOAP_ENABLED) {
				return $this->markTestSkipped('API support disabled');
			} else if (APNSCPD_HEADLESS) {
				return $this->markTestSkipped('Panel running in headless mode');
			}

			$adminCtx = \Auth::context(\Auth::get_admin_login());
			$adminAfi = \apnscpFunctionInterceptor::factory($adminCtx);
			$this->assertNotEmpty($key = $adminAfi->auth_create_api_key('API test'), 'Key generated successfully');
			defer($_, static function() use ($key, $adminAfi) {
				$adminAfi->auth_delete_api_key($key);
			});
			$adminSoapClient = Util_API::create_client($key);
			for ($i = 0; $i < 3; $i++) {
				$account = \Opcenter\Account\Ephemeral::create();
				$ctx = $account->getContext();
				$adminSoapClient->__setCookie(\session_name(), $adminCtx->id);
				$this->assertNotEmpty($hijackSession = $adminSoapClient->admin_hijack($ctx->getAccount()->site), 'Hijack ID generated');
				$adminSoapClient->__setCookie(\session_name(), $hijackSession);
				$this->assertSame($ctx->username, $adminSoapClient->common_whoami(), 'Hijack contexted into user');
			}

			// gated hijack, switch back to admin
			$adminSoapClient->__setCookie(\session_name(), $adminCtx->id);
			$id = $adminSoapClient->admin_hijack($ctx->getAccount()->site, null, 'UI');

			$this->assertTrue(apnscpSession::init()->exists($id));

			$client = new Client([
				'base_uri' => 'http://localhost:2082',
			]);
			$resp = $client->get('/apps/dashboard?' . \apnscpSession::SESSION_ID . '=' . $id)->getStatusCode();
			$this->assertEquals(200, $resp);
		}
	}