<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class GhostTest extends TestFramework
	{
		const VERSION = '3.41.5';
		const VERSION_NEXT = '3.41.6';
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testVersionFetch()
		{
			$afi = \apnscpFunctionInterceptor::init();
			$versions = $afi->ghost_get_versions();
			$this->assertNotEmpty($versions, 'Version check succeeded');
			$this->assertContains(self::VERSION, $versions, self::VERSION . ' in version index');
			$this->assertGreaterThan(array_search(self::VERSION, $versions), array_search(self::VERSION_NEXT, $versions), 'Versions are ordered');
		}

		public function testInstall()
		{
			$account = \Opcenter\Account\Ephemeral::create([
				'cgroup.enabled'  => 1,
				'cgroup.memory'   => 1024,
				'crontab.permit'  => 1,
				'ssh.enabled'     => 1,
				'pgsql.enabled'   => 1,
				'diskquota.quota' => 4,
				'diskquota.units' => 'GB'
			]);

			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;

			$versions = array_filter($afi->ghost_get_versions(), static function ($version) {
				return version_compare($version, '5.0.0', '<');
			});
			$upgradedVersion = array_pop($versions);
			$installedVersion = array_pop($versions);
			$this->assertTrue(
				$afi->ghost_install(
					$domain,
					'',
					[
						'version' => $installedVersion,
						'notify'  => false,
						'ssl'     => false,
						'email'   => null,
						'verlock' => 'none',
					]
				)
			);

			$this->assertEquals($installedVersion, $afi->ghost_get_version($domain),
				'Correct Ghost version is installed');

			if ($installedVersion !== $upgradedVersion) {
				$this->assertTrue($afi->ghost_update($domain, '', $upgradedVersion), 'Ghost updates');
				$this->assertEquals($upgradedVersion, $afi->ghost_get_version($domain),
					'Ghost upgrades to correct version');
			}
		}
	}