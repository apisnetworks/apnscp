<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class WordpressTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testVersionUpgrade()
		{
			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;
			$this->assertTrue(
				$afi->wordpress_install(
					$domain,
					'',
					[
						'version' => array_get(Definitions::get(), 'webapps.wordpress.install_version'),
						'notify'  => false,
						'ssl'     => false,
						'email'   => null
					]
				)
			);

			$waInstance = \Module\Support\Webapps\App\Loader::fromHostname('wordpress', $domain, '', $account->getContext());



			$this->assertInstanceOf(\Module\Support\Webapps\App\Type\Unknown\Handler::class, $waInstance);

			$this->assertSame(array_get(Definitions::get(), 'webapps.wordpress.install_version'), $waInstance->getVersion());
			$waInstance->setOption('verlock', 'minor');
			/**
			 * @xxx reloading + assignment creates a circular reference that
			 * results in old WA preferences being loaded. Calling $loader->reload()
			 * once without assignment, then a second time with assignment would
			 * free the reference, but that's cumbersome. Preferences::sync() is called
			 * explicitly in MetaManager to compensate for this
			 */
			$waInstance->reload();
			$this->assertSame($waInstance->getOption('verlock'), 'minor');
			$this->assertTrue($afi->wordpress_install_plugin(
				$domain,
				'',
				'google-analytics-for-wordpress',
				'7.9.0'
			));
			$this->assertTrue($afi->wordpress_install_theme(
				$domain,
				'',
				'oceanwp',
				'1.5.30'
			));
			// API does not implicitly check for next candidate version
			$job = \Module\Support\Webapps\UpdateCandidate::instantiateContexted($account->getContext(), [$domain, '']);
			$job->parseAppInformation($waInstance->getApplicationInfo());
			$job->setAvailableVersions($waInstance->getVersions());
			$this->assertNotSame(
				array_get(Definitions::get(), 'webapps.wordpress.install_version'),
				$job->getNextVersion()
			);
			$this->assertSame(
				array_get(Definitions::get(), 'webapps.wordpress.next_version'),
				$job->getNextVersion(),
				'Assert version upgrade'
			);
			$this->assertTrue($job->process(), 'Update Wordpress');
			// minor will bump 4.9.8 to 4.9.9
			$this->assertSame(array_get(Definitions::get(), 'webapps.wordpress.next_version'), $waInstance->reload()->getVersion());
			// 7.3.4 pulled from public, RIP
			$this->assertSame('7.9.0', $afi->wordpress_plugin_status(
				$domain,
				'',
				'google-analytics-for-wordpress'
			)['version']);
			$this->assertSame('1.5.32', $afi->wordpress_theme_status(
				$domain,
				'',
				'oceanwp'
			)['version']);
		}

		public function testLanguageInstall()
		{
			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;
			$this->assertTrue(
				$afi->wordpress_install(
					$domain,
					'',
					[
						'version' => array_get(Definitions::get(), 'webapps.wordpress.install_version'),
						'notify'  => false,
						'ssl'     => false,
						'email'   => null
					]
				)
			);

			$this->assertTrue(
				$afi->wordpress_install_language($domain, '', 'es_ES')
			);

			$lang = $afi->wordpress_language_status($domain, '', 'es_ES');
			$this->assertSame('active', $lang['status']);
			$this->assertTrue(
				$afi->wordpress_uninstall_language($domain, '', 'es_ES')
			);
		}

		/**
		 * Updating a webapp whose docroot is a symlink elsewhere
		 *
		 * Duplicate docroot as former directory to test cleanup
		 */
		public function testRelocateDocrootSymlink()
		{
			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;
			$this->assertTrue($afi->file_move('/var/www/html', '/var/www/wp-test'));
			$this->assertTrue($afi->file_symlink('/var/www/wp-test', '/var/www/html'));
			$this->assertTrue($afi->wordpress_install(
				$domain,
				'',
				[
					'version' => array_get(Definitions::get(), 'webapps.wordpress.install_version')
				]
			));
			// doubly detect the webapp
			$mm = \Module\Support\Webapps\MetaManager::factory($account->getContext());
			$this->assertNotEmpty($mm->get('/var/www/html')->toArray());
			$this->assertEquals(
				$mm->get('/var/www/html')->toArray(),
				$mm->get('/var/www/wp-test')->toArray()
			);
			$mm = null;

			$this->assertNotNull(
				\Module\Support\Webapps\MetaManager::factory($account->getContext())->get('/var/www/html')
			);
			// locate webapps, make sure dangling /var/www/html is removed
			$adminafi = \apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login()));
			$adminafi->admin_prune_webapps(['site' => $domain]);

			$this->assertTrue($afi->wordpress_update_all($domain, '', array_get(Definitions::get(), 'webapps.wordpress.next_version')));
			// validate /var/www/html symlink followed
			$meta = \Module\Support\Webapps\MetaManager::factory($account->getContext());
			$this->assertEquals(
				$meta->get('/var/www/html')->toArray(),
				$meta->get('/var/www/wp-test')->toArray()
			);

			$this->assertSame(
				array_get(Definitions::get(), 'webapps.wordpress.next_version'),
				$mmver = array_get(\Module\Support\Webapps\MetaManager::factory($account->getContext())->get('/var/www/wp-test'), 'version'),
				'Version in MetaManager matches upgrade version'
			);

			// update
			$this->assertSame(
				array_get(Definitions::get(), 'webapps.wordpress.next_version'),
				$afi->wordpress_get_version($domain),
				'Symlink version updated OK'
			);
			$this->assertSame(
				$mmver,
				array_get(
					\Preferences::factory($account->getContext()),
					implode(
						'.',
						[
							Module\Support\Webapps::APPLICATION_PREF_KEY,
							'/var/www/wp-test',
							'version'
						]
					)
				),
				'Preferences version matches MetaManager version'
			);

			$this->assertTrue($afi->wordpress_uninstall($domain));
			$this->assertTrue(\is_dir($account->getContext()->domain_shadow_path('/var/www/html')));
		}
	}