<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class DiscourseTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		const INSTALL_VERSION = '3.3.2';
		const EXPECTED_UPGRADE_VERSION = '3.3.3';

		public function testVersionFetch()
		{
			$afi = \apnscpFunctionInterceptor::init();
			$versions = $afi->discourse_get_versions();
			$this->assertNotEmpty($versions, 'Version check succeeded');
			$this->assertContains('2.3.0', $versions, '2.3.0 in version index');
			$this->assertGreaterThan(array_search('2.3.0.beta1', $versions), array_search('2.3.0', $versions),
				'Versions are ordered');
		}

		public function testInstall()
		{
			if (!SSH_USER_DAEMONS) {
				$this->markTestSkipped('[ssh] => user_daemons must be enabled');
			}

			if (version_compare($this->redis_version(), '6.0.2', '<')) {
				$this->markTestSkipped("Redis v6.2.0+ required");
			}

			$account = \Opcenter\Account\Ephemeral::create([
				'cgroup.enabled' => 0,
				'crontab.permit' => 1,
				'ssh.enabled' => 1,
				'pgsql.enabled' => 1,
				'diskquota.quota' => 10,
				'diskquota.units' => 'GB'
			]);
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;

			$this->assertTrue(
				$afi->discourse_install(
					$domain,
					'',
					[
						'version' => self::INSTALL_VERSION,
						'notify'  => false,
						'ssl'     => false,
						'email'   => null,
						'verlock' => 'minor'
					]
				)
			);

			$this->assertEquals(self::INSTALL_VERSION, $afi->discourse_get_version($domain), 'Correct Discourse version is installed');

			$this->assertTrue($afi->discourse_update_all($domain), 'Discourse updates');
			$this->assertEquals(self::EXPECTED_UPGRADE_VERSION, $afi->discourse_get_version($domain), 'Discourse upgrades to correct version');
		}
	}