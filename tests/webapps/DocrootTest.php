<?php

	namespace webapps;

	use Definitions;
	use Module\Support\Webapps\MetaManager;
	use Opcenter\Account\Ephemeral;
	use Opcenter\Filesystem;
	use TestFramework;
	use TestHelpers;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class DocrootTest extends TestFramework
	{
		public function testInstall()
		{
			$ctx = TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));

			$afi = \apnscpFunctionInterceptor::factory($ctx);
			$domain = Ephemeral::random('domain');
			$afi->aliases_add_domain($domain, "/var/www/{$domain}");
			$afi->aliases_synchronize_changes();

			// Laravel always installs maximal minor...
			$this->assertTrue(
				$afi->laravel_install(
					$domain,
					'',
					$opts = [
						'notify'  => false,
						'ssl'     => false,
						'email'   => null,
					]
				), 'Laravel Installed'
			);
			$oldRoot = $this->web_get_docroot($domain);

			$this->assertTrue(
				$afi->flarum_install(
					$domain,
					'',
					$opts + ['empty' => true]
				),
				'App replaces old'
			);

			$this->assertSame($oldRoot, $this->web_get_docroot($domain), 'Docroot remains same');
		}

		public function testEarlyUnwindRelocation() {
			$account = Ephemeral::create(['apache.jail' => 1]);
			$ctx = $account->getContext();
			$afi = \apnscpFunctionInterceptor::factory($ctx);

			$afi->web_add_subdomain($name = \Opcenter\Auth\Password::generate(8, 'a-z0-9'), "/var/www/{$name}");
			$info = $afi->web_subdomain_info($name);
			$this->assertDirectoryExists($ctx->domain_fs_path(\Web_Module::MAIN_DOC_ROOT));

			$installreq = \Module\Support\Webapps\App\Installer::factory($info['path'], $ctx);
			$installreq->setOption('type', 'bookstack');

			$afi->swap('bookstack', new class extends \Bookstack_Module {
				protected function checkVersion(array &$options): bool
				{
					$options['version'] = "9999999";
					return true;
				}
			});
			$job = \Lararia\Jobs\InstallAppJob::instantiateContexted($ctx, [$installreq]);
			$map = MetaManager::factory($ctx);
			$map->replace($info['path'], [
				'type' => 'bookstack',
				'version' => \Module\Support\Webapps\App\Installer::INSTALLING_VERSION
			]);

			$parent = dirname($ctx->domain_fs_path(\Web_Module::MAIN_DOC_ROOT));
			Filesystem::chogp($parent, $ctx->user_id, $ctx->group_id);
			Filesystem::chogp(dirname($parent), $ctx->user_id, $ctx->group_id);

			$this->assertFalse($job->fire());
			$this->assertDirectoryDoesNotExist($ctx->domain_fs_path($info['path']));
			$this->assertDirectoryExists($ctx->domain_fs_path(\Web_Module::MAIN_DOC_ROOT));
		}
	}