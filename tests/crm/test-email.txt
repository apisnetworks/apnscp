Return-Path: <help@hostineer.com>
X-Spam-Checker-Version: SpamAssassin 3.4.1 (2015-04-28) on atlas.hostineer.com
X-Spam-Level: 
X-Spam-Status: No, score=-1.1 required=4.0 tests=BAYES_00,DKIM_SIGNED,
	DKIM_VALID,DKIM_VALID_AU,HTML_MESSAGE,RCVD_IN_DNSWL_NONE,RP_MATCHES_RCVD,
	TXREP autolearn=no autolearn_force=no version=3.4.1
X-Spam-ASN: AS63949 50.116.16.0/20
X-Original-To: help@hostineer.com
Delivered-To: help@hostineer.com
Received: from thegoogle.apisnetworks.com (thegoogle.apisnetworks.com [64.22.68.251])
	by atlas.hostineer.com (Postfix) with ESMTP id 5CEBF1D06F35
	for <help@hostineer.com>; Sat,  8 Jul 2017 15:21:25 -0400 (EDT)
Received: from sputnik.apisnetworks.com (sputnik.hostineer.com [50.116.18.181])
	by thegoogle.apisnetworks.com (Postfix) with ESMTP id 4D2BA1482DA
	for <help@hostineer.com>; Sat,  8 Jul 2017 15:21:25 -0400 (EDT)
Received: from splunk.apisnetworks.com (24-124-49-73-static.midco.net [24.124.49.73])
	by sputnik.apisnetworks.com (Postfix) with ESMTPA id 12AD4847A
	for <help@hostineer.com>; Sat,  8 Jul 2017 19:21:25 +0000 (UTC)
DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed; d=hostineer.com;
	s=dkim-hostineer; t=1499541685;
	bh=ayexPbRftulTJu4NK1cqQ7cOhpPIrk7m5MZN0zARWS0=;
	h=To:Subject:Date:MIME-Version:Content-Type:Reply-To:Sender:From:
	 Message-Id:References:In-Reply-To;
	b=nvYPiexczaT9EYTtDswheMHYYBHQoKmu/SOQ2NomEicaMV63jg7XxXANRRZ/RHoxJ
	 YhBglpHbXBFdMH/xm3+9pxN/BExA64eWiQKLZRoNs4G6mI7ptMtPBNnEdVOhd4QfIa
	 r8zYMKOWxLzbLiGh1lbWcU07FEeBGOdNsadgIRxQ=
To: help@hostineer.com
Subject: fdssfd [ff7fbd09]
Date: Sat, 08 Jul 2017 15:21:25 -0400
MIME-Version: 1.0
Reply-To: help+1a15b71874da3502b039cf6ead270c324561f23a@hostineer.com
Sender: Hostineer Support <help@hostineer.com>
From: Hostineer Support <help@hostineer.com>
Message-Id: <1a15b71874da3502b039cf6ead270c324561f23a-21@hostineer.com>
References: <1a15b71874da3502b039cf6ead270c324561f23a-21@hostineer.com>
In-Reply-To: <1a15b71874da3502b039cf6ead270c324561f23a-0@hostineer.com>
Content-Type: multipart/alternative;
 boundary="------------0FD0D56AD0440C2E3723FA25"
Content-Language: en-US

This is a multi-part message in MIME format.
--------------0FD0D56AD0440C2E3723FA25
Content-Type: text/plain; charset=utf-8; format=flowed
Content-Transfer-Encoding: 8bit

Looking over the internal code for how Launchpad checks its domains for 
accessibility[1] before sending an ACME request to Let's Encrypt, 
thereis a possibility that Let's Encrypt's DNS servers picked up your 
old IP address when attempting to validate domain ownership and that 
error message isn't transmitted correctly in the ACME client. Launchpad 
uses a PHP ACME implementation[2], which from my patch contributions, is 
still rough around the edges.

When did you request the certificate in relation to changing your 
nameserver? I don't thinkit was related to one of the internal recursive 
nameservers returning a SERVFAIL response, but rather an issue with DNS 
TTL[3]. Either way, now that I am back in the office if you can 
reproduce the issue down the road it should be simpler for me to verify 
what segment (Launchpad, ACME client, LE ACME server) is causing problems.

1: 
https://github.com/apisnetworks/apnscp-modules/blob/master/modules/letsencrypt.php#L154-#L218
2: https://github.com/kelunik/acme-client
3: https://kb.hostineer.com/dns/dns-work/

- Matt


On 7/3/2017 12:19 PM, Hostineer Support wrote:
> Support Incident
>
>
>
> 	
> A new ticket has been opened.
>
>   * *DOMAIN:* kidfiji.net
>   * *SITE ID:* 176
>   * *SERVER:* atlas
>   * *ADMIN:* lukefiji
>   * *PACKAGE:*
>   * *INVOICE:* HOSTINEER-KV8BIZMH8ECA6XSZ4PO
>
> ------------------------------------------------------------------------
>
> After installing Let's Encrypt on my domain as http://www.kidfiji.net 
> <http://www.kidfiji.net/> and http//kidfiji.net and checking the 
> agreement and clicking on "Generate & Install", it proceeds and says 
> "Action succeeded" up top. But after that it doesn't seem to do 
> anything. When I refresh/revisit the page, it doesn't detect any 
> certificate.
>
> Respond to Ticket 
> <https://cp.hostineer.com/apps/troubleticket?view&id=17114> â€“ or just 
> reply to this email!
>
> This has been sent to all listed addresses on the ticket. To make 
> changes, open this ticket 
> <https://cp.hostineer.com/apps/troubleticket?view&id=17114> in the 
> control panel, then edit the EMAIL field to attach or remove 
> additional contacts.
>
>
> Thanks for choosing Hostineer!
>
> Launchpad Login <https://cp.hostineer.com> â€¢ help@hostineer.com 
> <mailto:help@hostineer.com> â€¢ @hostineer 
> <https://twitter.com/hostineer> â€¢ Knowledge Base 
> <https://kb.hostineer.com>
> 	
>


--------------0FD0D56AD0440C2E3723FA25
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  </head>
  <body text="#000000" bgcolor="#FFFFFF">
    <p><font face="Helvetica, Arial, sans-serif">Looking over the
        internal code for how <font face="Helvetica, Arial, sans-serif">Launchpad
          <font face="Helvetica, Arial, sans-serif">checks <font
              face="Helvetica, Arial, sans-serif"><font face="Helvetica,
                Arial, sans-serif">its domains for accessibility<font
                  face="Helvetica, Arial, sans-serif">[1] before <font
                    face="Helvetica, Arial, sans-serif">sending an ACME
                    request to Let's E<font face="Helvetica, Arial,
                      sans-serif">ncrypt, ther<font face="Helvetica,
                        Arial, sans-serif">e<font face="Helvetica,
                          Arial, sans-serif"> i</font>s a possib<font
                          face="Helvetica, Arial, sans-serif">il<font
                            face="Helvetica, Arial, sans-serif">it<font
                              face="Helvetica, Arial, sans-serif">y that
                              Let's Encrypt's DNS serve<font
                                face="Helvetica, Arial, sans-serif">rs
                                picked up your o<font face="Helvetica,
                                  Arial, sans-serif">ld IP address when
                                  attempting to <font face="Helvetica,
                                    Arial, sans-serif">validate <font
                                      face="Helvetica, Arial,
                                      sans-serif">domain ownership and t<font
                                        face="Helvetica, Arial,
                                        sans-serif">hat error message
                                        isn't <font face="Helvetica,
                                          Arial, sans-serif">transmitted
                                          <font face="Helvetica, Arial,
                                            sans-serif"></font></font>correctly
                                        in the ACME client</font>. <font
                                        face="Helvetica, Arial,
                                        sans-serif">Launchpad uses a PHP
                                        ACME implementation[2], which
                                        from my patch contributions, is
                                        still rough around the edges.</font><br>
                                    </font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></p>
    <p><font face="Helvetica, Arial, sans-serif"><font face="Helvetica,
          Arial, sans-serif"><font face="Helvetica, Arial, sans-serif"><font
              face="Helvetica, Arial, sans-serif"><font face="Helvetica,
                Arial, sans-serif"><font face="Helvetica, Arial,
                  sans-serif"><font face="Helvetica, Arial, sans-serif"><font
                      face="Helvetica, Arial, sans-serif"><font
                        face="Helvetica, Arial, sans-serif"><font
                          face="Helvetica, Arial, sans-serif"><font
                            face="Helvetica, Arial, sans-serif"><font
                              face="Helvetica, Arial, sans-serif"><font
                                face="Helvetica, Arial, sans-serif"><font
                                  face="Helvetica, Arial, sans-serif"><font
                                    face="Helvetica, Arial, sans-serif"><font
                                      face="Helvetica, Arial,
                                      sans-serif"><font face="Helvetica,
                                        Arial, sans-serif">When did you
                                        request the certificate in
                                        relation to changing your n<font
                                          face="Helvetica, Arial,
                                          sans-serif">ameserver? I don't
                                          think<font face="Helvetica,
                                            Arial, sans-serif"> it was
                                            related to one of t<font
                                              face="Helvetica, Arial,
                                              sans-serif">he internal
                                              recursive nameserver</font></font></font></font></font></font></font></font></font>s
                            returning a SERVFAIL <font face="Helvetica,
                              Arial, sans-serif">response, but rather an
                              issue with DNS TTL[<font face="Helvetica,
                                Arial, sans-serif">3</font>].</font>
                            Either way, now that I am back in the office
                            if you can reproduce the issue down the road
                            it should be <font face="Helvetica, Arial,
                              sans-serif">simpler for me to verify what
                              segment (Launchpad, ACME client, LE ACME
                              server) is causing problems.</font><br>
                          </font></font></font></font></font></font></font></font></font></font></font></p>
    <p><font face="Helvetica, Arial, sans-serif"><font face="Helvetica,
          Arial, sans-serif"><font face="Helvetica, Arial, sans-serif"><font
              face="Helvetica, Arial, sans-serif"><font face="Helvetica,
                Arial, sans-serif"><font face="Helvetica, Arial,
                  sans-serif"><font face="Helvetica, Arial, sans-serif"><font
                      face="Helvetica, Arial, sans-serif"><font
                        face="Helvetica, Arial, sans-serif"><font
                          face="Helvetica, Arial, sans-serif"><font
                            face="Helvetica, Arial, sans-serif"><font
                              face="Helvetica, Arial, sans-serif">1:
<a class="moz-txt-link-freetext" href="https://github.com/apisnetworks/apnscp-modules/blob/master/modules/letsencrypt.php#L154-#L218">https://github.com/apisnetworks/apnscp-modules/blob/master/modules/letsencrypt.php#L154-#L218</a><br>
                              <font face="Helvetica, Arial, sans-serif">2:
                                <a class="moz-txt-link-freetext" href="https://github.com/kelunik/acme-client">https://github.com/kelunik/acme-client</a><br>
                                <font face="Helvetica, Arial,
                                  sans-serif">3:
                                  <a class="moz-txt-link-freetext" href="https://kb.hostineer.com/dns/dns-work/">https://kb.hostineer.com/dns/dns-work/</a></font></font><br>
                            </font></font></font></font></font></font></font></font></font></font></font></font></p>
    <p><font face="Helvetica, Arial, sans-serif"><font face="Helvetica,
          Arial, sans-serif"><font face="Helvetica, Arial, sans-serif"><font
              face="Helvetica, Arial, sans-serif"><font face="Helvetica,
                Arial, sans-serif"><font face="Helvetica, Arial,
                  sans-serif"><font face="Helvetica, Arial, sans-serif"><font
                      face="Helvetica, Arial, sans-serif"><font
                        face="Helvetica, Arial, sans-serif"><font
                          face="Helvetica, Arial, sans-serif"><font
                            face="Helvetica, Arial, sans-serif"><font
                              face="Helvetica, Arial, sans-serif"><font
                                face="Helvetica, Arial, sans-serif">-
                                Matt</font></font></font></font></font></font></font></font></font></font></font></font></font><br>
    </p>
    <br>
    <div class="moz-cite-prefix">On 7/3/2017 12:19 PM, Hostineer Support
      wrote:<br>
    </div>
    <blockquote type="cite"
      cite="mid:be30a753e257bc7ffefce9fb1f259233405e0849-26498@hostineer.com">
      <meta name="viewport" content="width=device-width">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Support Incident</title>
      <style>
        * {
    margin: 0;
    padding: 0;
    font-family: Verdana, Geneva, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
}

img {
    max-width: 100%;
}
html {
    height: 100%;
}
body {
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
    width: 100% !important;
    height: 100%;
    line-height: 1.6;
}

/* Let's make sure all tables have defaults */
table td {
    vertical-align: top;
}

body, .body-wrap {
    background-color: #f6f6f6;

}
.body-wrap {
    width: 100%;
}

.container {
    display: block !important;
    max-width: 600px !important;
    margin: 0 auto !important;
    clear: both !important;
}

.content {
    max-width: 600px;
    margin: 0 auto;
    display: block;
    padding: 20px;
}

.main {
    background: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
}

.content-wrap {
    padding: 20px;
}

.content-block {
    padding: 0;
}

.header {
    width: 100%;
    margin-bottom: 20px;
}

.footer {
    width: 100%;
    clear: both;
    color: #999 !important;
    padding: 20px;
}

.footer a {
    color: #999;
}

.footer p, .footer a, .footer unsubscribe, .footer td {
    font-size: 12px;
}

.column-left {
    float: left;
    width: 50%;
}

.column-right {
    float: left;
    width: 50%;
}

h1, h2, h3 {
    font-family: Verdana, Geneva, sans-serif;
    color: #000;
    margin: 0 0 10px;
    line-height: 1.2;
    font-weight: 400;
}

h1 {
    font-size: 32px;
    font-weight: 500;
}

h2 {
    font-size: 24px;
}

h3 {
    font-size: 18px;
}

h4 {
    font-size: 14px;
    font-weight: 600;
}

p, ul, ol {
    margin-bottom: 10px;
    font-weight: normal;
}

p li, ul li, ol li {
    margin-left: 5px;
    list-style-position: inside;
}

a {
    color: #348eda;
    text-decoration: underline;
}

.btn-primary {
    text-decoration: none;
    color: #FFF;
    background-color: #742b3d;
    border: solid #742b3d;
    border-width: 10px 20px;
    line-height: 2;
    font-weight: bold;
    text-align: center;
    cursor: pointer;
    display: inline-block;
    border-radius: 5px;
    text-transform: capitalize;
}

.last {
    margin-bottom: 0;
}

.first {
    margin-top: 0;
}

.padding {
    padding: 10px 0;
}

.aligncenter {
    text-align: center;
}

.alignright {
    text-align: right;
}

.alignleft {
    text-align: left;
}

.clear {
    clear: both;
}

.alert {
    font-size: 16px;
    color: #fff;
    font-weight: 500;
    padding: 20px;
    text-align: center;
    border-radius: 3px 3px 3px 3px;
    margin-bottom: 10px;
}

.alert a {
    color: #fff;
    text-decoration: none;
    font-weight: 500;
    font-size: 16px;
}

.alert.alert-warning {
    background: #ff9f00;
}

.alert.alert-bad {
    background: #d0021b;
}

.alert.alert-good {
    background: #68b90f;
}

.alert.alert-info {
    background: #9d9d9d;
}

@media only screen and (max-width: 640px) {
    h2, h3, h4 {
        font-weight: 600 !important;
        margin: 20px 0 5px !important;
    }

    h1 {
        font-size: 22px !important;
    }

    h2 {
        font-size: 18px !important;
    }

    h3 {
        font-size: 16px !important;
    }

    .container {
        width: 100% !important;
    }

    .content, .content-wrapper {
        padding: 10px !important;
    }

    .invoice {
        width: 100% !important;
    }
}

ul.inline {
    list-style-type: none;
}

ul.inline li {
    display: inline-block;
    padding: 0.2em 1em;
    margin-left: -1em;
}

.context {
    color: #999;
}
    </style>
      <table class="body-wrap">
        <tbody>
          <tr>
            <td colspan="3">
              <h1 id="logo" style="height:40px;width: 200px;display:
                block;margin: 5px auto 0 auto;"> <img
                  src="https://hostineer.com/images/logo/hostineer-light.png"
                  moz-do-not-send="true"> </h1>
            </td>
          </tr>
          <tr>
            <td><br>
            </td>
            <td class="container" width="600">
              <div class="content">
                <table class="main" cellspacing="0" cellpadding="0"
                  width="100%">
                  <tbody>
                    <tr>
                      <td class="content-wrap">
                        <table cellspacing="0" cellpadding="0"
                          width="100%">
                          <tbody>
                            <tr>
                              <td class="content-block">
                                <table class="" cellspacing="0"
                                  cellpadding="0" width="100%">
                                  <tbody>
                                    <tr>
                                      <td class="alert alert-info"
style="color:#fff;font-weight:500;padding:20px;text-align:center;background:#9d9d9d;">
                                        A new ticket has been opened. </td>
                                    </tr>
                                    <tr>
                                      <td style="">
                                        <table style="border: 0 none; "
                                          cellspacing="0"
                                          cellpadding="0" width="100%"
                                          border="0">
                                          <tbody>
                                            <tr>
                                              <td class="">
                                                <ul class="inline">
                                                  <li class="meta"> <b>DOMAIN:</b>
                                                    kidfiji.net </li>
                                                  <li class="meta"> <b>SITE
                                                      ID:</b> 176 </li>
                                                  <li class="meta"> <b>SERVER:</b>
                                                    atlas </li>
                                                  <li class="meta"> <b>ADMIN:</b>
                                                    lukefiji </li>
                                                  <li class="meta"> <b>PACKAGE:</b>
                                                  </li>
                                                  <li class="meta"> <b>INVOICE:</b>
HOSTINEER-KV8BIZMH8ECA6XSZ4PO </li>
                                                </ul>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="3"
                                                class="content-block"
                                                style="padding:0 0
                                                20px;">
                                                <hr> </td>
                                            </tr>
                                            <tr>
                                              <td class="content-block"
                                                style="padding:0 0
                                                20px;">
                                                <p> After installing
                                                  Let's Encrypt on my
                                                  domain as <a
                                                    href="http://www.kidfiji.net/"
moz-do-not-send="true">http://www.kidfiji.net</a> and http//kidfiji.net
                                                  and checking the
                                                  agreement and clicking
                                                  on "Generate &amp;
                                                  Install", it proceeds
                                                  and says "Action
                                                  succeeded" up top. But
                                                  after that it doesn't
                                                  seem to do anything.
                                                  When I refresh/revisit
                                                  the page, it doesn't
                                                  detect any
                                                  certificate. </p>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="content-block"
                                                style="padding:0 0
                                                20px;"> <a
                                                  href="https://cp.hostineer.com/apps/troubleticket?view&amp;id=17114"
style="text-decoration:none;color:#FFF;background-color:#993950;border:solid
#993950;border-width:10px
20px;line-height:2;font-weight:bold;text-align:center;cursor:pointer;display:inline-block;border-radius:5px;text-transform:capitalize;"
                                                  class="btn-primary"
                                                  moz-do-not-send="true">Respond
                                                  to Ticket</a> â€“ or
                                                just reply to this
                                                email! </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <div class="footer">
                  <table width="100%">
                    <tbody>
                      <tr>
                        <td class="aligncenter content-block"
                          style="padding:0 0 20px;"> This has been sent
                          to all listed addresses on the ticket. To make
                          changes, open this <a
                            href="https://cp.hostineer.com/apps/troubleticket?view&amp;id=17114"
                            moz-do-not-send="true">ticket</a> in the
                          control panel, then edit the EMAIL field to
                          attach or remove additional contacts. </td>
                      </tr>
                    </tbody>
                  </table>
                  <div style="text-align: center;"> <br>
                    Thanks for choosing Hostineer! <br>
                    <br>
                    <a href="https://cp.hostineer.com"
                      moz-do-not-send="true">Launchpad Login</a> â€¢ <a
                      href="mailto:help@hostineer.com"
                      moz-do-not-send="true">help@hostineer.com</a> â€¢ <a
                      href="https://twitter.com/hostineer"
                      moz-do-not-send="true">@hostineer</a> â€¢ <a
                      href="https://kb.hostineer.com"
                      moz-do-not-send="true">Knowledge Base</a> <br>
                  </div>
                </div>
              </div>
            </td>
            <td><br>
            </td>
          </tr>
        </tbody>
      </table>
    </blockquote>
    <br>
  </body>
</html>

--------------0FD0D56AD0440C2E3723FA25--