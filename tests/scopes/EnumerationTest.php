<?php

	use Opcenter\Http\Apache;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class EnumerationTest extends TestFramework
	{
		const EXCEPTION_LEVEL = Error_Reporter::E_ERROR;

		public function testScopeList()
		{
			$afi = \apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login()));
			$this->assertNotEmpty($scopes = $afi->scope_list(), 'Scope retrieval');

			foreach ($scopes as $scope) {
				$this->assertNotEmpty($afi->scope_info($scope), "Testing scope:info retrieval on {$scope}");
			}
		}
	}

