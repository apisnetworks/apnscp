<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class AddonDomainGhostTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testGhostedDomain() {
			/**
			 * via Discord:
			 *
			 * domain example.com already attached to site id `2'
			 * but the account doesn't appear under Nexus
			 * ok, I think I had this issue in the past
			 * so basically, if you add a domain
			 * it should be added to Mail Routing
			 * if you delete the addon domain,
			 * it doesn't get removed from the mail routing
			 */
			$acct1 = \Opcenter\Account\Ephemeral::create([
				'mail.enabled' => true,
				'aliases.max'  => null
			]);

			$afi = $acct1->getApnscpFunctionInterceptor();
			$testDomain = \Opcenter\Auth\Password::generate(16, 'a-z0-9')  . '.test';
			$afi->aliases_add_domain($testDomain, '/var/www/somepath');
			$afi->aliases_synchronize_changes();
			$this->assertContains($testDomain, $acct1->getContext()->getAccount()->conf['aliases']['aliases'], 'New domain in aliases');
			$this->assertTrue($afi->email_add_virtual_transport($testDomain), 'Virtual transport created');
			$afi->aliases_remove_domain($testDomain);
			$afi->aliases_synchronize_changes();
			$db = \PostgreSQL::initialize();
			$db->query("SELECT site_id FROM domain_lookup WHERE domain = '" . pg_escape_string(\PostgreSQL::initialize()->getHandler(), $testDomain) . "'");
			$this->assertEquals(0, $db->num_rows());

		}
	}

