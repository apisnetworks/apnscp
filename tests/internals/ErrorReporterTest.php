<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */
require_once dirname(__DIR__, 1) . '/TestFramework.php';

class ErrorReporterTest extends TestFramework
{
	public function testSilence() {
		$wasDebug = $_SESSION['DEBUG'] ?? null;
		defer($_, static fn() => $_SESSION['DEBUG'] = $wasDebug);
		$_SESSION['DEBUG'] = false;
		$this->assertEmpty(\Error_Reporter::get_buffer());
		silence(function () {
			error("foo!");
		});
		$this->assertEmpty(\Error_Reporter::get_buffer());
	}

	public function testSymbolicMesssage() {
		error(":testing", "foo");
		$this->assertEquals("foo", \Error_Reporter::flush_buffer()[0]['message']);
	}

	public function testArrayUnpack() {
		$macros = [
			error(...),
			info(...),
			warn(...),
			debug(...),
			debug_ex(...),
			success(...),
			fatal(...)
		];
		foreach ($macros as $macro) {
			$oldex = Error_Reporter::exception_upgrade(0xFFFF);
			try {
				$macro([':testing', 'foo %s %d'], ['bar', $int = random_int(1,100)]);
			} catch (\apnscpException $e) {
				$this->assertStringContainsString("foo bar $int", $e->getMessage());
			} finally {
				\Error_Reporter::clear_buffer();
			}

			try {
				$macro([':testing', 'foo %s %d'], 'bar', $int);
			} catch (\apnscpException $e) {
				$this->assertStringContainsString("foo bar $int", $e->getMessage());
			} finally {
				\Error_Reporter::clear_buffer();
			}

			\Error_Reporter::exception_upgrade($oldex);
		}
	}

	public function testSymbolicFilter() {
		\Error_Reporter::filter(static fn() => error(":testing", "silence"), [':testing']);
		$this->assertEmpty(\Error_Reporter::flush_buffer());
		\Error_Reporter::filter(
			static fn() => error(":testing", "silence") || error(":abc", "loud"),
			[':testing']
		);
		$this->assertCount(1, \Error_Reporter::flush_buffer());
	}
}