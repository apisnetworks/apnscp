<?php

require_once dirname(__DIR__, 1) . '/TestFramework.php';

class MultilineMapTest extends TestFramework
{
	const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

	public function setUp(): void
	{
		$this->file = tempnam(TEMP_DIR, 'test');
		parent::setUp();
	}

	public function tearDown(): void
	{
		if (file_exists($this->file)) {
			unlink($this->file);
		}
		parent::tearDown();
	}

	public function testMultiKey() {
		$file = tmpfile();
		defer($_, fn() => fclose($file));
		$name = stream_get_meta_data($file)['uri'];
		$map = new Opcenter\Map\Textfile($name, 'r+', ':', true);
		$map['foo'] = 'bar';
		$map['foo'] = 'baz';
		$this->assertInstanceOf(\Opcenter\Map\Textfile\Collection::class, $map['foo']);
		$this->assertCount(2, $map['foo']->all());
		$this->assertEquals("foo:bar\nfoo:baz", (string)$map['foo']);
	}

	public function testRead() {
		$lines = "foo bar\nfoo baz\nquu qux";
		$map = \Opcenter\Map\Textfile::fromString($lines, ' ', true);
		$this->assertIsIterable($map->fetch('foo'));
		$this->assertCount(2, $map->fetch('foo'));
		$this->assertEquals($lines, (string)$map);
	}

	public function testMatch() {
		$lines = "foo bar\nfoo baz\nquu qux";
		$map = \Opcenter\Map\Textfile::fromString($lines, ' ', true);
		$this->assertNotNull($map->fetch('foo')->is('bar'));
		$this->assertNull($map->fetch('foo')->is('quu'));
	}

	public function testReplace() {
		$lines = "foo bar\nfoo baz\nquu qux";
		$map = \Opcenter\Map\Textfile::fromString($lines, ' ', true);
		$this->assertNotNull($map->fetch('foo')->is('bar'));
		$map->fetch('foo')->replace('foo', 'quu');
		$this->assertNull($map->fetch('foo')->is('quu'));
		$map->fetch('foo')->replace('bar', 'quu');
		$this->assertNotNull($map->fetch('foo')->is('quu'));
	}

	public function testRemoval() {
		$lines = "foo bar\nfoo baz\nquu qux";
		$map = \Opcenter\Map\Textfile::fromString($lines, ' ', true);
		$this->assertNotNull($map->fetch('foo')->is('bar'));
		$map->fetch('foo')->is('bar')->forget();
		$this->assertNull($map->fetch('foo')->is('bar'));
		$this->assertEquals("foo baz\nquu qux", (string)$map);
	}
}