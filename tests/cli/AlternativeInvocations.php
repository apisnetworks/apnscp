<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	use cli\{
		parse, merge
	};

	class AlternativeInvocationsTest extends TestFramework
	{
		const BIN = INCLUDE_PATH . '/bin/cmd';
		protected $filename;

		public function testInvocation()
		{
			$this->assertTrue(array_get(\Util_Process::exec(self::BIN . ' misc_cp_version'), 'success'));
			$this->assertTrue(array_get(\Util_Process::exec(self::BIN . ' misc:cp_version'), 'success'));
			$this->assertTrue(array_get(\Util_Process::exec(self::BIN . ' misc:cp-version'), 'success'));
			$this->assertFalse(array_get(\Util_Process::exec(self::BIN . ' misc:cp-version-does-not-exist'), 'success'));
		}
	}

