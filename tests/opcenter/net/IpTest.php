<?php

	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class IpTest extends TestFramework
	{
		const EXCEPTION_LEVEL = Error_Reporter::E_ERROR;

		public function testValidity()
		{
			$ips = [
				'Ip4' => [
					'pass' => [
						'12.15.66.12',
						'12.1.115.6/32',
					],
					'fail' => [
						'12.1.256.1',
						'12.1.112.1/33'
					]
				],
				'Ip6' => [
					'pass' => [
						'::1',
						'::1/24',
					],
					'fail' => [
						'1::2:4::1',
						'::1/255'
					]
				]
			];
			foreach ($ips as $class => $types) {
				foreach ($types as $type => $addrs) {
					foreach ($addrs as $addr) {
						$checker = "\\Opcenter\\Net\\$class";
						$truthyCheck = $type === 'pass' ? 'assertTrue' : 'assertFalse';
						if (false === strpos($addr, '/')) {
							// IPCommon supports range-based checks where IPv4/IPv6 is strict
							$this->assertSame(\Opcenter\Net\IpCommon::valid($addr), $checker::valid($addr));
						}

						$this->$truthyCheck(\Opcenter\Net\IpCommon::valid($addr), "Validate $class $addr is $type");
					}
				}
			}
		}

		public function testReserved() {
			$addrs = [
				'2607:f8b0:4000:814::200e' => false,
				'4.1.1.1' => false,
				'10.10.3.4' => true,
				'fe80:1:2:3:a:bad:1dea:dad' => true,
				'fe80::a00:27ff:fe25:4f8c' => true,
				'::ffff:127.0.0.1' => true
			];
			foreach ($addrs as $addr => $truthy) {
				$this->assertSame($truthy, \Opcenter\Net\IpCommon::reserved($addr));
			}
		}
	}

