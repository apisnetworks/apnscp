<?php
	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class VolatileTest extends TestFramework
	{
		const EXCEPTION_LEVEL = Error_Reporter::E_ERROR;

		public function testMemoryLimit()
		{
			$account = \Opcenter\Account\Ephemeral::create([
				'cgroup.memory' => 512,
				'cgroup.enabled' => 1
			]);
			$this->assertNotNull($account);

			$attr = \Opcenter\System\Cgroup::resolveParameterController('memory');
			$controller = \Opcenter\System\Cgroup\Controller::make(new \Opcenter\System\Cgroup\Group(null), 'memory');
			$this->assertEquals($controller::class, $attr, 'cgroup resolved');

			$volatile = \Opcenter\System\Cgroup\VolatileAttribute::instantiateContexted($account->getContext());
			$attr = $volatile->fromAttribute('memory');
			$this->assertTrue($attr->hasLimit());
			$this->assertEquals(512 * 1024 ** 2, $attr->read());
			$volatile->resource($attr);
			// -1 translates to 64-bit int max less 4095 bytes
			$this->assertFalse($attr->hasLimit());
			$volatile = null;
			$this->assertEquals(512 * 1024 ** 2, $attr->read());
			return true;
		}

		public function testLaravelInstall()
		{
			if (!WEBAPPS_COMPOSER_VOLATILE) {
				$this->markTestSkipped('[webapps] => composer_volatile is false');
			}
			$account = \Opcenter\Account\Ephemeral::create([
				'cgroup.memory' => 128,
				'cgroup.enabled' => 1
			]);
			$this->assertNotNull($account);
			$afi = $account->getApnscpFunctionInterceptor();
			$this->assertTrue($afi->laravel_install($account->getContext()->domain));
		}
	}