<?php

	namespace opcenter\cgroup;

	use Opcenter\Account\Ephemeral;
	use Opcenter\System\Cgroup;
	use Opcenter\System\Cgroup\Group;
	use TestFramework;

	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class DelegationTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		const CGROUP_HOME = CGROUP_HOME;
		const CGCONFIG_HOME = Cgroup::CGROUP_SERVER_DIR;

		public function testDelegation()
		{
			if (Cgroup::version() === 1) {
				$this->markTestSkipped("Migration requires cgroupv2");
			}
			$account = \Opcenter\Account\Ephemeral::create([
				'cgroup.memory'  => 512,
				'cgroup.enabled' => 1
			]);

			$subaccount = \Opcenter\Account\Ephemeral::create([
				'cgroup.memory'  => 512,
				'cgroup.enabled' => 1
			]);

			$group = new Group($account->getContext()->site);
			$subgroup = new Group($subaccount->getContext()->site);

			$this->assertDirectoryExists(self::CGROUP_HOME . '/' . $group);
			$this->assertDirectoryExists(self::CGROUP_HOME . '/' . $subgroup);


			$subaccount->edit(['cgroup.delegator' => (string)$group]);
			$this->assertDirectoryExists(self::CGROUP_HOME . '/' . $group . '/' . Group::CIRCULAR_IDENTIFIER);
			$this->assertDirectoryExists(self::CGROUP_HOME . "/{$group}/{$subgroup}");
			$this->assertStringEndsWith("{$group}/{$subgroup}", rtrim(file_get_contents($subaccount->getContext()->domain_fs_path(Cgroup::CGROUP_SITE_CONFIG))));
			$this->assertStringEndsWith("{$group}/" . Group::CIRCULAR_IDENTIFIER,
				rtrim(file_get_contents($account->getContext()->domain_fs_path(Cgroup::CGROUP_SITE_CONFIG))));

			$subaccount->edit(['cgroup.delegator' => null]);
			$this->assertDirectoryDoesNotExist(self::CGROUP_HOME . "/{$group}/{$subgroup}");
			$this->assertDirectoryDoesNotExist(self::CGROUP_HOME . '/' . $group . '/' . Group::CIRCULAR_IDENTIFIER);
			$this->assertStringEndsWith(" {$subgroup}",
				rtrim(file_get_contents($subaccount->getContext()->domain_fs_path(Cgroup::CGROUP_SITE_CONFIG))));
			$this->assertStringEndsWith("{$group}",
				rtrim(file_get_contents($account->getContext()->domain_fs_path(Cgroup::CGROUP_SITE_CONFIG))));

			$this->assertFileDoesNotExist(self::CGCONFIG_HOME . "/{$group}-{$subgroup}.conf");
			$this->assertFileDoesNotExist(self::CGCONFIG_HOME . "/{$group}-" . Group::CIRCULAR_IDENTIFIER . ".conf");
			$this->assertFileExists(self::CGCONFIG_HOME . "/{$subgroup}.conf");

			$subsubaccount = Ephemeral::create([
				'cgroup.memory' => 512,
				'cgroup.enabled' => 1,
			]);

			$subsubgroup = new Group($subsubaccount->getContext()->site);
			$subaccount->edit(['cgroup.delegator' => (string)$group]);
			$subsubaccount->edit(['cgroup.delegator' => (string)$subgroup]);
			$this->assertDirectoryExists(self::CGROUP_HOME . "/{$group}/{$subgroup}/{$subsubgroup}");
			$this->assertCount(4 /* self + sub + sub-self + sub-sub */, $group->descendants());
			$this->assertCount(2, ($subgroup = new Group($subgroup))->descendants());
			// NB: automatically includes group/subgroup upon reassignment
			$this->assertStringEndsWith("{$subgroup}/{$subsubgroup}",
				rtrim(file_get_contents($subsubaccount->getContext()->domain_fs_path(Cgroup::CGROUP_SITE_CONFIG))));
			$subsubaccount->destroy();
			$this->assertDirectoryDoesNotExist(self::CGROUP_HOME . "/{$subgroup}/{$subsubgroup}");
			$this->assertTrue($subgroup->migrate(null));
			$subaccount->destroy();

			return true;
		}
	}