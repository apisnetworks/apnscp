<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class BackupTest extends TestFramework
	{
		const EXCEPTION_LEVEL = Error_Reporter::E_ERROR;

		protected $path;

		public function testBackupRestore()
		{
			if (\Opcenter\Apnscp::failed()) {
				$this->markTestIncomplete("apnscp service disabled");
			}
			$this->path = tempnam(storage_path('tmp'), 'license-test');
			$this->assertTrue(unlink ($this->path), 'Unlink path');
			$this->assertTrue(array_get(\Util_Process::exec(script_path('license.php') . ' backup %s', [$this->path]), 'success'), 'Backup succeeded');
			$this->assertFileExists($this->path, 'License backed up');
			$this->assertTrue(array_get(\Util_Process::exec(script_path('license.php') . ' restore %s', [$this->path]), 'success'), 'License restore succeeded');
			sleep(10);
			// wait for backend activation to prevent unit tests prematurely failing
			\Opcenter\Apnscp::wait();
		}

		public function __destruct()
		{
			if (isset($this->path)) {
				@unlink($this->path);
			}

		}
	}

