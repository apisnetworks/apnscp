<?php
    /**
     *   /usr/local/bin/AddVirtDomain -c siteinfo,domain=testing.com -c siteinfo,admin_user=test \
     *   -c siteinfo,tpasswd=test -c siteinfo,email=test@test.com -c mysql,dbaseadmin=test \
     *   -c mysql,dbaseprefix=test_ -c bandwidth,rollover=13 -c bandwidth,threshold=10737418240 \
     *   -c diskquota,units=MB -c diskquota,quota=500 -c develenv,enabled=1 -c ssh,enabled=1 \
     *   -c ipinfo,namebased=0 -c ipinfo,ipaddrs=['192.168.0.33'] -c urchin,max_profiles=2 \
     *   -c ssl,enabled=1
     */

    class TestFramework extends \PHPUnit\Framework\TestCase
    {
    	const EXCEPTION_LEVEL = \Error_Reporter::E_FATAL;

        use apnscpFunctionInterceptorTrait;
        use FilesystemPathTrait;

		/**
		 * @var Auth_Info_User
		 */
        protected $context;
        protected $exceptionLevel;

        protected function setUp(): void
        {
			$this->exceptionLevel = \Error_Reporter::exception_upgrade(static::EXCEPTION_LEVEL);
			if (!\apnscpSession::init()->exists(\Auth::profile()->id) || !\Auth::profile()->valid()) {
				\Auth::autoload()->authenticate();
				\apnscpFunctionInterceptor::init(true);
			}
			\Error_Reporter::clear_buffer();
        }

        protected function tearDown(): void
        {
			unset($this->apnscpFunctionInterceptor);
			\Error_Reporter::exception_upgrade($this->exceptionLevel);
        	if ($this->hasFailed()) {
				echo Symfony\Component\Yaml\Yaml::dump(\Error_Reporter::flush_buffer());
			}
			gc_collect_cycles();
		}

    }