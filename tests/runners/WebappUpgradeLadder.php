<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class WebappUpgradeLadder extends TestFramework
	{
		public function testInstallUpgrade()
		{
			if (!$module = env('module')) {
				fatal("Missing module=name env");
			}

			$maxVersion = env('max') ?: 999999.99999999;
			$minVersion = env('min') ?: 0;
			$threshold = (int)env('threshold');

			$account = \Opcenter\Account\Ephemeral::create(['cgroup.enabled' => 0, 'dns.enabled' => 0, 'apache.jail' => 1]);
			\Opcenter\Http\Apache::buildConfig('now', true);
			\Opcenter\Http\Apache::waitRebuild();
			$afi = $account->getApnscpFunctionInterceptor();
			if (env("php_version")) {
				$afi->php_pool_set_version(env("php_version"));
			}
			$domain = $account->getContext()->domain;
			$versions = $afi->{$module . '_get_versions'}();
			array_shift($versions); // 2021.003 RC

			do { } while (\Opcenter\Versioning::compare($minVersion, $install = array_shift($versions), '>'));
			$this->assertTrue(
				$afi->{$module . '_install'}(
					$domain,
					'',
					[
						'version' => $install,
						'notify'  => false,
						'ssl'     => false,
						'email'   => null,
						'verlock' => false,
						'keep'    => true
					]
				)
			);
			$this->assertEquals($install, $afi->{$module . '_get_version'}($domain, ''));
			$httpClient = \HTTP\SelfReferential::instantiateContexted($account->getContext(),
				[$domain, $afi->site_ip_address()]);
			while (null !== ($version = array_shift($versions)) && \Opcenter\Versioning::compare($version, $maxVersion, '<=')) {
				$i = 0;
				do {
					try {
						$preCheck = $httpClient->get();
						break;
					} catch (\GuzzleHttp\Exception\ServerException $e) {
						if (++$i < (int)env('retry')) {
							sleep(1);
							continue;
						}

						if (\Error_Reporter::is_verbose(\Error_Reporter::E_EXCEPTION)) {
							echo $e->getResponse()->getBody()->getContents();
							die();
						}
						throw $e;
					}
				} while (true);

				$this->assertEquals(200, $preCheck?->getStatusCode());
				fprintf(STDERR, "Updating to {$version}\n");
				$this->assertTrue(
					$afi->{$module . '_update_all'}($domain, '', $version),
					"Update $module - {$version}"
				);
				fprintf(STDERR, "Updated to {$version}\n");
				$this->assertEquals($version, $afi->{$module . '_get_version'}($domain));

				do {
					try {
						$postCheck = $httpClient->get();
						break;
					} catch (\GuzzleHttp\Exception\ServerException $e) {
						if (++$i < (int)env('retry')) {
							sleep(1);
							continue;
						}
						if (\Error_Reporter::is_verbose(\Error_Reporter::E_EXCEPTION)) {
							echo $e->getResponse()->getBody()->getContents();
							die();
						}
						throw $e;
					} catch (\GuzzleHttp\Exception\ClientException $e) {

					}
				} while (true);

				$this->assertSame($preCheck->getStatusCode(), $postCheck->getStatusCode());
				if ($threshold) {
					$this->assertEqualsWithDelta(
						$preCheck->getBody()->getSize(),
						$postCheck->getBody()->getSize(),
						$preCheck->getBody()->getSize()*$threshold/100,
						sprintf('New page size within %d%%', $threshold)
					);
				}
			}
		}
	}