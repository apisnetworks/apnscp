<?php
require_once dirname(__FILE__) . '/../TestFramework.php';

class SubdomainTest extends TestFramework
{
    private $auth;
    private $afi;
    private $subdomains = [];

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
        $this->afi = \apnscpFunctionInterceptor::factory($this->auth);
        $this->bindPathContext($this->auth);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown(): void
    {
        foreach ($this->subdomains as $host => $path) {
            $this->cleanSubdomain($host, $path);
        }
        parent::tearDown();

    }

    private function cleanSubdomain($host, $docroot)
    {
        $path = $this->domain_fs_path() . $docroot;
        if (file_exists($path)) {
            $this->assertTrue(
                $this->afi->file_delete($docroot, true)
            );
        }
        if ($this->afi->web_subdomain_exists($host)) {
            $this->assertTrue(
                $this->afi->web_remove_subdomain($host)
            );
        }
    }

    private function addCleanup($domains, $path = null)
    {
        $this->subdomains[$domains] = $path;
    }

    public function testCreateSubdomain()
    {
        $host = uniqid('subdomain-');
        $docroot = '/var/www/' . $host;
        $this->addCleanup($host, $docroot);
        $this->assertTrue($this->afi->web_add_subdomain($host, $docroot));
    }

    public function testRename() {
    	// implied by testCreateSubdomain succeeding
		$host = uniqid('subdomain-');
		$docroot = '/var/www/' . $host;
		$this->assertTrue($this->afi->web_add_subdomain($host, $docroot));
		$this->assertFileExists($this->afi->file_make_path(Web_Module::SUBDOMAIN_ROOT . "/$host/html"));

		$this->assertTrue($this->afi->web_rename_subdomain($host, "test-$host", "/var/www/test-$host"));
		$this->addCleanup("test-$host", $docroot);

		$path = $this->afi->file_make_path(Web_Module::SUBDOMAIN_ROOT . "/test-$host/html");
		$this->assertFileExists($path);
		$this->assertSame($this->afi->file_make_path("/var/www/test-$host"), realpath($path));
	}

	public function testRemap() {
		$host = uniqid('subdomain-');
		$docroot = '/var/www/' . $host;
		$this->assertTrue($this->afi->web_add_subdomain($host, $docroot));
		$this->assertFileExists($this->afi->file_make_path(Web_Module::SUBDOMAIN_ROOT . "/$host/html"));

		$this->assertTrue($this->afi->web_rename_subdomain($host, $host, "/var/www/test-$host"));
		$this->addCleanup($host, $docroot);

		$path = $this->afi->file_make_path(Web_Module::SUBDOMAIN_ROOT . "/$host/html");
		$this->assertFileExists($path);
		$this->assertSame($this->afi->file_make_path("/var/www/test-$host"), realpath($path));
	}
}