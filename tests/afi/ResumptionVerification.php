<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2022
 */

require_once dirname(__DIR__, 1) . '/TestFramework.php';

class ResumptionVerification extends TestFramework
{
	private Auth_Info_User $ctx;
	public function tearDown(): void
	{
		if (isset($this->ctx)) {
			Auth::swap($this->ctx);
		}
	}

	public function testSessionIdentificationReplacement()
	{
		$auth = \TestHelpers::create($domain = array_get(Definitions::get(), 'auth.site.domain'));
		$ctx = \Auth::swap($auth);
		$this->assertNotEquals($auth->id, $ctx->id);
		$this->ctx = $ctx;
		$ui = \Module\Support\Webapps\App\UIPanel::instantiateContexted($auth);
		$ui->get($domain)->getManifest();
		$ui = null;
		$auth->destroy();


		$this->assertFalse(\apnscpSession::init()->exists($auth->id));

		unset($meta);
		gc_collect_cycles();

		$auth =\Auth::context(null, $domain);
		// global context must be held to trigger
		\Auth::swap($auth);
		$ui = \Module\Support\Webapps\App\UIPanel::instantiateContexted($auth);
		$this->assertTrue(\apnscpFunctionInterceptor::factory($auth)->context_matches_id($auth->id));
		// deserialization of an expired session regenerates an afi instance. This instance replaces
		// global context ID creating mismatch with afi ID
		$this->assertInstanceOf(\Module\Support\Webapps\App\Type\Adhoc\Manifest::class, $ui->get($domain)->getManifest());
	}
}

