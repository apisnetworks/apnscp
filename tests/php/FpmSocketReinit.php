<?php

	namespace php;

	use Opcenter\Account\Edit;
	use Opcenter\Account\Ephemeral;
	use Opcenter\Http\Php\Fpm;
	use Opcenter\SiteConfiguration;
	use TestFramework;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class FpmSocketReinit extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		/**
		 * @covers \Opcenter\Http\Php\Fpm
		 */
		public function testReconfigFlag()
		{
			if (!Fpm::hasCapability()) {
				$this->markTestSkipped('No FPM capability');
			}

			$account = Ephemeral::create([
				'apache.jail' => 1
			]);
			$config = Fpm\Configuration::bindTo($account->getContext()->domain_fs_path());
			$config->setServiceContainer(SiteConfiguration::shallow($account->getContext()));
			$fpmClass = new Fpm($config);
			$fpmClass->mockService()->run('start');
			$account->getApnscpFunctionInterceptor()->php_pool_restart();
			$path = $config->getSocketPath();
			while ($fpmClass->mockService()->state() !== 'active') {
				usleep(500000);
			}
			/**
			 * Tricky async problem, race condition between async socket enable + restart calls to systemd
			 *
			 * Cannot reproduce with 5 iterations, 10 magic number. Prior to ref #b0bed538
			 */
			$this->assertFileExists($path, 'Socket exists');
			$socketInstance = $fpmClass->mockService(true);

			$this->assertTrue($socketInstance->running(), 'PHP-FPM socket present');
			// evaluated up to 1800
			for ($i = 0; $i < 30; $i++) {
				\Error_Reporter::clear_buffer();
				unlink($path);
				if (file_exists($path)) {
					$this->fail('Socket present?');
				}

				(new Edit($account->getContext()->site))->setOption('reconfig', true)->exec();
				clearstatcache(true, $path);
				if (!file_exists($path)) {
					$this->fail("Socket not recreated");
				}
			}
			$this->assertTrue($socketInstance->running(), 'PHP-FPM active');


		}
	}