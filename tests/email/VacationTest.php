<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */


	use Opcenter\Mail\Vacation\Providers\Mailbot\Options\Message;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class VacationTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testUserSet()
		{
			$acct = \Opcenter\Account\Ephemeral::create(['mail.enabled' => true, 'mail.provider' => 'builtin']);

			$afi = $acct->getApnscpFunctionInterceptor();
			$primaryMsg = $acct->getContext()->domain_fs_path() . $afi->user_get_home() . '/' . Message::VACATION_FILE;
			$this->assertFileDoesNotExist($primaryMsg);
			$this->assertTrue($afi->email_enable_vacation());
			$this->assertFileExists($primaryMsg);

			$user = \Opcenter\Auth\Password::generate(8, 'a-z');
			$this->assertTrue($afi->user_add(
				$user,
				\Opcenter\Auth\Password::generate(),
				'',
				0,
				[
					'imap' => true,
					'pop3' => true,
					'smtp' => false
				]
			));

			$secondaryMsg = $acct->getContext()->domain_fs_path() . $afi->user_get_home($user) . '/' . Message::VACATION_FILE;
			$this->assertNotEquals($secondaryMsg, $primaryMsg);
			$this->assertFileDoesNotExist($secondaryMsg);
			$afi->email_enable_vacation($user);
			$this->assertFileExists($secondaryMsg);
		}
	}
