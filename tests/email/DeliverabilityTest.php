<?php

	use Opcenter\Admin\Bootstrapper\Config;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class DeliverabilityTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 *
		 */
		public function testDeliverability()
		{
			if (MAIL_PROVIDER_DEFAULT === 'null') {
				return;
				$this->markTestSkipped("Mail disabled");
			}
			$acct = \Opcenter\Account\Ephemeral::create(['mail.enabled' => true, 'mail.provider' => 'builtin']);
			$domain = $acct->getContext()->domain;
			$username = $acct->getContext()->username;
			$afi = $acct->getApnscpFunctionInterceptor();
			if (!$afi->email_transport_exists($domain)) {
				$this->assertTrue($afi->email_add_virtual_transport($domain), 'Create mail transport');
			}

			if (!$afi->email_address_exists($username, $domain)) {
				$this->assertTrue($afi->email_add_mailbox($username, $domain, $afi->user_get_uid_from_username($username)));
			}

			$home = $afi->user_get_home();
			$path = $acct->getContext()->domain_fs_path($home . "/" . \Opcenter\Mail\Storage::MAILDIR_HOME . '/new');
			$this->assertEmpty(glob($path . '/*'));
			mail(
				"{$username}@{$domain}",
				"ApisCP delivery test",
				"Created from the unit testing framework.\n\nHope it works!"
			);
			for ($i = 0; $i < 30; $i++, sleep(1)) {
				$files = glob($path . '/*');
				if (\count($files) > 0) {
					$this->assertStringContainsString("delivery test", file_get_contents($files[0]));
					return true;
				}
			}
			$this->fail("Failed to receive email in timely fashion");

		}

	}

