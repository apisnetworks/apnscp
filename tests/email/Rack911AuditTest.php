<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class Rack911AuditTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 *
		 */
		public function testAddressManipulationDomain()
		{
			$auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$afi1 = apnscpFunctionInterceptor::factory($auth1);
			$auth2 = \TestHelpers::create(array_get(Definitions::get(), 'auth.contextable.domain'));
			$afi2 = apnscpFunctionInterceptor::factory($auth2);

			if (!$afi1->email_transport_exists($auth1->domain)) {
				return $this->markTestIncomplete("Base domain {$auth1->domain} missing email support");
			} else if (!$afi2->email_transport_exists($auth2->domain)) {
				return $this->markTestIncomplete("Contexted domain {$auth2->domain} missing email support");
			}
			$testaddr = strtolower(\Opcenter\Auth\Password::generate(16));
			defer($_, static function () use ($testaddr, $auth1, $afi1) {
				if ($afi1->email_address_exists("{$testaddr}-ren", $auth1->domain)) {
					$afi1->delete_mailbox("{$testaddr}-ren", $auth1->domain);
				}
			});
			foreach ([
				'email_add_address'    => [$testaddr, $auth2->domain, $auth1->user_id],
				'email_add_alias'      => [$testaddr, $auth2->domain, 'remote@address.com'],
				'email_add_address'    => [$testaddr, $auth1->domain, $auth1->user_id],
				'email_modify_mailbox' => [$testaddr, $auth1->domain, "{$testaddr}-ren", $auth2->domain],
			 ] as $fn => $args) {
				try {
					$afi1->{$fn}(...$args);
				} catch (\apnscpException $e) {
					$this->assertTrue(true, "{$fn} generated error");
					continue;
				}
				$this->fail("{$fn} succeeded and must fail");
			}

		}

	}

