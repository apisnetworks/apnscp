<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class ContextabilityConcurrencyTest extends TestFramework
    {
        protected $filename;

		/**
		 *
		 */
		public function testContextConcurrency()
		{
			$auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$afi1 = apnscpFunctionInterceptor::factory($auth1);

			$auth2 = \TestHelpers::create(array_get(Definitions::get(), 'auth.contextable.domain'));
			$afi2 = apnscpFunctionInterceptor::factory($auth2);
			$this->assertNotEquals(spl_object_hash($afi1), spl_object_hash($afi2));
			$this->assertNotEquals($auth1->id, $auth2->id);
			$this->assertEquals($auth1->level, $auth2->level);
			$this->assertTrue($afi2->context_matches_id($auth2->id));
			$this->assertFalse($afi2->context_matches_id($auth1->id));

			$this->assertNotEquals($auth1->username, $auth2->username);
		}
    }