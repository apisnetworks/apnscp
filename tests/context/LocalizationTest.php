<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class LocalizationTest extends TestFramework
	{
		private array $tzPairs;
		private array $langPairs;

		protected function setUp(): void
		{
			parent::setUp();
			$this->context = TestHelpers::with([
				'crontab.enabled' => true,
				'apache.enabled'  => true
			]);
			$this->setApnscpFunctionInterceptor(\a23r::factory($this->context));

			$oldTz = $this->common_get_timezone();
			do {
				$newTz = DateTimeZone::listIdentifiers()[array_rand(DateTimeZone::listIdentifiers())];
				$delta = (new DateTime)->setTimezone(new DateTimeZone($newTz))->getOffset() - (new DateTime)->setTimezone(new DateTimeZone($oldTz))->getOffset();
			} while (abs($delta) < 60*60);

			$this->tzPairs = ['old' => $oldTz, 'new' => $newTz, 'delta' => $delta];

			$this->langPairs = [
				'old' => $this->common_get_language()
			];
			do {
				$newLang = ResourceBundle::getLocales('')[array_rand(ResourceBundle::getLocales(''))];
			} while (!setlocale(LC_ALL, $newLang));

			$this->langPairs['new'] = $newLang;
		}

		protected function tearDown(): void
		{
			$this->common_set_language($this->langPairs['old']);
			$this->common_set_timezone($this->tzPairs['old']);
			parent::tearDown();
		}

		public function testTimezoneBackend()
		{
			$this->assertEquals($this->tzPairs['old'], $this->pman_run('echo -n $TZ')['stdout']);
			$oldTime = $this->pman_run('echo -n $(date "+%%d%%H")')['stdout'];
			$this->common_set_timezone($this->tzPairs['new']);
			$newLines = explode("\n", $this->pman_run('echo $TZ; zsh -c \'echo -n $(date "+%%d%%H")\'')['stdout']);
			$this->assertStringContainsString($this->tzPairs['new'], $newLines[0]);
			$this->assertNotEquals($oldTime, $newLines[1]);
		}

		public function testContextTimezone()
		{
			$this->assertSame($this->tzPairs['old'], $this->context->timezone);
			$this->common_set_timezone($this->tzPairs['new']);
			$this->assertSame($this->tzPairs['new'], $this->context->timezone);
			$this->assertSame($this->common_get_timezone(), $this->context->timezone);
		}

		public function testUiConcurrentUpdate()
		{
			$preferences = \Preferences::factory($this->context);
			\Auth::autoload()->setID($this->context->id)->authInfo(true);
			/** @var \apps\changeinfo\Page $app */
			$app = Page_Container::initApp('changeinfo');
			$this->assertEquals($this->tzPairs['old'], $app->getMyTimezone());
			$this->assertEquals($this->langPairs['old'], $app->getMyLanguage());
			$this->assertTrue($app->setTimezone($this->tzPairs['new']));
			$this->assertTrue($app->setLanguage($this->langPairs['new']));
			$this->assertEquals($this->tzPairs['new'], $app->getMyTimezone());

			// simulate clobbered frontend
			$preferences->sync(true);
			$this->assertEquals($this->tzPairs['new'], $this->common_load_preferences()['timezone']);
			$this->assertEquals($this->langPairs['new'], $this->common_load_preferences()['language']);

			/** @var \apps\changeinfo\Page $app */
			\Auth::autoload()->setID($this->context->id)->authInfo(true);
			$app = Page_Container::initApp('changeinfo');
			$this->assertEquals($this->tzPairs['new'], $app->getMyTimezone());
			$this->assertEquals($this->langPairs['new'], $app->getMyLanguage());
		}

		/**
		 * Setting another language for a secondary user
		 */
		public function testLanguageSwitch()
		{
			$context = $this->context;
			$defaultLanguage = array_get(\Preferences::factory($context), 'language', 'en_US');
			$otherLanguage = $defaultLanguage === 'en_US' ? 'en_UK' : 'en_US';
			$context2 = TestHelpers::createUser($context->getAccount());
			$this->assertNotEmpty($context2);
			$afi2 = \apnscpFunctionInterceptor::factory($context2);
			$prefs = \Preferences::factory($context2);
			$prefs->unlock($afi2);
			$prefs['language'] = $otherLanguage;
			$prefs->sync();
			$this->assertEquals($otherLanguage, $afi2->common_get_language(),
				'Language correctly set for contexted user');
		}
	}