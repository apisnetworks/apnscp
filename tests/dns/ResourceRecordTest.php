<?php
	require_once dirname(__DIR__) . '/TestFramework.php';
	require_once(__DIR__ . '/helpers/RecordFramework.php');

	class ResourceRecordTest extends TestFramework
	{
		protected $filename;

		public function testProviders()
		{
			$defaultdomain = array_get(Definitions::get(), 'auth.site.domain');
			$providers = $this->getProviders();
			if (!$providers) {
				$this->markTestSkipped('No DNS providers to test against');
			}
			foreach ($providers as $provider) {
				$domain = array_get(Definitions::get(), "dns.{$provider}.domain", $defaultdomain);
				$auth = TestHelpers::create($domain);
				$framework = new RecordFramework($auth, $provider);
				$oldAfi = \apnscpFunctionInterceptor::factory($auth);
				foreach ($framework->getRecords() as $record) {
					if ($record === 'SOA') {
						// @TODO rewrite record tests to individual units
						$framework->setApnscpFunctionInterceptor(
							\apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login()))
						);
					}
					$framework->testRecord($record);

					if ($record === 'SOA') {
						$framework->setApnscpFunctionInterceptor($oldAfi);
					}
				}
				try {
					$framework->testMultipleRecordValues();
					$framework->testWeakMatchRemoval();
					$framework->testNonSpecificCheck();
					$framework->testReset();
				} catch (\PHPUnit\Framework\SkippedTestError|\PHPUnit\Framework\IncompleteTestError $e) {
					continue;
				}
			}
		}

		private function getProviders() {
			return array_get(Definitions::get(), 'dns.provider_list', \Opcenter\Dns::providers());
		}
	}