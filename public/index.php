<?php
if (!isset($_SERVER['REQUEST_URI'])) {
    die("OK.");
}

define('INCLUDE_PATH', realpath(__DIR__ . '/..'));
$debug = 0&extension_loaded('xdebug');
if ($debug) $__pr_start_time = microtime(true);

if ($_SERVER['SERVER_PORT'] == 2077 || $_SERVER['SERVER_PORT'] == 2078) {
	include(INCLUDE_PATH. '/dav/index.php');
	exit;
}

if (!defined('IS_ISAPI'))
	define('IS_ISAPI', 1);

include(INCLUDE_PATH."/lib/apnscpcore.php");

if (FRONTEND_CONTENT_SECURITY_POLICY_REPORT_ONLY) {
	header('Content-Security-Policy-Report-Only: ' . FRONTEND_CONTENT_SECURITY_POLICY_REPORT_ONLY, false);
}

$url = Template_Engine::url();
$req_page = null;

if (0 === strpos($url, '/apps/') && isset($url[7])) {
	// these should be moved to public/assets/xyz in the future
	$end = strpos($url,'/',7);
	if ($end) $req_page = substr($url,6, $end-6);
	else      $req_page = basename($url,'.php');
}
// default page
if (empty($req_page)) {
	$url = \Auth::authenticated() ? Template_Engine::init()->getEntryLocation() : \Frontend\Multipath::strip(Template_Engine::init()->getPathFromApp('login'));
	header('Location: ' . $url);
	exit;
}

include(INCLUDE_PATH."/lib/html/tip_engine.php");
// OPCache conflict? Needs to move over to namespace + autoload
// OPCache conflict? Needs to move over to namespace + autoload

include(INCLUDE_PATH."/lib/html/page_container.php");
include(INCLUDE_PATH."/lib/html/page_renderer.php");

$file = \Page_Container::resolve($req_page);

if (null === $file) {
	if (!Auth::authenticated()) {
		\Auth_Anvil::anvil();
		header('Status: 307 Temporary Redirect',true,307);
		header('Location: ' . \Frontend\Multipath::strip(Template_Engine::init()->getPathFromApp('login')) ,true,307);
		exit();
	}
	http_response_code(404);
} else {
	include_once($file);
	// autovivify afi in init() now
	\apnscpFunctionInterceptor::init(true);
	include(INCLUDE_PATH.'/lib/html/stats_logger.php');
}

if (!defined('NO_AUTH') && $req_page === 'login' || $req_page === 'logout' || $req_page === 'forgot_info') {
	// user overrode app, nag for now
	debug("Update login.php controller");
	define('NO_AUTH', 1);
}

Auth::handle();
/**
 * deny access if app is not defined in lib/html/templateconfig-<role>.php
 */
if (!NO_AUTH && !Template_Engine::init()->user_permitted($url)) {
	http_response_code(404);
} else {
	\Auth::init_hook();
}

if (http_response_code() >= 400) {
	$req_page = 'error';
	$file = \Page_Container::resolve($req_page);
	include_once($file);
}

include(INCLUDE_PATH."/lib/html/js_template.php");
if (!NO_AUTH && \Auth::authenticated() && $req_page !== 'error') {
	$UCard = UCard::init();
	$pagecnt = (int)$UCard->getPref2('pageview',$req_page);
	if ($pagecnt < 1) {
		Session::set("first.{$req_page}", true);
	}
	$UCard->setPref2('pageview',$req_page,++$pagecnt);
}

$kernel = Page_Container::kernelFromApp($req_page);

try {
	$Page = Page_Container::init($kernel);
	$Page->handle_request();
} catch (\Throwable $e) {
	$Page = Page_Container::init(\apps\error\Page::class, \Auth::profile(), [550]);
	$Page->setException($e);
	$Page->handle_request();
	\Error_Reporter::handle_exception($e);
}
Auth::end_hook();

if (is_debug() && $debug) {
	$__pr_elapsed_time = microtime(true)-$__pr_start_time;
	if ($__pr_elapsed_time >= 5) {
		print "Time spent: ".$__pr_elapsed_time;
	}
}