<?php
if (!isset($_GET['engine'])) {
    print json_encode(false);
    exit;
}
if (!defined('INCLUDE_PATH')) {
	define('INCLUDE_PATH',realpath(__DIR__ .'/../'));
}
include(INCLUDE_PATH.'/lib/apnscpcore.php');
include(INCLUDE_PATH . '/lib/html/ajax/functions.php');
//Error_Reporter::set_verbose(0);
\Auth::handle();
// verify session to avoid useless _initializeUser()
// bug reports
if (!Auth::authenticated() && (!isset($_GET['app']) || $_GET['app'] != "login"))  {
	// add a special case to let AJAX pw reset requests to go through
	// even if unauthenticated
	return error("session invalid");
}
\apnscpFunctionInterceptor::init(true);
$engine = $_GET['engine'];
unset($_GET['engine']);
switch ($engine) {
	case 'app':
        $type = 'json';
		$response = dispatch_app();
        if (isset($_POST['type'])) {
            $type = $_POST['type'];
        }
        /**
         * jQuery output types
         * @link http://api.jquery.com/jQuery.ajax/ cf. dataType
         */
        switch ($type) {
            case 'json':
		        header('Content-type: application/json', true);
	            print json_encode($response);
		        break;
            case 'jsonp':
                header('Content-type: application/javascript', true);
				print json_encode($response);
                break;
            case 'text':
            case 'script':
            case 'html':
            	if (is_array($response)) {
            		\Error_Reporter::report(var_export($response, true));
	            }
                print $response;
                break;
            case 'xml':
                if ($response instanceof SimpleXMLElement) {
                    $response = $response->asXML();
                }
                print $response;
                break;
            default:
                print "unknown encoding type " . $_GET['type'];
                break;
        }
        return;
	case 'postback':
		$app = get_app();
		if (!ctype_alpha($app)) return false;
		unset($_GET['app'], $engine);
		$params = array_merge($_GET, $_POST);
		print json_encode(postback_app($app, $params));
		return;
    case 'tail':
		$file = $_GET['file'];
	    if ($file[0] == '.' || $file[0] == '/' || !ctype_alnum($file)) {
		    return error("invalid tee file `%s'", $file);
	    }
	    $file = sys_get_temp_dir() . '/' . $file;
    	print json_encode(\Util_Process_Tee::read($file, $_GET['offset']));
    	return;
    case 'file_browser':
        break;
    case 'cmd':
    	include(INCLUDE_PATH.'/lib/html/ajax_cmd.php');
    	return;
    case 'ucard':
    	include_once(INCLUDE_PATH.'/lib/html/UCard.php');
    	print json_encode(dispatch_ucard($_GET['action']));
    	break;
    case 'tip':
		include(INCLUDE_PATH.'/lib/html/tip_engine.php');
	    print json_encode(Tip_Engine::get($_GET['tip'], get_app()));
	    return;
    default:
		header('Content-type: application/json', true);
		print json_encode(false);
        exit;
}
if (file_exists(INCLUDE_PATH.'/lib/html/ajax/'.$engine.'.php'))
	include(INCLUDE_PATH.'/lib/html/ajax/'.$engine.'.php');