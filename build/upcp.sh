#!/bin/bash
# shellcheck disable=SC1091
test -f /etc/sysconfig/apnscp && . /etc/sysconfig/apnscp

OPTS="$(getopt -o fablnswvm: --long flare,skip-code,bootstrap,list,auto,no-migrate,migrate:,reset,wait,force,var: -n 'upcp' -- "$@")"
if test $? -ne 0; then
	exit 1
fi
eval set -- "$OPTS"

APNSCP_ROOT="${APNSCP_ROOT:-"$(realpath "$(dirname "$(realpath "$0")")"/../)"}"

# Update code before running other tasks
SKIP_CODE=false
# Run Bootstrapper automatically
BOOTSTRAP=false
# Perform migration automatically
MIGRATE=auto
# Panel upgrade policy. Override in /etc/sysconfig/apnscp.
# Allow override via UPDATE_POLICY environment variable
UPDATE_POLICY=${UPDATE_POLICY:-${APNSCP_UPDATE_POLICY-edge}}
# Enable Bootstrapper singleton check
BSCHECK=${BSCHECK:-true}
# Set force=true to Bootstrapper, run upcp --reset before updating
FORCE=${FORCE:-false}
# Username to run as update
APNSCP_UPDATE_USER="${APNSCP_UPDATE_USER:-${APNSCP_SYSTEM_USER:-apnscp}}"
# Verbose flag count
VERBOSE=""
# Log update effort. Enabled on unassisted updates
LOG_UPDATE=${LOG_UPDATE:-$([[ $# -eq 1 ]] && ! test -t 1 && echo true)}
# Log file for update, by default use failure tracking log
LOGFILE="${LOGFILE:-${APNSCP_ROOT}/storage/.upcp.failure}"
# Composer location
COMPOSERLOCK="$APNSCP_ROOT/composer.lock"
# Upstream name
UPSTREAM="${ORIGIN:-origin}"
# Branch to pull from for non-tagged releases
BRANCH="${BRANCH:-}"
# FLARE check URL
APNSCP_FLARE_URL="${APNSCP_FLARE_URL:-https://flare.apnscp.com}"
# edge-major lockfile
EDGE_LOCK="$APNSCP_ROOT/storage/.edge.lock"
# Ansible lock
PLAYBOOK_LOCK="${APNSCP_ROOT}/storage/run/.ansible.lock"
# Duration in hours to abstain from update if another update has been released in this timeframe
UPDATE_OFFSET="${APNSCP_UPDATE_OFFSET:-0}"

# Doesn't pollute /var/log/secure using PAM
SUIDWRAPPER="/usr/bin/setpriv --nnp --clear-groups --reuid $(id -u "$APNSCP_UPDATE_USER") --regid $(id -g "$APNSCP_UPDATE_USER")"

ABORT=""
AGENT_SPAWN=""
# Bootstrapper vars
declare -a BSVARS=()
# shellcheck disable=SC2139
git() {
	local RET
	if test -z "$SSH_AGENT_PID" || ! ps -p "$SSH_AGENT_PID" >/dev/null; then
		mkdir "${APNSCP_ROOT}/storage/tmp/agent-root"
		chown root:"${APNSCP_UPDATE_USER}" "${APNSCP_ROOT}/storage/tmp/agent-root"
		# Disallow directory enumeration for APNSCP_UPDATE_USER
		chmod 0730 "${APNSCP_ROOT}/storage/tmp/agent-root"
		# shellcheck disable=SC2086
		eval "$(env TMPDIR="${APNSCP_ROOT}/storage/tmp/agent-root" $SUIDWRAPPER ssh-agent -s | grep -v '^echo')"
		export SSH_AUTH_SOCK
		AGENT_SPAWN=$SSH_AUTH_SOCK
		# export root in case needed for git + ssh
		ssh-add 2>/dev/null
	fi
	$SUIDWRAPPER env HOME="${APNSCP_ROOT}" git "$@"
	RET=$?
	if test $RET -eq 128 && test -z "$ABORT"; then
		ABORT=1
		echo "Reasserting permissions" >&2
		assert_permissions
		git "$@"
		RET=$?
	fi
	return $RET
}

if test $EUID -ne 0; then
	echo "This script must be run as root" >&2
	exit 1
fi


# Dearest bash, why must we mix exit codes and booleans
as_boolean() {
	case "${1,,}" in
	false | 0 | "")
		echo false
		return 1
		;;
	*)
		echo true
		return 0
		;;
	esac
}

pushd "$APNSCP_ROOT" >/dev/null || exit

cleanup_agent() {
	if [[ -n "$AGENT_SPAWN" ]]; then
		eval "$(ssh-agent -k | grep -v '^echo')"
	fi
	rm -rf "${APNSCP_ROOT}/storage/tmp/agent-root/"
}
# Post-update cleanup
cleanup() {
	cleanup_agent

	[[ ! -f "$LOGFILE" ]] && exit 0
	/usr/bin/apnscp_php "${APNSCP_ROOT}/bin/cmd" misc:notify-update-failure >/dev/null 2>&1 || true
	exit 1
}

trap cleanup 0
if as_boolean "$LOG_UPDATE" >/dev/null; then
	exec &> >(/bin/tee -a "$LOGFILE")
fi

# Check if wrapped git is possible
git --version >/dev/null
STATUS=$?
# Something's really bad, abort wrapper usage
[[ $STATUS -ne 0 ]] && echo "ERR: git wrapper defective - disabling usage" && unset -f git

# Determine if emergency update pushed
# $? is 0 if FLARE update received
# $? is 1 if no FLARE update received
flare_check() {
	# Remove cleanup trap
	trap cleanup_agent 0
	local REPOTS FLARETS FLAREMARKER
	declare -i REPOTS

	FLAREMARKER="${APNSCP_ROOT}/.git/.flare-check"
	REPOTS=999999999999999999
	if test -f "$FLAREMARKER"; then
		# 15 minute buffer just in case
		REPOTS="$(stat -c "%Y" "${FLAREMARKER}")"
	fi
	curl -o "${FLAREMARKER}" -s --cacert "${APNSCP_ROOT}/resources/apnscp.ca" \
		--cert "${APNSCP_ROOT}/config/license.pem" "${APNSCP_FLARE_URL}"
	# Failed check
	test $? -ne 0 && exit 0
	touch "${FLAREMARKER}"
	# First argument TS, second perform BSCHECK (default true)
	IFS=" " read -r -a FLARETS < "${FLAREMARKER}"

	test "$REPOTS" -le "${FLARETS[0]}"
	FLAREUPDATE=$?
	if [[ $FLAREUPDATE -eq 0 ]]; then
		# Allow multiple upcp proceses to run if one deadlocked
		# See 3.2.30.2/Dovecot 2.3 release tandem
		[[ -n "${FLARETS[1]}" ]] && [[ -f "$PLAYBOOK_LOCK" ]] && mv "$PLAYBOOK_LOCK" "${PLAYBOOK_LOCK}.old"
	fi
	exit $FLAREUPDATE
}

bootstrapper_running() {
	# Bypass Bootstrapper concurrency check for simple update
	if ! as_boolean "$BSCHECK" >/dev/null; then
		return 1
	fi
	# check if Bootstrapper running, restarting apnscp will
	local PLAYBOOK_LOCK PLAYBOOK_PID PROCNAME

	if [[ ! -f "$PLAYBOOK_LOCK" ]]; then
		return 1
	fi
	PLAYBOOK_PID="$(cat "${PLAYBOOK_LOCK}")"
	if [[ ! -d "/proc/${PLAYBOOK_PID}" ]]; then
		return 1
	fi
	PROCNAME="$(cat /proc/"${PLAYBOOK_PID}"/comm)" || true
	[[ "$PROCNAME" == ansible-* ]] && echo "$PLAYBOOK_PID"
	return $?
}

vercomp() {
	if [[ "$1" == "$2" ]]; then
		return 0
	fi

	IFS=. read -r -a ver1 <<<"$1"
	IFS=. read -r -a ver2 <<<"$2"
	# fill empty fields in ver1 with zeros
	for ((i = ${#ver1[@]}; i < ${#ver2[@]}; i++)); do
		ver1[i]=0
	done
	for ((i = 0; i < ${#ver1[@]}; i++)); do
		if [[ -z ${ver2[i]} ]]; then
			# fill empty fields in ver2 with zeros
			ver2[i]=0
		fi
		if ((10#${ver1[i]} > 10#${ver2[i]})); then
			return 1
		fi
		if ((10#${ver1[i]} < 10#${ver2[i]})); then
			return 2
		fi
	done
	return 0
}

fetch_releases() {
	if "$SKIP_CODE"; then
		return 1
	fi

	if test "$UPDATE_POLICY" == "edge" -o \
		"$UPDATE_POLICY" == "edge-major"; then
		return 0
	fi
	# Calc current tag
	# Version compare
	git fetch --tags -f
}

do_upgrade() {
	if $SKIP_CODE; then
		return 1
	fi

	if $FORCE; then
		git reset --soft "$(git merge-base HEAD "$UPSTREAM"/"${BRANCH:-master}")"
		reset_repo
	fi

	if test "$UPDATE_POLICY" == "edge"; then
		return 0
	elif test "$UPDATE_POLICY" != "all" -a \
		"$UPDATE_POLICY" != "major" -a \
		"$UPDATE_POLICY" != "minor"; then
		return 1
	fi

	walk_releases
}

walk_releases() {
	mapfile -t TAGS < <(git for-each-ref --sort=taggerdate --format '%(tag)' refs/tags | grep '^v')
	MYTAG="$(git describe 2> /dev/null)"

	if [[ "$MYTAG" =~ -.*$ ]]; then
		# Moving from edge to tagged releases cycles to first minor release
		# Remove commit meta, e.g. "<version>-8-gcb0f323", to avoid this long trek back
		echo "${MYTAG%%-*}"
		return 0
	fi

	if test -n "$MYTAG" -a "${MYTAG:0:1}" != "v"; then
		# upcp can pull panel off a custom tag, e.g. "benchmark"
		# @TODO use git merge-base (fallback timestamp of lservicelib.pht)
		# to determine commit ancestor to avoid replaying all tags
		MYTAG=${TAGS[0]}
	elif test -z "$MYTAG"; then
		# No common ancestor in tag history, this can arise because of a shallow
		# commit history. Treat as edge until next release comes out.
		return 0
	fi

	MYTAGCOMP="${MYTAG%%.*}"
	if test "$UPDATE_POLICY" == "minor"; then
		MYTAGCOMP="$(expr "$MYTAG" : '^\(v[1-9][0-9]*\.[0-9][0-9]*\)')"
	fi

	for TAG in "${TAGS[@]}"; do
		if test "$UPDATE_POLICY" == "major" && test "${TAG%%.*}" != "$MYTAGCOMP"; then
			continue
		elif test "$UPDATE_POLICY" == "minor" && test "$(expr "$TAG" : '^\(v[1-9][0-9]*\.[0-9][0-9]*\)')" != "$MYTAGCOMP"; then
			continue
		fi
		vercomp "${TAG:1}" "${MYTAG:1}"
		if test $? -eq 1; then
			break
		fi
	done
	echo "$TAG"
	test "${TAG}" != "${MYTAG}"
}

update_cured() {
	local TAGFLAGS=""

	if as_boolean "${1:-false}" > /dev/null; then
		TAGFLAGS=(--tags --no-walk)
	fi

	git fetch --recurse-submodules=on-demand
	test "$UPDATE_OFFSET" -eq 0 || \
		/bin/sh -c "git log -n1 --format=oneline ${TAGFLAGS[*]} --since=\"${UPDATE_OFFSET} hours\" ..FETCH_HEAD | grep -vq '.'"
}

# Commit has met requisite version
met_version() {
	# Version locked from
	# must be formatted as vx.y.z
	local WHENCE="$1"
	# Current tag/commit
	local MYTAG
	MYTAG="$(git describe --abbrev=0 2>/dev/null)"
	if test "$MYTAG" == ""; then
		return 1
	fi

	if ! test "${WHENCE:0:1}" == "v"; then
		return 1
	fi

	# Ignore hotfixes
	if test "$(expr "$MYTAG" : '\(^v[1-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$\)')" != "$MYTAG" ; then
		return 1
	fi

	MYTAG="${MYTAG%%-*}"
	vercomp "${WHENCE:1}" "${MYTAG:1}"
	RES=$?
	test "$RES" -eq 2
}

cur_branch() {
	local BRANCH
	BRANCH="$(git rev-parse --abbrev-ref HEAD)"
	# update policy is major/minor, uses version tag
	test "$BRANCH" == "HEAD" && BRANCH="master"
	echo "$BRANCH"
}

cur_commit() {
	git rev-parse HEAD
}

assert_permissions() {
	find "${APNSCP_ROOT}" -user "$(whoami)" -fprintf /dev/stderr "%P\n" -exec chown "${APNSCP_UPDATE_USER}:${APNSCP_UPDATE_USER}" '{}' +
}

reset_repo() {
	BRANCH=$(cur_branch)
	COMPOSERTS=$(stat -c "%Y" "$COMPOSERLOCK" 2>/dev/null)
	git fetch && git submodule foreach git fetch
	git reset --hard "$UPSTREAM"/"$BRANCH" && git submodule foreach git reset --hard "$UPSTREAM"/"$BRANCH"
	RET=$?
	update_composer "$COMPOSERTS"
	assert_permissions
	! bootstrapper_running && pushd "${APNSCP_ROOT}" >/dev/null && ./artisan ${VERBOSE:+-$VERBOSE} migrate --force
	return $RET
}

update_composer() {
	COMPOSERTS=$1
	if test "$COMPOSERTS" == "$(stat -c "%Y" "$COMPOSERLOCK")"; then
		return 1
	fi
	COMPOSER_FLAGS=""
	[[ ! -t 1 ]] && COMPOSER_FLAGS="--no-progress"
	chown "$APNSCP_UPDATE_USER" "$COMPOSERLOCK"
	$SUIDWRAPPER env COMPOSER_HOME="$APNSCP_ROOT/storage/.composer" /usr/bin/apnscp_php /usr/bin/composer $COMPOSER_FLAGS install
	rm -f "$APNSCP_ROOT"/storage/cache/{services,packages}.php
	return 0
}

migrate_pending() {
	pushd "${APNSCP_ROOT}" >/dev/null && ./artisan ${VERBOSE:+-$VERBOSE} migrate --force
}

has_pending_migrations() {
	/usr/bin/apnscp_php "${APNSCP_ROOT}/artisan" ${VERBOSE:+-$VERBOSE} migrate:status --pending | grep -q "${APNSCP_ROOT}"
}

while true; do
	case "$1" in
	--flare)
		flare_check
		;;
	-n | --no-migrate)
		MIGRATE=false
		shift
		;;
	-b | --bootstrap)
		if test "$MIGRATE" == "true" && test -n "${#MIGRATIONS[@]}" -gt 0; then
			echo "Both -m and -b may not be set" >&2
			exit 1
		fi
		BOOTSTRAP=true
		shift
		;;
	-s | --skip-code)
		SKIP_CODE=true
		MIGRATE=false
		shift
		;;
	--var)
		shift
		BSVARS+=("$1")
		shift
		;;
	-m | --migrate)
		if "$BOOTSTRAP"; then
			echo "Both -m and -b may not be set" >&2
			exit 1
		fi
		MIGRATE=true
		SKIP_CODE=true
		shift
		IFS=", " read -r -a RAW <<< "$1"
		declare -a MIGRATIONS=()
		for file in "${RAW[@]}"; do
			read -d $'\0' -ra IN <<< "$(find "${APNSCP_ROOT}/resources/playbooks/migrations/" -iname "$file" -printf "%P\n" ; echo $'\0')"
			MIGRATIONS+=("${IN[@]}")
		done
		printf "%s\n" "${MIGRATIONS[@]}"
		shift
		;;
	-l | --list)
		grep -E '^\s+- [a-z][^/]+/' "${APNSCP_ROOT}/resources/playbooks/bootstrap.yml" | cut -d- --complement -f1 | tr -d ' ' | sed -e 's!^role:!!g' | sort
		exit 0
		;;
	-v )
		VERBOSE="${VERBOSE}v"
		[[ $VERBOSE == "vvv"* ]] && ANSIBLE_STDOUT_CALLBACK="dense"
		[[ $VERBOSE == "vv"   ]] && ANSIBLE_STDOUT_CALLBACK="debug"
		[[ $VERBOSE == "v"   ]]  && ANSIBLE_STDOUT_CALLBACK="default"
		shift
		;;
	-a | --auto)
		BOOTSTRAP=auto
		shift
		;;
	-w | --wait)
		! bootstrapper_running && exit 1
		while : ; do
			sleep 1
			! bootstrapper_running && exit 0
		done
		;;
	--reset)
		reset_repo
		exit $?
		;;
	-f | --force)
		FORCE=true
		shift
		;;
	--)
		shift
		break
		;;
	*) break ;;
	esac
done

BOOTSTRAPPER_PID="$(bootstrapper_running)"
RET=$?
if [[ $RET -eq 0 ]]; then
	echo "Bootstrapper running with PID ${BOOTSTRAPPER_PID}. upcp will not run until this completes."
	exit 1
fi

if test "$MIGRATE" == "auto" -o "$MIGRATE" && "$SKIP_CODE" && test "${#MIGRATIONS[@]}" -gt 0; then
	pushd "${APNSCP_ROOT}/resources/playbooks" >/dev/null || (echo "Failed to chdir" && exit 1)
	# shellcheck disable=SC2086
	ANSIBLE_STDOUT_CALLBACK="${ANSIBLE_STDOUT_CALLBACK:-actionable}" ansible-playbook ${VERBOSE:+-$VERBOSE} -c local migrator.yml --tags=up --extra-vars="migrations=\"$(IFS=',' ; echo "${MIGRATIONS[*]}")\"" ${BSARGS:-}
	RET=$?
	popd >/dev/null || exit $RET
	exit $RET
fi

$SUIDWRAPPER /bin/sh -c '[[ -w '"${APNSCP_ROOT}/.git/FETCH_HEAD"' ]] && [[ -w '"${APNSCP_ROOT}/.git/objects"' ]]' \
	|| assert_permissions

if test "$UPDATE_POLICY" != "edge" && has_pending_migrations && ! "$FORCE" && ! migrate_pending; then
	# If a migration fails because DB connection is clipped or transient outage, let's try again silently
	echo "Migrations have not completed yet. Update cannot proceed!"
	echo "Run the following command to apply migrations, correcting failure as necessary:"
	echo ""
	echo "cd /usr/local/apnscp && ./artisan migrate --force"
	echo ""
	echo "To force an update to latest code, run the following command:"
	echo ""
	echo "cd /usr/local/apnscp && upcp -f && ./artisan migrate --force"
	exit 1
fi

fetch_releases
TAG="$(do_upgrade)"
TRUTHY=$?
# Fallback to edge if we can't ascertain
if test -z "$TAG" && test $TRUTHY -eq 0; then
	UPDATE_POLICY="edge"
fi

# Update loop
# Always process updates in case a migration gets amended at a later release
LOOPOK=0

# Incrementally run upgrades if behind
while test "$TRUTHY" -eq 0; do
	OLDHASH="$(cur_commit)"
	COMPOSERTS=$(stat -c "%Y" "$COMPOSERLOCK" 2>/dev/null)
	if test "${UPDATE_POLICY}" == "edge"; then
		if test -z "$BRANCH"; then
			BRANCH="$(cur_branch)"
		fi

		RET=-1
		if test -f "$EDGE_LOCK" || update_cured; then
			UPSTREAM_REF="$(git for-each-ref --format='%(upstream)' | grep -m1 '^refs/')"
			LC=$(($(git rev-parse HEAD "${UPSTREAM_REF:-$BRANCH}" | uniq | wc -l)))
			RET=$?

			if [[ $LC -ne 1 ]]; then
				# Emulate original git-pull.sh
				COMMITISH="$(grep -m1 -v $'\tnot-for-merge\t' .git/FETCH_HEAD | cut -f1)"
				[[ -z $COMMITISH ]] && echo "No branch marked for merge" && exit 1
				git merge --no-edit FETCH_HEAD
				RET=$?
			fi

			git submodule update --merge
			# Revert to major if limit-edge set
			if test -f "$EDGE_LOCK" && met_version "$(cat "$EDGE_LOCK")"; then
				/usr/bin/apnscp_php bin/cmd scope:set cp.update-policy major 2>&1 || true
				rm -f "$EDGE_LOCK"
			fi
		fi
	elif ! update_cured true; then
		RET=-1
	else
		echo "Updating to ${TAG}"
		git checkout "${TAG}"
		RET=$?
	fi

	case $RET in
		-1)
			echo "Updates available but offset condition not met"
			rm -f "$LOGFILE"
			exit 0
			;;
		0)
			;;
		*)
			echo "upcp halted due to errors"
			exit 1
			;;
	esac

	git submodule update --init --recursive --checkout .
	if test -d lib/modules/surrogates; then
		pushd lib/modules/surrogates || exit 1
		[[ -d .git ]] && git pull
		popd || exit 1
	fi

	[[ "$(type -t git)" == "file" ]] && assert_permissions

	update_composer "$COMPOSERTS"

	/usr/bin/apnscp_php bin/cmd misc_flush_cp_version >/dev/null 2>&1 || true
	/usr/bin/apnscp_php "${APNSCP_ROOT}/artisan" ${VERBOSE:+-$VERBOSE} clear-compiled
	systemctl restart apiscp

	if test "$MIGRATE" == "auto" || "$MIGRATE" ; then
		migrate_pending
		RET=$?
		LOOPOK=$( ([[ $RET -eq 0 ]] && [[ $LOOPOK -eq 0 ]] && echo 0) || echo 1)
	fi

	# edge updates will always be mainline
	if test "$UPDATE_POLICY" == "edge"; then
		break
	elif test "$OLDHASH" == "$(cur_commit)"; then
		# Sanity check to ensure it's updated
		echo "Upgrade requested, but old = new ($OLDHASH)"
		break
	fi

	TAG="$(walk_releases)"
	TRUTHY=$?
done

[[ $LOOPOK -ne 0 ]] && exit 1

if test "$BOOTSTRAP" == "auto"; then
	BOOTSTRAP=false
	git log --name-only --pretty=oneline --full-index HEAD~..HEAD |
		grep -v "resources/playbooks/migrations" | grep -q "resources/playbooks"
	if test $? -eq 0; then
		BOOTSTRAP=true
	fi
fi

RET=0

if "$BOOTSTRAP"; then
	pushd "${APNSCP_ROOT}"/resources/playbooks >/dev/null || exit 1
	BSTAGS=""
	if $FORCE; then
		BSVARS+=(force=true)
	fi

	for var in "${BSVARS[@]}"; do
		BSARGS+=("$(printf -- --extra-vars='%q' "$var")")
	done

	if test "$#" -gt 0; then
		BSTAGS=$(printf ",%s" "${@}")
		BSTAGS="--tags=${BSTAGS:1}"
	fi

	# nest for proper argument parsing
	/bin/sh -c "ANSIBLE_LOG_PATH=${APNSCP_ROOT}/storage/logs/bootstrapper.log ANSIBLE_STDOUT_CALLBACK=${ANSIBLE_STDOUT_CALLBACK:-actionable} ansible-playbook ${VERBOSE:+-$VERBOSE} -c local bootstrap.yml $BSTAGS ${BSARGS[*]}"
	RET=$?
	popd >/dev/null || exit 1
fi

[[ $RET -eq 0 ]] && as_boolean "$LOG_UPDATE" >/dev/null && rm -f "$LOGFILE"

popd >/dev/null || exit 0