![ApisCP Logo](https://apiscp.com/images/logo-inv.svg)

ApisCP is an modern hosting panel + platform that began in 2002 as a panel for Apis Networks. ApisCP runs agentless and is 100% self-hosted. Licenses can be purchased from [my.apiscp.com](https://my.apiscp.com). 

## Requirements

-   2 GB RAM
-   [Forward-confirmed reverse DNS](https://en.wikipedia.org/wiki/Forward-confirmed_reverse_DNS), i.e. 64.22.68.1 <-> apiscp.com
-   CentOS 7+ or RedHat 7+

## Installation

See [docs.apiscp.com](https://docs.apiscp.com/INSTALL/). Installation may be customized using the [utility](https://apiscp.com/#customize) on apiscp.com.

## Contributing

Use the [Issue Tracker](https://github.com/apisnetworks/apiscp/issues) to post feature requests.

## License

ApisCP is (c) Apis Networks. All components except for third-party modules and [ApisCP modules](https://github.com/apisnetworks/apnscp-modules) are licensed under a [commercial license](https://bitbucket.org/apisnetworks/apnscp/raw/HEAD/LICENSE). Contact licensing@apisnetworks.com for licensing enquiries. Any dissemination of material herein is prohibited without expressed written consent of Apis Networks.
